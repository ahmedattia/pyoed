# How to Contribute Code to the Repository

## Get started
- create Issue (on gitlab.com: Issues --> New issue)
- create branch (from "main" branch) by clicking the "create branch" link (under the issue)
- if needed, edit "Labels" and "Miltstones" under this issue 
- now, start coding and commit in the branch for the issue

## Create unit test(s)
- write a script in directory `tests` under each subpackage that tests the new feature(s)

## When code development is finished
- start pull request: on github.com, select the branch from the dropdown
  (usually saying `master`), then pop open the `contribute` menu on the right,
  and select to start a pull request
- Create a merge request (on gitlab.com: "Merge request" --> "New merge request")
- assign reviewer(s) to the pull request
- iterate with the people until everybody is happy
- finally, you or the reviewer merge the pull request into main branch 
