# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides access to functionality related to configurations of PyOED objects.
For example, any simulation model is instantiated with default configurations some of
which can be updated at runtime.  This module provides rigorous class and functions to
help create, validate and update configurations.

If you are building a new object that inherits from one of the base classes in PyOED,
you should familiarize yourself with `PyOEDConfigs` to understand how to define the
parameters of your object. In most instances, you need not to anything more than define
a class inheriting `PyOEDConfigs` and write the fields you need.  See the configuration
tutorial for more information.
"""
from __future__ import annotations  # allow using classes/names to be defined later
import os
from pathlib import Path
import tempfile
import errno
import shutil
import inspect
import re
import textwrap
from docstring_parser import parse

from typing import Any, Self, Iterable, Type
from dataclasses import dataclass, asdict, fields, replace
from abc import ABCMeta

from pyoed.utility import path_is_accessible

# Define all functions here so it can be easily aggregated in __init__.py of utility package
__all__ = [
    "SETTINGS",
    "PyOEDContainer",
    "PyOEDConfigs",
    "PyOEDObject",
    "PyOEDData",
    "PyOEDConfigsValidationError",
    "set_configurations",
    "remove_unknown_keys",
    "validate_key",
]


class SETTINGS:
    """A class for PYOED settings"""

    # Base output directory
    OUTPUT_DIR = "./_PYOED_RESULTS_"

    # Global Random Seed
    RANDOM_SEED = None

    # Basic verbosity
    VERBOSE = False

    # Global Debug mode
    DEBUG = False


@dataclass(kw_only=True, slots=True)
class PyOEDContainer:
    """
    Base class for creating containers to encompass PyOED's objects configurations and data, etc..

    .. note::
        This is an empty container that provides basic functionality but it does **NOT** provide
        any fields/attributes, and is intended to be derived by spcific implementations of classes
        encoding configurations, data, etc.
    """

    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  PyOED Empty Container\n{sep}\n"
        return msg

    def asdict(self, deep: bool = False, ignore_error=False, ) -> dict:
        """
        Return a dictionary representation of the object attributes. Any object is
        references in the returned configurations.

        :param deep: a boolean flag to control whether to return a deep copy of the.
            object or not. If True, this mirror the functionality of `asdict` from
            `dataclasses` module. If False, only a shallow copy of the values is
            returned.
        :param ignore_error: if `True` errors raised tue to using `deep` are ignored.
            See the note below about requirement of objects to be picklable/serializable.

        :returns: a dictionary representation of the container with/without deep copy
            of the objects associated with the container's attributes.

        .. note::
            The `deep` option is tricky as it requires all objets associated with
            the container attributes to be picklable/serializable.
            Thus, we added the option `ignore_error` to ignore such error
            (if it happens when ``deep=True``) in which case (that is, when
            both `deep` and `ignore_error` are set to `True`), `deep` is discarded
            and a shallow copy is created instead. A proper message is printed to
            show that.
        """
        if deep:
            try:
                res = asdict(self)
            except(Exception) as err:
                msg = f"Failed to deep copy some of the elements; See message below;\n"
                msg += f"Unexpected {err=} of {type(err)=}. "
                if ignore_error:
                    msg += "\n***NOTE: This Error Ignored As Requested. "
                    msg += "Shallow Copy of configs is created instead***\n"
                print(msg)
                if not ignore_error:
                    # Raise the error
                    raise err
                else:
                    # Ignore and create a shallow copy
                    res = {name: value for name, value in self.items()}

            return res
        else:
            return {name: value for name, value in self.items()}

    def items(self) -> Iterable[tuple[str, Any]]:
        """
        Return an iterable of tuples of the object attributes. The tuples are in the
        form (name, value). This is similar to the `items` method of a dictionary.

        :return: an iterable of tuples of the object attributes.
        """
        for field in fields(self):
            yield field.name, getattr(self, field.name)

    def lookup(self, name: str, data: dict | Self) -> tuple[bool, Any]:
        """
        A helper function to lookup a `name` in `data` if and only if the name is
        a field of self.

        :param name: the name of the field to lookup.
        :param data: the dictionary to lookup the name in.
        :return: a tuple of boolean and value. The boolean indicates whether the name
            is found in the data or not. The value is the value associated with the
            name in the data. If the name is not found, the value is None.
        :raises AttributeError: if the name is not a field in self.
        """
        if name not in {field.name for field in fields(self)}:
            raise AttributeError(f"The {name=} is not a field of the object!")
        return name in data, data.get(name, None)

    def update(
        self,
        new_configs: dict | Self,
    ) -> Self:
        """
        Update the configurations of this object with the passed configurations.
        The passed configurations can be a dictionary or an instance of `Self`.

        :param new_configs: the configurations to update this object with.
        """
        if not isinstance(new_configs, (PyOEDContainer, dict)):
            raise TypeError(
                "Expected either a dictionary or instance of `PyOEDContainer`!"
                f"Received {type(new_configs)=}"
            )
        for name, value in new_configs.items():
            setattr(self, name, value)
        return self

    @classmethod
    def data_to_dataclass(
        cls: Self,
        data: dict | Self | None,
    ) -> Self:
        """
        Convert data from a couple different forms into a dataclass instance.

        :param dict | dataclass | None data: data dictionary or dataclass instance or
            None. If None, the default configurations of the dataclass are used. If
            a dict, the dataclass is populated with the dictionary values. If a
            dataclass, the dataclass is returned as is.

        :raises TypeError: if the passed `data` is not a dictionary, an instance of
            `cls`, or None.
        """
        if not isinstance(data, (cls, dict, type(None))):
            raise TypeError(
                f"Expected data to be a dict, instance of `{cls}`, or None. \n"
                f"Received {type(data)=}"
            )
        if isinstance(data, cls):
            return data
        elif isinstance(data, dict):
            try:
                return cls(**data)
            except Exception as err:
                msg = f"     Failed to create {cls} instance from the passed data"
                sep =  "*" * (len(msg) + 10)
                msg = f"\n{sep}\n{msg}\n{sep}\n"
                msg += f"Expected full/partial data:\n"
                for f in fields(cls):
                    msg += f" >> Class key/field {f.name=}: >> {f.type=}; {f.default=} \n"
                msg += f"{sep}\nReceived data:\n"
                for d in data:
                    msg += f" >> Data key (d): >> {type(d)=} >> {d=}\n"
                msg += f"{sep}\nUnsupported (unrecognized) fields:\n"
                for d in set.difference(set([f for f in data]), set([f.name for f in fields(cls)])):
                    msg += f" >> Data key (d): >> {type(d)=} >> {d=}\n"
                msg += f"{sep}\nThis error is raises because the error below was encountered:"
                msg += f"\nUnexpected {err=}\n{sep}"
                raise AttributeError(msg) from err
        return cls()

@dataclass(kw_only=True, slots=True)
class PyOEDData(PyOEDContainer):
    """
    Base class for data objects in PyOED.

    .. note::
        This class is not necessarily deployed by objects in PyOED since each algorithms
        has its own output structure.
        Thus, adopting this class is not mandatory.
    """

    def __init_subclass__(cls, *args, **kwargs):
        """
        Modify the documentation of a subclass to aggregate parameters of the class with parent's.
        """
        class_doc_inheritance(
            cls=cls,
            aggr_params=True,           # This is what we need!
            aggr_raises=False,          # Data classes should not raise anything!
            aggr_examples=False,        # No examples, but if any, only child's is included
            aggr_returns=False,         # Class constructor does not return!
            ignore_common_params=True,  # Don't repeat parameters (take child only)
        )

    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  PyOED Empty Data Container\n{sep}\n"
        return msg

    def __getitem__(self, name):
        """Overload the getattr with subscription"""
        return getattr(self, name)

    def __setitem__(self, name, value):
        """Overload the setattr with subscription"""
        return setattr(self, name, value)

@dataclass(kw_only=True, slots=True)
class PyOEDConfigs(PyOEDContainer):
    """
    Base class for PyOED's objects configurations.
    All objects (simulation models, optimizers, etc.) have their own configurations.
    Thus, to unify and simplify checking and configuration definition, we recommend
    using a class derived from PyOED's based configuration class :py:class:`PyOEDConfigs`.

    In the past (up to PyOED 1.0,), configurations were held in dictionaries, which made
    it possible to pass configurations keys  those are ignored by the implementation
    (e.g., a simulation model). This caused a lot of errors those were hard to track.
    The intention of this class is to provide access to configurations in a unified form
    that makes it easy to develop new algorithms while avoiding such complications/errors.

    :param verbose: a boolean flag to control verbosity of the object.
    :param debug: a boolean flag that enables adding extra functionlity in a debug mode
    :param output_dir: the base directory where the output files will be saved.
    """

    # Basic configurations (imported from PyOED's default settings)
    debug: bool = SETTINGS.DEBUG
    verbose: bool = SETTINGS.VERBOSE
    output_dir: str | Path = SETTINGS.OUTPUT_DIR

    def __init_subclass__(cls, *args, **kwargs):
        """
        Modify the documentation of a subclass to aggregate parameters of the class with parent's.
        """
        class_doc_inheritance(
            cls=cls,
            aggr_params=True,           # This is what we need!
            aggr_raises=False,          # Data classes should not raise anything!
            aggr_examples=False,        # No examples, but if any, only child's is included
            aggr_returns=False,         # Class constructor does not return!
            ignore_common_params=True,  # Don't repeat parameters (take child only)
        )


    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  Configurations of: `{name}`\n{sep}\n"
        for field in fields(self):
            name = field.name
            msg += f"  >> {name} (type {type(getattr(self, name))}): {repr(getattr(self, name))}\n"
        msg += f"{sep}\n"
        return msg


## Decorator to set configurations class
def set_configurations(configurations_class: Type[PyOEDConfigs]):
    """
    This decorator is used to set the configurations class of an instance of a class
    that inherits from :py:class:`PyOEDObject`.

    Functionally syntactic sugar for setting the `_CONFIGURATIONS_CLASS` attribute of
    the class.

    :param configurations_class: the configurations class to set.
    """

    def decorator(cls):
        if not issubclass(configurations_class, PyOEDConfigs):
            raise TypeError(
                f"Expected configurations_class to be a subclass of `PyOEDConfigs`. "
                f"Received {configurations_class=}"
            )
        cls._CONFIGURATIONS_CLASS = configurations_class
        return cls

    return decorator


class PyOEDABCMeta(ABCMeta):
    """Meta class `PyOEDABCMeta`"""
    def __init__(cls, clsname, bases, attrs):
        # Restore the signature
        sig = inspect.signature(cls.__init__)
        parameters = tuple(sig.parameters.values())
        cls.__signature__ = sig.replace(parameters=parameters[1:])
        return super().__init__(clsname, bases, attrs)

    # TODO: add docstring
    def __call__(cls, *args, **kwargs):
        if "validate_configurations" in cls.__dict__:
            cls._HAS_VALIDATION = True
        else:
            cls._HAS_VALIDATION = False
        return super().__call__(*args, **kwargs)


# TODO: Update this from tst.py under doc/source once implemented...
def class_doc_inheritance(
    cls,
    aggr_params=True,
    aggr_raises=False,
    aggr_examples=False,
    aggr_returns=False,
    ignore_common_params=True,
):
    """
    A function (also can be used as a class wrapper) that modifies (in-place) class's docstring
    `__doc__` to include parameters, raises, etc., defined by the child as well as the parent class.
    This function takes the description from the passed class `cls` and
    aggregates parameters, etc., from itself with its parents.

    .. note::

        The package `docstring_parser` gives access to the following elements.
        If expanded, we can adapt:

          - depracation
          - description
          - params
          - examples
          - returns
          - many_returns
          - meta
          - raises
          - short_description

    :param cls: The class which documentation `__doc__` is to be modified
    :param aggr_params: aggregate parameter clauses (:param :) from both `cls` and its parent class
    :param aggr_raises: aggregate raise clauses from both `cls` and its parent class
    :param aggr_examples: aggregate example clauses from both `cls` and its parent class
    :param aggr_returns: aggregate return clauses from both `cls` and its parent class
    :param ignore_common_params: Only include parameters in child

    :returns: The passed class `cls` so this function can be used as a wrapper
    """
    def param_indentation(docstring, param_name):
        """
        Lookup the line that starts with :param param_name:
        and count number of indentation spaces

        .. note::
            This function might might be moved out...
        """
        lines = docstring.split("\n")
        matcher = f"( )*:( )*param( )*{param_name}( )*:( )*"
        indentations = None
        for l in lines:
            # print(f"{l=}")
            if re.match(rf"\A{matcher}", l, re.IGNORECASE):
                indentations = len(l.lstrip('\n')[: l.index(':param')])
                break
        # print({f"indentations="})
        return indentations

    ## Get docstrings and information...
    # Docstring of the passed class and its parent (if None, replace with empty string)
    cls_doc = cls.__doc__ or ""
    parent_doc = cls.__base__.__doc__ or ""

    # Parse and get indentations
    docstring = parse(cls_doc)
    parent_docstring = parse(parent_doc)

    # Try to get indentations based on where :param ...: is on any line
    # We may be able to use ast instead
    if len(docstring.params) > 0:
        indentations = param_indentation(cls_doc, docstring.params[0].arg_name)
    elif len(parent_docstring.params) > 0:
        indentations = param_indentation(parent_doc, parent_docstring.params[0].arg_name)
    else:
        indentations = None
    # Just in case fix indentations,
    if indentations is None: indentations = 0

    ## Reparse & Dedent docstrings
    docstring = parse(textwrap.dedent(cls_doc))
    parent_docstring = parse(textwrap.dedent(parent_doc))

    ## Initiate with description
    ############################

    # Initiate new docstring
    if docstring.description is None:
        new_docstring = "\n"
    else:
        new_docstring = docstring.description.lstrip('\n ')
    if docstring.blank_after_long_description:
        new_docstring += "\n\n"
    else:
        new_docstring += "\n\n\n"

    ## Aggregate parameters
    #######################
    target = [parent_docstring, docstring] if aggr_params else [docstring]
    docstring_param_names = [param.arg_name for param in docstring.params]
    parent_docstring_param_names = [param.arg_name for param in parent_docstring.params]
    for ds in target:
        if len(ds.params) > 0:
            for param in ds.params:
                # print(f"Aggregating {param.arg_name}")
                if (
                    ignore_common_params and
                    ds is parent_docstring and
                    param.arg_name in docstring_param_names
                ):
                    # print(f"Skipping duplicates")
                    continue
                #
                param_str = f":{' '.join(param.args).strip(' ')}: "
                for i, l in enumerate(param.description.split("\n")):
                    if i == 0:
                        param_str += l.lstrip(' ')
                    else:
                        param_str += textwrap.indent(l, " "*indentations)
                    param_str += "\n"
                # param_str += "\n"
                new_docstring += param_str
    new_docstring += "\n"

    # print(f"****\nAfter Aggregation: \n{new_docstring}\n****")
    ## Aggregate returns
    #######################
    target = [parent_docstring, docstring] if aggr_returns else [docstring]
    for ds in target:
        if len(ds.many_returns) > 0:
            for param in ds.many_returns:
                param_str = f":{' '.join(param.args).strip(' ')}: "
                for i, l in enumerate(param.description.split("\n")):
                    if i == 0:
                        param_str += l.lstrip(' ')
                    else:
                        param_str += textwrap.indent(l, " "*indentations)
                    param_str += "\n"
                # param_str += "\n"
            new_docstring += param_str
    new_docstring += "\n"


    ## Aggregate raises
    #######################
    target = [parent_docstring, docstring] if aggr_returns else [docstring]
    for ds in target:
        if len(ds.raises) > 0:
            for param in ds.raises:
                param_str = f":{' '.join(param.args).strip(' ')}: "
                for i, l in enumerate(param.description.split("\n")):
                    if i == 0:
                        param_str += l.lstrip(' ')
                    else:
                        param_str += textwrap.indent(l, " "*indentations)
                    param_str += "\n"
                # param_str += "\n"
            new_docstring += param_str
    new_docstring += "\n"


    ## Aggregate examples
    #######################
    if aggr_examples:
        raise NotImplementedError(
            "Aggregating examples is not supported (yet!)"
        )

    # print("Decorator...", cls.__doc__, new_docstring)
    # Apply indentation to
    cls.__doc__ = textwrap.indent(new_docstring, " "*indentations)

    return cls


@set_configurations(configurations_class=PyOEDConfigs)
class PyOEDObject(metaclass=PyOEDABCMeta):
    """
    Base class for all PyOED Objects
    Here is where `_CONFIGURATIOSN` attribute is associated with all PyOED
    objects to hold the configurations/settings of the object.
    Also, this checks whether a class has a validation method
    :py:meth:`validate_configurations` of its own or not.

    :param configs: configurations of the object.
        This can be full or partial dictionary, or an instance of the registered configurations object.
    """
    _CONFIGURATIONS_CLASS: Type[PyOEDConfigs] = PyOEDConfigs
    _HAS_VALIDATION: bool = True

    def __init__(self, configs: dict | PyOEDConfigs | None = None) -> None:
        # Make sure the passed configurations object is valid
        configs = self.configurations_class.data_to_dataclass(configs)
        if self._HAS_VALIDATION:
            self.validate_configurations(configs)
        self._CONFIGURATIONS = configs

        # Call super (to make sure mixins are included)
        super().__init__()

    # TODO: Consider replacing metclass with __init_subclass__ and add support for class_doc_inheritance

    def validate_configurations(
        self,
        configs: dict | PyOEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation model **MUST** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        .. note::
            The purposed of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a PyOEDConfigs object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`PyOEDConfigs`.
        """
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        ## Validation Stage
        # `verbose`: bool
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="verbose",
            test=lambda v: bool(v) == v,
            message=f"Verbosity flag 'verbose' need to be a boolean.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `debug`: bool
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="debug",
            test=lambda v: bool(v) == v,
            message=f"Debug model flag 'debug' need to be a boolean.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `output_dir`: str or Path
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="output_dir",
            test=lambda v: v is None or path_is_accessible(v),
            message=lambda v: f"Output directory `output_dir` is invalid.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return True

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations object, and update the values accordingly.

        .. note::
            This method is elementary and does not do any type of casting.
            If the user want to do any processing on the data, this method
            need to be overriden with custom implementation.

        .. warning::
            This method can be only called after the `PyOEDObject` is fully instantiated.
            Thus, it cannot be called withing `__init__` method of the PyOED object.
        """
        try:
            self._CONFIGURATIONS
        except AttributeError as err:
            raise PyOEDRulesViolationError(
                f"The method `update_configurations` can be only used after a `PyOEDObject` "
                f"is instantiated. Calling this method inside `__main__` is prohibited. Check below for details"
                f"Unexpected {err=} of {type(err)=}"
            )
        self.validate_configurations(configs=kwargs, raise_for_invalid=True, )

        # Update configurations
        self.configurations.update(kwargs)

        # TODO: Consider the case of nested configurations (as soon as visited) ...
        # ...


    def aggregate_configurations(self, configs: PyOEDConfigs | dict, ) -> PyOEDConfigs:
        """
        Aggregate the passed configurations object/dictionary `configs` into the current
        configurations (if present, that is after full instantiation of the object/self)
        or the default configurations if the object has not been full instantiated yet.

        .. note::
            This is a wrapper around `pyoed.configs.aggregate_configurations`
        """
        return aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

    @classmethod
    def get_default_configurations(self_or_cls) -> PyOEDConfigs | None:
        """
        A class-level method that return the default configurations associated with this class
        if this class is associated with one.
        This inspects whether the attribute `_CONFIGURATIONS_CLASS` is available or not.
        `None` is returned if no configurations class is set.
        """
        if hasattr(self_or_cls, '_CONFIGURATIONS_CLASS'):
            return self_or_cls._CONFIGURATIONS_CLASS()
        else:
            return None

    @classmethod
    def get_configurations_class(self_or_cls) -> Type[PyOEDConfigs] | None:
        """
        A class-level method that return the configurations class associated with this class
        if this class is associated with one.
        This inspects whether the attribute `_CONFIGURATIONS_CLASS` is available or not.
        `None` is returned if no configurations class is set.
        """
        if hasattr(self_or_cls, '_CONFIGURATIONS_CLASS'):
            return self_or_cls._CONFIGURATIONS_CLASS
        else:
            return None


    @property
    def configurations(self) -> PyOEDConfigs:
        return self._CONFIGURATIONS

    @property
    def configurations_class(self) -> Type[PyOEDConfigs]:
        return self._CONFIGURATIONS_CLASS

    @property
    def verbose(self) -> bool:
        return self.configurations.verbose
    @verbose.setter
    def verbose(self, val) -> None:
        self.configurations.verbose = bool(val)

    @property
    def debug(self) -> bool:
        return self.configurations.debug
    @debug.setter
    def debug(self, val) -> None:
        self.configurations.debug = bool(val)

    @classmethod
    @property
    def default_configurations(cls_or_cls):
        return cls_or_cls.get_default_configurations()


class PyOEDRulesViolationError(Exception):
    """Exception raised for violating PyOED rules.

    :param message: explanation of the error.  Default message is created if `None` is
        passed.
    """

    def __init__(self, message: str | None = None):
        if message is None:
            message = "\n************\nPyOED Rule(s) violated.\n************\n"
        self.message = message
        super().__init__(self.message)


class PyOEDConfigsValidationError(Exception):
    """Exception raised for errors in the configurations validators.

    :param message: explanation of the error.  Default message is created if `None` is
        passed.
    """

    def __init__(self, message: str | None = None):
        if message is None:
            message = "Invalid configuration key/value detected."
        self.message = message
        super().__init__(self.message)


def remove_unknown_keys(
    data: dict | PyOEDConfigs,
    target_dataclass: Type[PyOEDConfigs]
) -> dict:
    """
    Remove keys from data that are not part of target_dataclass.

    :param data: the dictionary to remove keys from.
    :param parent_dataclass: the parent dataclass.

    :returns: a dictionary holding keys/values in `data` those are
        acceptable/recognized by (i.e., members of) `target_dataclass`

    :raises TypeError: if the passed arguments are of wrong type
    """
    if not issubclass(
        target_dataclass, PyOEDConfigs
    ):
        raise TypeError(
            f"Expected target_dataclass to be subclass "
            f"of `PyOEDConfigs`. Received {target_dataclass}"
        )
    if not isinstance(
        data, (dict, PyOEDConfigs)
    ):
        raise TypeError(
            f"Unsupported configurations/data type: {type(data)=}"
        )

    ## New configs to dictionary
    if isinstance(data, PyOEDConfigs):
        data = data.asdict(deep=False)

    parent_keys = {field.name for field in fields(target_dataclass)}
    for key in list(data.keys()):
        if key not in parent_keys:
            data.pop(key)
    return data

def extract_unknown_keys(
    data: dict | PyOEDConfigs,
    target_dataclass: Type[PyOEDConfigs]
) -> dict:
    """
    Extract the keys from data that are not part of target_dataclass.
    The extracted keys (if any) and the associated values are
    inserted into a dictionary that is returned.

    :param data: the dictionary to remove keys from.
    :param parent_dataclass: the parent dataclass.

    :returns: the dictionary holding keys in `data` not acceptable
        (i.e., not part of) the `target_dataclass`.

    :raises TypeError: if the passed arguments are of wrong type
    """
    if not issubclass(
        target_dataclass, PyOEDConfigs
    ):
        raise TypeError(
            f"Expected target_dataclass to be subclass "
            f"of `PyOEDConfigs`. Received {target_dataclass}"
        )
    if not isinstance(
        data, (dict, PyOEDConfigs)
    ):
        raise TypeError(
            f"Unsupported configurations/data type: {type(data)=}"
        )

    ## New configs to dictionary
    if isinstance(data, PyOEDConfigs):
        data = data.asdict(deep=False)

    unknown_configs = {}
    parent_keys = {field.name for field in fields(target_dataclass)}
    for key in list(data.keys()):
        if key not in parent_keys:
            unknown_configs.update(
                {key: data[key]}
            )
    return unknown_configs

def validate_key(
    current_configs: PyOEDConfigs,
    new_configs: dict | PyOEDConfigs,
    key: str,
    test: callable,
    message: str | callable | None = None,
    raise_for_invalid: bool = True,
) -> bool:
    """
    Given a configurations dictionary or configurations object (derived from
    :py:class:`PyOEDConfigs`), apply the test to the value of the `key` extracted from
    `configs` if it exists.

    :param current_configs: a configurations object to which the `key` is expected to
        fit.
    :param new_configs: a configurations dictionary/object holding a subset (or all) of keys or
        attributes of `current_configs` to test validity of.
    :param key: name of the new configurations key/attribute to lookup inside
        `new_configs` and apply the test to.
    :param message: the error message to use if the test fails and `raise_for_invalid`
        is `True`.
    :param test: a test that takes as input the value associated with the `key`
        attribute/key in `new_configs`, and returns a `bool` that is `True` if the test
        is satisfied, and `False` otherwise.  For example, the following test is passed
        if the user wants to make sure `new_configs.key > 4`.

        ```
        test = lambda v: v > 4
        ```

    .. note::
        The logic of this function is as follows:

            1. Assertion error is raised if any of the arguments is invalid
            2. A custom `PyOEDConfigsValidationError` exception is raised if
               `raise_for_invalid` is `True` and the test is not satisfied or if `key`
               is not a member of `current_configs`.
            3. Return the result (bool value) of `test` applied to the value associated
               with `new_configs.key` returns.

    .. note::
        If the `message` is a callable, it is supposed to take the value as input and return a string
        that is passed to the exception constructor.

    :returns: a boolean flag based on the output of `test`
    """
    ## Check/Assert inputs
    assert callable(test), f"Expected `test` to be callable. Received {type(test)=}"
    assert isinstance(
        current_configs, PyOEDConfigs
    ), f"Unsupported configurations type: {type(current_configs)=}"
    assert isinstance(
        new_configs, (dict, PyOEDConfigs)
    ), f"Unsupported configurations type: {type(new_configs)=}"
    assert (
        isinstance(key, str) and len(key) > 0
    ), f"Expected nonempty string `key`. Received {key=} of {type(key)=}"

    ## New configs to dictionary
    if isinstance(new_configs, PyOEDConfigs):
        new_configs = new_configs.asdict(deep=False)

    ## Validation Stage

    # Check if `key` is a member of `current_configs` and then lookup `key` inside `configs`
    try:
        exists, value = current_configs.lookup(key, new_configs)
    except Exception as err:
        raise PyOEDConfigsValidationError(
            f"The {key=} is not a member of the current configurations object `current_configs`"
        ) from err

    ## If the message is a callable, apply it to the value to retrieve a string
    if callable(message):
        message = message(value)

    ## If the `key` exists apply the test
    if exists:
        if not test(value):
            if raise_for_invalid:
                message += f"\nRecieved {key}={value} of type{type(value)}"
                raise PyOEDConfigsValidationError(message)
            return False

    return True

def aggregate_configurations(
    obj: object,
    configs: PyOEDConfigs | dict,
    configs_class: Type[PyOEDConfigs],
) -> PyOEDConfigs:
    """
    Given an object `obj`, configurations object/dictionary `configs` and a
    configurations class `configs_class`, aggregate `cofigs` into the current
    configurations of the object (the attribute `_CONFIGURATIONS`) if present, otherwise
    it aggregates `configs` into the default configurations created by instantiating
    `configs_class`.
    """
    # Validation
    assert isinstance(
        obj, object
    ), f"aggregate_configurations must be called by an object not {type(obj)}"
    assert issubclass(
        configs_class, PyOEDConfigs
    ), f"Expected configs_class passed to the \
        `configurations_validator` to be subclass of `PyOEDConfigs`; passed {configs_class}"

    if not isinstance(configs, (configs_class, dict)):
        raise TypeError(
            f"Expected `configs` to be a dict, valid instance of `{configs_class}`. "
            f"However, the configurations `configs` is of type {type(configs)}"
        )
    if isinstance(configs, configs_class):
        configs = configs.asdict(deep=False)

    # Aggregate configurations
    if not hasattr(obj, "_CONFIGURATIONS"):  # If not instantiated
        base_configs = configs_class()
    else:
        base_configs = obj._CONFIGURATIONS
    aggregated_configs = replace(base_configs, **configs)

    return aggregated_configs

