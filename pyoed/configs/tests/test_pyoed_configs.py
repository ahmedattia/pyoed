import os
from dataclasses import asdict, field, dataclass
from copy import copy, deepcopy

import pytest
from pyoed.utility import path_is_accessible

from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)
from pyoed.configs.configs import (
    PyOEDConfigs,
    PyOEDData,
    PyOEDObject,
    PyOEDConfigsValidationError,
    validate_key,
    remove_unknown_keys,
    aggregate_configurations,
    set_configurations,
)

pytestmark = pytest.mark.configs

# Settings/Configurations:
# ========================

# Create a random number generator
# ================================
_RNG = create_random_number_generator()


# Tests (Classes & Functions):
# ============================


# Tests: (pyoed.configs.PyOEDConfigs)
# -----------------------------------
@dataclass(kw_only=True, slots=True)
class TestConfigs(PyOEDConfigs):
    __test__ = False
    mutable_value: dict = field(default_factory=dict)


def test_PyOEDConfigs_items():
    config = PyOEDConfigs()
    for key, value in config.items():
        assert getattr(config, key) == value, "Failed to iterate through the config!"


def test_PyOEDConfigs_update():
    configs_default = PyOEDConfigs()

    # Try and update one item
    modified_configs_dict = {"verbose": not configs_default.verbose}

    new_configs = copy(configs_default)
    new_configs.update(modified_configs_dict)
    assert new_configs.verbose == (
        not configs_default.verbose
    ), "Failed to update a single item from dict"

    # Try and update multiple items
    modified_configs_class = copy(configs_default)
    modified_configs_class.verbose = not configs_default.verbose
    modified_configs_class.output_dir = os.path.join(
        modified_configs_class.output_dir, "TEST"
    )
    modified_configs_dict = modified_configs_class.asdict()

    new_configs = copy(configs_default)
    new_configs.update(modified_configs_dict)
    assert (new_configs.verbose == modified_configs_class.verbose) and (
        new_configs.output_dir == modified_configs_class.output_dir
    ), "Failed to update multiple items from dict"
    new_configs = copy(configs_default)
    new_configs.update(modified_configs_class)
    assert (new_configs.verbose == modified_configs_class.verbose) and (
        new_configs.output_dir == modified_configs_class.output_dir
    ), "Failed to update multiple items from class"

    # Try and update to an invalid field
    modified_configs_class = copy(configs_default)
    modified_configs_dict = modified_configs_class.asdict()
    modified_configs_dict.update({"new_field": 10})

    new_configs = copy(configs_default)
    with pytest.raises(AttributeError):
        new_configs.update(modified_configs_dict)


def test_PyOEDConfigs_asdict():
    # Don't have to test dataclass asdict, but will test the shallow copy behavior
    configs = TestConfigs()
    configs.mutable_value.update({"a": 1, "b": 2})
    deep_configs_dict = configs.asdict(deep=True)
    shallow_configs_dict = configs.asdict()
    configs.mutable_value["a"] = 10
    assert (
        shallow_configs_dict["mutable_value"]["a"] == configs.mutable_value["a"]
    ), "asdict is no longer shallow copying, this is a change in intended behavior!"
    assert (deep_configs_dict["mutable_value"]["a"] != configs.mutable_value["a"]) and (
        deep_configs_dict["mutable_value"]["a"] == 1
    ), "asdict is not deep copying, this is a change in intended behavior!"


def test_PyOEDConfigs_lookup():
    # Must test the following behaviors
    # 1. name not in dataclass raises
    # 2. True, value works
    # 3. False, None works

    configs = PyOEDConfigs()
    configs_dict = configs.asdict()

    # 1.
    with pytest.raises(AttributeError):
        configs.lookup("invalid_field", configs_dict)

    # 2.
    for key, value in configs_dict.items():
        exists, lookup_value = configs.lookup(key, configs_dict)
        assert exists and (value == lookup_value), "Failed to lookup existing field!"


def test_PyOEDConfigs_setattr():
    # Two things to test
    # 1. Setting a valid field
    # 2. Setting an invalid field (AttributeError due to slots)

    configs_default = PyOEDConfigs()

    # 1. Setting a valid field
    configs_default.verbose = not configs_default.verbose
    assert configs_default.verbose == (
        not PyOEDConfigs().verbose
    ), "Failed to set an existing field!"

    # 2. Setting an invalid field
    with pytest.raises(AttributeError):
        configs_default.new_field = 10


def test_PyOEDConfigs_data_to_dataclass():
    # Must test the following behaviors
    # 1. data_to_dataclass creates from None
    # 2. data_to_dataclass creates from dict
    # 3. data_to_dataclass creates from dataclass
    # 4. data_to_dataclass raises TypeError for invalid input
    # 5. data_to_dataclass raises TypeError for invalid field

    configs = PyOEDConfigs()

    # 1. data_to_dataclass creates from None
    new_configs = PyOEDConfigs.data_to_dataclass(None)
    for key, value in configs.items():
        assert getattr(new_configs, key) == value, "Failed to create from None!"

    # Modify configs so that we can test the other cases
    configs.verbose = not configs.verbose

    # 2. data_to_dataclass creates from dict
    new_configs = PyOEDConfigs.data_to_dataclass(configs.asdict())
    for key, value in configs.items():
        assert getattr(new_configs, key) == value, "Failed to create from dict!"

    # 3. data_to_dataclass creates from dataclass
    new_configs = PyOEDConfigs.data_to_dataclass(configs)
    for key, value in configs.items():
        assert getattr(new_configs, key) == value, "Failed to create from dataclass!"

    # 4. data_to_dataclass raises TypeError for invalid input
    with pytest.raises(TypeError):
        PyOEDConfigs.data_to_dataclass([])

    # 5. data_to_dataclass raises AttributeError for invalid field
    with pytest.raises(AttributeError):
        PyOEDConfigs.data_to_dataclass({"invalid_field": "value"})


# Tests: (pyoed.configs.PyOEObject)
class TestObjectNoValidation(PyOEDObject):
    __test__ = False
    def __init__(self, configs):
        super().__init__()


@set_configurations(TestConfigs)
class TestObjectIndependentValidation(PyOEDObject):
    __test__ = False

    def __init__(self, configs):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self, configs: dict | TestConfigs, raise_for_invalid: bool = True
    ) -> bool:
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        ## Validation Stage
        # `verbose`: bool
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="verbose",
            test=lambda v: bool(v) == v,
            message=f"Verbosity need to be a boolean.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `output_dir`: str or Path
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="output_dir",
            test=lambda v: path_is_accessible(v),
            message=lambda v: f"Output directory `output_dir` is invalid. \
                \nPath is accessible? {path_is_accessible(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="mutable_value",
            test=lambda v: len(v) <= 1,
            message=f"Mutable value `mutable_value` has more than one entry.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return True


@set_configurations(TestConfigs)
class TestObjectInheritedValidation(PyOEDObject):
    __test__ = False

    def __init__(self, configs):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self, configs: dict | TestConfigs, raise_for_invalid: bool = True
    ) -> bool:
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="mutable_value",
            test=lambda v: len(v) <= 1,
            message=f"Mutable value `mutable_value` has more than one entry.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)


def test_PyOEDObject_init():
    # Test the following behaviors
    # 1. No validation
    # 2. Independent validation
    # 3. Inherited validation

    # 1. No validation
    broken_configs = TestConfigs(verbose=1)
    TestObjectNoValidation(broken_configs)  # Should not raise

    # 2. Independent validation
    TestObjectIndependentValidation(TestConfigs())
    with pytest.raises(PyOEDConfigsValidationError):
        TestObjectIndependentValidation(TestConfigs(verbose=0.5))
    with pytest.raises(PyOEDConfigsValidationError):
        TestObjectIndependentValidation(TestConfigs(mutable_value={"a": 1, "b": 2}))

    # 3. Inherited validation
    TestObjectInheritedValidation(TestConfigs())
    with pytest.raises(PyOEDConfigsValidationError):
        TestObjectInheritedValidation(TestConfigs(verbose=0.5))
    with pytest.raises(PyOEDConfigsValidationError):
        TestObjectIndependentValidation(TestConfigs(mutable_value={"a": 1, "b": 2}))


# Tests: (pyoed.configs.PyOEDData)
# -----------------------------------
@dataclass(kw_only=True, slots=True)
class TestData(PyOEDData):
    __test__ = False
    mutable_value: dict = field(default_factory=dict)


def test_PyOEDData_items():
    config = PyOEDData()
    for key, value in config.items():
        assert getattr(config, key) == value, "Failed to iterate through the config!"


def test_PyOEDData_asdict():
    # Don't have to test dataclass asdict, but will test the shallow copy behavior
    data = TestData()
    data.mutable_value.update({"a": 1, "b": 2})
    deep_data_dict = data.asdict(deep=True, )
    shallow_data_dict = data.asdict()
    data.mutable_value["a"] = 10
    assert (
        shallow_data_dict["mutable_value"]["a"] == data.mutable_value["a"]
    ), "asdict is no longer shallow copying, this is a change in intended behavior!"
    assert (deep_data_dict["mutable_value"]["a"] != data.mutable_value["a"]) and (
        deep_data_dict["mutable_value"]["a"] == 1
    ), "asdict is not deep copying, this is a change in intended behavior!"


def test_PyOEDData_lookup():
    # Must test the following behaviors
    # 1. name not in dataclass raises
    # 2. True, value works
    # 3. False, None works

    data = PyOEDData()
    data_dict = data.asdict()

    # 1.
    with pytest.raises(AttributeError):
        data.lookup("invalid_field", data_dict)

    # 2.
    for key, value in data_dict.items():
        exists, lookup_value = data.lookup(key, data_dict)
        assert exists and (value == lookup_value), "Failed to lookup existing field!"


def test_PyOEDData_setattr():
    # Things to test
    # 1. Setting an invalid field (AttributeError due to slots)

    data_default = PyOEDData()

    # 1. Setting an invalid field
    with pytest.raises(AttributeError):
        data_default.new_field = 10


def test_PyOEDData_data_to_dataclass():
    # Must test the following behaviors
    # 1. data_to_dataclass creates from None
    # 2. data_to_dataclass creates from dict
    # 3. data_to_dataclass creates from dataclass
    # 4. data_to_dataclass raises TypeError for invalid input
    # 5. data_to_dataclass raises TypeError for invalid field

    data = PyOEDData()

    # 1. data_to_dataclass creates from None
    new_data = PyOEDData.data_to_dataclass(None)
    for key, value in data.items():
        assert getattr(new_data, key) == value, "Failed to create from None!"

    # 2. data_to_dataclass creates from dict
    new_data = PyOEDData.data_to_dataclass(data.asdict())
    for key, value in data.items():
        assert getattr(new_data, key) == value, "Failed to create from dict!"

    # 3. data_to_dataclass creates from dataclass
    new_data = PyOEDData.data_to_dataclass(data)
    for key, value in data.items():
        assert getattr(new_data, key) == value, "Failed to create from dataclass!"

    # 4. data_to_dataclass raises TypeError for invalid input
    with pytest.raises(TypeError):
        PyOEDData.data_to_dataclass([])

    # 5. data_to_dataclass raises AttributeError for invalid field
    with pytest.raises(AttributeError):
        PyOEDData.data_to_dataclass({"invalid_field": "value"})


# Tests: (pyoed.configs.remove_unknown_keys)
# -----------------------------------


def test_configs_remove_unknown_keys():
    sub_configs = TestData()
    sub_configs_dict = sub_configs.asdict()
    sub_configs_dict["new_field"] = 10
    pruned_configs = remove_unknown_keys(sub_configs_dict, PyOEDConfigs)

    with pytest.raises(KeyError):
        pruned_configs["mutable_value"]

    with pytest.raises(KeyError):
        pruned_configs["new_field"]
