# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
Functionality and properties we want to be available througy `pyoed.tests`
All functions here are imported into `__main__`.
"""

import numpy as _np
import numpy.testing as _np_testing
from pyoed import utility


## Settings/Configurations:
## ========================
class SETTINGS:
    # Simulation Model
    SIMULATION_MODEL_STATE_SIZE = 5
    SIMULATION_MODEL_PARAMETER_SIZE = 3
    SIMULATION_MODEL_TIME_STEP = 1

    # Observations
    OBSERVATION_SIZE = SIMULATION_MODEL_STATE_SIZE

    # Assimilation
    NUM_OBSERVATION_TIMES = 3
    # OBSERVATION_TIMES  = list(range(0, NUM_OBSERVATION_TIMES))
    OBSERVATION_TIMES = _np.arange(0, NUM_OBSERVATION_TIMES)
    ASSIMILATION_TSPAN = (OBSERVATION_TIMES[0], OBSERVATION_TIMES[-1])

    # Reproductivity
    RANDOM_SEED = 1011

    # Precision & Accuracy
    COMPARISON_PRECISION = 1e-12
    NUM_COMPARISON_DECIMALS = 2
    NUM_COMPARISON_RTOL = 1e-3
    NUM_COMPARISON_ATOL = 1e-2
    CLOSE_COMPARISON_RTOL = 1e-5
    CLOSE_COMPARISON_ATOL = 0

    # Error Models
    CORRELATED_NOISE = True


## Tools for testing and comparisons
## =================================
def assert_allclose(
    actual,
    desired,
    rtol=SETTINGS.CLOSE_COMPARISON_RTOL,
    atol=SETTINGS.CLOSE_COMPARISON_ATOL,
    equal_nan=True,
    err_msg="",
    verbose=True,
    use_np_testing=False,
):
    """
    Wrapper around numpy.testing.assert_allclose; the main difference is that two arrays of zero size are close/equal
    """
    actual = utility.asarray(actual)
    desired = utility.asarray(desired)
    if _np.issubdtype(actual.dtype, bool):
        actual = actual.astype(int)
    if _np.issubdtype(desired.dtype, bool):
        desired = desired.astype(int)

    if actual.size == desired.size == 0:
        return None
    else:

        if actual.size <= 1: actual = actual.flatten()
        if desired.size <= 1: desired = desired.flatten()

        if actual.size == 1 < desired.size:
            actual = _np.repeat(actual, desired.size)
        elif desired.size == 1 < actual.size:
            desired = _np.repeat(desired, actual.size)
        elif desired.size != actual.size:
            raise TypeError(
                f"Cannot compare the two vectors:\n"
                f"{actual=} of {actual.size=}\n"
                f"{desired=} of {desired.size=}"
            )

        actual[_np.abs(actual) < SETTINGS.COMPARISON_PRECISION] = 0
        desired[_np.abs(desired) < SETTINGS.COMPARISON_PRECISION] = 0

        # Check relative differnce for seriously small numbers
        diff = utility.asarray(actual - desired).flatten()
        max_diff = max(abs(diff))
        if max_diff < 1e-5:
            rtol = max(rtol, 10)

        return _np_testing.assert_allclose(
            actual=actual,
            desired=desired,
            rtol=rtol,
            atol=atol,
            equal_nan=equal_nan,
            err_msg=err_msg,
            verbose=verbose,
        )


def assert_allequal(
    actual,
    desired,
    err_msg="",
    verbose=True,
):
    """
    Wrapper around numpy.testing.assert_allclose; the main difference is that two arrays of zero size are close/equal
    """
    actual = utility.asarray(actual)
    desired = utility.asarray(desired)
    if _np.issubdtype(actual.dtype, bool):
        actual = actual.astype(int)
    if _np.issubdtype(desired.dtype, bool):
        desired = desired.astype(int)

    if actual.size == desired.size == 0:
        return
    else:

        if actual.size <= 1: actual = actual.flatten()
        if desired.size <= 1: desired = desired.flatten()

        if actual.size == 1 < desired.size:
            actual = _np.repeat(actual, desired.size)
        elif desired.size == 1 < actual.size:
            desired = _np.repeat(desired, actual.size)
        elif desired.size != actual.size:
            raise TypeError(
                f"Cannot compare the two vectors:\n"
                f"{actual=} of {actual.size=}\n"
                f"{desired=} of {desired.size=}"
            )

        actual[_np.abs(actual) < SETTINGS.COMPARISON_PRECISION] = 0
        desired[_np.abs(desired) < SETTINGS.COMPARISON_PRECISION] = 0
        assert all((actual == desired).flatten()), err_msg


def create_random_number_generator(random_seed=SETTINGS.RANDOM_SEED):
    """Create a random number generator with fixed random seed"""
    return _np.random.default_rng(random_seed)
