# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Test Laplacian error (noise) models and related functionality: pyoed.models.error_models.Laplacian.*
"""
from pyoed.models.error_models import Laplacian

import pytest
pytestmark = [pytest.mark.error_models, pytest.mark.fenics]


def test_Laplacian_DolfinLaplacianErrorModel():
    """Test functionality of the class Laplacian.DolfinLaplacianErrorModel"""
    # TODO
    ...


def test_Laplacian_DolfinBiLaplacianErrorModel():
    """Test functionality of the class Laplacian.DolfinBiLaplacianErrorModel"""
    # TODO
    ...
