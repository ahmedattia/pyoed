# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test Gaussian error (noise) models and related functionality: in the module
`pyoed.models.error_models.Gaussian._Gaussian`
"""

import numpy as np
from scipy import sparse as sp

from pyoed.configs import (
    PyOEDConfigsValidationError,
)
from itertools import product
from pyoed.models.error_models.Gaussian import (
    GaussianErrorModel,
    GaussianErrorModelConfigs,
)
from pyoed import utility

import pytest
pytestmark = pytest.mark.error_models

from pyoed.tests import (
    SETTINGS,
    assert_allequal,
    assert_allclose,
    create_random_number_generator,
)

# Settings/Configurations:
# ========================
# Global settings
_ERROR_MODEL_SIZE = SETTINGS.SIMULATION_MODEL_STATE_SIZE
_NOISE_RANDOM_SEED = SETTINGS.RANDOM_SEED
_NUM_COMPARISON_RTOL = SETTINGS.NUM_COMPARISON_RTOL
_NUM_COMPARISON_ATOL = SETTINGS.NUM_COMPARISON_ATOL
_NUM_COMPARISON_DECIMALS = SETTINGS.NUM_COMPARISON_DECIMALS

# Local settings
_NOISE_SCALE = 3.0
_NUM_MAGNITUDE = 20.0
_STAT_COMPARISON_RTOL = 1e-2
_STAT_COMPARISON_ATOL = 10e-1 * np.power(_NOISE_SCALE, 2)
_MONTE_CARLO_SAMPLE_LARGE_SIZE = 5000
_MONTE_CARLO_SAMPLE_SMALL_SIZE = 50


# Create global random number generator
# =====================================
_RNG = create_random_number_generator()


# Fixtures and general functions:
# ===============================
def create_Gaussian_error_model(
    size=_ERROR_MODEL_SIZE,
    mean_offset=_NUM_MAGNITUDE,
    scl=_NOISE_SCALE,
    sparse=False,
    corr=True,
    random_seed=_NOISE_RANDOM_SEED,
):
    """Create an error model with the passed specifications"""

    # Create random number generator
    rng = create_random_number_generator()

    # Create Gaussian Error model,
    mean = mean_offset + rng.random(size) * scl
    covmat = utility.create_covariance_matrix(
        size=size,
        corr=corr,
        return_factor=False,
        random_seed=random_seed,
    )
    design = np.ones(size, dtype=bool)
    error_model = GaussianErrorModel(
        configs=dict(
            size=size,
            mean=mean,
            variance=covmat,
            sparse=sparse,
            random_seed=random_seed,
        ),
    )
    return mean, covmat, design, error_model


# Tests (Classes & Functions):
# ============================


# Tests: (pyoed.models.error_models.Gaussian.GaussianErrorModel)
# --------------------------------------------------------------
def _test_Gaussian_GaussianErrorModel(
    error_model,
    full_dimension,
    original_design,
    original_mean,
    original_covariance,
    sparse,
    new_design=None,
    new_mean=None,
    new_covariance=None,
    new_design_description=None,
    reset_parameters=True,
):
    """
    Core functionality of the tester of the class Gaussian.GaussianErrorModel
    Given an error model, and the correct mean and covariance,

    :remark:
        This test assumes the passed error model has full dimensionality, that is
        no design applied initially with size same as passed doimension
    """

    # Create random number generator
    rng = create_random_number_generator()

    # Correct/validate the original parameters passed to construct the error model
    original_mean = utility.asarray(original_mean)
    original_covariance = utility.asarray(original_covariance)
    original_design = (
        np.ones(full_dimension)
        if original_design is None
        else utility.asarray(original_design)
    )

    # Retrieve the initial parameters associated with error model
    initial_mean = error_model.mean
    initial_design = error_model.design
    initial_covariance = error_model.unweighted_covariance_matrix()

    ## Match original values passed to those initialized with the error model
    # Design
    assert_allclose(
        initial_design,
        original_design,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    # Mean
    assert_allclose(
        initial_mean,
        original_mean,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    # Variance/Covariance matrix
    assert_allclose(
        initial_covariance,
        original_covariance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    ## Check sparsity of the covariance matrix
    if error_model.sparse:
        assert sp.issparse(error_model.configurations.variance)
    else:
        assert isinstance(error_model.configurations.variance, np.ndarray)

    ## Validate & Update the passed parameters (when new_design is None --> replace with all-ones)
    all_ones = np.ones(full_dimension)
    if new_design is None:
        # new_design is None; error_model converts it to all ones
        error_model.update_design(new_design)
        new_design = all_ones

    elif (
        (new_design.size != error_model.size) or
        np.any(
                utility.asarray(new_design, dtype=bool)
                !=
                new_design
        )
    ):
        # Non-boolean designs -> raise PyOEDConfigsValidationError
        with pytest.raises(Exception) as e_info:
            error_model.update_design(new_design)
        assert e_info.type is PyOEDConfigsValidationError
        return

    else:
        error_model.update_design(new_design)

    # If no new mean is passed use existing
    if new_mean is not None:
        error_model.update_mean(new_mean)
    else:
        new_mean = original_mean.copy()

    # If no new variance/covariance is passed use existing
    if new_covariance is not None:
        unweighted_covariance = utility.asarray(new_covariance)
    else:
        unweighted_covariance = original_covariance.copy()
    error_model.update_covariance_matrix(unweighted_covariance)

    ## Calculate parameters (mean, covariance, cholesky factor, etc.) weighted by the given (new) design
    active_entries = np.where(new_design)[0]
    inactive_entries = np.where(new_design == 0)[0]
    unweighted_variance = np.diag(unweighted_covariance)
    unweighted_covariance_cholesky = np.linalg.cholesky(unweighted_covariance)
    unweighted_precision = np.linalg.inv(unweighted_covariance)

    # The weighted (select active rows/columns) covariance matrix & Cholesky factor
    weighted_covariance = unweighted_covariance[:, active_entries][active_entries, :]
    weighted_variance = np.diag(weighted_covariance)
    weighted_covariance_cholesky = np.linalg.cholesky(weighted_covariance)
    weighted_precision = np.linalg.inv(weighted_covariance)
    weighted_precision_cholesky = np.linalg.cholesky(weighted_precision)
    weighted_covariance_cholesky_inv = np.linalg.inv(weighted_covariance_cholesky)

    # Random vector to use for testing
    test_vec = rng.random(full_dimension)

    # Test model methods (all functionality with updated parameter(s)):
    # =================================================================

    # method `update_design`
    # ~~~~~~~~~~~~~~~~~~~~~~
    # test that it actually updates the design but doesn't touch other parameters (mean, covariance)
    error_model_design = error_model.design

    # Binary-valued design (regardless of the dtype):
    for dtype in [bool, int, float]:
        test_design = rng.choice((0, 1), error_model.size, ).astype(dtype)
        error_model.update_design(test_design)
        assert_allequal(
            error_model.design,
            test_design,
        )

    # Float (non-binary values):
    test_design = rng.random(error_model.size)
    with pytest.raises(Exception) as e_info:
        error_model.update_design(test_design)
    assert e_info.type is PyOEDConfigsValidationError

    # Reset the design
    error_model.update_design(error_model_design)

    # Effect of updating the design with wrong shape (test with raise!)
    error_model_design = error_model.design
    with pytest.raises(Exception) as e_info:
        wrong_sized_design = np.array([d for d in new_design] * 2)
        error_model.update_design(wrong_sized_design)
    assert e_info.type is PyOEDConfigsValidationError
    assert_allequal(
        error_model.design,
        error_model_design,
    )

    # method `update_mean`
    # ~~~~~~~~~~~~~~~~~~~~~~
    # test that it actually updates the design but doesn't touch other parameters (mean, covariance)
    error_model_mean = error_model.configurations.mean.copy()
    test_mean = rng.random(error_model.size)
    error_model.update_mean(test_mean)
    assert_allclose(
        error_model.mean,
        test_mean[error_model.active],
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    error_model.update_mean(error_model_mean)  # reset the mean

    # method `update_covariance_matrix`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    error_model_mean = error_model.mean
    error_model_unweighted_covariance = error_model.unweighted_covariance_matrix()

    test_covariance = (rng.random() + 1e-5) * error_model_unweighted_covariance
    error_model.update_covariance_matrix(test_covariance)
    assert_allclose(
        error_model.mean,
        error_model_mean,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    assert_allclose(
        error_model.unweighted_covariance_matrix(),
        test_covariance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    error_model.update_covariance_matrix(
        error_model_unweighted_covariance
    )  # reset the unweighted covariances

    # method `unweighted_covariance_matrix`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        utility.asarray(error_model.unweighted_covariance_matrix()),
        unweighted_covariance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # method `unweighted_precision_matrix`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        utility.asarray(error_model.unweighted_precision_matrix()),
        unweighted_precision,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )


    # method `covariance_matrix`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        utility.asarray(error_model.covariance_matrix()),
        weighted_covariance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # method `precision_matrix`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        utility.asarray(error_model.precision_matrix()),
        weighted_precision,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # method `covariance_matvec`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
    reference_input = test_vec[active_entries].copy()
    reference_output = np.dot(weighted_covariance, reference_input)
    for in_place in [False, True]:
        # in_place = False --> (return copy)
        # in_place = True  --> (overwrite input)
        input_vec = reference_input.copy()
        retrieved_output = error_model.covariance_matvec(input_vec, in_place=in_place)
        assert_allclose(
            retrieved_output,
            reference_output,
            rtol=_NUM_COMPARISON_RTOL,
            atol=_NUM_COMPARISON_ATOL,
        )
        assert retrieved_output is not test_vec
        if in_place:
            assert retrieved_output is input_vec
        else:
            assert retrieved_output is not input_vec

    # method `covariance_inv_matvec`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    reference_input = test_vec[active_entries].copy()
    reference_output = np.dot(weighted_precision, reference_input)
    for in_place in [False, True]:
        input_vec = reference_input.copy()
        retrieved_output = error_model.covariance_inv_matvec(
            input_vec, in_place=in_place
        )
        assert_allclose(
            retrieved_output,
            reference_output,
            rtol=_NUM_COMPARISON_RTOL,
            atol=_NUM_COMPARISON_ATOL,
        )
        assert retrieved_output is not test_vec
        if in_place:
            assert retrieved_output is input_vec
        else:
            assert retrieved_output is not input_vec

    # method `covariance_diagonal`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        error_model.covariance_diagonal(),
        weighted_variance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # method `variance`
    # ~~~~~~~~~~~~~~~~~
    assert_allclose(
        error_model.variance(),
        weighted_variance,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # method `covariance_trace` (method in ['exact', 'randomized'])
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method = "exact"
    assert_allclose(
        error_model.covariance_trace(method=method),
        np.sum(weighted_variance),
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    method = "randomized"
    noise_sample_size = _MONTE_CARLO_SAMPLE_SMALL_SIZE
    trace_sample = [
        error_model.covariance_trace(method=method) for i in range(noise_sample_size)
    ]
    assert_allclose(
        np.mean(trace_sample),
        np.sum(weighted_variance),
        atol=_STAT_COMPARISON_ATOL * np.max(unweighted_variance),
    )

    # method `covariance_logdet` (method in ['exact', 'randomized'])
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    method = "exact"
    assert_allclose(
        error_model.covariance_logdet(method=method),
        np.log(np.linalg.det(weighted_covariance)),
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )
    # TODO: add once randomized logdet estimator is up
    # method = 'randomized'
    # noise_sample_size = _MONTE_CARLO_SAMPLE_SMALL_SIZE
    # logdet_sample     = [error_model.covariance_logdet(method=method) for i in range(noise_sample_size)]
    # assert_allclose(np.mean(logdet_sample), np.log(np.linalg.det(weighted_covariance)), atol=_STAT_COMPARISON_ATOL)

    # method `covariance_sqrt_matvec`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    reference_input = test_vec[active_entries].copy()
    for lower in [False, True]:
        if lower:
            reference_output = np.dot(weighted_covariance_cholesky, reference_input)
        else:
            reference_output = np.dot(weighted_covariance_cholesky.T, reference_input)

        for in_place in [False, True]:
            input_vec = reference_input.copy()
            retrieved_output = error_model.covariance_sqrt_matvec(
                input_vec, lower=lower, in_place=in_place
            )
            assert_allclose(
                retrieved_output,
                reference_output,
                rtol=_NUM_COMPARISON_RTOL,
                atol=_NUM_COMPARISON_ATOL,
            )
            assert retrieved_output is not test_vec
            if in_place:
                assert retrieved_output is input_vec
            else:
                assert retrieved_output is not input_vec

    # method `covariance_sqrt_inv_matvec`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    reference_input = test_vec[active_entries].copy()
    for lower in [False, True]:
        if lower:
            reference_output = np.dot(weighted_covariance_cholesky_inv, reference_input)
        else:
            reference_output = np.dot(
                weighted_covariance_cholesky_inv.T, reference_input
            )

        for in_place in [False, True]:
            input_vec = reference_input.copy()
            retrieved_output = error_model.covariance_sqrt_inv_matvec(
                input_vec, lower=lower, in_place=in_place
            )
            assert_allclose(
                retrieved_output,
                reference_output,
                rtol=_NUM_COMPARISON_RTOL,
                atol=_NUM_COMPARISON_ATOL,
            )
            assert retrieved_output is not test_vec
            if in_place:
                assert retrieved_output is input_vec
            else:
                assert retrieved_output is not input_vec

    # method `generate_white_noise`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    zero_vector = np.zeros(error_model.active_size)
    noise_sample_size = _MONTE_CARLO_SAMPLE_LARGE_SIZE
    noise_sample = [
        error_model.generate_white_noise() for i in range(noise_sample_size)
    ]
    noise_sample = utility.asarray(noise_sample)

    noise_sample_mean = np.mean(noise_sample, axis=0)
    assert_allclose(noise_sample_mean, zero_vector, atol=_STAT_COMPARISON_ATOL)

    # sqrt(_STAT_COMPARISON_ATOL) becuase we are comparing variances, i.e., at squared scale & tol < 1
    noise_sample_covariance = np.cov(noise_sample, rowvar=False)
    assert_allclose(
        noise_sample_covariance,
        np.eye(error_model.active_size),
        atol=np.sqrt(_STAT_COMPARISON_ATOL),
    )

    # method `generate_noise`
    # ~~~~~~~~~~~~~~~~~~~~~~~
    zero_vector = np.zeros(error_model.active_size)
    noise_sample_size = _MONTE_CARLO_SAMPLE_LARGE_SIZE
    noise_sample = [error_model.generate_noise() for i in range(noise_sample_size)]
    noise_sample = utility.asarray(noise_sample)

    noise_sample_mean = np.mean(noise_sample, axis=0)
    assert_allclose(noise_sample_mean, zero_vector, atol=_STAT_COMPARISON_ATOL)

    # sqrt(_STAT_COMPARISON_ATOL) becuase we are comparing variances, i.e., at squared scale & tol < 1
    noise_sample_covariance = np.cov(noise_sample, rowvar=False)
    scale = (
        1.0 if error_model.active_size == 0 else np.max(np.diag(unweighted_covariance))
    )
    assert_allclose(
        noise_sample_covariance, weighted_covariance, atol=_STAT_COMPARISON_ATOL * scale
    )

    ## PDF and gradient evaluation methods
    # Truth
    test_vector = rng.standard_normal(error_model.active_size)
    precision_matrix = error_model.precision_matrix()
    innov = test_vector - error_model.mean
    scaled_innov = precision_matrix.dot(innov)
    log_scaling_factor = -0.5 * error_model.active_size * np.log(2 * np.pi)
    log_scaling_factor -= 0.5 * error_model.covariance_logdet()

    # methods `pdf`, `log_density`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Log density and pdf value
    unscaled_log_density = -0.5 * np.dot(innov, scaled_innov)
    unscaled_pdf = np.exp(unscaled_log_density)
    scaled_log_density = log_scaling_factor + unscaled_log_density
    scaled_pdf = np.exp(scaled_log_density)

    # Log density (scaled/unscaled)
    assert_allclose(
        unscaled_log_density, error_model.log_density(test_vector, normalize=False)
    )
    assert_allclose(
        unscaled_log_density, error_model.pdf(test_vector, log=True, normalize=False)
    )
    assert_allclose(
        scaled_log_density, error_model.log_density(test_vector, normalize=True)
    )
    assert_allclose(
        scaled_log_density, error_model.pdf(test_vector, log=True, normalize=True)
    )

    # (PDF) density (scaled/unscaled)
    assert_allclose(
        unscaled_pdf, error_model.pdf(test_vector, log=False, normalize=False)
    )
    assert_allclose(scaled_pdf, error_model.pdf(test_vector, log=False, normalize=True))

    # methods `pdf_gradient`, `log_density_gradient`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Log density & PDF gradients
    unscaled_log_density_grad = - scaled_innov
    unscaled_pdf_grad = unscaled_pdf * unscaled_log_density_grad

    scaled_log_density_grad = unscaled_log_density_grad  # Gradient of the scaling factor is zero
    scaled_pdf_grad = np.exp(log_scaling_factor) * unscaled_pdf_grad

    # gradient of log density (scaled/unscaled)
    assert_allclose(
        unscaled_log_density_grad,
        error_model.log_density_gradient(test_vector, normalize=False),
    )
    assert_allclose(
        unscaled_log_density_grad,
        error_model.pdf_gradient(test_vector, log=True, normalize=False),
    )
    assert_allclose(
        scaled_log_density_grad,
        error_model.log_density_gradient(test_vector, normalize=True),
    )
    assert_allclose(
        scaled_log_density_grad,
        error_model.pdf_gradient(test_vector, log=True, normalize=True),
    )

    # gradient of PDF (scaled/unscaled)
    assert_allclose(
        unscaled_pdf_grad,
        error_model.pdf_gradient(test_vector, log=False, normalize=False),
        rtol=_STAT_COMPARISON_RTOL,
    )
    assert_allclose(
        scaled_pdf_grad,
        error_model.pdf_gradient(test_vector, log=False, normalize=True),
        rtol=_STAT_COMPARISON_RTOL,
    )

    # method `sample`
    # ~~~~~~~~~~~~~~~
    noise_sample_size = _MONTE_CARLO_SAMPLE_LARGE_SIZE
    noise_sample = [error_model.sample() for i in range(noise_sample_size)]
    noise_sample = utility.asarray(noise_sample)

    noise_sample_mean = np.mean(noise_sample, axis=0)
    scale = (
        1.0
        if error_model.active_size == 0
        else np.linalg.norm(error_model.mean, ord=np.inf)
    )
    assert_allclose(
        noise_sample_mean, error_model.mean, atol=_STAT_COMPARISON_ATOL * scale
    )

    # sqrt(_STAT_COMPARISON_ATOL) becuase we are comparing variances, i.e., at squared scale & tol < 1
    noise_sample_covariance = np.cov(noise_sample, rowvar=False)
    scale = (
        1.0 if error_model.active_size == 0 else np.max(np.diag(unweighted_covariance))
    )
    assert_allclose(
        noise_sample_covariance, weighted_covariance, atol=_STAT_COMPARISON_ATOL * scale
    )

    # method `add_noise` (in_place in [False, True]); in both cases test statistics and functionlity of in_place
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    noisy_sample_size = _MONTE_CARLO_SAMPLE_LARGE_SIZE
    reference_input = test_vec[active_entries].copy()
    input_vec = reference_input.copy()

    # adding noisy to a copy (in_place=False) does not alter the input_vec
    in_place = False
    noisy_sample = [
        error_model.add_noise(input_vec, in_place=in_place)
        for i in range(noisy_sample_size)
    ]
    noisy_sample = utility.asarray(noisy_sample)

    noisy_sample_mean = np.mean(noisy_sample, axis=0)
    scale = (
        1.0
        if error_model.active_size == 0
        else np.linalg.norm(error_model.mean, ord=np.inf)
    )
    assert_allclose(
        noisy_sample_mean, reference_input, atol=_STAT_COMPARISON_ATOL * scale
    )

    # sqrt(_STAT_COMPARISON_ATOL) becuase we are comparing variances, i.e., at squared scale & tol < 1
    noisy_sample_covariance = np.cov(noisy_sample, rowvar=False)
    scale = (
        1.0 if error_model.active_size == 0 else np.max(np.diag(unweighted_covariance))
    )
    assert_allclose(
        noisy_sample_covariance, weighted_covariance, atol=_STAT_COMPARISON_ATOL * scale
    )

    # In-place perturbing the input vector (in_place=False) changes the values by adding noise in place.
    in_place = True
    noisy_sample_size = _MONTE_CARLO_SAMPLE_LARGE_SIZE
    input_vec = reference_input.copy()
    noisy_sample = [
        error_model.add_noise(input_vec.copy(), in_place=in_place)
        for i in range(noisy_sample_size)
    ]
    noisy_sample = utility.asarray(noisy_sample)

    noisy_sample_mean = np.mean(noisy_sample, axis=0)
    scale = (
        1.0
        if error_model.active_size == 0
        else np.linalg.norm(error_model.mean, ord=np.inf)
    )
    assert_allclose(
        noisy_sample_mean, reference_input, atol=_STAT_COMPARISON_ATOL * scale
    )

    # sqrt(_STAT_COMPARISON_ATOL) becuase we are comparing variances, i.e., at squared scale & tol < 1
    noisy_sample_covariance = np.cov(noisy_sample, rowvar=False)
    scale = (
        1.0 if error_model.active_size == 0 else np.max(np.diag(unweighted_covariance))
    )
    assert_allclose(
        noisy_sample_covariance, weighted_covariance, atol=_STAT_COMPARISON_ATOL * scale
    )

    # test functionality of the `in_place` flag
    in_place = True
    input_vec = reference_input.copy()
    output_vec = error_model.add_noise(input_vec, in_place=in_place)
    assert output_vec is input_vec

    # Test model properties (both getters & setters):
    # ===============================================

    # property `sparse`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if sparse is not None:
        assert sparse is bool(sparse) is error_model.sparse is bool(error_model.sparse) is error_model.configurations.sparse
    else:
        with pytest.raises(Exception) as e_info:
            is_sparse = error_model.sparse
        assert (
            e_info.type == AttributeError
        ), "sparse property is not supposed to be implemented as `sparse` is None"

    # property `mean`
    # ~~~~~~~~~~~~~~~
    assert_allclose(
        error_model.mean,
        new_mean[active_entries],
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # property `design`
    # ~~~~~~~~~~~~~~~~~
    assert_allclose(
        error_model.design,
        new_design,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # property `extended_design`
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allclose(
        error_model.design,
        new_design,
        rtol=_NUM_COMPARISON_RTOL,
        atol=_NUM_COMPARISON_ATOL,
    )

    # property `size`
    # ~~~~~~~~~~~~~~~
    updated_size = error_model.size
    assert utility.isnumber(updated_size)
    assert updated_size == full_dimension

    # property `active`
    # ~~~~~~~~~~~~~~~~~
    updated_active = error_model.active
    assert all(np.where(new_design)[0] == updated_active)
    assert updated_active.size == error_model.active_size
    assert updated_active.dtype == "int"

    # property `active_size`
    # ~~~~~~~~~~~~~~~~~~~~~~
    updated_active_size = error_model.active_size
    assert utility.isnumber(updated_active_size)
    assert updated_active_size == active_entries.size
    assert updated_active_size == updated_active.size
    assert updated_active_size == np.count_nonzero(new_design)

    # property `random_state`
    # ~~~~~~~~~~~~~~~~~~~~~~~
    pass

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Done with testing; reset design, mean, and covariance
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++
    if reset_parameters:
        error_model.update_design(original_design)
        error_model.update_mean(original_mean)
        error_model.update_covariance_matrix(original_covariance)

    return


@pytest.mark.parametrize(
    "design_and_descp, new_mean, corr, sparse",
    product(
        zip(
            [
                None,
                np.zeros(_ERROR_MODEL_SIZE, dtype=int),
                np.ones(_ERROR_MODEL_SIZE, dtype=int),
                np.ones(_ERROR_MODEL_SIZE, dtype=float),
                _RNG.choice((0, 1), _ERROR_MODEL_SIZE),
                _RNG.choice((0, 1), _ERROR_MODEL_SIZE).astype(bool),
                np.insert(_RNG.random(_ERROR_MODEL_SIZE - 1), 0, 0),
            ],
            [
                "No design (all active)",
                "all inactive/zeros (int)",
                "all active/ones (int)",
                "all active/ones (float)",
                "Integer (0/1) design",
                "Boolean  True/False design",
                "Real-valued design",
            ],
        ),  # designs and descriptions: tuple
        [
            None,
            _RNG.standard_normal(_ERROR_MODEL_SIZE) * _NOISE_SCALE + _NUM_MAGNITUDE,
        ],  # mean_pool
        [False, True],  # corr
        [False, True],  # sparse
    ),
)
def test_Gaussian_GaussianErrorModel(
    design_and_descp,
    new_mean,
    corr,
    sparse,
    size=_ERROR_MODEL_SIZE,
    mean_offset=_NUM_MAGNITUDE,
    scl=_NOISE_SCALE,
    random_seed=_NOISE_RANDOM_SEED,
):
    """
    Test functionality of the class Gaussian.GaussianErrorModel;
    This is a wrapper around the function `_test_Gaussian_GaussianErrorModel()`

    The desired behaviour is that:

        - when the design is 0/1, the corresponding rows/columns from the covariance matrix are eliminated.
        - when the design is None, it is replaced with all ones.
        - when the design is Float, the variances increase as the value of the
          design variable decreases and the covariances decreas. This is
          obtained by pointwise multiplication of the covariance matrix with
          entries conversly proportional to design on the diagonal, and directly
          proportional to the design on the offdiagonal. (see the correlated OED
          paper by Attia et. al.)

    """
    #  Construct a pool of designs to test functionality of the Gaussian Error
    #  model without updating the design, then update design as bool, int, and
    #  float

    covariance_pool = [
        None,
        utility.create_covariance_matrix(
            size=size, corr=corr, random_seed=random_seed, return_factor=False
        ),
    ]

    gmodel = create_Gaussian_error_model(
        size=size,
        mean_offset=mean_offset,
        scl=scl,
        sparse=sparse,
        corr=corr,
        random_seed=random_seed,
    )
    original_mean, original_covariance, original_design, error_model = gmodel

    for new_covariance in covariance_pool:
        _test_Gaussian_GaussianErrorModel(
            error_model=error_model,
            full_dimension=size,
            sparse=sparse,
            original_design=original_design.copy(),
            original_mean=original_mean.copy(),
            original_covariance=original_covariance.copy(),
            new_design=design_and_descp[0],
            new_design_description=design_and_descp[1],
            new_mean=new_mean,
            new_covariance=new_covariance,
            reset_parameters=True,
        )


# Tests: (pyoed.models.error_models.Gaussian.kl_divergence)
# ---------------------------------------------------------
def test_Gaussian_kl_divergence():
    """Test functionality of the function Gaussian.kl_divergence"""
    # TODO
    ...

