# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test Dolfin-based observation operators and related functionality: pyoed.models.observation_operators.fenics_observation_operators.*
"""
try:
    import dolfin as dl
except ImportError:
    dl = None
import numpy as np
from pyoed.models.observation_operators.fenics_observation_operators import (
    create_DolfinPointWise_observation_operator,
)
from pyoed.models.simulation_models.fenics_models import create_Subsurf2D_model
from pyoed.models.simulation_models.black_box import TimeIndependentBlackBox
from pyoed.utility import enumerate_binary

import pytest

pytestmark = [pytest.mark.fenics, pytest.mark.observation_operators]


def test_fenics_observation_operators_DolfinPointWiseObservations():
    """
    Test functionality of the class .fenics_observation_operators.DolfinPointWiseObservations
    for time independent models
    """
    if dl is None:
        pytest.fail("dolfin not installed")
    N = 8
    model = create_Subsurf2D_model(N, N)
    m = model.create_initial_condition(return_np=False)
    u = model(m, return_np=False)

    pts = np.linspace(0, 1, 3)
    targets = np.array([[x, y] for x in pts for y in pts])
    Q = create_DolfinPointWise_observation_operator(
        model,
        Vh=model.state_dof,
        targets=targets,
    )
    obs = Q(u)

    u_fn = dl.Function(model.state_dof)
    u_fn.vector().zero()
    u_fn.vector().axpy(1.0, u)

    assert Q.shape == (obs.size, u[:].size), "Q.shape != (N, u[:].size)"
    assert np.allclose(
        obs, [u_fn(x) for x in Q.targets]
    ), "Q(u) != u(x) for x in Q.targets"

    designs = enumerate_binary(Q.shape[0])
    for d in designs:
        bd = d.astype("bool")
        Q.design = bd
        assert np.allclose(Q(u), obs[bd]), "Q(u) != obs[d] for d in designs"


def test_fenics_observation_operators_BlackBox():
    """
    Test functionality of the class .fenics_observation_operators.DolfinPointWiseObservations
    for black box models.
    """
    if dl is None:
        pytest.fail("dolfin not installed")
    N = 8
    fenics_model = create_Subsurf2D_model(N, N)
    Vh_state = fenics_model.state_dof
    m = fenics_model.create_initial_condition(return_np=False)
    u = fenics_model(m, return_np=False)

    model = TimeIndependentBlackBox(
        configs={
            "solve_forward": fenics_model,
            "N_p": len(m),
            "N_x": len(u),
        }
    )

    pts = np.linspace(0, 1, 3)
    targets = np.array([[x, y] for x in pts for y in pts])
    Q = create_DolfinPointWise_observation_operator(
        model,
        Vh=Vh_state,
        targets=targets,
    )
    obs = Q(u[:])

    u_fn = dl.Function(Vh_state)
    u_fn.vector().zero()
    u_fn.vector().axpy(1.0, u)

    assert Q.shape == (obs.size, u[:].size), "Q.shape != (N, u[:].size)"
    assert np.allclose(
        obs, [u_fn(x) for x in Q.targets]
    ), "Q(u) != u(x) for x in Q.targets"

    designs = enumerate_binary(Q.shape[0])
    for d in designs:
        bd = d.astype("bool")
        Q.design = bd
        assert np.allclose(Q(u[:]), obs[bd]), "Q(u) != obs[d] for d in designs"
