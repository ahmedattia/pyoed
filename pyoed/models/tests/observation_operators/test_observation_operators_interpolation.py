# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test Interpolation observation operator and related functionality: pyoed.models.observation_operators.interpolation.*
"""
import numpy as np
from itertools import product

from pyoed.models.observation_operators import interpolation
from pyoed.models.simulation_models import toy_linear
from pyoed import utility
from pyoed.configs import PyOEDConfigsValidationError
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)

import pytest

pytestmark = pytest.mark.observation_operators

# Settings/Configurations:
# ========================
_NUM_COMPARISON_RTOL = SETTINGS.NUM_COMPARISON_RTOL
_NUM_COMPARISON_ATOL = SETTINGS.NUM_COMPARISON_ATOL
_NOISE_RANDOM_SEED = SETTINGS.RANDOM_SEED

# Create a random number generator
# ================================
_RNG = create_random_number_generator()


# Tests (Classes & Functions):
# ============================


# Tests: (pyoed.models.observation_operators.interpolation.ArrayOperator)
# ---------------------------------------------------------------------------
def test_interpolation_ArrayOperator():
    """Test functionality of the class interpolation.ArrayOperator"""

    # TODO: After creating models with multiple prognostic variables, update this
    for num_prog_vars in [1]:
        # Create a model (to retrieve model grid)
        if num_prog_vars > 1:
            print("TODO: Test ArrayOperator with multiple prognostic variables")
            raise NotImplementedError("TODO")

        else:
            # create a simple linear model (with 1 prognostic variable)
            model = toy_linear.create_time_dependent_linear_model()

            model_state_size = model.state_vector().size
            num_model_gridpoints = int(model_state_size // num_prog_vars)
            num_obs_gridpoints = max(2, num_model_gridpoints // 2)

            # Create a random observation array
            obs_array = _RNG.random((num_obs_gridpoints, num_model_gridpoints))

            # Create observation operator and test its type
            obs_oper = interpolation.ArrayOperator(
                configs=dict(
                    model=model,
                    obs_array=obs_array,
                )
            )
            assert isinstance(obs_oper, interpolation.ArrayOperator)

        # Test the original design
        # ~~~~~~~~~~~~~~~~~~~~~~~~
        assert_allequal(obs_oper.design, np.ones(num_obs_gridpoints))

        # Test methods for multiple choices of the design
        # ===============================================
        designs_pool = [
            np.ones(num_obs_gridpoints),
            _RNG.choice([0, 1], size=num_obs_gridpoints),
            np.round(_RNG.random(num_obs_gridpoints), 1),
        ]
        for design in designs_pool:
            # test property `design`
            # ~~~~~~~~~~~~~~~~~~~~~~
            # Update design
            obs_oper.design = design
            assert_allequal(obs_oper.design, design.astype(bool))

            # Observation vector size
            observation_size = num_prog_vars * np.count_nonzero(design)

            # Test methods:
            # -------------

            # test method `observation_vector`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for init_val in [0, _RNG.random()]:
                obs = obs_oper.observation_vector(init_val=init_val)
                assert isinstance(obs, np.ndarray)
                assert obs.size == observation_size
                assert_allclose(obs, obs.astype(float))
                assert_allclose(obs, init_val)

            # test method `is_observation_vector`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            obs = _RNG.random(observation_size)
            assert obs_oper.is_observation_vector(obs) is True
            assert (
                obs_oper.is_observation_vector(_RNG.random(observation_size + 1))
                is False
            )
            assert obs_oper.is_observation_vector(_RNG.random()) is False

            # test method `get_observation_grid`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            assert_allequal(
                np.arange(num_obs_gridpoints), obs_oper.get_observation_grid()
            )

            # test method `apply`
            # ~~~~~~~~~~~~~~~~~~~
            state = _RNG.random(model_state_size)
            y = obs_oper.apply(state)
            assert y.size == observation_size
            assert_allclose(y, obs_array.dot(state)[obs_oper.extended_design])

            # test method `__call__`
            # ~~~~~~~~~~~~~~~~~~~~~~
            state = _RNG.random(model_state_size)
            y = obs_oper(state)
            assert y.size == observation_size
            assert_allclose(y, obs_array.dot(state)[obs_oper.extended_design])

            # test method `Jacobian_T_matvec`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            obs = _RNG.random(observation_size)
            state = np.zeros(model_state_size)
            state = obs_array[obs_oper.extended_design, :].T.dot(obs)
            out_state = obs_oper.Jacobian_T_matvec(obs)
            assert_allclose(state, out_state)

            # Test properties
            # ---------------

            # test property `model`
            # ~~~~~~~~~~~~~~~~~~~~~
            assert obs_oper.model is model

            # test property `shape`
            # ~~~~~~~~~~~~~~~~~~~~~
            assert obs_oper.shape == (observation_size, model_state_size)

            # test property `extended_design`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            full_design = obs_oper.extended_design
            assert isinstance(full_design, np.ndarray)
            assert np.count_nonzero(full_design) == observation_size
            assert_allequal(full_design, full_design.astype(bool))


# Tests: (pyoed.models.observation_operators.interpolation.SelectionOperator)
# ---------------------------------------------------------------------------
def test_interpolation_SelectionOperator():
    """Test functionality of the class interpolation.SelectionOperator"""
    # TODO
    ...


# Tests: (pyoed.models.observation_operators.interpolation.CartesianInterpolator)
# -------------------------------------------------------------------------------
def _test_interpolation_CartesianInterpolator(
    ndim, num_model_prog_vars, num_obs_prog_vars
):
    """
    Test functionality of the class interpolation.CartesianInterpolator
    for `ndim` coordinate system. `ndim` must be 1, 2, or 3.
    The number of prognostic variables in the model space `num_model_prog_vars` and
    `num_obs_prog_vars` define the recurrance of the model and observation grids respectively
    """
    if not (
        num_model_prog_vars >= 1
        and num_obs_prog_vars >= 1
        and 1 <= ndim <= 3
        and int(ndim) == ndim
        and int(num_model_prog_vars) == num_model_prog_vars
        and int(num_obs_prog_vars) == num_obs_prog_vars
    ):
        raise ValueError("The passed values are not acceptable!")

    # Create model and observation grid (with proper repetition based on num_prog_vars)
    num_model_gridpoints = 10
    num_obs_gridpoints = 5
    lb = 0
    ub = 1
    # <<<<<<< WIP >>>>>>>
    if ndim == 1:
        model_grid = np.linspace(lb, ub, num_model_gridpoints)
        observation_grid = np.linspace(lb, ub, num_model_gridpoints)

    elif ndim == 2:
        xm = np.linspace(lb, ub, num_model_gridpoints)
        ym = np.linspace(lb, ub, num_model_gridpoints)
        xxm, yym = np.meshgrid(xm, ym)
        model_grid = np.hstack([xxm.reshape(-1, 1), yym.reshape(-1, 1)])

        xo = np.linspace(lb, ub, num_obs_gridpoints)
        yo = np.linspace(lb, ub, num_obs_gridpoints)
        xxo, yyo = np.meshgrid(xo, yo)
        observation_grid = np.hstack([xxo.reshape(-1, 1), yyo.reshape(-1, 1)])

    elif ndim == 3:
        xm = np.linspace(lb, ub, num_model_gridpoints // 2)
        ym = np.linspace(lb, ub, num_model_gridpoints // 2)
        zm = np.linspace(lb, ub, num_model_gridpoints // 2)
        xxm, yym, zzm = np.meshgrid(xm, ym, zm)
        model_grid = np.hstack(
            [xxm.reshape(-1, 1), yym.reshape(-1, 1), zzm.reshape(-1, 1)]
        )

        xo = np.linspace(lb, ub, num_obs_gridpoints // 2)
        yo = np.linspace(lb, ub, num_obs_gridpoints // 2)
        zo = np.linspace(lb, ub, num_obs_gridpoints // 2)
        xxo, yyo, zzo = np.meshgrid(xo, yo, zo)
        observation_grid = np.hstack(
            [xxo.reshape(-1, 1), yyo.reshape(-1, 1), zzo.reshape(-1, 1)]
        )

    else:
        raise ValueError(
            f"This test is designed for only 1, 2, or 3 dimensional cartesian coordiantes"
        )

    # make sure the grid is properly defined with points as rows
    model_grid, _ = utility.validate_Cartesian_grid(model_grid, points_as_rows=True)
    observation_grid, _ = utility.validate_Cartesian_grid(
        observation_grid, points_as_rows=True
    )

    # Number of points (without repeation)
    num_model_gridpoints = np.size(model_grid, 0)
    num_obs_gridpoints = np.size(observation_grid, 0)

    # repeat based on number of prognostic variables
    model_grid = np.tile(model_grid, (num_model_prog_vars, 1))
    observation_grid = np.tile(observation_grid, (num_obs_prog_vars, 1))

    if num_model_prog_vars != num_obs_prog_vars:
        # Try to create the operator, catch the raised error
        with pytest.raises(Exception) as e_info:
            obs_oper = interpolation.CartesianInterpolator(
                configs=dict(
                    model_grid=model_grid,
                    observation_grid=observation_grid,
                    method="linear",
                )
            )
        assert e_info.type is PyOEDConfigsValidationError
        return
    else:
        # Creat the interpolation observation operator
        obs_oper = interpolation.CartesianInterpolator(
            configs=dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
                method="linear",
            )
        )
        assert isinstance(obs_oper, interpolation.CartesianInterpolator)

    # model state size and observation vector size
    model_state_size = np.size(model_grid, 0)

    def value_func(grid, num_prog_vars):
        """
        Function returns values of for the model/observation vector given the number of prognostic variables
        """
        ndim = np.size(grid, 1)
        if ndim == 1:
            values = np.sin(grid).flatten()

        elif ndim == 2:
            values = np.sin(grid[:, 0]) * np.cos(grid[:, 1])

        elif ndim == 3:
            values = np.sin(grid[:, 0]) * np.cos(grid[:, 1]) * np.power(grid[:, 2], 2)

        else:
            raise ValueError

        num_points = int(np.size(grid, 0) // num_prog_vars)
        for i in range(num_prog_vars):
            values[i * num_points : (i + 1) * num_points] *= i + 1

        return values

    # Test the original design
    # ~~~~~~~~~~~~~~~~~~~~~~~~
    assert_allequal(obs_oper.design, np.ones(num_obs_gridpoints))

    # Test methods for multiple choices of the design
    # ===============================================
    designs_pool = [
        np.ones(num_obs_gridpoints),
        _RNG.choice([0, 1], size=num_obs_gridpoints),
        np.round(_RNG.random(num_obs_gridpoints), 1),
    ]
    for design in designs_pool:
        # test property `design`
        # ~~~~~~~~~~~~~~~~~~~~~~
        # Update design
        obs_oper.design = design
        assert_allequal(obs_oper.design, design.astype(bool))

        # Design with repeated pattern
        extended_design = np.array(
            [bool(d) for d in design] * num_obs_prog_vars, dtype=bool
        )

        # observation vector size
        observation_size = np.count_nonzero(design) * num_obs_prog_vars

        # Test methods:
        # -------------

        # test method `observation_vector`
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        for init_val in [0, _RNG.random()]:
            obs = obs_oper.observation_vector(init_val=init_val)
            assert isinstance(obs, np.ndarray)
            assert obs.size == observation_size
            assert_allclose(
                obs,
                obs.astype(float),
            )
            assert all(obs == init_val)

        # test method `is_observation_vector`
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        obs = _RNG.random(observation_size)
        assert obs_oper.is_observation_vector(obs) is True
        assert (
            obs_oper.is_observation_vector(_RNG.random(observation_size + 1)) is False
        )
        assert obs_oper.is_observation_vector(_RNG.random()) is False

        # test method `get_observation_grid`
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        assert_allclose(observation_grid, obs_oper.get_observation_grid())

        # test method `apply`
        # test method `__call__`
        # ~~~~~~~~~~~~~~~~~~~
        ref_state = value_func(grid=model_grid, num_prog_vars=num_model_prog_vars)
        test_state = ref_state.copy()
        ref_obs = value_func(grid=observation_grid, num_prog_vars=num_obs_prog_vars)[
            extended_design
        ]
        y1 = obs_oper.apply(test_state)
        y2 = obs_oper(test_state)
        assert y1.size == y2.size == observation_size
        assert_allclose(y1, y2)
        assert_allclose(
            y1,
            ref_obs,
            rtol=_NUM_COMPARISON_RTOL,
            atol=_NUM_COMPARISON_ATOL,
        )
        assert_allclose(ref_state, test_state)

        # test method `Jacobian_T_matvec`
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        obs_oper_array = np.empty((observation_size, model_state_size))
        e_vec = np.empty(model_state_size)
        for i in range(e_vec.size):
            e_vec[:] = 0.0
            e_vec[i] = 1.0
            obs_oper_array[:, i] = obs_oper(e_vec)

        ref_obs = value_func(grid=observation_grid, num_prog_vars=num_obs_prog_vars)[
            extended_design
        ]
        ref_state = obs_oper_array.T.dot(ref_obs)
        test_obs = ref_obs.copy()
        out_state = obs_oper.Jacobian_T_matvec(test_obs)
        assert_allclose(out_state, ref_state)
        assert_allclose(test_obs, ref_obs)

        # Test properties
        # ---------------

        # test property `model`
        # ~~~~~~~~~~~~~~~~~~~~~
        with pytest.raises(Exception) as e_info:
            obs_oper.model
        assert e_info.type is AttributeError

        # test property `shape`
        # ~~~~~~~~~~~~~~~~~~~~~
        assert obs_oper.shape == (observation_size, model_state_size)

        # test property `extended_design`
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        full_design = obs_oper.extended_design
        assert isinstance(full_design, np.ndarray)
        assert np.count_nonzero(full_design) == observation_size
        assert_allequal(full_design, full_design.astype(bool))
        assert_allequal(full_design, extended_design)
        for i in range(num_obs_prog_vars):
            assert_allequal(
                full_design[:num_obs_gridpoints],
                full_design[i * num_obs_gridpoints : (i + 1) * num_obs_gridpoints],
            )


@pytest.mark.parametrize("ndim, num_prog_vars", product([1, 2, 3], [1, 2]))
def test_interpolation_CartesianInterpolator(ndim, num_prog_vars):
    """
    Wrapper that initiates `_test_interpolation_CartesianInterpolator` with
    multiple choices of the arguments.
    """
    print(f"\nndim: {ndim}; num_prog_vars: {num_prog_vars}")
    _test_interpolation_CartesianInterpolator(
        ndim=ndim, num_model_prog_vars=num_prog_vars, num_obs_prog_vars=num_prog_vars
    )


@pytest.mark.parametrize("ndim, num_prog_vars", product([1, 2, 3], [1, 2]))
def test_interpolation_CartesianInterpolator_mismatchvars(ndim, num_prog_vars):
    """
    Wrapper that initiates `_test_interpolation_CartesianInterpolator` with
    multiple choices of the arguments, but with more observation variables than
    model variables. This is to test the error handling of the class.
    """
    print(f"\nndim: {ndim}; num_prog_vars: {num_prog_vars}")
    _test_interpolation_CartesianInterpolator(
        ndim=ndim,
        num_model_prog_vars=num_prog_vars,
        num_obs_prog_vars=num_prog_vars + 1,
    )


if __name__ == "__main__":
    test_interpolation_CartesianInterpolator()
