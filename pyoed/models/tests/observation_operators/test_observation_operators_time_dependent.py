# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test time-dependent observation operators classes.
This module tests all functionality under `pyoed.models.observation_operators.time_dependent`
"""
import pytest

pytestmark = pytest.mark.observation_operators

from pyoed.models.observation_operators import time_dependent

import numpy as np
import numpy.testing as np_testing

from pyoed.models.observation_operators import identity
from pyoed.models.simulation_models import toy_linear
from pyoed import utility

# Settings/Configurations:
# ========================
_NOISE_RANDOM_SEED = 1011

# Create a random number generator
# ================================
_RNG = np.random.default_rng(_NOISE_RANDOM_SEED)


# TODO: Update to new fixtures
def test_time_dependent_TimeDependentObservationOperator():
    """
    Test functionality of the class
    :py:class:`pyoed.models.observation_operators.time_dependent.TimeDependentObservationOperator`
    """

    # Create a base observation operator
    model = toy_linear.create_time_independent_linear_model()
    base_observation_operator = identity.Identity(configs={"model": model})

    # Create a time-dependent observation operator,
    observation_times = [1, 2]
    obs_oper = time_dependent.TimeDependentObservationOperator(
        configs={
            "base_observation_operator": base_observation_operator,
            "observation_times": observation_times,
        }
    )
    assert isinstance(obs_oper, time_dependent.TimeDependentObservationOperator)

    # Test methods and attributes
    # ===========================

    # Registerd base observation operator
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    assert obs_oper.base_observation_operator is base_observation_operator

    # Registerd observation times
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    np_testing.assert_equal(obs_oper.observation_times, observation_times)

    # Regisgtered observation operators
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for t in observation_times:
        assert isinstance(
            obs_oper.retrieve_observation_operator(t),
            base_observation_operator.__class__,
        )

    # Register a new observation time/operator
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    t = 3
    obs_oper.register_observation_operator(
        t=t,
    )
    assert isinstance(
        obs_oper.retrieve_observation_operator(t), base_observation_operator.__class__
    )

    # Register again with overwrite
    obs_oper.register_observation_operator(t=t, overwrite=True)
    assert isinstance(
        obs_oper.retrieve_observation_operator(t), base_observation_operator.__class__
    )

    # Try again without overwrite
    with pytest.raises(Exception) as e_info:
        obs_oper.register_observation_operator(
            t=t,
            overwrite=False,
        )
    assert e_info.type is ValueError

    # Try with invalid operator
    with pytest.raises(Exception) as e_info:
        obs_oper.register_observation_operator(
            t=t,
            observation_operator=10,
            overwrite=True,
        )
    assert e_info.type is TypeError

    # find_observation_time
    # ~~~~~~~~~~~~~~~~~~~~~
    for t in obs_oper.observation_times:
        assert t == obs_oper.find_observation_time(t)

    # Sanity check on each registerd operator
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    init_val = 3.0
    for t in obs_oper.observation_times:
        t_oper = obs_oper.retrieve_observation_operator(t)
        test_state = model.state_vector(init_val=init_val)
        test_obs = t_oper.observation_vector(init_val=init_val)

        # observation_vector()
        # ~~~~~~~~~~~~~~~~~~~~
        np_testing.assert_equal(
            test_obs, obs_oper.observation_vector(_t=t, init_val=init_val)
        )

        # is_observation_vector()
        # ~~~~~~~~~~~~~~~~~~~~~~~
        assert obs_oper.is_observation_vector(t=t, observation=test_obs) is True

        # get_observation_grid()
        # ~~~~~~~~~~~~~~~~~~~~~~
        np_testing.assert_equal(
            t_oper.get_observation_grid(), obs_oper.get_observation_grid(t=t)
        )

        # apply(), __call__()
        # ~~~~~~~~~~~~~~~~~~~
        np_testing.assert_equal(
            t_oper(state=test_state), obs_oper(t=t, state=test_state)
        )
        np_testing.assert_equal(
            t_oper.apply(state=test_state), obs_oper.apply(t=t, state=test_state)
        )

        # Jacobian_T_matvec()
        # ~~~~~~~~~~~~~~~~~~~
        np_testing.assert_equal(
            t_oper.Jacobian_T_matvec(observation=test_obs),
            obs_oper.Jacobian_T_matvec(t=t, observation=test_obs),
        )

    ## Test design retrieval and update
    ## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Retrieve the design
    design = obs_oper.design
    assert isinstance(design, dict), f"Invalid design {type(design)=}"
    assert set(list(design.keys())) == set(obs_oper.observation_times), "Retrieved design keys are different from registered observation times"
    for t in obs_oper.observation_times:
        t_oper = obs_oper.retrieve_observation_operator(t)
        np_testing.assert_equal(t_oper.design, design[t])

    # Update the design
    for t, d in design.items():
        design[t][0] = abs(design[t][0]-1)
    obs_oper.design = design
    assert obs_oper.design is not design, "The updated design didn't properly copy passed design"
    for t in obs_oper.observation_times:
        t_oper = obs_oper.retrieve_observation_operator(t)
        np_testing.assert_equal(t_oper.design, design[t])
        assert t_oper.design is not design[t], "didn't copy desings as expected when design is updated"
    obs_oper.design = design[observation_times[0]]
    for t in obs_oper.observation_times:
        t_oper = obs_oper.retrieve_observation_operator(t)
        np_testing.assert_equal(t_oper.design, design[observation_times[0]])
        assert t_oper.design is not design[observation_times[0]], "didn't copy desings as expected when design is updated"

    # Test extended_design retrieval
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Retrieve the extended design
    design = obs_oper.extended_design
    assert isinstance(design, dict), f"Invalid extended_design {type(design)=}"
    assert set(list(design.keys())) == set(obs_oper.observation_times), "Retrieved design keys are different from registered observation times"
    for t in obs_oper.observation_times:
        t_oper = obs_oper.retrieve_observation_operator(t)
        np_testing.assert_equal(t_oper.extended_design, design[t])


    # Test unsupported attributes/properties
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    with pytest.raises(Exception) as e_info:
        obs_oper()
    assert e_info.type is TypeError  # time must be passed

    with pytest.raises(Exception) as e_info:
        obs_oper.shape
    assert e_info.type is NotImplementedError

