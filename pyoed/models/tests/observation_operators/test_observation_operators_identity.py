# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
Test Identity observation operator and related functionality: pyoed.models.observation_operators.identity.*
"""

import numpy as np

from pyoed.models.observation_operators import identity
from pyoed.models.simulation_models import toy_linear
from pyoed import utility
from pyoed.tests import (
    SETTINGS,
    assert_allequal,
    assert_allclose,
    create_random_number_generator,
)

import pytest
pytestmark = pytest.mark.observation_operators

# Settings/Configurations:
# ========================
_NOISE_RANDOM_SEED = SETTINGS.RANDOM_SEED

# Create a random number generator
# ================================
_RNG = create_random_number_generator()


def test_identity_Identity():
    """Test functionality of the class identity.Identity"""

    # TODO: After creating models with multiple prognostic variables, update this
    for num_prog_vars in [1]:
        # Create a model (to retrieve model grid)
        if num_prog_vars == 1:
            # create a simple linear model (with 1 prognostic variable)
            model = toy_linear.create_time_independent_linear_model()
        else:
            # TODO
            print(
                "TODO: Test Identity operator for models with multiple prognostic variables"
            )
            raise NotImplementedError("TODO")

        model_state_size = model.state_vector().size
        num_gridpoints = int(model_state_size // num_prog_vars)

        if num_prog_vars > 1:
            # TODO: This branch will be removed as soon as the Identity operator accepts more than one prognostic variable
            with pytest.raises(Exception) as e_info:
                obs_oper = identity.Identity(configs={"model": model})
            assert e_info.type == AssertionError

        else:
            # Create observation operator and test its type
            obs_oper = identity.Identity(configs={"model": model})
            assert isinstance(obs_oper, identity.Identity)

        # Test the original design
        # ~~~~~~~~~~~~~~~~~~~~~~~~
        assert_allequal(obs_oper.design, np.ones(num_gridpoints))

        # Test methods for multiple choices of the design
        # ===============================================
        designs_pool = [
            np.ones(num_gridpoints),
            _RNG.choice([0, 1], size=num_gridpoints),
            np.round(_RNG.random(num_gridpoints), 1),
        ]
        for design in designs_pool:
            # test property `design`
            # ~~~~~~~~~~~~~~~~~~~~~~
            # Update design
            obs_oper.design = design
            assert_allequal(obs_oper.design, design.astype(bool))

            # Observation vector size
            observation_size = num_prog_vars * np.count_nonzero(design)

            # Test methods:
            # -------------

            # test method `observation_vector`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for init_val in [0, _RNG.random()]:
                obs = obs_oper.observation_vector(init_val=init_val)
                assert isinstance(obs, np.ndarray)
                assert obs.size == observation_size
                assert_allclose(obs, obs.astype(float))
                assert_allclose(obs, init_val)

            # test method `is_observation_vector`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            obs = _RNG.random(observation_size)
            assert obs_oper.is_observation_vector(obs) is True
            assert (
                obs_oper.is_observation_vector(_RNG.random(observation_size + 1))
                is False
            )
            assert obs_oper.is_observation_vector(_RNG.random()) is False

            # test method `get_observation_grid`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            model_grid, _ = utility.validate_Cartesian_grid(
                model.get_model_grid(),
            )
            observation_grid, _ = utility.validate_Cartesian_grid(
                obs_oper.get_observation_grid(),
            )
            assert_allequal(model_grid, observation_grid)

            # test method `apply`
            # ~~~~~~~~~~~~~~~~~~~
            state = _RNG.random(model_state_size)
            y = obs_oper.apply(state)
            assert y.size == observation_size
            assert_allclose(y, state[obs_oper.extended_design])

            # test method `__call__`
            # ~~~~~~~~~~~~~~~~~~~~~~
            state = _RNG.random(model_state_size)
            y = obs_oper(state)
            assert y.size == observation_size
            assert_allclose(y, state[obs_oper.extended_design])

            # test method `Jacobian_T_matvec`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            obs = _RNG.random(observation_size)
            state = np.zeros(model_state_size)
            state[obs_oper.extended_design] = obs
            out_state = obs_oper.Jacobian_T_matvec(obs)
            assert_allclose(state, out_state)

            # Test properties
            # ---------------

            # test property `model`
            # ~~~~~~~~~~~~~~~~~~~~~
            assert obs_oper.model is model

            # test property `shape`
            # ~~~~~~~~~~~~~~~~~~~~~
            assert obs_oper.shape == (observation_size, model_state_size)

            # test property `extended_design`
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            full_design = obs_oper.extended_design
            assert isinstance(full_design, np.ndarray)
            assert np.count_nonzero(full_design) == observation_size
            assert_allequal(full_design, full_design.astype(bool))
