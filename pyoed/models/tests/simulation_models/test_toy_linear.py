# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test Toy Linear Simulation models and related functionality: pyoed.models.simulation_models.toy_linear.*
"""

import numpy as np

from pyoed.models.simulation_models import toy_linear
from pyoed import utility
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)
import pytest
from dataclasses import asdict

pytestmark = pytest.mark.simulation_models

# Settings/Configurations:
# ========================
_SIMULATION_MODEL_STATE_SIZE = SETTINGS.SIMULATION_MODEL_STATE_SIZE
_SIMULATION_MODEL_PARAMETER_SIZE = SETTINGS.SIMULATION_MODEL_PARAMETER_SIZE
_NOISE_RANDOM_SEED = SETTINGS.RANDOM_SEED
_NUM_COMPARISON_RTOL = SETTINGS.NUM_COMPARISON_RTOL
_NUM_COMPARISON_ATOL = SETTINGS.NUM_COMPARISON_ATOL
_NUM_COMPARISON_DECIMALS = SETTINGS.NUM_COMPARISON_DECIMALS

_SIMULATION_TSPAN = (0, 10)
_SIMULATION_NUM_POINTS = 6


# Create a random number generator
# ================================
_RNG = create_random_number_generator()


# Tests (Classes & Functions):
# ============================


# Tests: (pyoed.models.simulation_models.toy_linear.ToyLinearTimeDependent)
# -------------------------------------------------------------------------
def test_toy_linear_ToyLinearTimeIndependent(
    state_size=_SIMULATION_MODEL_STATE_SIZE,
    parameter_size=_SIMULATION_MODEL_PARAMETER_SIZE,
    num_decimals=_NUM_COMPARISON_DECIMALS,
    seed=_NOISE_RANDOM_SEED,
):
    """
    The simulation model maps parameters (from parameter space) into states (in state space).
    Thus, the model has a domain of dimension equal to `parameter_size` and codomain of dimension
    equal to the `state_size`.
    """

    # method `create_model`: create the model based on the random seed above
    model_configs = dict(
        np=parameter_size,
        nx=state_size,
        random_seed=seed,
        num_decimals=num_decimals,
    )

    model = toy_linear.ToyLinearTimeIndependent(configs=model_configs)
    assert isinstance(model, toy_linear.ToyLinearTimeIndependent)

    # method `get_model_array`
    model_array = model.get_model_array()
    assert isinstance(model_array, np.ndarray)
    assert model_array.shape == (state_size, parameter_size)
    if num_decimals is not None:
        assert_allequal(model_array, np.round(model_array, decimals=num_decimals))

    if seed is not None:
        model_copy = toy_linear.ToyLinearTimeIndependent(configs=model_configs)
        model_copy_array = model_copy.get_model_array()
        if num_decimals is None:
            assert_allclose(
                model_copy_array,
                mdodel_array,
                atol=_NUM_COMPARISON_ATOL,
                rtol=_NUM_COMPARISON_RTOL,
            )
        else:
            assert_allequal(model_array, model_copy_array)
        del model_copy, model_copy_array

    # method `parameter_vector`
    for init_val in [0, _RNG.random()]:
        param = model.parameter_vector(init_val=init_val)
        assert isinstance(param, np.ndarray)
        assert param.size == parameter_size
        assert_allclose(param, param.astype(float))
        assert_allclose(param, init_val)

    # method `is_parameter_vector`
    param = _RNG.random(parameter_size)
    assert model.is_parameter_vector(param) is True
    assert model.is_parameter_vector(_RNG.random(parameter_size + 1)) is False
    assert model.is_parameter_vector(_RNG.random()) is False

    # method `state_vector`
    for init_val in [0, _RNG.random()]:
        state = model.state_vector(init_val=init_val)
        assert isinstance(state, np.ndarray)
        assert state.size == state_size
        assert_allclose(state, state.astype(float))
        assert_allclose(state, init_val)

    # method `is_state_vector`
    state = _RNG.random(state_size)
    assert model.is_state_vector(state) is True
    assert model.is_state_vector(_RNG.random(state_size + 1)) is False
    assert model.is_state_vector(_RNG.random()) is False

    # method `solve_forward`
    test_param = _RNG.random(parameter_size)
    test_state = model_array.dot(test_param)
    out_state = model.solve_forward(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `apply`
    test_param = _RNG.random(parameter_size)
    test_state = model_array.dot(test_param)
    out_state = model.apply(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `__call__`
    test_param = _RNG.random(parameter_size)
    test_state = model_array.dot(test_param)
    out_state = model(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `Jacobian_T_matvec`
    test_state = _RNG.random(state_size)
    test_param = model_array.T.dot(test_state)
    assert_allclose(
        model.Jacobian_T_matvec(test_state),
        test_param,
        atol=_NUM_COMPARISON_ATOL,
        rtol=_NUM_COMPARISON_RTOL,
    )

    # method `get_default_configurations`
    pass

    # method `get_model_grid`
    model_grid = np.arange(state_size)
    assert np.all(model_grid == model.get_model_grid())

    # property `configurations`

    retrieved_configs = asdict(model.configurations)
    for key in model_configs:
        assert model_configs[key] == retrieved_configs[key]

    # property `random_state`
    pass


def test_toy_linear_ToyLinearTimeDependent(
    state_size=_SIMULATION_MODEL_STATE_SIZE,
    seed=_NOISE_RANDOM_SEED,
    num_decimals=_NUM_COMPARISON_DECIMALS,
):
    """
    Test functionality of the class toy_linear.ToyLinearTimeDependent;

    """

    # method `create_model`: create the model based on the random seed above
    dt = (_SIMULATION_TSPAN[-1] - _SIMULATION_TSPAN[0]) / float(
        _SIMULATION_NUM_POINTS - 1
    )
    model_step_size = np.round(dt, decimals=_NUM_COMPARISON_DECIMALS)
    assert (
        model_step_size > 0
    ), "Check the test configurations; time step must be positive!"

    parameter_size = state_size
    model_configs = dict(
        nx=state_size,
        dt=dt,
        random_seed=seed,
        num_decimals=num_decimals,
    )

    model = toy_linear.ToyLinearTimeDependent(configs=model_configs)
    assert isinstance(model, toy_linear.ToyLinearTimeDependent)

    # method `get_model_array`
    model_array = model.get_model_array()
    assert isinstance(model_array, np.ndarray)
    assert model_array.shape == (state_size, state_size)
    if num_decimals is not None:
        assert_allequal(model_array, np.round(model_array, decimals=num_decimals))

    if seed is not None:
        model_copy = toy_linear.ToyLinearTimeDependent(configs=model_configs)
        model_copy_array = model_copy.get_model_array()
        if num_decimals is None:
            assert_allclose(
                model_copy_array,
                mdodel_array,
                atol=_NUM_COMPARISON_ATOL,
                rtol=_NUM_COMPARISON_RTOL,
            )
        else:
            assert_allequal(model_array, model_copy_array)
        del model_copy, model_copy_array

    # method `create_initial_condition`
    model_ic = model.create_initial_condition()
    assert isinstance(model_ic, np.ndarray)
    assert_allclose(model_ic, model_ic.astype(float))

    # method `parameter_vector`
    for init_val in [0, _RNG.random()]:
        param = model.parameter_vector(init_val=init_val)
        assert isinstance(param, np.ndarray)
        assert param.size == parameter_size
        assert_allclose(param, param.astype(float))
        assert_allclose(param, init_val)

    # method `is_parameter_vector`
    param = _RNG.random(parameter_size)
    assert model.is_parameter_vector(param) is True
    assert model.is_parameter_vector(_RNG.random(parameter_size + 1)) is False
    assert model.is_parameter_vector(_RNG.random()) is False

    # method `state_vector`
    for init_val in [0, _RNG.random()]:
        state = model.state_vector(init_val=init_val)
        assert isinstance(state, np.ndarray)
        assert state.size == state_size
        assert_allclose(state, state.astype(float))
        assert_allclose(state, init_val)

    # method `is_state_vector`
    state = _RNG.random(state_size)
    assert model.is_state_vector(state) is True
    assert model.is_state_vector(_RNG.random(state_size + 1)) is False
    assert model.is_state_vector(_RNG.random()) is False

    # TODO: method `integrate_state`
    checkpoints = [i * dt for i in range(10)]
    test_ic = _RNG.random(state_size)
    test_traject = [test_ic]
    for i in range(len(checkpoints) - 1):
        test_traject.append(model_array.dot(test_traject[-1]))

    out_checkpoints, out_traject = model.integrate_state(
        test_ic.copy(), tspan=(checkpoints[0], checkpoints[-1]), checkpoints=checkpoints
    )
    # assert_allclose(out_checkpoints, checkpoints)
    assert_allequal(out_checkpoints, checkpoints)
    for x0, x1 in zip(out_traject, test_traject):
        assert_allclose(x0, x1, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL)

    out_traject = [test_ic.copy()]
    for t0, t1 in zip(checkpoints[:-1], checkpoints[1:]):
        out_checkpoints, tmp_traject = model.integrate_state(
            out_traject[-1], tspan=(t0, t1), checkpoints=None
        )
        assert_allequal(out_checkpoints, (t0, t1))
        out_traject.append(tmp_traject[-1])
    for x0, x1 in zip(out_traject, test_traject):
        assert_allclose(x0, x1, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL)

    test_state = _RNG.random(state_size)
    with pytest.raises(Exception) as e_info:
        model.integrate_state(test_state, tspan=(0, 1.5 * dt))
    assert e_info.type is AssertionError

    # method `Jacobian_T_matvec`
    test_state = _RNG.random(state_size)
    test_param = model_array.T.dot(test_state)
    assert_allclose(
        model.Jacobian_T_matvec(test_state),
        test_param,
        atol=_NUM_COMPARISON_ATOL,
        rtol=_NUM_COMPARISON_RTOL,
    )

    # method `get_default_configurations`
    pass

    # method `get_model_grid`
    model_grid = np.arange(state_size)
    assert np.all(model_grid == model.get_model_grid())

    # property `configurations`
    retrieved_configs = asdict(model.configurations)
    for key in model_configs:
        assert model_configs[key] == retrieved_configs[key]

    # property `random_state`
    pass
