# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test Toy Linear Simulation models and related functionality: pyoed.models.simulation_models.toy_linear.*
"""

import numpy as np

from pyoed.models.simulation_models import black_box
from pyoed.models.simulation_models.black_box import TimeIndependentBlackBox
from pyoed import utility
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    create_random_number_generator,
)

import pytest

pytestmark = pytest.mark.simulation_models


# Settings/Configurations:
# ========================
_NOISE_RANDOM_SEED = SETTINGS.RANDOM_SEED
_NUM_COMPARISON_ATOL = SETTINGS.NUM_COMPARISON_ATOL
_NUM_COMPARISON_RTOL = SETTINGS.NUM_COMPARISON_RTOL


# Create a random number generator
# ================================
_RNG = create_random_number_generator()


# Tests: (pyoed.models.simulation_models.TimeIndependentBlackBox)
# -------------------------------------------------------------------------
def test_black_box_linear():
    # method `create_model`: create the model based on the random seed above
    A = np.arange(1, 13).reshape(4, 3)
    f = lambda x: A @ x
    state_size, parameter_size = A.shape
    model_configs = dict(
        solve_forward=f,
        N_x=state_size,
        N_p=parameter_size,
    )
    model = TimeIndependentBlackBox(configs=model_configs)
    assert isinstance(model, TimeIndependentBlackBox)

    # method `parameter_vector`
    for init_val in [0, _RNG.random()]:
        param = model.parameter_vector(init_val=init_val)
        assert isinstance(param, np.ndarray)
        assert param.size == parameter_size
        assert_allclose(param, param.astype(float))
        assert_allclose(param, init_val * np.ones(param.size))

    # method `is_parameter_vector`
    param = _RNG.random(parameter_size)
    assert model.is_parameter_vector(param) is True
    assert model.is_parameter_vector(_RNG.random(parameter_size + 1)) is False
    assert model.is_parameter_vector(_RNG.random()) is False

    # method `state_vector`
    for init_val in [0, _RNG.random()]:
        state = model.state_vector(init_val=init_val)
        assert isinstance(state, np.ndarray)
        assert state.size == state_size
        assert_allclose(state, state.astype(float))
        assert_allclose(state, init_val * np.ones(state_size))

    # method `is_state_vector`
    state = _RNG.random(state_size)
    assert model.is_state_vector(state) is True
    assert model.is_state_vector(_RNG.random(state_size + 1)) is False
    assert model.is_state_vector(_RNG.random()) is False

    # method `solve_forward`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model.solve_forward(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `apply`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model.apply(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `__call__`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `Jacobian_T_matvec`
    test_state = _RNG.random(state_size)
    test_param = A.T.dot(test_state)
    assert_allclose(
        model.Jacobian_T_matvec(test_state, test_param),
        test_param,
        atol=_NUM_COMPARISON_ATOL,
        rtol=_NUM_COMPARISON_RTOL,
    )

    # property `configurations`
    retrieved_configs = model.configurations.asdict(deep=False)
    for key in model_configs:
        assert model_configs[key] == retrieved_configs[key]


def test_black_box_nonlinear():
    # method `create_model`: create the model based on the random seed above
    A = np.arange(1, 13).reshape(4, 3)
    f = lambda x: np.sin(A @ x)
    J = lambda x: np.diag(np.cos(A @ x)) @ A
    state_size, parameter_size = A.shape
    model_configs = dict(
        solve_forward=f,
        N_x=state_size,
        N_p=parameter_size,
    )
    model = TimeIndependentBlackBox(configs=model_configs)
    assert isinstance(model, TimeIndependentBlackBox)

    # method `parameter_vector`
    for init_val in [0, _RNG.random()]:
        param = model.parameter_vector(init_val=init_val)
        assert isinstance(param, np.ndarray)
        assert param.size == parameter_size
        assert_allclose(param, param.astype(float))
        assert_allclose(param, init_val * np.ones(parameter_size))

    # method `is_parameter_vector`
    param = _RNG.random(parameter_size)
    assert model.is_parameter_vector(param) is True
    assert model.is_parameter_vector(_RNG.random(parameter_size + 1)) is False
    assert model.is_parameter_vector(_RNG.random()) is False

    # method `state_vector`
    for init_val in [0, _RNG.random()]:
        state = model.state_vector(init_val=init_val)
        assert isinstance(state, np.ndarray)
        assert state.size == state_size
        assert_allclose(state, state.astype(float))
        assert_allclose(state, init_val * np.ones(state_size))

    # method `is_state_vector`
    state = _RNG.random(state_size)
    assert model.is_state_vector(state) is True
    assert model.is_state_vector(_RNG.random(state_size + 1)) is False
    assert model.is_state_vector(_RNG.random()) is False

    # method `solve_forward`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model.solve_forward(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `apply`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model.apply(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `__call__`
    test_param = _RNG.random(parameter_size)
    test_state = f(test_param)
    out_state = model(test_param)
    assert_allclose(
        out_state, test_state, atol=_NUM_COMPARISON_ATOL, rtol=_NUM_COMPARISON_RTOL
    )

    # method `Jacobian_T_matvec`
    eval_at = _RNG.random(parameter_size)
    test_state = _RNG.random(state_size)
    test_param = J(eval_at).T.dot(test_state)
    assert_allclose(
        model.Jacobian_T_matvec(test_state, eval_at),
        test_param,
        atol=_NUM_COMPARISON_ATOL,
        rtol=_NUM_COMPARISON_RTOL,
    )

    # property `configurations`
    retrieved_configs = model.configurations.asdict(deep=False)
    for key in model_configs:
        assert model_configs[key] == retrieved_configs[key]
