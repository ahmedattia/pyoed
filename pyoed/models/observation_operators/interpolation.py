# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Class implementing multiple observation-by-interpolation operators.

- Selection (restriction) operator, as well as 1D, 2D, 3D interpolation operators
  in cartesian coordinates are developed.
"""
from dataclasses import dataclass, field
import re
import numpy as np
from scipy import interpolate as _sp_interpolate

from pyoed.models.core import (
    ObservationOperator,
    ObservationOperatorConfigs,
    SimulationModel,
)
from pyoed import utility
from pyoed.configs import (
    PyOEDConfigsValidationError,
    validate_key,
    aggregate_configurations,
    set_configurations,
)



@dataclass(kw_only=True, slots=True)
class ArrayOperatorConfigs(ObservationOperatorConfigs):
    """
    Configuration class for ArrayOperator

    :param model: the simulation model to use
    :param obs_array: The array/matrix representation of the observation operator
    """

    model: SimulationModel | None = None
    obs_array: np.ndarray | None = None


@set_configurations(ArrayOperatorConfigs)
class ArrayOperator(ObservationOperator):
    """
    This class implements a linear observation operator given its array representation.
    The operator is thus both linear and constant.

    All functions/methods return a restriction of the states passed as arguments to the observation space by dot product with the underlying array represenation of the observation operator passed upon initialization.

    In addition to standard functionality, here we provide a specific attribute 'design'
    which enables modifying the space of the error model by projection or reweighting \n
        - If the design is a binary vector (bool or int dtype attributes with 0/1
          entries) the mean, the covariance, and all random vectors are projected onto
          the space identified by the 1/True entries.
        - If the design contains non-binary values, the covariance matrix is weighted
          based on the design as follows. The underlying Cholesky decomposition of the
          distribution covariance is premultiplied by a design matrix with the design
          set to its diagonal. This is the standard relaxed OED formulation
    """

    def __init__(self, configs: ArrayOperatorConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        model = self.configurations.model
        obs_array = self.configurations.obs_array

        # All good; associate the model
        self._OBS_ARRAY = np.squeeze(utility.asarray(obs_array))
        self._SHAPE = self._OBS_ARRAY.shape
        self._OBSERVATION_GRID = np.arange(self._SHAPE[0])

        # Initiate a design
        self._DESIGN = np.ones(self._SHAPE[0], dtype=bool)

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`ArrayOperatorConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ArrayOperatorConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model",
            test=lambda x: isinstance(x, SimulationModel),
            message="Model must be an instance of SimulationModel!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        def obs_array_test_1(x):  # TODO: This is kind of evil.
            try:
                x = np.squeeze(utility.asarray(x))
            except (TypeError, ValueError):
                return False
            return True

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="obs_array",
            test=obs_array_test_1,
            message="Valid observation array must be passed in the configs dictionary!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        def obs_array_test_2(x):
            return (np.ndim(x) == 2) and (
                x.shape[1] == aggregated_configs.model.state_size
            )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="obs_array",
            test=obs_array_test_2,
            message="Passed observation array must have number of columns equal to model state size!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def observation_vector(self, init_val=0.0):
        """
        Create an observation vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        :returns: an observation vector identical to state vectors generated by the underlying model
        """
        obs = np.empty(self.shape[0])
        obs[:] = init_val
        return obs

    def is_observation_vector(self, observation):
        """
        Test whether the passed observation vector is valid or not

        :param observation: an observation vector
        """
        valid = False
        if isinstance(observation, np.ndarray):
            valid = observation.size == self.shape[0]
        return valid

    def get_observation_grid(self):
        """
        Return a copy of the observational grid
        """
        return self._OBSERVATION_GRID.copy()

    def apply(self, state):
        """
        Given a state/parameter vector (laid out as defined by the model grid passed upon instantiation),
        evaluate the observation vector by applying interpolation

        :param state: instance of a model state with entries corresponding the model grid passed upon instantiation
        :returns: an observation vector/instance
        :raises: `TypeError` is raised if state has the wrong shape,
        """
        if not self.model.is_state_vector(state):
            print(
                "The passed vector is not a valid model state/parameter or is incompatible with the underlying simulation model"
            )
            raise TypeError
        obs = self._OBS_ARRAY.dot(state)
        return obs[self._DESIGN]

    def Jacobian_T_matvec(self, observation, eval_at=None):
        """
        Multiply (Matrix-free matrix-vector product) of *transpose* of the Jacobian/tangent-linear (TLM)
        of the observation operator by the passed observation;

        :param observation: observation instance/vector
        :param eval_at: state around which observation operator is linearized; ignored here since :math:`\\mathbf{I}` is linear

        :returns: result of multiplying the transpose of derivative of the observation operator by the passed observation

        """
        if not self.is_observation_vector(observation):
            print("The passed vector is not a valid observation vector")
            raise TypeError
        state = self._OBS_ARRAY[self._DESIGN, :].T.dot(observation)
        return state

    @property
    def shape(self):
        """A tuple holding the shape of the observation operator (observation size, model state size)"""
        return self._SHAPE

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        return self._DESIGN.copy()

    @property
    def design(self):
        """Get a copy of the design vector (bool representing active/inactive sensors)"""
        return self._DESIGN.copy()

    @design.setter
    def design(self, design):
        """
        Update the design vector . The passed vector is regarded as boolean: all nonzero values are replaced with 1
        """
        design = np.asarray(design, dtype=bool).flatten()
        assert (
            design.size == self._DESIGN.size
        ), "The passed design vector has the wrong size!"

        # Active sensors, and design variables
        self._DESIGN = design
        active_size = np.count_nonzero(design)
        self._SHAPE = (active_size, self._SHAPE[1])

    @property
    def model(self):
        """The underlying simulation model"""
        return self.configurations.model

    @model.setter
    def model(self, value):
        print(
            "You are not supposed to change the underlying model after inistantiation."
        )
        raise NotImplementedError()


@dataclass(kw_only=True, slots=True)
class SelectionOperatorConfigs(ObservationOperatorConfigs):
    """
    Configuration class for SelectionOperator

    :param model: the simulation model to use
    :param observation_indexes: indexes of the model state_vector to select.
    :param str target: either 'state' or 'observation'
    """

    model: SimulationModel | None = None
    observation_indexes: np.ndarray | None = None
    target: str = "state"


@set_configurations(SelectionOperatorConfigs)
class SelectionOperator(ObservationOperator):
    """
    This class implements an observation operator that is selects entries of the state vector.
    The operator is thus both linear and constant.

    All functions/methods return a restriction of the states passed as arguments to the observation indexes

    In addition to standard functionality, here we provide a specific attribute 'design' which
    enables modifying the space of the error model by projection or reweighting \n
        - If the design is a binary vector (bool or int dtype attributes with 0/1 entries)
          the mean, the covariance, and all random vectors are projected onto the space identified by
          the 1/True entries.
        - If the design contains non-binary values, the covariance matrix is weighted based on the design
          as follows. The underlying Cholesky decomposition of the distribution covariance is premultiplied
          by a design matrix with the design set to its diagonal. This is the standard relaxed OED formulation

    :param model: the simulation model to use
    :param observation_indexes: indexes of the model state_vector to select.
    :param str target: either 'state' or 'observation'

    """

    def __init__(self, configs=None):
        # Validate and Assure Access to super attributes!
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        model = self.configurations.model
        observation_indexes = self.configurations.observation_indexes
        target = self.configurations.target

        # All good; associate the model
        observation_indexes = np.unique(np.array(observation_indexes).flatten())
        self._OBSERVATION_GRID = observation_indexes
        observation_grid_size = observation_indexes.size

        self._TARGET_OBSERVATION = target
        if re.match(r"\Astate\Z", target):
            model_size = model.state_vector().size
        elif re.match(r"\Aparameter\Z", target):
            model_size = model.parameter_vector().size
        else:
            print("Unrecognized value of the target to be observed {0}".format(target))
            raise ValueError

        self._SHAPE = (observation_grid_size, model_size)

        # Initiate a design
        self._DESIGN = np.ones(observation_grid_size, dtype=bool)

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`SelectionOperatorConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`SelectionOperatorConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model",
            test=lambda x: isinstance(x, SimulationModel),
            message="Model must be an instance of SimulationModel!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="target",
            test=lambda x: re.match(r"\A(state|parameter)\Z", x, re.IGNORECASE),
            message="Target must be either 'state' or 'parameter'!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        def obs_ind_test_1(x):
            try:
                x = np.unique(np.array(x).flatten())
            except (TypeError, ValueError):
                return False
            return True

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observation_indexes",
            test=obs_ind_test_1,
            message="Valid observation indexes must be passed in the configs dictionary!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        def obs_ind_test_2(x):
            x = np.unique(np.array(x).flatten())
            if x.size == 0:
                return False
            elif x.size == 1:
                return 0 <= x[0] <= aggregated_configs.model.state_size
            return 0 <= x[0] < x[-1] <= aggregated_configs.model.state_size

        msg = (
            "At least one index must be observed"
            + " and the indexes must be in the interval [0, model.state_size]!"
        )
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observation_indexes",
            test=obs_ind_test_2,
            message=msg,
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def observation_vector(self, init_val=0.0):
        """
        Create an observation vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        :returns: an observation vector identical to state vectors generated by the underlying model
        """
        target = self._TARGET_OBSERVATION
        if re.match(r"\A(state)\Z", target, re.IGNORECASE):
            obs = self.model.state_vector()[self._OBSERVATION_GRID][self._DESIGN]
        elif re.match(r"\A(parameter)\Z", target, re.IGNORECASE):
            obs = self.model.parameter_vector()[self._OBSERVATION_GRID][self._DESIGN]
        else:
            print("Unrecognized value of the target to be observed {0}".format(target))
            raise ValueError
        obs[:] = init_val
        return obs

    def is_observation_vector(self, observation):
        """
        Test whether the passed observation vector is valid or not

        :param observation: an observation vector
        """
        valid = False
        if isinstance(observation, np.ndarray):
            valid = observation.size == self.shape[0]
        return valid

    def get_observation_grid(self):
        """
        Return a copy of the observational grid
        """
        return self._OBSERVATION_GRID.copy()

    def apply(self, state):
        """
        Given a state/parameter vector (laid out as defined by the model grid passed upon instantiation),
        evaluate the observation vector by applying interpolation

        :param state: instance of a model state with entries corresponding the model grid passed upon instantiation
        :returns: an observation vector/instance
        :raises: `TypeError` is raised if state has the wrong shape,
        """
        target = self._TARGET_OBSERVATION
        if re.match(r"\A(state)\Z", target, re.IGNORECASE):
            checker = self.model.is_state_vector
        elif re.match(r"\A(parameter)\Z", target, re.IGNORECASE):
            checker = self.model.is_parameter_vector
        else:
            print("Unrecognized value of the target to be observed {0}".format(target))
            raise ValueError

        if not checker(state):
            print(
                "The passed vector is not a valid model state/parameter or is incompatible with the underlying simulation model"
            )
            raise TypeError
        obs = state[self._OBSERVATION_GRID].copy()
        return obs[self._DESIGN]

    def Jacobian_T_matvec(self, observation, eval_at=None):
        """
        Multiply (Matrix-free matrix-vector product) of *transpose* of the Jacobian/tangent-linear (TLM)
        of the observation operator by the passed observation;

        :param observation: observation instance/vector
        :param eval_at: state around which observation operator is linearized; ignored here since :math:`\\mathbf{I}` is linear

        :returns: result of multiplying the transpose of derivative of the observation operator by the passed observation

        """
        state = np.zeros(self._SHAPE[1], dtype=np.double)
        full_obs = self.observation_vector(init_val=0)
        full_obs[self._DESIGN] = observation.copy()
        state[self._OBSERVATION_GRID] = full_obs
        return state

    @property
    def shape(self):
        """A tuple holding the shape of the observation operator (observation size, model state size)"""
        return self._SHAPE

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        return self._DESIGN.copy()

    @property
    def design(self):
        """Get a copy of the design vector (bool representing active/inactive sensors)"""
        return self._DESIGN.copy()

    @design.setter
    def design(self, design):
        """
        Update the design vector . The passed vector is regarded as boolean: all nonzero values are replaced with 1
        """
        design = np.asarray(design, dtype=bool).flatten()
        assert (
            design.size == self._OBSERVATION_GRID.size
        ), f"The passed design vector has the wrong size {design.size}; expected design of size {self._OBSERVATION_GRID.size}!"

        # Active sensors, and design variables
        self._DESIGN = design
        active_size = np.count_nonzero(design)
        self._SHAPE = (active_size, self._SHAPE[1])

    @property
    def model(self):
        """The underlying simulation model"""
        return self.configurations.model

    @model.setter
    def model(self, value):
        print(
            "You are not supposed to change the underlying model after inistantiation."
        )
        raise NotImplementedError()


@dataclass(kw_only=True, slots=True)
class CartesianInterpolatorConfigs(ObservationOperatorConfigs):
    """
    Configuration class for CartesianInterpolator

    :param model_grid: one or two dimensional numpy array holding grid coordinates
        corresponding to model state entries.
    :param observation_grid: one or two dimensional numpy array (must be same as
        `model_grid`) holding observational coordinates corresponding to observation
        instance entries.
    :param str method: interpolation method to use; default is 'linear'
    """

    model_grid: np.ndarray | None = None
    observation_grid: np.ndarray | None = None
    method: str = "linear"

    def __post_init__(self):
        if self.model_grid is not None:
            self.model_grid = np.squeeze(utility.asarray(self.model_grid))
            if self.model_grid.size == 0:
                self.model_grid = self.model_grid.flatten()
        if self.observation_grid is not None:
            self.observation_grid = np.squeeze(utility.asarray(self.observation_grid))
            if self.observation_grid.size == 0:
                self.observation_grid = self.observation_grid.flatten()


@set_configurations(CartesianInterpolatorConfigs)
class CartesianInterpolator(ObservationOperator):
    """
    An obsevation operator that interpolates prognostic (physics) variables from the
    model grid to the observation grid points.

    :remarks:
        - It is assumed in general that there are multiple prognostic variables, thus
          there must be an assumption about the layout of the model state and the
          observation entries, specifically whether the prognostic variables (all
          physics variables) at one grid are stored consecutively, or not. This is
          determined by inspecting the first two entries of the model grid and the
          observation grid respectively\n
        - It is also assumed that all prognostic variables are observed

    .. Warning::
        All observational grid points must be inside the model domain (we can
        interpolate, not extrapolate)

    :raises:
        - `TypeError` is raised if either the model grid or the observation grid
          is not in one/two/three dimensional coordinate systems or if they are not in
          the same coordinate system\n
        - `TypeError` is raised if the model or observation grid have irregular structure; e.g., not all variables are observed;\n
        - `ValueError` is raised if the observation grid is not within the model grid domain
    """

    def __init__(self, configs: CartesianInterpolatorConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)
        # Validate and Assure Access to super attributes!

        method = self.configurations.method
        model_grid = self.configurations.model_grid
        observation_grid = self.configurations.observation_grid

        # Multi-Prognostic variables support
        model_grid, model_prog_vars_indexes = self.__validate_grid(model_grid)
        observation_grid, observation_prog_vars_indexes = self.__validate_grid(
            observation_grid
        )

        # All good; associate the model and observation grid
        self._INTERPOLATION_METHOD = method
        self._MODEL_GRID = model_grid
        self._OBSERVATION_GRID = observation_grid
        self._MODEL_PROG_INDEXES = model_prog_vars_indexes  # list of indexes of each prognostic variable in the model grid
        self._OBSERVATION_PROG_INDEXES = observation_prog_vars_indexes  # list of indexes of each prognostic variable in the observation grid

        self._MODEL_STATE_SIZE = np.size(self._MODEL_GRID, 0)
        self._OBSERVATION_SIZE = np.size(self._OBSERVATION_GRID, 0)
        self._SHAPE = (self._OBSERVATION_SIZE, self._MODEL_STATE_SIZE)

        # Initiate a design
        _design = np.ones(
            self._OBSERVATION_SIZE // len(observation_prog_vars_indexes), dtype=bool
        )
        self._DESIGN = np.empty(self._OBSERVATION_SIZE, dtype=bool)
        for inds in self._OBSERVATION_PROG_INDEXES:
            self._DESIGN[inds] = _design.copy()

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`CartesianInterpolatorConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`CartesianInterpolatorConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="method",
            test=lambda x: re.match(r"\A(linear)\Z", x, re.IGNORECASE),
            message="Method can only be 'linear'!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # TODO: Validation is complex in this model, so I'm just going to forgo the
        # smart-skipping. Fix this later!
        model_grid = aggregated_configs.model_grid
        observation_grid = aggregated_configs.observation_grid
        if model_grid.ndim != observation_grid.ndim:
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    f"Model grid and Observation grid must be in the same coordinate system "
                    f"Both must by either 1, 2, or 3 dimensional cartesian coordinates "
                    f"model_grid is {model_grid.ndim}d, "
                    f"while observation_grid is {observation_grid.ndim}d"
                )
            else:
                return False

        # Sanity checks on the model and observational grid points
        model_grid, model_prog_vars_indexes = self.__validate_grid(
            model_grid,
        )
        observation_grid, observation_prog_vars_indexes = self.__validate_grid(
            observation_grid,
        )

        if np.size(model_grid, 1) != np.size(observation_grid, 1):
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    f"model grid and observation grid do not live in the same coordinate systems!\n"
                    f"model grid is {np.size(model_grid, 1)}d; "
                    f"observation grid is {np.size(observation_grid, 1)}d"
                )
            else:
                return False

        if len(model_prog_vars_indexes) != len(observation_prog_vars_indexes):
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    f"Number of prognostic variables in the model doesn't match that in the observation grid.\n"
                    f"This operator assumes all prognostic variables are observed!\n"
                    f"You must define your own derived observation operator otherwise!\n"
                )
            else:
                return False

        # Make sure all observational grid points are inside the model domain (we can interpolate, not extrapolate)
        for j in range(np.size(model_grid, 1)):
            m_domain = (model_grid[:, j].min(), model_grid[:, j].max())
            o_domain = (observation_grid[:, j].min(), observation_grid[:, j].max())
            if not (m_domain[0] <= o_domain[0] <= o_domain[1] <= m_domain[1]):
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Observation grid points in dimension {j=} "
                        f"are NOT within the model domain in this dimension.\n"
                        f" > Model domain: {m_domain}\n"
                        f" > Observation domain: {o_domain}"
                    )
                else:
                    return False

        return super().validate_configurations(configs, raise_for_invalid)

    def observation_vector(self, init_val=0.0):
        """
        Create an observation vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        :returns: a (1d) numpy array with size equal to the range of the observation operator
        """
        obs = np.empty(self._SHAPE[0], dtype=np.double)
        if utility.isnumber(init_val):
            obs[:] = init_val
        return obs

    def is_observation_vector(self, observation):
        """
        Test whether the passed observation vector is valid or not

        :param observation: an observation vector
        """
        valid = False
        if isinstance(observation, np.ndarray):
            if observation.ndim == 1 and observation.size == self._SHAPE[0]:
                valid = True
        return valid

    def get_observation_grid(self):
        """
        Return a copy of the observational grid
        """
        return self._OBSERVATION_GRID.copy()

    def apply(self, state):
        """
        Given a state vector (laid out as defined by the model grid passed upon instantiation),
        evaluate the observation vector by applying interpolation

        :param state: instance of a model state with entries corresponding the model grid passed upon instantiation
        :returns: an observation vector/instance
        :raises: `TypeError` is raised if state has the wrong shape,
        """
        if state.size != np.size(self._MODEL_GRID, 0):
            print(
                "the passed state has size {0} which doesn't match the associated grid; \
                  expected state of size {1}".format(
                    state.size, np.size(self._MODEL_GRID, 0)
                )
            )
            raise TypeError

        # Dimensionlaity of the model (and the observation) coordinate system
        grid_ndim = np.size(self._MODEL_GRID, 1)
        if grid_ndim == 1:
            obs = self.__lc_apply_1d(state)
        elif grid_ndim == 2:
            obs = self.__lc_apply_2d(state)
        elif grid_ndim == 3:
            obs = self.__lc_apply_3d(state)
        else:
            print(
                "This should never happen; cartesian coordinate system dimension can't exceed 3!"
            )
            raise ValueError
        return obs

    def __lc_apply_1d(self, state, missing_as_nan=True):
        """
        Local function that returns ::math:`H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state. Here, the observation operator extracts the prognostic variables at
            each observation grid point; this involves linear interpolation for nearest points inside a line stencil of model grid points.
        This function is designed for 1D-coordinate systems only

        :param state: model state vector
        :param bool missing_as_nan: if `True` observations requiring any missing values in the state will be set to `np.nan`

        :returns: ::math:`y:=H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state
        :raises: `ValueError` is raised if this method is invoked for a grid that is not 1-dimensional
        :remarks: the implementation here assumes the observation gridpoints are fully inside the model grid domain
        """
        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 1 or np.size(observation_grid, 1) != 1:
            print("This function is valid only for 1D interpolation only.")
            raise ValueError

        # Initialize observation vector
        obs = np.empty(np.size(observation_grid, 0), dtype=np.double)
        # obs = self.observation_vector()

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            model_prog_vars_indexes, observation_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = model_grid[src_indexes, :]
            trgt_grid = observation_grid[trgt_indexes, :]
            src_values = state[src_indexes].copy()
            trgt_values = np.empty(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by interpolation

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs = list(set(src_grid[:, 0]))
            model_Xs.sort()
            model_Xs = np.asarray(model_Xs)

            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(trgt_values.size):
                x = trgt_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0})".format(
                            x
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # All two sides of the stencil (in the model grid) are found; proceed
                X_0 = xbelow
                X_1 = xabove
                # Get indexes of these corners in the model state
                X_0_ind = utility.gridpoint_to_index(X_0, src_grid)
                X_1_ind = utility.gridpoint_to_index(X_1, src_grid)

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (X_0_ind, X_1_ind):
                    if missing_as_nan:
                        if self.debug:
                            print(
                                "DEBUG; filling index {0} with np.nan".format(obs_ind)
                            )
                        trgt_values[obs_ind] = np.nan
                        continue
                    else:
                        if self.debug:
                            print(
                                "One or more of the following model coordinates is NOT found in the model grid:"
                            )
                            print(X_0, X_1)
                            print("*** Terminating ***\n")
                        raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                trgt_values[obs_ind] = src_values[X_0_ind] + dx * (
                    src_values[X_1_ind] - src_values[X_0_ind]
                )

            # Update the corresponding prognostic variable
            obs[trgt_indexes] = trgt_values.copy()

        return obs[
            self._DESIGN
        ]  # TODO: better move it before applying interpolation; wasted computation

    def __lc_apply_2d(self, state, missing_as_nan=True):
        """
        Local function that returns ::math:`H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state. Here, the observation operator extracts the prognostic variables at
            each observation grid point; this involves bilinear interpolation for points inside a valid square/rectangle
            stencil of model grid points.
        This function is designed for 2D-coordinate systems only

        :param state: model state vector
        :param bool missing_as_nan: if `True` observations requiring any missing values in the state will be set to `np.nan`

        :returns: ::math:`y:=H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state
        :raises: `ValueError` is raised if this method is invoked for a grid that is not 2-dimensional
        :remarks: the implementation here assumes the observation gridpoints are fully inside the model grid domain
        """
        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 2 or np.size(observation_grid, 1) != 2:
            print("This function is valid only for 2D Bilinear interpolation.")
            raise ValueError

        # Bilinear-Interpolation elementary blocks
        P = np.zeros(4, dtype=float)
        Q = np.zeros(4, dtype=float)
        B = np.zeros((4, 4), dtype=int)
        B = np.array([[1, 0, 0, 0], [-1, 0, 1, 0], [-1, 1, 0, 0], [1, -1, -1, 1]])

        # Initialize observation vector
        obs = np.empty(np.size(observation_grid, 0), dtype=np.double)

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            model_prog_vars_indexes, observation_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = model_grid[src_indexes, :]
            trgt_grid = observation_grid[trgt_indexes, :]
            src_values = state[src_indexes].copy()
            trgt_values = np.empty(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by interpolation

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs, model_Ys = list(set(src_grid[:, 0])), list(set(src_grid[:, 1]))
            model_Xs.sort()
            model_Ys.sort()
            model_Xs, model_Ys = np.asarray(model_Xs), np.asarray(model_Ys)

            if self.debug:
                print("Model_Xs", model_Xs)
                print("Model_Ys", model_Ys)
            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(trgt_values.size):
                x, y = trgt_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0}, {1})".format(
                            x, y
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x, y)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # 2- Nearest y's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Ys <= y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys > y)[0][0]  # TODO: >=?
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if y == model_Ys.max():
                        below = np.where(model_Ys < y)[0][-1]
                        above = np.where(model_Ys >= y)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                ybelow = model_Ys[below]
                yabove = model_Ys[above]

                # All four corners of the lattice (in the model grid) are found; proceed
                X_00 = (xbelow, ybelow)
                X_01 = (xbelow, yabove)
                X_10 = (xabove, ybelow)
                X_11 = (xabove, yabove)

                # Get indexes of these corners in the model state
                X_00_ind = utility.gridpoint_to_index(X_00, src_grid)
                X_01_ind = utility.gridpoint_to_index(X_01, src_grid)
                X_10_ind = utility.gridpoint_to_index(X_10, src_grid)
                X_11_ind = utility.gridpoint_to_index(X_11, src_grid)

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (X_00_ind, X_01_ind, X_10_ind, X_11_ind):
                    if missing_as_nan:
                        if self.debug:
                            print(
                                "DEBUG; filling index {0} with np.nan".format(obs_ind)
                            )
                        trgt_values[obs_ind] = np.nan
                        continue
                    else:
                        print(
                            "One or more of the following model coordinates is NOT found in the model grid:"
                        )
                        print(X_00_ind, X_01_ind, X_10_ind, X_11_ind)
                        print("*** Terminating ***\n")
                        raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                Q[:] = 1, dx, dy, dx * dy
                P[0] = src_values[X_00_ind]
                P[1] = src_values[X_01_ind]
                P[2] = src_values[X_10_ind]
                P[3] = src_values[X_11_ind]

                trgt_values[obs_ind] = np.dot(Q, B.dot(P))

            # Update the corresponding prognostic variable
            obs[trgt_indexes] = trgt_values.copy()
        return obs[
            self._DESIGN
        ]  # TODO: better move it before applying interpolation; wasted computation

    def __lc_apply_3d(self, state, missing_as_nan=True):
        """
        Local function that returns ::math:`H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state. Here, the observation operator extracts the prognostic variables at
            each observation grid point; this involves trilinear interpolation for points inside a valid cubic
            stencil of model grid points.
        This function is designed for 3D-coordinate systems only

        :param state: model state vector
        :param bool missing_as_nan: if `True` observations requiring any missing values in the state will be set to `np.nan`

        :returns: ::math:`y:=H(x)`, where ::math:`H` is the observation/interpolation operator,
            and `x` is a model state
        :raises: `ValueError` is raised if this method is invoked for a grid that is not 3dimensional
        :remarks: the implementation here assumes the observation gridpoints are fully inside the model grid domain;
            Later we can/will consider Prism Linear Interpolation for cases where observation gridpoint are outside
            the model domain but within a slice of a cubic stencil
        """
        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 3 or np.size(observation_grid, 1) != 3:
            print("This function is valid only for 3D trilinear interpolation.")
            raise ValueError

        # Trilinear-Interpolation elementary blocks
        P = np.zeros(8, dtype=float)
        Q = np.zeros(8, dtype=float)
        B = np.zeros((8, 8), dtype=int)
        B = np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [-1, 0, 0, 0, 1, 0, 0, 0],
                [-1, 0, 1, 0, 0, 0, 0, 0],
                [-1, 1, 0, 0, 0, 0, 0, 0],
                [1, 0, -1, 0, -1, 0, 1, 0],
                [1, -1, -1, 1, 0, 0, 0, 0],
                [1, -1, 0, 0, -1, 1, 0, 0],
                [-1, 1, 1, -1, 1, -1, -1, 1],
            ]
        )

        # Initialize observation vector
        obs = np.empty(np.size(observation_grid, 0), dtype=np.double)

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            model_prog_vars_indexes, observation_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = model_grid[src_indexes, :]
            trgt_grid = observation_grid[trgt_indexes, :]
            src_values = state[src_indexes].copy()
            trgt_values = np.empty(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by interpolation

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs, model_Ys, model_Zs = (
                list(set(src_grid[:, 0])),
                list(set(src_grid[:, 1])),
                list(set(src_grid[:, 2])),
            )
            model_Xs.sort()
            model_Ys.sort()
            model_Zs.sort()
            model_Xs, model_Ys, model_Zs = (
                np.asarray(model_Xs),
                np.asarray(model_Ys),
                np.asarray(model_Zs),
            )

            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(trgt_values.size):
                x, y, z = trgt_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0}, {1}, {2})".format(
                            x, y, z
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x, y, z)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]  # TODO: check
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]  # TODO: >=?
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # 2- Nearest y's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Ys <= y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys > y)[0][0]  # TODO: >=?
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if y == model_Ys.max():
                        below = np.where(model_Ys < y)[0][-1]
                        above = np.where(model_Ys >= y)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                ybelow = model_Ys[below]
                yabove = model_Ys[above]

                # 3- Nearest z's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Zs <= z)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Zs > z)[0][0]  # TODO: >=?
                except IndexError:
                    above = None
                #
                if above is None and below is None:
                    print(
                        "This is IMPOSSIBLE unless no grid points exist in the whole model grid!"
                    )
                    raise ValueError
                elif (
                    above is None or below is None
                ):  # only one is valid; either above or below
                    if z == model_Zs.max():
                        below = np.where(model_Zs < z)[0][-1]
                        above = np.where(model_Zs >= z)[0][0]
                    else:
                        if missing_as_nan:
                            if self.debug:
                                print(
                                    "DEBUG; filling index {0} with np.nan".format(
                                        obs_ind
                                    )
                                )
                            trgt_values[obs_ind] = np.nan
                            continue
                        else:
                            print(
                                "This observation operator is designed for model grid covering all observation points!"
                            )
                            raise ValueError
                # else:  # both are valid
                zbelow = model_Zs[below]
                zabove = model_Zs[above]

                # All eight corners of the lattice (in the model grid) are found; proceed
                X_000 = (xbelow, ybelow, zbelow)
                X_001 = (xbelow, ybelow, zabove)
                X_010 = (xbelow, yabove, zbelow)
                X_011 = (xbelow, yabove, zabove)
                X_100 = (xabove, ybelow, zbelow)
                X_101 = (xabove, ybelow, zabove)
                X_110 = (xabove, yabove, zbelow)
                X_111 = (xabove, yabove, zabove)

                # Get indexes of these corners in the model state
                X_000_ind = utility.gridpoint_to_index(
                    (xbelow, ybelow, zbelow), src_grid
                )
                X_001_ind = utility.gridpoint_to_index(
                    (xbelow, ybelow, zabove), src_grid
                )
                X_010_ind = utility.gridpoint_to_index(
                    (xbelow, yabove, zbelow), src_grid
                )
                X_011_ind = utility.gridpoint_to_index(
                    (xbelow, yabove, zabove), src_grid
                )

                X_100_ind = utility.gridpoint_to_index(
                    (xabove, ybelow, zbelow), src_grid
                )
                X_101_ind = utility.gridpoint_to_index(
                    (xabove, ybelow, zabove), src_grid
                )
                X_110_ind = utility.gridpoint_to_index(
                    (xabove, yabove, zbelow), src_grid
                )
                X_111_ind = utility.gridpoint_to_index(
                    (xabove, yabove, zabove), src_grid
                )

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (
                    X_000_ind,
                    X_001_ind,
                    X_010_ind,
                    X_011_ind,
                    X_100_ind,
                    X_101_ind,
                    X_110_ind,
                    X_111_ind,
                ):
                    if missing_as_nan:
                        if self.debug:
                            print(
                                "DEBUG; filling index {0} with np.nan".format(obs_ind)
                            )
                        trgt_values[obs_ind] = np.nan
                        continue
                    else:
                        if self.debug:
                            print(
                                "One or more of the following model coordinates is NOT found in the model grid:"
                            )
                            print(
                                X_000, X_001, X_010, X_011, X_100, X_101, X_110, X_111
                            )
                            print("*** Terminating ***\n")
                        raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                dz = float(z - zbelow) / (zabove - zbelow)
                Q[:] = 1, dx, dy, dz, dx * dy, dy * dz, dz * dx, dx * dy * dz
                P[0] = src_values[X_000_ind]
                P[1] = src_values[X_001_ind]
                P[2] = src_values[X_010_ind]
                P[3] = src_values[X_011_ind]
                P[4] = src_values[X_100_ind]
                P[5] = src_values[X_101_ind]
                P[6] = src_values[X_110_ind]
                P[7] = src_values[X_111_ind]

                trgt_values[obs_ind] = np.dot(Q, B.dot(P))

            # Update the corresponding prognostic variable
            obs[trgt_indexes] = trgt_values.copy()

        return obs[
            self._DESIGN
        ]  # TODO: better move it before applying interpolation; wasted computation

    def Jacobian_T_matvec(self, observation, eval_at=None):
        """
        Multiply (Matrix-free matrix-vector product) of *transpose* of the Jacobian/tangent-linear (TLM)
        of the observation operator by the passed observation;
        The Jacobian is evaluated (for nonlinear operators) at `eval_at`

        :param observation: observation instance/vector
        :param eval_at: state around which observation operator is linearized; ignored here for I is linear

        :returns: result of multiplying the transpose of Jacobian of the observation operator by the passed observation

        :raises: TypeError is raised if the passed observation ]
        """
        if not self.is_observation_vector(observation):
            print(
                "The passed observation has a wrong type or size. \
                   Passed observation type: {0}; and size: {1}; \
                  expected observation of size {2}".format(
                    type(observation),
                    observation.size,
                    np.size(self._OBSERVATION_GRID, 0),
                )
            )
            raise TypeError

        # premultiply (project up) by the design matrix (diag of design vector to expand)
        proj_observation = np.zeros(np.size(self._OBSERVATION_GRID, 0))
        proj_observation[self._DESIGN] = observation.copy()
        # Dimensionlaity of the model (and the observation) coordinate system
        grid_ndim = np.size(self._OBSERVATION_GRID, 1)
        if grid_ndim == 1:
            state = self.__Jacobian_T_matvec_1d(proj_observation, eval_at=eval_at)
        elif grid_ndim == 2:
            state = self.__Jacobian_T_matvec_2d(proj_observation, eval_at=eval_at)
        elif grid_ndim == 3:
            state = self.__Jacobian_T_matvec_3d(proj_observation, eval_at=eval_at)
        else:
            print(
                "This should never happen; cartesian coordinate system dimension can't exceed 3!"
            )
            raise ValueError
        return state

    def __Jacobian_T_matvec_1d(self, observation, eval_at=None):
        """Jacobian transpose multiplied by an observation assuming 1D cartesian coordiantes"""

        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 1 or np.size(observation_grid, 1) != 1:
            print("This function is valid only for 3D trilinear interpolation.")
            raise ValueError

        # Initialize state vector
        state = np.empty(np.size(model_grid, 0), dtype=np.double)

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            observation_prog_vars_indexes, model_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = observation_grid[src_indexes, :]
            trgt_grid = model_grid[trgt_indexes, :]
            src_values = observation[src_indexes].copy()
            trgt_values = np.zeros(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by TLM

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs = list(set(trgt_grid[:, 0]))
            model_Xs.sort()
            model_Xs = np.asarray(model_Xs)

            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(src_values.size):
                x = src_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0})".format(
                            x
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]
                except IndexError:
                    above = None
                if above is None or below is None:
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        continue  # TODO: why do not we raise errors here?
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # All two sides of the stencil (in the model grid) are found; proceed
                X_0 = xbelow
                X_1 = xabove

                # Get indexes of these corners in the model state
                X_0_ind = utility.gridpoint_to_index(X_0, trgt_grid)
                X_1_ind = utility.gridpoint_to_index(X_1, trgt_grid)

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (X_0_ind, X_1_ind):
                    if self.debug:
                        print(
                            "DEBUG; One or more of the following model coordinates is NOT found in the model grid:"
                        )
                        print(X_0, X_1)
                        print("*** Terminating ***\n")
                    raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)

                # Now, rather than interpolation at that gridpoint, we need derivative of the observed prognostic variable, w.r.t each of the corners of
                trgt_values[X_0_ind] += (1 - dx) * src_values[obs_ind]
                trgt_values[X_1_ind] += (dx) * src_values[obs_ind]

            # Update the corresponding prognostic variable
            state[trgt_indexes] = trgt_values.copy()

        return state

    def __Jacobian_T_matvec_2d(self, observation, eval_at=None):
        """Jacobian transpose multiplied by an observation assuming 2D cartesian coordiantes"""

        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid
        # num_prog_vars           = len(model_prog_vars_indexes)

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 2 or np.size(observation_grid, 1) != 2:
            print("This function is valid only for 3D trilinear interpolation.")
            raise ValueError

        # Bilinear-Interpolation elementary blocks
        Q = np.zeros(4, dtype=float)
        B = np.zeros((4, 4), dtype=int)
        B = np.array([[1, 0, 0, 0], [-1, 0, 1, 0], [-1, 1, 0, 0], [1, -1, -1, 1]])

        # Initialize state vector
        state = np.empty(np.size(model_grid, 0), dtype=np.double)

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            observation_prog_vars_indexes, model_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = observation_grid[src_indexes, :]
            trgt_grid = model_grid[trgt_indexes, :]
            src_values = observation[src_indexes].copy()
            trgt_values = np.zeros(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by interpolation
            # eval_at_values = eval_at[src_indexes]

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs, model_Ys = list(set(trgt_grid[:, 0])), list(set(trgt_grid[:, 1]))
            model_Xs.sort()
            model_Ys.sort()
            model_Xs, model_Ys = np.asarray(model_Xs), np.asarray(model_Ys)

            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(src_indexes.size):
                x, y = src_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0}, {1})".format(
                            x, y
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x, y)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]  # TODO >=
                except IndexError:
                    above = None
                if above is None or below is None:
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        continue
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # 2- Nearest y's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Ys <= y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys > y)[0][0]
                except IndexError:
                    above = None
                if above is None or below is None:
                    if y == model_Ys.max():
                        below = np.where(model_Ys < y)[0][-1]
                        above = np.where(model_Ys >= y)[0][0]
                    else:
                        continue
                # else:  # both are valid
                ybelow = model_Ys[below]
                yabove = model_Ys[above]

                # All four corners of the lattice (in the model grid) are found; proceed
                X_00 = (xbelow, ybelow)
                X_01 = (xbelow, yabove)
                X_10 = (xabove, ybelow)
                X_11 = (xabove, yabove)

                # Get indexes of these corners in the model state
                X_00_ind = utility.gridpoint_to_index(X_00, trgt_grid)
                X_01_ind = utility.gridpoint_to_index(X_01, trgt_grid)
                X_10_ind = utility.gridpoint_to_index(X_10, trgt_grid)
                X_11_ind = utility.gridpoint_to_index(X_11, trgt_grid)

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (X_00_ind, X_01_ind, X_10_ind, X_11_ind):
                    if self.debug:
                        print(
                            "DEBUG; One or more of the following model coordinates is NOT found in the model grid:"
                        )
                        print(X_00, X_01, X_10, X_11)
                        print("*** Terminating ***\n")
                    raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                Q[:] = 1, dx, dy, dx * dy

                # Now, rather than interpolation at that gridpoint, we need derivative of the observed prognostic variable, w.r.t each of the corners of
                trgt_values[X_00_ind] += Q.dot(B[:, 0]) * src_values[obs_ind]
                trgt_values[X_01_ind] += Q.dot(B[:, 1]) * src_values[obs_ind]
                trgt_values[X_10_ind] += Q.dot(B[:, 2]) * src_values[obs_ind]
                trgt_values[X_11_ind] += Q.dot(B[:, 3]) * src_values[obs_ind]

            # Update the corresponding prognostic variable
            state[trgt_indexes] = trgt_values.copy()

        return state

    def __Jacobian_T_matvec_3d(self, observation, eval_at=None):
        """Jacobian transpose multiplied by an observation assuming 3D cartesian coordiantes"""
        # Retrieve model information
        model_grid = self._MODEL_GRID
        model_prog_vars_indexes = (
            self._MODEL_PROG_INDEXES
        )  # list of indexes of each prognostic variable in the model grid
        # num_prog_vars           = len(model_prog_vars_indexes)

        # Retrieve observation information
        observation_grid = self._OBSERVATION_GRID
        observation_prog_vars_indexes = (
            self._OBSERVATION_PROG_INDEXES
        )  # list of indexes of prognostic variables in the observation grid

        if np.size(model_grid, 1) != 3 or np.size(observation_grid, 1) != 3:
            print("This function is valid only for 3D trilinear interpolation.")
            raise ValueError

        # Trilinear-Interpolation elementary blocks
        Q = np.zeros(8, dtype=float)
        B = np.zeros((8, 8), dtype=int)
        B = np.array(
            [
                [1, 0, 0, 0, 0, 0, 0, 0],
                [-1, 0, 0, 0, 1, 0, 0, 0],
                [-1, 0, 1, 0, 0, 0, 0, 0],
                [-1, 1, 0, 0, 0, 0, 0, 0],
                [1, 0, -1, 0, -1, 0, 1, 0],
                [1, -1, -1, 1, 0, 0, 0, 0],
                [1, -1, 0, 0, -1, 1, 0, 0],
                [-1, 1, 1, -1, 1, -1, -1, 1],
            ]
        )

        # Initialize state vector
        state = np.empty(np.size(model_grid, 0), dtype=np.double)

        # Apply interpolation for each of the prognostic variables
        for src_indexes, trgt_indexes in zip(
            observation_prog_vars_indexes, model_prog_vars_indexes
        ):
            # TODO: some redundancy in the following code can be eliminated/reduced
            src_grid = observation_grid[src_indexes, :]
            trgt_grid = model_grid[trgt_indexes, :]
            src_values = observation[src_indexes].copy()
            trgt_values = np.zeros(
                len(trgt_indexes), dtype=np.double
            )  # to be filled by interpolation

            # these are not used!
            eval_at_values = eval_at[src_indexes] if eval_at is not None else None

            # Extract unique coordinates (TODO: could be even moved to __init__ to reduce redundancy)
            model_Xs, model_Ys, model_Zs = (
                list(set(trgt_grid[:, 0])),
                list(set(trgt_grid[:, 1])),
                list(set(trgt_grid[:, 2])),
            )
            model_Xs.sort()
            model_Ys.sort()
            model_Zs.sort()
            model_Xs, model_Ys, model_Zs = (
                np.asarray(model_Xs),
                np.asarray(model_Ys),
                np.asarray(model_Zs),
            )

            # Loop over each observation gridpoint for this prognostic variable
            for obs_ind in range(src_indexes.size):
                x, y, z = src_grid[obs_ind, :]
                if self.debug:
                    print(
                        "DEBUG; Interpolation at observation coordinates: ({0}, {1}, {2})".format(
                            x, y, z
                        )
                    )

                # Lookup gridpoints forming stencil around the observation gridpoint (x, y, z)
                # 1- Nearest x's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Xs <= x)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Xs > x)[0][0]
                except IndexError:
                    above = None
                if above is None or below is None:
                    if x == model_Xs.max():
                        below = np.where(model_Xs < x)[0][-1]
                        above = np.where(model_Xs >= x)[0][0]
                    else:
                        continue
                # else:  # both are valid
                xbelow = model_Xs[below]
                xabove = model_Xs[above]

                # 2- Nearest y's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Ys <= y)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Ys > y)[0][0]
                except IndexError:
                    above = None
                if above is None or below is None:
                    if y == model_Ys.max():
                        below = np.where(model_Ys < y)[0][-1]
                        above = np.where(model_Ys >= y)[0][0]
                    else:
                        continue
                # else:  # both are valid
                ybelow = model_Ys[below]
                yabove = model_Ys[above]

                # 3- Nearest z's above and below (We know it won't happen because we assert inclusion already, but adding it for later )
                try:
                    below = np.where(model_Zs <= z)[0][-1]
                except IndexError:
                    below = None
                try:
                    above = np.where(model_Zs > z)[0][0]
                except IndexError:
                    above = None
                if above is None or below is None:
                    if z == model_Zs.max():
                        below = np.where(model_Zs < z)[0][-1]
                        above = np.where(model_Zs >= z)[0][0]
                    else:
                        continue
                # else:  # both are valid
                zbelow = model_Zs[below]
                zabove = model_Zs[above]

                # All eight corners of the lattice (in the model grid) are found; proceed
                X_000 = (xbelow, ybelow, zbelow)
                X_001 = (xbelow, ybelow, zabove)
                X_010 = (xbelow, yabove, zbelow)
                X_011 = (xbelow, yabove, zabove)
                X_100 = (xabove, ybelow, zbelow)
                X_101 = (xabove, ybelow, zabove)
                X_110 = (xabove, yabove, zbelow)
                X_111 = (xabove, yabove, zabove)

                # Get indexes of these corners in the model state
                X_000_ind = utility.gridpoint_to_index(
                    (xbelow, ybelow, zbelow), trgt_grid
                )
                X_001_ind = utility.gridpoint_to_index(
                    (xbelow, ybelow, zabove), trgt_grid
                )
                X_010_ind = utility.gridpoint_to_index(
                    (xbelow, yabove, zbelow), trgt_grid
                )
                X_011_ind = utility.gridpoint_to_index(
                    (xbelow, yabove, zabove), trgt_grid
                )
                X_100_ind = utility.gridpoint_to_index(
                    (xabove, ybelow, zbelow), trgt_grid
                )
                X_101_ind = utility.gridpoint_to_index(
                    (xabove, ybelow, zabove), trgt_grid
                )
                X_110_ind = utility.gridpoint_to_index(
                    (xabove, yabove, zbelow), trgt_grid
                )
                X_111_ind = utility.gridpoint_to_index(
                    (xabove, yabove, zabove), trgt_grid
                )

                # NOTE: The following shouldn't happen for regular grid; adding for later
                if None in (
                    X_000_ind,
                    X_001_ind,
                    X_010_ind,
                    X_011_ind,
                    X_100_ind,
                    X_101_ind,
                    X_110_ind,
                    X_111_ind,
                ):
                    if self.debug:
                        print(
                            "DEBUG; One or more of the following model coordinates is NOT found in the model grid:"
                        )
                        print(X_000, X_001, X_010, X_011, X_100, X_101, X_110, X_111)
                        print("*** Terminating ***\n")
                    raise ValueError

                # All good; interpolate:
                dx = float(x - xbelow) / (xabove - xbelow)
                dy = float(y - ybelow) / (yabove - ybelow)
                dz = float(z - zbelow) / (zabove - zbelow)
                Q[:] = 1, dx, dy, dz, dx * dy, dy * dz, dz * dx, dx * dy * dz

                # Now, rather than interpolation at that gridpoint, we need derivative of the observed prognostic variable, w.r.t each of the corners of
                trgt_values[X_000_ind] += Q.dot(B[:, 0]) * src_values[obs_ind]
                trgt_values[X_001_ind] += Q.dot(B[:, 1]) * src_values[obs_ind]
                trgt_values[X_010_ind] += Q.dot(B[:, 2]) * src_values[obs_ind]
                trgt_values[X_011_ind] += Q.dot(B[:, 3]) * src_values[obs_ind]
                trgt_values[X_100_ind] += Q.dot(B[:, 4]) * src_values[obs_ind]
                trgt_values[X_101_ind] += Q.dot(B[:, 5]) * src_values[obs_ind]
                trgt_values[X_110_ind] += Q.dot(B[:, 6]) * src_values[obs_ind]
                trgt_values[X_111_ind] += Q.dot(B[:, 7]) * src_values[obs_ind]

            # Update the corresponding prognostic variable
            state[trgt_indexes] = trgt_values.copy()

        return state

    def __validate_grid(self, grid):
        """
        Given a grid, validate the shape, and return a two-dimensional numpy array with each row
            representing one coordinate point
        :returns: validated grid, and a list containing indexes of the prognostic variables
            (each entry is a numpy array holding indexes of one prognostic variable.)
        :raises: `TypeError` is raised if grid is not a numpy array or if the array doesn't hold
            coordinates of one/two/three dimensional cartesian system
        :remarks:
            Number of gridpoints must be more than the dimensionality of the coordinate system.
            The coordinate system dimension is the minimum of rows/columns of the passed grid
        """
        if not isinstance(grid, np.ndarray):
            print("grid must be a numpy array")
            raise TypeError
        # Make sure each row corresponds to one grid point coordinate
        if grid.ndim == 1:
            # One-dimensional cartesian coordinates
            grid = grid.reshape((grid.size, 1))
        elif grid.ndim == 2:
            # Two or Three dimensional cartesian coordinates
            space_dim = min(grid.shape)
            if 2 <= space_dim <= 3:
                # TODO: what if we have one observation point in a 2D grid, or 1 or 2 observations in 3D grid
                if (
                    grid.shape[0] == grid.shape[1]
                ):  # considering the case where we have 2 observations in 2D grid or 3 observations in 3D grid
                    print(
                        "Every row of the passed grid is considered a single point, if you want something else, please use the transpose!"
                    )
                elif (grid.shape[0] == space_dim) and (grid.shape[0] > grid.shape[0]):
                    grid = grid  # .T #TODO
            else:
                print("Only one/two/three cartesian coordinates are supported")
                print("Received grid with coordinates in {0}D".format(space_dim))
                raise TypeError

        else:
            print(
                "grid must be 1 or 2 dimensional numpy array; received array of shape {0}".format(
                    grid.shape
                )
            )
            raise TypeError

        # Extract indexes of prognostic variables
        _, unique_indexes = np.unique(grid, axis=0, return_index=True)
        unique_indexes.sort()
        incr = unique_indexes[1:] - unique_indexes[0:-1]

        if not np.all(incr[0] == incr):
            print("The grid (corresponding to state entries) has unsupported structure")
            print(
                "Either prognostic variables must be consecutive in the state vector, or number-of-gridpoints apart!"
            )
            raise TypeError

        if incr[0] == 1:
            # Prognostic variable at all gridpoints is stored in adjacent memory locations
            num_obs_gridpoints = unique_indexes.size
        else:
            # All Prognostic variables at one gridpoint are adjacent in memory
            num_obs_gridpoints = incr[0]
            if unique_indexes.size != num_obs_gridpoints:
                if self.debug:
                    print(
                        "DEBUG: This shouldn't happen; state indexes increment doesn't match unique gridpoints!"
                    )
                raise AssertionError
        num_prog_vars = np.size(grid, 0) // num_obs_gridpoints

        if num_obs_gridpoints * num_prog_vars != np.size(grid, 0):
            print("This shouldn't happen!")
            print("The grid (corresponding to state entries) has unsupported structure")
            print(
                "Model/Observation size must be equal to number of grid points x number of prognostic variables!"
            )
            raise TypeError

        # List of prognostic variables to use for mapping
        if incr[0] == 1:
            prog_vars_indexes = [
                np.arange(i * num_obs_gridpoints, (i + 1) * num_obs_gridpoints)
                for i in range(num_prog_vars)
            ]
        else:
            prog_vars_indexes = [
                np.arange(i, num_obs_gridpoints * num_prog_vars, num_prog_vars)
                for i in range(num_prog_vars)
            ]

        return grid, prog_vars_indexes

    @property
    def shape(self):
        """A tuple holding the shape of the observation operator (observation size, model state size)"""
        return self._SHAPE

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        design = np.zeros(self._OBSERVATION_SIZE, dtype=bool)
        for ind in self._OBSERVATION_PROG_INDEXES:
            design[ind] = self.design.copy()  # TODO: check?
        return design

    @property
    def design(self):
        """Get a copy of the design vector (bool representing active/inactive sensors)"""
        return self._DESIGN[self._OBSERVATION_PROG_INDEXES[0]].copy()

    @design.setter
    def design(self, design):
        """
        Update the design vector .
        The passed vector is regarded as boolean: all nonzero values are replaced with 1
        """
        if self.debug:
            print("DEBUG: In Interpolation ObsOper; received design: ", design)
        _design = np.asarray(design, dtype=bool).flatten()
        if _design.size != len(self._OBSERVATION_PROG_INDEXES[0]):
            print("The passed design vector has the wrong size!")
            print(
                "Received design of size {0}; expected size {1}".format(
                    _design.size, len(self._OBSERVATION_PROG_INDEXES[0])
                )
            )
            if self.debug:
                print("DEBUG: design", design)
            raise AssertionError
        else:
            design = _design

        # Initiate a design
        self._DESIGN = np.empty(self._OBSERVATION_SIZE, dtype=bool)
        for inds in self._OBSERVATION_PROG_INDEXES:
            self._DESIGN[inds] = design.copy()

        # Active sensors, and design variables
        active_size = np.count_nonzero(self._DESIGN)
        self._SHAPE = (active_size, self._SHAPE[1])

