# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module presents observation operators only tailored for Fenics/Dolfin-based simulation models
The module is thus optional for the PyOED package, and is provided only as an example to
enable interfacing third-party packages.
"""
import os
from dataclasses import dataclass, field, replace
import numpy as np
import scipy.sparse as sps
import warnings


try:
    import dolfin as dl
    import ufl
    from petsc4py import PETSc
except ImportError:
    dl = ufl = PETSc = None
    print(
        """\n
          \r***\n  Failed to import Fenics (dolfin);
          \rto use simulation models in this module, you must install Fenics/Dolfin first!
          \rSee information/instruction in 'optional-requirements.txt'
          \r***\n
          """
    )

from pyoed.models.core import (
    SimulationModel,
    ObservationOperator,
    ObservationOperatorConfigs,
)
from pyoed.configs import (
    validate_key,
    aggregate_configurations,
    set_configurations,
)
from pyoed import utility


@dataclass(kw_only=True, slots=True)
class DolfinPointWiseConfigs(ObservationOperatorConfigs):
    """
    Configuration class for DolfinPointWise observation operator

    :param model: the simulation model
    :param targets: 2D array with shape (n, 2) elements describing coordinates
        of the n observation/sensor points.
    :param Vh: The finite element space of the model state.
    """

    model: SimulationModel | None = None
    targets: np.ndarray | None = None
    Vh: dl.FunctionSpace | None = None


@set_configurations(DolfinPointWiseConfigs)
class DolfinPointWise(ObservationOperator):
    """
    An observation operator that interpolates Values (DOF) over a Fenics-based Mesh
    onto predefined observation (cartesian) coordinates

    :param configs: an object holding the class configurations.
    """

    def __init__(self, configs: DolfinPointWiseConfigs | dict):
        if None in (dl, ufl, PETSc):
            raise ImportError(
                "This model requires Python's package 'dolfin'!"
                + " Failed to import dolfin"
            )
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        model = self.configurations.model
        targets = self.configurations.targets

        if self.configurations.Vh is None:
            try:
                self.configurations.Vh = model.state_dof
                warnings.warn(
                    "Reading the finite element space from the passed model is now"
                    " deprecated! You must pass a valid finite element space (Vh) for"
                    " the model state to the observation operator configurations!",
                    DeprecationWarning,
                )
            except AttributeError as err:
                raise ValueError(
                    f"You must pass a valid finite element space (Vh) for the model"
                    f" state!\n See the error below\n"
                    f"{err=} of {type(err)=}"
                )

        self._BBOX_TREE = self.Vh.mesh().bounding_box_tree()

        # Note: _FULL_OBS_OPER is a scipy matrix and _OBS_OPER is a dolfin matrix
        self._FULL_OBS_OPER = self.__create_dolfin_observation_operator(
            self.Vh, self.targets
        )
        self._OBS_OPER = self.__scipy_to_dolfin(self._FULL_OBS_OPER)

        self._SHAPE = self._FULL_OBS_OPER.shape
        self._DESIGN = np.ones(self._SHAPE[0], dtype=bool)

    def __scipy_to_dolfin(self, M):
        """Convert a scipy sparse matrix to a dolfin matrix.

        :param M: A scipy sparse matrix.
        :return: A dolfin matrix.
        """
        return dl.PETScMatrix(
            PETSc.Mat().createAIJ(size=M.shape, csr=(M.indptr, M.indices, M.data))
        )

    def __create_dolfin_observation_operator(self, V, targets):
        """Create a linear operator that maps elements in V to their evaluation at
        the points in targets.

        :param V: FEniCS finite element space.
        :param targets: n x d observation points where n is the number of points and d is the dimension.

        :return: A scipy csr sparse matrix representing the observation operator.
        """
        tree = self._BBOX_TREE
        row_idx = []
        col_idx = []
        data = []
        cell_idx = [tree.compute_first_entity_collision(dl.Point(*x)) for x in targets]
        cell_global_dofs = [V.dofmap().cell_dofs(idx) for idx in cell_idx]
        for i, target in enumerate(targets):
            for local_dof, global_dof in enumerate(cell_global_dofs[i]):
                cell = dl.Cell(V.mesh(), cell_idx[i])
                row_idx.append(i)
                col_idx.append(global_dof)
                data.append(
                    V.element().evaluate_basis(
                        local_dof,
                        target,
                        cell.get_vertex_coordinates(),
                        cell.orientation(),
                    )[0]
                )

        return sps.csr_matrix(
            (data, (row_idx, col_idx)), shape=(targets.shape[0], V.dim())
        )

    def __create_dolfin_state_vector(self, u=None):
        """Create a dolfin state vector from a numpy array.

        :param u: A numpy array. If None, a zero vector is created.
        :return: A dolfin state vector.
        """
        u_dolfin = dl.Vector()
        self._OBS_OPER.init_vector(u_dolfin, 1)
        if u is not None:
            u_dolfin[:] = u
        return u_dolfin

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`DolfinPointWiseConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`DolfinPointWiseConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        if None in (dl, ufl, PETSc):
            raise ImportError(
                "This model requires Python's package 'dolfin'!"
                + " Failed to import dolfin."
            )
        aggregated_configs = aggregate_configurations(
            self, configs, self.configurations_class
        )

        model_has_Vh = hasattr(aggregated_configs.model, 'state_dof') and isinstance(aggregated_configs.model.state_dof, dl.FunctionSpace)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model",
            test=lambda x: isinstance(x, SimulationModel),
            message="Model must be an instance of SimulationModel!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="Vh",
            test=lambda x: isinstance(x, dl.FunctionSpace) or model_has_Vh,
            message="Vh must be an instance of dolfin.FunctionSpace!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="targets",
            test=lambda x: np.squeeze(np.array(x)).ndim == 2,
            message="Targets has unacceptable shape, expected (nobs,2)!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def observation_vector(self, init_val=0.0, return_np=True):
        """
        Create an observation vector
        """
        if return_np:
            return np.full(self.shape[0], init_val)
        y = dl.Vector()
        self._OBS_OPER.init_vector(y, 0)
        y[:] = init_val
        return y

    def is_observation_vector(self, observation, **kwargs):
        """Test whether the passed observation vector is valid or not"""
        valid = False
        if isinstance(observation, np.ndarray):
            if observation.size == self.shape[0]:
                valid = True
        elif isinstance(observation, dl.Vector):
            if observation.size() == self.shape[0]:
                valid = True
        return valid

    def apply(self, state, return_np=True):
        """Apply the observation operator to a model state, and return
        an observation instance
        """
        assert self.model.is_state_vector(
            state
        ), "passed state is not a valid state vector!"
        if isinstance(state, np.ndarray):
            x = self.__create_dolfin_state_vector(state)
        else:
            x = state
        y = self.observation_vector(return_np=False)
        self._OBS_OPER.mult(x, y)
        if return_np:
            y = y.get_local()
        return y

    def Jacobian_T_matvec(self, observation, eval_at=None, return_np=True):
        """
        Evaluate and return the product of the Jacobian (tangent-linear (TLM) of the
        observation operator), evaluated at `eval_at` transposed multiplied by the
        passed `observation`.
        """
        assert self.is_observation_vector(
            observation
        ), "passed observation is not a valid state vector!"

        x = self.__create_dolfin_state_vector()
        y = self.observation_vector(return_np=False)
        y[:] = observation[:]

        self._OBS_OPER.transpmult(y, x)
        if return_np:
            x = x.get_local()
        return x

    def get_observation_grid(self):
        """
        Return a copy of the observational grid
        """
        return self.targets.copy()

    @property
    def shape(self):
        """A tuple holding the shape of the observation operator (observation size, model state size)"""
        return self._SHAPE

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        return self._DESIGN.copy()

    @property
    def design(self):
        """Get a copy of the design vector (bool representing active/inactive sensors)"""
        return self._DESIGN.copy()

    @design.setter
    def design(self, design):
        """
        Update the design vector . The passed vector is regarded as boolean:
            all nonzero values are replaced with 1
        """
        design = np.asarray(design, dtype=bool).flatten()
        assert (
            design.size == self._FULL_OBS_OPER.shape[0]
        ), "The passed design vector has the wrong size!"

        active_indexes = np.where(design)[0]
        active_size = np.count_nonzero(design)
        if active_size != active_indexes.size:
            raise AssertionError("Somehow active_size != active_indexes.size")

        self._OBS_OPER = self.__scipy_to_dolfin(self._FULL_OBS_OPER[active_indexes, :])

        # Active sensors, and design variables
        self._DESIGN = design
        self._SHAPE = (active_size, self._SHAPE[1])

    @property
    def model(self):
        """The underlying simulation model"""
        return self.configurations.model

    @model.setter
    def model(self, value):
        raise NotImplementedError("Changing the underlying model is not supported.")

    @property
    def Vh(self):
        """The finite element space of the model state"""
        return self.configurations.Vh

    @Vh.setter
    def Vh(self, value):
        raise NotImplementedError("Changing the finite element space is not supported.")

    @property
    def targets(self):
        """The observation targets"""
        return self.configurations.targets

    @targets.setter
    def targets(self, targets):
        raise NotImplementedError("Changing the observation targets is not supported.")


def create_DolfinPointWise_observation_operator(
    model,
    Vh=None,
    targets=None,
    num_obs_points=20,
    offset=0.1,
    exclude_boxes=[[(0.25, 0.150), (0.50, 0.400)], ((0.60, 0.625), (0.75, 0.850))],
    verbose=False,
):
    """
    A wrapper around :py:class:`DolfinPointWise`.
    This function create a pointwise observation operator for Dolfin-based model.

    :param model: the simulation model
    :param Vh: The finite element space of the model state.
    :param targets: 2D array with shape (n, 2) elements describing coordinates
        of the n observation/sensor points. If None, a set of targets will be
        created using the following arguments
    :param int num_obs_points: (approximate) number of observation points (sensors).
        This number will mostly change especially if there are buildings to avoid
    :param offset: margin/offset to leave between boundary and extreme observation points
    :param exclude_boxes: either None, or a list of tuples; each tuple must be
        a pair of tuples (xl, yl), (xu, yu) to exclude points in the box
        with corners set to these coordintes.
        This simulates buildings in the domain.
    :param bool verbose: screen verbosity

    :returns: an observation operator; an instance of :py:class:`DolfinPointWise`
    """
    if targets is None:
        # define observation operator, and observations locations
        ntargets = num_obs_points
        n_rows = int(np.sqrt(num_obs_points))
        n_cols = num_obs_points // n_rows

        n_obs = 0
        while n_obs < num_obs_points:
            _candidates = []
            x_coords = np.linspace(offset, 1 - offset, num=n_rows)
            y_coords = np.linspace(offset, 1 - offset, num=n_cols)

            for x in x_coords:
                for y in y_coords:
                    # Check the coordinate (x, y) and add if not inside the boxes
                    in_box = False
                    if exclude_boxes is not None:
                        for coord in exclude_boxes:
                            (xl, yl), (xu, yu) = coord
                            if xl < x < xu and yl < y < yu:
                                in_box = True
                                break
                    if not in_box:
                        _candidates.append([x, y])
                        n_obs = len(_candidates)

                    if n_obs == num_obs_points:
                        break

                if n_obs == num_obs_points:
                    break

            # If not enough add more rows and columns (more dense)
            n_rows += 1
            n_cols += 1

        targets = np.asarray(_candidates).squeeze()
        if targets.shape != (num_obs_points, 2):
            print("This should never happen!")
            print("Targets shape: ", targets.shape)
            print("n_obs: ", n_obs)
            print("num_obs_points: ", num_obs_points)
            raise ValueError

    else:
        targets = np.asarray(targets)
        assert np.ndim(targets) == 2, "targets must be a 2D array"
        targets_shape = targets.shape
        assert targets_shape[1] == 2, "targets must be of shape (n, 2)!"
        ntargets = targets_shape[0]

        if targets_shape[0] != num_obs_points:
            print(
                "NOTE: number of observation points in the passed array is"
                f" {targets_shape[0]}"
            )
            print(
                "      This is different from the value set in teh argument"
                f" 'num_obs_points' {num_obs_points}"
            )

    if verbose:
        msg = "***************Observation Coordinates:***************"
        print(msg)
        print("Number of observation points: %d " % ntargets)
        print("Coordinates:")
        print(targets)
        print("*" * len(msg))

    # Create & return the observation operator
    observation_operator = DolfinPointWise(
        DolfinPointWiseConfigs(
            model=model,
            targets=targets,
            Vh=Vh or model.state_dof,
        )
    )
    return observation_operator


## Add alias for backward compatibility
DolfinPointWiseObservations = DolfinPointWise
create_pointwise_observation_operator = create_DolfinPointWise_observation_operator
