# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides implementation(s) of time-dependent observation operators.
An observation operator that is time dependent can have different settings
(e.g., time-varying design) at each observation time instance.
"""

from dataclasses import dataclass
import numpy as np

from pyoed.models.core import (
    ObservationOperator,
    ObservationOperatorConfigs,
    SimulationModel,
)
from pyoed.configs import (
    validate_key,
    set_configurations,
)
from pyoed import utility


@dataclass(kw_only=True, slots=True)
class TimeDependentObservationOperatorConfigs(ObservationOperatorConfigs):
    """
    :py:class:`TimeDependentObservationOperatorConfigs` holds configurations for
    a time-dependent observation operator.

    :param base_observation_operator: an instance of an observation operator to be
        replicated.
    :param observation_times: an iterable hodling increasing observation time.
    :param t_eps: a small value to compare time instances for equality
    """

    base_observation_operator: ObservationOperator | None = None
    observation_times: np.ndarray | None = None
    t_eps: float = 1e-10

    def __post_init__(self):
        if self.observation_times is not None:
            self.observation_times = np.unique(self.observation_times)
            self.observation_times.sort()


@set_configurations(TimeDependentObservationOperatorConfigs)
class TimeDependentObservationOperator(ObservationOperator):
    """
    :py:class:`TimeDependentObservationOperator` takes a base observation operator and
    time instances and create replicas of the base observation operator for each time
    instance.  After registration, each operator (at each time instance) can be
    manipulated independently; e.g., modify the observation noise model, observation
    design, etc.  The observation operator at each time instance can be retrieved, and
    can be used to apply standard observation operator methods by specifying the
    corresponding time.

    :raises:
        - :py:class:`TypeError` is raised if the `based_observation_operator` in
          `configs` is not an instance of
          :py:class:`pyoed.models.core.ObservationOperator`
    """

    def __init__(
        self, configs: TimeDependentObservationOperatorConfigs | dict | None = None
    ):
        # Validate and Assure Access to super attributes!
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Data container; dictionary keyd by times with values being observation operators
        self._DATA = {}

        # For each time instance, register an observation operator from the base
        # operator
        base_observation_operator = self.configurations.base_observation_operator
        observation_times = np.unique(self.configurations.observation_times)
        for t in observation_times:
            self.register_observation_operator(t, )

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`TimeDependentObservationOperatorConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`TimeDependentObservationOperatorConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Local validation tests
        def is_valid_times(observation_times):
            if not utility.isiterable(observation_times):
                return False
            if any([not utility.isnumber(t) or t<0 for t in observation_times]):
                return False
            else:
                return True

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="base_observation_operator",
            test=lambda x: isinstance(x, ObservationOperator),
            message="Model must be an instance of SimulationModel!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observation_times",
            test=lambda x: x is not None,
            message="observation_times must be passed!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Check if `random_seed' is passed
        if 'observation_times' in kwargs:
            self.configurations.observation_times = np.unique(kwargs['observation_times'])
            self._DATA = {}
            for t in observation_times:
                self.register_observation_operator(t, )
        if 'base_observation_operator' in kwargs:
            base_observation_operator = kwargs['base_observation_operator']
            self._DATA = {}
            for t in observation_times:
                self.register_observation_operator(t, )

    def register_observation_operator(
        self, t, observation_operator=None, overwrite=False
    ):
        """
        Given an `observation operator` instance/vector and the associated time `t`,
        update observation operator information

        :param float t: time at which the passed `observation` is registered
        :param observation_operator: (optional) observation operator instance to
            assign at the passed observation time.
            If `None`, the registerd base observation operator is copied at this
            observation time instance.
        :param bool overwrite: overwrite an existing observation operator if already
            registered at the passed time

        :raises:
            - :py:class:`ValueError` is raised if `overwrite` is `False` and a valid observation
              operator is registerd at the passed time `t`.
            - :py:class:`TypeError` is raised if the observation operator passed
              is not  a valid type, if not an instance of
              :py:class:`pyoed.models.observation_operators.ObservationOperator`.
        """
        # Rounding the time to the registered time precision
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
        t = np.round(t, prec)

        # Check if observation is previously adde to the data dictionary
        checked_t = self.find_observation_time(t)
        if checked_t is not None and not overwrite:
            raise ValueError(
                f"An observation is already registered at time t={t}\n"
                f"Set overwrite flag to True if you want to replace/overwrite"
                f" observation at this time"
            )

        if observation_operator is None:
            observation_operator = self.base_observation_operator.copy()

        if not isinstance(observation_operator, ObservationOperator):
            raise TypeError(
                f"The passed base observation operator must be derived from "
                f"`pyoed.models.observtion_operators.ObservationOperator`; "
                f"received {type(observation_operator)=}!"
            )

        # All good; add the observation
        self._DATA.update({t: observation_operator})

    def find_observation_time(self, t, time_keys=None):
        """
        Local function to find the key in `self._DATA` reresenting time equal to
        or within `t_eps` of the passed time
        """
        if time_keys is not None:
            time_keys = np.asarray(time_keys).flatten()
        else:
            time_keys = np.asarray([k for k in self._DATA.keys()])
        if len(time_keys) == 0:
            found_t = None
        else:
            locs = np.where(np.abs(time_keys - t) <= self.t_eps)[0]
            if locs.size == 0:
                found_t = None
            elif locs.size == 1:
                found_t = time_keys[locs[0]]
            else:
                raise ValueError(
                    "Found multiple time instances equal, or very close, to the passed"
                    " time\nThis should never happen!"
                )

        return found_t

    def retrieve_observation_operator(self, t):
        """
        Retrieve and return the observation operator (if any) registerd at
        the passed time `t`. If no operator is registered at this time,
        a :py:class:`ValueError` is raise
        """
        _t = t  # for error raising
        t = self.find_observation_time(t=t)
        if t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[t]
        return obs_oper

    def observation_vector(self, _t, init_val=0.0):
        """
        Create an observation vector

        :param float t: observation time.
        :param float init_val: (optional) value assigned to entries of the state
            vector upon initialization.
        :returns: an observation vector identical to state vectors generated by
            the underlying model.
        """
        _t = self.find_observation_time(t=_t)
        if _t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[_t]
        return obs_oper.observation_vector(init_val=init_val)

    def is_observation_vector(self, t, observation):
        """
        Test whether the passed observation vector is valid or not

        :param observation: an observation vector
        :param float t: observation time.
        """
        _t = t  # for error raising
        t = self.find_observation_time(t=t)
        if t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[t]
        return obs_oper.is_observation_vector(observation)

    def get_observation_grid(self, t):
        """
        Return a copy of the observational grid
        :param float t: observation time.
        """
        _t = t  # for error raising
        t = self.find_observation_time(t=t)
        if t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[t]
        return obs_oper.get_observation_grid()

    def apply(
        self,
        t,
        state,
    ):
        """
        Given a state vector (laid out as defined by the model grid passed upon
        instantiation), evaluate the observation vector by applying interpolation

        :param float t: observation time.
        :param state: instance of a model state with entries corresponding the model
            grid passed upon instantiation

        :returns: an observation vector/instance

        :raises: `TypeError` is raised if state has the wrong shape,

        :notes:
          - The returned value will be nan if the power is non-integral and any
            of the entries in `state` is negative.
        """
        _t = t  # for error raising
        t = self.find_observation_time(t=t)
        if t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[t]
        return obs_oper.apply(state)

    def Jacobian_T_matvec(self, t, observation, eval_at=None):
        """
        Multiply (Matrix-free matrix-vector product) of *transpose* of the
        Jacobian/tangent-linear (TLM) of the observation operator by the passed
        observation; The Jacobian is evaluated (for nonlinear operators) at `eval_at`

        :param float t: observation time.
        :param observation: observation instance/vector
        :param eval_at: state around which observation operator is linearized

        :returns: result of multiplying the transpose of derivative of the observation
            operator by the passed observation
        """
        _t = t  # for error raising
        t = self.find_observation_time(t=t)
        if t is None:
            self._raise_time_not_found_error(_t)
        else:
            obs_oper = self._DATA[t]
        return obs_oper.Jacobian_T_matvec(
            observation=observation,
            eval_at=eval_at,
        )

    @staticmethod
    def _raise_time_not_found_error(t):
        raise ValueError(
            f"There is nothing registerd at the time instance; {t=}!\n"
            f"Please pass a valid time instance or register an observation operator\n"
            f"at this time instance by calling:\n"
            f"`self.register_observation_operator(t, observation_operator)`"
        )

    @staticmethod
    def _raise_functionality_not_available_error():
        raise NotImplementedError(
            f"This functionality/property is not accessible for time-dependent observation "
            f"operators.\n"
            f"You can retrieve it from the observation operator assigned to a specific time.\n"
            f" You can use :py:meth`self.retrieve_observation_operator(t)`,"
            f"and then retrieve the property for that specific instance!"
        )

    @property
    def shape(self):
        self._raise_functionality_not_available_error()

    @property
    def extended_design(self):
        design = {}
        for t in self.observation_times:
            design.update({t: self.retrieve_observation_operator(t=t).extended_design, })
        return design

    @property
    def design(self):
        """
        Retrieve a dictionary of designs indexed by the time instances.
        The designs are compied from the observation operators at each time point.
        """
        design = {}
        for t in self.observation_times:
            design.update({t: self.retrieve_observation_operator(t=t).design, })
        return design

    @design.setter
    def design(self, val):
        """
        Update the design for each observation operator registerd at each time instance.

        :param val: the value of the new design to be used for updating all
            registered observation operators.
            The `val` is allowed to be either 1d numpy array (which is acceptable by each)
            observation operator, or a dictionary keyed by times.
            In the former (the same design is set for all registered observation operators),
            while in the latter, each design will be registered with the observation operator
            corresponding the time specifying the key of this element in the dictionary.

        :raises TypeError: if the passed design is of wrong type
        :raises TypeError: if no observation operators have been registered (and valid design is passed)
        """
        if isinstance(val, dict):
            if set(list(val.keys())) == set(self.observation_times):
                if len(self.observation_times) == 0:
                    warnings.warn(
                        f"No registered observation operators, and no valid design passed."
                        f"Nothing to do here!"
                    )
                for t in self.observation_times:
                    obs_oper = self.retrieve_observation_operator(t)
                    if obs_oper is not None:
                        obs_oper.design = val[t]
                    else:
                        raise TypeError(
                            f"No observation operator registered at time {t=}"
                        )
            else:
                raise TypeError(
                    f"The passed dictionary has time instances inconsistent with registered"
                    f" observation operators/times."
                )

        else:
            if self.observation_times is None:
                raise TypeError(
                    f"No observation operators are registered. "
                    f"Can't set the design before setting observation operators!"
                )

            for t in self.observation_times:
                obs_oper = self.retrieve_observation_operator(t)
                if obs_oper is not None:
                    obs_oper.design = val
                else:
                    raise TypeError(
                        f"No observation operator registered at time {t=}"
                    )

    @property
    def observation_times(self):
        """Return a numpy array holding registered obserevation times"""
        return self.configurations.observation_times

    @property
    def base_observation_operator(self):
        """Return the registerd base observation operator (in the configurations
        dictionary)"""
        return self.configurations.base_observation_operator

    @property
    def t_eps(self):
        """Return the time precision"""
        return self.configurations.t_eps


# =================================================================================== #
# Example Functions to help inistantiate instances of the operators implemented here  #
# =================================================================================== #
def create_time_dependent_observation_operator(
    base_observation_operator, observation_times
):
    """
    Create a time-dependent observation operator from a base operator.

    :param base_observation_operator: an instance of an observation operator
        :py:class:`pyoed.models.observation_operators.ObservationOperator`
    :param observation_times: time instances at which to replicate the observaiton operator

    :returns: an instance of :py:class:`TimeDependentObservationOperator` inistantiated
        based on the passed arguments.
    """
    return TimeDependentObservationOperator(
        configs={
            "base_observation_operator": base_observation_operator,
            "observation_times": observation_times,
        }
    )
