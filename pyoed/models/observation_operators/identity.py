# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Class implementing identity observation operator. This is added for compatibility.
All functions/methods return a copy of the states passed as arguments
"""

import numpy as np
from dataclasses import dataclass, field

from pyoed.models.core import (
    ObservationOperator,
    ObservationOperatorConfigs,
    SimulationModel,
)
from pyoed import utility
from pyoed.configs import (
    validate_key,
    set_configurations,
    aggregate_configurations,
)



@dataclass(kw_only=True, slots=True)
class IdentityConfigs(ObservationOperatorConfigs):
    """
    Configuration class for the identity observation operator.

    :param model: an instance of a simulation model derived from
        :py:class:`pyoed.models.SimulationModel`.
    """

    model: SimulationModel | None = None


@set_configurations(IdentityConfigs)
class Identity(ObservationOperator):
    """
    Identity observation operator added for compatibility.  All functions/methods return
    a copy of the states passed as arguments In addition to standard functionality, here
    we provide a specific attribute 'design' which enables modifying the observation
    space by projection/selection\n
        - If the design is a binary vector (bool or int dtype attributes with 0/1
          entries), the observation operator restricts observations to only entries
          identified by the 1/True entries.  This acts like sensor placing/activation
          where 1 means ON/ACTIVE, and 0 means OFF.
        - If the design contains non-binary values, it is transformed to boolean flag;
          any entry in the design variable that's not zero is set to 1.

    :raises TypeError: if the passed model is not an instance of
        :py:class:`pyoed.models.SimulationModel`.
    :raises NotImplementedError: if the passed model simulates more than one prognostic
        variable; extensions require proper handling of the design which is on our TODO
        list.

    :remarks:
        - Currently, this implementation assumes a single prognostic variable; that is,
          the design does not repeat itself on the covariance matrix.
          This will be provided as needed!
    """

    # TODO: Update the code with proper extended design variable which handles
    #       the case of multiple prognostic variables. See interpolation.py for example!
    def __init__(self, configs: IdentityConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        model = self.configurations.model

        model_grid = model.get_model_grid()
        model_grid = np.squeeze(model_grid)
        if model_grid.ndim <= 1:
            model_grid = model_grid.flatten()

        model_grid, model_prog_vars_indexes = utility.validate_Cartesian_grid(
            model_grid,
        )
        observation_grid = model_grid
        observation_prog_vars_indexes = model_prog_vars_indexes

        # All good; associate the model and observation grid
        self._MODEL_GRID = model_grid
        self._OBSERVATION_GRID = observation_grid
        self._MODEL_PROG_INDEXES = model_prog_vars_indexes  # list of indexes of each prognostic variable in the model grid
        self._OBSERVATION_PROG_INDEXES = observation_prog_vars_indexes  # list of indexes of each prognostic variable in the observation grid

        self._MODEL_STATE_SIZE = np.size(self._MODEL_GRID, 0)
        self._OBSERVATION_SIZE = np.size(self._OBSERVATION_GRID, 0)
        self._SHAPE = (self._OBSERVATION_SIZE, self._MODEL_STATE_SIZE)

        # Initiate a design
        _design = np.ones(
            self._OBSERVATION_SIZE // len(observation_prog_vars_indexes), dtype=bool
        )
        self._DESIGN = np.empty(self._OBSERVATION_SIZE, dtype=bool)
        for inds in self._OBSERVATION_PROG_INDEXES:
            self._DESIGN[inds] = _design.copy()

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`IdentityConfigs` are validated.  Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise an error for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`IdentityConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model",
            test=lambda x: isinstance(x, SimulationModel),
            message="Model must be an instance of SimulationModel!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def observation_vector(self, init_val=0.0):
        """
        Create an observation vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        :returns: an observation vector identical to state vectors generated by the underlying model
        """
        return utility.asarray(self.model.state_vector(init_val=init_val))[self._DESIGN]

    def is_observation_vector(self, observation):
        """
        Test whether the passed observation vector is valid or not

        :param observation: an observation vector
        """
        valid = False
        if isinstance(observation, np.ndarray):
            valid = observation.size == self.shape[0]
        return valid

    def get_observation_grid(self):
        """
        Return a copy of the observational grid
        """
        return self._OBSERVATION_GRID.copy()

    def apply(self, state):
        """
        Given a state vector (laid out as defined by the model grid passed upon instantiation),
        evaluate the observation vector by applying interpolation

        :param state: instance of a model state with entries corresponding the model grid passed upon instantiation
        :returns: an observation vector/instance
        :raises: `TypeError` is raised if state has the wrong shape,
        """
        if not self.model.is_state_vector(state):
            print(
                "The passed state is not a valid model state or is incompatible with the underlying simulation model"
            )
            raise TypeError
        return state[self._DESIGN].copy()

    def Jacobian_T_matvec(self, observation, eval_at=None):
        """
        Multiply (Matrix-free matrix-vector product) of *transpose* of the Jacobian/tangent-linear (TLM)
        of the observation operator by the passed observation;
        The Jacobian is evaluated (for nonlinear operators) at `eval_at`

        :param observation: observation instance/vector
        :param eval_at: state around which observation operator is linearized; ignored here since :math:`\\mathbf{I}` is linear

        :returns: result of multiplying the transpose of derivative of the observation operator by the passed observation

        """
        proj_observation = np.zeros(self._SHAPE[1], dtype=np.double)
        proj_observation[self._DESIGN] = observation.copy()
        return proj_observation

    @property
    def shape(self):
        """A tuple holding the shape of the observation operator (observation size, model state size)"""
        return self._SHAPE

    # TODO: This was duplicated, the second one seems to be the correct one
    # @property
    # def extended_design(self):
    #    """
    #    Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
    #    of the observation vector/range
    #    """
    #    return self._DESIGN.copy()

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        design = np.zeros(self._OBSERVATION_SIZE, dtype=bool)
        for ind in self._OBSERVATION_PROG_INDEXES:
            design[ind] = self.design.copy()  # TODO: check?
        return design

    @property
    def design(self):
        """Get a copy of the design vector (bool representing active/inactive sensors)"""
        return self._DESIGN[self._OBSERVATION_PROG_INDEXES[0]].copy()

    @design.setter
    def design(self, design):
        """
        Update the design vector .
        The passed vector is regarded as boolean: all nonzero values are replaced with 1
        """
        if self.debug:
            print("DEBUG: In Interpolation ObsOper; received design: ", design)
        _design = np.asarray(design, dtype=bool).flatten()
        if _design.size != len(self._OBSERVATION_PROG_INDEXES[0]):
            print("The passed design vector has the wrong size!")
            print(
                "Received design of size {0}; expected size {1}".format(
                    _design.size, len(self._OBSERVATION_PROG_INDEXES[0])
                )
            )
            if self.debug:
                print("DEBUG: design", design)
            raise AssertionError
        else:
            design = _design

        # Initiate a design
        self._DESIGN = np.empty(self._OBSERVATION_SIZE, dtype=bool)
        for inds in self._OBSERVATION_PROG_INDEXES:
            self._DESIGN[inds] = design.copy()

        # Active sensors, and design variables
        active_size = np.count_nonzero(self._DESIGN)
        self._SHAPE = (active_size, self._SHAPE[1])

    @property
    def model(self):
        """The underlying simulation model"""
        return self.configurations.model

    @model.setter
    def model(self, value):
        raise NotImplementedError(
            "You are not supposed to change the underlying model after inistantiation!"
        )


# =================================================================================== #
# Example Functions to help inistantiate instances of the operators implemented here  #
# =================================================================================== #
def create_Identity_observation_operator(model):
    """
    Create an identity observation operator from a simulation model.
    The observation operator is identity, so it just mirrors a model state.

    :param model: a simulation model object; instance of :py:class:`SimulationModel`

    :returns: an instance of :py:class:`Identity` inistantiated based on the
        passed simulation model
    """
    return Identity(IdentityConfigs(model=model))
