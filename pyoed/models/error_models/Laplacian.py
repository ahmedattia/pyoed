# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module contains a set of classes/functions to enable utilizaiton of Laplacian-like
priors (error models) to fenics-based inverse problems and simulation models.
"""
import re
import warnings
from dataclasses import dataclass

import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import LinearOperator, isolve, spsolve_triangular

from pyoed.models.core import (
    ErrorModel,
    ErrorModelConfigs,
)
from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    set_configurations,
    validate_key,
    aggregate_configurations,
)


try:
    import dolfin as dl
    import ufl
except ImportError:
    dl = ufl = None  # TODO: Either this or just raise ImportError (as suggested below!)
    warnings.warn(
        "The Laplacian module is loaded, however it won't be possible to actually use it witout Fenics/Dolfin installed!"
    )


# Global (ish) variables
_LINSOLVE_TOL = 1e-12  # Base tolerance of linear system solves
_RANDOMIZATION_SAMPLE_SIZE = 50  # 250


@dataclass(kw_only=True, slots=True)
class DolfinLaplacianErrorModelConfigs(ErrorModelConfigs):
    """
    Configurations for :py:class:`DolfinLaplacianErrorModel`

    :param Vh: finite element space to which the distribution is assocciated
    :param gamma: positive number (float) that controls the variance/uncertainty level
    :param delta: positive number (float) such that `gamma / delta` controls the
        correlation length (lengthscale),
    :param mean: float or iterable of floats (numpy array, or dolfin vector) of the
        error model;
    :param random_seed: random seed used when the Gaussian model is initiated.
        This is useful for reproductivity. If `None`, random seed follows
        `numpy.random.seed` rules
    """

    Vh: dl.FunctionSpace | None = None
    gamma: float = 1.0
    delta: float = 0.5
    mean: float | np.ndarray | dl.Vector = 0.0
    random_seed: int | None = None


@set_configurations(DolfinLaplacianErrorModelConfigs)
class DolfinLaplacianErrorModel(ErrorModel, RandomNumberGenerationMixin):
    """
    A class implementing the simplest Error model with a Laplacian-based covariance model.
    The covariance matrix takes the form :math:`C = (\\delta I - \\gamma \\Delta) ^ {-1}`,
    where :math:`\\gamma / \\delta` controls the correlation length (lengthscale),
    :math:`\\gamma` controls the variance/uncertainty level, and
    :math:`\\Delta` is the Laplacian operator.

    Note that the covariance operator :math:`C` is a trace-class only in 1D-problems,
    however, it is not valid for > 1D problems.

    :param configs: settings of the error model.

    :raises:
        - `ImportError`: if dolfin or ufl are not available/installed
        - `TypeError`: if mean type is not supported (scalars and iterables are supported)
        - `ValueError`: if no valid FEM space (DOF) `Vh` is found in the configurations dictionary
        - `ValueError`: if `gamma` or `delta` are non-positive
    """

    def __init__(self, configs: dict | DolfinLaplacianErrorModelConfigs | None = None):
        if None in (dl, ufl):
            msg = "This class can be used only with dolfin and UFL installed. "
            msg += "Failed to load/import these modules."
            raise ImportError(msg)
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        # Extract configurations
        Vh = self.Vh
        mean = self.mean
        random_seed = self.configurations.random_seed
        gamma, delta = self.configurations.gamma, self.configurations.delta

        # Create variance/covariance operator and the square root (STDEV), and solvers
        trial = dl.TrialFunction(Vh)
        test = dl.TestFunction(Vh)

        # Mass
        varf_M = ufl.inner(trial, test) * ufl.dx
        self._M = dl.assemble(varf_M)
        self._M_SOLVER = self._create_solver(self._M)

        self._PREC = None
        self._PREC_SOLVER = None

        # Do these ever get used?
        self._COV = None
        self._COV_SOLVER = None

        # Create Covariances operator and related solver(s)
        self.update_covariance_matrix(gamma, delta)
        # Check the passed mean, and associate to self
        self.update_mean(mean=mean)

        # Update/reset the associated random number generator/seed
        self.update_random_number_generator(random_seed=random_seed)

    def _create_solver(
        self,
        mat,
        solver="dl-lu",
        preconditioner="ml_amg",
        rel_tol=1e-15,
        abs_tol=1e-18,
        error_on_nonconvergence=True,
        nonzero_initial_guess=False,
    ):
        """
        Initialize the solvers and assign operators and preconditioners;
        A solver is created and associated only with valid input

        :param mat: matrix to create a solver for.
        :param solver: solver to use (default: dl-lu)
        :param preconditioner: preconditioner to use (default: ml_amg)
        :param rel_tol: relative tolerance for the solver (default: 1e-15)
        :param abs_tol: absolute tolerance for the solver (default: 1e-18)
        :param error_on_nonconvergence: raise an error if the solver does not converge (default: True)
        :param nonzero_initial_guess: use a non-zero initial guess (default: False)
        """
        if re.match(r"\Apetsc(-| |_)lu\Z", solver, re.IGNORECASE):
            mat_solver = dl.PETScLUSolver()
            mat_solver.set_operator(mat)
        elif re.match(r"\Adl(-| |_)lu\Z", solver, re.IGNORECASE):
            mat_solver = dl.LUSolver()
            mat_solver.set_operator(mat)
        elif re.match(r"\Apetsc(-| |_)Krylov", solver, re.IGNORECASE):
            # Preconditioner
            if preconditioner not in dl.krylov_solver_preconditioners():
                warnings.warn(
                    f"Preconditioner '{preconditioner}' is not recognized; using 'petsc_amg' instead"
                )
                preconditioner = "petsc_amg"
            mat_solver = dl.PETScKrylovSolver("cg", preconditioner)
            mat_solver.set_operator(mat)
            mat_solver.parameters["relative_tolerance"] = rel_tol
            mat_solver.parameters["absolute_tolerance"] = abs_tol
            mat_solver.parameters["error_on_nonconvergence"] = error_on_nonconvergence
            mat_solver.parameters["nonzero_initial_guess"] = nonzero_initial_guess
        else:
            msg = f"Unrecognized solver '{solver}'"
            msg += "Currently, we support LU and PETSc-Krylov solvers"
            raise ValueError(msg)
        return mat_solver

    def validate_configurations(
        self,
        configs: dict | DolfinLaplacianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the DolfinLaplacian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`DolfinLaplacianErrorModelConfigs`.
        """
        aggregated_configs = self.aggregate_configurations(configs)

        if not validate_key(
            aggregated_configs,
            configs,
            "Vh",
            lambda x: isinstance(x, dl.FunctionSpace),
            message="Vh must be a valid FEM space",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        for p in ["gamma", "delta"]:
            if not validate_key(
                aggregated_configs,
                configs,
                p,
                lambda x: utility.isnumber(x) and x > 0,
                message=f"{p} must be a positive number",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        def mean_test(x):
            if utility.isnumber(x):
                return True
            elif isinstance(x, np.ndarray):
                dim = x.size
            elif isinstance(x, dl.Vector):
                dim = x.size()
            return dim == aggregated_configs.Vh.dim()

        if not validate_key(
            aggregated_configs,
            configs,
            "mean",
            mean_test,
            message="mean must be a scalar or an array conformant to Vh",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_covariance_matrix(self, gamma, delta, validate=False):
        """
        Define the Variational forms of the covariance, precision, and factors of the
        covariance matrix, and assocate to self. This enables updating covariances
        consistently

        :param gamma: positive number (float) that controls the variance/uncertainty
            level
        :param delta: positive number (float) such that `gamma / delta` controls the
            correlation length (lengthscale),
        :param bool validate: validate the covariance parameters before updating.
        :raises PyOEDConfigsValidationError: if the covariance parameters are invalid
            and `validate` is set to `True`
        """
        if validate:
            self.validate_configurations(
                {"gamma": gamma, "delta": delta}, raise_for_invalid=True
            )

        # Create variance/covariance operator and the square root (STDEV), and solvers
        trial = dl.TrialFunction(self.Vh)
        test = dl.TestFunction(self.Vh)

        # Precisions (Covariance Inverse)
        varf_PREC = (
            gamma * ufl.inner(ufl.grad(trial), ufl.grad(test)) * ufl.dx
            + delta * ufl.inner(trial, test) * ufl.dx
        )
        self._PREC = dl.assemble(varf_PREC)
        self._PREC_SOLVER = self._create_solver(self._PREC)

        # Square Root-Operator (inverse)
        # TODO: This code has got to get updated

        # Update dl form compiler parameters
        dl_form_compiler = dl.parameters["form_compiler"]
        orig_representation = dl_form_compiler["representation"]
        orig_quadrature_degree = dl_form_compiler["quadrature_degree"]
        orig_element_degree = self.Vh.ufl_element().degree()
        dl_form_compiler.update(
            {"quadrature_degree": -1, "representation": "quadrature"}
        )

        # Define FE element
        new_element_degree = 2 * orig_element_degree
        mesh = self.Vh.mesh()
        physics_ndim = mesh.geometry().dim()
        quad_element = ufl.VectorElement(
            family="Quadrature",
            cell=mesh.ufl_cell(),
            degree=new_element_degree,
            dim=physics_ndim + 1,
            quad_scheme="default",
        )
        Qh = dl.FunctionSpace(mesh, quad_element)
        ph = dl.TrialFunction(Qh)
        qh = dl.TestFunction(Qh)

        ufl_dx = ufl.dx(metadata=dict(quadrature_degree=new_element_degree))
        Mqh = dl.assemble(ufl.inner(ph, qh) * ufl_dx)
        ones = dl.Vector()

        Mqh.init_vector(ones, 0)
        ones_np = ones.get_local()
        ones_np[:] = 1
        ones.set_local(ones_np)

        dMqh = Mqh * ones
        dMqh_np = dMqh.get_local()
        dMqh.set_local(ones_np / np.sqrt(dMqh_np))

        Mqh.zero()
        Mqh.set_diagonal(dMqh)

        pph = ufl.split(ph)
        varf_G = np.sqrt(delta) * pph[0] * test * ufl_dx
        for i in range(physics_ndim):
            varf_G += np.sqrt(gamma) * pph[i + 1] * test.dx(i) * ufl_dx
        G = dl.assemble(varf_G)

        # Inverse of the square root of the covariance
        G_mat = dl.as_backend_type(G).mat()
        Mqh_mat = dl.as_backend_type(Mqh).mat()
        STD_INV_mat = G_mat.matMult(Mqh_mat)
        rmap, _ = G_mat.getLGMap()
        _, cmap = Mqh_mat.getLGMap()
        STD_INV_mat.setLGMap(rmap, cmap)
        # self._STDEV_INV = dl.Matrix(STD_INV_mat)
        self._STDEV_INV = dl.Matrix(dl.PETScMatrix(STD_INV_mat))

        self._NOISE_SIZE = self._STDEV_INV.size(1)

        # Reset form compiler parameters
        dl_form_compiler.update(
            {
                "quadrature_degree": orig_quadrature_degree,
                "representation": orig_representation,
            }
        )

    def update_mean(self, mean, validate=False):
        """
        Update the mean of the distribution

        :param mean: mean of the distribution
        :param bool validate: validate the mean before updating.
        :raises PyOEDConfigsValidationError: if the mean has a type/size mismatch and
            `validate` is set to `True`
        """
        if validate:
            self.validate_configurations({"mean": mean}, raise_for_invalid=True)

        # Check mean (type and size) and variance data types and sizes/shapes
        if utility.isnumber(mean) or isinstance(mean, np.ndarray):
            mean = np.array(mean).flatten()  # local copy
        elif sp.issparse(mean):  # When are we expecting a sparse mean?
            mean = mean.toarray().flatten()  # local copy
        elif isinstance(mean, dl.Vector):
            mean = mean.get_local()  # this might be redundant, but let's be consistent
        else:
            raise TypeError("`mean` is of unsupported type: {0}".format(type(mean)))

        # Associate Mean attributes; allow changing later by properties
        # 1- Mean (convert from np array to dl.Vector and set entries)
        _mean = self.create_vector(return_np=False)
        _mean.set_local(mean)
        self.configurations.mean = _mean

    def update_design(self, design):
        """
        Update the design vector (which entries are active)
        """
        raise NotImplementedError(
            "Observartional/Experimental Design is not supported for this error model!"
        )

    def create_noise_vector(self, return_np=True):
        """
        Create a vector compatible with the domain of the underlying covariance (SQRT) operator
        """
        vec = dl.Vector()
        self._STDEV_INV.init_vector(vec, 1)
        if return_np:
            vec = vec.get_local()
        return vec

    def create_vector(self, return_np=True):
        """
        Create a vector compatible with the range of the underlying covariance operator
        """
        try:
            _ = self._PREC
        except AttributeError:
            print("Failed to locate the precision operator.")
            print(
                "You need to initiate covariances by calling `update_covariance_matrix()` first!)"
            )
            raise

        vec = dl.Vector()
        self._PREC.init_vector(vec, 0)
        if return_np:
            vec = vec.get_local()
        return vec

    def generate_white_noise(self, truncate=False, return_np=False):
        """
        Generate a standard normal random vector of size `size` with values truncated
            at -/+3 if `truncate` is set to `True`

        :param bool truncate: if `True`, truncate the samples at -/+3, that is any sample
            point/entry above 3 is set to 3, and any value below -3 is set to -3.
        :param bool return_np: if `True`, convert the result into a numpy array (from fenics vector)

        :returns: numpy array (even if the dimension is 1) containing white noise
        """
        # Sample
        white_noise = self.random_number_generator.standard_normal(self.noise_size)

        # Truncate if requested
        if truncate:
            white_noise[white_noise > 3] = 3
            white_noise[white_noise < -3] = -3

        # Convert to Fenics vector (unless `return_np`==`True`)
        if not return_np:
            _white_noise = self.create_noise_vector(
                return_np=False,
            )
            _white_noise.set_local(white_noise)
            white_noise = _white_noise
        return white_noise

    def add_noise(self, x, in_place=False):
        """
        Add random noise to the passed vector `x`

        :param x: vector in a space of dimension equal to size of the undrlying error model
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :raises: TypeError if x is of the wrong shape or type
        """
        # Check type of the passed vector
        if isinstance(x, np.ndarray):
            x_size = x.size
        elif isinstance(x, dl.Vector):
            x_size = x.size()
        else:
            raise TypeError("Unsuported type '{0}' of passed vector x".format(type(x)))

        # Check size of the passed vector
        if self.size != x_size:
            raise TypeError(
                "Size mismatch; expected vector of size {0}; received vectror of size {1}".format(
                    self.size, x_size
                )
            )

        out_vec = x if in_place else x.copy()

        # Scale and shift the white noise
        if isinstance(x, np.ndarray):
            out_vec += self.generate_noise(return_np=True)
        else:
            noise = self.generate_noise(return_np=False)
            out_vec.axpy(1.0, noise)

        return out_vec

    def generate_noise(self, return_np=True):
        """
        Generate a random noise vector sampled from the underlying Gaussian distribution
        """
        # Generate white noise: normal random noise with zero mean and variance 1.
        white_noise = self.generate_white_noise(
            truncate=False,
            return_np=False,
        )
        noise = self.covariance_sqrt_matvec(
            white_noise,
            lower=True,
            in_place=False,
            return_np=False,
        )
        noise.axpy(1.0, self.mean)
        if return_np:
            noise = noise.get_local()

        return noise

    def sample(self, return_np=True):
        """
        Sample a random vector from the underlying Gaussian distribution
        """
        # Add a scaled random noise (with the underlying covariance matrix) to the underlying mean
        out_vec = self.mean.get_local()
        out_vec += self.generate_noise(return_np=True)
        if not return_np:
            _out_vec = self.create_vector(return_np=False)
            _out_vec.set_local(out_vec)
            out_vec = _out_vec
        return out_vec

    def covariance_sqrt_matvec(self, x, lower=True, return_np=True, in_place=False):
        """
        Evaluate and return the product of Square root (Lower Cholesky) covariance matrix
        by a vector `x`. The product is determined by applying the cholesky decomposition
        of the covariance matrix once (dot product) to the passed state `x`

        :param x: vector in a space of dimension equal to the size of the
            undrlying error model. x` could be scalar or number array,
            however, it must match the distribution size.
        :param bool lower: use the lower Cholesky as the square root
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the sqrt-covariance matrix by `x`
        """
        if isinstance(x, np.ndarray):
            x_size = x.size
        elif isinstance(x, dl.Vector):
            x_size = x.size()
        else:
            raise TypeError(f"Unsupported type '{type(x)}' of passed vector x")

        if lower and (x_size == self.noise_size):
            pass
        elif not lower and (x_size == self.size):
            pass
        else:
            raise TypeError(
                "The passed vector has wrong shape/size. "
                f"Expected size {x_size}; recieved "
                f"{self.noise_size if lower else self.size}"
            )

        if isinstance(x, np.ndarray):
            if lower:
                _x = self.create_noise_vector(return_np=False)
                _x.set_local(x)
            else:
                _x = self.create_vector(return_np=False)
                _x.set_local(x)
            x = _x

        if lower:
            rhs = self.create_vector(return_np=False)
            out_vec = self.create_vector(return_np=False)
            self._STDEV_INV.mult(x, rhs)
            self._PREC_SOLVER.solve(out_vec, rhs)
        else:
            rhs = self.create_vector(return_np=False)
            out_vec = self.create_noise_vector(return_np=False)
            self._PREC_SOLVER.solve(rhs, x)  # the operator is symmetric
            self._STDEV_INV.transpmult(rhs, out_vec)

        if return_np:
            out_vec = out_vec.get_local()
        return out_vec

    def covariance_matvec(self, x, return_np=True, in_place=False):
        """
        Evaluate and return the product of covariance matrix by a vector `x`.
        The product is determined by applying the cholesky decomposition of the covariance matrix
        twice (dot product) to the passed state `x`
        Specifically, we assume the covariance matrix is on the form :math:`\\mathbf{L} \\mathbf{S} \\mathbf{S}^T \\mathbf{L}^T`, where :math:`\\mathbf{L}` is the design matrix (fat-short matrix), and :math:`\\mathbf{S}` is the lower Cholesky factor of the covariance matrix.

        :param x: vector in a space of dimension equal to the size of the undrlying error model.
            `x` could be scalar or number array, however, it must match the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the covariance matrix by `x`
        """
        out_vec = self.covariance_sqrt_matvec(
            x, lower=False, in_place=False, return_np=False
        )
        out_vec = self.covariance_sqrt_matvec(
            out_vec, lower=True, in_place=True, return_np=return_np
        )
        # TODO: Compare to assembling the full covariances matrix, and applying it!
        return out_vec

    def covariance_diagonal(self, method="exact", verbose=False):
        """
        Return a copy the diagonal of the covariance matrix, i.e., the variances

        :returns: 1D numpy array holding the diagonal of the underlying covariance matrix.
            If the size of the underlying distribution is 1, this returns a scalar (variance)
        """
        if re.match(r"\Aexact\Z", method, re.IGNORECASE):
            # This is really not a great function; it'll take forever to evaluate the covariance diagonal; will use for debugging only
            size = self.size
            diag = np.empty(size)
            e_vec = self.create_vector(return_np=False)
            for i in range(size):
                if verbose:
                    print(
                        f"\rEvaluating variance (diagonal entry): {i+1}/{size}",
                        end=" ",
                    )
                e_vec.zero()
                e_vec[i] = 1.0
                diag[i] = self.covariance_matvec(e_vec)[i]
            if verbose:
                print()

        elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
            raise NotImplementedError(
                "TODO: Randomized estimate of the diagonal is not yet implemented"
            )
        else:
            raise ValueError(
                f"Unrecognized method '{method}'\n"
                "Currently, only 'exact' and 'randomized' methods are supported."
            )
        return diag

    def variance(self):
        """Same as :py:meth:`covariance_diagonal`"""
        return self.covariance_diagonal()

    def covariance_inv_matvec(self, x, in_place=False, return_np=True):
        """
        Evaluate and return the product of inverse of the error covariance matrix by a vector `x`.
        A linear system is solved using the cholesky decomposition of the covariance matrix

        :param x: vector in a space of dimension equal to the size of the undrlying error model.
            `x` could be scalar or number array, however, it must match the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of inverse of the error covariance matrix by `x`

        :raises: ValueError if solving the linear system  using GMRES fails
        """
        # We can do better with dl solvers and operators!
        if isinstance(x, np.ndarray):
            is_dl = False
            in_vec = x.copy()
            pass
        elif isinstance(x, dl.Vector):
            is_dl = True
            in_vec = x.get_local()
        else:
            raise TypeError(f"Unsupported type '{type(x)}' of passed vector x")

        # Define an operator out of self.covariance_matvec, then solve a linear system
        # solve a linear system to find covariance-inverse vector-product
        matvec = lambda x: self.covariance_matvec(x, in_place=False, return_np=True)
        size = self.size
        A = LinearOperator((size, size), matvec=matvec)

        # TODO: Add other (more efficient) solvers and preconditioners as needed!
        _out = isolve.lgmres(A=A, b=x)
        if not _out[1]:
            # Solution is successful
            _out = _out[0]
        else:
            raise ValueError(
                "Failed to apply GMRES to solve the linear system "
                "involving the covariance matrix"
            )

        if in_place:
            out = x
            if is_dl:
                out.set_local(_out)
            else:
                out[:] = _out
        else:
            if not return_np:
                out = self.create_vector(return_np=False)
                out.set_local(_out)
            else:
                out = _out
        return out

    def covariance_matrix(self):
        """
        Construct a numpy array representation (if the dimension is more than 1)
        of the underlying covariance matrix. (projected onto the observation space)
        """
        e_vec = self.create_vector(return_np=False)
        size = e_vec.size()

        cov = np.empty((size, size))
        for j in range(size):
            e_vec.zero()
            e_vec[j] = 1.0
            cov[:, j] = self.covariance_matvec(
                e_vec, in_place=True, return_np=False
            ).get_local()

        return cov

    def precision_matrix(self):
        """
        Construct a numpy array representation (if the dimension is more than 1)
        of the precision matrix (inverse of the underlying covariance matrix).
        """
        e_vec = self.create_vector(return_np=False)
        size = e_vec.size()

        precisions = np.empty((size, size))
        for j in range(size):
            e_vec.zero()
            e_vec[j] = 1.0
            precisions[:, j] = self.covariance_inv_matvec(
                e_vec, in_place=True, return_np=False
            ).get_local()

        return precisions

    def covariance_trace(
        self,
        method="exact",
        sample_size: int = _RANDOMIZATION_SAMPLE_SIZE,
        optimize_sample_size: bool = False,
        min_sample_size: int = 10,
        max_sample_size: int = 100,
        distribution: str = "Rademacher",
        accuracy: float = 1e-2,
    ) -> float:
        """
        Evaluate/Approximate the trace of the covariance matrix

        .. note::
            This is a wrapper around the utility function `pyoed.utility.math.matrix_trace`
            that is called with the method :py:meth:`covariance_matvec` as the operator
            to compute the trace of.

        :param method: Method of evaluation. If "exact", we simply sum the diagonal of A. If
            "randomized", a Hutchinson style trace estimator, as described in
            :py:meth:`pyoed.utility.trace_estimator`, is employed. All of the futher keyword
            arguments are in the case of method="randomized" and are otherwise ignored if
            method="exact".
        :param randomization_vectors: The randomization vectors drawn to perform the
            computation. If not passed, they will be sampled.
        :param sample_size: The number of samples to use. :param optimize_sample_size: if
            `True`, `sample_size` is replaced with an optimized version, where the optimal
            sample size minimizes the variance of the estimator and is between
            `min_sample_size`, and `max_sample_size`.
        :param min_sample_size: Minimum number of random samples.
        :param max_sample_size: Maximum number of random samples.
        :param distribution: The probability distribution used for random sampling. Both
            'Rademacher' and 'Gaussian' are supported.
        :param random_seed: The random seed to use for the distribution samples.
        :param accuracy: convergence criterion.

        :returns: exact/approximate value of the trace of the covariance matrix.
            of the underlying model.
        """
        if re.match(r"\Aexact\Z", method, re.IGNORECASE):
            variance = self.covariance_diagonal()
            tr = np.sum(variance)

        elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
            size = self.size
            matvec = lambda x: self.covariance_matvec(x, return_np=True, in_place=False)

            out = utility.trace_estimator(
                matvec_fun=matvec,
                vector_size=size,
                sample_size=sample_size,
                optimize_sample_size=optimize_sample_size,
                min_sample_size=min_sample_size,
                max_sample_size=max_sample_size,
                distribution=distribution,
                accuracy=accuracy,
            )
            tr = out[1]
            if not out[0]:
                print(  # Should be a warning
                    "The randomized approximation of the trace has not converged, "
                    "hence is untrusted!"
                )
        else:
            raise ValueError(f"Unsupported evaluation method '{method}'")
        return tr

    def covariance_logdet(
        self,
        method="exact",
        sample_size: int = _RANDOMIZATION_SAMPLE_SIZE,
        optimize_sample_size: bool = False,
        min_sample_size: int = 10,
        max_sample_size: int = 100,
        distribution: str = "Rademacher",
        accuracy: float = 1e-2,
    ) -> float:
        """
        Evaluate/approximate the logarithm of the determinant of the covariance
        matrix of the passed error model

        .. note::
            This is a wrapper around the utility function `pyoed.utility.math.matrix_logdet`
            that is called with the method :py:meth:`covariance_matvec` as the operator
            to compute the trace of.

        :param method: Method of evaluation. If "exact", we simply sum the diagonal of A.
            If "randomized", a chebyshev-approximation type-method is employed from
            :py:meth:`pyoed.utility.logdet_estimator`. All of the futher keyword arguments
            are in the case of method="randomized" and are otherwise ignored if
            method="exact".
        :param randomization_vectors: The randomization vectors drawn to perform the
            computation. If not passed, they will be sampled.
        :param sample_size: The number of samples to use. :param optimize_sample_size: if
            `True`, `sample_size` is replaced with an optimized version, where the optimal
            sample size minimizes the variance of the estimator and is between
            `min_sample_size`, and `max_sample_size`.
        :param min_sample_size: Minimum number of random samples.
        :param max_sample_size: Maximum number of random samples.
        :param distribution: The probability distribution used for random sampling. Both
            'Rademacher' and 'Gaussian' are supported.
        :param random_seed: The random seed to use for the distribution samples.
        :param accuracy: convergence criterion.

        :returns: exact/approximate value of the logdet of the covariance matrix
            of the underlying model.
        """
        if re.match(r"\Aexact\Z", method, re.IGNORECASE):
            cov = self.covariance_matrix()
            s, logdet = np.linalg.slogdet(cov)
            if s == 0:
                # Singular matrix
                print("The covariance matrix is singular")
                raise ValueError

            elif s < 0:
                # Negative definite
                print("The covariance matrix is negative definite!")
                raise ValueError

            else:
                # Good
                pass

            pass  # TODO

        elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
            logdet = utility.logdet_estimator(
                matvec_fun=self.covariance_matvec,
                vector_size=self.active_size,
                sample_size=sample_size,
                optimize_sample_size=optimize_sample_size,
                min_sample_size=min_sample_size,
                max_sample_size=max_sample_size,
                distribution=distribution,
                accuracy=accuracy,
            )

        else:
            raise ValueError(f"Unsupported evaluation method '{method}'")
        return logdet

    @property
    def Vh(self):
        """Get a copy of the finite element space"""
        return self.configurations.Vh

    @property
    def mean(self):
        """Get a copy of the distribution mean"""
        return self.configurations.mean

    @mean.setter
    def mean(self, new_mean):
        """Update distribution mean (in the full space; regardless the design)"""
        self.update_mean(new_mean, validate=True)

    @property
    def size(self):
        """Dimension of the underlying probability distribution"""
        return self.Vh.dim()

    @property
    def noise_size(self):
        """Dimension of the noise space given the square root of the covariance operator"""
        return self._NOISE_SIZE

    @property
    def design(self):
        """Get a copy of the design vector"""
        raise NotImplementedError("TODO")

    @design.setter
    def design(self, design):
        """
        Update the design vector; If the design is non-binary; a design weight matrix is
            constructed as the square root of the passed design; this pre- and post-multiply
            the observation error covariance matrix. Othe behaviour can be define if needed!
        """
        raise NotImplementedError("TODO")

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for multiple prognostic variables)
            of the observation vector/range
        """
        raise NotImplementedError("TODO")

    @property
    def active(self):
        """flags corresponding to active/inactive dimensions"""
        raise NotImplementedError("TODO")

    @property
    def active_size(self):
        """flags corresponding to active/inactive dimensions"""
        raise NotImplementedError("TODO")


@dataclass(kw_only=True, slots=True)
class DolfinBiLaplacianErrorModelConfigs(DolfinLaplacianErrorModelConfigs):
    """
    Configurations for :py:class:`DolfinBiLaplacianErrorModel`.
    Same as :py:class:`DolfinLaplacianErrorModelConfigs`.
    """


@set_configurations(DolfinBiLaplacianErrorModelConfigs)
class DolfinBiLaplacianErrorModel(DolfinLaplacianErrorModel):
    """
    A class implementing a simple Error model with a Bi-Laplacian-based covariance model.
    The covariance matrix takes the form:
    :math:`\\mathbf{C} = \\mathbf{B}^{-1} \\mathbf{M} \\mathbf{B}^{-1}`,
    where :math:`\\mathbf{A}` is the FE matrix resulting from discretization of
    the variational form characterizing the inverse of the square root of the
    covariance :math:`\\mathbf{B} = (\\delta \\mathbf{I} + \\gamma \\mbox{div} \\nabla)^{-2}`,
    where :math:`\\gamma / \\delta` controls the correlation length (lengthscale),
    :math:`\\delta\\gamma` controls the variance/uncertainty level, and
    :math:`\\Delta` is the Laplacian operator.

    The magnitude of :math:`\\delta\\gamma` governs the variance of the samples, while
    the ratio :math:`\\frac{\\gamma}{\\delta}` governs the correlation length.

    :param configs: settings of the error model

    :raises:
        - `ImportError`: if dolfin or ufl are not available/installed
        - `TypeError`: if mean or variance types are not supported (scalars and iterables are supported)
        - `ValueError`: if no valid FEM space (DOF) `Vh` is found in the configurations dictionary
        - `ValueError`: if `gamma` or `delta` are non-positive
    """

    def __init__(
        self,
        configs: DolfinBiLaplacianErrorModelConfigs | dict | None = None,
    ):
        if None in (dl, ufl):
            msg = "This class can be used only with dolfin and UFL installed. "
            msg += "Failed to load/import these modules."
            raise ImportError(msg)
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        # Extract configurations
        Vh = self.Vh
        mean = self.mean
        gamma, delta = self.configurations.gamma, self.configurations.delta

        # FEM space defines the dimension of the Gaussian distribution space

        # Create variance/covariance operator and the square root (STDEV), and solvers
        trial = dl.TrialFunction(Vh)
        test = dl.TestFunction(Vh)

        # Create Covariances operator and related solver(s)
        self.update_covariance_matrix(gamma=gamma, delta=delta)

        # Check the passed mean, and associate to self
        self.update_mean(mean=mean)

        # No Design for this model (at least not now!); we will see if it makes sense!
        self._DESIGN = self._DESIGNMAT = None

        # Mass & and Factor
        varf_M = ufl.inner(trial, test) * ufl.dx
        self._M = dl.assemble(varf_M)
        self._M_SOLVER = self._create_solver(self._M)

        # Update dl form compiler parameters
        dl_form_compiler = dl.parameters["form_compiler"]
        orig_representation = dl_form_compiler["representation"]
        orig_quadrature_degree = dl_form_compiler["quadrature_degree"]
        orig_element_degree = self.Vh.ufl_element().degree()
        dl_form_compiler.update(
            {"quadrature_degree": -1, "representation": "quadrature"}
        )

        # Define FE element
        new_element_degree = 2 * orig_element_degree
        mesh = self.Vh.mesh()
        physics_ndim = mesh.geometry().dim()
        num_subspaces = self.Vh.num_sub_spaces()
        if num_subspaces <= 1:
            quad_element = ufl.FiniteElement(
                family="Quadrature",
                cell=mesh.ufl_cell(),
                degree=new_element_degree,
                quad_scheme="default",
            )
        else:
            quad_element = ufl.VectorElement(
                family="Quadrature",
                cell=mesh.ufl_cell(),
                degree=new_element_degree,
                dim=num_subspaces,
                quad_scheme="default",
            )
        Qh = dl.FunctionSpace(mesh, quad_element)
        ph = dl.TrialFunction(Qh)
        qh = dl.TestFunction(Qh)

        ufl_dx = ufl.dx(metadata=dict(quadrature_degree=new_element_degree))
        Mqh = dl.assemble(ufl.inner(ph, qh) * ufl_dx)
        ones = dl.Vector()

        Mqh.init_vector(ones, 0)
        ones_np = ones.get_local()
        ones_np[:] = 1
        ones.set_local(ones_np)

        dMqh = Mqh * ones
        dMqh_np = dMqh.get_local()
        dMqh.set_local(ones_np / np.sqrt(dMqh_np))

        Mqh.zero()
        Mqh.set_diagonal(dMqh)

        varf_G = ufl.inner(ph, test) * ufl_dx
        G = dl.assemble(varf_G)
        G_mat = dl.as_backend_type(G).mat()
        Mqh_mat = dl.as_backend_type(Mqh).mat()
        M_SQRT_mat = G_mat.matMult(Mqh_mat)
        rmap, _ = G_mat.getLGMap()
        _, cmap = Mqh_mat.getLGMap()
        M_SQRT_mat.setLGMap(rmap, cmap)
        self._M_SQRT = dl.Matrix(dl.PETScMatrix(M_SQRT_mat))

        # Dimension of the noise space
        self._NOISE_SIZE = self._M_SQRT.size(1)

        # Mapper from model to noise space (by replication!)
        self._M_SQRT_MAPPER = sp.lil_matrix((self._NOISE_SIZE, self.size), dtype=int)
        for i in range(self.size):
            locs = self._M_SQRT.getrow(i)[0]
            self._M_SQRT_MAPPER[locs, i] = 1
        self._M_SQRT_MAPPER = self._M_SQRT_MAPPER.tocsc()

        # Reset form compiler parameters
        dl_form_compiler.update(
            {
                "quadrature_degree": orig_quadrature_degree,
                "representation": orig_representation,
            }
        )

    def validate_configurations(
        self,
        configs: dict | DolfinBiLaplacianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the DolfinBiLaplacian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`DolfinBiLaplacianErrorModelConfigs`.
        """
        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_covariance_matrix(self, gamma, delta, validate=False):
        """
        Define the Variational forms of the covariance, precision,
        and factors of the covariance matrix, and assocate to self. This enables updatign covariances consistently
        """
        if validate:
            self.validate_configurations(
                {"gamma": gamma, "delta": delta}, raise_for_invalid=True
            )

        # Create variance/covariance operator and the square root (STDEV), and solvers
        trial = dl.TrialFunction(self.Vh)
        test = dl.TestFunction(self.Vh)

        # Square Root (Lower/Left) of the Precisions (Covariance Inverse); dropping M
        varf_B = (
            dl.Constant(gamma) * ufl.inner(ufl.grad(trial), ufl.grad(test)) * ufl.dx
            + dl.Constant(delta) * ufl.inner(trial, test) * ufl.dx
        )
        self._PREC_SQRT = dl.assemble(varf_B)
        self._PREC_SQRT_SOLVER = self._create_solver(self._PREC_SQRT)

    def create_noise_vector(self, return_np=True):
        """
        Create a vector compatible with the domain of the underlying covariance (SQRT) operator
        """
        vec = dl.Vector()
        self._M_SQRT.init_vector(vec, 1)
        if return_np:
            vec = vec.get_local()
        return vec

    def create_vector(self, return_np=True):
        """
        Create a vector compatible with the range of the underlying covariance operator
        """
        vec = dl.Vector()
        self._PREC_SQRT.init_vector(vec, 0)
        if return_np:
            vec = vec.get_local()
        return vec

    def __map_up(self, x):
        """
        Map vector from model domain to noise domain by replication!
        """
        x_np = x[:]
        assert (
            x_np.size == self.size
        ), "the passed vector must be in the model domain space!"
        mapped_x = self._M_SQRT_MAPPER.dot(x_np)
        if isinstance(x, np.ndarray):
            out = mapped_x
        else:
            out = self.create_noise_vector(return_np=False)
            out.set_local(mapped_x)
        return out

    def covariance_sqrt_matvec(self, x, lower=True, return_np=True, in_place=False):
        """
        Evaluate and return the product of Square root (Lower Cholesky) covariance matrix
        by a vector `x`. The product is determined by applying the cholesky decomposition
        of the covariance matrix once (dot product) to the passed state `x`

        :param x: vector in a space of dimension equal to the size of the
            undrlying error model. x` could be scalar or number array,
            however, it must match the distribution size.
        :param bool lower: use the lower Cholesky as the square root
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the sqrt-covariance matrix by `x`
        """
        # print(f"DEBUG: BiLaplacian Getting in (x) size {x[:].size}; lower? {lower}")

        if isinstance(x, np.ndarray):
            x_size = x.size
        elif isinstance(x, dl.Vector):
            x_size = x.size()
        else:
            raise TypeError(f"Unsupported type '{type(x)}' of passed vector x")

        if lower and (x_size == self.noise_size):
            pass
        elif lower and (x_size == self.size):
            x = self.__map_up(x)
            x_size = self.noise_size
        elif not lower and (x_size == self.size):
            pass
        else:
            raise TypeError(
                "The passed vector has wrong shape/size. "
                f"Expected size {x_size}; recieved "
                f"{self.noise_size if lower else self.size}"
            )

        if isinstance(x, np.ndarray):
            if lower:
                _x = self.create_noise_vector(return_np=False)
                _x.set_local(x)
            else:
                _x = self.create_vector(return_np=False)
                _x.set_local(x)
            x = _x

        if lower:
            rhs = self.create_vector(return_np=False)
            out_vec = self.create_vector(return_np=False)
            self._M_SQRT.mult(x, rhs)
            self._PREC_SQRT_SOLVER.solve(out_vec, rhs)
        else:
            rhs = self.create_vector(return_np=False)
            out_vec = self.create_noise_vector(return_np=False)
            self._PREC_SQRT_SOLVER.solve(rhs, x)  # the operator is symmetric
            self._M_SQRT.transpmult(rhs, out_vec)

        # print(f"DEBUG: BiLaplacian Getting out_vec size {out_vec[:].size}")

        if return_np:
            out_vec = out_vec.get_local()
        return out_vec

    def covariance_matvec(self, x, return_np=True, in_place=False):
        """
        Evaluate and return the product of covariance matrix by a vector `x`.
        The product is determined by applying the cholesky decomposition of the covariance matrix
        twice (dot product) to the passed state `x`
        Specifically, we assume the covariance matrix is on the form :math:`\\mathbf{L} \\mathbf{S} \\mathbf{S}^T \\mathbf{L}^T`, where :math:`\\mathbf{L}` is the design matrix (fat-short matrix), and :math:`\\mathbf{S}` is the lower Cholesky factor of the covariance matrix.

        :param x: vector in a space of dimension equal to the size of the undrlying error model.
            `x` could be scalar or number array, however, it must match the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the covariance matrix by `x`
        """
        if False:
            out_vec = self.covariance_sqrt_matvec(
                x, lower=False, in_place=False, return_np=False
            )
            out_vec = self.covariance_sqrt_matvec(
                out_vec, lower=True, in_place=True, return_np=return_np
            )
            # TODO: Compare to assembling the full covariances matrix, and applying it!
        else:
            # We can actually eliminate one matvec operation
            if isinstance(x, np.ndarray):
                x_size = x.size
            elif isinstance(x, dl.Vector):
                x_size = x.size()
            else:
                raise TypeError(f"Unsupported type '{type(x)}' of passed vector x")

            if x_size != self.size:
                raise TypeError(
                    "The passed vector has wrong shape/size. "
                    f"Expected size {x_size}; recieved {self.size}"
                )

            if isinstance(x, np.ndarray):
                _x = self.create_vector(return_np=False)
                _x.set_local(x)
                out_vec = x = _x
            else:
                out_vec = x if in_place else x.copy()

            rhs1 = self.create_vector(return_np=False)
            rhs2 = self.create_vector(return_np=False)
            out_vec = self.create_vector(return_np=False)
            self._PREC_SQRT_SOLVER.solve(rhs1, x)
            self._M.mult(rhs1, rhs2)
            self._PREC_SQRT_SOLVER.solve(out_vec, rhs2)

            if return_np:
                out_vec = out_vec.get_local()
                if in_place and isinstance(x, np.ndarray):
                    x[:] = out_vec
        return out_vec

    def covariance_inv_matvec(self, x, return_np=True, in_place=False):
        """
        Evaluate and return the product of precision (inverse covariance) matrix by a vector `x`.

        :param x: vector in a space of dimension equal to the size of the undrlying error model.
            `x` could be scalar or number array, however, it must match the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the covariance matrix by `x`
        """
        # We can actually eliminate one matvec operation
        if isinstance(x, np.ndarray):
            x_size = x.size
        elif isinstance(x, dl.Vector):
            x_size = x.size()
        else:
            raise TypeError(f"Unsupported type '{type(x)}' of passed vector x")

        if x_size != self.size:
            raise TypeError(
                "The passed vector has wrong shape/size. "
                f"Expected size {x_size}; recieved {self.size}"
            )

        if isinstance(x, np.ndarray):
            _x = self.create_vector(return_np=False)
            _x.set_local(x)
            x = _x
        out_vec = x if in_place else x.copy()

        rhs1 = self.create_vector(return_np=False)
        rhs2 = self.create_vector(return_np=False)
        out_vec = self.create_vector(return_np=False)
        self._PREC_SQRT.mult(x, rhs1)
        self._M_SOLVER.solve(rhs2, rhs1)
        self._PREC_SQRT.mult(rhs2, out_vec)

        if return_np:
            out_vec = out_vec.get_local()
            if in_place and isinstance(x, np.ndarray):
                x[:] = out_vec
        return out_vec


def create_Laplacian_error_model(Vh, gamma=1.0, delta=0.5, mean=0.0, random_seed=None):
    """
    A simple interface to instantiate an object of the :py:class:`DolfinLaplacianErrorModel` class.

    :param Vh: finite element space to which the distribution is assocciated
    :param gamma: positive number (float) that controls the variance/uncertainty level
    :param delta: positive number (float) such that `gamma / delta` controls the correlation length (lengthscale),
    :param mean: (float or iterable of floats (numpy array, or dolfin vector)) of the error model;
    :param random_seed: random seed used when the Gaussian model is initiated.
      This is useful for reproductivity. If `None`, random seed follows `numpy.random.seed` rules

    :returns: an instance of :py:class:`DolfinLaplacianErrorModel` with passed configurations
    """
    error_model = DolfinLaplacianErrorModel(
        configs=dict(
            Vh=Vh, gamma=gamma, delta=delta, mean=mean, random_seed=random_seed
        )
    )
    return error_model


def create_BiLaplacian_error_model(
    Vh, gamma=1.0, delta=0.5, mean=0.0, random_seed=None
):
    """
    A simple interface to instantiate an object of the :py:class:`DolfinBiLaplacianErrorModel` class.

    :param Vh: finite element space to which the distribution is assocciated
    :param gamma: positive number (float) such that `gamma delta`  controls the variance/uncertainty level
    :param delta: positive number (float) such that `gamma / delta` controls the correlation length (lengthscale),
    :param mean: (float or iterable of floats (numpy array, or dolfin vector)) of the error model;
    :param random_seed: random seed used when the Gaussian model is initiated.
      This is useful for reproductivity. If `None`, random seed follows `numpy.random.seed` rules

    :returns: an instance of :py:class:`DolfinBiLaplacianErrorModel` with passed configurations
    """
    error_model = DolfinBiLaplacianErrorModel(
        configs=dict(
            Vh=Vh, gamma=gamma, delta=delta, mean=mean, random_seed=random_seed
        )
    )
    return error_model
