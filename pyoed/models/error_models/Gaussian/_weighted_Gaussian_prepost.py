# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements weighted versions of Gaussian error model
with support for binary as well as relaxed (non-binary) design.
The role of a design is to activate/deactivate entries (dimensions) if it is binary,
however, if it is relaxed, the design applies some weights to the entries of the
covariance (and the precision) matrix based on the formulation approach taken.

The approach taken here is **pre-post** multiplication.
This means either the covariance matrix (or its inverse) is
multiplied (pre -> from left, and post -> from right) by a diagonal matrix
with the design vector (relaxed) on its main diagonal.

.. note::
    This is reduces to the functionality provided by :py:class:`GaussianErrorModel`
    only when the design is binary, and when the *covariance* is pre-post multiplied.

.. warning::
    The functionality here is provided for testing widely used approaches in the
    literature of optimal experimental design.
    We have shown, however, that pre-post multiplication of either the covariance
    matrix or its inverse by relaxed design is generally wrong if the covariance
    matrix is non-diagonal, that is if there are correlations in the error model.
    To apply proper weighting for relaxed (non-binary) designs, use the functionality
    provided by :py:class:`PointwiseWeightedGaussianErrorModel` instead of using
    :py:class:`PrePostWeightedGaussianErrorModel`.
    For details, on why pre-post multiplication is incorrect, see:

    1. Ahmed Attia, and Emil Constantinescu.
       "Optimal experimental design for inverse problems in the presence of
       observation correlations."
       SIAM Journal on Scientific Computing 44, no. 4 (2022): A2808-A2842.
"""

import sys
import inspect
import re
from dataclasses import dataclass, replace
from typing import (
    Callable,
    Any,
)

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg

from pyoed.models.core import (
    ErrorModel,
    ErrorModelConfigs,
)
from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    validate_key,
    set_configurations,
    PyOEDConfigsValidationError,
)
from ._Gaussian import (
    GaussianErrorModelConfigs,
    GaussianErrorModel,
    _LINSOLVE_TOL,
    _RANDOMIZATION_SAMPLE_SIZE,
)


@dataclass(kw_only=True, slots=True)
class PrePostWeightedGaussianErrorModelConfigs(GaussianErrorModelConfigs):
    """
    Configuration settings for :py:class:`PrePostWeightedGaussianErrorModel`.
    These are exactly the same as the configurations of the parent class
    :py:class:`GaussianErrorModelConfigs` with additional parameters as follows:

    :param design_weighting_scheme: a string describing the weighting scheme;
        you can choose from the following options:

        - 'covariance': the covariance matrix is pre- and post-multiplied by the
          design matrix defined as a diagonal matrix with the design on
          its diagonal.

        - 'precision': the precision (inverse of the covariance) matrix is
          pre- and post-multiplied by the design matrix defined as
          a diagonal matrix with the design on its diagonal.
    """
    design_weighting_scheme: str = "covariance"



@set_configurations(PrePostWeightedGaussianErrorModelConfigs)
class PrePostWeightedGaussianErrorModel(
    GaussianErrorModel,
):
    r"""
    A simple Numpy-based (or scipy sparse) Gaussian error model with pre- and
    post-multiplicative design weighting. Specifically, the design (binary or relaxed)
    takes place by pre- and post-multiplication of the covariance or the precision
    matrix (based on the `design_weighting_scheme` in the configurations) by a diagonal
    matrix with the design set to its diagonal (the design matrix here).

    As a clarifying example, consider the A-optimal design for linear inverse problems.
    In this case, the posterior covariance :math:`\Gamma_{\rm post}` takes on the following
    forms:

    1. `design_weighting_scheme=='covariance'`: pre-post multiplication of the
       covariance matrix). The covariance of the Gaussian posterior is:

       .. math::
           \Gamma_{\rm post}
             = \left(
               \mathbf{F}^{T} \left(
                  diag(\mathbf{d}) \Gamma_{\rm noise} diag(\mathbf{d})
               \right)^{-1} \mathbf{F}
               + \Gamma_{\rm prior}^{-1}
             \right)^{-1} \,,

    2. `design_weighting_scheme=='precision'`: pre-post imultiplication of the
       precision matrix). The covariance of the Gaussian posterior is:

       .. math::
          \Gamma_{\rm post}
             = \left(
               \mathbf{F}^{T} diag(\mathbf{d}) \Gamma_{\rm noise}^{-1}
               diag(\mathbf{d}) \mathbf{F}
               + \Gamma_{\rm prior}^{-1}
              \right)^{-1} \,,

    where :math:`\mathbf{F}` is the forward model (e.g., parameter-to-observable map),
    :math:`\mathbf(d)` is the relaxed/binary design vector, :math:`diag(\mathbf{d})`
    is a diagonal matrix with :math:`\mathbf{d}` on its main diagonal,
    :math:`\Gamma_{\rm prior}` is the covariance matrix of the Gaussian prior, and
    :math:`\Gamma_{\rm noise}` is the covariance matrix of the Gaussian observation
    noise/error model.

    Thus, in this formulation/implementation, the effect of the design takes place on
    the covariance/precision matrix.

    .. note::
        This implementation is not accurate for correlated covariances as it creates
        discontinuity at the corner (binary) points. Though, we add it for comparison as
        it has been utilized in the past in the OED literature, and the implementation
        is simpler and less computationally expensive.

    :param configs: an object containing configurations of the error model

    :raises PyOEDConfigsValidationError: if mean or variance types are not supported
        (scalars and iterables are supported)
    :raises PyOEDConfigsValidationError: if there is a size mismatch between size,
        mean, and variance.
    """

    def __init__(
        self, configs: PrePostWeightedGaussianErrorModelConfigs | dict | None = None
    ):
        super().__init__(self.configurations_class.data_to_dataclass(configs))

        # Make sure the design weighting scheme name is unified (covariance/precision)
        self.update_design_weighting_scheme(self.configurations.design_weighting_scheme)


    def validate_configurations(
        self,
        configs: dict | PrePostWeightedGaussianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the Gaussian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises PyOEDConfigsValidationError: if any (or a group) of the configurations
            does not exist in the model configurations
            :py:class:`PrePostWeightedGaussianErrorModelConfigs`.
        """
        aggregated_configs = self.aggregate_configurations(configs)

        ## Local validation tests:
        def is_valid_scheme(scheme):
            if not isinstance(scheme, str):
                return False
            else:
                if re.match(r"\A(cov|covariance|prec|precision)\Z", scheme, re.IGNORECASE):
                    return True
                else:
                    return False

        ## Validate new params in the configurations
        # `design_weighting_scheme`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="design_weighting_scheme",
            test=is_valid_scheme,
            message="Invalid weighting scheme type/value! Expected `covariance` or `precision`",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def standardize_configurations(
        self,
        configs: dict | PrePostWeightedGaussianErrorModelConfigs,
        mean_dtype:type = float,
        variance_dtype: type = float,
        design_dtype: type = float,
    ) -> PrePostWeightedGaussianErrorModelConfigs:
        """
        Check the configurations of the error model (size, mean, design, and variance),
        and make sure they are consistent.

        .. note::
            Here are the rules:

            1. If the `size` is passed, the mean and the design must be one-dimensional
               arrays/vectors of this size (if either is scalar, they are casted to vectors),
               and the variance's size/shape is asserted accordingly.
            2. If the `size` is not passed, it is inferred from `mean` and/or `variance`

        :raises PyOEDConfigsValidationError: if the validation of the configurations fail
        """
        return self.configurations_class.data_to_dataclass(
            super().standardize_configurations(
                configs=configs,
                mean_dtype=mean_dtype,
                variance_dtype=variance_dtype,
                design_dtype=design_dtype,
            )
        )

    def update_design(self, design):
        """
        If the design matches existing one, don't do anything, otherwise, update it

        :param design: the design to be used update model's configurations

        :raises PyOEDConfigsValidationError: if the passed design is of
            invalid type/shape/size
        """
        if design is None:
            design = np.ones(self.size, dtype=float)
        else:
            design = np.asarray(design, dtype=float).flatten()

        if design.size != self.size:
            raise PyOEDConfigsValidationError(
                f"The design must be 1d array of size {self.size}\n"
                f"Received {design=} with {design.shape=}"
            )

        # Compare the design against the one used for calculating _STDEV
        if self._STDEV is not None and any(design != self._DESIGN):
            # Existing design differs from the new one

            # Destroy the lower Cholesky factor (& inverse, & logdet) of projected covariance
            # matrix and rebuild when needed
            self._reset_covariance_attributes()

        elif self._STDEV is not None:
            if self.verbose:
                print(
                    f"The passed design is not different from current one. "
                    f"Skipping design update..."
                )
        else:
            pass

        # Update configurations to make sure an array is set in the configurations
        self.configurations.design = design

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # If a new design weighting scheme is passed, update the weighting scheme
        if "design_weighting_scheme" in kwargs:
            self.update_design_weighting_scheme(kwargs["design_weighting_scheme"])

    def update_design_weighting_scheme(self, design_weighting_scheme: str) -> None:
        """
        Update the design weighting scheme.

        :param design_weighting_scheme: name of the design weighting scheme;
            expected 'covariance' or 'precision'

        :raises PyOEDConfigsValidationError: if the scheme is invalid
        """
        if not isinstance(design_weighting_scheme, str):
            raise PyOEDConfigsValidationError(
                f"Invalid {design_weighting_scheme=} of {type(design_weighting_scheme)=}\n"
                f"Expected either 'covariance' or 'precision'"
            )
        # Get new scheme name : `covariance` or `precision`
        if re.match(r"\A(cov|covariance)\Z", design_weighting_scheme, re.IGNORECASE):
            new_scheme = "covariance"
        elif re.match(r"\A(prec|precision)\Z", design_weighting_scheme, re.IGNORECASE):
            new_scheme = "precision"
        else:
            raise PyOEDConfigsValidationError(
                f"Invalid {design_weighting_scheme=} of {type(design_weighting_scheme)=}\n"
                f"Expected either 'covariance' or 'precision'"
            )

        existing_scheme = self.configurations.design_weighting_scheme
        if re.match(r"\A(cov|covariance)\Z", existing_scheme, re.IGNORECASE):
            existing_scheme = "covariance"
        elif re.match(r"\A(prec|precision)\Z", existing_scheme, re.IGNORECASE):
            existing_scheme = "precision"
        else:
            raise PyOEDConfigsValidationError(
                f"Invalid {self.configurations.design_weighting_scheme=} "
                f"of {type(self.configurations.design_weighting_scheme)=}\n"
                f"Expected either 'covariance' or 'precision'"
            )

        if existing_scheme != new_scheme:
            # The scheme is different; reset covariance lazy attributes
            self._reset_covariance_attributes()

        # Set the scheme name (even if the scheme does not change to unify the name)
        self.configurations.design_weighting_scheme = new_scheme


    def apply_weighting_matrix(self, variance, design=None, ):
        """
        Apply the design weighting matrix by pre- and post-multiplying the passed
        `variance` matrix with the passed `design` or the registered one.

        :param variance: the variance covariance matrix
        :param design: an experimental design used to calculate the weights.
            This is only added for flexibility. If `None`, the internal/current
            deesign is used.
        :returns: `variance` pointwise multiplied by the underlying weighting matix
        """
        if design is None:
            design = self.design
            active = self.active
        else:
            design = np.asarray(design).flatten()
            active = np.where(design)[0]

        if not (isinstance(variance, np.ndarray) or sp.issparse(variance)):
            try:
                variance = utility.asarray(variance)
            except Exception as err:
                raise TypeError(
                    f"Invalid {type(variance)=}. "
                    f"Expected an array or callable castable to an array!"
                    f"See the error below for details:\n"
                    f"Unexpected {err=} of {type(err)=}"
                )

        # Build a weignting matrix
        if variance.shape == (design.size, design.size):
            dimension = design.size
            weights = design

        elif variance.shape == (active.size, active.size):
            dimension = active.size
            weights = design[active]

        else:
            raise TypeError(
                f"The {variance.shape=} is not acceptable!\n"
                f"{design.size=}; {active.size=}"
            )

        # Creat design matrix (diagonal with design/weights on its diagonal)
        if sp.issparse(variance):
            D = sp.diags(weights, offsets=0, format="csc", )
        else:
            D = np.diag(weights, k=0, )

        # Apply the weighting matrix based on the registered scheme
        if self.design_weighting_scheme == "covariance":
            variance = D @ (variance @ D)

        elif self.design_weighting_scheme == "precision":
            inverter = sp.linalg.inv if sp.issparse(variance) else np.linalg.inv
            variance = inverter(D @ (inverter(variance) @ D))

        else:
            raise PyOEDConfigsValidationError(
                f"\n<<< THIS IS A ***BUG*** PLEASE REPORT >>>\n"
                f"Invalid {self.configurations.design_weighting_scheme=} "
                f"of {type(self.configurations.design_weighting_scheme)=}\n"
                f"Expected either 'covariance' or 'precision'"
            )

        return variance

    @property
    def design_weighting_scheme(self):
        """Name of the design weighting scheme"""
        return self.configurations.design_weighting_scheme
    @design_weighting_scheme.setter
    def design_weighting_scheme(self, val):
        self.update_design_weighting_scheme(val)

    @property
    def stdev(self, ):
        r"""
        Construct and return the lower Cholesky factor (if not available) in the
        projected space (after applying the design) of the covariance matrix

        .. note::
            The update of the covariance Cholesky factor based on the weighting scheme
            is as follows:

            1. `design_weighting_scheme` is `'covariance'`:

               .. math::
                    C \gets D C D^T; \qquad
                    C \gets D (L L^T) D^T = (DL) (DL)^T \,.

               Thus, the covariance matrix is projected onto the active subspace, and
               the lower Cholesky factor is calculated there, and then weights are applied.

            2. `design_weighting_scheme` is `'precision'`:

               .. math::
                   C^{-1} \gets D C^{-1} D;
                   C \gets ( D (L L^T)^{-1} D )^{-1}; \qquad

               where the inverse is replaced with pseudo inverse when the design
               includes inactive entries.
               Here, the Lower Cholesky factor is calculated for the full covariance
               matrix (by ignoring the design and including inactive entries), and then
               the lower factor is projected onto the active subspace and is then weighted
               by the inverse of the active design entries (weights).
        """
        if self._STDEV is None:
            if self.verbose:
                print(
                    f"Generating lower Cholesky factor of the covariance matrix"
                )

            if self.active_size == 0:
                self._STDEV = sp.csc_array(np.array([[]])).reshape(0, 0) if self.sparse else np.array([[]]).reshape(0, 0)

            else:

                # The design used to create _STDEV
                self._DESIGN = utility.asarray(self.design)

                # Scale the lower Cholesky factor by the design (or its inverse)
                if self.design_weighting_scheme == "covariance":

                    # 1) Create Cholesky factor (regardless of the weights):
                    self._STDEV = utility.factorize_spsd_matrix(
                        self.configurations.variance[self.active, :][:, self.active],
                        shape=(self.active_size, self.active_size),
                    )

                    # 2) Multiply it by the design matrix (scale each row)
                    for i, weight in zip(range(self.active_size), self.design[self.active]):
                        self._STDEV[[i], :] *= weight

                elif self.design_weighting_scheme == "precision":

                    # Calculate inverse of the covariance matrix then extract active entries
                    inverter = sp.linalg.inv if self.sparse else np.linalg.inv
                    c_inv = inverter(self.configurations.variance)[self.active, :][:, self.active]

                    # Weight
                    # 2) Multiply it by the design matrix from left and right
                    for i, weight in zip(range(self.active_size), self.design[self.active]):
                        c_inv[[i], :] *= weight
                        c_inv[:, [i]] *= weight

                    # Factorize the covariance matrix (factor of the inverse of the projected precision)
                    self._STDEV = utility.factorize_spsd_matrix(
                        inverter(c_inv),
                        shape=(self.active_size, self.active_size),
                    )

                else:
                    raise PyOEDConfigsValidationError(
                        f"\n<<< THIS IS A ***BUG*** PLEASE REPORT >>>\n"
                        f"Invalid {self.configurations.design_weighting_scheme=} "
                        f"of {type(self.configurations.design_weighting_scheme)=}\n"
                        f"Expected either 'covariance' or 'precision'"
                    )

        return self._STDEV


