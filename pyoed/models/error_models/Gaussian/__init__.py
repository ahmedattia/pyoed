# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import sys
import inspect

from .Gaussian import *
from .complexGaussian import *


def isGaussian(err_model):
    """
    Determine if the passed model is Gaussian or no.
    A model is Gaussian if it subclasses one of the classes implemented in this
    module with name containing the string *Gaussian*
    """
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    Gaussian_classes = tuple([cls[1] for cls in clsmembers if "Gaussian" in cls[0]])
    if isinstance(err_model, Gaussian_classes):
        return True
    else:
        return False

def isComplexGaussian(err_model):
    """
    Determine if the passed model is ComplexGaussian or no.
    A model is ComplexGaussian if it subclasses one of the *ComplexGaussian* classes
    implemented in this module
    """
    def _type_to_string(t):
        """Convert type of the passed object/type to string and strip all dependencies"""
        ...
        return str(t).strip(" < '>").split('.')[-1]

    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    ComplexGaussian_classes = tuple(
        [cls[1] for cls in clsmembers if "ComplexGaussian" in cls[0]]
    )

    err_model_class = _type_to_string(type(err_model))
    for ct in ComplexGaussian_classes:
        if err_model_class == _type_to_string(ct):
            return True
    return False


