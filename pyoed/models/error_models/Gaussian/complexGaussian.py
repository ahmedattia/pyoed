# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Classes for complex-valued error models: background, observation, and model error models.
Here, we implement two variants:

  1. :py:class:`ComplexGaussianErrorModel`: A general implementation for complex-valued
     random variables (vectors) following a Gaussian distribution.
     This implementation is valid for both proper and improper signals, that is, it
     properly handles the case where there is correlation between real and imaginary
     parts of the complex-valued random variable, moreover, it does not require
     real and imaginary parts to have equal covariances.

  2. :py:class:`ProperComplexGaussianErrorModel`: This is a special case of
     a complex Gaussian model (e.g., :py:class:`ComplexGaussianErrorModel`) where
     the pseudo covariance matrix (i.e., complementary covariance) vanishes.
     In this case, two conditions are satisfied:

       1. The real and imaginary parts of the random variable are independent
          (also uncorrelated).
       2. The real and imaginary parts have equal variance/covariance matrices.
"""

import sys
import inspect
import re

from dataclasses import (
    dataclass,
    replace,
)
from typing import Sequence
import numpy as np
import scipy.sparse as sp
from scipy.sparse import linalg as splinalg

from pyoed.configs import (
    validate_key,
    set_configurations,
    PyOEDConfigsValidationError,
)
from pyoed.models.core import (
    ErrorModel,
    ErrorModelConfigs,
)
from ._Gaussian import (
    GaussianErrorModel,
    GaussianErrorModelConfigs,
)
from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)

# Global (ish) variables
_LINSOLVE_TOL = 1e-12  # Base tolerance of linear system solves
_RANDOMIZATION_SAMPLE_SIZE = 50  # 250


@dataclass(kw_only=True, slots=True)
class ComplexGaussianErrorModelConfigs(ErrorModelConfigs):
    """
    Configuration settings for :py:class:`ComplexGaussianErrorModel`.
    See details about the complex Gaussian distribution in the description (docstring) of
    :py:class:`ComplexGaussianErrorModel`.

    .. note::
        By default, the `relation` is configured to be zero. This is mainly because the most
        commonly used form of complex Gaussian is the so-called proper complex Gaussian distribution.
        Zero-valued relation (pseudo-covariance) implies proper distribution (random variable).

    :param size: dimension of the error model space. Detected from mean if None.
    :param mean: mean of the error model
    :param variance: variance/covariance of the error model
    :param sparse: convert covariance to `scipy.csc_array` used if ``size > 1``
    :param map_to_real: apply computations (e.g., pdf, etc.) by mapping to the real domain
        using the duality between with the composite real vector
    :param random_seed: random seed used when the Gaussian model is initiated. This is
        useful for reproductivity.
    """

    size: int | None = None
    mean: complex | Sequence[complex] | np.ndarray = 0.0
    variance: complex | Sequence[complex] | Sequence[Sequence[complex]] | np.ndarray | sp.spmatrix = 1.0 + 0.0j
    relation: complex | Sequence[complex] | Sequence[Sequence[complex]]| np.ndarray | sp.spmatrix = 0.0 + 0.0j
    design: Sequence[int] | Sequence[bool] | np.ndarray = 1.0
    sparse: bool = True
    map_to_real: bool = False
    random_seed: int | None = None


@set_configurations(ComplexGaussianErrorModelConfigs)
class ComplexGaussianErrorModel(ErrorModel, RandomNumberGenerationMixin):
    """
    A simple Numpy-based (or scipy sparse) Complex-Valued Gaussian error model.
    A complex-valued random variable/vector ::math:`\\mathbf{Z}` is said to follow
    a Gaussian distribution ::math:`\\mathbf{Z}\\sim \\mathcal{CN}(\\mu, \\Gamma, C)` iff
    its real and imaginary parts are normally distributed. ::math:`\\Gamma` is
    a positive (semi-) definite covariance (defining total covariances of both real
    and imaginary parts) matrix with real-valued diagonals, and ::math:`C` is the
    relation matrix defining covariances between real and imaginary parts.

    This differs from :py:class:`ComplexGaussianErrorModel` in that it provides
    covariance matrix as well as a relation (pseudo-covariance) matrix.

    .. note::
        This model creates lazy objects under the hood. These are lazy in the sense that
        they are constructed/calculated only when they are needed, and are kept in memory
        unless the design changes. These objects are accessible through the following
        properties:

        1. `complex_augmented_stdev`: this refers to a lazy evaluation of the lower
           Cholesky factor of the complex augmented covariance matrix.

        2. `complex_augmented_stdev_inv`: this refers to a lazy evaluation of the inverse
           of the lower Cholesky factor of the complex augmented covariance matrix.

        3. `complex_augmented_stdev_logdet`: this refers to a lazy evaluation of the logarithm
           of the the determinant (logdet) of the lower Cholesky factor of the complex
           augmented covariance matrix.

        4. `real_composite_stdev`: this refers to a lazy evaluation of the lower
           Cholesky factor of the real composite covariance matrix.

        5. `real_composite_stdev_inv`: this refers to a lazy evaluation of the inverse
           of the lower Cholesky factor of the real composite covariance matrix.

        6. `real_composite_stdev_logdet`: this refers to a lazy evaluation of the logarithm
           of the the determinant (logdet) of the lower Cholesky factor of the complex
           real composite matrix.

    .. note::
        Currently the design is limited to be binary (on/off). No relaxation is supported.

    .. note::
        If mean, variance, or relation  is scalar, the size is inferred from `size` or from
        the argument with larger size, as long as there is no contradiction, otherwise
        an AssertionError is raised

    .. note::
        We use the fact that the logdet of the covariance matrix is obtained from the the
        Cholesky factorization as follows

        .. math::
            logdet (C) = log (det(L L^T)) = log (det(L) det(L^T)) = logdet(L) + logdet (L^T) ,

        Thus, :math:`0.5 logdet(C) = logdet(L)`, a fact used in calculating the (normalized)
        PDF, etc.

    :param dict | ComplexGaussianErrorModelConfigs | None configs: (optional)
        configurations for the goal-oriented inverse problem

    :raises PyOEDConfigsValidationError: if passed invalid configs
    :raises TypeError: if mean variance, or relation types are not supported (scalars and
          iterables are supported)
    :raises AssertionError : if there is a size mismatch between size, mean, and variance
    """

    def __init__(self, configs: ComplexGaussianErrorModelConfigs | dict | None = None):
        # Standardize the configurations & initialize
        super().__init__( configs=self.standardize_configurations(configs), )

        # TODO: Further initializations
        ...

        # Lazy initialization of crucial matrices and constants:
        self._COMPLEX_AUGMENTED_STDEV = None
        self._REAL_COMPOSITE_STDEV = None

        self._COMPLEX_AUGMENTED_STDEV_INV = None
        self._REAL_COMPOSITE_STDEV_INV = None

        self._COMPLEX_AUGMENTED_STDEV_LOGDET = None
        self._REAL_COMPOSITE_STDEV_LOGDET = None

        # Check the need to update anything related to the design
        self.update_design(design=self.configurations.design)

        # Assure the random seed
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

    def validate_configurations(
        self,
        configs: dict | ComplexGaussianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the Gaussian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations
            :py:class:`ComplexGaussianErrorModelConfigs`.
        """
        aggregated_configs = self.aggregate_configurations(configs)

        # Validate distribution parameters by trying to standardize them
        try:
            aggregated_configs = self.standardize_configurations(aggregated_configs)
        except (TypeError, ValueError) as err:
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    f"Failed type/shape validation!\n"
                    f"Unexpected {err=} of {type(err)=}"
                )
            return False

        ## Test other configurations

        # Local tests
        is_bool = lambda x: utility.isnumber(x) and (x == bool(x))
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        # `sparse` and `map_to_real`: bool
        for key in ["sparse", "map_to_real"]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=is_bool,
                message=f"'{key}' must be boolean!",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # `random_seed`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda x: is_positive_int(x) or x is None,
            message=f"'random_seed' must be positive integer or `None`!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # mean
        if "mean" in kwargs:
            self.update_mean(kwargs["mean"])

        # design
        if "design" in kwargs:
            self.update_design(kwargs["design"])

        # size
        if "size" in kwargs:
            raise TypeError(
                f"CANNOT MODIFY DIMENSIONALITY. You must create new error model"
            )

        # "variance"
        if "variance" in kwargs:
            self.update_covariance_matrix(kwargs["variance"])

        if "relation" in kwargs:
            self.update_relation_matrix(kwargs["relation"])

        if "random_seed" in kwargs:
            self.update_random_number_generator(random_seed=kwargs["random_seed"])

    def standardize_configurations(
        self,
        configs: ComplexGaussianErrorModelConfigs,
    ) -> GaussianErrorModelConfigs:
        """
        Check the configurations of the error model (size, mean, variance, relation,
        and design), and make sure they are consistent.
        Infer the size of the distribution from mean/variance/relation if not given explicitly.

        .. note::
            Here are the rules:

            1. If the `size` is passed, the mean and the design are expected to be one-dimensional
               arrays/vectors of this size (if either is scalar, they are casted to vectors),
               and the variance's size/shape is asserted accordingly.
            2. If the `size` is not passed, it is inferred from `mean`, `variance` and/or `relation`.

        :param configs: the configurations object to be standardized.
        :returns: a modified version (instance of :py:class:`ComplexGaussianErrorModelConfigs`)
            of the passed `configs` with consistent configurations.

        :raises PyOEDConfigsValidationError: if the validation of the configurations fail
        """
        # First Aggregate and convert to the proper (expected) type
        configs = self.configurations_class.data_to_dataclass(configs)

        # Extract Distribution Elements from Configurations:
        size = configs.size
        mean = configs.mean
        variance = configs.variance
        relation = configs.relation
        design = configs.design
        sparse = configs.sparse

        # Data types (for clarity)
        mean_dtype = complex
        variance_dtype = complex
        relation_dtype = complex
        design_dtype = bool

        # ====================================================================================
        ## TYPE CHECK:
        ##   Check mean, design and variance (and relation) types and
        ##   convert to numpy/sparse arrays (based on the value of configs.sparse).
        # ====================================================================================
        # 1) Mean: make sure it is 1-d numpy array (of dtype complex)
        # -----------------------------------------------------------
        try:
            mean = utility.asarray(mean, dtype=mean_dtype, )
            if 1 in mean.shape: mean = mean.flatten()

        except (Exception) as err:
            raise PyOEDConfigsValidationError(
                f"`mean` is of unsupported type: {type(mean)}; "
                f"See further details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )

        if mean.ndim != 1:
            raise PyOEDConfigsValidationError(
                f"The mean is expected to be one dimensional array! "
                f"After validation -> {mean.shape=}"
            )

        # 2) Design: make sure it is 1-d numpy array
        # -----------------------------------------------------------
        if design is None: design = 1
        try:
            _design = utility.asarray(design, dtype=design_dtype)  # This is supposed to be int/bool!
            if 1 in _design.shape: _design = _design.flatten()
        except (Exception) as err:
            raise PyOEDConfigsValidationError(
                f"`design` is of unsupported type: {type(design)}; "
                f"See further details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
        else:
            if any (_design != design):
                raise PyOEDConfigsValidationError(
                    f"Only Boolean design is currently supported"
                )
            else:
                design = _design

        if design.ndim != 1:
            raise PyOEDConfigsValidationError(
                f"The design is expected to be one dimensional array! "
                f"After validation -> {design.shape=}"
            )

        # 3) Variance/Covariance: make sure it is 1- or 2-d numpy array (or scipy sparse matrix)
        # -----------------------------------------------------------
        if utility.isnumber(variance):
            # A scalar
            variance = utility.asarray([variance])

        elif isinstance(variance, np.ndarray):
            # Numpy array
            variance = utility.asarray(variance, dtype=variance_dtype, )
            if 1 in variance.shape or variance.size == 1: variance = variance.flatten()

        elif sp.issparse(variance):
            # Numpy Scipy array/matrix
            # Check ndim; if 1 -> 1D numpy array (diagonal of covariances)
            if 1 in variance.shape  or variance.size == 1:
                variance = utility.asarray(variance, dtype=variance_dtype).flatten()

        else:
            # Something else (e.g., list, etc.)
            try:
                variance = utility.asarray(variance, dtype=variance_dtype, )
                if 1 in variance.shape: variance= variance.flatten()

            except (Exception) as err:
                raise PyOEDConfigsValidationError(
                    f"`variance` is of unsupported type: {type(variance)}; "
                    f"Tried casting `variance` to a numpy array but failed! "
                    f"See further details below: \n"
                    f"Unexpected {err=} of {type(err)=}"
                )
        # At this point, `variance` is either numpy or scipy array

        # 4) Relation (pseudo-covariance) make sure it is 1- or 2-d numpy array (or scipy sparse matrix)
        # -----------------------------------------------------------
        if utility.isnumber(relation):
            # A scalar
            relation = utility.asarray([relation])

        elif isinstance(relation, np.ndarray):
            # Numpy array
            relation = utility.asarray(relation, dtype=relation_dtype, )
            if 1 in variance.shape or variance.size == 1: variance = variance.flatten()

        elif sp.issparse(variance):
            # Numpy Scipy array/matrix
            # Check ndim; if 1 -> 1D numpy array (diagonal of covariances)
            if 1 in variance.shape  or variance.size == 1:
                variance = utility.asarray(variance, dtype=relation_dtype).flatten()

        else:
            # Something else (e.g., list, etc.)
            try:
                relation = utility.asarray(relation, dtype=relation_dtype, )
                if 1 in relation.shape: relation = relation.flatten()

            except (Exception) as err:
                raise PyOEDConfigsValidationError(
                    f"`relation` is of unsupported type: {type(relation)}; "
                    f"Tried casting `relation` to a numpy array but failed! "
                    f"See further details below: \n"
                    f"Unexpected {err=} of {type(err)=}"
                )
        # At this point, `relation` is either numpy or scipy array
        # ====================================================================================

        # Make sure mean, design, variance, and relation are not empty
        if mean.size < 1 or np.prod(variance.shape) < 1 or np.prod(relation.shape) < 1 or design.size < 1:
            raise PyOEDConfigsValidationError(
                f"mean, variance relation, and design must be valid (non-empty)! \n"
                f"{mean=} with {mean.shape=}\n"
                f"{variance=} with {variance.shape=}\n"
                f"{relation=} with {relation.shape=}\n"
                f"{design=} with {design.shape=}"
            )

        ## Extract/Infer size (if not explicitly passed), otherwise make sure parameters are conformable with it
        if size is None:
            # No size given, infer from mean, design, variance, or relation in this order

            # Infer size from mean:
            # ---------------------
            if mean.size == design.size == variance.size == relation.size == 1:
                # All are scalars
                size = 1

            elif mean.size > 1:
                # Set the size to the mean size
                size = mean.size

            elif design.size > 1:
                # Mean is scalar; Set the size to the design size
                size = design.size

            else:
                # Mean & design are both scalars; extract size from variance
                if variance.size == relation.size == 1:
                    # Variance and relation are 1d with one entry (scalars)
                    size = 1

                elif variance.ndim == 1 and variance.size > 1:
                    # variance is 1d (diagonal of covariances)
                    size = variance.size

                elif relation.ndim == 1 and relation.size > 1:
                    # relation is 1d (diagonal of covariances)
                    size = relation.size

                elif variance.ndim == 2 and variance.shape[0] == variance.shape[1]:
                    size = variance.shape[0]

                elif relation.ndim == 2 and relation.shape[0] == relation.shape[1]:
                    size = relation.shape[0]

                else:
                    raise PyOEDConfigsValidationError(
                        f"Failed to extract size from variance and/or relation matrices; "
                        f"expected two dimensional array/matrix; found array(s) of shape:"
                        f"{variance.shape=}\n"
                        f"{relation.shape=}"
                    )

        # Make sure the given/inferred size is positive
        if not (utility.isnumber(size) and size==int(size) > 0):
            raise PyOEDConfigsValidationError(
                f"Invalid {size=} of {type(size)=}; expected positive integer"
            )

        ## Given the size passed/inferred; expand mean/design/variance/relation properly
        ## =============================================================================
        # mean & design: 1d of size `size`
        if mean.size == 1: mean = mean.repeat(size)
        if design.size == 1: design = design.repeat(size)

        # if variance or relation is scalar -> 1D array of size `size`
        if variance.size == 1: variance = variance.repeat(size)
        if relation.size == 1: relation = relation.repeat(size)


        # variance: -> 2D array of shape (`size` x `size`)
        if variance.ndim == 1 and variance.size == size:
            # Variance is 1D vector (take as the diagonal of the covariance matrix)
            if sparse:
                variance = sp.csc_array(sp.diags(variance, offsets=0, format="csc", dtype=variance_dtype, ))
            else:
                variance = np.diag(variance, k=0, ).astype(variance_dtype)

        elif variance.shape == (size, size):
            if sparse and not sp.issparse(variance):
                variance = sp.csc_array(variance, dtype=variance_dtype, )
            elif not sparse and sp.issparse(variance):
                variance = variance.toarray().astype(variance_dtype)

        else:
            raise PyOEDConfigsValidationError(
                f"Invalid `variance` array/matrix. \n"
                f"Expected 2d square array of shape ({size}, {size})\n"
                f"Received (or casted to) `{variance=}; {variance.ndim=}; of {variance.shape=};"
            )

        # relation: -> 2D array of shape (`size` x `size`)
        if relation.ndim ==1 and relation.size == size:
            # relation is 1D vector (take as the diagonal of the covariance matrix)
            if sparse:
                relation = sp.csc_array(sp.diags(relation, offsets=0, format="csc", dtype=relation_dtype, ))
            else:
                relation = np.diag(relation, k=0, ).astype(relation_dtype)

        elif relation.shape == (size, size):
            if sparse and not sp.issparse(relation):
                relation = sp.csc_array(relation, dtype=relation_dtype, )
            elif not sparse and sp.issparse(relation):
                relation = relation.toarray().astype(relation_dtype)

        else:
            raise PyOEDConfigsValidationError(
                f"Invalid `relation array/matrix. \n"
                f"Expected 2d square array of shape ({size}, {size})\n"
                f"Received (or casted to) `{relation=}; {relation.ndim=}; of {relation.shape=};"
            )


        # Final sanity check in case anything is missing!
        if (
            mean.size != size or
            design.size != size or
            variance.shape != (size, size) or
            relation.shape != (size, size)
        ):
            raise PyOEDConfigsValidationError(
                f"Inconsistency in the dimensionality of some (or all) of the "
                f"distribution components: .\n"
                f" {size=}\n "
                f" {mean.shape=}\n "
                f" {design.shape=}\n "
                f" {variance.shape=}\n"
                f" {relation.shape=}\n This SHOULD NEVER HAPPEN! PLEASE REPORT THIS BUG!"
            )

        # Replace and return the configurations (in place with validated versions)
        return replace(
            configs,
            size=size,
            mean=mean,
            design=design,
            variance=variance,
            relation=relation,
            sparse=sparse,
        )

    def _validate_vector(
        self, z,
    ):
        """
        Check the validity of the passed vector, and return a numpy array
        representation of the vector with `complex` data type.

        :raises TypeError: if `z` is of invalid type/shape/size
        """
        z = utility.asarray(
            z,
            dtype=complex,
        ).flatten()

        if z.size != self.active_size:
            raise TypeError(
                f"Size mismatch; expected vector z of size {self.active_size}; "
                f"received vector of size {z.size=}"
            )
        return z

    def log_density(
        self,
        z: Sequence[complex],
        normalize: bool = False,
    ) -> float:
        """
        Evaluate the logarithm of the density function evaluated at the passed
        commplex random variable realization `z`.

        .. note::
            Due to duality between real and complex representation, we calculate the
            log-density either by using the real or the complex formulation
            based on the configurations key `self.configurations.map_to_real`.

        :param z: complex-valued random vector/variable
        :param bool normalize: if `False` ignore the normalization constant,
            otherwise employ it in the calculations

        :returns: value of the logarithm of the density function (with/without)
            normalization constant

        :raises TypeError: if `z` is of invalid type/shape/size
        """
        if self.configurations.map_to_real:
            return self._log_density_real(
                z,
                normalize=normalize,
            )
        else:
            return self._log_density_complex(
                z,
                normalize=normalize,
            )

    def _log_density_real(
        self,
        z,
        normalize=False,
    ):
        """
        Evaluate the logarithm of the density function evaluated at the passed
        commplex random variable realization `z`.

        This method uses the real-valued representation of the composite real vector
        composed of the real and the imaginary parts of `z`.
        The distribution of the real composite vector is jointly Gaussian with
        mean, covariances and cross covariances extracted from the covariance and
        pseudo covariances of the complex-valued distribution.

        :param z: complex-valued random vector/variable
        :param bool normalize: if `False` ignore the normalization constant, otherwise
            employ it in the calculations

        :raises TypeError: if `z` is of invalid type/shape/size
        """
        # Check the passed vector
        z = self._validate_vector(z)

        ## Convert the random variable into the real composite version

        # Real and Imaginary components of the passed vector
        z_composite = np.concatenate([z.real, z.imag])

        # Real and Imaginary components of the mean
        mean = self.mean
        mean_composite = np.concatenate([mean.real, mean.imag])

        # The inverse of the real composite vector lower cholesky factor
        stdev = self.real_composite_stdev_inv

        stdev_innov = stdev.T @ (z_composite - mean_composite)
        log_pdf = - 0.5 * (stdev @ stdev_innov)

        if normalize:
            # Evaluate log of the normalization constant
            scl = - self.active_size * np.log(2 * np.pi)
            scl -= self.real_composite_stdev_logdet

            # Add the normalization constant
            log_density += scl

        return log_density

    def _log_density_complex(
        self,
        z,
        normalize=False,
    ):
        """
        Evaluate the logarithm of the density function evaluated at the passed
        commplex random variable realization `z`.

        This method uses the complex augmented representation of the complex vector
        composed of the vector `z` and its complex conjugate.

        :param z: complex-valued random vector/variable
        :param bool normalize: if `False` ignore the normalization constant, otherwise
            employ it in the calculations

        :raises TypeError: if `z` is of invalid type/shape/size
        """
        # Check the passed vector
        z = self._validate_vector(z)


        ## Convert the random variable into the complex augmented version

        # Real and Imaginary components of the passed vector
        z_augmented = np.concatenate([z, z.conjugate()])

        # Real and Imaginary components of the mean
        mean = self.mean
        mean_composite = np.concatenate([mean, mean.conjugate()])

        # The inverse of the real composite vector lower cholesky factor
        stdev = self.complex_augmented_stdev_inv

        stdev_innov = stdev.T @ (z_augmented - mean_augmented)
        log_pdf = - 0.5 * (stdev @ stdev_innov.conjugate())

        if normalize:
            # Evaluate log of the normalization constant
            scl = - self.active_size * np.log(np.pi)
            scl -= self.complex_augmented_stdev_logdet

            # Add the normalization constant
            log_density += scl

        return log_density

    def real_composite_covariance_matrix(self, ):
        """
        Calculate the real composite covariance matrix. This is a matrix with
        four blocks defining covariances of the real and imaginary parts and
        the cross-covariances between real and imaginary parts.

        :returns: 2D array representing covariances of the composite (real
            and imaginary) parts of the random variable/vector.
            The returned array is either dense (Numpy.ndarray) or sparse
            (scipy.sparse.csc_array) based on the internal flag `sparse`
            selected in the model configurations.
        """
        # Block matrix constructor
        bmat = sp.block_array if self.sparse else np.bmat

        # Cov: The covariance matrix (of the complex random variable)
        Cov = self.configurations.variance[self.active, :][:, self.active]

        # PCov: The Pseudo-covariance (Relation) matrix (of the complex random variable)
        PCov = self.configurations.relation[self.active, :][:, self.active]

        ## Extract covariance (and cross-covariances) of the real and imaginary parts
        # Covariance of the real components
        C_RR = 0.5 * (Cov.real + PCov.real)

        # Covariance of the imaginary components
        C_II = 0.5 * (Cov.real - PCov.real)

        # Cross covariances of real and imaginary components
        C_RI = 0.5 * (PCov.imag - Cov.imag)

        # The real composite covariance matrix
        composite_covariances = bmat(
            [
                [ C_RR,  C_RI],
                [C_RI.T, C_II]
            ]
        )
        return composite_covariances

    def covariance_matrix(self, ):
        r"""
        Calculate the covariance matrix and return it.
        The covariance matrix is defined as:

        .. math::
            \mathbb{E} (x - \mu ) ( x - \mu )^{H}
            = \mathbb{E} \left(x - \mathbb{E}(x) \right)
                \left( x - \mathbb{E}(x) \right)^{H} \,,

            where ::math:`x` is the random variable, ::math:`\mu = \mathbb{E}[x]` is the
            distribution mean, and ::math:`x^{H}` is the Hermitian (complex-conjugate transpose)
            of a complex vector/matrix ::math:`x`.

        :returns: 2D array representing covariances of the complex variable.
            The returned array is either dense (Numpy.ndarray) or sparse
            (scipy.sparse.csc_array) based on the internal flag `sparse`
            selected in the model configurations.
        """
        # Copy the covariance matrix (of the complex random variable)
        return self.configurations.variance[self.active, :][:, self.active].copy()


    def precision_matrix(self, ):
        r"""
        Calculate the precision matrix. This is just the inverse of the
        covariance matrix.

        :returns: 2D array representing inverse of the covariances of the complex variable.
            The returned array is either dense (Numpy.ndarray) or sparse
            (scipy.sparse.csc_array) based on the internal flag `sparse`
            selected in the model configurations.
        """
        # Function to calculate the inverse
        inverter = sp.linalg.inv if self.sparse else np.linalg.inv

        return inverter(self.covariance_matrix())


    def pseudo_covariance_matrix(self, ):
        r"""
        Calculate the relation (pseudo-covariance) matrix and return it.
        The relation matrix is defined as:

        .. math::
            \mathbb{E} (x - \mu ) ( x - \mu )^{T}
            = \mathbb{E} \left(x - \mathbb{E}(x) \right)
                \left( x - \mathbb{E}(x) \right)^{T} \,,

            where ::math:`x` is the random variable, ::math:`\mu = \mathbb{E}[x]` is the
            distribution mean, and ::math:`x^{T}` is the transpose of ::math:`x`.

        :returns: 2D array representing relation pseudo-covariances of the complex variable.
            The returned array is either dense (Numpy.ndarray) or sparse
            (scipy.sparse.csc_array) based on the internal flag `sparse`
            selected in the model configurations.
        """
        # Copy the covariance matrix (of the complex random variable)
        return self.configurations.relation[self.active, :][:, self.active].copy()
    ## Alias
    relation_matrix = pseudo_covariance_matrix

    def complex_augmented_covariance_matrix(self, ):
        """
        Calculate the augmented covariance matrix and return it (full).

        :returns: 2D array representing covariances of the augmented
            (complex variable and its conjugate) of the random variable/vector.
            The returned array is either dense (Numpy.ndarray) or sparse
            (scipy.sparse.csc_array) based on the internal flag `sparse`
            selected in the model configurations.
        """
        # Block matrix constructor
        bmat = sp.block_array if self.sparse else np.bmat

        # Cov: The covariance matrix (of the complex random variable)
        Cov = self.configurations.variance[self.active, :][:, self.active]

        # PCov: The Pseudo-covariance (Relation) matrix (of the complex random variable)
        PCov = self.configurations.relation[self.active, :][:, self.active]

        # The complex augmented covariance matrix
        augmented_covariances = bmat(
            [
                [ Cov,  PCov],
                [PCov.conjugate(), Cov.conjugate()]
            ]
        )
        return augmented_covariances

    def update_design(self, design):
        """
        If the design matches existing one, don't do anything, otherwise, update it.
        """
        if design is None:
            _design = design = np.ones(self.size, dtype=bool)
        else:
            _design = utility.asarray(design, dtype=bool)

        if any(_design != design):
            raise TypeError(
                f"The design is only allowed to be boolean"
            )
        else:
            design = _design

        if design.size != self.size:
            raise TypeError(
                f"The design must be 1d array of size {self.size}\n"
                f"Received {design=} with {design.shape=}"
            )

        if any(design != self.configurations.design):
            # Update configurations
            self.configurations.design = design

            # Destroy the lower Cholesky factor (& inverse, & logdet) of projected covariance
            # matrix and rebuild when needed
            self._COMPLEX_AUGMENTED_STDEV = None
            self._REAL_COMPOSITE_STDEV = None

            self._COMPLEX_AUGMENTED_STDEV_INV = None
            self._REAL_COMPOSITE_STDEV_INV = None

            self._COMPLEX_AUGMENTED_STDEV_LOGDET = None
            self._REAL_COMPOSITE_STDEV_LOGDET = None

        else:
            if self.verbose:
                print(
                    f"The passed design is not different from current one. "
                    f"Skipping design update..."
                )

    def update_mean(self, mean):
        """
        Update the mean
        """
        mean = utility.asarray(mean, dtype=complex)

        if mean.size != self.size:
            raise TypeError(
                f"The mean must be 1d array of size {self.size}\n"
                f"Received {mean=} with {mean.shape=}"
            )

        if any(mean!= self.configurations.mean):
            # Update configurations
            self.configurations.mean = mean

        else:
            if self.verbose:
                print(
                    f"The passed mean is not different from current one. "
                    f"Skipping mean update..."
                )

    def update_covariance_matrix(self, variance):
        """Update the covariance matrix"""
        try:
            configs = self.configurations.asdict()
            configs.update({'variance': variance})
            configs=self.standardize_configurations(configs)
            self.configurations.variance = configs.variance

        except Exception as err:
            raise PyOEDConfigsValidationError(
                f"Failed to validate/standardize the aggregated configurations "
                f"given the passed covariance (variance) matrix. "
                f"See caught error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )


    def update_relation_matrix(self, relation):
        """Update the relation (pseudo covariance) matrix"""
        try:
            configs = self.configurations.asdict()
            configs.update({'relation': relation})
            configs=self.standardize_configurations(configs)
            self.configurations.relation = configs.relation

        except Exception as err:
            raise PyOEDConfigsValidationError(
                f"Failed to validate/standardize the aggregated configurations "
                f"given the passed covariance (variance) matrix. "
                f"See caught error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )

    def generate_white_noise(
        self,
        ignore_design=False,
        truncate=False,
    ):
        """
        Safely generate white noise: normal random noise with zero mean and variance 1
        for each of the real and the imaginary parts. Note that scaling is taken care
        of by the (augmented) covariance matrices.

        This function returns two entries, noise_re, noise_im providing white noise
        for the real and the imaginary parts consecutively.
        For each of the real and imaginary parts, the returned vector has size equal
        to the size of the active entries as specified by the design.

        :param bool ignore_design: the size of the white noise vector is in the
            full (ignore the design) if `True`, otherwise the size matches the dimension
            of the active subspace.
        :param bool truncate: if `True`, truncate the samples at -/+3, that is any sample
            point/entry above 3 is set to 3, and any value below -3 is set to -3.

        :returns: complex-valued white noise (composed of white noise of
            the real and the imaginary parts assuming they are independent which noises)
        """
        active_size = self.size if ignore_design else self.active_size
        size = active_size * 2

        # Generate white noise with variance 1
        white_noise = self.random_number_generator.standard_normal(size)

        # Split the noise vector
        noise_re = white_noise[:active_size]
        noise_imag = white_noise[active_size:]

        # Truncate (real and imaginary parts) if requested
        if truncate:
            noise_re[noise_re > 3] = 3
            noise_re[noise_re < -3] = -3

            noise_imag[noise_imag > 3] = 3
            noise_imag[noise_imag < -3] = -3

        # return results
        return noise_re + 1.0j *noise_imag

    def generate_noise(self):
        """
        Generate a random noise vector sampled from the underlying Complex-Valued
        Gaussian distribution.
        This sampling is generic and does not assume circular symmetry
        """
        # complex-valued white noise
        white_noise = self.generate_white_noise(ignore_design=False)

        # Scale the aggregated vector by standard deviation (in the real space)
        if self.configurations.map_to_real:
            # Convert the complex noise to a composite real vector
            noise_composite = np.concatenate([white_noise.real, white_noise.imag])
            noise_composite[:] = self.real_composite_stdev @ noise_composite

            # Map to the complex space
            noise = noise_composite[: self.active_size] + 1j * noise_composite[self.active_size :]

        else:
            # Convert the complex noise to a complex augmented vector
            noise_augmented = np.concatenate([white_noise, white_noise.conjugate()])
            noise_augmented[:] = self.complex_augmented_stdev @ noise_augmented
            noise_augmented *= 1.0 / np.sqrt(2)  # NOTE: This is because we use `z` and conj

            # Extract first half (samples of `z` and drop samples of the conjugate)
            if True:
                noise = noise_augmented[: self.active_size] + noise_augmented[self.active_size: ].conjugate()
                noise /= 2.0
            else:
                # This is wrong <<TODO: RMOVE>>...
                noise_re = (noise_augmented[: self.active_size] + noise_augmented[self.active_size: ].conjugate() )
                noise_im = (noise_augmented[: self.active_size] - noise_augmented[self.active_size: ].conjugate() ) / 1.0j
                noise = 0.5 * (noise_re + 1.0j * noise_im)
        return noise

    def add_noise(self, z, in_place=False):
        """
        Add random noise to the passed vector `x`

        :param x: vector in a space of dimension equal to size of the undrlying error model
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :raises: TypeError if x is of the wrong shape or type
        """
        _z = self._validate_vector(z)

        if not isinstance(z, np.ndarray):
            warnings.warn(
                f"Cannot write in-place since {typ(z)=}; Expected numpy array!"
                f"Returning a new vector (numpy array), that is in_place is ignored"
            )
            in_place = False

        if in_place and not np.issubdtype(z.dtype, complex):
            warnings.warn(
                f"Cannot write in-place since {z.dtype=}; Expected dtype to be complex\n"
                f"Returning a new vector, that is in_place is ignored"
            )
            in_place = False
        out = z if in_place else _z

        # Scale and shift the white noise
        out += self.generate_noise()

        return out

    def sample(self):
        """
        Sample a random vector from the underlying Complex-Valued Gaussian distribution
            This sampling is generic and does not assume circular symmetry
        """
        # Add a scaled random noise (with the underlying covariance matrix) to the underlying mean
        out_x = self.mean + self.generate_noise()
        return out_x

    def complex_augmented_covariance_matrix_from_sample(
        self,
        sample,
        ddof=1,
    ):
        """
        Evaluate the complex augmented covariance matrix from a sample.
        This method calculates the sample based covariances and pseudo covariances
        and use them to construct the augmented covariance matrix.

        :parm sample: ArrayLike iterable of two dimensions. Each row represetns
            a sample of the underlying distribution
        :param int ddof: `ddof=1` will return unbiased estimate since in the
            covariance formula, we divide by sample_size-ddof.

        :returns:
            - `covariances`: sample-based covariance matrix
            - `pseudo_covariances`: sample-based pseudo covariance matrix
        """
        # Extract sample size and probability space dimension
        sample = np.asarray(sample, dtype=complex)
        if np.ndim(sample) == 1:
            np.reshape(sample, (sample.size, 1))

        # Extract sample size
        sample_size = len(sample)

        # Innovation (sample shifted to center around the mean)
        innov = sample - np.mean(sample, axis=0)

        # Covariances and Pseudo Covariances (Relations)
        scl  = 1.0 / (sample_size - ddof)
        Cov  = scl * (innov.T @ innov.conjugate()).T
        PCov = scl * (innov.T @ innov).T

        # Block matrix constructor
        bmat = sp.block_array if self.sparse else np.bmat

        # The complex augmented covariance matrix
        augmented_covariances = bmat(
            [
                [ Cov,  PCov],
                [PCov.conjugate(), Cov.conjugate()]
            ]
        )
        return augmented_covariances

    def real_composite_covariance_matrix_from_sample(
        self,
        sample,
        ddof=1,
    ):
        """
        Evaluate the real composite covariance matrix from a sample.
        This method calculates the covariance matrix of the composite real vector
        composed of the real and the imaginary parts of the complex vector.

        :parm sample: ArrayLike iterable of two dimensions. Each row represetns
            a sample of the underlying distribution
        :param int ddof: `ddof=1` will return unbiased estimate since in the
            covariance formula, we divide by sample_size-ddof.

        :returns:
            - `covariances`: sample-based covariance matrix
            - `pseudo_covariances`: sample-based pseudo covariance matrix
        """
        # Extract sample size and probability space dimension
        sample = np.asarray(sample, dtype=complex)
        if np.ndim(sample) == 1:
            np.reshape(sample, (sample.size, 1))

        # Extract sample size
        sample_size = len(sample)

        # Innovation (sample shifted to center around the mean)
        innov = sample - np.mean(sample, axis=0)

        innov_real = innov.real
        innov_imag = innov.imag

        # Scaling factor
        scl  = 1.0 / (sample_size - ddof)

        # Covariance of real part
        C_RR = scl * (innov_real.T @ innov_real)
        C_II = scl * (innov_imag.T @ innov_imag)
        C_RI = scl * (innov_real.T @ innov_imag)


        # Block matrix constructor
        bmat = sp.block_array if self.sparse else np.bmat

        # Covariances of the real composite vector
        composite_covariances = bmat(
            [
                [C_RR, C_RI],
                [C_RI.T, C_II]
            ]
        )

        return composite_covariances

    @property
    def sparse(self):
        """True if the covariance data structure is constructed using scipy.spmatrix)"""
        return self.configurations.sparse

    @property
    def size(self):
        """Dimension of the underlying probability distribution"""
        return self.configurations.size

    @property
    def design(self):
        """Get a copy of the design vector"""
        return self.configurations.design
    @design.setter
    def design(self, val):
        self.update_configurations(design=val)

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for
        multiple prognostic variables) of the observation vector/range Right now, the
        model is agnostic to any prognostic (physics) variables and is left to other
        modules to handle such periodicity if exists.  Thus, this function is identical
        to the :py:meth:`design`
        """
        return self.design

    @property
    def active(self):
        """flags corresponding to active/inactive dimensions"""
        return np.where(self.design)[0]

    @property
    def active_size(self):
        """
        Dimension of the probability distribution projected onto the design
        space (only active/nonzero entries of the design)
        """
        return self.active.size

    @property
    def mean(self):
        """Get a copy of the distribution mean"""
        return self.configurations.mean[self.active]
    @mean.setter
    def mean(self, val):
        self.update_configurations(mean=val)

    @property
    def real_composite_stdev(self, ):
        """
        Construct and return the lower Cholesky factor (if not available) in the
        projected space (after applying the design) of the real composite
        covariance matrix
        """
        if self._REAL_COMPOSITE_STDEV is None:
            if self.verbose:
                print(
                    f"Generating lower Cholesky factor of the real composite "
                    f"covariance matrix"
                )

            self._REAL_COMPOSITE_STDEV = utility.factorize_spsd_matrix(
                self.real_composite_covariance_matrix(),
                shape=(self.active_size, self.active_size),
            )

        # Return it
        return self._REAL_COMPOSITE_STDEV

    @property
    def real_composite_stdev_inv(self, ):
        """
        Construct and return the inverse of the lower Cholesky factor (if not
        available) in the projected space (after applying the design)
        of the real composite covariance matrix
        """
        if self._REAL_COMPOSITE_STDEV_INV is None:
            if self.verbose:
                print(
                    f"Generating the inverse of the lower Cholesky factor of "
                    f"the real composite covariance matrix"
                )

            # Function to calculate the inverse
            inverter = sp.linalg.inv if self.sparse else np.linalg.inv

            self._REAL_COMPOSITE_STDEV_INV = inverter(self.real_composite_stdev)

        # Return it
        return self._REAL_COMPOSITE_STDEV

    @property
    def real_composite_stdev_logdet(self, ):
        """
        Evaluate the log-det (logarithm of the determinant) of the lower Cholesky
        factor (if not available) in the projected space (after applying the design)
        of the real composite covariance matrix
        """
        if self._REAL_COMPOSITE_STDEV_LOGDET is None:
            if self.verbose:
                print(
                    f"Evaluating the logdet of the lower Cholesky factor of "
                    f"the real composite covariance matrix"
                )

            self._REAL_COMPOSITE_STDEV_LOGDET = utility.matrix_logdet(
                self.real_composite_stdev,
                method="exact",
            )

        # Return it
        return self._REAL_COMPOSITE_STDEV_LOGDET

    @property
    def complex_augmented_stdev(self, ):
        """
        Construct and return the lower Cholesky factor (if not available) in the
        projected space (after applying the design) of the complex augmented
        covariance matrix
        """
        if self._COMPLEX_AUGMENTED_STDEV is None:
            if self.verbose:
                print(
                    f"Generating lower Cholesky factor of the complex "
                    f"augmented covariance matrix"
                )

            self._COMPLEX_AUGMENTED_STDEV = utility.factorize_spsd_matrix(
                self.complex_augmented_covariance_matrix(),
                shape=(self.active_size, self.active_size),
            )
        return self._COMPLEX_AUGMENTED_STDEV

    @property
    def complex_augmented_stdev_inv(self, ):
        """
        Construct and return the inverse of the lower Cholesky factor (if not
        available) of the complex augmented covariance matrix
        in the projected space (after applying the design)
        """
        if self._COMPLEX_AUGMENTED_STDEV_INV is None:
            if self.verbose:
                print(
                    f"Generating the inverse of the lower Cholesky factor of "
                    f"the complex augmented covariance matrix"
                )

            # Function to calculate the inverse
            inverter = sp.linalg.inv if self.sparse else np.linalg.inv

            self._COMPLEX_AUGMENTED_STDEV_INV = inverter(self.complex_augmented_stdev)

        # Return it
        return self._COMPLEX_AUGMENTED_STDEV_INV

    @property
    def complex_augmented_stdev_logdet(self, ):
        """
        Evaluate the log-det (logarithm of the determinant) of the lower Cholesky
        factor (if not available) in the projected space (after applying the design)
        of the complex augmented covariance matrix
        """
        if self._COMPLEX_AUGMENTED_STDEV_LOGDET is None:
            if self.verbose:
                print(
                    f"Evaluating the logdet of the lower Cholesky factor of "
                    f"the complex augmented covariance matrix"
                )

            self._COMPLEX_AUGMENTED_STDEV_LOGDET = utility.matrix_logdet(
                self.complex_augmented_stdev,
                method="exact",
            )

        # Return it
        return self._COMPLEX_AUGMENTED_STDEV_LOGDET

    @property
    def map_to_real(self, ):
        """
        Whether to map to real domain (of `True`) for calculations
        (i.e., use the real-composite formulation)
        or to use the complex augmented formulation
        """
        return self.configurations.map_to_real
    @map_to_real.setter
    def map_to_real(self, val):
        """Update `map_to_real`"""
        return self.update_configurations(map_to_real=val)



##################################################################################
##                               Helper Functions                               ##
##################################################################################
def real_to_complex_covariances(
    C_RR,
    C_II,
    C_RI,
):
    """
    Convert covariances and cross covariances or real and imaginary parts
    of a complex random variable into covariances and pseudo-covariances
    (relations) of the complex vector

    :param C_RR: covariances of the real part of a complex random vector/variable
    :param C_II: covariances of the imaginary part of a complex random vector/variable
    :param C_RI: cross-covariances of the real with the imaginar part of
        a complex random vector/variable

    :returns: Two matrices in the following order:
        - Cov: the covariances of the complex variable
        - PCov: the pseudo covariances
    """
    C_RR = utility.asarray(C_RR, dtype=float, )
    C_II = utility.asarray(C_II, dtype=float, )
    C_RI = utility.asarray(C_RI, dtype=float, )

    if not (
        C_RR.shape == C_II.shape == C_RI.shape and
        C_RR.ndim==2 and
        C_RR.shape[0] == C_RR.shape[1] > 0
    ):
        raise TypeError(
            f"Invalid or inconsistent covariance matrices; "
            f"expected square matrices of equal shpaes;\n"
            f"Received: \n"
            f"{C_RR.shape=}\n"
            f"{C_II.shape=}\n"
            f"{C_RI.shape=}"
        )

    Cov = C_RR + C_II + 1j * (C_RI.T - C_RI)
    PCov = C_RR - C_II + 1j * (C_RI.T + C_RI)
    return Cov, PCov

def complex_to_real_covariances(
    Cov,
    PCov,
):
    """
    Convert covariances and cross covariances or real and imaginary parts to
    covariances and pseudo-covariances (relations) of the complex vector

    :param Cov: the covariances of the complex variable
    :param PCov: the pseudo covariances

    :returns: Three matrices in the following order:
        - C_RR: covariances of the real part of a complex random vector/variable
        - C_II: covariances of the imaginary part of a complex random vector/variable
        - C_RI: cross-covariances of the real with the imaginary part of
          a complex random vector/variable
    """
    Cov = utility.asarray(Cov, dtype=complex, )
    PCov = utility.asarray(PCov, dtype=complex, )

    if not (
        Cov.shape == PCov.shape and
        Cov.ndim==2 and
        Cov.shape[0] == Cov.shape[1] > 0
    ):
        raise TypeError(
            f"Invalid or inconsistent covariance matrices; "
            f"expected square matrices of equal shpaes;\n"
            f"Received: \n"
            f"{Cov.shape=}\n"
            f"{PCov.shape=}"
        )

    ## Extract covariance (and cross-covariances) of the real and imaginary parts
    # Covariance of the real components
    C_RR = 0.5 * (Cov.real + PCov.real)

    # Covariance of the imaginary components
    C_II = 0.5 * (Cov.real - PCov.real)

    # Cross covariances of real and imaginary components
    C_RI = 0.5 * (PCov.imag - Cov.imag)

    return C_RR, C_II, C_RI


