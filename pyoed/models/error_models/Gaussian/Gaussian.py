# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides access to several flavors of Gaussian Error Models
with various formulations of the design effect.
"""

import sys
import numpy as np
import inspect

from pyoed import utility

# ================================================================================ #
#                        Import Gaussian Error Model Classe                        #
# ================================================================================ #
from ._Gaussian import (
    GaussianErrorModelConfigs,
    GaussianErrorModel,
    _LINSOLVE_TOL,
    _RANDOMIZATION_SAMPLE_SIZE,
)

from ._weighted_Gaussian_prepost import (
    PrePostWeightedGaussianErrorModelConfigs,
    PrePostWeightedGaussianErrorModel,
)

from ._weighted_Gaussian_pointwise import (
    PointwiseWeightedGaussianErrorModelConfigs,
    PointwiseWeightedGaussianErrorModel,
)

from ._fenics_Gaussian import (
    DolfinGaussianErrorModelConfigs,
    DolfinGaussianErrorModel,
)
# ================================================================================ #


# ================================================================================ #
#                     Helper functions for Gaussian Error models                   #
# ================================================================================ #
def kl_divergence(
    N0,
    N1,
    method="exact",
    sample_size=_RANDOMIZATION_SAMPLE_SIZE,
    optimize_sample_size=False,
    min_sample_size=10,
    max_sample_size=100,
    distribution="Rademacher",
    random_seed=None,
    accuracy=1e-2,
):
    """
    Calculate the KL divergence between two Gaussian distributions as follows,

    .. math::
       D_{\\text{KL}}\\left({\\mathcal {N}}_{0}\\parallel {\\mathcal {N}}_{1}\\right)
       ={\\frac {1}{2}}\\left(\\operatorname {tr} \\left(\\Sigma_{1}^{-1}\\Sigma_{0}\\right)
       +\\left(\\mu_{1}-\\mu_{0}\\right)^{\\mathsf{T}}\\Sigma_{1}^{-1}\\left(\\mu_{1}-\\mu_{0}\\right)
       -k+\\ln \\left({\\frac {\\det \\Sigma_{1}}{\\det \\Sigma_{0}}}\\right)\\right)


    .. note:: See https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
    """
    if not isinstance(N0, GaussianErrorModel):
        print("N0 must be a GaussianErrorModel isntance; received {0}".format(type(N0)))
        raise TypeError
    if not isinstance(N1, GaussianErrorModel):
        print("N1 must be a GaussianErrorModel isntance; received {0}".format(type(N1)))
        raise TypeError
    assert N0.size == N1.size, "N0 and N1 must have the same dimension"

    D_KL = utility.matrix_trace(
        lambda x: N1.covariance_inv_matvec(N0.covariance_matvec(x)),
        size=N0.size,
        method=method,
        sample_size=sample_size,
        optimize_sample_size=optimize_sample_size,
        min_sample_size=min_sample_size,
        max_sample_size=max_sample_size,
        distribution=distribution,
        accuracy=accuracy,
        random_seed=random_seed,
        return_randomization_vectors=False,
    )  # TODO: this can be accelerated tr(np.dot(A,B)) = np.sum (A*B)

    D_KL += np.dot((N1.mean - N0.mean).T, N1.covariance_inv_matvec(N1.mean - N0.mean))
    D_KL += N1.covariance_logdet(
        method=method,
        sample_size=sample_size,
        optimize_sample_size=optimize_sample_size,
        min_sample_size=min_sample_size,
        max_sample_size=max_sample_size,
        distribution=distribution,
        random_seed=random_seed,
        accuracy=accuracy,
    )
    print(f"{D_KL=}")
    D_KL -= N0.covariance_logdet(
        method=method,
        sample_size=sample_size,
        optimize_sample_size=optimize_sample_size,
        min_sample_size=min_sample_size,
        max_sample_size=max_sample_size,
        distribution=distribution,
        random_seed=random_seed,
        accuracy=accuracy,
    )
    D_KL -= N0.size

    D_KL /= 2.0

    return D_KL

# ================================================================================ #


# ================================================================================ #
# Example Functions to help inistantiate instances of the models implemented here  #
# ================================================================================ #
def create_GaussianErrorModel(
    size,
    mean=0.0,
    variance=1.0,
    sparse=False,
    random_seed=None,
):
    return GaussianErrorModel(
        configs=dict(
            size=size,
            mean=mean,
            variance=variance,
            sparse=sparse,
            random_seed=random_seed,
        )
    )

def create_PrePostWeightedGaussianErrorModel(
    size,
    mean=0.0,
    variance=1.0,
    sparse=False,
    random_seed=None,
):
    return PrePostWeightedGaussianErrorModel(
        configs=dict(
            size=size,
            mean=mean,
            variance=variance,
            sparse=sparse,
            random_seed=random_seed,
        )
    )

def create_PointwiseWeightedGaussianErrorModel(
    size,
    mean=0.0,
    variance=1.0,
    sparse=False,
    random_seed=None,
):
    return PointwiseWeightedGaussianErrorModel(
        configs=dict(
            size=size,
            mean=mean,
            variance=variance,
            sparse=sparse,
            random_seed=random_seed,
        )
    )
# ================================================================================ #

