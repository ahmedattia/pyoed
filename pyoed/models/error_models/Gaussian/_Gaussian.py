# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


r"""
This module implements the most basic form of a Gaussian error model
with support for binary design.
The role of a binary design is to activate/deactivate entries (dimensions)
of the random variable (probability space) modeled by the Gaussian error
model implemented here.

The Gaussian error model here is defined as follows:

.. math::
    x \sim \mathcal{N}(\mu, \mathbf{D} \mathbf{\Sigma} \mathbf{D} ),

where :math:`x` is a normally distributed random vector with mean
:math:`\mu` and covariance matrix :math:`\mathbf{\Sigma}`.
The matrix :math:`\mathbf{D}` is a diagonal (square) matrix with a binary
design vector :math:`\mathbf{\zeta}` on its diagonal.
An entry in :math:`\mathbf{\zeta}` set to ``1`` corresponds to an active entry,
and an entry set to ``0`` corresponds to an inactive (degenerate) entry that
is eliminated from calculations.
"""

import sys
import inspect
import re
from dataclasses import dataclass, replace
from typing import Sequence

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg

from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    validate_key,
    set_configurations,
    PyOEDConfigsValidationError,
)
from pyoed.models.core import (
    ErrorModel,
    ErrorModelConfigs,
)

# Global (ish) variables
_LINSOLVE_TOL = 1e-12  # Base tolerance of linear system solves
_RANDOMIZATION_SAMPLE_SIZE = 50  # 250


@dataclass(kw_only=True, slots=True)
class GaussianErrorModelConfigs(ErrorModelConfigs):
    """
    Configuration settings for Gaussian error models.

    :param size: dimension of the error model space. Detected from mean if None.
    :param mean: mean of the error model
    :param variance: variance/covariance of the error model
    :param design: an experimental design to define active/inactive entries
        of the random variable (mean, variance/covariance matrix).

        - If the design is None, it is set to all ones; that is everything is observed
          (default)

        - If the design is a binary vector ( or int dtype attributes with 0/1
          entries) the mean, the covariance, and all random vectors are projected onto
          the space identified by the 1/True entries.

    :param sparse: convert covariance to `scipy.csc_matrix` used if ``size > 1``
    :param random_seed: random seed used when the Gaussian model is initiated. This is
        useful for reproductivity.
    """
    size: int | None = None
    mean: float | np.ndarray = 0.0
    variance: float | np.ndarray | sp.spmatrix = 1.0
    design: None | bool | Sequence[bool] | np.ndarray = True
    sparse: bool = True
    random_seed: int | None = None


@set_configurations(GaussianErrorModelConfigs)
class GaussianErrorModel(ErrorModel, RandomNumberGenerationMixin):
    r"""
    A simple Numpy-based (or scipy sparse) Gaussian error model.  In addition to
    standard functionality, here we provide a specific attribute 'design' which enables
    modifying the space of the error model by projection or reweighting.

    .. note::
        This version does not support non-binary designs. For non-bianry (relaxed)
        design, consider :py:class:`PointwiseWeightedGaussianErrorModel`  or
        :py:class:`PrePostWeightedGaussianErrorModel`.

    .. note::
        If mean, or variance is scalar, the size is inferred from `size` or from the
        argument with larger size, as long as there is no contradiction, otherwise an
        AssertionError is raised.

    .. note::
        This model creates lazy objects under the hood. These are lazy in the sense that
        they are constructed/calculated only when they are needed, and are kept in memory
        unless the design changes. These objects are accessible through the following
        properties:

        1. `stdev`: this refers to a lazy evaluation of the lower
           Cholesky factor of the covariance matrix.

        2. `stdev_inv`: this refers to a lazy evaluation of the inverse
           of the lower Cholesky factor of the covariance matrix.

        3. `stdev_logdet`: this refers to a lazy evaluation of the logarithm
           of the the determinant (logdet) of the lower Cholesky factor of the
           covariance matrix (used for normalizing the PDF valued/gradient when/if needed).

    .. note::
        We use the fact that the logdet of the covariance matrix is obtained from the the
        Cholesky factorization as follows

        .. math::
            logdet (C) = log (det(L L^T)) = log (det(L) det(L^T)) = logdet(L) + logdet (L^T) ,

        Thus, :math:`0.5 logdet(C) = logdet(L)`, a fact used in calculating the (normalized)
        PDF, etc.

    :param configs: an object containing configurations of the error model

    :raises PyOEDConfigsValidationError: if mean or variance types are not supported
        (scalars and iterables are supported)
    :raises PyOEDConfigsValidationError: if there is a size mismatch between size,
        mean, and variance.
    """

    def __init__(self, configs: GaussianErrorModelConfigs | dict | None = None):
        # Standardize the passed configurations then pass up for initialization
        super().__init__(
            self.standardize_configurations(
                self.configurations_class.data_to_dataclass(
                    configs
                ),
            ),
        )

        ## Additional Initialization
        # Lazy initialization of crucial matrices and constants:
        self._DESIGN = None  # this is the design used to calculate values below; need to be maintained...
        self._STDEV = None
        self._STDEV_INV = None
        self._STDEV_LOGDET = None

        # Check the need to update anything related to the design
        self.update_design(design=self.configurations.design)

        # Assure the random seed
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

    def standardize_configurations(
        self,
        configs: dict | GaussianErrorModelConfigs,
        mean_dtype:type = float,
        variance_dtype: type = float,
        design_dtype: type = bool,
    ) -> GaussianErrorModelConfigs:
        """
        Check the configurations of the error model (size, mean, design, and variance),
        and make sure they are consistent.

        .. note::
            Here are the rules:

            1. If the `size` is passed, the mean and the design must be one-dimensional
               arrays/vectors of this size (if either is scalar, they are casted to vectors),
               and the variance's size/shape is asserted accordingly.
            2. If the `size` is not passed, it is inferred from `mean` and/or `variance`

        :raises PyOEDConfigsValidationError: if the validation of the configurations fail
        """
        # First Aggregate and convert to the proper (expected) type
        configs = self.configurations_class.data_to_dataclass(configs)

        # Extract Distribution Elements from Configurations:
        size = configs.size
        mean = configs.mean
        variance = configs.variance
        design = configs.design
        sparse = configs.sparse

        # ====================================================================================
        ## TYPE CHECK:
        ##   Check mean, design and variance types and
        ##   convert to numpy/sparse arrays (based on the value of configs.sparse).
        # ====================================================================================
        # 1) Mean: make sure it is 1-d numpy array (of dtype complex)
        # -----------------------------------------------------------
        try:
            mean = utility.asarray(mean, dtype=mean_dtype, )
            if 1 in mean.shape: mean = mean.flatten()

        except (Exception) as err:
            raise PyOEDConfigsValidationError(
                f"`mean` is of unsupported type: {type(mean)}; "
                f"See further details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )

        if mean.ndim != 1:
            raise PyOEDConfigsValidationError(
                f"The mean is expected to be one dimensional array! "
                f"After validation -> {mean.shape=}"
            )

        # 2) Design: make sure it is 1-d numpy array
        # -----------------------------------------------------------
        if design is None: design = 1
        try:
            _design = utility.asarray(design, dtype=design_dtype)  # This is supposed to be int/bool!
            if 1 in _design.shape: _design = _design.flatten()
        except (Exception) as err:
            raise PyOEDConfigsValidationError(
                f"`design` is of unsupported type: {type(design)}; "
                f"See further details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
        else:
            design = _design

        if design.ndim != 1:
            raise PyOEDConfigsValidationError(
                f"The design is expected to be one dimensional array! "
                f"After validation -> {design.shape=}"
            )

        # 3) Variance/Covariance: make sure it is 1- or 2-d numpy array (or scipy sparse matrix)
        # -----------------------------------------------------------
        if utility.isnumber(variance):
            # A scalar
            variance = utility.asarray([variance])

        elif isinstance(variance, np.ndarray):
            # Numpy array
            variance = utility.asarray(variance, dtype=variance_dtype, )
            if 1 in variance.shape or variance.size == 1: variance = variance.flatten()

        elif sp.issparse(variance):
            # Numpy Scipy array/matrix
            # Check ndim; if 1 -> 1D numpy array (diagonal of covariances)
            if 1 in variance.shape  or variance.size == 1:
                variance = utility.asarray(variance, dtype=variance_dtype).flatten()

        else:
            # Something else (e.g., list, etc.)
            try:
                variance = utility.asarray(variance, dtype=variance_dtype, )
                if 1 in variance.shape: variance= variance.flatten()

            except (Exception) as err:
                raise PyOEDConfigsValidationError(
                    f"`variance` is of unsupported type: {type(variance)}; "
                    f"Tried casting `variance` to a numpy array but failed! "
                    f"See further details below: \n"
                    f"Unexpected {err=} of {type(err)=}"
                )
        # At this point, `variance` is either numpy or scipy array

        # ====================================================================================

        # Make sure mean, design, and variance are not empty
        if mean.size < 1 or np.prod(variance.shape) < 1 or design.size < 1:
            raise PyOEDConfigsValidationError(
                f"mean, variance, and design must be valid (non-empty)! \n"
                f"{mean=} with {mean.shape=}\n"
                f"{variance=} with {variance.shape=}\n"
                f"{design=} with {design.shape=}"
            )

        ## Extract/Infer size (if not explicitly passed), otherwise make sure parameters are conformable with it
        if size is None:
            # No size given, infer from mean, design, or variance in this order

            # Infer size from mean:
            # ---------------------
            if mean.size == design.size == variance.size == 1:
                # All are scalars
                size = 1

            elif mean.size > 1:
                # Set the size to the mean size
                size = mean.size

            elif design.size > 1:
                # Mean is scalar; Set the size to the design size
                size = design.size

            else:
                # Mean & design are both scalars; extract size from variance
                if variance.size == 1:
                    # Variance is 1d with one entry (scalars)
                    size = 1

                elif variance.ndim == 1 and variance.size > 1:
                    # variance is 1d (diagonal of covariances)
                    size = variance.size

                elif variance.ndim == 2 and variance.shape[0] == variance.shape[1]:
                    size = variance.shape[0]

                else:
                    raise PyOEDConfigsValidationError(
                        f"Failed to extract size from variance (covariance matrix); "
                        f"expected two dimensional array/matrix; found array(s) of shape:"
                        f"{variance.shape=}"
                    )

        # Make sure the given/inferred size is positive
        if not (utility.isnumber(size) and size==int(size) > 0):
            raise PyOEDConfigsValidationError(
                f"Invalid {size=} of {type(size)=}; expected positive integer"
            )

        ## Given the size passed/inferred; expand mean/design/variance properly
        ## =============================================================================
        # mean & design: 1d of size `size`
        if mean.size == 1: mean = mean.repeat(size)
        if design.size == 1: design = design.repeat(size)

        # if variance is scalar -> 1D array of size `size`
        if variance.size == 1: variance = variance.repeat(size)

        # variance: -> 2D array of shape (`size` x `size`)
        if variance.ndim == 1 and variance.size == size:
            # Variance is 1D vector (take as the diagonal of the covariance matrix)
            if sparse:
                variance = sp.csc_array(sp.diags(variance, offsets=0, format="csc", dtype=variance_dtype, ))
            else:
                variance = np.diag(variance, k=0, ).astype(variance_dtype)

        elif variance.shape == (size, size):
            if sparse and not sp.issparse(variance):
                variance = sp.csc_array(variance, dtype=variance_dtype, )
            elif not sparse and sp.issparse(variance):
                variance = variance.toarray().astype(variance_dtype)

        else:
            raise PyOEDConfigsValidationError(
                f"Invalid `variance` array/matrix. \n"
                f"Expected 2d square array of shape ({size}, {size})\n"
                f"Received (or casted to) `{variance=}; {variance.ndim=}; of {variance.shape=};"
            )

        # Final sanity check in case anything is missing!
        if (
            mean.size != size or
            design.size != size or
            variance.shape != (size, size)
        ):
            raise PyOEDConfigsValidationError(
                f"Inconsistency in the dimensionality of some (or all) of the "
                f"distribution components: .\n"
                f" {size=}\n "
                f" {mean.shape=}\n "
                f" {design.shape=}\n "
                f" {variance.shape=}\n This SHOULD NEVER HAPPEN! PLEASE REPORT THIS BUG!"
            )

        # Replace and return the configurations (in place with validated versions)
        return replace(
            configs,
            size=size,
            mean=mean,
            design=design,
            variance=variance,
            sparse=sparse,
        )

    def _reset_covariance_attributes(self, ):
        """
        Reset the entities (lazy objects) related to the covariance matrix
        .. warning::
            This function should not be called by the user!
        """
        if self.verbose:
            print(
                "Resetting (setting to None) the following lazy objects: \n"
                "self._DESIGN \n"
                "self._STDEV \n"
                "self._STDEV_INV \n"
                "self._STDEV_LOGDET"
            )
        self._DESIGN = None
        self._STDEV = None
        self._STDEV_INV = None
        self._STDEV_LOGDET = None

    def _validate_vector(
        self,
        x,
        in_place=False,
    ):
        """
        Check the validity of the passed vector, and return a numpy array
        representation of the vector with `float` data type.

        :raises TypeError: if `x` is of invalid type/shape/size
        """
        _x = utility.asarray(
            x,
            dtype=float,
        ).flatten()

        if _x.size != self.active_size:
            raise TypeError(
                f"Size mismatch; expected vector x of size {self.active_size}; "
                f"received vector of size {_x.size=}"
            )

        if not isinstance(_x, np.ndarray):
            warnings.warn(
                f"Cannot write in-place since {typ(_x)=}; Expected numpy array!"
                f"Returning a new vector (numpy array), that is in_place is ignored"
            )
            in_place = False
        if in_place and not np.issubdtype(_x.dtype, float):
            warnings.warn(
                f"Cannot write in-place since {_x.dtype=}; Expected dtype to be complex\n"
                f"Returning a new vector, that is in_place is ignored"
            )
            in_place = False

        if not in_place:
            x = _x

        return x, in_place

    def validate_configurations(
        self,
        configs: dict | GaussianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the Gaussian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises PyOEDConfigsValidationError: if any (or a group) of the configurations
            does not exist in the model configurations :py:class:`GaussianErrorModelConfigs`.
        """
        aggregated_configs = self.aggregate_configurations(configs)

        try:  # Half-baked validation; need to move validation to this function.
            aggregated_configs = self.standardize_configurations(aggregated_configs)
        except (TypeError, ValueError) as e:
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    "Failed type/shape validation!"
                ) from e
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # mean
        if "mean" in kwargs:
            self.update_mean(kwargs["mean"])

        # design
        if "design" in kwargs:
            self.update_design(kwargs["design"])

        # size
        if "size" in kwargs:
            raise TypeError(
                f"CANNOT MODIFY DIMENSIONALITY. You must create new error model"
            )

        # "variance"
        if "variance" in kwargs:
            self.update_covariance_matrix(kwargs["variance"])

        if "random_seed" in kwargs:
            self.update_random_number_generator(random_seed=kwargs["random_seed"])

    def generate_white_noise(
        self,
        ignore_design=False,
        truncate=False,
    ):
        """
        Generate a standard normal random vector of size `size` with values truncated
            at -/+3 if `truncate` is set to `True`

        :param bool ignore_design: the size of the white noise vector is in the full (ignore the
            design) if `True`, otherwise the size matches the dimension of the active subspace.
        :param bool truncate: if `True`, truncate the samples at -/+3, that is any sample
            point/entry above 3 is set to 3, and any value below -3 is set to -3.

        :returns: numpy array (even if the dimension is 1) containing white noise
        """
        size = self.size if ignore_design else self.active_size

        # Sample
        white_noise = self.random_number_generator.standard_normal(size)

        # Truncate if requested
        if truncate:
            white_noise[white_noise > 3] = 3
            white_noise[white_noise < -3] = -3
        return white_noise

    def generate_noise(self):
        """
        Generate a random noise vector sampled from the underlying Gaussian distribution
        """
        # Generate white noise: normal random noise with zero mean and variance 1.
        noise = self.generate_white_noise(ignore_design=False)
        noise = self.covariance_sqrt_matvec(noise, lower=True, in_place=True)

        return noise

    def sample(self):
        """
        Sample a random vector from the underlying Gaussian distribution
        """
        # Add a scaled random noise (with the underlying covariance matrix) to the underlying mean
        out_x = self.mean + self.generate_noise()

        return out_x

    def pdf(
        self,
        x,
        normalize=False,
        log=False,
    ):
        """
        Evaluate the value of the density function (normalized or upto
        a *fixed* scaling constant) at the passed state/vector.

        :param x: realization of the random variable
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state/vector.
        """
        pdf = self.log_density(
            x=x,
            normalize=normalize,
        )
        # scale (if needed) and return
        if not log:
            pdf = np.exp(pdf)
        if abs(pdf) == 0:
            pdf = 0  # Avoid retrning -0.0
        return pdf

    def log_density(
        self,
        x,
        normalize=False,
    ):
        """
        Evaluate the logarithm of the density function at the passed state `x`.

        :param x: realization of the random variable
        :param bool normalize: scale the PDF by the normalization factor

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        # Check passed state vs. the current distribution configurations
        x, _ = self._validate_vector(x)

        # Evaluate the exponent
        innov = x - self.mean
        scaled_innov = self.covariance_inv_matvec(innov)
        pdf = -0.5 * (innov @ scaled_innov)

        # Normalization constant (log-scale)
        if normalize:
            scl = - 0.5 * self.active_size * np.log(2 * np.pi)
            scl -= self.stdev_logdet
            pdf += scl

        return pdf

    def pdf_gradient(
        self,
        x,
        normalize=False,
        log=False,
        in_place=False,
    ):
        """
        Evaluate the gradient of the density function at the passed state/vector.

        :param x: realization of the random variable
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state/vector.
        """
        grad = self.log_density_gradient(
            x=x,
            in_place=in_place,
            normalize=False,
        )
        # Full PDF gradient
        if not log:
            # multiply by log-pdf
            grad *= self.pdf(
                x=x,
                log=False,
                normalize=False,
            )

            if normalize:
                scl = - 0.5 * self.active_size * np.log(2 * np.pi)
                scl -= self.stdev_logdet
                grad *= scl

        return grad

    def log_density_gradient(
        self,
        x,
        normalize=False,
        in_place=False,
    ):
        """
        Evaluate the gradient of the logarithm of the density function at the passed state `x`.

        :param x: realization of the random variable
        :param bool normalize: scale the PDF by the normalization factor

        .. note::
            The derivative of the log of the normalization factor vanishes; thus, `normalize`
            takes no effect here. Just added for unification

        :returns: the gradient of the logarithm of the density function evaluated at the passed state.
        """
        # Check passed state vs. the current distribution configurations
        _x, _ = self._validate_vector(x)
        grad = x if in_place else _x

        # Evaluate the exponent
        innov = x - self.mean
        grad[:] = - self.covariance_inv_matvec(innov)

        return grad

    def add_noise(self, x, in_place=False):
        """
        Add random noise to the passed vector `x`

        :param x: vector in a space of dimension equal to size of the underlying error model
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size) is created
            and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :raises TypeError: if x is of the wrong shape or type
        """
        # Overwire x or use a copy
        _x, in_place = self._validate_vector(x, in_place=in_place)
        out = x if in_place else _x

        # Scale and shift the white noise
        out += self.generate_noise()

        return out

    def covariance_matvec(self, x, in_place=False):
        """
        Evaluate and return the product of covariance matrix by a vector `x`.
        The product is determined by applying the cholesky decomposition
        of the covariance matrix twice (dot product) to the passed state `x`

        :param x: vector in a space of dimension equal to the size of the
            underlying error model.
            `x` could be scalar or number array, however, it must match
            the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the covariance matrix by `x`
        """
        # Overwire x or use a copy
        _x, in_place = self._validate_vector(x, in_place=in_place)
        out = x if in_place else _x

        if out.size > 0:
            # Multiply the covariance matrix (using its Cholesky factor) by the vector
            out[:] = self.stdev @ (self.stdev.T @ out)

        return out

    def unweighted_covariance_matrix(self):
        """
        Construct a numpy array representation of the covariance matrix discarding
        any design effect
        """
        return utility.asarray(self.configurations.variance)

    def unweighted_precision_matrix(self):
        """
        Construct a numpy array representation of the precision matrix
        discarding any design effect
        """
        return np.linalg.inv(self.unweighted_covariance_matrix())

    def covariance_inv_matvec(self, x, in_place=False, ):
        """
        Evaluate and return the product of inverse of the error covariance
        matrix by a vector `x`. The inverse of the lower Cholesky factor
        is used

        :param x: vector in a space of dimension equal to the size of the
            underlying error model. `x` could be scalar or number array,
            however, it must match the distribution size.
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of inverse of the error covariance matrix by `x`
        """
        # Overwire x or use a copy
        _x, in_place = self._validate_vector(x, in_place=in_place)
        out = x if in_place else _x

        if out.size > 0:
            # Multiply the covariance matrix (using its Cholesky factor) by the vector
            out[:] = self.stdev_inv.T @ (self.stdev_inv @ out)

        return out

    def covariance_sqrt_matvec(self, x, lower=True, in_place=False):
        """
        Evaluate and return the product of Square root (Lower Cholesky) covariance matrix
            by a vector `x`. The product is determined by applying the cholesky decomposition
            of the covariance matrix once (dot product) to the passed state `x`
            This metho is tricky. See remarks below to understand how it works.

        .. note::
            This function multiples the Cholesky factor (lower or
            upper/lower-transpose) by a vector.  The covariance matrix is weighted based
            on the underlying design. When the design has some zero entries
            (rows/columns of the covariance are eliminated), the dimension of the model
            reduces.  Let us assume the full space has size :math:`n` and the design is
            all ones, then the covariance matrix is :math:`C:=LL^T` where :math:`L` is
            the lower Cholesky factor of size :math:`n\\times n`.  Thus, applying the
            lower Cholesky factor (matvec-product) takes a vector in the full space
            (size :math:`n`) and returns a vector of the same size. However, when the
            design has zeros the output of the lower Cholesky matvec product is a vector
            of size equal to the number of active entries.  Similar analogy is followed
            for multiplying the Upper Cholesky factor :math:`L^T`.  In all cases, when
            `lower` is `True`, the input must reside in the full observation space,
            otherwise, the Cholesky factor is no longer valid and must be recalculated
            in the reduced space for the projected covariance.

        :param x: vector in a space of dimension equal to the size of the
            underlying error model. x` could be scalar or number array,
            however, it must match the distribution size.
        :param bool lower: use the lower Cholesky as the square root
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of the sqrt-covariance matrix by `x`:

            - when `lower==True`, the output is in the space of active sensors.
              The input can be either in the full space or the space of active
              sensors if `in_place==False`, however it must be in the reduced
              space if `in_place==True` otherwise a `ValueError` is raised.

            - when `lower==False`, the output is in the full space.
              The input must be in the space of active sensors and `in_place`
              must be set to `False` otherwise a `ValueError` is raised.
        """
        # Overwire x or use a copy
        _x, in_place = self._validate_vector(x, in_place=in_place)
        out = x if in_place else _x

        if out.size > 0:
            mat = self.stdev if lower else self.stdev.T
            out[:] = mat @ out

        return out

    def covariance_sqrt_inv_matvec(
        self, x, lower=True, in_place=False,
    ):
        """
        Evaluate and return the product of inverse of the square root of the
        error covariance matrix by a vector `x`.
        A linear system is solved using the cholesky decomposition of the covariance matrix

        :param x: vector in a space of dimension equal to the size of the
            underlying error model. `x` could be scalar or number array,
            however, it must match the distribution size.
        :param bool lower: use the lower Cholesky as the square root
        :param bool in_place: overwrite the passed vector `x` (if `True`).
            If set to `False`, a new instance (based on the distribution size)
            is created and returned.
            This is ignored of course if `x` is a non-iterable, i.e., a scalar.

        :returns: the product of inverse of the square root of the error
            covariance matrix by `x`
        """
        # Overwire x or use a copy
        _x, in_place = self._validate_vector(x, in_place=in_place)
        out = x if in_place else _x

        if out.size > 0:
            mat = self.stdev_inv if lower else self.stdev_inv.T
            out[:] = mat @ out

        return out

    def covariance_matrix(self):
        """
        Construct a numpy array representation (if the dimension is more than 1)
        of the underlying covariance matrix. (projected onto the observation space)
        """
        if self.active_size == 0:
            return sp.csc_array(np.array([[]])).reshape(0, 0) if self.sparse else np.array([[]]).reshape(0, 0)
        else:
            return utility.asarray(self.stdev @ self.stdev.T)

    def precision_matrix(self):
        """
        Construct a numpy array representation (if the dimension is more than 1)
        of the precision matrix (inverse of the underlying covariance matrix).
        """
        if self.active_size == 0:
            return sp.csc_array(np.array([[]])).reshape(0, 0) if self.sparse else np.array([[]]).reshape(0, 0)
        else:
            return utility.asarray(self.stdev_inv.T @ self.stdev_inv)

    def covariance_diagonal(self):
        """
        Return a copy the diagonal of the covariance matrix, i.e., the variances

        :returns: 1D numpy array holding the diagonal of the underlying covariance matrix.
            If the size of the underlying distribution is 1, this returns a scalar (variance)
        """
        diag = np.empty(self.active_size)
        for i in range(self.active_size):
            if self.sparse:
                stdev_row_i = self.stdev[[i], :].toarray().flatten()
            else:
                stdev_row_i = self.stdev[i, :]
            diag[i] = np.power(stdev_row_i, 2).sum()

        return diag

    def variance(self):
        """Same as :py:meth:`covariance_diagonal`"""
        return self.covariance_diagonal()

    def covariance_trace(
        self,
        method="exact",
        sample_size: int = _RANDOMIZATION_SAMPLE_SIZE,
        optimize_sample_size: bool = False,
        min_sample_size: int = 10,
        max_sample_size: int = 100,
        distribution: str = "Rademacher",
        random_seed: None | int = None,
        accuracy: float = 1e-2,
    ) -> float:
        """
        Evaluate/Approximate the trace of the covariance matrix

        .. note::
            This is a wrapper around the utility function `pyoed.utility.math.matrix_trace`
            that is called with the method :py:meth:`covariance_matvec` as the operator
            to compute the trace of.

        :param method: Method of evaluation. If "exact", we simply sum the diagonal of A. If
            "randomized", a Hutchinson style trace estimator, as described in
            :py:meth:`pyoed.utility.trace_estimator`, is employed. All of the futher keyword
            arguments are in the case of method="randomized" and are otherwise ignored if
            method="exact".
        :param randomization_vectors: The randomization vectors drawn to perform the
            computation. If not passed, they will be sampled.
        :param sample_size: The number of samples to use. :param optimize_sample_size: if
            `True`, `sample_size` is replaced with an optimized version, where the optimal
            sample size minimizes the variance of the estimator and is between
            `min_sample_size`, and `max_sample_size`.
        :param min_sample_size: Minimum number of random samples.
        :param max_sample_size: Maximum number of random samples.
        :param distribution: The probability distribution used for random sampling. Both
            'Rademacher' and 'Gaussian' are supported.
        :param random_seed: The random seed to use for the distribution samples.
        :param accuracy: convergence criterion.

        :returns: exact/approximate value of the trace of the covariance matrix.
            of the underlying model.
        """
        if re.match(r"\Aexact\Z", method, re.IGNORECASE):
            variance = self.covariance_diagonal()
            tr = np.sum(variance)

        elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
            tr = utility.trace_estimator(
                matvec_fun=self.covariance_matvec,
                vector_size=self.active_size,
                sample_size=sample_size,
                optimize_sample_size=optimize_sample_size,
                min_sample_size=min_sample_size,
                max_sample_size=max_sample_size,
                distribution=distribution,
                random_seed=random_seed,
                accuracy=accuracy,
            )
            if not tr[0]:
                print("Estimator didn't converge; results are unreliable...")
            tr = tr[1]

        else:
            raise ValueError(
                f"Unsupported evaluation '{method=}'"
            )

        return tr

    def covariance_logdet(
        self,
        method="exact",
        sample_size: int = _RANDOMIZATION_SAMPLE_SIZE,
        optimize_sample_size: bool = False,
        min_sample_size: int = 10,
        max_sample_size: int = 100,
        distribution: str = "Rademacher",
        random_seed: None | int = None,
        accuracy: float = 1e-2,
    ) -> float:
        """
        Evaluate/approximate the logarithm of the determinant of the covariance
        matrix of the passed error model

        .. note::
            This is a wrapper around the utility function `pyoed.utility.math.matrix_logdet`
            that is called with the method :py:meth:`covariance_matvec` as the operator
            to compute the trace of.

        :param method: Method of evaluation. If "exact", we simply sum the diagonal of A.
            If "randomized", a chebyshev-approximation type-method is employed from
            :py:meth:`pyoed.utility.logdet_estimator`. All of the futher keyword arguments
            are in the case of method="randomized" and are otherwise ignored if
            method="exact".
        :param randomization_vectors: The randomization vectors drawn to perform the
            computation. If not passed, they will be sampled.
        :param sample_size: The number of samples to use. :param optimize_sample_size: if
            `True`, `sample_size` is replaced with an optimized version, where the optimal
            sample size minimizes the variance of the estimator and is between
            `min_sample_size`, and `max_sample_size`.
        :param min_sample_size: Minimum number of random samples.
        :param max_sample_size: Maximum number of random samples.
        :param distribution: The probability distribution used for random sampling. Both
            'Rademacher' and 'Gaussian' are supported.
        :param random_seed: The random seed to use for the distribution samples.
        :param accuracy: convergence criterion.

        :returns: exact/approximate value of the logdet of the covariance matrix
            of the underlying model.
        """
        if re.match(r"\Aexact\Z", method, re.IGNORECASE):
            # Determinant of a triangular matrix L is the product of it's diagonal
            # The idea below
            # det (L L^T) = det(L) det (L^T) --> log(det(L L^T)) = 2 sum log(diagonal(L))
            logdet = 2 * self.stdev_logdet

        elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
            logdet = utility.logdet_estimator(
                matvec_fun=self.covariance_matvec,
                vector_size=self.active_size,
                sample_size=sample_size,
                optimize_sample_size=optimize_sample_size,
                min_sample_size=min_sample_size,
                max_sample_size=max_sample_size,
                distribution=distribution,
                random_seed=random_seed,
                accuracy=accuracy,
            )
            if not logdet[0]:
                print("Estimator didn't converge; results are unreliable...")
            logdet = logdet[1]

        else:
            raise ValueError(
                f"Unsupported evaluation '{method=}'"
            )
        return logdet

    def update_design(self, design):
        """
        If the design matches existing one, don't do anything, otherwise, update it

        :param design: the design to be used update model's configurations

        :raises PyOEDConfigsValidationError: if the passed design is of
            invalid type/shape/size
        """
        if design is None:
            _design = design = np.ones(self.size, dtype=bool)
        else:
            _design = utility.asarray(design, dtype=bool)

        if any(_design != design):
            raise PyOEDConfigsValidationError(
                f"The design is only allowed to be boolean"
            )

        if _design.size != self.size:
            raise PyOEDConfigsValidationError(
                f"The design must be 1d array of size {self.size}\n"
                f"Received {_design=} with {_design.shape=}"
            )

        # Use the design array
        design = _design

        # Compare the design against the one used for calculating _STDEV
        if self._STDEV is not None and not np.allclose(design, self._DESIGN):
            # Existing design differs from the new one

            # Destroy the lower Cholesky factor (& inverse, & logdet) of projected covariance
            # matrix and rebuild when needed
            self._reset_covariance_attributes()

        elif self._STDEV is not None:
            if self.verbose:
                print(
                    f"The passed design is not different from current one. "
                    f"Skipping design update..."
                )
        else:
            pass

        # Update configurations to make sure an array is set in the configurations
        self.configurations.design = design

    def update_mean(self, mean):
        """
        Update the mean of the error model (in the full space) regardless the design
        dimensionalty

        :param mean: the mean to be used update model's configurations

        :raises PyOEDConfigsValidationError: if the passed mean is of
            invalid type/shape/size
        """
        new_mean = utility.asarray(mean)
        try:
            np.copyto(self.configurations.mean, new_mean)
        except ValueError as err:
            raise PyOEDConfigsValidationError(
                f"Failed to update the distribution mean. "
                f"Check the error details below:\n"
                f"Unexpected {err=} of {type(err)=}"
            )

    def update_covariance_matrix(self, variance, ):
        """
        Update the covariance matrix (and the square root) in the full space,
        regardless the design

        :param variance: covariance matrix (or its square root) to be used in the error model
        """
        # Validate passed variance along with existing configurations
        configs = self.standardize_configurations(
            self.aggregate_configurations(
                {
                    'variance': variance,
                }
            )

        )
        self.configurations.variance = configs.variance

        ## Reset (lazy evaluation of) STDEV and related entities
        # Destroy the lower Cholesky factor (& inverse, & logdet) of projected covariance
        # matrix and rebuild when needed
        self._reset_covariance_attributes()

    def apply_weighting_matrix(self, variance, design=None, ):
        """
        Apply the weighting matrix (binary activation/deactivation) of the elements
        in `variance`. Is expected to be of dimension `(design.size, design.size )`.
        Rows/columns in `variance` corresponding to `0` in `design` are zero'd out.
        If `variance` has dimension equal to active entries only in `design`, nothing
        is done, and `variance` is returned as is.

        :param variance: the variance covariance matrix
        :param design: an experimental design used to calculate the weights.
            This is only added for flexibility. If `None`, the internal/current
            deesign is used.
        :returns: `variance` with inactive entries in `design` or registered design
            set to '0'
        """
        if design is None:
            design = self.design
            active = self.active
        else:
            design = np.asarray(design).flatten()
            active = np.where(design)[0]

        if not (isinstance(variance, np.ndarray) or sp.issparse(variance)):
            try:
                variance = utility.asarray(variance)
            except Exception as err:
                raise TypeError(
                    f"Invalid {type(variance)=}. "
                    f"Expected an array or callable castable to an array!"
                    f"See the error below for details:\n"
                    f"Unexpected {err=} of {type(err)=}"
                )

        # Check space/size and build weighting matrix
        if variance.shape == (design.size, design.size):
            variance[active, :] = 0
            variance[:, active] = 0

        elif variance.shape == (active.size, active.size):
            pass

        else:
            raise TypeError(
                f"The {variance.shape=} is not acceptable!\n"
                f"{design.size=}; {active.size=}"
            )

        return variance

    @property
    def sparse(self):
        """True if the covariance data structure is constructed using scipy.spmatrix)"""
        return self.configurations.sparse

    @property
    def size(self):
        """Dimension of the underlying probability distribution"""
        return self.configurations.size

    @property
    def design(self):
        """Get a copy of the design vector"""
        return self.configurations.design
    @design.setter
    def design(self, val):
        self.update_configurations(design=val)

    @property
    def extended_design(self):
        """
        Return a vector indicating active/inactive entries (with proper replication for
        multiple prognostic variables) of the observation vector/range Right now, the
        model is agnostic to any prognostic (physics) variables and is left to other
        modules to handle such periodicity if exists.  Thus, this function is identical
        to the :py:meth:`design`
        """
        return self.design

    @property
    def active(self):
        """entries (indexes) corresponding to active/inactive dimensions"""
        return np.where(self.design)[0]

    @property
    def active_size(self):
        """
        Dimension of the probability distribution projected onto the design
        space (only active/nonzero entries of the design)
        """
        return self.active.size

    @property
    def mean(self):
        """Get a copy of the distribution mean"""
        return self.configurations.mean[self.active]
    @mean.setter
    def mean(self, val):
        self.update_configurations(mean=val)

    @property
    def stdev(self, ):
        """
        Construct and return the lower Cholesky factor (if not available) in the
        projected space (after applying the design) of the covariance matrix
        """
        if self._STDEV is None:
            if self.verbose:
                print(
                    f"Generating lower Cholesky factor of the covariance matrix"
                )

            # The design used to create _STDEV
            self._DESIGN = utility.asarray(self.design)

            # Get active entries, and create lower Cholesky factor for those
            self._STDEV = utility.factorize_spsd_matrix(
                self.configurations.variance[self.active, :][:, self.active],
                shape=(self.active_size, self.active_size),
            )

        return self._STDEV

    @property
    def stdev_inv(self, ):
        """
        Construct and return the inverse of the lower Cholesky factor (if not
        available) of the covariance matrix in the projected space
        (after applying the design)
        """
        if self._STDEV_INV is None:
            if self.verbose:
                print(
                    f"Generating the inverse of the lower Cholesky factor of "
                    f"the covariance matrix"
                )

            if self.active_size >= 1:
                # Function to calculate the inverse
                inverter = sp.linalg.inv if self.sparse else np.linalg.inv
                self._STDEV_INV = inverter(self.stdev)

                ## NOTE: The following line is a workaround a bug in scipy linalg.inv
                # sp.linalg.inv returns 1d if input shape is 1x1 (they have another bug with @)
                # sp.csc_array does support construction from 1d
                if self.active_size == 1 and self.sparse:
                    self._STDEV_INV = sp.csc_array(
                        [[ self._STDEV_INV[0] ]],
                        shape=(1, 1),
                    )

            else:
                self._STDEV_INV = sp.csc_array([[]]) if self.sparse else np.array([[]])

        # Return it
        return self._STDEV_INV

    @property
    def stdev_logdet(self, ):
        """
        Evaluate the log-det (logarithm of the determinant) of the lower Cholesky
        factor (if not available) in the projected space (after applying the design)
        of the covariance matrix

        .. note::
            We use the fact that the determinant is the product of the diagonal
            elements of the lower Cholesky factor. Thus, the log-det is the sum
            of log of diagonal elements.
        """
        if self._STDEV_LOGDET is None:
            if self.verbose:
                print(
                    f"Evaluating the logdet of the lower Cholesky factor of "
                    f"the covariance matrix"
                )

            # Get the diagonal of the lower Cholesky factor and multiply them
            self._STDEV_LOGDET = sum(
                [
                    np.log(
                        self.stdev[i, i]
                    ) for i in range(self.active_size)
                ]
            )

        # Return it
        return self._STDEV_LOGDET

