# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements weighted versions of Gaussian error model
with support for binary as well as relaxed (non-binary) design.
The role of a design is to activate/deactivate entries (dimensions) if it is binary,
however, if it is relaxed, the design applies some weights to the entries of the
covariance (and the precision) matrix based on the formulation approach taken.

The approach taken here is **pre-post** multiplication.
This means either the covariance matrix (or its inverse) is
multiplied (pre -> from left, and post -> from right) by a diagonal matrix
with the design vector (relaxed) on its main diagonal.

.. note::
    This is the right approach to applying relaxed design for sensor placement
    in inverse problems. For details of the approach, see:

    1. Ahmed Attia, and Emil Constantinescu.
       "Optimal experimental design for inverse problems in the presence of
       observation correlations."
       SIAM Journal on Scientific Computing 44, no. 4 (2022): A2808-A2842.
"""

import sys
import inspect
import re
from dataclasses import dataclass, replace
from typing import (
    Callable,
    Any,
)

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg

from pyoed.models.core import (
    ErrorModel,
    ErrorModelConfigs,
)
from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    validate_key,
    set_configurations,
    PyOEDConfigsValidationError,
)
from ._weighted_Gaussian_prepost import (
    PrePostWeightedGaussianErrorModelConfigs,
    PrePostWeightedGaussianErrorModel,
    _LINSOLVE_TOL,
    _RANDOMIZATION_SAMPLE_SIZE,
)


@dataclass(kw_only=True, slots=True)
class PointwiseWeightedGaussianErrorModelConfigs(PrePostWeightedGaussianErrorModelConfigs):
    """
    Configuration settings for :py:class:`PointwiseWeightedGaussianErrorModel`.
    These are exactly the same as the configurations of the parent class
    :py:class:`GaussianErrorModelConfigs` with additional parameters as follows:

    :param design_weighting_scheme: a string describing the weighting scheme;
        you can choose from the following options:

        - 'covariance': the covariance matrix is pointwise-multiplied by the
          weighting matrix constructed (elementwise) by useing the passed
          `pointwise_weighting_kernel`.
        - 'precision': the precision (inverse of the covariance) matrix is
           pointwise-multiplied by the weighting matrix constructed
           (elementwise) by useing the passed `pointwise_weighting_kernel`.

    :param pointwise_weighting_kernel: a callable (function) that accpts three arguments
        (design, i, j) and evaluates a weighting value; used only when
        `design_weighting_scheme` is 'pointwise-covariance' or 'pointwise-precision'
    """
    pointwise_weighting_kernel: Callable[[np.ndarray, int, int], float] = (
        lambda design, i, j: (
            design[i] * design[j]
            if i != j
            else (
                0.0 if design[i] * design[j] == 0 else 1.0 / (design[i] * design[j])
            )
        )
    )


@set_configurations(PointwiseWeightedGaussianErrorModelConfigs)
class PointwiseWeightedGaussianErrorModel(
    PrePostWeightedGaussianErrorModel,
):
    r"""
    A simple Numpy-based (or scipy sparse) Gaussian error model with pre- and
    post-multiplicative design weighting. Specifically, the design (binary or relaxed)
    takes place by pre- and post-multiplication of the covariance or the precision
    matrix (based on the `design_weighting_scheme` in the configurations) by a diagonal
    matrix with the design set to its diagonal (the design matrix here).

    As a clarifying example, consider the A-optimal design for linear inverse problems.
    In this case, the posterior covariance :math:`\Gamma_{\rm post}` takes on the following
    forms:

      1. `design_weighting_scheme=='covariance'`: pre-post multiplication of the
         covariance matrix). The covariance of the Gaussian posterior is:

         .. math::
           \Gamma_{\rm post}
             = \left(
               \mathbf{F}^{T} \left(
                   \Gamma_{\rm noise} \cdot \mathbf{W}
               \right)^{\dager} \mathbf{F}
               + \Gamma_{\rm prior}^{-1}
             \right)^{-1} \,,

      2. `design_weighting_scheme=='precision'`: pre-post imultiplication of the
         precision matrix). The covariance of the Gaussian posterior is:

         .. math::
           \Gamma_{\rm post}
             = \left(
               \mathbf{F}^{T}
               \left( \mathbf{W} \cdot \Gamma_{\rm noise}^{-1} \right)
               \mathbf{F}
               + \Gamma_{\rm prior}^{-1}
              \right)^{-1} \,,

    where :math:`\mathbf{F}` is the forward model (e.g., parameter-to-observable map),
    :math:`\mathbf(W)` is the weighting matrix constructed elementwise by using the
    `pointwise_weighting_kernel` in the configurations.
    :math:`\Gamma_{\rm prior}` is the covariance matrix of the Gaussian prior, and
    :math:`\Gamma_{\rm noise}` is the covariance matrix of the Gaussian observation
    noise/error model.

    Thus, in this formulation/implementation, the effect of the design takes place on
    the covariance/precision matrix.

    .. warning::
        The value "precision" of the design weighting scheme here is not mathematically
        correct, and is only added for testing and validation.

    :param configs: an object containing configurations of the error model

    :raises PyOEDConfigsValidationError: if mean or variance types are not supported
        (scalars and iterables are supported)
    :raises PyOEDConfigsValidationError: if there is a size mismatch between size,
        mean, and variance.
    """

    def __init__(
        self, configs: PointwiseWeightedGaussianErrorModelConfigs | dict | None = None
    ):
        super().__init__(self.configurations_class.data_to_dataclass(configs))


    def validate_configurations(
        self,
        configs: dict | PointwiseWeightedGaussianErrorModelConfigs,
        raise_for_invalid: bool = False,
    ):
        """
        Validate the passed configurations for the Gaussian error model

        :param configs: configurations to validate. If a configs dataclass
            is passed validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        :param raise_for_invalid: raise an exception if the configurations are invalid

        :returns: `True` if the configurations are valid, otherwise `False`
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises PyOEDConfigsValidationError: if any (or a group) of the configurations
            does not exist in the model configurations
            :py:class:`PointwiseWeightedGaussianErrorModelConfigs`.
        """
        aggregated_configs = self.aggregate_configurations(configs)

        ## Local validation tests:
        def test_pointwise_weighting_kernel(kernel):
            if not callable(kernel):
                return False
            try:
                design = np.ones(aggregated_configs.size)
                design[-1] = 0.5
                vals = (
                    kernel(design, 0, 0),
                    kernel(design, 0, design.size - 1),
                    kernel(design, design.size - 1, 0),
                )
                return all(utility.isnumber(v) and v >= 0 for v in vals)
            except (TypeError, ValueError):
                return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="pointwise_weighting_kernel",
            test=test_pointwise_weighting_kernel,
            message=f"Invalid pointwise weighting kernel.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def build_weighting_matrix(self, design=None, full_space=False, ):
        """
        Use the registered weighting kernel to build the weighting matrix.
        If no `design` is passed, the current design is used.

        :param design: an experimental design used to calculate the weights.
            This is only added for flexibility. If `None`, the internal/current
            deesign is used.
        :param full_space: if `True`, the weighting matrix will include
            weights corresponding to non-zero entries only. If `False`,
            the weights are calculated for active entries only.
        """
        if design is None:
            design = self.design
            active = self.active
        else:
            design = np.asarray(design).flatten()
            active = np.where(design)[0]

        if full_space:
            # Build the weighting matrix in the full space (active & inactive subspaces)
            dimension = design.size
            weights = design

        else:
            # Build the weighting matrix in the reduced space (only active subspace)
            dimension = active.size
            weights = design[active]


        # Build the weighting matrix
        # Build the weighting matrix in the full space (active & inactive subspaces)
        weighting_mat = np.empty((dimension, dimension))
        for i in range(dimension):
            row_i = np.array(
                [
                    self.pointwise_weighting_kernel(weights, i, j)
                    for j in range(i, dimension)
                ]
            )
            weighting_mat[i, i:] = row_i
        for j in range(0, dimension - 1):
            weighting_mat[j + 1 :, j] = weighting_mat[j, j + 1 :]

        if self.sparse:
            weighting_mat = sp.csc_array(weighting_mat, shape=weighting_mat.shape)
        return weighting_mat


    def apply_weighting_matrix(self, variance, design=None, ):
        """
        Apply (poinwise) the weighting matrix W created by
        calling :py:meth:`build_weighting_matrix` to the passed variance/covariance
        matrix `variance`.
        The weighting is carried out in the full or projected (based on design/active)
        space based on the dimension of `variance`.

        :param variance: the variance covariance matrix
        :param design: an experimental design used to calculate the weights.
            This is only added for flexibility. If `None`, the internal/current
            deesign is used.
        :returns: `variance` pointwise multiplied by the underlying weighting matix
        """
        if design is None:
            design = self.design
            active = self.active
        else:
            design = np.asarray(design).flatten()
            active = np.where(design)[0]

        if not (isinstance(variance, np.ndarray) or sp.issparse(variance)):
            try:
                variance = utility.asarray(variance)
            except Exception as err:
                raise TypeError(
                    f"Invalid {type(variance)=}. "
                    f"Expected an array or callable castable to an array!"
                    f"See the error below for details:\n"
                    f"Unexpected {err=} of {type(err)=}"
                )

        # Check space/size and build weighting matrix
        if variance.shape == (design.size, design.size):
            W = self.build_weighting_matrix(design=design, full_space=True)

        elif variance.shape == (active.size, active.size):
            W = self.build_weighting_matrix(design=design, full_space=False)

        else:
            raise TypeError(
                f"The {variance.shape=} is not acceptable!\n"
                f"{design.size=}; {active.size=}"
            )

        # W Array/sparse --> variance
        if isinstance(variance, np.ndarray) and sp.issparse(W):
            W = W.toarray()
        elif isinstance(W, np.ndarray) and sp.issparse(variance):
            W = sp.csc_array(W, shape=variance.shape)

        # Apply the weighting matrix based on the registered scheme
        if self.design_weighting_scheme == "covariance":
            variance = variance * W

        elif self.design_weighting_scheme == "precision":
            inverter = sp.linalg.inv if sp.issparse(variance) else np.linalg.inv
            variance = inverter(inverter(variance) * W)

        else:
            raise PyOEDConfigsValidationError(
                f"\n<<< THIS IS A ***BUG*** PLEASE REPORT >>>\n"
                f"Invalid {self.configurations.design_weighting_scheme=} "
                f"of {type(self.configurations.design_weighting_scheme)=}\n"
                f"Expected either 'covariance' or 'precision'"
            )

        return variance


    @property
    def pointwise_weighting_kernel(self):
        """Name of the design weighting scheme"""
        return self.configurations.pointwise_weighting_kernel
    @pointwise_weighting_kernel.setter
    def pointwise_weighting_kernel(self, val):
        self.update_configurations(pointwise_weighting_kernel=val)

    @property
    def stdev(self, ):
        r"""
        Construct and return the lower Cholesky factor (if not available) in the
        projected space (after applying the design) of the covariance matrix

        .. note::
            The update of the covariance Cholesky factor based on the weighting scheme
            is as follows:

            1. `design_weighting_scheme` is `'covariance'`:

               .. math::
                    C \gets W \codt C \,.

               Thus, the covariance matrix is projected onto the active subspace, and
               the lower Cholesky factor is calculated there, and then weights are applied.

            2. `design_weighting_scheme` is `'precision'`:

                .. math::
                    C \gets \left( W \codt C^{-1} \right)^{\dagger} \,.
        """
        if self._STDEV is None:
            if self.verbose:
                print(
                    f"Generating lower Cholesky factor of the covariance matrix"
                )

            if self.active_size == 0:
                self._STDEV = sp.csc_array(np.array([[]])).reshape(0, 0) if self.sparse else np.array([[]]).reshape(0, 0)

            else:
                # The design used to create _STDEV
                self._DESIGN = utility.asarray(self.design)

                # Scale the lower Cholesky factor by the design (or its inverse)
                if self.design_weighting_scheme == "covariance":

                    # Weight the covariance matrix (in the reduced space)
                    cov =  self.build_weighting_matrix(full_space=False) \
                        * self.configurations.variance[self.active, :][:, self.active]

                    # 1) Create Cholesky factor (regardless of the weights):
                    self._STDEV = utility.factorize_spsd_matrix(
                        cov,
                        shape=(self.active_size, self.active_size),
                    )

                elif self.design_weighting_scheme == "precision":

                    # Weight the precision matrix (in the reduced space) then invert it
                    inverter = sp.linalg.inv if self.sparse else np.linalg.inv

                    if self.active_size >= 1:
                        cov = inverter(
                            self.build_weighting_matrix(full_space=False) \
                                * inverter(self.configurations.variance)[self.active, :][:, self.active]
                        )

                        ## NOTE: The following line is a workaround a bug in scipy linalg.inv
                        # sp.linalg.inv returns 1d if input shape is 1x1 (they have another bug with @)
                        # sp.csc_array does support construction from 1d
                        if self.active_size == 1 and self.sparse:
                            cov = sp.csc_array(
                                [[ cov[0] ]],
                                shape=(1, 1),
                            )

                        # Factorize
                        self._STDEV = utility.factorize_spsd_matrix(
                            cov,
                            shape=(self.active_size, self.active_size),
                        )

                    else:
                        self._STDEV = sp.csc_array([[]]) if self.sparse else np.array([[]])

                else:
                    raise PyOEDConfigsValidationError(
                        f"\n<<< THIS IS A ***BUG*** PLEASE REPORT >>>\n"
                        f"Invalid {self.configurations.design_weighting_scheme=} "
                        f"of {type(self.configurations.design_weighting_scheme)=}\n"
                        f"Expected either 'covariance' or 'precision'"
                    )

        return self._STDEV


