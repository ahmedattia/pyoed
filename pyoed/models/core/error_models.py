# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


from abc import ABC, abstractmethod
from dataclasses import dataclass

from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    validate_key,
    set_configurations,
)
import numpy as np

@dataclass(kw_only=True, slots=True)
class ErrorModelConfigs(PyOEDConfigs):
    """
    Base configuration class for error models
    """


@set_configurations(ErrorModelConfigs)
class ErrorModel(PyOEDObject):
    """
    Base class for error models. The simplest is a Gaussian error model.
    More will be added as needed.
    """

    def __init__(self, configs=None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        # Flag (lazy evaluation) to identify whether the model supports relaxed (non-binary) design
        self._SUPPORTS_RELAXATION = None


    def validate_configurations(
        self, configs: dict | PyOEDConfigs, raise_for_invalid: bool = True
    ) -> bool:
        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    @abstractmethod
    def generate_noise(self):
        """
        Generate a random noise vector sampled from the underlying distribution
        """
        ...

    @abstractmethod
    def sample(self):
        """
        Sample a random vector from the underlying distribution
        """
        ...

    def pdf(self, *args, **kwargs):
        """
        Evaluate the value of the density function (normalized or upto a *fixed* scaling
        constant) at the passed state/vector.
        """
        raise NotImplementedError(
            "The PDF evaluation function is not implemented for this model"
            "This needs to be implemented for each error model individually"
        )

    def pdf_gradient(self, *args, **kwargs):
        """
        Evaluate the gradient of the density function at the passed state/vector.
        """
        raise NotImplementedError(
            "The PDF evaluation function is not implemented for this model"
            "This needs to be implemented for each error model individually"
        )

    def log_density(self, *args, **kwargs):
        """
        Evaluate the logarithm of the density function at the passed state `x`.
        """
        raise NotImplementedError(
            "The PDF evaluation function is not implemented for this model"
            "This needs to be implemented for each error model individually"
        )

    def log_density_gradient(self, *args, **kwargs):
        """
        Evaluate the gradient of the logarithm of the density function at the passed state.
        """
        raise NotImplementedError(
            "The PDF evaluation function is not implemented for this model"
            "This needs to be implemented for each error model individually"
        )

    @property
    def supports_relaxation(self):
        """Flag to check whether the model supports relaxation or not"""
        if self._SUPPORTS_RELAXATION is None:

            # Test relaxation support by setting nonbinary design
            current_design = self.design
            relaxed_design = np.round(np.random.rand(self.size), 4)
            try:
                self.design = relaxed_design
            except:
                self._SUPPORTS_RELAXATION = False

            self._SUPPORTS_RELAXATION = np.allclose(self.design, relaxed_design)

            # Reset design
            self.design = current_design

        return self._SUPPORTS_RELAXATION

    @property
    @abstractmethod
    def size(self):
        """
        Dimension of the underlying probability distribution
        """
        ...

    @property
    def active(self):
        """
        Flags (boolean 1d array-like) corresponding to active/inactive dimensions
        """
        ...

    @property
    def random_seed(self):
        """
        Registered random seed (if available in configurations)
        """
        try:
            return self.configurations.random_seed
        except Exception as err:
            raise AttributeError(
                "Tried extracting the random seed from the configurations object "
                "but failed!\n"
                "See the caught error details below:\n"
                f"Unexpected {err=} of {type(err)=}"
            )
    @random_seed.setter
    def random_seed(self, val):
        """
        Update the random seed
        """
        try:
            return self.update_configurations(random_seed=val)
        except Exception as err:
            raise AttributeError(
                "Tried updating random seed in configurations but failed!\n"
                "See the caught error details below:\n"
                f"Unexpected {err=} of {type(err)=}"
            )

    @property
    @abstractmethod
    def active_size(self):
        """
        Dimension of the probability distribution projected onto the
        design space (only active/nonzero entries of the design)
        """
        ...

    @property
    @abstractmethod
    def design(self):
        """A **copy** of the design vector"""
        ...

    @property
    @abstractmethod
    def extended_design(self):
        """
        A vector (1d array-like) indicating active/inactive entries (with proper replication for multiple prognostic variables)
        of the observation vector/range
        """
        ...
