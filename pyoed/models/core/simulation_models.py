# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Abstract class for simulation models.
Code snippets given in the abstract methods is just given as an example, but will never utilized
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass, field

from pyoed import utility
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
    aggregate_configurations,
)


@dataclass(kw_only=True, slots=True)
class SimulationModelConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`SimulationModel` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`SimulationModelConfigs`
    provides the following fields:

    :param model_name: name of the model. Default is `None`.
    :param screen_output_iter: iteration interval for screen output. Default is 1. Note
        that this should be a positive integer to enforce proper effect.
    :param file_out_iter: iteration interval for file output. Default is 1. Note
        that this should be a positive integer to enforce proper effect.
    """

    model_name: str | None = None
    screen_output_iter: int = 1
    file_output_iter: int = 1


@set_configurations(SimulationModelConfigs)
class SimulationModel(PyOEDObject):
    """
    Abstract class (following Python's abc convention) for Simulation models (both
    time-dependent and time-independent)' (wrappers') implementation.

    The implementation in classes inheriting this base class MUST carry out all
    essential tasks (marked with abstractmethod decorators) which in turn should be
    provided by dynamical model.

    .. note::

        Each class derived from `SimulationModel` should have its own `__init__` method
        in which the constructor just calls `super().__init__(configs=configs)`
        and then add any additiona initialization as needed.
        The validation :py:meth:`self.validate_configurations` is carried out at the
        initialization time by the base class :py:class:`SimulationModel`.
        See for example, the `__init__` method of :py:class:`TimeIndependentModel`.

    .. note::

        The structure is similar to `DATeS`_ base class for simulation models.

    :param dict | SimulationModelConfigs | None configs: (optional) configurations for
        the model

    .. _DATeS: https://github.com/a-attia/DATeS
    """

    def __init__(self, configs: dict | SimulationModelConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | SimulationModelConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation model **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a SimulationModelConfigs object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeIndependentConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `model_name`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model_name",
            test=lambda v: isinstance(v, (str, type(None))),
            message=f"Model name is of invalid type. Expected None or string. ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `screen_output_iter`: positive integer >=1
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="screen_output_iter",
            test=lambda v: (utility.isnumber(v) and v == int(v) and v >= 1),
            message=f"Screen output iterations `screen_output_iter` is invalid. Expected postive integer >= 1",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `file_output_iter`: positive integer >=1
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="file_output_iter",
            test=lambda v: (utility.isnumber(v) and v == int(v) and v >= 1),
            message=f"File output iterations `file_output_iter` is invalid. Expected postive integer >= 1",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def get_model_grid(self):
        raise NotImplementedError(
            f"This model does not have an implementation of "
            f"the model grid function `get_model_grid`"
        )

    @property
    def model_grid(self):
        """
        Retrieve a copy of the model grid (enumeration of all model grid coordinates
        as they are ranked in a model state vector) as an array
        """
        return self.get_model_grid()

    @property
    def state_size(self):
        """
        An example to return the model state size; this should be overwridden by
        a more efficient implementation that does not require building a full state vector
        """
        if hasattr(self, "state_vector"):
            return self.state_vector().size
        else:
            return None

    @property
    def parameter_size(self):
        """
        An example to return the model parameter size; this should be overwridden by
        a more efficient implementation that does not require building a full parameter vector
        """
        if hasattr(self, "parameter_vector"):
            return self.parameter_vector().size
        else:
            return None


class TimeIndependentModelConfigs(SimulationModelConfigs):
    """
    Configuration dataclass for the :py:class:`TimeIndependentModel` abstract base
    class.
    This class mirrors :py:class:`SimulationModelConfigs` and does not add additional
    features/attributes.
    """


@set_configurations(TimeIndependentModelConfigs)
class TimeIndependentModel(SimulationModel):
    """
    Base class for time-independent models (such as tomography, ptychography, etc.)

    The implementation in classes inheriting this base class MUST carry out all
    essential tasks (marked with abstractmethod decorators) which in turn should be
    provided by dynamical model.

    :param [dict | SimulationModelConfigs | None] configs: (optional) configurations for the model
    """

    def __init__(self, configs: dict | TimeIndependentModelConfigs | None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

    def validate_configurations(
        self,
        configs: dict | TimeIndependentModelConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation model **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        .. note::
            The purposed of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a SimulationModelConfigs object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeIndependentConfigs`.
        """
        return super().validate_configurations(configs, raise_for_invalid)

    @abstractmethod
    def state_vector(self, init_val=0, **kwargs):
        """
        Create an instance of model state vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        ...

    @abstractmethod
    def parameter_vector(self, init_val=0, **kwargs):
        """
        Create an instance of model parameter vector

        :param float init_val: (optional) value assigned to entries of the parameter vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        ...

    @abstractmethod
    def is_state_vector(self, state, **kwargs):
        """Test whether the passed state vector is valid or not"""
        ...

    @abstractmethod
    def is_parameter_vector(self, parameter, **kwargs):
        """Test whether the passed parameter vector is valid or not"""
        ...

    @abstractmethod
    def solve_forward(self, state, verbose=False):
        """
        Apply (solve the forward model) to the given state,
            and return the result.

        :param state: state to which the forward model is applied.
        :param bool verboe: flag to control screen-verbosity
        :returns: result (usually an observation vector) resulting from applying the forward model to ``state``
        """
        pass

    def apply(self, *args, **kwargs):
        """calls ``self.solve_forward()``"""
        return self.solve_forward(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        """calls ``self.solve_forward()``"""
        return self.solve_forward(*args, **kwargs)

    def solve_adjoint(self, adjoint):
        """
        solve the adjoint problem (solve the model backward) to the
            given adjoint, and return the result.

        :param adjoint: adjoint to which the forward model is applied.
        :returns: result resulting from applying the adjoint model to `adjoint`
        """
        raise NotImplementedError(
            "This is not implemented in the particular model created!"
        )

    def Jacobian_T_matvec(self, state, eval_at):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
            the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at: state around which the Jacobian is evaluated
        """
        msg = """Similar to `solve_adjoint`, this method is not necessary for some
            \rassimilation methods, e.g., ensemble based,
             \rhowever, it is elementary for variational (4D-Var) assimialtion algorithms.
             \rIt is not always accessible."""
        raise NotImplementedError(msg)


@dataclass(kw_only=True, slots=True)
class TimeDependentModelConfigs(SimulationModelConfigs):
    """
    Configuration dataclass for the :py:class:`TimeDependentModel` base class.

    :param time_integration: dictionary holding time integration
        configurations:

          - `scheme`: string specifying the time integration scheme (e.g., 'RK4', 'RK45', 'BDF', etc.).
            Default is None.
          - `stepsize`: float specifying the time integration stepsize. Default is None.
          - `adaptive`: bool specifying whether the time integration is adaptive.
            Default is False.
    :param num_prognostic_variables: number of prognostic variables in the model.
        Default is None. Must be a positive integer if not None.
    :param space_discretization: dictionary holding space discretization
        configurations. Contains:

        - scheme: string specifying the space discretization scheme (e.g., 'FD', 'FE',
          'BE', etc.). Default is None.
    """

    time_integration: dict | None = field(
        default_factory=lambda: {
            "scheme": None,
            "stepsize": None,
            "adaptive": False,
        }
    )
    num_prognostic_variables: int | None = None
    space_discretization: dict | None = field(
        default_factory=lambda: {
            "scheme": None,
        }
    )


@set_configurations(TimeDependentModelConfigs)
class TimeDependentModel(SimulationModel):
    """
    Abstract class (following Python's abc convention) for PyOED dynamical models' (wrappers') implementation.

    A base class for dynamical models implementations/wrappers.

    The implementation in classes inheriting this base class MUST carry out
        all essential tasks (marked with abstractmethod decorators) which in turn should be provided
        by dynamical model.

    The structure is similar to `DATeS`_ base class for simulation models.

    .. _DATeS: https://github.com/a-attia/DATeS
    """

    def __init__(self, configs: dict | TimeDependentModelConfigs | None = None) -> None:
        # Create configurations object and join new/passed configurations if any
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

    def validate_configurations(
        self,
        configs: dict | TimeDependentModelConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validation stage for the the passed configs.

        :param configs: configurations to validate. If a TimeDependentModelConfigs
            object is passed, validation is performed on the entire set of
            configurations. However, if a dictionary is passed, validation is performed
            only on the configurations corresponding to the keys in the dictionary.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeIndependentConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        ## Validation Stage
        # `time_integration`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="time_integration",
            test=lambda v: isinstance(v, (dict, type(None))),
            message=lambda v: f"time_integration must be a dictionary or None. Received {type(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `num_prognostic_variables`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="num_prognostic_variables",
            test=lambda v: (
                (utility.isnumber(v) and v == int(v) and v >= 1) or (v is None)
            ),
            message=lambda v: f"`num_prognostic_variables` must be positive int or None. Received {type(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `space_discretization`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="space_discretization",
            test=lambda v: isinstance(v, (dict, type(None))),
            message=lambda v: f"`space_discretization` must be a dictionary or None. Received {type(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        ## Validate super to check configurations defined in parent class
        return super().validate_configurations(configs, raise_for_invalid)

    @abstractmethod
    def state_vector(self, t=None, init_val=0, **kwargs):
        """
        Create an instance of model state vector

        :param t: (optional) time assigned to the state vector (for time dependent models); None otherwise
        """
        if t is not None:
            assert (
                utility.isnumber(t) and t >= 0.0
            ), "time `t` must be a non-negative numer"
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"

    @abstractmethod
    def is_state_vector(self, state, **kwargs):
        """Test whether the passed state vector is valid or not"""
        ...

    @abstractmethod
    def integrate_state(self, state, tspan, checkpoints, *argv, **kwargs):
        """
        Simulate/integrate the mdoel starting from the initial `state` over the passed `checkpoints`.
            If state is assigned a time `t`, it is replaced with the first entry of `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time integration window
        :param checkpoints: times at which to store the computed solution, must be sorted and
            lie within tspan. If None (default), use points selected by the solver [t0, t1, ..., tf].

        :returns: timespan and a trajectory (checkpointed solution):
            - the timespan is an iterable (e.g., a list) hodlding timepoints at which state is propagated,
            - the trajectory is an iterable (e.g., a list) holding the model trajectory
            with entries corresponding to the simulated model state at entries of `checkpoints`
            starting from `checkpoints[0]` and ending at `checkpoints[-1]`.
        """
        ...

    def Jacobian_T_matvec(self, state, eval_at_t, eval_at):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
            the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        """
        msg = """This method is not necessary for some assimilation methods, e.g., ensemble based,
                 \rhowever, it is elementary for variational (4D-Var) assimialtion algorithms.
                 \rIt is not always accessible."""
        raise NotImplementedError(msg)
