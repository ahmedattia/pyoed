# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Base class for observation operators: transform a model state into an observaiton
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass, asdict
from typing import Self

from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    set_configurations,
    validate_key,
)


@dataclass(kw_only=True, slots=True)
class ObservationOperatorConfigs(PyOEDConfigs):
    """
    Base configuration class for observation operators.
    """


@set_configurations(ObservationOperatorConfigs)
class ObservationOperator(PyOEDObject):
    """
    Base class for observation operators: transform a model state into an observaiton
    """

    def __init__(self, configs: ObservationOperatorConfigs | dict | None = None):
        # Check the configurations data type
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(self, configs, raise_for_invalid=True) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each other,
        and with current configurations once combined.
        This guarantees that any key-value pair passed in configs can be properly used

        :param dict configs: a dictionary holding key/value configurations
        :param bool raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key

        :returns: ``bool`` flag indicating whether passed coinfigurations dictionary is valid or not

        :raises: see the parameter `raise_for_invalid`
        """
        return super().validate_configurations(configs, raise_for_invalid)

    @abstractmethod
    def observation_vector(self, *args, **kwargs):
        """
        Create an observation vector
        """

    @abstractmethod
    def apply(self, state, **kwargs):
        """Apply the observation operator to a model state, and return an observation instance"""
        ...

    def Jacobian_T_matvec(self, observation, eval_at, **kwargs):
        """
        Evaluate and return the product of the Jacobian (tangent-linear (TLM) of the observation operator),
            evaluated at `eval_at` transposed multiplied by the passed `observation`.
        """
        msg = """This method is not necessary for some assimilation methods, e.g., ensemble based,
                 \rhowever, it is elementary for variational (4D-Var) assimialtion algorithms.
                 \rIt is not always accessible."""
        raise NotImplementedError(msg)

    @abstractmethod
    def is_observation_vector(self, observation, **kwargs):
        """Test whether the passed observation vector is valid or not"""
        ...

    @property
    def observation_size(self) -> int:
        """
        An example to return the observation vector size;
        This should be overwridden by a more efficient implementation
        that does not require building a full state vector
        """
        return self.observation_vector().size

    def __call__(self, *args, **kwargs):
        """Apply the observation operator as a callable"""
        return self.apply(*args, **kwargs)

    # TODO: If this is unused, remove it.
    def copy(self) -> Self:
        """
        Return a copy of this observation operator by creating a new instance of this object with
        registered the configurations.
        """
        return self.__class__(asdict(self.configurations))
