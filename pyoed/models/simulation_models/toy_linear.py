# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
This module provides functionality to create and test very basic toy models for debugging, performance evaluation, etc.
"""

import re
from dataclasses import dataclass, field
import numpy as np
from scipy import sparse

from pyoed import SETTINGS as pyoed_settings
from pyoed.configs import validate_key, aggregate_configurations, set_configurations
from pyoed import utility
from pyoed.models.core import (
    TimeDependentModelConfigs,
    TimeDependentModel,
    TimeIndependentModelConfigs,
    TimeIndependentModel,
)
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)

_DEBUG = False


@dataclass(kw_only=True, slots=True)
class ToyLinearTimeIndependentConfigs(TimeIndependentModelConfigs):
    """
    Configurations for the :py:class:`ToyLinearTimeIndependent` model. This model is an
    implementation of a very simple model

    .. math::

        x = A p

    where :math:`x` is the model state, :math:`p` is the model parameter,
    and :math:`A` is an array representing the simulation model.

    In addition to the configurations available in
    :py:class:`TimeIndependentModelConfigs`, the configurations below are available.

    :param np: dimension of the model parameter. Default is 5. Note that this must be a
        positive integer.
    :param nx: dimension of the model state. Default is 8. Note that this must be a
        positive integer.
    :param num_decimals: Number of decimal points to use for rounding the randomly
        generated values of the model array :math:`A`. If `None`, no rounding takes
        place. Default is 2.
    :param random_seed: the random seed to use while generating the model/initial
        condition as well as the model array. Default is set to the PyOED's
        settings/default value of random seed found in
        `pyoed.configs.SETTINGS.RANDOM_SEED`.
    :param model_name: name of the model. Default is "Toy-Linear-Time-Independent".
    """

    np: int = 5
    nx: int = 8
    num_decimals: int = 2
    random_seed: int | None = pyoed_settings.RANDOM_SEED
    model_name: str = "Toy-Linear-Time-Independent"


@set_configurations(ToyLinearTimeIndependentConfigs)
class ToyLinearTimeIndependent(TimeIndependentModel, RandomNumberGenerationMixin):
    """
    Implementations of a toy linear model with equation :math:`x = A p` (or :math:`F(p,
    x) := A p - x = 0)`.  Here :math:`A` is assumed to be an (nx x np) matrix with
    :math`np` being the parameter space dimension, and :math:`nx` the dimension of the
    model state space. The state results by solving the model forward, i.e., by applying
    to the model equations to the parameter.  This is useful for testing time
    independent models such as imaging, etc.  An observation operator can be added on
    top of it, e.g., identity, with proper noise models for inversion/assimilation.  The
    underlying matrix `A` is randomly generated; reproduction can be controlled by
    choosing the random seed.

    :param ToyLinearTimeIndependentConfigs | dict | None configs: a dataclass containing
        configurations of the model. See :py:class:`ToyLinearTimeIndependentConfigs`.
    """

    def __init__(self, configs: dict | ToyLinearTimeIndependentConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)

        # Add further initialization as needed
        self.initialize()

    def initialize(self):
        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.random_seed)

        # Initialize the underlying model, and model grid
        self._MODEL_GRID = np.arange(self.nx)
        self._MODEL = self.create_model()

    def validate_configurations(
        self,
        configs: dict | TimeIndependentModelConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`ToyLinearTimeIndependentConfigs` are validated.  Finally, super
            classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeIndependentConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_integer = lambda x: (is_integer(x) and (x > 0))

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="np",
            test=is_positive_integer,
            message="np must be a positive integer.",
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nx",
            test=is_positive_integer,
            message="nx must be a positive integer.",
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda x: (x is None) or is_integer(x),
            message="Random seed must be None or an integer.",
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="num_decimals",
            test=lambda x: (x is None) or is_integer(x) or (x >= 0),
            message="Number of decimal points must be a non-negative integer or None.",
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_model(self):
        """
        Create the model matrix (linear dynamics operator).
        Here, the model is a normally distributed square matrix
        """
        # Generate a model-matrix randomly
        model = self.random_number_generator.standard_normal((self.nx, self.np))

        # round decimals
        num_decimals = self.configurations.num_decimals
        if num_decimals is not None:
            model = np.round(model, decimals=num_decimals)

        return model

    def parameter_vector(self, init_val=0):
        """
        Create an instance of model parameter vector

        :param float init_val: (optional) value assigned to entries of
            the state vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        parameter = np.empty(self.np)
        parameter[:] = init_val
        return parameter

    def is_parameter_vector(self, parameter):
        """Test whether the passed parameter vector is valid or not"""
        valid = False
        if isinstance(parameter, np.ndarray):
            if parameter.ndim == 1 and parameter.size == self.np:
                valid = True
        return valid

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector

        :param float init_val: (optional) value assigned to entries of
            the state vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        state = np.empty(self.nx)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self.nx:
                valid = True
        return valid

    def solve_forward(self, parameter, verbose=False):
        """
        Apply (solve the forward model) to the given parameter, and return the
        resulting state.

        :param parameter: parameter to which the forward model is applied.
        :returns: result from applying
            the model/matrix (`A`) to the passed ``parameter``
        """
        assert self.is_parameter_vector(
            parameter
        ), "passed parameter is not a valid parameter vector!"
        return self._MODEL.dot(parameter)

    def Jacobian_T_matvec(self, state, eval_at=None, verbose=False):
        """
        Multiply the Jacobian of the model (derivative of model equation w.r.t
        parameter) by the passed state

        :param state: state to which the forward sensitivities are applied to.
        :returns: result resulting from applying the forward sensitivities to
            model to the passed `state`
        """
        assert self.is_state_vector(state), "passed state is not valid!"
        return self._MODEL.T.dot(state)

    def get_model_array(self):
        """return a copy of the model operator (matrix)"""
        return self._MODEL.copy()

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def np(self):
        return self.configurations.np

    @property
    def nx(self):
        return self.configurations.nx

    @property
    def random_seed(self):
        return self.configurations.random_seed

    @property
    def num_decimals(self):
        return self.configurations.num_decimals


@dataclass(kw_only=True, slots=True)
class ToyLinearTimeDependentConfigs(TimeDependentModelConfigs):
    """
    Configurations for the ToyLinearTimeDependent model.

    :param nx: dimension of the model state. Default is 2
    :param dt: time step  assumed for time integration. Default is 0.5
    :param t_eps: a small number to compare time steps. Default is 1e-6.
    :param num_decimals: number of decimal points used to round the random model matrix.
        If `None`, no rounding takes place. Default is 2.
    :param random_seed: for reproduction of the model/initial condition.
    :param model_name: name of the model. Default is "Toy-Linear-Time-Dependent".
    """
    nx: int = 2
    dt: float = 0.5
    t_eps: float = 1e-6
    random_seed: int = 1234
    num_decimals: int | None = 2
    model_name: str = "Toy-Linear-Time-Dependent"


@set_configurations(ToyLinearTimeDependentConfigs)
class ToyLinearTimeDependent(TimeDependentModel, RandomNumberGenerationMixin):
    """
    Implementations of a toy linear model with linear forward operator\n
    .. math::
       x_{k+1} = Mx_k

    Here, An application of the operator :math:`M` is associated with the time step `dt`
    specified in the configurations dictionary.  Only multiples of this step size are
    allowed for forward integration (checkpointing) and/or adjoint integration (e.g.,
    sensitivity analysis).

    :param dict | ToyLinearTimeDependentConfigs | None configs: an object containing
        configurations of the toy linear model.
    """

    def __init__(self, configs: dict | ToyLinearTimeDependentConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs=configs)
        self.initialize()

    def initialize(self):
        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.random_seed)

        # Initialize the udnerlying model, model grid, and random initial
        # condition/state
        self._MODEL_GRID = np.arange(self.nx)
        self._MODEL = self.create_model()
        self._INITIAL_CONDITION = self.create_initial_condition()

    def validate_configurations(
        self,
        configs: dict | TimeDependentModelConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configurations and make sure they are conformable with each
        other, and with current configurations once combined. This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`ToyLinearTimeDependentConfigs` are validated. Finally, super
            classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeDependentConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: (is_integer(x) and (x > 0))
        is_float = lambda x: utility.isnumber(x) and x == float(x)
        is_positive_float = lambda x: (is_float(x) and (x > 0))

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nx",
            test=is_positive_integer,
            message="nx must be a positive integer.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="Time step must be a positive float.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="Time epsilon must be a positive float.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda x: (x is None) or is_integer(x),
            message="Random seed must be None or an integer.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="num_decimals",
            test=lambda x: (x is None) or is_positive_integer(x),
            message="Number of decimal points must be a positive integer or None.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_model(self):
        """
        Create the model matrix (linear dynamics operator).
        Here, the model is a normally distributed matrix
        """
        model = self.random_number_generator.standard_normal((self.nx, self.nx))

        # round decimals
        if self.num_decimals is not None:
            model = np.round(model, decimals=self.num_decimals)

        return model

    def create_initial_condition(self):
        """
        Create the initial condition associated with the passed model dimension
        We are assuming a random integer initial condition

        :returns: numpy array (same shape as model_grid associated with the model)
            holding initial condition values.
        """
        # Generate random state
        u_out = self.state_vector()
        u_out[:] = self.random_number_generator.standard_normal(u_out.size)

        return u_out

    def parameter_vector(self, init_val=0):
        """
        Create an instance of model parameter vector.
        Here, the parameter is the model initial state,  and thus :py:meth:`parameter_vector` actually calls :py:meth:`state_vector`

        :param float init_val: (optional) value assigned to entries of
            the parameter vector upon initialization
        """
        return self.state_vector(init_val=init_val)

    def is_parameter_vector(self, parameter):
        """
        Test whether the passed parameter vector is valid or not
        Here, the parameter is the model initial state,  and thus :py:meth:`is_parameter_vector` actually calls :py:meth:`is_state_vector`

        """
        return self.is_state_vector(parameter)

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector. Here, this is a 1D Numpy array of size
        equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        state = np.empty(self._MODEL_GRID.size, dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self._MODEL_GRID.size:
                valid = True
        return valid

    def integrate_state(self, state, tspan, checkpoints=None, verbose=False):
        """
        Simulate/integrate the model starting from the initial `state` over
            the passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time
            integration window
        :param checkpoints: times at which to store the computed solution, must be
            sorted and lie within tspan.  If None (default), use points selected by the
            solver [t0, t1, ..., tf].
        :param bool verbose: output progress to screen if `True`.  If set to `False`,
            nothing is printed to screen.

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of
            `checkpoints` starting from `checkpoints[0]` and ending at
            `checkpoints[-1]`.

        :raises:
            - `AssertionError` if `tspan` is not valid, or checkpoints are
              not within `tspan`
            - `TypeError` is raised if the passed state is not a valid state vector

        :remarks: This model is tied to a predefined time step dt in
            `self.configurations`; The distance between all checkpoints (including
            tspan) must be multiples of this timestep.
        """
        try:
            t0, tf = tspan
        except Exception as e:
            raise AssertionError(
                "tspan must be an iterable with two intries (t0, tf)"
            ) from e
        if t0 >= tf:
            raise ValueError("The timespan 'tspan' must be (t0, tf) with t0 < tf")

        # Number of decimal for proper rounding of timestepping
        num_decimals = max(1, len(np.format_float_positional(self.t_eps)) - 1)

        # Default timestep and checkpoints if not passed
        dt = self.dt
        if checkpoints is None:
            subwindow = np.round(tf - t0, decimals=num_decimals)
            nt = int(np.round(subwindow / dt))
            checkpoints = np.linspace(t0, t0 + dt * nt, nt + 1)

            if abs(tf - checkpoints[-1]) > self.t_eps:
                checkpoints = [t for t in checkpoints] + [tf]
            elif abs(checkpoints[-1] - tf) > self.t_eps:
                checkpoints[-1] = tf

        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(
            utility.asarray(checkpoints).flatten(), decimals=num_decimals
        )
        checkpoints = np.unique(checkpoints)

        # Make sure times in checkpoints are multiples of model's dt.
        # This is becuase the Matrix A of the model is defined as forward integrator over dt.
        steps = checkpoints[1:] - checkpoints[:-1]
        # print([float(x) for x in steps], dt, float(dt), [float(x) for x in np.remainder(steps, dt)])
        if utility.remainder(steps, dt, num_decimals=num_decimals).any():
            msg = f"This model is tied to a predefined time step dt={dt}"
            msg += "The distance between all checkpoints must be multiples of this timestep."
            if _DEBUG:
                print("DEBUG: checkpoints: {0}".format(checkpoints))
                print("DEBUG: INCREMENTS: {0}".format(steps))
                print("DEBUG: REMAINDERS: {0}".format(np.remainder(steps, dt)))
            raise AssertionError(msg)

        # Validate the passed timespan and checkpoints
        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 - self.t_eps <= checkpoints[0] <= checkpoints[-1] <= tf + self.t_eps
        ), "checkpoints must lie within the passed tspan!"

        # All good; proceed

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t

                # Number of steps/iterations to apply the underlying operator/matrix/model
                subwindow = np.round(t - flt_time, decimals=num_decimals)
                nt = int(np.round(subwindow / dt, decimals=num_decimals))

                # Another sanity check!
                if not abs(nt * dt - subwindow) < self.t_eps:
                    msg = "This should never happen!\n"
                    msg += f"nt:{nt};  dt:{dt};  t:{t};  flt_time:{flt_time}\n"
                    if _DEBUG:
                        print(
                            f"t-flt_time:{subwindow}; (t-flt_time)//dt:{subwindow//dt}  "
                        )
                        print(
                            f"round(t-flt_time)//dt:{np.round(t-flt_time, num_decimals)//dt}  "
                        )
                    raise ValueError(msg)

                # Propage state by repeated multiplication by the underlying model (nt times)
                for i in range(nt):
                    if verbose:
                        print(
                            f"Integration over interval [{flt_time}, {flt_time + dt}]"
                        )
                    flt_state = self._MODEL.dot(flt_state)
                    flt_time += dt

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def Jacobian_T_matvec(
        self, state, eval_at_t=None, eval_at=None, dt=None, sparse=True
    ):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
            the model (TLM) transposed, by a model state.
            This moodel is linear, so the TLM is the same as the underlying model

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """
        nx = self._MODEL_GRID.size
        assert (
            state.size == nx
        ), f"Size mismatch! The passed state `u` has size {state.size} while expected {nx} "
        if dt is None:
            dt = self.dt

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()

        JT_u = u_in.copy()

        # solve the tridiagonal system of linear equations
        JT_u = np.dot(self._MODEL.T, u_in)

        return JT_u

    def get_model_array(self):
        """return a copy of the model operator (matrix)"""
        return self._MODEL.copy()

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def nx(self):
        return self.configurations.nx

    @property
    def dt(self):
        return self.configurations.dt

    @property
    def t_eps(self):
        return self.configurations.t_eps

    @property
    def random_seed(self):
        return self.configurations.random_seed

    @property
    def num_decimals(self):
        return self.configurations.num_decimals


# ================================================================================ #
# Example Functions to help inistantiate instances of the models implemented here  #
# ================================================================================ #
def create_ToyLinearTimeIndependent_model(
    parameter_size=5,
    state_size=8,
    random_seed=1234,
    output_dir=pyoed_settings.OUTPUT_DIR,
):
    """
    A simplified interface to create instances of :py:class:`ToyLinearTimeIndependent`

    :param int np: number of parameter entries; i.e., size of the model's parameter vector
    :param int nx: number of state entries; i.e., size of the model's state vector
    :param random_seed: `None` or an integer random seed (passed to numpy)
        for reproduction of the model/initial condition
    :output_dir: Path to folder (will be created if doesn't exist) in
        which to write reports, output, etc.

    :returns: an instance of :py:class:`ToyLinearTimeIndependent`
    """
    configs = dict(
        np=parameter_size,
        nx=state_size,
        random_seed=random_seed,
        output_dir=output_dir,
    )
    return ToyLinearTimeIndependent(configs)


def create_ToyLinearTimeDependent_model(
    nx=3,
    dt=0.5,
    random_seed=1234,
    output_dir=pyoed_settings.OUTPUT_DIR,
):
    """
    A simplified interface to create toy linear time-dependent models;
        this inistantiates an instance of :py:class:`ToyLinearTimeDependent`

    :param int nx: number of state entries; i.e., size of the model vector
    :param float dt: time step  assumed for time integration.
    :param random_seed: `None` or an integer random seed (passed to numpy)
        for reproduction of the model/initial condition
    :param output_dir: Path to folder (will be created if doesn't exist) in
        which to write reports, output, etc.

    :returns: an instance of :py:class:`ToyLinearTimeDependent`
    """
    configs = ToyLinearTimeDependentConfigs(
        nx=nx,
        dt=dt,
        random_seed=random_seed,
        output_dir=output_dir,
    )
    return ToyLinearTimeDependent(configs)


## Add aliases for backward compatibitlity
create_time_independent_linear_model = create_ToyLinearTimeIndependent_model
create_time_dependent_linear_model = create_ToyLinearTimeDependent_model
