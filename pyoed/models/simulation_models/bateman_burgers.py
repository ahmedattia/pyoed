# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Bateman Burger's Equation model(s) (1D/2D)
"""
import numpy as np
import scipy.sparse as sp
from dataclasses import dataclass

from pyoed.models.core import (
    TimeDependentModel,
    TimeDependentModelConfigs,
)
from pyoed import utility
from pyoed.configs import (
    aggregate_configurations,
    validate_key,
    set_configurations,
    SETTINGS,
)


@dataclass(kw_only=True, slots=True)
class Burgers1DConfigs(TimeDependentModelConfigs):
    """
    Configuration class for the one-dimensional Bateman-Burgers' model, given by the equation:

    .. math::
       \\frac{\\partial u}{\\partial t} + u \\frac{\\partial u}{\\partial x}
       = \\nu \\frac{\\partial^2 u}{\\partial^2 x},\\, x\\in[0, L],\\, t\\in (0, t_f]

    :param domain: boundary of the spatial domain. Default is (0, 1)
    :param nu: kinematic viscosity
    :param nx: number of spatial discretization points (of the domain)
    :param dt: *default* time integration step size
    """

    model_name: str = "Burgers-1D"
    domain: tuple[float, float] = (0, 1)
    nu: float = 0.01
    nx: int = 101
    dt: float = 0.01
    t_eps: float = 1e-6


@set_configurations(Burgers1DConfigs)
class Burgers1D(TimeDependentModel):
    """
    Implementation of one-dimensional Bateman-Burgers' equation model.

    .. math::
       \\frac{\\partial u}{\\partial t} + u \\frac{\\partial u}{\\partial x}
        = \\nu \\frac{\\partial^2 u}{\\partial^2 x},\\, x\\in[0, L],\\, t\\in (0, t_f]

    This model is equivalent to the Navier-Stokes equation for incompressible flow with
    the pressure term removed.
    We assume Dirichlet homogeneous boundary conditions: :math:`u(0, t) = u(L, t) =
    0,\\, t \\in (0, t_f]`, and as initial conditions we use the smooth function:
    :math:`u(x, 0) = x \\sin(x) \\sin(\\pi x) \\exp{(x/10)}`.

    Spatial grid is equally-spaced based on the configurations passed upon
    initialization. We are using finite differences for spatial discretization.

    Time integration is carried out using a simple implicit Euler method, employing the
    Newton-Raphson iterations for which the Jacobian of the resiual term is required.

    :param configs: an object containing configurations of the one-dimensional Burgers'
        model. See :py:class:`Burgers1DConfigs` for more details.
    """

    def __init__(self, configs=None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        self._MODEL_GRID = np.linspace(self.domain[0], self.domain[1], self.nx)
        self._INITIAL_CONDITION = self.create_initial_condition()

    def validate_configurations(
        self,
        configs: dict | Burgers1DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`Burgers1DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`Burgers1DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="domain",
            test=lambda x: (len(x) == 2) and (x[0] < x[1]),
            message="Domain must have two entries with the first entry less than the second",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nu",
            test=is_positive_float,
            message="Kinematic viscosity must be a positive float",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nx",
            test=is_positive_integer,
            message="Discretization dimension must be a positive integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_initial_condition(self):
        """
        Create the initial condition associated with the passed model grid points
        We are assuming initial condition defined as :math:`\\sin(x) \\sin(\\pi x) \\exp{(0.1 x)}`
        where :math:`x` is a model grid point.

        :returns: numpy array (same shape as model_grid associated with the model) holding initial condition values.
        """
        # TODO: consider adding argument to enable multiple choices of the initial condition for flexibility
        # fw = lambda x: x * np.sin(x) * np.sin(np.pi * x) * np.exp(x/10.0)
        nu = self.nu
        fw = lambda x: x / (1 + np.exp((4 * x**2 - 1) / (16 * nu)))
        u_out = self.state_vector()
        u_out[:] = fw(self._MODEL_GRID)
        # zero-out boundaries (should be more flexible based on the choice of the boundary conditions (TODO)
        u_out[0] = 0.0
        u_out[-1] = 0.0

        return u_out

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector. Here, this is a 1D Numpy array of size equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        state = np.empty(self._MODEL_GRID.size, dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self._MODEL_GRID.size:
                valid = True
        return valid

    def integrate_state(self, state, tspan, checkpoints=None, verbose=False):
        """
        Simulate/integrate the mdoel starting from the initial `state` over the passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time integration window
        :param checkpoints: times at which to store the computed solution, must be sorted and
            lie within tspan. If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param bool verbose: output progress to screen if `True`.
            If set to `False` `scr_out_iter` is discarede, i.e., nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of `checkpoints`
            starting from `checkpoints[0]` and ending at `checkpoints[-1]`.

        :raises: AssertionError if `tspan` is not valid, or checkpoints are not within `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError
        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self._MODEL_T_EPS:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state, dt=dt, scr_out_iter=verbose
                    )
                    flt_time = flt_time + dt

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state, dt=t - flt_time, scr_out_iter=verbose
                    )
                    flt_time = t  # just for consistency

                else:
                    print(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )
                    raise ValueError

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def __implicit_Euler_step(self, state, dt, maxiter=500, tol=1e-10, scr_out_iter=5):
        """
        Integrate the model one step (of size `dt`) using implicit Euler method.
        The resulting linear system is solved using Newton Raphson, with maximum iterations set to `maxiter`

        :param state: the initial condition to propagate forward. Each entry of `state` corresponds to
            the velocity :math:`u` at the corresponding entry in the model grid
        :param float dt: the step size
        :param int maxiter: maximum number of iterations of the Newton Raphson solver
        :param float tol: tolerance of the Newton Raphson solver; here taken as the norm of the update
        :param int scr_out_iter: frequency of screen output of the Newton-Raphson solver;
            you can set to negative value or 0 to turn it screen output off.

        :returns: the result of applying implicit Euler once to `state` with time stepsize `dt`.
            result is a one dimensional numpy array of the same size as `state`.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        # Solve the linear system using Newton-Raphson algorithm
        nx = self.nx
        nu = self.nu
        dx = self._MODEL_GRID[1] - self._MODEL_GRID[0]
        rhs = np.zeros(nx)  # right-hand side

        # Finite-difference operators (sparse matrices) # TODO: Could be sped up -> Matrix-Free
        Ax = sp.lil_matrix((nx, nx), dtype=np.double)
        Ax.setdiag(1, k=1)
        Ax.setdiag(-1, k=-1)
        Ax[0, :] = 0.0
        Ax[-1, :] = 0.0
        Ax = Ax.tocsc()
        #
        Axx = sp.lil_matrix((nx, nx), dtype=np.double)
        Axx.setdiag(-2, k=0)
        Axx.setdiag(1, k=1)
        Axx.setdiag(1, k=-1)
        Axx[0, :] = 0.0
        Axx[-1, :] = 0.0
        Axx = Axx.tocsc()

        if scr_out_iter <= 0:
            scr_out_iter = maxiter + 1

        # Newton-Raphson
        converged = False
        for itr in range(maxiter):
            # Calculate the right-hand side (the residual)
            rhs = u_out - u_in
            rhs += dt * u_out * Ax.dot(u_out) / (2 * dx)
            rhs -= nu * dt * Axx.dot(u_out) / (dx**2)

            # Evaluate the Jacobian at the current iteration
            jac = self.__residual_Jacobian(u=u_out, dt=dt, sparse=True)

            # Solve for state update (du = - J^{-1} * residual)
            du = sp.linalg.spsolve(jac[1:-1, 1:-1], rhs[1:-1])

            # Update state; u = u - du
            u_out[1:-1] -= du

            du_tol = np.linalg.norm(du)
            if du_tol < tol:
                converged = True
                break

            if itr > 0 and (itr) % scr_out_iter == 0:
                print("itr: {0:04d} \t|du|={1:12.10g} ".format(itr + 1, du_tol))

        if not converged:
            print("Failed to converge in {0} iterations!".format(itr))
            print(du_tol)
            raise ValueError  # TODO: decide either to raise error or pass

        # save convergence info
        stat = dict(converged=converged, num_iter=itr, tol=du_tol)
        return u_out, stat

    def Jacobian_T_matvec(self, state, eval_at_t, eval_at, dt=None, sparse=True):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """

        # TODO: After implementing this function, migrate to Cython for fast
        # integration/solution
        nx = self.nx
        assert (
            state.size == nx
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx
        )

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()

        JT_u = u_in.copy()
        # Jacobian matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        A = self.__residual_Jacobian(u=eval_at, dt=dt, sparse=sparse)
        # solve the tridiagonal system of linear equations
        JT_u[1:-1] = sp.linalg.spsolve(A[1:-1, 1:-1].T, u_in[1:-1])
        # Enforce Dirichlet BCs
        JT_u[0] = 0.0
        JT_u[-1] = 0.0
        return JT_u

    def __residual_Jacobian(self, u, dt=None, sparse=True):
        """
        Evaluate the Jacobian of the residual term at state `u` and time `t` with a step size `dt`

        :param u: current velocity
        :param float dt: temporal stepsize. If `None`, default step size is loaded from model configurations
        :param bool sparse: if `True` return a sparse array (csc format used), otherwise return numpy array instance

        :returns: sparse (or dense based on`sparse`) matrix holding the Jacobian of the residual term
        """
        u = np.asarray(u).flatten()
        nx = self.nx
        assert (
            u.size == nx
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            u.size, nx
        )

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        dx = self._MODEL_GRID[1] - self._MODEL_GRID[0]
        nu = self.nu

        # Jacobian matrix (tridiagonal)
        jac = sp.lil_matrix((nx, nx), dtype=np.double)
        jac[0, 0:2] = 0.0
        jac[-1, -2:] = 0.0
        for i in range(1, nx - 1):
            jac[i, i - 1] = -dt * u[i] / (2 * dx) - nu * dt / (dx**2)
            jac[i, i] = (
                1 + dt * (u[i + 1] - u[i - 1]) / (2 * dx) + 2 * nu * dt / (dx**2)
            )
            jac[i, i + 1] = dt * u[i] / (2 * dx) - nu * dt / (dx**2)

        # fix boundaries
        jac[1, 0] = 0
        jac[-2, -1] = 0
        if not sparse:
            jac = jac.toarray()
        else:
            jac = jac.tocsc()
        return jac

    def exact_solution(self, t):
        """Exact solution for verification"""

        x = self._MODEL_GRID
        nu = self.nu
        true_state = self.state_vector()

        t0 = np.exp(1 / (8 * nu))
        true_state[:] = (
            x
            / (t + 1)
            / (1 + np.sqrt((t + 1) / t0) * np.exp((x**2) / (4 * nu * (t + 1))))
        )
        return true_state

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def domain(self) -> tuple[float, float]:
        """return the domain of the model"""
        return self.configurations.domain

    @property
    def nu(self) -> float:
        """return the kinematic viscosity"""
        return self.configurations.nu

    @property
    def nx(self) -> int:
        """return the number of spatial discretization points"""
        return self.configurations.nx

    @property
    def dt(self) -> float:
        """return the time integration step size"""
        return self.configurations.dt

    @property
    def t_eps(self) -> float:
        """return the time epsilon"""
        return self.configurations.t_eps


@dataclass(kw_only=True, slots=True)
class Burgers2DConfigs(TimeDependentModelConfigs):
    """
    Configuration class for the two-dimensional Bateman-Burgers' model, given by the equation:

    .. math::
        \\frac{\\partial u}{\\partial t} + u \\frac{\\partial u}{\\partial x} + v \\frac{\\partial u}{\\partial y}
        &= \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ), \\\\
        \\frac{\\partial v}{\\partial t} + u \\frac{\\partial v}{\\partial x} + v \\frac{\\partial v}{\\partial y}
        &= \\nu (\\frac{\\partial^2 v}{\\partial^2 x} + \\frac{\\partial^2 v}{\\partial^2 y} ),

    for :math:`x\\in[0, L_x],\\, y\\in[0, L_y],\\, t\\in (0, t_f]`.
    These equations can be also written in conservative form as follows,

    .. math::
       \\frac{\\partial u}{\\partial t} + \\frac{\\partial u^2}{\\partial x} + \\frac{\\partial uv}{\\partial y}
       &= \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ), \\\\
       \\frac{\\partial v}{\\partial t} + \\frac{\\partial uv}{\\partial x} + \\frac{\\partial v^2}{\\partial y}
       &= \\nu (\\frac{\\partial^2 v}{\\partial^2 x} + \\frac{\\partial^2 v}{\\partial^2 y} ).

    We assume a time-dependent non-zero Dirichlet boundary conditions, extracted from the following exact solution:

    .. math::
       u(x,y,t) = \\frac{3}{4} - \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}, \\\\
       v(x,y,t) = \\frac{3}{4} + \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}.

    Initial conditions are also defined from the equation above (at :math:`t=0`).

    """

    model_name: str = "Burgers-2D"
    domain: tuple[tuple[float, float], tuple[float, float]] = ((0, 1), (0, 1))
    nu: float = 0.01
    nx: int = 101
    ny: int = 101
    dt: float = 0.001
    t_eps: float = 1e-6


@set_configurations(Burgers2DConfigs)
class Burgers2D(TimeDependentModel):
    """
    Implementations of the two-dimensional Bateman-Burgers' model, whose convective form is defined below:

    .. math::
           \\frac{\\partial u}{\\partial t} + u \\frac{\\partial u}{\\partial x} + v \\frac{\\partial u}{\\partial y}
            &= \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ), \\\\
            \\frac{\\partial v}{\\partial t} + u \\frac{\\partial v}{\\partial x} + v \\frac{\\partial v}{\\partial y}
            &= \\nu (\\frac{\\partial^2 v}{\\partial^2 x} + \\frac{\\partial^2 v}{\\partial^2 y} ),

    for :math:`x\\in[0, L_x],\\, y\\in[0, L_y],\\, t\\in (0, t_f]`.
    These equations can be also written in conservative form as follows,

    .. math::
       \\frac{\\partial u}{\\partial t} + \\frac{\\partial u^2}{\\partial x} + \\frac{\\partial uv}{\\partial y}
        &= \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ), \\\\
        \\frac{\\partial v}{\\partial t} + \\frac{\\partial uv}{\\partial x} + \\frac{\\partial v^2}{\\partial y}
        &= \\nu (\\frac{\\partial^2 v}{\\partial^2 x} + \\frac{\\partial^2 v}{\\partial^2 y} ).

    We assume a time-dependent non-zero Dirichlet boundary conditions, extracted from the following exact solution:

    .. math::
       u(x,y,t) = \\frac{3}{4} - \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}, \\\\
       v(x,y,t) = \\frac{3}{4} + \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}.

    Initial conditions are also defined from the equation above (at :math:`t=0`).
    Spatial grid is equally-spaced based on the configurations passed upon initialization.
    We are using finite differences for spatial discretization.
    Time integration is carried out using an explicit fourth-order Runge-Kutta method,
    which requires some attention to ensure stability.

    :param configs: An object containing configurations of the two-dimensional Burgers' model.
    """

    def __init__(self, configs: dict | Burgers2DConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        _x = np.linspace(self.domain[0][0], self.domain[0][1], self.nx)
        _y = np.linspace(self.domain[1][0], self.domain[1][1], self.ny)
        [xx, yy] = np.meshgrid(_x, _y)
        self._MODEL_GRID = np.tile(
            np.hstack([xx.reshape(-1, 1), yy.reshape(-1, 1)]), (2, 1)
        )
        self._INITIAL_CONDITION = self.create_initial_condition()

    def validate_configurations(
        self,
        configs: dict | Burgers2DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`Burgers2DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`Burgers2DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="domain",
            test=lambda x: (len(x) == 2) and all([x[i][0] < x[i][1] for i in range(2)]),
            message="Domain must have two entries with the first entry less than the second",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nu",
            test=is_positive_float,
            message="Kinematic viscosity must be a positive float",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=dim_param,
                    test=is_positive_integer,
                    message=f"{dim_param} must be a positive integer!",
                    raise_for_invalid=raise_for_invalid,
                )
                for dim_param in ["nx", "ny"]
            ]
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_initial_condition(self):
        """
        Create the initial condition associated with the passed model grid points
        We are assuming ... initial condition defined as

        .. math::
           u(x,y,t) = \\frac{3}{4} - \\frac{1}{4[1+\\exp(\\frac{-4x+4y}{32\\nu})]}, \\\\
           v(x,y,t) = \\frac{3}{4} + \\frac{1}{4[1+\\exp(\\frac{-4x+4y}{32\\nu})]}.

        where :math:`x, y` define the model grid coordinates.

        :returns: 1D numpy array holding initial condition values.
        """
        # TODO: consider adding argument to enable multiple choices of the initial condition for flexibility
        nu, nx, ny = self.nu, self.nx, self.ny
        fw = lambda x, y: np.concatenate(
            [
                3 / 4 - 1 / (4 * (1 + np.exp((-4 * x + 4 * y) / (32 * nu)))),
                3 / 4 + 1 / (4 * (1 + np.exp((-4 * x + 4 * y) / (32 * nu)))),
            ]
        )
        u_out = self.state_vector()
        u_out[:] = fw(self._MODEL_GRID[: nx * ny, 0], self._MODEL_GRID[: nx * ny, 1])
        return u_out

    def set_boundary_condition(self, state, t):
        """
        Set the boundary conditions, defined using the followin relation:

        .. math::
           u(x,y,t) = \\frac{3}{4} - \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}, \\\\
           v(x,y,t) = \\frac{3}{4} + \\frac{1}{4[1+\\exp(\\frac{-4x+4y-t}{32\\nu})]}.

        :returns: numpy array with set boundary condition values.
        """
        nu, nx, ny = self.nu, self.nx, self.ny
        fw = lambda x, y: np.concatenate(
            [
                3 / 4 - 1 / (4 * (1 + np.exp((-4 * x + 4 * y - t) / (32 * nu)))),
                3 / 4 + 1 / (4 * (1 + np.exp((-4 * x + 4 * y - t) / (32 * nu)))),
            ]
        )

        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        for i in [
            0,
            nx - 1,
        ]:  # left and right boundaries (x=0 for i=0, and x=lx for i=nx-1)
            _ind = [i + j * nx for j in range(ny)]
            u_out[_ind + [k + nx * ny for k in _ind]] = fw(
                self._MODEL_GRID[_ind, 0], self._MODEL_GRID[_ind, 1]
            )

        for j in [
            0,
            ny - 1,
        ]:  # top and bpttom boundaries (y=0 for j=0, and y=ly for j=ny-1)
            _ind = [i + j * nx for i in range(nx)]
            u_out[_ind + [k + nx * ny for k in _ind]] = fw(
                self._MODEL_GRID[_ind, 0], self._MODEL_GRID[_ind, 1]
            )
        return u_out

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector. Here, this is a 1D Numpy array
        of length equal to double the length of the model grid (for u and v).

        :param float init_val: value assigned to entries of the state vector
        :returns: 1D numpy array
        """
        state = np.empty(self._MODEL_GRID.shape[0], dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self._MODEL_GRID.shape[0]:
                valid = True
        return valid

    def integrate_state(
        self, state, tspan, checkpoints=None, form="conv", verbose=False
    ):
        """
        Simulate/integrate the model starting from the initial `state` over the passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time integration window
        :param checkpoints: times at which to store the computed solution, must be sorted and
            lie within tspan. If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param form: formulation of the 2D Burgers problem,
            either conv' for convective or 'cons' for conservative. Default is convective formulation.
        :param bool verbose: output progress to screen if `True`.
            If set to `False`, nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of `checkpoints`
            starting from `checkpoints[0]` and ending at `checkpoints[-1]`.

        :raises: AssertionError if `tspan` is not valid, or checkpoints are not within `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError
        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self.t_eps:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state = self.__explicit_RungeKutta4_step(
                        state=flt_state, dt=dt, form=form
                    )
                    flt_time = flt_time + dt

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state = self.__explicit_RungeKutta4_step(
                        state=flt_state, dt=t - flt_time, form=form
                    )
                    flt_time = t  # just for consistency

                else:
                    raise ValueError(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def __explicit_RungeKutta4_step(self, state, dt, form):
        """
        Integrate the model one step (of size `dt`) using the explicit 4th order Runge-Kutta method.

        :param state: the initial condition to propagate forward. Each entry of `state` corresponds to
            the velocity components at the corresponding entries in the model grid
        :param float dt: the step size

        :returns: the result of applying explicit 4th order Runge-Kutta once to `state` with time stepsize `dt`.
            result is a one dimensional numpy array of the same size as `state`.

        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution
        nx, ny = self.nx, self.ny

        assert (
            state.size == nx * ny * 2
        ), "Size mismatch! The passed state `u` has size {0} while expected {1}*2 = {2} ".format(
            state.size, nx * ny, nx * ny * 2
        )

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        kk = np.zeros(state.size, dtype=np.double)
        _c0 = np.array([0, 0.5, 0.5, 1])
        _c1 = np.array([1, 2, 2, 1]) * dt / 6

        for i in range(4):
            kk = self.__rhs(u_in + kk * dt * _c0[i], form=form)
            u_out += kk * _c1[i]

        return u_out

    def Jacobian_T_matvec(
        self, state, eval_at_t, eval_at, dt=None, sparse=True, form="conv"
    ):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """

        # TODO: After implementing this function, migrate to Cython for fast integration/solution
        nx, ny = self.nx, self.ny

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        assert (
            state.size == nx * ny * 2
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx * ny * 2
        )

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        # JT_u = u_in.copy()

        # linear system matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        Jac = self.__explicit_RungeKutta4_jacobian(
            eval_at, dt, form=form, sparse=sparse
        )
        if not sparse:
            JT_u = np.dot(Jac.T, u_in)
        else:
            JT_u = (Jac.transpose()).dot(u_in)
        return JT_u

    def __rhs(self, state, form):
        """
        Compute the the right-hand side (the residual :math:`\\frac{du_{i,j}}{dt}=r^{u}_{i,j},
        \\, \\frac{dv_{i,j}}{dt}=r^{v}_{i,j}`) of the 2D Burgers problem
        using second order centered finite difference discretization to approximate the spatial derivatives.\n
        Two options exist, inclduing the convective and conservative forms of the 2D Burgers equations

        :param state: the state at which the right-hand side is evaluated to propagate forward.
               The `state` is 1D array, whose entries correspond to
               the velocity components at the corresponding entries in the model grid

        :param form: the state at which the right-hand side is evaluated to propagate forward.
               The `state` is 1D array, whose entries correspond to
               the velocity components at the corresponding entries in the model grid

        :returns: the residual r as a one dimensional numpy array of the same shape as `state`.

        """

        nx, ny, nu = self.nx, self.ny, self.nu
        dx = self._MODEL_GRID[1, 0] - self._MODEL_GRID[0, 0]
        dy = self._MODEL_GRID[nx, 1] - self._MODEL_GRID[0, 0]

        u = self.state_vector()
        u[:] = state[:].copy()

        i = np.arange(1, nx - 1)  # internal points in x-direction
        j = np.arange(1, ny - 1)  # internal points in y-direction
        [_i, _j] = np.meshgrid(i, j)  # to mitigate nested loops
        _i, _j = _i.flatten(), _j.flatten()
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # east
        _indW = (_i - 1) + _j * nx  # west
        _indN = _i + (_j + 1) * nx  # north
        _indS = _i + (_j - 1) * nx  # south

        _ind2 = [k + nx * ny for k in _ind]
        _ind2E = [k + nx * ny for k in _indE]
        _ind2W = [k + nx * ny for k in _indW]
        _ind2N = [k + nx * ny for k in _indN]
        _ind2S = [k + nx * ny for k in _indS]

        r = np.zeros(nx * ny * 2, dtype=np.double)

        if form == "conv":  # convective form
            r[_ind] = (
                -u[_ind] * (u[_indE] - u[_indW]) / (2 * dx)
                - u[_ind2] * (u[_indN] - u[_indS]) / (2 * dy)
                + nu * (u[_indE] - 2 * u[_ind] + u[_indW]) / (dx**2)
                + nu * (u[_indN] - 2 * u[_ind] + u[_indS]) / (dy**2)
            )

            r[_ind2] = (
                -u[_ind] * (u[_ind2E] - u[_ind2W]) / (2 * dx)
                - u[_ind2] * (u[_ind2N] - u[_ind2S]) / (2 * dy)
                + nu * (u[_ind2E] - 2 * u[_ind2] + u[_ind2W]) / (dx**2)
                + nu * (u[_ind2N] - 2 * u[_ind2] + u[_ind2S]) / (dy**2)
            )

        elif form == "cons":  # conservative form
            r[_ind] = (
                -(u[_indE] ** 2 - u[_indW] ** 2) / (2 * dx)
                - (u[_indN] * u[_ind2N] - u[_indS] * u[_ind2S]) / (2 * dy)
                + nu * (u[_indE] - 2 * u[_ind] + u[_indW]) / (dx**2)
                + nu * (u[_indN] - 2 * u[_ind] + u[_indS]) / (dy**2)
            )

            r[_ind2] = (
                -(u[_indE] * u[_ind2E] - u[_indW] * u[_ind2W]) / (2 * dx)
                - (u[_ind2N] ** 2 - u[_ind2S] ** 2) / (2 * dy)
                + nu * (u[_ind2E] - 2 * u[_ind2] + u[_ind2W]) / (dx**2)
                + nu * (u[_ind2N] - 2 * u[_ind2] + u[_ind2S]) / (dy**2)
            )

        return r

    def __Jrhs(self, state, form, sparse=True):
        """
        Compute the the Jacobian of the right-hand side for the 2D Burgers problem
        using second order centered finite difference discretization to approximate the spatial derivatives.\n
        Two options exist, inclduing the convective and conservative forms of the 2D Burgers equations

        :param state: the state at which the Jacobian of the right-hand side is evaluated.
               The `state` is 1D array, whose entries correspond to
               the velocity components at the corresponding entries in the model grid

        :returns: the Jacobian a two-dimensional numpy array.

        """

        nx, ny, nu = self.nx, self.ny, self.nu
        dx = self._MODEL_GRID[1, 0] - self._MODEL_GRID[0, 0]
        dy = self._MODEL_GRID[nx, 1] - self._MODEL_GRID[0, 0]

        u = self.state_vector()
        u[:] = state[:].copy()

        i = np.arange(1, nx - 1)  # internal points in x-direction
        j = np.arange(1, ny - 1)  # internal points in y-direction
        [_i, _j] = np.meshgrid(i, j)  # to mitigate nested loops
        _i, _j = _i.flatten(), _j.flatten()
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # east
        _indW = (_i - 1) + _j * nx  # west
        _indN = _i + (_j + 1) * nx  # north
        _indS = _i + (_j - 1) * nx  # south

        _ind2 = [k + nx * ny for k in _ind]
        _ind2E = [k + nx * ny for k in _indE]
        _ind2W = [k + nx * ny for k in _indW]
        _ind2N = [k + nx * ny for k in _indN]
        _ind2S = [k + nx * ny for k in _indS]

        Jr = sp.lil_matrix((nx * ny * 2, nx * ny * 2), dtype=np.double)

        if form == "conv":  # convective form
            Jr[_ind, _ind] = (
                -(u[_indE] - u[_indW]) / (2 * dx)
                + nu * (-2) / (dx**2)
                + nu * (-2) / (dy**2)
            )
            Jr[_ind, _indE] = -u[_ind] * (1) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind, _indW] = -u[_ind] * (-1) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind, _indN] = -u[_ind2] * (1) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind, _indS] = -u[_ind2] * (-1) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind, _ind2] = -1 * (u[_indN] - u[_indS]) / (2 * dy)

            Jr[_ind2, _ind] = -1 * (u[_ind2E] - u[_ind2W]) / (2 * dx)
            Jr[_ind2, _ind2E] = -u[_ind] * (1) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind2, _ind2W] = -u[_ind] * (-1) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind2, _ind2N] = -u[_ind2] * (1) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind2, _ind2S] = -u[_ind2] * (-1) / (2 * dy) + nu * (1) / (dy**2)

        elif form == "cons":  # conservative form
            Jr[_ind, _ind] = nu * (-2) / (dx**2) + nu * (-2) / (dy**2)
            Jr[_ind, _indE] = -(2 * u[_indE]) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind, _indW] = -(-2 * u[_indW]) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind, _indN] = -(u[_ind2N]) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind, _indS] = -(-u[_ind2S]) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind, _ind2N] = -(u[_indN]) / (2 * dy)
            Jr[_ind, _ind2S] = -(-u[_indS]) / (2 * dy)

            Jr[_ind2, _ind2] = nu * (-2) / (dx**2) + nu * (-2) / (dy**2)
            Jr[_ind2, _ind2E] = -(u[_indE]) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind2, _ind2W] = -(-u[_indW]) / (2 * dx) + nu * (1) / (dx**2)
            Jr[_ind2, _ind2N] = -(2 * u[_ind2N]) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind2, _ind2S] = -(-2 * u[_ind2S]) / (2 * dy) + nu * (1) / (dy**2)
            Jr[_ind2, _indE] = -(u[_ind2E]) / (2 * dx)
            Jr[_ind2, _indW] = -(-u[_ind2W]) / (2 * dx)

        # fix boundaries
        _j = np.arange(ny)
        _i = 1
        _ind = _i + _j * nx
        _indW = (_i - 1) + _j * nx  # left boundary
        _ind2 = _ind + nx * ny
        _ind2W = _indW + nx * ny
        Jr[_ind, _indW], Jr[_ind, _ind2W] = 0, 0
        Jr[_ind2, _indW], Jr[_ind2, _ind2W] = 0, 0

        _i = nx - 2
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # right boundary
        _ind2 = _ind + nx * ny
        _ind2E = _indE + nx * ny
        Jr[_ind, _indE], Jr[_ind, _ind2E] = 0, 0
        Jr[_ind2, _indE], Jr[_ind2, _ind2E] = 0, 0

        _i = np.arange(nx)
        _j = 1
        _ind = _i + _j * nx
        _indS = _i + (_j - 1) * nx  # bottom boundary
        _ind2 = _ind + nx * ny
        _ind2S = _indS + nx * ny
        Jr[_ind, _indS], Jr[_ind, _ind2S] = 0, 0
        Jr[_ind2, _indS], Jr[_ind2, _ind2S] = 0, 0

        _j = ny - 2
        _ind = _i + _j * nx
        _indN = _i + (_j + 1) * nx  # top boundary
        _ind2 = _ind + nx * ny
        _ind2N = _indN + nx * ny
        Jr[_ind, _indN], Jr[_ind, _ind2N] = 0, 0
        Jr[_ind2, _indN], Jr[_ind2, _ind2N] = 0, 0

        if not sparse:
            Jr = Jr.toarray()
        else:
            Jr = Jr.tocsc()

        return Jr

    def __explicit_RungeKutta4_jacobian(self, state, dt, form, sparse=True):
        """
        Evaluate the Jacobian matrix of the forward model (TLM) using the explicit 4th order Runge-Kutta method.

        :param state: the state at which the Jacobian is evaluated. Each entry of `state` corresponds to
            the velocity :math:`u` at the corresponding entries in the model grid
        :param float dt: the step size

        :returns: the Jacobian matrix employing the explicit 4th order Runge-Kutta.

        """
        # TODO: After implementing this function, migrate to Cython for fast
        # integration/solution
        nx, ny = self.nx, self.ny

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()

        Jac = sp.identity(state.size, dtype=np.double, format="csc")
        I = sp.identity(state.size, dtype=np.double, format="csc")

        kk = np.zeros(state.size, dtype=np.double)
        dkk = sp.csc_matrix((nx * ny * 2, nx * ny * 2), dtype=np.double)

        # TODO: check the loop implementation/math
        _c0 = np.array([0, 0.5, 0.5, 1])
        _c1 = np.array([1, 2, 2, 1]) * dt / 6
        for i in range(4):
            dkk = (self.__Jrhs(u_in + kk * dt * _c0[i], form, sparse)).dot(
                I + dkk * dt * _c0[i]
            )
            kk = self.__rhs(u_in + kk * dt * _c0[i], form)
            Jac += dkk * _c1[i]

        Jac = Jac.tolil()

        # fix boundaries
        for i in [
            0,
            nx - 1,
        ]:  # left and right boundaries (x=0 for i=0, and x=lx for i=nx-1)
            _ind = [i + j * nx for j in range(ny)]
            Jac[_ind + [k + nx * ny for k in _ind], :] = 0

        for j in [
            0,
            ny - 1,
        ]:  # top and bottom boundaries (y=0 for j=0, and y=ly for j=ny-1)
            _ind = [i + j * nx for i in range(nx)]
            Jac[_ind + [k + nx * ny for k in _ind], :] = 0

        if not sparse:
            Jac = Jac.toarray()
        else:
            Jac = Jac.tocsc()

        return Jac

    def exact_solution(self, t):
        """Exact solution for verification"""

        nu, nx, ny = self.nu, self.nx, self.ny

        x = self._MODEL_GRID[: nx * ny, 0]
        y = self._MODEL_GRID[: nx * ny, 1]
        true_state = self.state_vector()
        true_state[:] = np.concatenate(
            [
                3 / 4 - 1 / (4 * (1 + np.exp((-4 * x + 4 * y - t) / (32 * nu)))),
                3 / 4 + 1 / (4 * (1 + np.exp((-4 * x + 4 * y - t) / (32 * nu)))),
            ]
        )
        return true_state

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def domain(self) -> tuple:
        """retrieve the model domain"""
        return self.configurations.domain

    @property
    def nu(self) -> float:
        """retrieve the model diffusion coefficient"""
        return self.configurations.nu

    @property
    def nx(self) -> int:
        """retrieve the model x-discretization size"""
        return self.configurations.nx

    @property
    def ny(self) -> int:
        """retrieve the model y-discretization size"""
        return self.configurations.ny

    @property
    def dt(self) -> float:
        """retrieve the model time step size"""
        return self.configurations.dt

    @property
    def t_eps(self) -> float:
        """retrieve the model time epsilon"""
        return self.configurations.t_eps


def create_Burgers_1D_model(
    domain=(0, 1), nu=0.005, nx=101, dt=0.01, output_dir=SETTINGS.OUTPUT_DIR
):
    """
    A simple interface to creating an instance of `Burgers1D`.

    :param domain: boundary of the spatial domain. Default is (-1, 1)
    :param float nu: kinematic viscosity -- diffusion coefficient
    :param int nx: number of spatial discretization points (of the domain)
    :param float dt: *default* time integration step size
    :output_dir: Path to folder (will be created if doesn't exist)
        in which to write reports, output, etc.

    :returns: an instance of :py:class:`Burgers1D`
    """
    configs = Burgers1DConfigs(
        domain=domain, nu=nu, nx=nx, dt=dt, output_dir=output_dir
    )
    model = Burgers1D(configs=configs)
    return model


def create_Burgers_2D_model(
    domain=((0, 1), (0, 1)),
    nu=0.005,
    nx=101,
    ny=101,
    dt=0.01,
    output_dir=SETTINGS.OUTPUT_DIR,
):
    """
    A simple interface to creating an instance of `Burgers2D`.

    :param domain: boundary of the spatial domain.
    :param float nu: kinematic viscosity -- diffusion coefficient
    :param int nx: number of spatial discretization points (of the domain)
        in the x-direction
    :param int ny: number of spatial discretization points in the y-direction
    :param float dt: *default* time integration step size
    :output_dir: Path to folder (will be created if doesn't exist) in which to write reports, output, etc.

    :returns: an instance of :py:class:`AdvectionDiffusion1D`
    """
    configs = Burgers2DConfigs(
        domain=domain, nu=nu, nx=nx, ny=ny, dt=dt, output_dir=output_dir
    )
    model = Burgers2D(configs=configs)
    return model
