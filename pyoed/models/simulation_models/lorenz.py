# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
Implementations of multiple variants of the Lorenz model.
"""
from dataclasses import dataclass, field
from collections.abc import Callable

import numpy as np
import scipy.sparse as sp

from pyoed.models.core import (
    TimeDependentModel,
    TimeDependentModelConfigs,
)
from pyoed import utility
from pyoed.configs import (
    validate_key,
    aggregate_configurations,
    set_configurations,
    SETTINGS,
)


@dataclass(kw_only=True, slots=True)
class Lorenz63Configs(TimeDependentModelConfigs):
    """
    Configuration dataclass for the Lorenz-63 model. Recall that the Lorenz-63 model is
    given by the equations

     .. math::
       \\frac{d x}{d t} &= \\sigma (y-x) \\\\
       \\frac{d y}{d t} &= x(\\rho-z) - y \\\\
       \\frac{d z}{d t} &= xy - \\beta z \\\\

    :param sigma: :math:`\\sigma`. Default is 10.0. Must be positive.
    :param rho: :math:`\\rho`. Default is 28.0. Must be positive.
    :param beta: :math:`\\beta`. Default is 8.0/3.0. Must be positive.
    :param ic: initial condition. Default is [1.0, 1.0, 1.0].
    :param dt: time integration step size. Default is 0.01. Must be positive.
    :param t_eps: tolerance for time step comparison. Default is 1e-8. Must be positive
        and smaller than `dt`.
    """

    sigma: float = 10.0
    rho: float = 28.0
    beta: float = 8.0 / 3.0
    ic: np.ndarray = field(default_factory=lambda: np.array([1.0, 1.0, 1.0]))
    dt: float = 0.01
    t_eps: float = 1e-8
    model_name: str = "Lorenz-63: Three-Variable Model"


@set_configurations(Lorenz63Configs)
class Lorenz63(TimeDependentModel):
    """
    Implementations of the Chaotic Lorenz-63 model

     .. math::
       \\frac{d x}{d t} &= \\sigma (y-x) \\\\
       \\frac{d y}{d t} &= x(\\rho-z) - y \\\\
       \\frac{d z}{d t} &= xy - \\beta z \\\\

    This model is a toy model in meteorology and exhibit a notable chaotic behavior
    for specific values of the parameters :math:`\\sigma, \\rho, \\beta`.

    Time integration is carried out using a simple implicit Euler method, employing
    the Newton-Raphson iterations for which the Jacobian of the resiual term is required.

    :param Lorenz63Configs | dict | None configs:  Configurations of the model, either
        as an instance of Lorenz63Configs, a dictionary, or None. The default is None,
        which uses the default configurations.
    """

    def __init__(self, configs: Lorenz63Configs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def create_initial_condition(self):
        """
        Create the initial condition for the Lorenz 63 model

        :returns: 1D numpy array holding initial condition values.
        """
        ic = self.ic
        u_out = self.state_vector()
        u_out[:] = ic
        return u_out

    def validate_configurations(
        self,
        configs: dict | Lorenz63Configs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`Lorenz63Configs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`Lorenz63Configs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self, configs=configs, configs_class=self.configurations_class
        )

        # Local tests
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=lorenz_param,
                    test=is_positive_float,
                    message=f"{lorenz_param} must be a positive float!",
                    raise_for_invalid=raise_for_invalid,
                )
                for lorenz_param in ["sigma", "rho", "beta"]
            ]
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector.
        Here, this is a 1D Numpy array with three entries corresponding to the Lorenz 63 variables.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        state = np.empty(3, dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """
        Test whether the passed state vector is valid or not
        """
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == 3:
                valid = True
        return valid

    def __residual_Jacobian(self, u, dt=None, sparse=True):
        """
        Evaluate the Jacobian of the residual term at state `u` and time `t` with a step
        size `dt`

        :param u: current state
        :param float dt: temporal stepsize. If `None`, default step size is loaded from
            model configurations
        :param bool sparse: if `True` return a sparse array (csc format used), otherwise
            return numpy array instance
        :returns: sparse (or dense based on`sparse`) matrix holding the Jacobian of the
            residual term
        """
        u = np.asarray(u).flatten()
        assert (
            u.size == 3
        ), f"Size mismatch! The passed state `u` has size {u.size} but expected 3."

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        assert dt > 0, "dt must be positive"

        sigma = self.sigma
        beta = self.beta
        rho = self.rho

        # Jacobian matrix
        jac = sp.lil_matrix(
            (3, 3), dtype=np.double
        )  # it doesn't really need to be sparse

        jac[0, 0] = 1 + dt * sigma
        jac[0, 1] = -sigma * dt

        jac[1, 0] = -rho * dt + u[2] * dt
        jac[1, 1] = 1 + dt
        jac[1, 2] = u[0] * dt

        jac[2, 0] = -u[1] * dt
        jac[2, 1] = -u[0] * dt
        jac[2, 2] = 1 + beta * dt

        if not sparse:
            jac = jac.toarray()
        else:
            jac = jac.tocsc()
        return jac

    def __implicit_Euler_step(
        self, state, dt, maxiter=200, tol=1e-10, scr_out_iter=5, sparse=False
    ):
        """
        Integrate the model one step (of size `dt`) using implicit Euler method.  The
        resulting linear system is solved using Newton Raphson, with maximum iterations
        set to `maxiter`

        :param state: the initial condition to propagate forward.
        :param float dt: the step size
        :param int maxiter: maximum number of iterations of the Newton Raphson solver
        :param float tol: tolerance of the Newton Raphson solver; here taken as the norm
            of the update
        :param int scr_out_iter: frequency of screen output of the Newton-Raphson
            solver; you can set to negative value or 0 to turn it screen output off.

        :returns: the result of applying implicit Euler once to `state` with time
            stepsize `dt`.  result is a one dimensional numpy array of the same size as
            `state`.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        # Solve the linear system using Newton-Raphson algorithm
        sigma = self.sigma
        beta = self.beta
        rho = self.rho

        if scr_out_iter == 0:
            scr_out_iter = maxiter + 1

        rhs = np.zeros(3)  # right-hand side
        # Newton-Raphson
        converged = False
        for itr in range(maxiter):
            # Calculate the right-hand side (the residual)
            rhs = u_out - u_in
            rhs -= (
                np.array(
                    [
                        sigma * (u_out[1] - u_out[0]),
                        u_out[0] * (rho - u_out[2]) - u_out[1],
                        u_out[0] * u_out[1] - beta * u_out[2],
                    ]
                )
                * dt
            )

            # Evaluate the Jacobian at the current iteration
            jac = self.__residual_Jacobian(u=u_out, dt=dt, sparse=sparse)

            # Solve for state update (du = - J^{-1} * residual)
            if sparse:
                du = sp.linalg.spsolve(jac, rhs)
            else:
                du = np.linalg.solve(jac, rhs)

            # Update state; u = u - du
            u_out -= du

            du_tol = np.linalg.norm(du)
            if du_tol < tol:
                converged = True
                break

            if itr > 0 and (itr) % scr_out_iter == 0:
                print("itr: {0:04d} \t|du|={1:12.10g} ".format(itr + 1, du_tol))
        if not converged:
            raise ValueError(  # TODO: decide either to raise error or pass
                f"Failed to converge in {itr} iterations!"
            )

        # save convergence info
        stat = dict(converged=converged, num_iter=itr, tol=du_tol)
        return u_out, stat

    def integrate_state(
        self, state, tspan, checkpoints=None, maxiter=200, tol=1e-10, verbose=False
    ):
        """
        Simulate/integrate the mdoel starting from the initial `state` over the passed
        `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time
            integration window
        :param checkpoints: times at which to store the computed solution, must be
            sorted and lie within tspan. If None (default), use points selected by the
            solver [t0, t1, ..., tf].
        :param int maxiter: maximum number of iterations of the Newton Raphson solver
        :param float tol: tolerance of the Newton Raphson solver; here taken as the norm
            of the update
        :param bool verbose: output progress to screen if `True`.  If set to `False`
            `scr_out_iter` is discarede, i.e., nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of
            `checkpoints` starting from `checkpoints[0]` and ending at
            `checkpoints[-1]`.

        :raises: AssertionError if `tspan` is not valid, or checkpoints are not within
            `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError
        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            raise TypeError(
                "Couldn't cast the passed state into a state vector; check type/size!"
            )

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                raise ValueError(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self._MODEL_T_EPS:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state,
                        dt=dt,
                        maxiter=maxiter,
                        tol=tol,
                        scr_out_iter=verbose,
                    )
                    flt_time = np.round(flt_time + dt, prec)

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state,
                        dt=t - flt_time,
                        maxiter=maxiter,
                        tol=tol,
                        scr_out_iter=verbose,
                    )
                    flt_time = t  # just for consistency

                else:
                    raise ValueError(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )

                # Update trajectory
                traject.append(flt_state.copy())
                # print("DEBUG: Updating at time {0}".format(flt_time))

        return checkpoints, traject

    def Jacobian_T_matvec(self, state, eval_at_t, eval_at, dt=None, sparse=False):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """

        # TODO: After implementing this function, migrate to Cython for fast integration/solution
        nx = self.model_grid.size
        assert (
            state.size == nx
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx
        )

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()

        JT_u = u_in.copy()
        # Jacobian matrix
        A = self.__residual_Jacobian(u=eval_at, dt=dt, sparse=sparse)
        # solve the system of linear equations
        if sparse:
            JT_u = sp.linalg.spsolve(A.T, u_in)
        else:
            JT_u = np.linalg.solve(A.T, u_in)

        return JT_u

    def get_model_grid(self):
        """return a copy of the model grid"""
        return np.arange(3)

    @property
    def sigma(self):
        return self.configurations.sigma

    @property
    def rho(self):
        return self.configurations.rho

    @property
    def beta(self):
        return self.configurations.beta

    @property
    def ic(self):
        return self.configurations.ic

    @property
    def dt(self):
        return self.configurations.dt

    @property
    def t_eps(self):
        return self.configurations.t_eps


@dataclass(kw_only=True, slots=True)
class Lorenz96Configs(TimeDependentModelConfigs):
    """
    Configurations for the Chaotic Lorenz-96 model

     .. math::
       \\frac{d x_i}{d t} = (x_{i+1} - x_{i-2})x_{i-1} - x_{i} + F ,\\, i=1,2,\\dots, n,\\, t\\in (0, t_f]

    This model is a simplified model that describes an arbitrary atmospheric quantity
    as it evolves on a circular array of sites, undergoing forcing, dissipation,
    and rotation invariant advection.
    """

    model_name: str = "Lorenz-96 Model"
    nx: int = 40
    F: float = 8.0
    dt: float = 0.01
    t_eps: float = 1e-8


@set_configurations(Lorenz96Configs)
class Lorenz96(TimeDependentModel):
    """
    Implementations of the Chaotic Lorenz-96 model

     .. math::
       \\frac{d x_i}{d t} = (x_{i+1} - x_{i-2})x_{i-1} - x_{i} + F ,\\, i=1,2,\\dots, n,\\, t\\in (0, t_f]

    This model is a simplified model that describes an arbitrary atmospheric quantity
    as it evolves on a circular array of sites, undergoing forcing, dissipation,
    and rotation invariant advection.

    Time integration is carried out using a simple implicit Euler method, employing
    the Newton-Raphson iterations for which the Jacobian of the resiual term is required.

    :param configs: Configurations of the model, either as an instance of
        Lorenz96Configs, a dictionary, or None. The default is None, which uses the
        default configurations.
    """

    def __init__(self, configs: Lorenz96Configs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | Lorenz96Configs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`Lorenz96Configs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`Lorenz96Configs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self, configs=configs, configs_class=self.configurations_class
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and x == float(x)
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nx",
            test=is_positive_integer,
            message="nx must be a positive integer!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="F",
            test=is_float,
            message="F must be a float!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Check parent for further validation (if any)
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid)

    def create_initial_condition(self):
        """
        Create the initial condition for the Lorenz 96 model.
        Here, it is obtained by adding a small perturbation to the equilibrium conditions
        and apply time integration for a few time steps and consider the last state as initial condition

        :returns: 1D numpy array holding initial condition values.
        """
        F = self.F
        u_out = self.state_vector()
        u_out[:] = F
        u_out[19] += 0.01
        _, traject = self.integrate_state(
            u_out,
            tspan=[0, 5],
            checkpoints=[0, 5],
            maxiter=200,
            tol=1e-10,
            verbose=False,
        )
        u_out[:] = traject[-1]
        return u_out

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector.
        Here, this is a 1D Numpy array with n entries corresponding to the Lorenz 96 variables.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        n = self.nx
        state = np.empty(n, dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self.nx:
                valid = True
        return valid

    def integrate_state(
        self, state, tspan, checkpoints=None, maxiter=200, tol=1e-10, verbose=False
    ):
        """
        Simulate/integrate the mdoel starting from the initial `state` over the passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time integration window
        :param checkpoints: times at which to store the computed solution, must be sorted and
            lie within tspan. If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param int maxiter: maximum number of iterations of the Newton Raphson solver
        :param float tol: tolerance of the Newton Raphson solver; here taken as the norm of the update
        :param bool verbose: output progress to screen if `True`.

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of `checkpoints`
            starting from `checkpoints[0]` and ending at `checkpoints[-1]`.

        :raises: AssertionError if `tspan` is not valid, or checkpoints are not within `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError
        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self._MODEL_T_EPS:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state,
                        dt=dt,
                        maxiter=maxiter,
                        tol=tol,
                        scr_out_iter=verbose,
                    )
                    flt_time = np.round(flt_time + dt, prec)

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state, _ = self.__implicit_Euler_step(
                        state=flt_state,
                        dt=t - flt_time,
                        maxiter=maxiter,
                        tol=tol,
                        scr_out_iter=verbose,
                    )
                    flt_time = t  # just for consistency

                else:
                    print(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )
                    raise ValueError

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def __implicit_Euler_step(self, state, dt, maxiter=200, tol=1e-10, scr_out_iter=5):
        """
        Integrate the model one step (of size `dt`) using implicit Euler method.
        The resulting linear system is solved using Newton Raphson, with maximum iterations set to `maxiter`

        :param state: the initial condition to propagate forward.
        :param float dt: the step size
        :param int maxiter: maximum number of iterations of the Newton Raphson solver
        :param float tol: tolerance of the Newton Raphson solver; here taken as the norm of the update
        :param int scr_out_iter: frequency of screen output of the Newton-Raphson solver;
            you can set to negative value or 0 to turn it screen output off.

        :returns: the result of applying implicit Euler once to `state` with time stepsize `dt`.
            result is a one dimensional numpy array of the same size as `state`.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        # Solve the linear system using Newton-Raphson algorithm
        n, F = self.nx, self.F

        rhs = np.zeros(n)  # right-hand side

        # Difference operators (sparse matrices) # TODO: Could be sped up -> Matrix-Free
        Ax = sp.lil_matrix((n, n), dtype=np.double)
        Ax.setdiag(1, k=-1)
        Ax[0, :] = 0.0
        Ax[1, :] = 0.0
        Ax[-1, :] = 0.0
        Ax = Ax.tocsc()

        Ax2 = sp.lil_matrix((n, n), dtype=np.double)
        Ax2.setdiag(1, k=1)
        Ax2.setdiag(-1, k=-2)
        Ax2[0, :] = 0.0
        Ax2[1, :] = 0.0
        Ax2[-1, :] = 0.0
        Ax2 = Ax2.tocsc()

        if scr_out_iter == 0:
            scr_out_iter = maxiter + 1

        # ii = np.arange(2,n-1)
        # Newton-Raphson
        converged = False
        for itr in range(maxiter):
            # Calculate the right-hand side (the residual)
            rhs = u_out - u_in
            rhs += u_out * dt
            rhs -= Ax2.dot(u_out) * Ax.dot(u_out) * dt

            rhs[0] -= (u_out[1] - u_out[n - 2]) * u_out[n - 1] * dt
            rhs[1] -= (u_out[2] - u_out[n - 1]) * u_out[0] * dt
            rhs[n - 1] -= (u_out[0] - u_out[n - 3]) * u_out[n - 2] * dt

            rhs -= F * dt

            # Evaluate the Jacobian at the current iteration
            jac = self.__residual_Jacobian(u=u_out, dt=dt, sparse=True)

            # Solve for state update (du = - J^{-1} * residual)
            du = sp.linalg.spsolve(jac, rhs)

            # Update state; u = u - du
            u_out -= du

            du_tol = np.linalg.norm(du)
            if du_tol < tol:
                converged = True
                break

            if itr > 0 and (itr) % scr_out_iter == 0:
                print("itr: {0:04d} \t|du|={1:12.10g} ".format(itr + 1, du_tol))

        if not converged:
            print("Failed to converge in {0} iterations!".format(itr))
            print(rhs)
            print(du_tol)
            raise ValueError  # TODO: decide either to raise error or pass

        # save convergence info
        stat = dict(converged=converged, num_iter=itr, tol=du_tol)
        return u_out, stat

    def Jacobian_T_matvec(self, state, eval_at_t, eval_at, dt=None, sparse=True):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """

        # TODO: After implementing this function, migrate to Cython for fast integration/solution
        nx = self.model_grid.size
        assert (
            state.size == nx
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx
        )

        model_dt = self.configurations.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()

        JT_u = u_in.copy()
        # Jacobian matrix
        A = self.__residual_Jacobian(u=eval_at, dt=dt, sparse=sparse)
        # solve the system of linear equations
        JT_u = sp.linalg.spsolve(A.T, u_in)

        return JT_u

    def __residual_Jacobian(self, u, dt=None, sparse=True):
        """
        Evaluate the Jacobian of the residual term at state `u` and time `t` with a step size `dt`

        :param u: current state
        :param float dt: temporal stepsize. If `None`, default step size is loaded from model configurations
        :param bool sparse: if `True` return a sparse array (csc format used), otherwise return numpy array instance

        :returns: sparse (or dense based on`sparse`) matrix holding the Jacobian of the residual term
        """
        u = np.asarray(u).flatten()
        n = self.nx
        assert (
            u.size == n
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            u.size, n
        )

        model_dt = self.configurations.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Jacobian matrix
        jac = sp.lil_matrix((n, n), dtype=np.double)

        jac[0, 0] = 1 + dt
        jac[0, 1] = -u[n - 1] * dt
        jac[0, n - 2] = u[n - 1] * dt
        jac[0, n - 1] = -(u[1] - u[n - 2]) * dt

        jac[1, 0] = -(u[2] - u[n - 1]) * dt
        jac[1, 1] = 1 + dt
        jac[1, 2] = -u[0] * dt
        jac[1, n - 1] = u[0] * dt

        for i in range(2, n - 1):
            jac[i, i - 2] = u[i - 1] * dt
            jac[i, i - 1] = -(u[i + 1] - u[i - 2]) * dt
            jac[i, i] = 1 + dt
            jac[i, i + 1] = -u[i - 1] * dt

        jac[n - 1, 0] = -u[n - 2] * dt
        jac[n - 1, n - 3] = u[n - 2] * dt
        jac[n - 1, n - 2] = -(u[0] - u[n - 3]) * dt
        jac[n - 1, n - 1] = 1 + dt

        if not sparse:
            jac = jac.toarray()
        else:
            jac = jac.tocsc()
        return jac

    def get_model_grid(self):
        """return a copy of the model grid"""
        n = self.nx
        return np.arange(n)

    @property
    def nx(self):
        return self.configurations.nx

    @property
    def F(self):
        return self.configurations.F

    @property
    def dt(self):
        return self.configurations.dt

    @property
    def t_eps(self):
        return self.configurations.t_eps


def create_Lorenz63_model(
    sigma=10.0,
    rho=28.0,
    beta=8.0 / 3.0,
    ic=(1, 1, 1),
    dt=0.001,
    output_dir=SETTINGS.OUTPUT_DIR,
):
    """
    A simple interface to creating an instance of `Lorenz63`.

    :returns: an instance of :py:class:`Lorenz63`
    """
    configs = dict(
        sigma=sigma,
        rho=rho,
        beta=beta,
        ic=ic,
        dt=dt,
        output=dict(output_dir=output_dir),
    )
    model = Lorenz63(configs=configs)
    return model


def create_Lorenz96_model(nx=40, F=8, dt=0.01, output_dir=SETTINGS.OUTPUT_DIR):
    """
    A simple interface to creating an instance of `Lorenz96`.

    :param int nx: the dimension (number of variables)
    :param float F: forcing term
    :param float dt: *default* time integration step size
    :param output_dir: Path to folder (will be created if doesn't exist)
        in which to write reports, output, etc.

    :returns: an instance of :py:class:`Lorenz96`
    """
    configs = dict(nx=nx, F=F, dt=dt, output=dict(output_dir=output_dir))
    model = Lorenz96(configs=configs)
    return model


