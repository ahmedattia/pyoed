# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
A module that provides implementation(s) of Poisson Equations model(s)
"""
from dataclasses import dataclass, field
import numpy as np
import scipy.sparse as sp

from pyoed.models.core import (
    TimeDependentModel,
    TimeDependentModelConfigs,
)
from pyoed.configs import (
    validate_key,
    aggregate_configurations,
    set_configurations,
    SETTINGS,
)
from pyoed import utility


@dataclass(kw_only=True, slots=True)
class AdvectionDiffusion1DConfigs(TimeDependentModelConfigs):
    """
    Configuration class for the 1D Advection-Diffusion model.

    .. math::
       \\frac{\\partial u}{\\partial t} + c \\frac{\\partial u}{\\partial x}
        = \\nu \\frac{\\partial^2 u}{\\partial^2 x},\\, x\\in[-L, L],\\, t\\in (0, t_f]  \\\\

    This model is similar to the 1D Burgers problem, but with linear advection term.  We
    assume Dirichlet homogeneous boundary conditions: :math:`u(0, t) = u(L, t) = 0,\\, t
    \\in (0, t_f]`.  For initial conditions, we use the smooth function:  :math:`u(x, 0)
    = - \\sin(\\pi x)`.

    :param domain: boundary of the spatial domain. Default is (-1, 1)
    :param nu: kinematic viscosity -- diffusion coefficient
    :param c: constant advection velocity
    :param nx: number of spatial discretization points (of the domain)
    :param dt: *default* time integration step size
    :param t_eps: tolerance for time-step comparison
    """

    model_name: str = "AdvectionDiffusion-1D"
    domain: tuple[float, float] = (-1, 1)
    nu: float = 0.1
    c: float = 0.1
    nx: int = 101
    dt: float = 0.01
    t_eps: float = 1e-6


@set_configurations(AdvectionDiffusion1DConfigs)
class AdvectionDiffusion1D(TimeDependentModel):
    """
    Implementations of the one-dimensional advection-diffusion model:

    .. math::
       \\frac{\\partial u}{\\partial t} + c \\frac{\\partial u}{\\partial x}
        = \\nu \\frac{\\partial^2 u}{\\partial^2 x},\\, x\\in[-L, L],\\, t\\in (0, t_f]  \\\\

    This model is similar to the 1D Burgers problem, but with linear advection term.
    We assume Dirichlet homogeneous boundary conditions: :math:`u(0, t) = u(L, t) = 0,\\, t \\in (0, t_f]`.
    For initial conditions, we use the smooth function:  :math:`u(x, 0) = - \\sin(\\pi x)`.

    Spatial grid is equally-spaced based on the configurations passed upon initialization.
    We are using finite differences for spatial discretization.
    Time integration is carried out using a simple implicit Euler method,
    which requires the solution of a (tridiagonal) linear system.

    :param configs: an object containing configurations of the one-dimensional
        advection-diffusion model.
    """

    def __init__(self, configs: AdvectionDiffusion1DConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        self._MODEL_GRID = np.linspace(configs.domain[0], configs.domain[1], configs.nx)
        self._INITIAL_CONDITION = self.create_initial_condition()
        self.__ImpEulerMat = self.__implicit_Euler_matrix(sparse=True)

    def validate_configurations(
        self,
        configs: dict | AdvectionDiffusion1DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`AdvectionDiffusion1DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`AdvectionDiffusion1DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="domain",
            test=lambda x: (len(x) == 2) and (x[0] < x[1]),
            message="Domain must have two entries with the first entry less than the second",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nu",
            test=is_positive_float,
            message="Kinematic viscosity must be a positive float",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="c",
            test=is_positive_float,
            message="Advection velocity must be a positive float",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nx",
            test=is_positive_integer,
            message="Number of spatial grid points must be a positive integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_initial_condition(self):
        """
        Create the initial condition associated with the passed model grid points
        We are assuming ... initial condition defined as :math:`-\\sin(\\pi x)`
        where :math:`x` is a model grid point.
        :returns: numpy array (same shape as model_grid associated with the model) holding initial condition values.
        """
        # TODO: consider adding argument to enable multiple choices of the initial condition for flexibility
        fw = lambda x: -np.sin(np.pi * x)
        u_out = self.state_vector()
        u_out[:] = fw(self._MODEL_GRID)
        u_out[0] = 0.0
        u_out[-1] = 0.0
        return u_out

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector. Here, this is a 1D Numpy array of size equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        state = np.empty(self._MODEL_GRID.size, dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self._MODEL_GRID.size:
                valid = True
        return valid

    def integrate_state(self, state, tspan, checkpoints=None, verbose=False):
        """
        Simulate/integrate the model starting from the initial `state` over the
        passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of
            the time integration window
        :param checkpoints: times at which to store the computed solution,
            must be sorted and lie within tspan.
            If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param bool verbose: output progress to screen if `True`.
            If set to `False`, nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the
            model trajectory with entries corresponding to the simulated
            model state at entries of `checkpoints` starting from `checkpoints[0]`
            and ending at `checkpoints[-1]`.

        :raises: `AssertionError ` if `tspan` is not valid, or checkpoints are
            not within `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            raise AssertionError("tspan must be an iterable with two intries (t0, tf)")
        if t0 >= tf:
            raise ValueError("The timespan 'tspan' must be (t0, tf) with t0 < tf")

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self.t_eps:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state = self.__implicit_Euler_step(state=flt_state, dt=dt)
                    flt_time = np.round(flt_time + dt, prec)

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state = self.__implicit_Euler_step(
                        state=flt_state, dt=t - flt_time
                    )
                    flt_time = t  # just for consistency

                else:
                    raise ValueError(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def __implicit_Euler_step(self, state=None, dt=None, sparse=True):
        """
        Integrate the model one step (of size `dt`) using implicit Euler method.
        The resulting tridiagonal linear system can be solved directly and efficiently

        :param state: the initial condition to propagate forward.
            Each entry of `state` corresponds to the velocity
            :math:`u` at the corresponding entry in the model grid
        :param float dt: the step size

        :returns: the result of applying implicit Euler once to `state` with time
            stepsize `dt`.
            The result is a one dimensional numpy array of the same size as `state`.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: Consider migration to Cython for fast integration/solution
        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        # linear system matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        if abs(dt - self.dt) < self.t_eps:
            # print("DEBUG: using predefined Euler matrix; dt={0}".format(dt))
            A = self.__ImpEulerMat
        else:
            # print("DEBUG: Updating Euler matrix; dt={0}".format(dt))
            A = self.__implicit_Euler_matrix(
                dt, sparse=sparse
            )  # computed once, not updated since it's independent of the state

        # solve the tridiagonal system of linear equations
        u_out[1:-1] = sp.linalg.spsolve(
            A[1:-1, 1:-1], u_in[1:-1]
        )  # TODO: this only works for zero BCs -- otherwise, first and last entries of u_in[1:-1] should be changed
        return u_out

    def __implicit_Euler_matrix(self, dt=None, sparse=True):
        """
        Compute the forward operator matrix A defined by :math:`A u(t_{n+1}) = u(t_{n})

        :param float dt: the step size

        :returns: the resulting matrix of applying implicit Euler.
        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution
        nx = self._MODEL_GRID.size
        dx = self._MODEL_GRID[1] - self._MODEL_GRID[0]
        nu, c = self.nu, self.c

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        # Linear system matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        A = sp.lil_matrix((nx, nx), dtype=np.double)

        # Loop over all inner rows
        for i in range(1, nx - 1):
            A[i, i - 1] = -dt * c / (2 * dx) - nu * dt / (dx**2)
            A[i, i] = 1 + 2 * nu * dt / (dx**2)
            A[i, i + 1] = dt * c / (2 * dx) - nu * dt / (dx**2)

        if not sparse:
            A = A.toarray()
        else:
            A = A.tocsc()  # csc is more efficient for linear solves and inversion
        return A

    def Jacobian_T_matvec(self, state, eval_at_t=None, eval_at=None, dt=None, sparse=True):
        """
        Evaluate the product of the Jacobian (of the right-hand-side) of AD
        model (the TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
            (ignored for linear models as here)
        :param float dt: the step size
        :param bool sparse: sparse/dense structure for the forward model matrix

        :returns: product of the Jacobian transposed (adjoint operator) by a model state.
        """
        # TODO: Consider migration to Cython for fast integration/solution

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        if dt is None:
            dt = self.dt

        # Create local copy of the state; use to initialize solution
        u_in = state[:].copy()
        JT_u = self.state_vector()  # u_in.copy()

        # linear system matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        if abs(dt - self.dt) < self.t_eps:
            A = self.__ImpEulerMat
        else:
            A = self.__implicit_Euler_matrix(dt=dt, sparse=sparse)

        # solve the tridiagonal system of linear equations
        JT_u[1:-1] = sp.linalg.spsolve(A[1:-1, 1:-1].T, u_in[1:-1])

        # Enforce Dirichlet BCs  # TODO: Consider other boundary conditions
        JT_u[0] = 0.0
        JT_u[-1] = 0.0

        return JT_u

    def exact_solution(self, t):
        """Exact solution for verification -- note that it only works for a limited range of c and nu"""

        x = self._MODEL_GRID
        nu, c = self.nu, self.c

        sm1, sm2 = 0, 0
        for p in range(150):
            sm1 += (
                (-1) ** p
                * 2
                * p
                * np.sin(p * np.pi * x)
                * np.exp(-nu * p**2 * np.pi**2 * t)
            ) / (
                c**4
                + 8 * (c * np.pi * nu) ** 2 * (p**2 + 1)
                + 16 * (np.pi * nu) ** 4 * (p**2 - 1) ** 2
            )
            sm2 += (
                (-1) ** p
                * (2 * p + 1)
                * np.cos((2 * p + 1) * np.pi * x / 2)
                * np.exp(-nu * (2 * p + 1) ** 2 * np.pi**2 * t / 4)
            ) / (
                c**4
                + (c * np.pi * nu) ** 2 * (8 * p**2 + 8 * p + 10)
                + (np.pi * nu) ** 4 * (4 * p**2 + 4 * p - 3) ** 2
            )

        true_state = self.state_vector()
        true_state[:] = (
            16
            * (np.pi**2)
            * (nu**3)
            * c
            * np.exp(c / (2 * nu) * (x - c * t / 2))
            * (np.sinh(c / (2 * nu)) * sm1 + np.cosh(c / (2 * nu)) * sm2)
        )
        return true_state

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def domain(self):
        """retrieve the model domain"""
        return self.configurations.domain

    @property
    def nu(self):
        """retrieve the model kinematic viscosity"""
        return self.configurations.nu

    @property
    def c(self):
        """retrieve the model advection velocity"""
        return self.configurations.c

    @property
    def nx(self):
        """retrieve the model spatial discretization points"""
        return self.configurations.nx

    @property
    def dt(self):
        """retrieve the model time step size"""
        return self.configurations.dt

    @property
    def t_eps(self):
        """retrieve the model time epsilon tolerance"""
        return self.configurations.t_eps


@dataclass(kw_only=True, slots=True)
class AdvectionDiffusion2DConfigs(TimeDependentModelConfigs):
    """
    Configuration class for the 2D Advection-Diffusion model.

    .. math::
       \\frac{\\partial u}{\\partial t} + c_x \\frac{\\partial u}{\\partial x} + c_y \\frac{\\partial u}{\\partial y}
        = \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ),\\, x\\in[0, L_x],\\, y\\in[0, L_y],\\, t\\in (0, t_f]

    We assume a time-dependent non-zero Dirichlet boundary conditions, extracted from
    the following exact solution:

    .. math::
       u(x,y,t) = \\frac{1}{1+4t}\\exp\\left\\{-\\frac{(x-0.25-c_xt)^2+(y-0.25-c_yt)^2}{\\nu(1+4t)}\\right\\}

    Initial conditions are defined as :math:`u(x,y,0) =
    \\exp\\left\\{-\\frac{(x-x_0)^2+(y-y_0)^2}{\\nu}\\right\\}`.

    :param domain: boundary of the spatial domain. Default is ((0,1),(0,1))
    :param nu: kinematic viscosity -- diffusion coefficient
    :param cx: constant advection velocity in the x-direction
    :param cy: constant advection velocity in the y-direction
    :param nx: number of spatial discretization points (of the domain) in the
        x-direction
    :param ny: number of spatial discretization points (of the domain) in the
        y-direction
    :param dt: *default* time integration step size
    :param t_eps: tolerance for time-step comparison (default 1e-6)
    """

    model_name: str = "AdvectionDiffusion-2D"
    domain: tuple[tuple[float, float], tuple[float, float]] = ((0, 1), (0, 1))
    nu: float = 0.01
    cx: float = 0.5
    cy: float = 0.5
    nx: int = 101
    ny: int = 101
    dt: float = 0.001
    t_eps: float = 1e-6


@set_configurations(AdvectionDiffusion2DConfigs)
class AdvectionDiffusion2D(TimeDependentModel):
    """
    Implementations of the two-dimensional advection-diffusion model:

    .. math::
       \\frac{\\partial u}{\\partial t} + c_x \\frac{\\partial u}{\\partial x} + c_y \\frac{\\partial u}{\\partial y}
        = \\nu (\\frac{\\partial^2 u}{\\partial^2 x} + \\frac{\\partial^2 u}{\\partial^2 y} ),\\, x\\in[0, L_x],\\, y\\in[0, L_y],\\, t\\in (0, t_f]

    We assume a time-dependent non-zero Dirichlet boundary conditions, extracted from
    the following exact solution:

    .. math::
       u(x,y,t) = \\frac{1}{1+4t}\\exp\\left\\{-\\frac{(x-0.25-c_xt)^2+(y-0.25-c_yt)^2}{\\nu(1+4t)}\\right\\}

    Initial conditions are defined as :math:`u(x,y,0) =
    \\exp\\left\\{-\\frac{(x-x_0)^2+(y-y_0)^2}{\\nu}\\right\\}`.

    Spatial grid is equally-spaced based on the configurations passed upon
    initialization. We are using finite differences for spatial discretization. Time
    integration is carried out using an explicit fourth-order Runge-Kutta method, which
    requires some attention to ensure stability.

    :param configs: an object containing configurations of the two-dimensional
        advection-diffusion model.
    """

    def __init__(self, configs: AdvectionDiffusion2DConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        _x = np.linspace(self.domain[0][0], self.domain[0][1], self.nx)
        _y = np.linspace(self.domain[1][0], self.domain[1][1], self.ny)
        [xx, yy] = np.meshgrid(_x, _y)
        self._MODEL_GRID = np.hstack([xx.reshape(-1, 1), yy.reshape(-1, 1)])

        self.__INITIAL_CONDITION = self.create_initial_condition()
        self.__Jrhs = self.__JrhsCompute(sparse=True)
        self.__Jac = self.__explicit_RungeKutta4_jacobian()

    def validate_configurations(
        self,
        configs: dict | AdvectionDiffusion2DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`AdvectionDiffusion2DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`AdvectionDiffusion2DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="domain",
            test=lambda x: (len(x) == 2) and all([x[i][0] < x[i][1] for i in range(2)]),
            message="Domain must have two entries with the first entry less than the second",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="nu",
            test=is_positive_float,
            message="Kinematic viscosity must be a positive float",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=dim_param,
                    test=is_positive_integer,
                    message=f"{dim_param} must be a positive integer!",
                    raise_for_invalid=raise_for_invalid,
                )
                for dim_param in ["nx", "ny"]
            ]
        ):
            return False

        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=dim_param,
                    test=is_positive_float,
                    message=f"{dim_param} must be a positive float!",
                    raise_for_invalid=raise_for_invalid,
                )
                for dim_param in ["cx", "cy"]
            ]
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def create_initial_condition(self):
        """
        Create the initial condition associated with the passed model grid points
        We are assuming ... initial condition defined as :math:`u(x,y,0) = \\exp\\bigg\\{-\\frac{(x-x_0)^2+(y-y_0)^2}{\\nu}\\bigg\\}`
        where :math:`x, y` define the model grid coordinates.
        :returns: numpy array holding initial condition values.
        """
        # TODO: consider adding argument to enable multiple choices of the initial
        # condition for flexibility
        nu = self.nu

        fw = lambda x, y: np.exp(-((x - 0.25) ** 2 + (y - 0.25) ** 2) / nu)
        u_out = self.state_vector()
        u_out[:] = fw(self._MODEL_GRID[:, 0], self._MODEL_GRID[:, 1])
        return u_out

    def set_boundary_condition(self, state, t):
        """
        Set the boundary conditions, defined using the followin relation:

        .. math::
           u(x,y,t) = \\frac{1}{1+4t}\\exp\\bigg\\{-\\frac{(x-0.25-c_xt)^2+(y-0.25-c_yt)^2}{\\nu(1+4t)}\\bigg\\}.

        :returns: numpy array (same shape as model_grid associated with the model) with set boundary condition values.
        """
        nu, cx, cy = self.nu, self.cx, self.cy
        nx, ny = self.nx, self.ny

        fw = (
            lambda x, y: 1
            / (1 + 4 * t)
            * np.exp(
                -((x - 0.25 - cx * t) ** 2 + (y - 0.25 - cy * t) ** 2)
                / (nu * (1 + 4 * t))
            )
        )

        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        for i in [
            0,
            nx - 1,
        ]:  # left and right boundaries (x=0 for i=0, and x=lx for i=nx-1)
            _ind = [i + j * nx for j in range(ny)]
            u_out[_ind] = fw(self._MODEL_GRID[_ind, 0], self._MODEL_GRID[_ind, 1])

        for j in [
            0,
            ny - 1,
        ]:  # top and bottom boundaries (y=0 for j=0, and y=ly for j=ny-1)
            _ind = [i + j * nx for i in range(nx)]
            u_out[_ind] = fw(self._MODEL_GRID[_ind, 0], self._MODEL_GRID[_ind, 1])

        return u_out

    def state_vector(self, init_val=0):
        """
        Create an instance of model state vector. Here, this is a 1D Numpy array of size equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1D numpy array
        """
        state = np.empty(self._MODEL_GRID.shape[0], dtype=np.double)
        state[:] = init_val
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.ndim == 1 and state.size == self._MODEL_GRID.shape[0]:
                valid = True
        return valid

    def __rhs(self, state):
        """
        Compute the the right-hand side (the residual :math:`\\frac{du_{i,j}}{dt}=r_{i,j}`) of the 2D advection-diffusion equation
        using second order centered finite difference discretization to approximate the spatial derivatives

        :param state: the state at which the right-hand side is evaluated. Each entry of `state` corresponds to
            the velocity :math:`u` at the corresponding entries in the model grid

        :returns: the residual r as a one dimensional numpy array of the same shape as `state`.

        """

        nx, ny = self.nx, self.ny
        nu, cx, cy = self.nu, self.cx, self.cy

        dx = self._MODEL_GRID[1, 0] - self._MODEL_GRID[0, 0]
        dy = self._MODEL_GRID[nx, 1] - self._MODEL_GRID[0, 0]

        u = self.state_vector()
        u[:] = state[:].copy()

        i = np.arange(1, nx - 1)  # internal points in x-direction
        j = np.arange(1, ny - 1)  # internal points in y-direction
        [_i, _j] = np.meshgrid(i, j)  # to mitigate nested loops
        _i, _j = _i.flatten(), _j.flatten()
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # east
        _indW = (_i - 1) + _j * nx  # west
        _indN = _i + (_j + 1) * nx  # north
        _indS = _i + (_j - 1) * nx  # south

        r = np.zeros(nx * ny, dtype=np.double)
        r[_ind] = (
            -cx * (u[_indE] - u[_indW]) / (2 * dx)
            - cy * (u[_indN] - u[_indS]) / (2 * dy)
            + nu * (u[_indE] - 2 * u[_ind] + u[_indW]) / (dx**2)
            + nu * (u[_indN] - 2 * u[_ind] + u[_indS]) / (dy**2)
        )
        return r

    def __JrhsCompute(self, state=None, sparse=True):
        """
        Compute the the Jacobian of the right-hand side for the 2D advection-diffusion
        equation using second order centered finite difference discretization to
        approximate the spatial derivatives

        :param state: the state at which the Jacobian of the right-hand side is
            evaluated. Each entry of `state` corresponds to the velocity :math:`u` at
            the corresponding entries in the model grid

        :returns: the Jacobian a two-dimensional numpy array.

        """

        nx, ny = self.nx, self.ny
        nu, cx, cy = self.nu, self.cx, self.cy

        dx = self._MODEL_GRID[1, 0] - self._MODEL_GRID[0, 0]
        dy = self._MODEL_GRID[nx, 1] - self._MODEL_GRID[0, 0]

        i = np.arange(1, nx - 1)  # internal points in x-direction
        j = np.arange(1, ny - 1)  # internal points in y-direction
        [_i, _j] = np.meshgrid(i, j)  # to mitigate nested loops
        _i, _j = _i.flatten(), _j.flatten()
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # east
        _indW = (_i - 1) + _j * nx  # west
        _indN = _i + (_j + 1) * nx  # north
        _indS = _i + (_j - 1) * nx  # south

        Jr = sp.lil_matrix((nx * ny, nx * ny), dtype=np.double)

        Jr[_ind, _ind] = nu * (-2) / (dx**2) + nu * (-2) / (dy**2)
        Jr[_ind, _indE] = -cx * (1) / (2 * dx) + nu * (1) / (dx**2)
        Jr[_ind, _indW] = -cx * (-1) / (2 * dx) + nu * (1) / (dx**2)
        Jr[_ind, _indN] = -cy * (1) / (2 * dy) + nu * (1) / (dy**2)
        Jr[_ind, _indS] = -cy * (-1) / (2 * dy) + nu * (1) / (dy**2)

        # fix boundaries
        _j = np.arange(ny)
        _i = 1
        _ind = _i + _j * nx
        _indW = (_i - 1) + _j * nx  # left boundary
        Jr[_ind, _indW] = 0

        _i = nx - 2
        _ind = _i + _j * nx
        _indE = (_i + 1) + _j * nx  # right boundary
        Jr[_ind, _indE] = 0

        _i = np.arange(nx)
        _j = 1
        _ind = _i + _j * nx
        _indS = _i + (_j - 1) * nx  # bottom boundary
        Jr[_ind, _indS] = 0

        _j = ny - 2
        _ind = _i + _j * nx
        _indN = _i + (_j + 1) * nx  # top boundary
        Jr[_ind, _indN] = 0

        if not sparse:
            Jr = Jr.toarray()
        else:
            Jr = Jr.tocsc()

        return Jr

    def __explicit_RungeKutta4_step(self, state, dt):
        """
        Integrate the model one step (of size `dt`) using the explicit 4th order Runge-Kutta method.

        :param state: the initial condition to propagate forward. Each entry of `state` corresponds to
            the velocity :math:`u` at the corresponding entries in the model grid
        :param float dt: the step size

        :returns: the result of applying explicit 4th order Runge-Kutta once to `state` with time stepsize `dt`.
            result is a one dimensional numpy array of the same shape as `state`.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: After implementing this function, migrate to Cython for fast
        # integration/solution
        nx, ny = self.nx, self.ny

        assert (
            state.size == nx * ny
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx * ny
        )

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        u_out = u_in.copy()

        kk = np.zeros(state.size, dtype=np.double)
        _c0 = np.array([0, 0.5, 0.5, 1])
        _c1 = np.array([1, 2, 2, 1]) * dt / 6.0

        for i in range(4):
            kk = self.__rhs(u_in + kk * dt * _c0[i])
            u_out += kk * _c1[i]

        return u_out

    def __explicit_RungeKutta4_jacobian(self, state=None, dt=None, sparse=True):
        """
        Evaluate the Jacobian matrix of the forward model (TLM) using the explicit 4th order Runge-Kutta method.

        :param state: the state at which the Jacobian is evaluated. Each entry of `state` corresponds to
            the velocity :math:`u` at the corresponding entries in the model grid
        :param float dt: the step size

        :returns: the Jacobian matrix employing the explicit 4th order Runge-Kutta.

        :raises: ValueError if failed to converge given passed settings
        """
        # TODO: After implementing this function, migrate to Cython for fast integration/solution

        nx, ny = self.nx, self.ny
        state_size = nx * ny

        model_dt = self.dt
        if dt is None:
            dt = model_dt
        else:
            prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])
            dt = np.round(dt, prec)
        #
        assert dt > 0, "dt must be positive"

        Jac = sp.identity(state_size, dtype=np.double, format="csc")
        I = sp.identity(state_size, dtype=np.double, format="csc")

        dkk = sp.csc_matrix((nx * ny, nx * ny), dtype=np.double)

        # TODO: check the loop implementation/math
        _c0 = np.array([0, 0.5, 0.5, 1])
        _c1 = np.array([1, 2, 2, 1]) * dt / 6

        for i in range(4):
            dkk = self.__Jrhs.dot(I + dkk * dt * _c0[i])
            Jac += dkk * _c1[i]

        Jac = Jac.tolil()
        # fix boundaries
        # left and right boundaries (x=0 for i=0, and x=lx for i=nx-1)
        j = np.arange(ny)
        i = 0
        _ind = i + j * nx
        Jac[_ind, :] = 0

        i = nx - 1
        _ind = i + j * nx
        Jac[_ind, :] = 0

        # top and bottom boundaries (y=0 for j=0, and y=ly for j=ny-1)
        i = np.arange(nx)
        j = 0
        _ind = i + j * nx
        Jac[_ind, :] = 0

        j = ny - 1
        _ind = i + j * nx
        Jac[_ind, :] = 0

        if not sparse:
            Jac = Jac.toarray()
        else:
            Jac = Jac.tocsc()

        return Jac

    def integrate_state(self, state, tspan, checkpoints=None, verbose=False):
        """
        Simulate/integrate the model starting from the initial `state` over the passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of the time integration window
        :param checkpoints: times at which to store the computed solution, must be sorted and
            lie within tspan. If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param bool verbose: output progress to screen if `True`.
            If set to `False`, nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the model trajectory
            with entries corresponding to the simulated model state at entries of `checkpoints`
            starting from `checkpoints[0]` and ending at `checkpoints[-1]`.

        :raises: AssertionError if `tspan` is not valid, or checkpoints are not within `tspan`
        """
        # precision for time integration checkpointing
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Validate tspan and checkpoings, state, and loop over __implicit_Euler_step
        try:
            t0, tf = tspan
            t0 = np.round(t0, prec)
            tf = np.round(tf, prec)
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError
        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Default timestep and checkpoints if not passed
        dt = self.dt

        if checkpoints is None:
            # No checkpoints passed; checkpoint at every timestep
            nt = int((tf - t0) // dt)
            checkpoints = np.arange(nt + 1) * dt + t0
            # checkpoints = np.linspace(t0, t0+nt*dt, nt+1)
            if tf > checkpoints[-1]:
                checkpoints = np.append(checkpoints, tf)
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf
        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.round(checkpoints, prec)
        checkpoints = np.unique(np.asarray(checkpoints).flatten())

        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector()
        flt_state[:] = state[:].copy()

        # Initialize the trajectory;
        traject = []

        #
        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t
                # Number of steps/iterations to apply the underlying operator/matrix/model
                nt = int((t - flt_time) // dt)

                # Propage state by repeated multiplication by the underlying model (nt times)
                # while (t-flt_time) > self.t_eps:
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state = self.__explicit_RungeKutta4_step(state=flt_state, dt=dt)
                    flt_time = np.round(flt_time + dt, prec)

                    # update boundary conditions
                    flt_state = self.set_boundary_condition(flt_state, flt_time)

                # Check if smaller time step is further needed!
                if abs(t - flt_time) < self.t_eps:
                    pass
                elif (t - flt_time) > self.t_eps:
                    flt_state = self.__explicit_RungeKutta4_step(
                        state=flt_state, dt=t - flt_time
                    )
                    flt_time = t  # just for consistency

                    # update boundary conditions
                    flt_state = self.set_boundary_condition(flt_state, flt_time)

                else:
                    print(
                        "Model is integrated beyond checkpionts; this shouldn't happen!"
                    )
                    raise ValueError

                # Update trajectory
                traject.append(flt_state.copy())

        return checkpoints, traject

    def Jacobian_T_matvec(
        self, state, eval_at_t=None, eval_at=None, dt=None, sparse=True
    ):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state.

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        """

        # TODO: After implementing this function, migrate to Cython for fast integration/solution

        nx, ny = self.nx, self.ny
        if dt is None:
            dt = self.dt

        assert (
            state.size == nx * ny
        ), "Size mismatch! The passed state `u` has size {0} while expected {1} ".format(
            state.size, nx * ny
        )

        # Create local copy of the state; use to initialize solution
        u_in = self.state_vector()
        u_in[:] = state[:].copy()
        # JT_u = u_in.copy()

        # linear system matrix (tridiagonal) # TODO: Could be sped up -> Matrix-Free
        if dt == self.dt:
            Jac = self.__Jac
        else:
            Jac = self.__explicit_RungeKutta4_jacobian(
                state=eval_at, dt=dt, sparse=sparse
            )  # computed once, not updated since it's independent of the state

        if not sparse:
            JT_u = np.dot(Jac.T, u_in)
        else:
            JT_u = (Jac.transpose()).dot(u_in)

        return JT_u

    def exact_solution(self, t):
        """Exact solution for verification"""

        x = self._MODEL_GRID[:, 0]
        y = self._MODEL_GRID[:, 1]
        nu, cx, cy = self.nu, self.cx, self.cy

        true_state = self.state_vector()
        true_state[:] = (
            1
            / (1 + 4 * t)
            * np.exp(
                -((x - 0.25 - cx * t) ** 2) / (nu * (1 + 4 * t))
                - (y - 0.25 - cy * t) ** 2 / (nu * (1 + 4 * t))
            )
        )
        return true_state

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()

    @property
    def domain(self):
        """retrieve the model domain"""
        return self.configurations.domain

    @property
    def nu(self):
        """retrieve the model kinematic viscosity"""
        return self.configurations.nu

    @property
    def cx(self):
        """retrieve the model advection velocity in the x-direction"""
        return self.configurations.cx

    @property
    def cy(self):
        """retrieve the model advection velocity in the y-direction"""
        return self.configurations.cy

    @property
    def nx(self):
        """retrieve the model spatial discretization points"""
        return self.configurations.nx

    @property
    def ny(self):
        """retrieve the model spatial discretization points"""
        return self.configurations.ny

    @property
    def dt(self):
        """retrieve the model time step size"""
        return self.configurations.dt

    @property
    def t_eps(self):
        """retrieve the model time epsilon tolerance"""
        return self.configurations.t_eps


class AdvectionDiffusion3D(TimeDependentModel):
    """
    Implementations of 3D advection diffusion model
    """

    def __init__(self, *args, **kwargs):
        raise NotImplementedError("TODO")


# ====================================================================== #
#               Simple interfaces to inistantiate AD models              #
# ====================================================================== #
def create_AD_1D_model(
    domain=(-1, 1), nu=0.05, c=0.75, nx=51, dt=0.01, output_dir=SETTINGS.OUTPUT_DIR
):
    """
    A simple interface to creating an instance of `AdvectionDiffusion1D`.

    :param domain: boundary of the spatial domain. Default is (-1, 1)
    :param float nu: kinematic viscosity -- diffusion coefficient
    :param float c: constant advection velocity
    :param int nx: number of spatial discretization points (of the domain)
    :param float dt: *default* time integration step size
    :output_dir: Path to folder (will be created if doesn't exist)
        in which to write reports, output, etc.

    :returns: an instance of :py:class:`AdvectionDiffusion1D`
    """
    configs = AdvectionDiffusion1DConfigs(
        domain=domain, nu=nu, c=c, dt=dt, output_dir=output_dir, nx=nx
    )
    model = AdvectionDiffusion1D(configs=configs)
    return model


def create_AD_2D_model(
    domain=((0, 1), (0, 1)),
    nu=0.01,
    cx=0.5,
    cy=0.75,
    nx=51,
    ny=51,
    dt=0.01,
    output_dir=SETTINGS.OUTPUT_DIR,
):
    """
    A simple interface to creating an instance of `AdvectionDiffusion2D`.

    :param domain: boundary of the spatial domain.
    :param float nu: kinematic viscosity -- diffusion coefficient
    :param float cx: constant advection velocity in the x-direction
    :param float cy: constant advection velocity in the y-direction
    :param int nx: number of spatial discretization points (of the domain)
        in the x-direction
    :param int ny: number of spatial discretization points in the y-direction
    :param float dt: *default* time integration step size
    :output_dir: Path to folder (will be created if doesn't exist) in which to write reports, output, etc.

    :returns: an instance of :py:class:`AdvectionDiffusion1D`
    """
    configs = AdvectionDiffusion2DConfigs(
        domain=domain,
        nu=nu,
        cx=cx,
        cy=cy,
        nx=nx,
        ny=ny,
        dt=dt,
        output_dir=output_dir,
    )
    model = AdvectionDiffusion2D(configs=configs)
    return model


def create_AD_3D_model(*args, **kwargs):
    """
    A simple interface to creating an instance of `AdvectionDiffusion3D`.
    """
    raise NotImplementedError("TODO")
