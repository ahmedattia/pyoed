from dataclasses import dataclass
from collections.abc import Callable
import numpy as np
from pyoed.models.core import (
    TimeIndependentModel,
    TimeIndependentModelConfigs,
)
from pyoed import utility
from pyoed.configs import (
    validate_key,
    aggregate_configurations,
    set_configurations,
)


@dataclass(kw_only=True, slots=True)
class TimeIndependentBlackBoxConfigs(TimeIndependentModelConfigs):
    """
    Configuration class for TimeIndependentBlackBox model. `solve_forward`, `N_p`, and
    `N_x` are required fields, whereas `Jacobian_T_matvec` is optional.

    :param solve_forward: A callable that takes a np.ndarray of size ``N_p`` and returns
        a np.ndarray of size ``N_x``.
    :param N_p: Number of parameters. Must be an int > 0.
    :param N_x: Number of state variables. Must be an int > 0.
    :param Jacobian_T_matvec: A callable that takes a np.ndarray of size ``N_x`` and a
        np.ndarray of size ``N_p`` and returns a np.ndarray of size ``N_p``. This is the
        Jacobian of the solve_forward function transposed, evaluated in the direction of
        the second argument. If this is not provided, then a finite-difference
        approximation is used (expensive!).
    """

    solve_forward: Callable[[np.ndarray], np.ndarray] | None = None
    N_p: int = 1
    N_x: int = 1
    Jacobian_T_matvec: Callable[[np.ndarray, np.ndarray], np.ndarray] | None = None

    def __post_init__(self):
        if self.Jacobian_T_matvec is None:
            self.Jacobian_T_matvec = (
                lambda state, eval_at: utility.finite_differences_gradient(
                    lambda x: np.inner(self.solve_forward(x), state), eval_at
                )
            )


@set_configurations(TimeIndependentBlackBoxConfigs)
class TimeIndependentBlackBox(TimeIndependentModel):
    """Wrapper simulation model for time independent models with some user provided
    black box forward model.

    :param TimeIndependentBlackBoxConfigs | dict | None configs: Configurations of the
        model, either as a TimeIndependentBlackBoxConfigs object, a dictionary, or None.
        The default is None, in which case the default configurations are used.
    """

    def __init__(self, configs: TimeIndependentBlackBoxConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | TimeIndependentBlackBoxConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`TimeIndependentBlackBoxConfigs` are validated.  Finally, super
            classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`TimeIndependentBlackBoxConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="N_p",
            test=is_positive_integer,
            message="Number of parameters must be a positive integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="N_x",
            test=is_positive_integer,
            message="Number of state variables must be a positive integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="solve_forward",
            test=callable,
            message="`solve_forward` must be a callable!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="Jacobian_T_matvec",
            test=callable,
            message="`Jacobian_T_matvec` must be a callable!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def state_vector(self, init_val=0, **kwargs):
        """
        Create an instance of model state vector

        :param float init_val: (optional) value assigned to entries of the state vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        return np.full(self.N_x, init_val)

    def parameter_vector(self, init_val=0, **kwargs):
        """
        Create an instance of model parameter vector

        :param float init_val: (optional) value assigned to entries of the parameter vector upon initialization
        """
        assert utility.isnumber(
            init_val
        ), "Initial values of entries `init_val` must be a number"
        return np.full(self.N_p, init_val)

    def is_parameter_vector(self, parameter, **kwargs):
        """Test whether the passed parameter vector is valid or not"""
        valid = False
        if isinstance(parameter, np.ndarray):
            if parameter.shape == (self.N_p,):
                valid = True
        return valid

    def is_state_vector(self, state, **kwargs):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.shape == (self.N_x,):
                valid = True
        return valid

    def solve_forward(self, state, verbose=False):
        """
        Apply (solve the forward model) to the given state,
            and return the result.

        :param state: state to which the forward model is applied.
        :param bool verbose: flag to control screen-verbosity
        :returns: result (usually an observation vector) resulting from applying the
            forward model to ``state``
        """
        return self.configurations.solve_forward(state)

    def Jacobian_T_matvec(self, state, eval_at):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side) of
        the model (TLM) transposed, by a model state. If the Jacobian is not
        provided by the user (i.e. ``Jacobian_T_matvec`` is ``None``), then a
        finite-difference approximation is used.

        :param state: state to multiply the Jacobian by
        :param eval_at: state around which the Jacobian is evaluated
        """
        return self.configurations.Jacobian_T_matvec(state, eval_at)

    @property
    def N_x(self):
        """Number of state variables"""
        return self.configurations.N_x

    @property
    def N_p(self):
        """Number of parameters"""
        return self.configurations.N_p


def create_TimeIndependentBlackBox_model(
    solve_forward,
    Np,
    Nx,
    Jacobian_T_matvec=None,
):
    """
    A simple interface to creating an instance of `TimeIndependentBlackBox`.

    :param Callable[[np.ndarray], np.ndarray] solve_forward: a callable that takes
        a np.ndarray of size ``N_p`` and returns a np.ndarray of size ``N_x``.
    :param int N_p: number of parameters of the model.
    :param int N_x: number of state variables.
    :param Callable[[np.ndarray, np.ndarray], np.ndarray] Jacobian_T_matvec: a callable
        that takes a np.ndarray of size ``N_x`` and a np.ndarray of size
        ``N_p`` and returns a np.ndarray of size ``N_p``.
        This is the Jacobian of the ``solve_forward`` function transposed, evaluated
        in the direction of the second argument.
        If this is not provided, then a finite-difference approximation
        is used (expensive!).
    """
    return TimeIndependentBlackBox(
        TimeIndependentBlackBoxConfigs(
            solve_forward=solve_forward,
            N_p=Np,
            N_x=Nx,
            Jacobian_T_matvec=Jacobian_T_matvec,
        )
    )
