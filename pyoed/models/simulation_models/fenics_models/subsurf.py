# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
A module that provides implementation(s) of Poisson Equations model(s)
This module utilizes fenics (dolfin) if supported
"""
from dataclasses import dataclass, field
from pyoed import utility
from pyoed.models.core import (
    TimeIndependentModel,
    TimeIndependentModelConfigs,
)
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    validate_key,
    aggregate_configurations,
    set_configurations,
    SETTINGS,
)

try:
    import dolfin as dl
    import ufl
except ImportError:
    dl = ufl = None  # TODO: Either this or just raise ImportError (as suggested below!)
    print(
        """\n
          \r***\n  Failed to import Fenics (dolfin);
          \rto use simulation models in this module, you must install Fenics/Dolfin first!
          \rSee information/instruction in 'optional-requirements.txt'
          \r***\n
          """
    )

import numpy as np
import math

import os
import re

try:
    import matplotlib.pyplot as plt
    import matplotlib.tri as mtri
    from matplotlib import cm
except ImportError:
    plt = mtri = cm = None


DEF_PLOT_FORMAT = "pdf"
DEBUG = False


@dataclass(kw_only=True, slots=True)
class Subsurf2DConfigs(TimeIndependentModelConfigs):
    """
    Configurations for the :py:class:`Subsurf2D` model.

    :param nx: number of discretization points in x-direction
    :param ny: number of discretization points in y-direction
    :param mesh_elements: name/type of finite elements 'Lagrange' is the default value
    :param mesh_elements_degree: degree of mesh elements
    :param random_seed: random seed to control synthetic ground truth generation if
        random noise is choosen for that.
    """

    model_name: str = "Subsurf2D"
    nx: int = 64
    ny: int = 64
    mesh_elements: str = "Lagrange"
    mesh_elements_degree: tuple[int, int] = (1, 2)
    random_seed: int | None = 123


@set_configurations(Subsurf2DConfigs)
class Subsurf2D(TimeIndependentModel, RandomNumberGenerationMixin):
    """
    A two dimensional Subsurface modeling model.

    :param dict configs: an object holding model configurations. See
        :py:class:`Subsurf2DConfigs`
    """

    def __init__(self, configs=None):
        if dl is None:
            raise ImportError(
                "This model requires Python's package 'dolfin', but failed to import dolfin"
            )
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

        # Create the mesh
        self.__NDIM = 2
        self.__MESH = dl.UnitSquareMesh(self.nx, self.ny)

        # Parameter DOFs
        mesh_elements = self.mesh_elements
        mesh_elements_degree = self.mesh_elements_degree
        self.__PARAMETER_DOF = dl.FunctionSpace(
            self.__MESH, mesh_elements, mesh_elements_degree[0]
        )
        # State DOFs
        self.__STATE_DOF = dl.FunctionSpace(
            self.__MESH, mesh_elements, mesh_elements_degree[1]
        )

        # Degrees of Freedom (ndofs)
        self.__STATE_SIZE = self.__STATE_DOF.dim()
        self.__PARAMETER_SIZE = self.__PARAMETER_DOF.dim()
        if DEBUG:
            print("Set up the mesh and finite element spaces")
            print(
                "Number of dofs: STATE={0}, PARAMETER={1}, ADJOINT={0}".format(
                    self.__STATE_SIZE, self.__PARAMETER_SIZE
                )
            )

        # Initialize Expressions
        self.__BCfwd = dl.DirichletBC(
            self.__STATE_DOF,
            dl.Expression("x[1]", element=self.__STATE_DOF.ufl_element()),
            lambda x, on_boundary: self.__v_boundary(x=x, on_boundary=on_boundary),
        )
        self.__BCadj = dl.DirichletBC(
            self.__STATE_DOF,
            dl.Constant(0.0),
            lambda x, on_boundary: self.__v_boundary(x=x, on_boundary=on_boundary),
        )
        self.__BCfwd_inc = dl.DirichletBC(
            self.__STATE_DOF,
            dl.Constant(0.0),
            lambda x, on_boundary: self.__v_boundary(x=x, on_boundary=on_boundary),
        )

        # Parametr Assembler
        trial = dl.TrialFunction(self.__PARAMETER_DOF)
        test = dl.TestFunction(self.__PARAMETER_DOF)
        self.__PARAMETER_M = dl.assemble(ufl.inner(trial, test) * ufl.dx)

        trial = dl.TrialFunction(self.__STATE_DOF)
        test = dl.TestFunction(self.__STATE_DOF)
        self.__STATE_M = dl.assemble(ufl.inner(trial, test) * ufl.dx)

        # Forward and Adjoint solvers
        self.__create_solvers()

        # Associate ground truth
        self.__TRUE_INITIAL_CONDITION = self.create_initial_condition(
            method="sin",
            return_np=True,
        )

    def validate_configurations(
        self,
        configs: dict | Subsurf2DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`Subsurf2DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`Subsurf2DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=dim_param,
                    test=is_positive_integer,
                    message=f"{dim_param} must be a positive integer!",
                    raise_for_invalid=raise_for_invalid,
                )
                for dim_param in ["nx", "ny"]
            ]
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="mesh_elements",
            test=lambda x: isinstance(x, str),
            message="Mesh elements must be a string!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="mesh_elements_degree",
            test=lambda x: all(is_positive_integer(y) for y in x),
            message="Mesh elements degree must be a tuple!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda x: x is None or is_integer(x),
            message="Random seed must be an integer or None!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def __u_boundary(self, x, on_boundary):
        """Check whether x falls on the boundary (for forward model BCs)"""
        # flag = on_boundary and ( x[1] < dl.DOLFIN_EPS or x[1] > 1.0 - dl.DOLFIN_EPS)  # Orig from Hipp
        flag = on_boundary and (
            x[1] < dl.DOLFIN_EPS
            or x[1] > 1.0 - dl.DOLFIN_EPS
            or x[0] < dl.DOLFIN_EPS
            or x[0] > 1.0 - dl.DOLFIN_EPS
        )  # From Vishwas
        return flag

    def __v_boundary(self, x, on_boundary):
        """Check whether x falls on the boundary (for adjoing model BCs)"""
        # flag = on_boundary and (x[0]<dl.DOLFIN_EPS or x[0] > 1.0-dl.DOLFIN_EPS)  # Orig
        flag = on_boundary and (x[1] < 1e-10 or x[1] > 1.0 - 1e-10)  # from Vishwas
        return flag

    def __true_random_IC(self, return_np=False):
        """
        Create true parameter/initial condition; here, we use white noise;
            could be replaced with some prior knowledge
        """
        noise = self.random_number_generator.standard_normal(self.__PARAMETER_SIZE)
        if return_np:
            mtrue = noise
        else:
            mtrue = self.parameter_vector(return_np=False)
            mtrue.set_local(noise)
        return mtrue

    def __true_deterministic_IC(self, return_np=False):
        """
        Create true parameter/initial condition; here, we use white noise;
            could be replaced with some prior knowledge
        """

        mtrue_expr = "log1p(1+7*(pow(pow(x[0]-0.5,2) + pow(x[1] - 0.5,2),0.5) > 0.2))"
        mtrue = dl.interpolate(
            dl.Expression(mtrue_expr, degree=5), self.__PARAMETER_DOF
        )
        mtrue = mtrue.vector().get_local()
        if not return_np:
            _mtrue = self.parameter_vector(return_np=False)
            _mtrue.set_local(mtrue)
            mtrue = _mtrue
        return mtrue

    def __true_sin_IC(self, return_np=False):
        """
        Create true parameter/initial condition;
        """
        mtrue = dl.interpolate(
            dl.Expression("sin(x[0])", element=self.__PARAMETER_DOF.ufl_element()),
            self.__PARAMETER_DOF,
        )

        mtrue = mtrue.vector().get_local()
        if not return_np:
            _mtrue = self.parameter_vector(return_np=False)
            _mtrue.set_local(mtrue)
            mtrue = _mtrue
        return mtrue

    def create_initial_condition(self, method="sin", return_np=True):
        """
        Create the initial condition associated with the passed model grid points

        :param str method: two methods are supported now:\n
            - 'random': create noramlly distributed noise vector\n
            - 'deterministic': this creates a predefined IC, given a hard-coded expression; will be fixed/updated/extended as needed
        :returns: numpy array (same shape as model_grid associated with
            the model) holding initial condition values.
        """
        if re.match(r"\Arandom\Z", method, re.IGNORECASE):
            mtrue = self.__true_random_IC(return_np=return_np)
        elif re.match(r"\Asin\Z", method, re.IGNORECASE):
            mtrue = self.__true_sin_IC(return_np=return_np)
        elif re.match(r"\Adet(erministic)*\Z", method, re.IGNORECASE):
            mtrue = self.__true_deterministic_IC(return_np=return_np)
        else:
            raise ValueError(
                "Unsupported initialization method {0}; only 'deterministic' and 'random are currently supported".format(
                    method
                )
            )
        return mtrue

    def __create_solvers(
        self, solver="petsc-krylov", preconditioner="ml_amg", rel_tol=1e-9, abs_tol=1e-9
    ):
        """
        Initialize the FE solver and assign operators and preconditioners
        """
        if re.match(r"\Apetsc(-| |_)lu\Z", solver, re.IGNORECASE):
            self.__SOLVER = dl.PETScLUSolver()
            self.__ADJOINT_SOLVER = dl.PETScLUSolver()
            self.__INCFWD_SOLVER = dl.PETScLUSolver()
            self.__INCADJ_SOLVER = dl.PETScLUSolver()

        elif re.match(r"\Adl(-| |_)lu\Z", solver, re.IGNORECASE):
            self.__SOLVER = dl.LUSolver()
            self.__ADJOINT_SOLVER = dl.LUSolver()
            self.__INCFWD_SOLVER = dl.LUSolver()
            self.__INCADJ_SOLVER = dl.LUSolver()

        elif re.match(r"\Apetsc(-| |_)Krylov", solver, re.IGNORECASE):
            # Preconditioner
            amg = "petsc_amg"  # basic
            for pp in dl.krylov_solver_preconditioners():
                if pp[0] == preconditioner:
                    amg = preconditioner
                    break

            # Set preconditioner
            self.__SOLVER = dl.PETScKrylovSolver("cg", amg)
            self.__ADJOINT_SOLVER = dl.PETScKrylovSolver("cg", amg)
            #
            self.__INCFWD_SOLVER = dl.PETScKrylovSolver("cg", amg)
            self.__INCADJ_SOLVER = dl.PETScKrylovSolver("cg", amg)

            # Update solvers' parameters
            self.__SOLVER.parameters["relative_tolerance"] = rel_tol
            self.__SOLVER.parameters["absolute_tolerance"] = abs_tol
            self.__INCFWD_SOLVER.parameters["relative_tolerance"] = rel_tol
            self.__INCFWD_SOLVER.parameters["absolute_tolerance"] = abs_tol
            #
            self.__ADJOINT_SOLVER.parameters["relative_tolerance"] = rel_tol
            self.__ADJOINT_SOLVER.parameters["absolute_tolerance"] = abs_tol
            self.__INCADJ_SOLVER.parameters["relative_tolerance"] = rel_tol
            self.__INCADJ_SOLVER.parameters["absolute_tolerance"] = abs_tol

        else:
            raise ValueError(
                f"Unrecognized solver {solver}\n"
                f"Currently, we support LU and PETSc-Krylov solvers"
            )

    def __variational_form(self, u, m, p, f=0.0):
        """
        This function returns a weak variational form of the forward PDE

        :param u: the state variable and is also the trial function for the state equations
        :param m: the parameter variable
        :param p: the adjoint variable and is also the test function for the state equations
        :param f:
        :returns: a UFL expression; the weak variational formaulation of the forward model
        """
        if utility.isnumber(f):
            f = dl.Constant(f)
        varf = (
            ufl.exp(m) * ufl.inner(ufl.grad(u), ufl.grad(p)) * ufl.dx - f * p * ufl.dx
        )
        return varf

    def __variational_adjoint_form(self, u, m, p, f=0.0):
        """
        This function returns a weak variational form of the left hand side of the adjoint PDE.

        :param u: the state variable and is also the test function for the adjoint equations
        :param m: the parameter variable
        :param p: the adjoint variable and is also the trial function for the adjoint equations
        :param f:

        :returns: a UFL expression; the weak variational formaulation of the forward model

        :remakrs: Innovations need to be assembled separately
        A good practice to have the test function be u_hat -- for example in nonlinear problems
        we might have u (the state) in the adjoint equation
        """
        f = dl.Constant(f)
        varf = (
            ufl.exp(m) * ufl.inner(ufl.grad(u), ufl.grad(p)) * ufl.dx - f * u * ufl.dx
        )
        return varf

    def __variational_misfit_form(self, u, m, p):
        """
        This function returns a weak variational form of the left hand side of the adjoint PDE.

        :param u: the state variable and is also the test function for the adjoint equations
        :param m: the parameter variable
        :param p: the adjoint variable and is also the trial function for the adjoint equations

        :returns: a UFL expression; the weak variational formaulation of the forward model

        :remakrs: Innovations need to be assembled separately
        """
        mhat = dl.TestFunction(self.__PARAMETER_DOF)
        varf = ufl.exp(m) * mhat * ufl.inner(ufl.grad(u), ufl.grad(p)) * ufl.dx
        return varf

    def parameter_vector(self, init_val=0, return_np=True):
        """
        Create an instance of model parameter vector.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        parameter = np.empty(self.__PARAMETER_SIZE)
        parameter[:] = init_val
        if not return_np:
            out = dl.Vector()
            self.__PARAMETER_M.init_vector(out, 0)
            out.set_local(parameter)
            parameter = out
        return parameter

    def state_vector(self, init_val=0, return_np=True):
        """
        Create an instance of model state vector.
            Here, this is a 1D Numpy array of size equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        state = np.empty(self.__STATE_SIZE)
        state[:] = init_val
        if not return_np:
            out = dl.Vector()
            self.__STATE_M.init_vector(out, 0)
            out.set_local(state)
            state = out
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.size == self.__STATE_SIZE:
                valid = True
        elif isinstance(state, dl.Vector):
            if state.size() == self.__STATE_SIZE:
                valid = True

        return valid

    def is_parameter_vector(self, parameter):
        """Test whether the passed parameter vector is valid or not"""
        valid = False
        if isinstance(parameter, np.ndarray):
            if parameter.size == self.__PARAMETER_SIZE:
                valid = True
        elif isinstance(parameter, dl.Vector):
            if parameter.size() == self.__PARAMETER_SIZE:
                valid = True
        return valid

    def solve_forward(self, parameter, return_np=True, verbose=False):
        """
        Solve the forward problem (parameter -> state)
        """
        if isinstance(parameter, np.ndarray):
            _parameter = self.parameter_vector(return_np=False)
            _parameter.set_local(parameter)
            parameter = _parameter

        # Define residual form, and set Solver Operator
        u = dl.TrialFunction(self.__STATE_DOF)
        m = self.__vector2Function(parameter, self.__PARAMETER_DOF)
        p = dl.TestFunction(self.__STATE_DOF)  # Adjoint space size matches state
        f = dl.interpolate(
            dl.Expression("(x[0]*x[0] + x[1]*x[1])", degree=2), self.__STATE_DOF
        )
        #
        res_form = self.__variational_form(u=u, m=m, p=p, f=f)
        A_form = ufl.lhs(res_form)
        b_form = ufl.rhs(res_form)

        # Assemble separately to avoid artifacts on the boundary imposed by nonsymetry in fenics
        A = dl.assemble(A_form)
        b = dl.assemble(b_form)
        # Enforce Boundary conditions
        self.__BCfwd.apply(A)
        self.__BCfwd.apply(b)
        self.__SOLVER = dl.LUSolver(A)

        state = self.state_vector(return_np=False)
        # Solve
        self.__SOLVER.solve(state, b)

        if DEBUG:
            print("DEBUG:forward solver: A; ", A)
            print("DEBUG:forward solver: b", b)
            print("DEBUG:forward solver: state", state)

        if return_np:
            state = state.get_local()
        return state

    def solve_adjoint(
        self, adj_rhs, eval_at=None, solve_for_param=True, return_np=True, verbose=False
    ):
        """
        Solve the variational adjoint problem;
            and optionally solve for parameter!
        """
        # Validate the parameter
        if eval_at is None:
            raise ValueError(
                "You MUST pass a valid 'eval_at' for this model implementation"
            )

        elif isinstance(eval_at, np.ndarray):
            parameter = self.parameter_vector(return_np=False)
            parameter.set_local(eval_at)
        else:
            parameter = eval_at.copy()

        if isinstance(adj_rhs, np.ndarray):
            _adj_rhs = self.state_vector(return_np=False)
            _adj_rhs.set_local(adj_rhs)
            adj_rhs = _adj_rhs

        #
        u = dl.TestFunction(self.__STATE_DOF)
        m = self.__vector2Function(parameter, self.__PARAMETER_DOF)
        p = dl.TrialFunction(self.__STATE_DOF)
        b = self.__vector2Function(adj_rhs, self.__STATE_DOF)  # TODO: NOT NEEDED!
        res_form = self.__variational_adjoint_form(
            u=u, m=m, p=p
        )  # Forward and adjoint are identical here
        Aadj_form = ufl.lhs(res_form)
        badj_form = ufl.rhs(res_form)

        Aadj = dl.assemble(Aadj_form)
        b = dl.assemble(badj_form)
        b.set_local(adj_rhs.get_local())

        # Enforce boundary conditions
        self.__BCadj.apply(Aadj)
        self.__BCadj.apply(b)

        adj = self.state_vector(return_np=False)
        if DEBUG:
            print("DEBUG:adjoint solver: Aadj; ", Aadj)
            print(
                "DEBUG:adjoint solver: Aadj shape ({0}, {1}) ".format(
                    Aadj.size(0), Aadj.size(1)
                )
            )
            print("DEBUG:adjoint solver: adj_rhs", adj_rhs)
            print("DEBUG:adjoint solver: adj_rhs size", adj_rhs.size())
            print("DEBUG:adjoint solver: adj", adj)
            print("DEBUG:adjoint solver: adj size", adj.size())

        # solve
        self.__ADJOINT_SOLVER = dl.LUSolver(Aadj)
        self.__ADJOINT_SOLVER.solve(adj, b)

        # up to this point, adj is $\lambda$ the Lagrangian parameter
        # If requested, we solve the integral term for parameter (m)
        if solve_for_param:
            # TODO: solve the integral term, and replace adj with the parameter

            # Forward solve
            state = self.solve_forward(parameter, return_np=False)
            u = self.__vector2Function(state, self.__STATE_DOF)
            p = self.__vector2Function(adj, self.__STATE_DOF)
            varform = self.__variational_misfit_form(u=u, m=m, p=p)
            adj = dl.assemble(varform)
            if DEBUG:
                print("DEBUG: adj (in param space)", adj.size())

        if return_np:
            adj = adj.get_local()
        return adj

    def solve_incremental_forward(
        self, H_dir, parameter, state=None, return_np=True, verbose=False
    ):
        """

        :param parameter: parameter at which the Hessian is evaluated
        :param state: state resulting by solving the forward problem using the passed `parameter`.
        :param H_dir: direction in which to evaluate the Hessian (2nd-order derivative)
        """
        if not self.is_parameter_vector(parameter):
            raise TypeError(
                "The passed parameter vector is not valid; check type/shape/size"
            )

        if isinstance(parameter, np.ndarray):
            parameter = self.parameter_vector(parameter, return_np=False)
        if state is None:
            state = self.solve_forward(parameter, return_np=False)
        elif not self.is_state_vector(state):
            raise TypeError(
                "The passed state vector is not valid; check type/shape/size"
            )

        elif isinstance(state, np.ndarray):
            state = self.state_vector(state, return_np=False)

        lam_hat = dl.TestFunction(self.__STATE_DOF)
        u_inc = dl.TrialFunction(self.__STATE_DOF)

        #
        if DEBUG:
            print("H_dir: ")
            for v in H_dir:
                print(f"{v},", end=" ")
            print()

            print("parameter/eval_at: ")
            for v in parameter:
                print(f"{v},", end=" ")
            print()

        #
        u = self.__vector2Function(state, self.__STATE_DOF)
        m = self.__vector2Function(parameter, self.__PARAMETER_DOF)
        H_dir_dl = parameter.copy()
        H_dir_dl[:] = H_dir
        mhat = self.__vector2Function(H_dir_dl, self.__PARAMETER_DOF)
        # Sp_varf = ufl.exp(m) * ufl.inner(ufl.grad(u_inc), ufl.grad(lam_hat)) * ufl.dx \
        #     + ufl.exp(m) * H_dir * ufl.inner(ufl.grad(state), ufl.grad(lam_hat)) * ufl.dx
        Sp_varf = self.__variational_incremental_forward_form(
            uinc=u_inc, u=u, m=m, pinc=lam_hat, mhat=mhat
        )
        # Assemble and apply boundary conditions
        Sp_form = ufl.lhs(Sp_varf)
        b_form = ufl.rhs(Sp_varf)

        if DEBUG:
            print("B_FORM: ", b_form)
            print("Sp_Form: ", Sp_form)

        Sp = dl.assemble(Sp_form)
        b = dl.assemble(b_form)
        self.__BCadj.apply(Sp)
        self.__BCadj.apply(b)

        # Sp_Mat = Sp.array()
        # __, ss, __ = np.linalg.svd(Sp_Mat)
        # print(ss)
        # print(np.linalg.norm(Sp_Mat))

        # self.__INCFWD_SOLVER.set_operator(Sp)
        # u_out = self.state_vector(return_np=False)
        # self.__INCFWD_SOLVER.solve(u_out, b)

        # Set operator and solve
        self.__SOLVER = dl.LUSolver(Sp)
        u_out = self.state_vector(return_np=False)
        self.__SOLVER.solve(u_out, b)

        if DEBUG:
            print("u_out:")
            for v in u_out:
                print(f"{v},", end=" ")
            print()
            # print("LU Solver works")

        return u_out

    def solve_incremental_adjoint(
        self,
        H_dir,
        parameter,
        adj,
        adj_res,
        state,
        u_inc=None,
        return_np=True,
        verbose=False,
    ):
        """

        :param parameter: parameter at which the Hessian is evaluated
        :param state: state resulting by solving the forward problem using the passed `parameter`.
        :param H_dir: direction in which to evaluate the Hessian (2nd-order derivative)
        :param u_inc: incremental adjoint returned by :py:meth:`solve_incremental_forward`
        """
        if u_inc is None:
            u_inc = self.solve_incremental_forward(
                H_dir=H_dir,
                parameter=parameter,
                state=state,
                return_np=False,
                verbose=verbose,
            )
        if not self.is_parameter_vector(parameter):
            raise TypeError(
                "parameter is not valid parameter vector; check size/type/shape"
            )

        elif isinstance(parameter, np.ndarray):
            parameter = self.parameter_vector(parameter, return_np=False)

        if not self.is_parameter_vector(H_dir):
            raise TypeError(
                "H_dir is not valid parameter vector; check size/type/shape"
            )

        elif isinstance(H_dir, np.ndarray):
            H_dir = self.parameter_vector(H_dir, return_np=False)

        if not self.is_state_vector(adj):
            raise TypeError(
                "adj is not valid adjoint vector (should be in state space here); check size/type/shape"
            )

        elif isinstance(adj, np.ndarray):
            adj = self.state_vector(adj, return_np=False)
        if not self.is_state_vector(adj_res):
            raise TypeError(
                "adj_res is not valid adjoint vector (should be in state space here); check size/type/shape"
            )

        elif isinstance(adj_res, np.ndarray):
            adj_res = self.state_vector(adj_res, return_np=False)

        test = dl.TestFunction(self.__STATE_DOF)
        trial = dl.TrialFunction(self.__STATE_DOF)
        m = self.__vector2Function(parameter, self.__PARAMETER_DOF)
        adj = self.__vector2Function(adj, self.__STATE_DOF)
        H_dir = self.__vector2Function(H_dir, self.__PARAMETER_DOF)
        #
        # TODO: Vishwas fix this!
        Ap_varf_1 = (
            ufl.exp(m) * ufl.inner(ufl.grad(test), ufl.grad(trial)) * ufl.dx
            + ufl.exp(m) * H_dir * ufl.inner(ufl.grad(adj), ufl.grad(test)) * ufl.dx
        )
        # Ap_varf_2 = ufl.exp(m) * H_dir * ufl.inner(ufl.grad(adj), ufl.grad(test)) * ufl.dx
        Ap_form = ufl.lhs(Ap_varf_1)
        b_form = ufl.rhs(Ap_varf_1)

        # TODO: DEBUG Here; the varform doesn't seem to be correct!

        Ap = dl.assemble(Ap_form)
        if DEBUG:
            print("DEBUG: Ap, ", Ap)
        b = dl.assemble(b_form)
        if DEBUG:
            print("DEBUG: b, ", b)
        b.axpy(1.0, adj_res)
        self.__BCadj.apply(Ap)
        self.__BCadj.apply(b)

        # add adjoint residual term to b

        self.__SOLVER = dl.LUSolver(Ap)
        u_out = self.state_vector(return_np=False)
        # print("U_out: ", u_out)
        self.__SOLVER.solve(u_out, b)
        # self.__INCFWD_SOLVER.solve(u_out, b)
        # print("LU Solver works")
        # return u_out

        # self.__INCADJ_SOLVER.set_operator(Ap)
        # u_out = self.state_vector(return_np=False)
        # self.__INCADJ_SOLVER.solve(u_out, b)

        return u_out

    def evaluate_second_order_sensitivities(
        self,
        H_dir,
        adj,
        inc_state,
        inc_adj,
        parameter,
        state=None,
        return_np=False,
        verbose=False,
    ):
        """
        Hessian-vector product of the data-misfit term in variational DA problems
        """
        # TODO: Discuss how to make this more general, and other potential cases.
        if state is None:
            state = self.solve_forward(parameter, return_np=False)

        test = dl.TestFunction(self.__PARAMETER_DOF)

        m = self.__vector2Function(
            self.parameter_vector(parameter, return_np=False), self.__PARAMETER_DOF
        )
        H_dir = self.__vector2Function(
            self.parameter_vector(H_dir, return_np=False), self.__PARAMETER_DOF
        )
        state = self.__vector2Function(
            self.state_vector(state, return_np=False), self.__STATE_DOF
        )
        inc_state = self.__vector2Function(
            self.state_vector(inc_state, return_np=False), self.__STATE_DOF
        )
        adj = self.__vector2Function(
            self.state_vector(adj, return_np=False), self.__STATE_DOF
        )
        inc_adj = self.__vector2Function(
            self.state_vector(inc_adj, return_np=False), self.__STATE_DOF
        )

        varf = (
            ufl.exp(m)
            * test
            * H_dir
            * ufl.inner(ufl.grad(adj), ufl.grad(state))
            * ufl.dx
            + ufl.exp(m) * test * ufl.inner(ufl.grad(adj), ufl.grad(inc_state)) * ufl.dx
            + ufl.exp(m) * test * ufl.inner(ufl.grad(inc_adj), ufl.grad(state)) * ufl.dx
        )
        res = dl.assemble(varf)
        return res

    def __variational_incremental_forward_form(self, uinc, u, m, pinc, mhat):
        """
        This function returns a weak variational form of the incremental forward PDE.

        :param u: the state variable and is also the test function for the adjoint equations
        :param m: the parameter variable
        :param pinc: the incremental forward variable and is also the trial function for the incremental forward equations
        :param mhat: the vector to which the Hessian needs to be applied

        :returns: a UFL expression; the weak variational formaulation of the forward model

        :remarks: Innovations need to be assembled separately
        Incremental forward can be checked independently. By using a TLM type calculation.
        """
        varf = (
            ufl.exp(m) * mhat * ufl.inner(ufl.grad(u), ufl.grad(pinc)) * ufl.dx
            + ufl.exp(m) * ufl.inner(ufl.grad(uinc), ufl.grad(pinc)) * ufl.dx
        )
        return varf

    def __variational_incremental_adjoint_form(self, uinc, u, m, p, pinc, mhat):
        """
        This function returns a weak variational form of the incremental forward PDE.

        :param u: the state variable and is also the test function for the adjoint equations
        :param m: the parameter variable
        :param p: the adjoint variable and is also the trial function for the adjoint equations
        :param mhat: the vector to which the Hessian needs to be applied

        :returns: a UFL expression; the weak variational formaulation of the forward model

        :remarks: Innovations need to be assembled separately
        """
        raise NotImplementedError("TODO")
        # TODO: move the variational form here, and update notation
        varf = (
            ufl.exp(m) * ufl.inner(ufl.grad(test), ufl.grad(trial)) * ufl.dx
            + ufl.exp(m) * H_dir * ufl.inner(ufl.grad(adj), ufl.grad(test)) * ufl.dx
        )
        return varf

    def __vector2Function(self, state, Vh, **kwargs):
        """Convert an FE vector `state` into an FE function"""
        f = dl.Function(Vh, **kwargs)
        f.vector().zero()
        f.vector().axpy(1.0, state)
        return f

    def plot(
        self,
        vec,
        target="state",
        show_mesh=True,
        mesh_linewidth=0.1,
        mesh_linecolor="#FFAC67",
        clean_xy_ticks=True,
        xlim=None,
        ylim=None,
        title=None,
        fontsize=20,
        cmap="jet",
        alpha=0.95,
        dpi=800,
        add_colorbar=True,
        saveto=None,
        verbose=False,
    ):
        """
        Create a 2D triangulation plot of the passed state/parameter vector `x`

        :param vec: model state/parameter vector
        :param str target: string identifying whether `vec` is a state or a parameter vector
        :param bool show_mesh: show/hide mesh lines
        :param mesh_linewidth:
        :param mesh_linecolor:
        :param clean_xy_ticks:
        :param xlim:
        :param ylim:
        :param title:
        :param fontsize:
        :param cmap:
        :param alpha:
        :param add_colorbar:
        :param saveto: filename/path to save plot to (format is set to 'pdf' not part of the filename)
        :param bool verbose: screen verbosity
        """
        if plt is None:
            print("Matplotlib couldn't be loaded!")
            raise ImportError

        try:
            cmap = eval("cm.%s" % cmap)
        except AttributeError:  # just in case!
            if cmap.lower() == "jet":  # more can be added later
                cmap = cm.jet
            elif cmap.lower() == "hot":
                cmap = cm.hot
            elif cmap.lower() == "hot_r":
                cmap = cm.hot_r
            elif cmap.lower() == "afmhot":
                cmap = cm.afmhot
            elif cmap.lower() == "afmhot_r":
                cmap = cm.afmhot_r
            else:
                print("Unrecognized color map. Resetting to jet color map")
                cmap = cm.jet

        if re.match(r"\Astate\Z", target, re.IGNORECASE):
            if not self.is_state_vector(vec):
                print("The passed vector is not a valid state vector!")
                raise TypeError
            file_prefix = "Subsurface2D_state"
            dof = self.__STATE_DOF
        elif re.match(r"\Aparam(eter)*\Z", target, re.IGNORECASE):
            if not self.is_parameter_vector(vec):
                print("The passed vector is not a valid parameter vector!")
                raise TypeError
            file_prefix = "Subsurface2D_parameter"
            dof = self.__PARAMETER_DOF
        else:
            print(
                "Unknown target '{0}'. Only 'state' or 'parameter' are supported".format(
                    target
                )
            )
            raise ValueError
        vec_array = vec[:]

        # extract coordinates of the mesh:
        gdim = self.__MESH.geometry().dim()
        try:
            dofmap = dof.dofmap()
            mesh_dof_xy = dofmap.tabulate_all_coordinates(self.__MESH).reshape(
                (-1, gdim)
            )
        except:
            mesh_dof_xy = dof.tabulate_dof_coordinates().reshape((-1, gdim))

        # Construct the triangulation for the cartesian grid in the model data
        x = mesh_dof_xy[:, 0]
        y = mesh_dof_xy[:, 1]
        triangulation = mtri.Triangulation(x, y)

        fig = plt.figure(facecolor="white")
        ax = fig.add_subplot(111)

        # plot
        cf = ax.tricontourf(triangulation, vec_array, cmap=cmap, alpha=alpha, zorder=-1)
        if show_mesh:
            ax.triplot(
                triangulation,
                lw=mesh_linewidth,
                c=mesh_linecolor,
                alpha=alpha,
                zorder=0,
            )

        if add_colorbar:
            cb = fig.colorbar(cf, ax=ax)
            cb.ax.tick_params(labelsize=fontsize)

        # set plot ticks and limits
        if clean_xy_ticks:
            ax.set_xticks([])
            ax.set_yticks([])

        # set plot ticks and limits
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.set_ylim(ylim)
        try:
            ax.set_aspect("equal")
        except NotImplementedError:
            pass

        if title is not None:
            fig.suptitle(title, fontsize=fontsize)

        def_format = DEF_PLOT_FORMAT
        if saveto is None:
            dir_name = os.path.abspath(os.getcwd())
            file_name = file_prefix
            file_format = def_format
        else:
            dir_name, file_name = os.path.split(os.path.abspath(saveto))
            file_name, file_format = os.path.splitext(file_name)
            file_format = file_format.strip(" .")
            if file_format == "":
                file_format = def_format

        saveto = os.path.join(dir_name, "{0}.{1}".format(file_name, file_format))
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        fig.savefig(saveto, dpi=dpi, bbox_inches="tight")
        if verbose:
            print("Plot saved to: {0}".format(saveto))
        plt.close(fig)

    def get_model_grid(self):
        """
        Return a numpy array representation of the model grid
        """
        # extract coordinates of the mesh:
        gdim = self.__MESH.geometry().dim()
        try:
            dofmap = self.__STATE_DOF.dofmap()
            mesh_dof_xy = dofmap.tabulate_all_coordinates(self.__MESH).reshape(
                (-1, gdim)
            )
        except:
            mesh_dof_xy = self.__STATE_DOF.tabulate_dof_coordinates().reshape(
                (-1, gdim)
            )

        # Construct the triangulation for the cartesian grid in the model data
        # x = mesh_dof_xy[:, 0]
        # y = mesh_dof_xy[:, 1]
        # triangulation = mtri.Triangulation(x, y)

        return mesh_dof_xy

    def __validate_configs(self, configs):
        """
        Aggregate (with default) and validate model config
        """
        # Time step settings
        nx = configs["nx"]
        ny = configs["ny"]
        assert int(nx) == nx and nx > 1, "nx must be an integer with nx >= 1"
        assert int(ny) == ny and ny > 1, "ny must be an integer with ny >= 1"
        configs.update({"nx": int(nx), "ny": int(ny)})
        # TODO: add more verification
        pass

    @property
    def true_initial_condition(self):
        return self.__TRUE_INITIAL_CONDITION

    @property
    def state_dof(self):
        """dof/FunctionSpace correspnding the the model state"""
        return self.__STATE_DOF

    @property
    def parameter_dof(self):
        """dof/functionspace correspnding the the model parameter"""
        return self.__PARAMETER_DOF

    @property
    def nx(self):
        """Number of grid points in the x-direction"""
        return self.configurations.nx

    @property
    def ny(self):
        """Number of grid points in the y-direction"""
        return self.configurations.ny

    @property
    def mesh_elements(self):
        """Type of mesh elements"""
        return self.configurations.mesh_elements

    @property
    def mesh_elements_degree(self):
        """Degree of mesh elements"""
        return self.configurations.mesh_elements_degree


def create_Subsurf2D_model(
    nx=64,
    ny=64,
    mesh_elements="Lagrange",
    mesh_elements_degree=(1, 2),
    output_dir=SETTINGS.OUTPUT_DIR,
):
    """
    A simple interface to creating an instance of `Subsurf2D`.

    :param int nx:
    :param int ny:
    :param str mesh_elements:
    :param mesh_elements_degree:

    :returns: an instance of Subsurf2D with passed settings
    """
    configs = Subsurf2DConfigs(
        nx=nx,
        ny=ny,
        mesh_elements=mesh_elements,
        mesh_elements_degree=mesh_elements_degree,
        output_dir=output_dir,
    )
    model = Subsurf2D(configs=configs)
    return model
