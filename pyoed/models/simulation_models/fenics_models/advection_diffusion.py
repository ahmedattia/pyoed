# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
A module that provides implementation(s) of simulation models implemented in
Dolfin (Fenics)
"""
import warnings
from dataclasses import dataclass, field
from pyoed import utility
from pyoed.models.core import (
    TimeIndependentModel,
    TimeDependentModel,
    TimeDependentModelConfigs,
)
from pyoed.configs import (
    validate_key,
    PyOEDConfigsValidationError,
    aggregate_configurations,
    set_configurations,
    SETTINGS,
)

from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.models.observation_operators import fenics_observation_operators

try:
    import dolfin as dl
    import ufl
except ImportError:
    dl = ufl = None  # TODO: Either this or just raise ImportError (as suggested below!)
    print(
        """\n
          \r***\n  Failed to import Fenics (dolfin);
          \rto use simulation models in this module, you must install Fenics/Dolfin first!
          \rSee information/instruction in 'optional-requirements.txt'
          \r***\n
          """
    )
    warnings.warn(
        f"The AD model with Fenics backend module is loaded, however it won't be possible to actually use it witout Fenics/Dolfin installed!"
    )


import numpy as np
import math

import re
import os

try:
    import matplotlib.pyplot as plt
    import matplotlib.tri as mtri
    from matplotlib import cm
    import matplotlib.patches as patches
    from matplotlib.patches import Circle

except ImportError:
    plt = mtri = cm = patches = Circle = None

DEF_PLOT_FORMAT = "pdf"
DEBUG = True


# TODO: once better plotting is developed, this hardcoding will be removed
# These define the corner points of inner boundaries and should be identified automatically (if any) from the mesh
_AD_20__BUILDINGS_COORDINATES = [
    [(0.25, 0.15, 0.00), 0.25, 0.25],
    [(0.60, 0.60, 0.00), 0.15, 0.25],
]


@dataclass(kw_only=True, slots=True)
class AdvectionDiffusion2DConfigs(TimeDependentModelConfigs):
    """
    Configurations for the two dimensional Advection Diffusion Model.

    :param mesh_configs: dictionary holding mesh configurations/settings.
        They include:

          - `mesh_file`: path to the mesh file. Default path is to included `ad_20.xml`
            file.
          - * `n_mesh_refinement`: number of mesh refinements. Default is 0.
          - `mesh_elements`: type of mesh elements. Default is `Lagrange`.
          - `mesh_elements_degree`: degree of mesh elements. Default is 2.

    :param gls_stab: use Galerkin Least Squares stabilization. Default is True.
    :param dt: time step size. Default is 0.2.
    :param t_eps: time precision. Default is 1e-6.
    """

    model_name: str = "AD-Dolfin-2D"
    mesh_configs: dict = field(
        default_factory=lambda: {
            "mesh_file": os.path.join(os.path.dirname(__file__), "meshes", "ad_20.xml"),
            "n_mesh_refinement": 0,
            "mesh_elements": "Lagrange",
            "mesh_elements_degree": 2,
        }
    )  # TODO: make flexible and enable constructing it
    gls_stab: bool = True
    dt: float = 0.2
    t_eps: float = 1e-6


@set_configurations(AdvectionDiffusion2DConfigs)
class AdvectionDiffusion2D(TimeDependentModel):
    """
    An implementation of the two dimensional Advection Diffusion Model.

    :param configs: object holding model configurations/settings.
    """

    def __init__(self, configs: AdvectionDiffusion2DConfigs | dict | None = None):
        if dl is None:
            raise ImportError(
                f"This model requires Python's package 'dolfin! Failed to import dolfin!"
            )

        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Create the mesh
        mesh_configs = self.mesh_configs
        self.__NDIM = 2
        self.__MESH = self.__create_mesh(mesh_configs)

        # State/Parameter DOFs
        mesh_elements = mesh_configs["mesh_elements"]
        mesh_elements_degree = mesh_configs["mesh_elements_degree"]
        self.__PARAMETER_DOF = dl.FunctionSpace(
            self.__MESH, mesh_elements, mesh_elements_degree
        )
        # State DOFs
        self.__STATE_DOF = self.__PARAMETER_DOF

        # Degrees of Freedom (ndofs)
        self.__STATE_SIZE = self.__STATE_DOF.dim()
        self.__PARAMETER_SIZE = self.__PARAMETER_DOF.dim()
        print("Set up the mesh and finite element spaces")
        print(
            "Number of dofs: STATE={0}, PARAMETER={1}, ADJOINT={0}".format(
                self.__STATE_SIZE, self.__PARAMETER_SIZE
            )
        )

        # Create mass matrix
        u = dl.TrialFunction(self.__STATE_DOF)
        v = dl.TestFunction(self.__STATE_DOF)
        kappa = dl.Constant(0.001)
        dt = self.dt
        dt_expr = dl.Constant(dt)
        wind_velocity = self.__create_velocity_field()
        r_trial = u + dt_expr * (
            -ufl.div(kappa * ufl.grad(u)) + ufl.inner(wind_velocity, ufl.grad(u))
        )
        r_test = v + dt_expr * (
            -ufl.div(kappa * ufl.grad(v)) + ufl.inner(wind_velocity, ufl.grad(v))
        )
        h = dl.CellDiameter(self.__MESH)
        vnorm = ufl.sqrt(ufl.inner(wind_velocity, wind_velocity))
        gls_stab = self.gls_stab
        if gls_stab:
            tau = ufl.min_value((h * h) / (dl.Constant(2.0) * kappa), h / vnorm)
        else:
            tau = dl.Constant(0.0)
        Nvarf = (
            ufl.inner(kappa * ufl.grad(u), ufl.grad(v))
            + ufl.inner(wind_velocity, ufl.grad(u)) * v
        ) * ufl.dx
        N = dl.assemble(Nvarf)
        Ntvarf = (
            ufl.inner(kappa * ufl.grad(v), ufl.grad(u))
            + ufl.inner(wind_velocity, ufl.grad(v)) * u
        ) * ufl.dx
        Nt = dl.assemble(Ntvarf)
        stab = dl.assemble(tau * ufl.inner(r_trial, r_test) * ufl.dx)

        # State settings
        self.__STATE_M = dl.assemble(ufl.inner(u, v) * ufl.dx)
        self.__STATE_M_stab = dl.assemble(ufl.inner(u, v + tau * r_test) * ufl.dx)
        self.__STATE_Mt_stab = dl.assemble(ufl.inner(u + tau * r_trial, v) * ufl.dx)

        # Parameter is the initial condition (same settings as state here.)
        self.__PARAMETER_M = self.__STATE_M
        self.__PARAMETER_M_stab = self.__STATE_M_stab
        self.__PARAMETER_Mt_stab = self.__STATE_Mt_stab

        # Forward and Adjoint solvers
        L = self.__STATE_M + dt * N + stab
        Lt = self.__STATE_M + dt * Nt + stab
        self.__create_solver(L=L, Lt=Lt)

        # Create ground truth
        self.__TRUE_INITIAL_CONDITION = self.create_initial_condition()

        # TODO: add more...
        pass

    def validate_configurations(
        self,
        configs: dict | AdvectionDiffusion2DConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in :py:class:`AdvectionDiffusion2DConfigs`
            are validated. Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`AdvectionDiffusion2DConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        is_integer = lambda x: utility.isnumber(x) and x == int(x)
        is_positive_integer = lambda x: is_integer(x) and (x > 0)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)

        # Not sure how to convert this into a validate_key call
        exists, mesh_configs = aggregated_configs.lookup(
            "mesh_configs", configs.asdict(deep=False)
        )
        if exists:
            if len(mesh_configs) != 4:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Partial mesh_configs update not supported, provide all four configs!"
                        f"Received {mesh_configs=}"
                    )
                else:
                    return False
            # ensure mesh_file exists
            mesh_file = mesh_configs["mesh_file"]
            if not os.path.isfile(mesh_file):
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Mesh file not found in '{mesh_file}'!"
                    )
                else:
                    return False
            # n_mesh_refinement
            n_mesh_refinement = mesh_configs["n_mesh_refinement"]
            if not is_integer(n_mesh_refinement):
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Number of mesh refinements must be an integer. "
                        f"Received {n_mesh_refinement=} of {type(n_mesh_refinement)=}"
                    )
                else:
                    return False
            if n_mesh_refinement < 0:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Number of mesh refinements must be non-negative. "
                        f"Received {n_mesh_refinement=}"
                    )
                else:
                    return False
            # mesh_elements
            mesh_elements = mesh_configs["mesh_elements"]
            if not isinstance(mesh_elements, str):
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Mesh elements must be a string. "
                        f"Received {mesh_elements=} of {type(mesh_elements)=}"
                    )
                else:
                    return False
            # mesh_elements_degree
            mesh_elements_degree = mesh_configs["mesh_elements_degree"]
            if not is_integer(mesh_elements_degree):
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Mesh elements degree must be an integer. "
                        f"Received {mesh_elements_degree=} of {type(mesh_elements_degree)=}"
                    )
                else:
                    return False
            if mesh_elements_degree < 1:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        f"Mesh elements degree must be at least 1. "
                        f"Received {mesh_elements_degree=}"
                    )
                else:
                    return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="gls_stab",
            test=lambda x: isinstance(x, bool),
            message="Galerkin Least Squares stabilization must be a boolean!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="dt",
            test=lambda x: is_positive_float(x) and (x >= aggregated_configs.t_eps),
            message="dt must be a positive float greater than t_eps!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="t_eps",
            test=lambda x: is_positive_float(x) and (x <= aggregated_configs.dt),
            message="t_eps must be a positive float smaller than dt!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def __create_mesh(self, mesh_configs):
        """
        Given the dictionary mesh_configs', create a mesh for the model, and return it.

        :param dict mesh_configs: dictionary with following configurations
            - `mesh_file`: filepath contining mesh description to be loaded by dolfin.Mesh
            - `n_mesh_refinement`: number of refiniments applied to the loaded/created mesh

        :returns: an instance of `dolfin.Mesh`
        """
        # Extract mesh configurations
        mesh_file = mesh_configs["mesh_file"]
        # Create and refine the mesh
        if os.path.isfile(mesh_file):
            mesh = dl.Mesh(mesh_file)
        else:
            raise NotImplementedError(
                f"Mesh file not found in '{mesh_file}'\n"
                f"Only reading mesh is currently supported; auto-generate is to-be-developed"
            )

        # Refine mesh if needed
        n_mesh_refinement = mesh_configs["n_mesh_refinement"]
        for i in range(n_mesh_refinement):
            mesh = dl.refine(mesh)

        return mesh

    def __create_velocity_field(self):
        """
        Create *constant* velocity field
        """

        # define local functions
        def v_boundary(x, on_boundary):
            return on_boundary

        def q_boundary(x, on_boundary):
            return x[0] < dl.DOLFIN_EPS and x[1] < dl.DOLFIN_EPS

        def strain(v):
            return ufl.sym(ufl.grad(v))

        mesh = self.__MESH
        Xh = dl.VectorFunctionSpace(mesh, "Lagrange", 2)
        Wh = dl.FunctionSpace(mesh, "Lagrange", 1)
        XW = dl.FunctionSpace(
            mesh, dl.MixedElement([Xh.ufl_element(), Wh.ufl_element()])
        )

        Re = 1e2
        g = dl.Expression(
            ("0.0", "(x[0] < 1e-14) - (x[0] > 1 - 1e-14)"), element=Xh.ufl_element()
        )
        bc1 = dl.DirichletBC(XW.sub(0), g, v_boundary)
        bc2 = dl.DirichletBC(XW.sub(1), dl.Constant(0), q_boundary, "pointwise")
        bcs = [bc1, bc2]

        vq = dl.Function(XW)
        (v, q) = ufl.split(vq)
        (v_test, q_test) = dl.TestFunctions(XW)

        F = (
            (2.0 / Re) * ufl.inner(strain(v), strain(v_test))
            + ufl.inner(ufl.nabla_grad(v) * v, v_test)
            - (q * ufl.div(v_test))
            + (ufl.div(v) * q_test)
        ) * ufl.dx

        dl.solve(
            F == 0,
            vq,
            bcs,
            solver_parameters={
                "newton_solver": {
                    "relative_tolerance": 1e-15,
                    "maximum_iterations": 100,
                    "linear_solver": "default",
                }
            },
        )
        return v

    def __create_solver(
        self, L, Lt, solver="LU", preconditioner="ml_amg", rel_tol=1e-15, abs_tol=1e-20
    ):
        """
        Initialize the FE solver and assign operators and preconditioners
        """
        if re.match(r"\Alu\Z", solver, re.IGNORECASE):
            # forward & adjoint solvers
            self.__SOLVER = dl.LUSolver(L)
            self.__ADJOINT_SOLVER = dl.LUSolver(Lt)

            # Set properties
            if "relative_tolerance" in self.__SOLVER.parameters.keys():
                self.__SOLVER.parameters["relative_tolerance"] = rel_tol

            if "relative_tolerance" in self.__ADJOINT_SOLVER.parameters.keys():
                self.__ADJOINT_SOLVER.parameters["relative_tolerance"] = rel_tol

        elif re.match(r"\Apetsc(-| |_)*lu\Z", solver, re.IGNORECASE):
            # forward & adjoint solvers
            self.__SOLVER = dl.PETScLUSolver(L)
            self.__ADJOINT_SOLVER = dl.PETScLUSolver(Lt)

            # Set Forward Solver properties
            if "relative_tolerance" in self.__SOLVER.parameters.keys():
                self.__SOLVER.parameters["relative_tolerance"] = rel_tol

            # Set Forward Solver properties
            if "relative_tolerance" in self.__ADJOINT_SOLVER.parameters.keys():
                self.__ADJOINT_SOLVER.parameters["relative_tolerance"] = rel_tol

            if "absolute_tolerance" in self.__SOLVER.parameters.keys():
                self.__SOLVER.parameters["absolute_tolerance"] = abs_tol

            if "absolute_tolerance" in self.__ADJOINT_SOLVER.parameters.keys():
                self.__ADJOINT_SOLVER.parameters["absolute_tolerance"] = abs_tol

        elif re.match(r"\Apetsc(-| |_)*Krylov", solver, re.IGNORECASE):
            # Preconditioner
            amg = "petsc_amg"  # basic
            for pp in dl.krylov_solver_preconditioners():
                if pp[0] == preconditioner:
                    amg = preconditioner
                    break

            # forward & adjoint solvers
            self.__SOLVER = dl.PETScKrylovSolver("cg", amg)
            self.__ADJOINT_SOLVER = dl.PETScKrylovSolver("cg", amg)

            # Set Operators
            self.__SOLVER.set_operator(dl.as_backend_type(self.L))
            self.__ADJOINT_SOLVER.set_operator(dl.as_backend_type(Lt))

            # Set Forward Solver properties
            if "relative_tolerance" not in self.__SOLVER.parameters.keys():
                self.__SOLVER.parameters.add("relative_tolerance", rel_tol)
            else:
                self.__SOLVER.parameters["relative_tolerance"] = rel_tol

            # Set Forward Solver properties
            if "relative_tolerance" not in self.__ADJOINT_SOLVER.parameters.keys():
                self.__ADJOINT_SOLVER.parameters.add("relative_tolerance", rel_tol)
            else:
                self.__ADJOINT_SOLVER.parameters["relative_tolerance"] = rel_tol

            if "absolute_tolerance" not in self.__SOLVER.parameters.keys():
                self.__SOLVER.parameters.add("absolute_tolerance", abs_tol)
            else:
                self.__SOLVER.parameters["absolute_tolerance"] = abs_tol

            if "absolute_tolerance" not in self.__ADJOINT_SOLVER.parameters.keys():
                self.__ADJOINT_SOLVER.parameters.add("absolute_tolerance", abs_tol)
            else:
                self.__ADJOINT_SOLVER.parameters["absolute_tolerance"] = abs_tol
        else:
            print("Unrecognized solver {0}".format(solver))
            print("Currently, we support LU and PETSc-Krylov solvers")
            raise ValueError

    def parameter_vector(self, init_val=0, return_np=True):
        """
        Create an instance of model parameter vector.

        :param float init_val: value assigned to entries of the state vector
        :returns: 1d numpy array
        """
        parameter = np.empty(self.__PARAMETER_SIZE)
        parameter[:] = init_val
        if not return_np:
            out = dl.Vector()
            self.__PARAMETER_M.init_vector(out, 0)
            out.set_local(parameter)
            parameter = out
        return parameter

    def state_vector(self, init_val=0, return_np=True):
        """
        Create an instance of model state vector.  Here, this is a 1D Numpy
        array of size equal to the model grid.

        :param float init_val: value assigned to entries of the state vector
        :param bool return_np: return a numpy array or dolfin vector
        :returns: 1d numpy array
        """
        state = np.empty(self.__STATE_SIZE)
        state[:] = init_val
        if not return_np:
            out = dl.Vector()
            self.__STATE_M.init_vector(out, 0)
            out.set_local(state)
            state = out
        return state

    def is_state_vector(self, state):
        """Test whether the passed state vector is valid or not"""
        valid = False
        if isinstance(state, np.ndarray):
            if state.size == self.__STATE_SIZE:
                valid = True
        elif isinstance(state, dl.GenericVector):
            if state.size() == self.__STATE_SIZE:
                valid = True

        return valid

    def is_parameter_vector(self, parameter):
        """Test whether the passed parameter vector is valid or not"""
        valid = False
        if isinstance(parameter, np.ndarray):
            if parameter.size == self.__PARAMETER_SIZE:
                valid = True
        elif isinstance(parameter, dl.GenericVector):
            if parameter.size() == self.__PARAMETER_SIZE:
                valid = True
        return valid

    def integrate_state(
        self, state, tspan, checkpoints=None, return_np=True, verbose=False
    ):
        """
        Simulate/integrate the model starting from the initial `state` over the
        passed `checkpoints`.

        :param state: data structure holding the initial model state
        :param tspan: (t0, tf) iterable with two entries specifying of
            the time integration window
        :param checkpoints: times at which to store the computed solution,
            must be sorted and lie within tspan.
            If None (default), use points selected by the solver [t0, t1, ..., tf].
        :param bool verbose: output progress to screen if `True`.
            If set to `False`, nothing is printed to screen`

        :returns: a list holding the timespan, and a list holding the
            model trajectory with entries corresponding to the simulated
            model state at entries of `checkpoints` starting from `checkpoints[0]`
            and ending at `checkpoints[-1]`.

        :raises:
            - `AssertionError` if `tspan` is not valid, or checkpoints are
              not within `tspan`
            - `TypeError` is raised if the passed state is not a valid state vector

        :remarks: This Model is tied to a predefined time step dt in configurations;
            The distance between all checkpoints (including tspan) must be
            multiples of this timestep
        """
        try:
            t0, tf = tspan
        except:
            print("tspan must be an iterable with two intries (t0, tf)")
            raise AssertionError

        if t0 >= tf:
            print("The timespan 'tspan' must be (t0, tf) with t0 < tf")
            raise ValueError

        # Time precision
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        # Default timestep and checkpoints if not passed
        dt = self.dt
        # nt = int(np.round(tf - t0, prec) // dt)
        nt = int(np.round((tf - t0) / dt, prec))

        if nt == 0:
            print(f"Tspan must be of length >= {dt}")
            print(f"t0 = {t0}; tf={tf}")
            raise ValueError

        if checkpoints is None:
            checkpoints = np.array([t0 + i * dt for i in range(nt)])
            # checkpoints = np.linspace(t0, dt * nt, nt)
            if tf > checkpoints[-1]:
                checkpoints = [t for t in checkpoints] + [tf]
            elif tf < checkpoints[-1]:
                checkpoints[-1] = tf

        # Get unique checkpoints and sort (implicitly done by np.unique())
        checkpoints = np.unique(np.asarray(checkpoints).flatten())
        checkpoints = np.round(checkpoints, prec)

        # This is becuase the Matrix A of the model is defined as forward integrator over dt.
        steps = checkpoints[1:] - checkpoints[:-1]
        steps = np.round(steps, prec)
        nsteps = np.round(steps / dt).astype(int)
        remainders = np.round(checkpoints[1:] - checkpoints[:-1] - nsteps * dt, prec)
        if np.any(remainders):
            print("DEBUG: checkpoints: {0}".format(checkpoints))
            print("DEBUG: INCREMENTS: {0}".format(steps))
            print("DEBUG: REMAINDERS: {0}".format(np.remainder(steps, dt)))
            print("This Model is tied to a predefined time step dt={0}".format(dt))
            print(
                "The distance between all checkpoints must be multiples of this timestep."
            )
            raise AssertionError

        # Validate the passed timespan and checkpoints
        assert checkpoints.size >= 1, "Number of checkpoints must be at least one"
        assert checkpoints[0] >= 0, "Only non-negative checkpoints are allowed"
        assert (
            t0 <= checkpoints[0] <= checkpoints[-1] <= tf
        ), "checkpoints must lie within the passed tspan!"
        # All good; proceed

        # Check the passed state
        if not self.is_state_vector(state):
            print("Couldn't cast the passed state into a state vector; check type/size")
            raise TypeError

        # verbosity and screen output settings
        scr_out_end = "\n"  # " "

        # Slice the passed state, and cast into a state that floats over windows
        flt_time = t0  # time associated with the floating state below
        flt_state = self.state_vector(return_np=False)
        try:
            flt_state[:] = (
                state[:].copy().flatten()
            )  # flatten is needed for scipy iterative solvers such as svds
        except:
            print("Failed to slice passed state: ", state)
            raise

        # Initialize the trajectory;
        traject = []

        # Loop over all times in checkpoints
        for t in checkpoints:
            if (flt_time - t) > self.t_eps:
                # here, t < flt_time
                print(
                    "The model has been propagated beyond a checkpoint while it shouldn't!"
                )
                raise ValueError
            elif abs(flt_time - t) < self.t_eps:
                # here, t == flt_time
                if return_np:
                    traject.append(flt_state.get_local())
                else:
                    traject.append(flt_state.copy())
            else:
                # here, t > flt_time; integrate forward from flt_time to t

                # Number of steps/iterations to apply the underlying operator/matrix/model
                # nt = int(np.round(t-flt_time, prec) // dt)
                nt = int(np.round((t - flt_time) / dt, prec))

                # Another sanity check!
                if not abs((t - flt_time) - nt * dt) < self.t_eps:
                    print("This should never happen!")
                    raise AssertionError

                # Propage state by repeated multiplication by the underlying model (nt times)
                for i in range(nt):
                    if verbose:
                        print(
                            "Integration over interval [{0}, {1}]".format(
                                flt_time, flt_time + dt
                            )
                        )

                    flt_state = self.solve_forward_sub(
                        flt_state, 0, dt, return_np=False
                    )
                    flt_time += dt

                # Update trajectory
                if return_np:
                    traject.append(flt_state.get_local())
                else:
                    traject.append(flt_state.copy())

        return checkpoints, traject

    def solve_forward_sub(self, ic, t_init, t_final, return_np=True):
        """
        Propagate the initial condition ic, over the interval [t_init, t_final], and return the resulting state

        :param ic:
        :param t_init:
        :param t_final:
        :param bool return_np:

        :returns:
        """
        assert t_init < t_final, "Final time must succeed the initial time"
        if not self.is_state_vector(ic):
            print("Couldn't cast the passed ic into a state vector; check type/size")
            raise TypeError

        uold = self.state_vector(return_np=False)
        uold[:] = ic[:].copy()

        u = dl.Vector()
        rhs = dl.Vector()
        self.__STATE_M.init_vector(u, 0)
        self.__STATE_M.init_vector(rhs, 0)

        dt = self.dt
        t = t_init
        while t < t_final - 0.5 * dt:
            t += dt
            self.__STATE_M_stab.mult(uold, rhs)
            self.__SOLVER.solve(u, rhs)
            uold = u
        if return_np:
            u = u.get_local()
        return u

    def Jacobian_T_matvec(
        self, state, eval_at_t=None, eval_at=None, dt=None, return_np=True
    ):
        """
        Evaluate and return the product of the Jacobian (of the right-hand-side)
        of the model (TLM) transposed, by a model state.  This model is linear,
        so the TLM is the same as the underlying model

        :param state: state to multiply the Jacobian by
        :param eval_at_t: time at which the Jacobian is evaluated
        :param eval_at: state around which the Jacobian is evaluated
        :param float dt: the step size
        :param bool return_np: return numpy array or dolfin.Vector instance

        :returns: the product of the Jacobian transposed (adjoint operator) by a model state.
        :remakrs: `eval_at_t` and `eval_at` are added to match signature of inherited methods,
            but they are ignored since the Jacobian is independent from the state/time and is fixed here.
        """
        if dt is None:
            dt = self.dt
        if not abs(dt - self.dt) < self.t_eps:
            raise AssertionError(
                "This Model is tied to a predefined time step dt={0}".format(dt)
            )

        JT_u = self.solve_adjoint_sub(
            state, t_final=dt, t_init=0.0, return_np=return_np
        )
        return JT_u

    def solve_adjoint_sub(self, adjoint, t_final, t_init, return_np=True):
        """
        Propagate the adjoint/sensitivity vector backward by solving the adjoint
        problem over the interval [t_init, t_final], and return the resulting
        initial condition/state This ignores any observations made in the time
        subinterval

        :param adjoint:
        :param float t_final:
        :param float t_init:
        """
        assert t_init < t_final, "Final time must succeed the initial time"

        # Time precision
        prec = len(("%f" % (self.t_eps - int(self.t_eps))).split(".")[1])

        dt = self.dt
        # nt = int(np.round(t_final-t_init, prec) // dt)
        nt = int(np.round((t_final - t_init) / dt, prec))

        if nt * dt != np.round(t_final - t_init, prec):
            print("This Model is tied to a predefined time step dt={0}".format(dt))
            raise AssertionError

        if not self.is_state_vector(adjoint):
            print(
                "Couldn't cast the passed adjoint into a adjoint/state vector; check type/size"
            )
            raise TypeError

        pold = self.state_vector(return_np=False)
        pold[:] = adjoint[:].copy()

        p = dl.Vector()
        u = dl.Vector()
        self.__STATE_M.init_vector(p, 0)
        self.__STATE_M.init_vector(u, 0)

        for i in range(nt):
            self.__ADJOINT_SOLVER.solve(p, pold)
            self.__STATE_M_stab.transpmult(p, u)
            pold = u

        if return_np:
            u = u.get_local()
        return u

    def create_initial_condition(self, return_np=True):
        """
        Create initial condition vector

        :param bool return_np: return numpy array or dolfin.Vector instance

        :remarks: we will consider enabling multiple settings
        """
        ic_expr = dl.Expression(
            "min(0.5,exp(-100*(pow(x[0]-0.35,2) +  pow(x[1]-0.7,2))))",
            element=self.__STATE_DOF.ufl_element(),
        )
        ic = dl.interpolate(ic_expr, self.__STATE_DOF).vector()
        if return_np:
            ic = ic.get_local()
        return ic

    def plot_velocity_field(
        self,
        ax=None,
        show_mesh=True,
        mesh_alpha=0.1,
        show_axis_frame=False,
        boxes_coordinates=None,
        boxes_texts=None,
        boxes_hatch=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        fontsize=22,
        save_to_file=None,
        verbose=False,
    ):
        """ """
        # Utility plots enhancer
        utility.plots_enhancer(
            usetex=True,
            fontweight="normal",
            fontsize=fontsize,
        )

        wind_velocity = self.__create_velocity_field()
        ax = self.plot_domain(
            ax=ax,
            mesh_alpha=mesh_alpha,
            show_mesh=show_mesh,
            show_axis_frame=show_axis_frame,
            boxes_coordinates=boxes_coordinates,
            boxes_texts=boxes_texts,
            boxes_hatch=boxes_hatch,
            sensors_coordinates=sensors_coordinates,
            annotate_sensors=annotate_sensors,
        )
        fig = ax.get_figure()
        dl.plot(
            wind_velocity,
            pivot="tail",
            units="inches",
        )

        if save_to_file is not None:
            save_to_file = os.path.abspath(save_to_file)
            dir_name = os.path.dirname(save_to_file)
            if not os.path.isdir(dir_name):
                os.makedirs(dir_name)
            fig.savefig(save_to_file, dpi=800, bbox_inches="tight", transparent=True)
            if verbose:
                print(f"Plot saved to: '{save_to_file}'")

        return ax

    def plot_domain(
        self,
        ax=None,
        clean_xy_ticks=True,
        xlim=None,
        ylim=None,
        title=None,
        show_mesh=True,
        mesh_linewidth=1.0,
        mesh_linecolor="#000C67",
        mesh_alpha=0.5,
        fontsize=22,
        triangles_lw=0.01,
        boxes_coordinates=None,
        boxes_texts=None,
        boxes_hatch=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        prediction_coordinates=None,
        show_axis_frame=False,
        save_to_file=None,
        verbose=False,
    ):
        """
        Plot the domain, with optional buildings, and optional sensor locations
        and prediction coordinates (for goal-oriented work)

        :param ax: either `None` or a matplotlib axis to plot on. If `None`, a new figure is created.
        :param bool clean xy_ticks: whether to show the x and y ticks. Default
            is `True`
        :param xlim: tuple of (xmin, xmax) to set the x-axis limits. Default is
            `None`
        :param ylim: tuple of (ymin, ymax) to set the y-axis limits. Default is
            `None`
        :param title: string to set the title of the plot. Default is `None`
        :param bool show_mesh: whether to show the mesh. Default is `True`
        :param float mesh_linewidth: linewidth of the mesh. Default is `0.4`
        :param str mesh_linecolor: color of the mesh. Default is `#000C67`
        :param float mesh_alpha: alpha value of the mesh. Default is `0.5`
        :param int fontsize: fontsize of the plot. Default is `16`
        :param float triangles_lw: linewidth of the triangles. Default is `0.01`
        :param boxes_coordinates: list of 3-len entries [(origin, width,
            height)] of buildings. Default is `None`
        :param boxes_texts: list of strings to annotate the buildings. Default
            is None
        :param boxes_hatch: list of strings to hatch the buildings. Default is
            None
        :param sensors_coordinates: list of 2-len entries [(x, y)] of sensor
            locations. Default is `None`.
        :param bool annotate_sensors: whether to annotate the sensors. Default
            is None.
        :param prediction_coordinates: list of 2-len entries [(x, y)] of the
            prediction coordinates. Default is `None`.
        :param bool show_axis_frame: whether to show the axis frame. Default is
            `False`
        :param save_to_file: string of the path to save the plot to. Default is `None`
        :param bool verbose: screen verbosity

        :returns: `ax`, the axis on which the plot is created
        """
        # Utility plots enhancer
        utility.plots_enhancer(
            usetex=True,
            fontweight="normal",
            fontsize=fontsize,
        )

        # TODO: update docstring after validation and testing...
        # Retrieve mesh and create model grid
        mesh = self.state_dof.mesh()
        mesh_dof_xy = mesh.coordinates()
        triangulation = mtri.Triangulation(
            mesh_dof_xy[:, 0], mesh_dof_xy[:, 1], triangles=mesh.cells()
        )

        # Create (or get a handle of the passed) figure, plot, and return axis handle
        if ax is None:
            fig, ax = plt.subplots(facecolor="white", figsize=(10, 10))
            # ax = fig.add_subplot(111)
        else:
            if not isinstance(ax, plt.Axes):
                print(f"The passed axes is of type {type(ax)}")
                print(f"Only matplotlib.axes instance is used here!")
                raise TypeError
            # Good to go;
            fig = ax.get_figure()

        # plot triangular mesh
        if show_mesh:
            ax.triplot(
                triangulation,
                lw=mesh_linewidth,
                c=mesh_linecolor,
                zorder=10,
                alpha=mesh_alpha,
            )

        if boxes_coordinates is not None:
            for i, coord in enumerate(boxes_coordinates):
                try:
                    box_text = boxes_texts[i]
                except:
                    box_text = None
                ax = utility.add_box_to_axis(
                    ax,
                    coord[0],
                    coord[1],
                    coord[2],
                    facecolor="black",
                    edgecolor="black",
                    box_hatch=boxes_hatch,
                    box_text=box_text,
                    zorder=3,
                )

        if sensors_coordinates is not None:
            lbl_ind = 0
            for coord in sensors_coordinates:
                ax = utility.add_circle_to_axis(
                    ax,
                    origin=coord,
                    radius=15,
                    facecolor="#75211B",
                    edgecolor="#75211B",
                    fill=True,
                    alpha=0.75,
                    zorder=5,
                )
                if annotate_sensors:
                    lbl_ind += 1
                    ax.annotate(str(lbl_ind), coord, fontsize=max(fontsize * 0.75, 16))

        if prediction_coordinates is not None:
            for coord in prediction_coordinates:
                ax = utility.add_circle_to_axis(
                    ax,
                    origin=coord,
                    radius=2,
                    facecolor="#60000C",
                    edgecolor="#000000",
                    fill=True,
                    alpha=0.85,
                    zorder=10,
                )

        # set plot ticks and limits
        if clean_xy_ticks:
            ax.set_xticks([])
            ax.set_yticks([])

        # set plot ticks and limits
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.set_ylim(ylim)
        try:
            ax.set_aspect("equal")
        except NotImplementedError:
            pass

        if title is not None:
            fig.suptitle(title, fontsize=fontsize)

        ax.set_frame_on(show_axis_frame)

        if save_to_file is not None:
            save_to_file = os.path.abspath(save_to_file)
            dir_name = os.path.dirname(save_to_file)
            if not os.path.isdir(dir_name):
                os.makedirs(dir_name)
            fig.savefig(save_to_file, dpi=800, bbox_inches="tight", transparent=True)
            if verbose:
                print(f"Plot saved to: '{save_to_file}'")
        return ax

    def plot_state(
        self,
        vec,
        ax=None,
        show_mesh=True,
        mesh_linewidth=1.0,
        mesh_linecolor="#000C67",
        mesh_alpha=0.25,
        clean_xy_ticks=True,
        xlim=None,
        ylim=None,
        cmap="jet",
        alpha=0.75,
        add_colorbar=True,
        title=None,
        fontsize=16,
        triangles_lw=0.01,
        boxes_coordinates=None,
        boxes_texts=None,
        boxes_hatch=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        prediction_coordinates=None,
        show_axis_frame=True,
        save_to_file=None,
        return_axis=False,
        verbose=False,
    ):
        """
        2D Plot of the passed vector
        """
        # TODO: update docstring after validation and testing...

        if save_to_file is None and not return_axis:
            print("save_to_fiel is None and axis won't be returned! wast of plot!")
            print("Nothing to do here!")
            return

        # Utility plots enhancer
        utility.plots_enhancer(
            usetex=True,
            fontweight="normal",
            fontsize=fontsize,
        )

        # Get Proper colormap
        try:
            cmap = eval("cm.%s" % cmap)
        except AttributeError:  # just in case!
            if cmap.lower() == "jet":  # more can be added later
                cmap = cm.jet
            elif cmap.lower() == "hot":
                cmap = cm.hot
            elif cmap.lower() == "hot_r":
                cmap = cm.hot_r
            elif cmap.lower() == "afmhot":
                cmap = cm.afmhot
            elif cmap.lower() == "afmhot_r":
                cmap = cm.afmhot_r
            else:
                print("Unrecognized color map. Resetting to jet color map")
                cmap = cm.jet

        # Validate the passed vector
        if not self.is_state_vector(vec):
            print(f"The passed vector is not valid model state!")
            raise TypeError
        else:
            vec_array = utility.asarray(vec)

        # plot the domain
        ax = self.plot_domain(
            ax=ax,
            clean_xy_ticks=clean_xy_ticks,
            xlim=xlim,
            ylim=ylim,
            title=title,
            show_mesh=show_mesh,
            mesh_linewidth=mesh_linewidth,
            mesh_linecolor=mesh_linecolor,
            mesh_alpha=mesh_alpha,
            fontsize=fontsize,
            triangles_lw=triangles_lw,
            boxes_coordinates=boxes_coordinates,
            boxes_texts=boxes_texts,
            boxes_hatch=boxes_hatch,
            sensors_coordinates=sensors_coordinates,
            annotate_sensors=annotate_sensors,
            prediction_coordinates=prediction_coordinates,
            show_axis_frame=show_axis_frame,
            save_to_file=None,
        )
        fig = ax.get_figure()

        # Retrieve mesh and create model grid
        mesh = self.state_dof.mesh()
        mesh_dof_xy = self.get_model_grid()

        # Construct the triangulation for the cartesian grid in the model data
        x = mesh_dof_xy[:, 0]
        y = mesh_dof_xy[:, 1]

        triangulation = mtri.Triangulation(x, y)

        # plot triangular surfaces
        cf = ax.tricontourf(triangulation, vec_array, cmap=cmap, zorder=0)

        if add_colorbar:
            cb = fig.colorbar(
                cf,
                ax=ax,
                shrink=0.83,
                pad=0.03,
            )
            cb.ax.tick_params(labelsize=fontsize)

        if save_to_file is not None:
            save_to_file = os.path.abspath(save_to_file)
            dir_name = os.path.dirname(save_to_file)
            if not os.path.isdir(dir_name):
                os.makedirs(dir_name)
            fig.savefig(save_to_file, dpi=800, bbox_inches="tight", transparent=True)
            if verbose:
                print(f"Plot saved to: '{save_to_file}'")

        if not return_axis:
            plt.close(fig)
            ax = None

        return ax

    def __get_inner_bounds(self):
        """"""
        mesh = self.state_dof.mesh()
        boundary = dl.BoundaryMesh(mesh, "exterior", True)
        boundary_xy = boundary.coordinates()
        lb = np.min(boundary_xy, 0)
        ub = np.max(boundary_xy, 0)
        inner_bounds = []
        for bc in boundary_xy:
            if lb[0] < bc[0] < ub[0] and lb[1] < bc[1] < ub[1]:
                inner_bounds.append(np.round(bc, 5))
        # inner_bounds = list(set([(a, b) for a, b in inner_bounds]))
        # inner_bounds.sort()

        inner_bounds = np.asarray(inner_bounds)

        corners = []
        for ib in inner_bounds:
            row_matches = np.where(inner_bounds[:, 0] == ib[0])[0].size
            row_vals = inner_bounds[row_matches, :]
            col_matches = np.where(inner_bounds[:, 1] == ib[1])[0].size
            col_vals = inner_bounds[row_matches, :]
            if row_matches > 1 and col_matches > 1:
                corners.append(tuple(list(ib)))
            corners = set(corners)
            print(corners)

        return inner_bounds

    def get_model_grid(self):
        """
        Return a numpy array representation of the model grid
        """
        # extract coordinates of the mesh:
        dof = self.state_dof
        mesh = dof.mesh()
        gdim = mesh.geometry().dim()
        try:
            dofmap = dof.dofmap()
            mesh_dof_xy = dofmap.tabulate_all_coordinates(mesh).reshape((-1, gdim))
        except:
            mesh_dof_xy = dof.tabulate_dof_coordinates().reshape((-1, gdim))

        # mesh = self.state_dof.mesh()
        # mesh_dof_xy = mesh.coordinates()
        return mesh_dof_xy

    @property
    def true_initial_condition(self):
        return self.__TRUE_INITIAL_CONDITION

    @property
    def state_dof(self):
        """dof/FunctionSpace correspnding the the model state"""
        return self.__STATE_DOF

    @property
    def parameter_dof(self):
        """dof/functionspace correspnding the the model parameter"""
        return self.__PARAMETER_DOF

    @property
    def mesh_configs(self):
        """retrieve the model mesh configurations"""
        return self.configurations.mesh_configs

    @property
    def gls_stab(self):
        """retrieve the model mesh configurations"""
        return self.configurations.gls_stab

    @property
    def dt(self):
        """retrieve the model time step size"""
        return self.configurations.dt

    @property
    def t_eps(self):
        """retrieve the model time epsilon tolerance"""
        return self.configurations.t_eps


def create_AdvectionDiffusion2D_model(
    mesh_file=os.path.join(os.path.dirname(__file__), "meshes", "ad_20.xml"),
    mesh_elements="Lagrange",
    mesh_elements_degree=2,
    n_mesh_refinement=0,
    gls_stab=True,
    dt=0.2,
    output_dir=SETTINGS.OUTPUT_DIR,
):
    """
    A simple interface to creating an instance of `AdvectionDiffusion2D`.

    :param mesh_file:
    :param str mesh_elements:
    :param mesh_elements_degree:

    :returns: an instance of Subsurf2D with passed settings
    """
    configs = AdvectionDiffusion2DConfigs(
        dt=dt,
        gls_stab=gls_stab,
        mesh_configs=dict(
            mesh_file=mesh_file,
            mesh_elements=mesh_elements,
            mesh_elements_degree=mesh_elements_degree,
            n_mesh_refinement=n_mesh_refinement,
        ),
        output_dir=output_dir,
    )
    return AdvectionDiffusion2D(configs=configs)
