# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
A module that provides implementation(s) of imaging models (tomography, coherent diffraction imaging (CDI), etc.)

This module is incomplete, and will be updated gradually.
"""
import numpy as np
import scipy as sp
import skimage
import skimage.transform
from dataclasses import dataclass

from pyoed.models.core import (
    TimeIndependentModel,
    TimeIndependentModelConfigs,
)
from pyoed import utility
from pyoed.configs import aggregate_configurations, set_configurations
from warnings import warn

########################################################################################
# TODO: (From Abhi)
# This module needs a good refactor. In particular, Tomography, the only implemented
# model, is missing configuration validation. It is occuring in the
# create_radon_forward_operator function as it is now and I didn't want to touch it
# without understanding the code.
########################################################################################


class Imaging(TimeIndependentModel):
    """
    Base class for imaging models in real-space.
    """

    ...


@dataclass(kw_only=True, slots=True)
class TomographyConfigs(TimeIndependentModelConfigs):
    """
    Configurations for the Tomography model.

    :param model_name: 'Radon-Tomography' this is just for verbosity and output.
    :param n_voxels: Number of pixels along each edge of the image. Default is 50.
    :param n_angles: Number of angles at which to probe the object used to create
        `theta` (if `None` found); as linearly spaced <n_angles> angles between 1, 360.
        This is overridden if theta is not None. Default is 10.
    :param theta: If passed, array-like objects that contains angles (all must be
        between 0, 360). Overridess `n_angles`  if passed. Default is None.
    :param scale_matrix: default True; scale A, by maximum sum of rows!
    """

    model_name: str = "Radon-Tomography"
    n_voxels: int = 50
    n_angles: int = 10
    theta: np.ndarray | None = None
    scale_matrix: bool = True


@set_configurations(TomographyConfigs)
class Tomography(Imaging):
    """
    Tomography forward model.

    :param configs: Object containing the configurations of the Tomography model.

    .. warning::
        This implementation is by no means efficient. In fact, it relies on a very
        inefficient implementation of radon transform.  We will consider more
        efficient approaches if we resort to this much!
    """

    def __init__(self, configs: TomographyConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        self._MODEL = self.create_radon_forward_operator(
            n_voxels=self.configurations.n_voxels,
            n_angles=self.configurations.n_angles,
            theta=self.configurations.theta,
            scale_matrix=self.configurations.scale_matrix,
            verbose=(self.configurations.screen_output_iter > 1),
        )
        self._MODEL_GRID = np.arange(np.size(self._MODEL, 1))

    def validate_configurations(
        self,
        configs: dict | TomographyConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`TomographyConfigs` are validated. Finally, super classes
            validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or
            not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`TomographyConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self,
            configs=configs,
            configs_class=self.configurations_class,
        )

        warn(
            "Configuration validation is not implemented for Tomography model. ",
            RuntimeWarning,
        )

        return super().validate_configurations(configs, raise_for_invalid)

    def create_radon_forward_operator(
        self, n_voxels, n_angles, theta, scale_matrix, verbose=False
    ):
        """
        Construct a radon-transoform matrix A for multiple projection angles.  Each row
        of A, takes as an input an image (flattened out as a vector), and create a radon
        transform.

        See :py:class:`TomographyConfigs` for more details on the parameters.

        :returns: 2D Numpy array representing the radon transform (Linear model for an image)

        """
        if theta is not None:
            theta = np.asarray(theta).flatten()
            theta[theta == 360] = 0
            theta = np.unique(theta)
            num_angles = theta.size
            assert n_angles > 0, "The passsed array 'theta' can't be empty!"
            assert (
                theta[0] >= 0 and theta[-1] < 360
            ), "theta values must be in the interval [0, 360)!"

        else:
            assert n_angles > 0, "Number of angles can't be negative!"
            theta = np.linspace(0, 360, n_angles, endpoint=False)

        # Construct radon transform array:
        if verbose:
            print("Creating Forward Model (Radon Transofrms...)")
        A = np.empty((n_voxels * theta.size, n_voxels**2))
        image = np.zeros((n_voxels, n_voxels))
        count = 0
        for i in range(n_voxels):
            for j in range(n_voxels):
                # compute radon transform, and update matrix A
                image[i, j] = 1
                A[:, i * n_voxels + j] = skimage.transform.radon(
                    image, theta=theta, circle=True
                ).flatten()
                image[i, j] = 0  # reset image

        if verbose:
            print("Scaling..")
        if scale_matrix:
            A /= np.max(np.sum(A, 1))

        if verbose:
            print("Done.")

        return A

    def state_vector(self, init_val=0):
        """
        Create an empty model state vector
        """
        state = np.zeros(np.size(self._MODEL, 1))
        return state

    def is_state_vector(self, state):
        """
        Test if the passed state is of appropriate shape/type/size
        """
        valid = False
        if isinstance(state, np.ndarray):
            if state.shape == (self._MODEL, 1):
                valid = True
        return valid

    def parameter_vector(self, init_val=0):
        """
        Create an instance of model parameter vector.
        Here, the parameter is the model initial state,  and thus :py:meth:`parameter_vector` actually calls :py:meth:`state_vector`

        :param float init_val: (optional) value assigned to entries of
            the parameter vector upon initialization
        """
        return self.state_vector(init_val=init_val)

    def is_parameter_vector(self, parameter):
        """
        Test whether the passed parameter vector is valid or not
        Here, the parameter is the model initial state,  and thus :py:meth:`is_parameter_vector` actually calls :py:meth:`is_state_vector`

        """
        return self.is_state_vector(parameter)

    def solve_forward(self, state, design=None, verbose=False):
        """
        Apply/Solve the model (apply the radon transform to an image or a vectorized
            version thereof)

        :param state: either 1d *flattened* image, or 2d array describing an image
        :param design: observational design; typically a binary array of
            the same size as design, or an array of indexes to extract active observations
            from the result of applying the forward operator.
        :returns: result of applying radon transorm to the passed state/image
        :raises: AssertionError is raised if `state` or `design` don't have
            the right shape/size
        """
        _state = np.asarray(state)
        if np.ndim(_state) == 1:
            return self._apply_forward_operator_1d(state=_state, design=design)
        elif np.ndim(_state) == 2:
            return self._apply_forward_operator_1d(
                state=_state.flatten(), design=design
            )
        else:
            print("passed 'state' must be 1d or 2d array")
            raise AssertionError

    def _apply_forward_operator_1d(self, state, design=None):
        """ """
        if state.size != self._MODEL.shape:
            print("Passed `state` shape is not compatible with the forward  operator")
            print(
                "Expected 1d array/image of *flattened* size {0}".format(
                    self._MODEL.size
                )
            )
            print("Received array of size {0}".format(state.size))
            raise AssertionError

        if design is not None:
            design = np.asarray(design).flatten()
            err_msg = (
                "Design must be 1d array of size {0}; received size is: {1}".format(
                    np.size(self._MODEL, 0), design.size
                )
            )
            assert design.size == np.size(self._MODEL, 0), err_msg

            # Review flattening; Fortran vs. C!
            obs = self._MODEL[design, :].dot(state.flatten())  # TODO: check
        else:
            obs = self._MODEL.dot(state.flatten())

        return obs

    def get_model_array(self):
        """return a copy of the model operator (matrix)"""
        return self._MODEL.copy()

    def get_model_grid(self):
        """return a copy of the model grid"""
        return self._MODEL_GRID.copy()


class CDI(TimeIndependentModel):
    """
    Base class for coherent-diffraction imaging (CDI) models.
    The infrastructure, however, might be different because the
    domain is in complex space,
    rather than real space
    This might end up being similar to `Imaging`; we will see!
    """

    def __init__(self, configs=None):
        raise NotImplementedError("TODO...")


class FourierPtychography(CDI):
    """
    Fourier-based Ptychography imaging model
    """

    def __init__(self, configs=None):
        raise NotImplementedError("TODO...")


class BraggPtychography(CDI):
    """
    Bragg Ptychography imaging model
    """

    def __init__(self, configs=None):
        raise NotImplementedError("TODO...")


def create_Tomography_model(
    n_voxels=50,
    n_angles=10,
    theta=None,
    scale_matrix=True,
):
    """
    Simple interface to create an instace of :py:class:`Tomography`
    """
    return Tomography(
        configs={
            "n_voxels": n_voxels,
            "n_angles": n_angles,
            "theta": theta,
            "scale_matrix": scale_matrix,
        }
    )


def create_CDI_model(*args, **kwargs):
    """
    Simple interface to create an instace of :py:class:`CDI`
    """
    raise NotImplementedError("TODO")


def create_FourierPtychography_model(*args, **kwargs):
    """
    Simple interface to create an instace of :py:class:`FourierPtychography`
    """
    raise NotImplementedError("TODO")


def create_BraggPtychography_model(*args, **kwargs):
    """
    Simple interface to create an instace of :py:class:`BragPtychography`
    """
    raise NotImplementedError("TODO")
