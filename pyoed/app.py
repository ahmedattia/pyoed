# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import sys

def run():
    """
    This function is called upon running the package from commandline
    e.g., python -m pyoed
    """
    sys.exit(
        "Runnig PyOED from commandline is not yet supported"
    )

