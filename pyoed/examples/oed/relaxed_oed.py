# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides several experiments and examples explaining how relaxed OED
can be applied to Bayesian inverse problems under PyOED.
"""

# Essential imports
import os
import sys
import re
import numpy as np
import pickle
import matplotlib.pyplot as plt

# from mpl_toolkits.mplot3d import axes3d, Axes3D

# Add PyOED root path for proper access
pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility
OED_RANDOMIZATION_SETTINGS = {
    "sample_size": 50,
    "optimize_sample_size": False,
    "min_sample_size": 10,
    "max_sample_size": 100,
    "distribution": "Rademacher",
    "randomization_vectors": None,
    "random_seed": None,
    "accuracy": 1e-2,
}
USE_FIM = True

CONFIGS_FILENAME = "settings.pcl"

# Import PyOED  modules (model, observation operator, error models)
import pyoed
from pyoed import utility

# simulation model(s)
from pyoed.models.simulation_models import (
    toy_linear,
    # advection_diffusion,
    fenics_models,
)

# observation operators(s)
from pyoed.models.observation_operators import (
    identity,
    interpolation,
    fenics_observation_operators,
)

# error models
from pyoed.models.error_models import (
    Gaussian,
    Laplacian,
)

# data assimilation (inversion)
from pyoed.assimilation.smoothing import fourDVar
from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "OED/Relaxed_OED")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


def create_penalty_function(name="l1", input_size=None, budget=None):
    """
    Create two handles to evaluate the value and the gradient of
    a penalty term (for regularization/sparsification) in OED optimization problems.
    The function takes the form ::math:`f(d)` where ::math:`d` is a design vector

    :param str name: name of the penalty function
        - 'l0': number of nonzero entries in the
        - 'l1'

    :returns: two functions to evaluate the penalty term and associated gradient, respectively.
        If the function is non-differentiable, e.g., 'l0' penalty, the handle to the gradient is set to `None`.
    """
    if budget is None:
        budget = 0
    if re.match(r"\Al(_| |-)*0\Z", name, re.IGNORECASE):

        def penalty_function(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return np.abs(np.count_nonzero(d) - budget)

        penalty_function_gradient = None

    elif re.match(r"\Al(_| |-)*1\Z", name, re.IGNORECASE):

        def penalty_function(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return np.power(np.sum(d) - budget, 2)

        def penalty_function_gradient(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return 2.0 * (np.sum(d) - budget) * np.ones_like(d)

    else:
        print(f"Unsupported penalty function name '{name}'")
        raise ValueError

    return penalty_function, penalty_function_gradient


def relaxed_OED_time_dependent_toy_linear(
    model_state_size=4,
    num_obs_time_points=1,
    use_FIM=USE_FIM,
    #
    # Reproduction and General Settings
    random_seed=RANDOM_SEED,
    #
    # Inverse Problem Settings
    prior_variance=1,
    prior_correlation=False,
    #
    obs_operator="uniform",
    uniform_obs_grid_size=2,
    observation_error_variance=0.25,
    obs_error_correlation=False,
    #
    # OED Settings
    oed_criterion="Bayesian A-opt",
    oed_evaluation_method="randomized",
    penalty_function="l1",
    penalty_weight=0,
    budget=None,
    #
    #
    # Tasks to Carry Out
    solve_OED_problem=True,
    validate_IP_gradient=False,
    validate_OED_gradient=False,
    #
    # Verbosity and Output Settings
    output_dir=os.path.join(get_base_output_dir(), "TimeDependentToyLinearModel"),
):
    """
    A driver that enables testing/experimenting with relaxed optimal experimental design
    (Relaxed OED) with an inverse problem that involves a toy linear inverse problem.
    The linear inverse problem consists of a Toy linear forward model on the form
    :math:`x_{t_{n+1}} = A x_{t_n}` where :math:`x_{t_n}` is the model state at time
    :math:`t_n`.

    Since the inverse problem is linear, we can form a Gaussian posterior exactly and
    test things properly and efficiently.

    Observation is carried out by interpolation; that is, the model output is
    interpolated on the observation grid. Observations are obtained by perturbing the
    model output by Gaussian noise (with/without) correlations based on the experiments
    setup.

    :param model_state_size:
    :param num_obs_time_points:
    :param checkpoints:
    :param random_seed:
    :param prior_variance:
    :param bool prior_correlations: if `True` the prior covariance matrix is initially
        correlated, otherwise a diagonal matrix is created. If the uncertain parameter
        is prior covariance, this is overriden by the specification of the uncertainty
        kernel.
    :param obs_operator: Type of the observation operator to create here; the following
        are supported in this driver:\n

            - 'identity': observe all model state vectors; in this case the number of
              sensors = number of observation points = number of model grid points. In
              this case, the `uniform_obs_grid_size` argument is ignored as the
              observation (and design) size is equal to `model_state_size`
            - 'uniform': distribute the grid points (number of grid points is defined by
              `uniform_obs_grid_size` argument below) Thus, the design size is equal to
              `uniform_obs_grid_size`, and interpolation is carried out for observation.
    :param uniform_obs_grid_size:
    :param observation_error_variance:
    :param bool obs_error_correlation: if `True` the observation error covariances are
        initially correlated, otherwise a diagonal matrix is created. If the uncertain
        parameter is observation covariance, this is overriden by the specification of
        the uncertainty kernel.
    :param str oed_criterion:
    :param str penalty_function: name of the penalty function to use. Either 'l0' or
        'l1'.
    :param budget: used in combination with `penalty_function` to define the penalty
        term
    :param penalty_weight: the penalty parameter of the OED optimization problem (for
        soft-constraint)
    :param solve_OED_problem:
    :param bool validate_IP_gradient: use finite differences (FD) to validate the
        gradient of the inverse problem (IP) objective
    :param bool validate_OED_gradient: use finite differences (FD) to validate the
        gradient of the optimal experimental design (OED) objective
    :param output_dir:

    :returns: A tuple of two entitles: (oed_problem, oed_results).
    """
    # Create a local random number generator from the passed seed
    rng = np.random.default_rng(random_seed)

    # ------------------------------------------------------------------------
    # Create the simulation model; Instantiate the toy linear model object,
    #    and extract true/reference initial condition and othe model entities
    # ------------------------------------------------------------------------
    model_time_step = 1.0
    model_configs = dict(
        nx=model_state_size,
        dt=model_time_step,
    )
    model = toy_linear.ToyLinearTimeDependent(configs=model_configs)
    true_model_state = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # -------------------------------------------------
    # Create prior model for the model state (Gaussian)
    # -------------------------------------------------
    prior_covmat = utility.create_covariance_matrix(
        size=model_state_size,
        corr=prior_correlation,
        stdev=np.sqrt(prior_variance),
    )
    prior_configs = dict(
        size=model_state_size,
        mean=0.0,
        variance=prior_covmat,
        random_seed=random_seed,
    )
    prior = Gaussian.GaussianErrorModel(
        configs=prior_configs,
    )
    prior.mean = prior.add_noise(true_model_state, in_place=False)

    # ---------------------------------------------------
    # Create Observation grid and an Observation operator
    # ---------------------------------------------------
    obs_operator = obs_operator.strip().lower()
    if re.match(r"\Aidentity\Z", obs_operator, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid.copy()
        observation_operator = identity.Identity(configs=dict(model=model))

    elif re.match(r"\Auniform\Z", obs_operator, re.IGNORECASE):
        # Uniformly distributed grid; interpolation operator is used
        observation_grid = np.linspace(
            0.5, model_state_size - 1.5, uniform_obs_grid_size
        )
        obs_configs = dict(
            model_grid=model_grid,
            observation_grid=observation_grid,
        )
        observation_operator = interpolation.CartesianInterpolator(configs=obs_configs)

    else:
        print(f"The observation operator '{obs_operator}' is not supported!")
        raise ValueError

    # --------------------------------------------
    # Create an observation error model (Gaussian)
    # --------------------------------------------
    observation_size = observation_grid.size  # Observation vector size
    obs_err_cov = utility.create_covariance_matrix(
        size=observation_size,
        corr=obs_error_correlation,
        stdev=np.sqrt(observation_error_variance),
    )
    obs_err_configs = dict(
        size=observation_size,
        mean=0.0,
        variance=obs_err_cov,
        random_seed=random_seed,
    )
    # The error model need to allow relaxation
    obs_err_model = Gaussian.PrePostWeightedGaussianErrorModel(configs=obs_err_configs)

    # -------------------------------------------------------------
    # Create an inverse problem (here use FourDVar/Smoother), then:
    #   - Create and assign noisy/synthetic observations
    # -------------------------------------------------------------
    checkpoints = np.arange(
        0, model_time_step * (num_obs_time_points + 0.5), model_time_step
    )
    fdvar_configs = dict(
        window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior=prior,
        observation_operator=observation_operator,
        observation_error_model=obs_err_model,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=fdvar_configs)

    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[1:],
    )
    # perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)

    # ---------------------------
    # Create relaxed OED problem:
    # ---------------------------
    # Create Relaxed OED problem with kernel generated above
    oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            inverse_problem=inverse_problem,
            optimizer="ScipyOptimizer",
            optimizer_configs={"bounds": (0, 1)},
            problem_is_linear=True,
            output_dir=output_dir,
        )
    )

    # Register The optimality criterion
    oed_problem.register_optimality_criterion(oed_criterion)
    oed_problem.oed_criterion.update_configurations(
        use_FIM=use_FIM,
        evaluation_method=oed_evaluation_method,
        **OED_RANDOMIZATION_SETTINGS,
    )

    # Create (and register) an optional penalty function (with associated gradient; for relaxation approach)
    penalty_function, penalty_function_gradient = create_penalty_function(
        name=penalty_function,
        budget=budget,
    )
    oed_problem.register_penalty_term(
        penalty_function=penalty_function,
        penalty_function_gradient=penalty_function_gradient,
        penalty_weight=penalty_weight,
    )

    # ======================================
    # Start carrying out the requested tasks
    # ======================================

    # ----------------------------------
    # Test the 4D-Var objective gradient
    # ----------------------------------
    if validate_IP_gradient:
        print("Validating Inverse problem gradient")
        inverse_problem.objective(prior.mean, fd_validation=validate_IP_gradient)

    # ----------------------------------
    # Test the OED objective gradient
    # ----------------------------------
    if validate_OED_gradient:
        design = oed_problem.inverse_problem.observation_error_model.design
        designs_pool = [0.5 * rng.random(design.size) for i in range(3)]
        for design in designs_pool:
            oed_problem.oed_objective_gradient(design=design, fd_validation=True)

    # ----------------------------------------
    # Solve the stochastic binary OED problem
    # ----------------------------------------
    if solve_OED_problem:
        msg = "=" * 50 + "\n"
        msg += "***\nModel Grid:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.model.get_model_grid()
        )
        msg += "***\nObservation Grid:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_operator.get_observation_grid()
        )
        msg += "***\nForward Operator:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.model.get_model_array()
        )
        msg += "***\nPrior Covariance Matrix:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.prior.covariance_matrix()
        )
        msg += "***\nObservation Error Covariance Matrix:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_error_model.covariance_matrix()
        )
        msg += "***\nAssimilation Window:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.window
        )
        msg += "***\nObservation Time Points:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_times
        )
        msg += "=" * 50 + "\n\n"
        print(msg)

        # Solve the OED problem, and plot results as needed
        oed_results = oed_problem.solve()

        # Pickle/Write Results, and plot
        saveto = os.path.join(output_dir, "oed_results.pcl")
        oed_results.write_results(saveto=saveto)
        try:
            oed_results.plot_results(output_dir=output_dir)
        except:
            print("Failed to plot OED results...")

    else:
        oed_results = None

    return oed_problem, oed_results  # for debugging


# Test Relaxed OED using a 2D Advection-Diffusion model (with Finite Elements discretization)
def create_dolfin_observation_operator(
    model,
    targets=None,
    num_obs_points=20,
    offset=0.1,
    exclude_boxes=[((0.25, 0.150), (0.50, 0.400)), ((0.60, 0.625), (0.75, 0.850))],
    verbose=False,
):
    """
    Create a pointwise observation operator for Dolfin-based model.

    :param model: the simulation model
    :param targets: 2D array with shape (n, 2) elements describing coordinates
        of the n observation/sensor points. If None, a set of targets will be
        created using the following arguments
    :param int num_obs_points: (approximate) number of observation points (sensors).
        This number will mostly change especially if there are buildings to avoid
    :param offset: margin/offset to leave between boundary and extreme observation points
    :param exclude_boxes: either None, or a list of tuples; each tuple must be
        a pair of tuples (xl, yl), (xu, yu) to exclude points in the box
        with corners set to these coordintes.
        This simulates buildings in the domain.
    :param bool verbose: screen verbosity

    :returns: oed_problem, oed_results
    """
    if targets is None:
        # define observation operator, and observations locations
        n_rows = int(np.sqrt(num_obs_points))
        n_cols = num_obs_points // n_rows

        n_obs = 0
        while n_obs < num_obs_points:
            _candidates = []
            x_coords = np.linspace(offset, 1 - offset, num=n_rows)
            y_coords = np.linspace(offset, 1 - offset, num=n_cols)

            for x in x_coords:
                for y in y_coords:
                    # Check the coordinate (x, y) and add if not inside the boxes
                    in_box = False
                    if exclude_boxes is not None:
                        for coord in exclude_boxes:
                            (xl, yl), (xu, yu) = coord
                            if xl < x < xu and yl < y < yu:
                                in_box = True
                                break
                    if not in_box:
                        _candidates.append([x, y])
                        n_obs = len(_candidates)

                    if n_obs == num_obs_points:
                        break

                if n_obs == num_obs_points:
                    break

            # If not enough add more rows and columns (more dense)
            n_rows += 1
            n_cols += 1

        targets = np.asarray(_candidates).squeeze()
        if targets.shape != (num_obs_points, 2):
            print("This should never happen!")
            print("Targets shape: ", targets.shape)
            print("n_obs: ", n_obs)
            print("num_obs_points: ", num_obs_points)
            raise ValueError

    else:
        targets = np.asarray(targets)
        assert np.ndim(targets) == 2, "targets must be a 2D array"
        targets_shape = targets.shape
        assert targets_shape[1] == 2, "targets must be of shape (n, 2)!"
        ntargets = targets_shape[0]

    if verbose:
        msg = "***************Observation Coordinates:***************"
        print(msg)
        print("Number of observation points: %d " % ntargets)
        print("Coordinates:")
        print(targets)
        # print("targets.shape:", targets.shape)  # for debugging
        print("*" * len(msg))

    configs = dict(model=model, targets=targets, Vh=model.state_dof)
    observation_operator = fenics_observation_operators.DolfinPointWiseObservations(
        configs
    )

    return observation_operator, targets


def relaxed_OED_Dolfin_AD_2d(
    num_obs_time_points=6,
    use_FIM=USE_FIM,
    #
    # Reproduction and General Settings
    random_seed=RANDOM_SEED,
    #
    # Inverse Problem Settings
    prior_type="BiLaplacian",
    Laplacian_prior_params=(1, 16),
    #
    obs_oper_type="uniform",
    uniform_obs_grid_size=25,
    observation_error_variance=1e-4,
    obs_error_correlation=False,
    #
    # OED Settings
    solve_by_relaxation=True,
    oed_criterion="Relaxed Bayesian A-opt",
    oed_evaluation_method="randomized",
    penalty_function="l1",
    penalty_weight=0.0,
    budget=None,
    maxiter=30,
    #
    # Tasks to Carry Out
    solve_OED_problem=True,
    validate_IP_gradient=False,
    validate_OED_gradient=False,
    #
    # Verbosity and Output Settings
    verbose=False,
    overwrite_results=True,
    output_dir=os.path.join(get_base_output_dir(), "Dolfin_AD-2D"),
):
    """
    Test Relaxed OED using a 2D Advection-Diffusion model (with Finite Elements discretization)
    """
    # Create a local random number generator from the passed seed
    rng = np.random.default_rng(random_seed)

    # Get the passed configurations
    configurations = locals()
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    configs_file = os.path.join(output_dir, CONFIGS_FILENAME)
    found_match_configs = False
    if os.path.isfile(configs_file):
        _configs = pickle.load(open(configs_file, "rb"))
        if configurations == _configs and not overwrite_results:
            found_match_configs = True
            return None  # TODO: instead of return proceed and load results!
        else:
            print(
                "Found file but the configurations are different than passed ones; will"
                " overwrite"
            )
            if overwrite_results is not True:
                overwrite_results = True
    else:
        print(f"Didn't find configurations file: {configs_file}")
        print("Running experiments...")

    # proceed
    pickle.dump(configurations, open(configs_file, "wb"))

    # ------------------------------------------------------------------------
    # Create the simulation model; Instantiate the Dolfin 2D AD model object,
    #    and extract true/reference initial condition and othe model entities
    # ------------------------------------------------------------------------
    model_time_step = 1.0
    model = fenics_models.create_AdvectionDiffusion2D_model(
        dt=model_time_step,
        output_dir=output_dir,
    )
    true_model_state = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # --------------------------------------
    # Create prior model and update its mean
    # --------------------------------------
    prior_mean = model.parameter_vector(init_val=0.0)

    Vh = model.parameter_dof
    if re.match(r"\AGaussian\Z", prior_type, re.IGNORECASE):
        configs = dict(
            mean=prior_mean,
            variance=prior_variance,
            random_seed=random_seed,
        )
        prior = Gaussian.GaussianErrorModel(configs)

    elif re.match(r"\ALaplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinLaplacianErrorModel(configs)

    elif re.match(r"\ABi(| |-|_)*Laplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinBiLaplacianErrorModel(configs)

    else:
        print("Unrecognized Prior Type")
        raise ValueError

    # ---------------------------------------------------
    # Create Observation grid and an Observation operator
    # ---------------------------------------------------
    obs_oper_type = obs_oper_type.strip().lower()
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(configs=dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        observation_operator, observation_grid = create_dolfin_observation_operator(
            model, num_obs_points=uniform_obs_grid_size
        )

    else:
        print(f"Unsupporrted observation operator type: '{obs_oper_type}'")
        raise ValueError
    observation_grid = observation_operator.get_observation_grid()

    # --------------------------------------------
    # Create an observation error model (Gaussian)
    # --------------------------------------------
    observation_size = observation_grid.shape[0]  # Observation vector size
    obs_err_cov = utility.create_covariance_matrix(
        size=observation_size,
        corr=obs_error_correlation,
        stdev=np.sqrt(observation_error_variance),
    )
    obs_err_configs = dict(
        size=observation_size,
        mean=0.0,
        variance=obs_err_cov,
        random_seed=random_seed,
    )
    obs_err_model = Gaussian.PrePostWeightedGaussianErrorModel(configs=obs_err_configs)

    # -------------------------------------------------------------
    # Create an inverse problem (here use FourDVar/Smoother), then:
    #   - Create and assign noisy/synthetic observations
    # -------------------------------------------------------------
    checkpoints = np.arange(
        0, model_time_step * (num_obs_time_points + 0.5), model_time_step
    )
    fdvar_configs = dict(
        window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior=prior,
        observation_operator=observation_operator,
        observation_error_model=obs_err_model,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=fdvar_configs)

    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[1:],
    )
    # perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)

    # ---------------------------
    # Create relaxed OED problem:
    # ---------------------------
    # Create Relaxed OED problem with kernel generated above
    oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            inverse_problem=inverse_problem,
            problem_is_linear=True,
            optimizer="ScipyOptimizer",
            optimizer_configs={"bounds": (0, 1)},
            output_dir=output_dir,
        )
    )

    # Register The optimality criterion
    oed_problem.register_optimality_criterion(oed_criterion)
    oed_problem.oed_criterion.update_configurations(
        use_FIM=use_FIM,
        evaluation_method=oed_evaluation_method,
        **OED_RANDOMIZATION_SETTINGS,
    )

    # Create (and register) an optional penalty function (with associated gradient; for relaxation approach)
    penalty_function, penalty_function_gradient = create_penalty_function(
        name=penalty_function,
        budget=budget,
    )
    oed_problem.register_penalty_term(
        penalty_function=penalty_function,
        penalty_function_gradient=penalty_function_gradient,
        penalty_weight=penalty_weight,
    )

    # ======================================
    # Start carrying out the requested tasks
    # ======================================

    # ----------------------------------
    # Test the 4D-Var objective gradient
    # ----------------------------------
    if validate_IP_gradient:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            inverse_problem,
            eval_at=prior.mean,
            use_noise=True,
            validate_gradient=True,
            validate_Hessian=True,
            create_plots=True,
            output_dir=output_dir,
        )

    # ----------------------------------
    # Test the OED objective gradient
    # ----------------------------------
    if validate_OED_gradient:
        print("Validating OED problem gradients...")

        print("Derivative w.r.t uncertain parameter...")
        grad_fun = lambda d, p: oed_problem.oed_objective_grad_param(
            d, uncertain_param_val=p, fd_validation=True, fd_eps=1e-9
        )
        design = oed_problem.inverse_problem.observation_error_model.design
        designs_pool = [np.ones_like(design), rng.choice([0, 1], design.size)]
        param_pool, _ = uncertain_param_sampler.sample(sample_size=2)
        for d in designs_pool:
            for p in param_pool:
                grad_fun(d, p)

        if solve_by_relaxation:
            print("Derivative w.r.t relaxed design...")
            grad_fun = lambda d, p: oed_problem.oed_objective_grad_design(
                d,
                uncertain_param_val=p,
                fd_validation=True,
                fd_central=False,
                fd_eps=1e-9,
            )
            design = oed_problem.inverse_problem.observation_error_model.design
            for i in range(3):
                # Make sure design is inside the open interval (0, 1) for FD to be applicable
                design = rng.random(design.size)
                lb = 0.01
                ub = 0.99
                design[design < lb] = lb
                design[design > ub] = ub
                for p in param_pool:
                    grad_fun(design, p)

    # ----------------------------------------
    # Solve the stochastic binary OED problem
    # ----------------------------------------
    # TODO: Proceed here with debugging (Need to debug stochastic optimization's Polyak, OED code, objectives, gradients, etc.)
    if solve_OED_problem:
        # Solve the OED problem, and plot results as needed
        oed_results = oed_problem.solve()

        # Pickle/Write Results, and plot
        saveto = os.path.join(output_dir, "oed_results.pcl")
        oed_results.write_results(saveto=saveto)
        try:
            oed_results.plot_results(output_dir=output_dir)
        except:
            print("Failed to plot OED results...")

    else:
        oed_results = None

    return oed_problem, oed_results  # for debugging


def main(test):
    """
    Choose the test (4D-var data assimilation for a selected simulation mdoel)
        to run and call the proper function from this module

    :arg str test: name of the test to run; supported names are:
      - 'Toy' :  One-dimensional (1D) toy linear time dependent  model
      - 'D-AD2' : Dolfin-based Two-dimensional (2D) Advection-Diffusion (AD) model

    :raises: ValueError is raised if the passed `test` value is not recognized
    """
    if re.match(r"\AToy\Z", test, re.IGNORECASE):
        # 1D AD model test
        print("\nTesting the Toy Lienar (time-dependent) simulation model")
        relaxed_OED_time_dependent_toy_linear()

    elif re.match(r"\AD(_|-|)*AD(_|-| )*2\Z", test, re.IGNORECASE):
        # Dolfin-based 2D AD model test
        print(
            "\nTesting the Dolfin-based 2D Advection-Diffusion Model with default"
            " settings"
        )
        relaxed_OED_Dolfin_AD_2d()

    else:
        print("Unrecognized model/test-case: '{0}'!".format(test))
        raise ValueError


if __name__ == "__main__":
    # Select the model you want to test --- go up to change configuration as needed
    msg = """Available simulation models to use:
             \r - 'Toy':  Toy linear model
             \r - 'D-AD2': Dolfin-based Two-dimensional (2D) Advection-Diffusion (AD) model\n
             \rInput/Type the model you want to test: """
    test = input(msg)
    main(test=test)
