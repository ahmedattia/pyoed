# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides several experiments and examples explaining how robust OED
can be applied to Bayesian inverse problems under PyOED.
"""

# Essential imports
import os
import sys
import re
import numpy as np
import pickle
import matplotlib.pyplot as plt

# Add PyOED root path for proper access
# pyoed_path = os.path.abspath("../../../../")
# if not pyoed_path in sys.path:
#     sys.path.append(pyoed_path)


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility
OED_RANDOMIZATION_SETTINGS = {
    "sample_size": 32,
    "optimize_sample_size": False,
    "min_sample_size": 10,
    "max_sample_size": 100,
    "distribution": "Rademacher",
    "randomization_vectors": None,
    "random_seed": None,
    "accuracy": 1e-2,
}


# Import PyOED  modules (model, observation operator, error models)
import pyoed
from pyoed import utility

# simulation model(s)
from pyoed.models.simulation_models import (
    toy_linear,
)
from pyoed.models.simulation_models import fenics_models
from pyoed.models.observation_operators import fenics_observation_operators

# observation operators(s)
from pyoed.models.observation_operators import (
    identity,
    interpolation,
)

# error models
from pyoed.models.error_models import (
    Gaussian,
    Laplacian,
)

# data assimilation (inversion)
from pyoed.assimilation.smoothing import fourDVar

# from pyoed.assimilation import reduced_order_modeling

# optimal experimental design (OED)
from pyoed.oed import (
    robust_oed,
    utility_functions as uf,
)


# General Settings
CONFIGS_FILENAME = "settings.pcl"
OED_RESULTS_FILENAME = "robust_oed_results.pcl"
BRUREFORCE_RESULTS_FILENAME = "bruteforce_oed_results.pcl"
EXHAUSTIVE_SEARCH_RESULTS_FILENAME = "exhaustive_search_robust_oed_results.pcl"


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "OED/Robust_OED")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


def create_problem_from_settings(settings_file):
    """"""
    try:
        settings = pickle.load(open(settings_file, "rb"))
        settings["solve_OED_problem"] = False
        settings["overwrite"] = True

    except:
        print(f"Failed to load settings from: '{settings_file}'!")
        print(f"Nothing to be done here!")
        return

    model_name = settings["model_name"]
    if re.match(r"\AToy-Linear-Time-Dependent\Z", model_name, re.IGNORECASE):
        creator = robust_OED_time_dependent_toy_linear

    elif re.match(r"\AAD-Dolfin-2D\Z", model_name, re.IGNORECASE):
        creator = robust_OED_Dolfin_AD_2d

    else:
        print(f"Unrecognized simulation model name '{model_name}' !")
        raise ValueError

    output_dir = os.path.dirname(settings_file)
    if output_dir != settings["output_dir"]:
        settings.update({"output_dir": output_dir})  # if results were moved around
    oed_problem, _, _, _ = creator(**settings)

    return oed_problem


def plot_robust_oed_results_2d_from_file(
    settings_file=None,
    output_dir=None,
    recursive=False,
):
    """
    Load everthing from results files and plot 2D plots; wrapper around 'twoD_oed_plotter'

    param output_dir: location of the results (output direcotry)
    """
    if settings_file is not None:
        output_dir = os.path.dirname(settings_file)
        recursive = False

    elif output_dir is not None:
        # Find all settings in this directory; or recursively if recursive is True
        all_files = utility.get_list_of_files(
            output_dir,
            recursive=recursive,
            ignore_special_dirs=False,
            ignore_special_files=False,
        )
        settings_files = []
        for f in all_files:
            _, f_name = os.path.split(f)
            if re.match(r"\A%s\Z" % (CONFIGS_FILENAME), f_name, re.IGNORECASE):
                settings_files.append(f)

        for f in settings_files:
            # Recurse
            plot_robust_oed_results_2d_from_file(
                settings_file=f, output_dir=os.path.dirname(f)
            )

    else:
        print(
            f"Wrong combination of arguments; either pass valid settings file or output dir"
        )
        raise ValueError

    if not os.path.isdir(output_dir):
        print("The passed directory does not exist!")
        raise IOError

    # Load settings (to recreate OED problem
    if settings_file is None and output_dir is not None:
        settings_file = os.path.join(output_dir, CONFIGS_FILENAME)
    try:
        settings = pickle.load(open(settings_file, "rb"))
    except:
        print(f"Failed to load settings from: '{settings_file}'!")
        print(f"Passed output_dir: '{output_dir}'!")
        print(f"Nothing to be done here!")
        return

    # Load oed_results
    oed_results_file = os.path.join(output_dir, OED_RESULTS_FILENAME)
    try:
        oed_results = pickle.load(open(oed_results_file, "rb"))
        run_exp = False
        settings["solve_OED_problem"] = False
        settings["overwrite"] = True
    except:
        print(f"Failed to load oed results from: '{oed_results_file}'!")
        print(f"Will resolve the OED problem!")
        settings["solve_OED_problem"] = True
        settings["overwrite"] = True

    # Load exhaustive search results (recreate if does not exist!)
    exhaustive_search_results_file = os.path.join(
        output_dir, EXHAUSTIVE_SEARCH_RESULTS_FILENAME
    )
    try:
        exhaustive_search_results = pickle.load(
            open(exhaustive_search_results_file, "rb")
        )
        settings["exhaustive_plots"] = False
    except:
        exhaustive_search_results = None
        print(
            f"Failed to load exhaustive_search results from: '{exhaustive_search_results_file}'!"
        )
        print(f"Will recreate!")
        settings["exhaustive_plots"] = True

    # Create OED object
    model_name = settings["model_name"]
    if re.match(r"\AToy-Linear-Time-Dependent\Z", model_name, re.IGNORECASE):
        creator = robust_OED_time_dependent_toy_linear

    elif re.match(r"\AAD-Dolfin-2D\Z", model_name, re.IGNORECASE):
        creator = robust_OED_Dolfin_AD_2d
    else:
        print(f"Unrecognized simulation model name '{model_name}' !")
        raise ValueError

    out = creator(**settings)
    if out is not None:
        oed_problem, _oed_results, _, _exhaustive_search_results = out
        if oed_results is None:
            oed_results = _oed_results

    if exhaustive_search_results is None:
        exhaustive_search_results = _exhaustive_search_results

    # Call the plotter
    robust_oed.plot_results(
        oed_results=oed_results,
        exhaustive_search_results=exhaustive_search_results,
        output_dir=output_dir,
    )


def plot_AD_settings(settings_file, output_dir=None):
    """
    Given settings file (created by calling 'run_two_dimensional_experiments' for AD model),
        create problem, model and use them to create experimental setup of the problem
        (model grid, observation sensors, etc.)
    """
    if not os.path.isfile(settings_file):
        print(f"The settings file is not found! {settings_file}")
        raise IOError

    if output_dir is None:
        output_dir = os.path.join(os.path.dirname(settings_file), "Setup_Plots")

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    oed_problem = create_problem_from_settings(
        settings_file=settings_file,
    )

    model = oed_problem.inverse_problem.model
    model_name = model.configurations["model_name"].lower().strip()

    if not ("ad" in model_name and "dolfin" in model_name and "2d" in model_name):
        print("This function plots settings of 2D AD model")
        raise ValueError

    mesh_file = model.configurations["mesh_configs"]["mesh_file"]
    if os.path.split(mesh_file)[1] == "ad_20.xml":
        boxes_coords = fenics_models.advection - diffusion._AD_20__BUILDINGS_COORDINATES
        boxes_texts = [r"B1", r"B2"]
    else:
        boxes_texts = boxes_coords = None
    sensors_coords = (
        oed_problem.inverse_problem.observation_operator.get_observation_grid()
    )

    # Plot velocity field (without grid)
    f_name = "AD_2D_Velocity_NoGrid.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_velocity_field(
        show_mesh=False,
        mesh_alpha=0.25,
        show_axis_frame=False,
        save_to_file=save_to_file,
    )

    # Plot velocity field (with grid)
    f_name = "AD_2D_Velocity_WithGrid.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_velocity_field(
        show_mesh=True,
        mesh_alpha=0.25,
        show_axis_frame=False,
        save_to_file=save_to_file,
    )

    # Plot velocity field (with grid and Buildings)
    f_name = "AD_2D_Velocity_WithGrid_and_Buildings.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_velocity_field(
        show_mesh=True,
        mesh_alpha=0.25,
        show_axis_frame=False,
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        save_to_file=save_to_file,
    )

    # Plot model domain (along with buildings)
    f_name = "AD_2D_Domain.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_domain(
        boxes_coordinates=None,
        boxes_texts=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        save_to_file=save_to_file,
    )

    # Plot model domain (along with buildings)
    f_name = "AD_2D_Domain_withBuildings.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_domain(
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        sensors_coordinates=None,
        annotate_sensors=False,
        save_to_file=save_to_file,
    )

    # Plot mdoel domain with sensors
    f_name = "AD_2D_Domain_WithSensors.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_domain(
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        sensors_coordinates=sensors_coords,
        annotate_sensors=True,
        save_to_file=save_to_file,
    )

    # Plot sensor locations without grid (mesh_alpha = 0)
    f_name = "AD_2D_Sensors.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_domain(
        boxes_coordinates=boxes_coords,
        show_mesh=False,
        show_axis_frame=True,
        boxes_texts=boxes_texts,
        sensors_coordinates=sensors_coords,
        annotate_sensors=True,
        save_to_file=save_to_file,
    )

    #
    # Plot model True initial condition
    true_ic = model.create_initial_condition()

    # Plot true initial condition (without grid)
    f_name = "AD_2D_TrueIC.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_state(
        true_ic,
        show_mesh=False,
        mesh_alpha=0.5,
        add_colorbar=True,
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        boxes_hatch=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        show_axis_frame=False,
        return_axis=False,
        save_to_file=save_to_file,
    )

    # Plot true initial condition (with grid)
    f_name = "AD_2D_TrueIC_WithGrid.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_state(
        true_ic,
        show_mesh=True,
        mesh_alpha=0.5,
        add_colorbar=True,
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        boxes_hatch=None,
        sensors_coordinates=None,
        annotate_sensors=False,
        show_axis_frame=False,
        return_axis=False,
        save_to_file=save_to_file,
    )

    # Plot true initial condition (with grid and sensors)
    f_name = "AD_2D_TrueIC_WithGrid_and_Sensors.pdf"
    save_to_file = os.path.join(output_dir, f_name)
    model.plot_state(
        true_ic,
        show_mesh=True,
        mesh_alpha=0.5,
        add_colorbar=True,
        boxes_coordinates=boxes_coords,
        boxes_texts=boxes_texts,
        boxes_hatch=None,
        sensors_coordinates=sensors_coords,
        annotate_sensors=True,
        show_axis_frame=False,
        return_axis=False,
        save_to_file=save_to_file,
    )

    plt.close("all")


def plot_robust_oed_results(
    settings_file=None,
    output_dir=None,
    recursive=False,
    plot_setup=True,
    overwrite=True,
):
    """ """
    if settings_file is not None:
        output_dir = os.path.dirname(settings_file)
        oed_results_file = os.path.join(output_dir, OED_RESULTS_FILENAME)
        exhaustive_search_results_file = os.path.join(
            output_dir, EXHAUSTIVE_SEARCH_RESULTS_FILENAME
        )

        if not os.path.isfile(oed_results_file):
            print(f"Couldn't find OED results file: {oed_results_file}")
            return
        else:
            print(f"Plotting results for experimental configs: '{settings_file}'")

        if plot_setup:
            plot_AD_settings(settings_file)

        # Create OED problem instance and Load OED results
        oed_problem = create_problem_from_settings(settings_file)
        oed_results = oed_problem.load_results(oed_results_file)
        robust_oed.plot_results(oed_results, output_dir=output_dir, overwrite=overwrite)

    elif output_dir is not None:
        # Find all settings in this directory; or recursively if recursive is True
        all_files = utility.get_list_of_files(
            output_dir,
            recursive=recursive,
            ignore_special_dirs=False,
            ignore_special_files=False,
        )
        settings_files = []
        for f in all_files:
            _, f_name = os.path.split(f)
            if re.match(r"\A%s\Z" % (CONFIGS_FILENAME), f_name, re.IGNORECASE):
                settings_files.append(f)

        for f in settings_files:
            # Recurse
            plot_robust_oed_results(settings_file=f, output_dir=os.path.dirname(f))

    else:
        print(
            f"Wrong combination of arguments; either pass valid settings file or output dir"
        )
        print(f"Nothing to be done here!!!")


def create_penalty_function(name="l0", input_size=None, budget=None):
    """
    Create two handles to evaluate the value and the gradient of
    a penalty term (for regularization/sparsification) in OED optimization problems.
    The function takes the form ::math:`f(d)` where ::math:`d` is a design vector

    :param str name: name of the penalty function
        - 'l0': number of nonzero entries in the
        - 'l1'

    :returns: two functions to evaluate the penalty term and associated gradient, respectively.
        If the function is non-differentiable, e.g., 'l0' penalty, the handle to the gradient is set to `None`.
    """
    if budget is None:
        budget = 0
    if re.match(r"\Al(_| |-)*0\Z", name, re.IGNORECASE):

        def penalty_function(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return np.abs(np.count_nonzero(d) - budget)

        penalty_function_gradient = None

    elif re.match(r"\Al(_| |-)*1\Z", name, re.IGNORECASE):

        def penalty_function(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return np.power(np.sum(d) - budget, 2)

        def penalty_function_gradient(d):
            d = np.asarray(d).flatten()
            if input_size is not None:
                assert (
                    d.size == input_size
                ), "input size mismatches the predefined size!!"
            assert np.all(d >= 0), "The design can not be negative!"
            return 2.0 * (np.sum(d) - budget) * np.ones_like(d)

    else:
        print(f"Unsupported penalty function name '{name}'")
        raise ValueError

    return penalty_function, penalty_function_gradient


def robust_OED_time_dependent_toy_linear(
    model_state_size=4,
    num_obs_time_points=5,
    #
    # Inverse Problem Settings
    prior_variance=1,
    prior_correlation=False,
    #
    obs_oper_type="uniform",
    uniform_obs_grid_size=2,
    observation_error_variance=0.25,
    obs_error_correlation=False,
    #
    # OED Settings
    solve_by_relaxation=False,
    oed_criterion="Robust Bayesian A-opt",
    oed_evaluation_method="exact",
    penalty_function="l1",
    penalty_weight=0,  # Make large if budget is not None
    budget=None,
    #
    # Binary Stochastic OED Settings
    stoch_grad_batch_size=32,
    stoch_learning_rate=0.25,
    stoch_maxiter=100,  # inexactly solve the outer problem
    stoch_baseline="optimal",
    maxiter=20,
    #
    # Robust OED Settings
    uncertain_parameter="observation-variance",
    uncertain_parameter_kernel="diagonal",
    uncertain_parameter_nominal_value=None,
    prior_variance_bounds=[(0.25, 3.25)],
    observation_error_variance_bounds=[(0.25, 0.49), (0.36, 0.64)],
    #
    # Tasks to Carry Out
    solve_OED_problem=True,
    solve_inverse_problem=False,
    bruteforce=True,
    bruteforce_with_MC=False,
    exhaustive_plots=True,
    #
    # Reproduction and General Settings
    random_seed=RANDOM_SEED,
    #
    # Verbosity and Output Settings
    verbose=False,
    overwrite=True,
    model_name="Toy-Linear-Time-Dependent",
    output_dir=os.path.join(get_base_output_dir(), "TimeDependentToyLinearModel"),
):
    """
    A driver that enables testing/experimenting with robust optimal experimental design
    (Robust OED) with an inverse problem that involves a toy linear inverse problem. The
    linear inverse problem consists of a Toy linear forward model on the form
    :math:`x_{t_{n+1}} = A x_{t_n}` where :math:`x_{t_n}` is the model state at time
    :math:`t_n`.

    Since the inverse problem is linear, we can form a Gaussian posterior exactly and
    test things properly and efficiently.

    Observation is carried out by interpolation; that is, the model output is
    interpolated on the observation grid. Observations are obtained by perturbing the
    model output by Gaussian noise (with/without) correlations based on the experiments
    setup.

    :param model_state_size:
    :param num_obs_time_points:
    :param checkpoints:
    :param random_seed:
    :param prior_variance:
    :param bool prior_correlations: if `True` the prior covariance matrix is initially
        correlated, otherwise a diagonal matrix is created. If the uncertain parameter
        is prior covariance, this is overriden by the specification of the uncertainty
        kernel.
    :param obs_operator: Type of the observation operator to create here; the following
        are supported in this driver:\n

            - 'identity': observe all model state vectors; in this case the number of
              sensors = number of observation points = number of model grid points. In
              this case, the `uniform_obs_grid_size` argument is ignored as the
              observation (and design) size is equal to `model_state_size`
            - 'uniform': distribute the grid points (number of grid points is defined by
              `uniform_obs_grid_size` argument below) Thus, the design size is equal to
              `uniform_obs_grid_size`, and interpolation is carried out for observation.
    :param bool uniform_obs_grid_size:
    :param observation_error_variance:
    :param bool obs_error_correlation: if `True` the observation error covariances are
        initially correlated, otherwise a diagonal matrix is created. If the uncertain
        parameter is observation covariance, this is overriden by the specification of
        the uncertainty kernel.
    :param solve_by_relaxation: if `False` a binary OED (stochastic) formulation is
        considered, othrewise the relaxation approach is followed
    :param str oed_criterion:
    :param str penalty_function: name of the penalty function to use. Either 'l0' or
        'l1'.
    :param budget: used in combination with `penalty_function` to define the penalty
        term
    :param penalty_weight: the penalty parameter of the OED optimization problem (for
        soft-constraint)
    :param stoch_grad_batch_size:
    :param stoch_learning_rate:
    :param stoch_maxiter:
    :param stoch_baseline:
    :param maxiter:
    :param uncertain_parameter: the name of the uncertain parameter; the following
        values are supported

        - 'prior-variance': the uncertain parameter is the variances of the prior model
        - 'observation-variance': the uncertain parameter is the observation error
          variances

        In all cases, the shape of the covariances is defined by the
        `uncertain_parameter_kernel` argument
    :param uncertain_parameter_kernel: name of the kernel used to create a generator for
        the uncertain parameter. These are passed to `robust_oed.CovarianceSampler`, and
        the supported values are:

            - 'constant': in this case, the covariance kernel is ::math:`a\\mathbf{I}`
              where ::math:'a' is a scalar and ::math:`\\mathbf{I}` is an idenity.
            - 'diagonal': in this case, the covariance kernel is
              :math:`Diag{\\mathbf{a}}` where :math:'\\mathbf{a}' is a vector defining
              variances is an idenity.
            - 'banded-1': in this case, the covariance kernel is a banded (correlated)
              covariance matrix; see :py:meth:`robust_oed.CovarianceSampler`
    :param observation_error_variance_bounds:
    :param bool solve_OED_problem:
    :param solve_inverse_problem: After solving the OED problem (if `solve_OED_problem`
        is `True`), solve the inverse problem using the resulting optimal design. If set
        to `False`, or `solve_OED_problem` is `False` this is ignored.
    :param bruteforce:
    :param exhaustive_plots:
    :param output_dir:

    :returns: Three entities are returned for further testing (nothing is returned if
        configurations exist and overwrite is False); these are:

        - `oed_problem`:
        - `oed_results`:
        - `exhaustive_search_results`:
    """
    # Get the passed configurations
    configurations = locals()
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    configs_file = os.path.join(output_dir, CONFIGS_FILENAME)
    found_match_configs = False
    if os.path.isfile(configs_file):
        _configs = pickle.load(open(configs_file, "rb"))
        if configurations == _configs and not overwrite:
            found_match_configs = True
            return None  # TODO: instead of return proceed and load results!
        else:
            print(
                "Found file but the configurations are different than passed ones; will overwrite"
            )
            if overwrite is not True:
                overwrite = True
    else:
        print(f"Didn't find configurations file: {configs_file}")
        print("Running experiments...")

    # proceed
    if not found_match_configs:
        print(f"Dumping configurations to: {configs_file}")
        pickle.dump(configurations, open(configs_file, "wb"))

    # Create a local random number generator from the passed seed
    rng = np.random.default_rng(random_seed)

    # ------------------------------------------------------------------------
    # CREATE THE SIMULATION MODEL; Instantiate the toy linear model object,
    #    and extract true/reference initial condition and othe model entities
    # ------------------------------------------------------------------------
    model_time_step = 1.0
    model_configs = dict(
        nx=model_state_size,
        dt=model_time_step,
    )
    model = toy_linear.ToyLinearTimeDependent(configs=model_configs)
    true_model_state = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # -------------------------------------------------
    # Create prior model for the model state (Gaussian)
    # -------------------------------------------------
    prior_covmat = utility.create_covariance_matrix(
        size=model_state_size,
        corr=prior_correlation,
        stdev=np.sqrt(prior_variance),
    )
    prior_configs = dict(
        size=model_state_size,
        mean=0.0,
        variance=prior_covmat,
        random_seed=random_seed,
    )
    prior = Gaussian.GaussianErrorModel(
        configs=prior_configs,
    )
    prior.mean = prior.add_noise(true_model_state, in_place=False)

    # ---------------------------------------------------
    # Create Observation grid and an Observation operator
    # ---------------------------------------------------
    obs_oper_type = obs_oper_type.strip().lower()
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid.copy()
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed grid; interpolation operator is used
        observation_grid = np.linspace(
            0.5, model_state_size - 1.5, uniform_obs_grid_size
        )
        obs_configs = dict(
            model_grid=model_grid,
            observation_grid=observation_grid,
        )
        observation_operator = interpolation.CartesianInterpolator(obs_configs)

    else:
        print(f"The observation operator '{obs_oper_type}' is not supported!")
        raise ValueError

    # --------------------------------------------
    # Create an observation error model (Gaussian)
    # --------------------------------------------
    observation_size = observation_grid.size  # Observation vector size
    obs_err_cov = utility.create_covariance_matrix(
        size=observation_size,
        corr=obs_error_correlation,
        stdev=np.sqrt(observation_error_variance),
    )
    obs_err_configs = dict(
        size=observation_size,
        mean=0.0,
        variance=obs_err_cov,
        random_seed=random_seed,
    )
    obs_err_model = Gaussian.GaussianErrorModel(configs=obs_err_configs)

    # -------------------------------------------------------------
    # Create an inverse problem (here use FourDVar/Smoother), then:
    #   - Create and assign noisy/synthetic observations
    # -------------------------------------------------------------
    checkpoints = np.arange(
        0, model_time_step * (num_obs_time_points + 0.5), model_time_step
    )
    fdvar_configs = dict(
        window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior=prior,
        observation_operator=observation_operator,
        observation_error_model=obs_err_model,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=fdvar_configs)

    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[1:],
    )
    # perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)

    # -------------------------------------------------------------------------------
    # Specify the uncertain parameter (for robust OED) and Create robust OED problem
    # -------------------------------------------------------------------------------

    # Specify the uncertain parameter configurations
    if re.match(r"\Aprior(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE):
        parameter_size = model_state_size
        hyperparameter_def_value = np.sqrt(prior_variance)
        hyperparameter_bounds = (
            None
            if prior_variance_bounds is None
            else [np.sqrt(v) for v in prior_variance_bounds]
        )

    elif re.match(
        r"\Aobservation(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE
    ):
        parameter_size = observation_size
        hyperparameter_def_value = np.sqrt(observation_error_variance)
        hyperparameter_bounds = (
            None
            if observation_error_variance_bounds is None
            else [np.sqrt(v) for v in observation_error_variance_bounds]
        )

    else:
        print("Unsupported uncertain parameter {0}".format(uncertain_parameter))
        raise ValueError

    # Create the variance/covariance sampler
    configs = dict(
        kernel=uncertain_parameter_kernel,
        size=parameter_size,
        hyperparameter_def_value=hyperparameter_def_value,
        hyperparameter_bounds=hyperparameter_bounds,
    )
    uncertain_parameter_sampler = robust_oed.CovarianceSampler(
        configs=configs,
    )

    # Discretize the grid of the uncertain parameter (for Max-Min estimate with Monte Carlo)
    if bruteforce and bruteforce_with_MC:
        print("Discretizing the domain of the uncertain parameter", end=" ")
        if uncertain_parameter_sampler.hyperparameter_bounds is None:
            print(
                f"No bounds found in the uncertain parameter sampler configurations! "
                f"Cannot do MC-based bruteforce without uncertain parameter sampler bounds."
            )
            uncertain_parameter_sample = None
        else:
            uncertain_parameter_meshgrid = [
                np.linspace(lb, ub)
                for lb, ub in uncertain_parameter_sampler.hyperparameter_bounds
            ]
            uncertain_parameter_meshgrid = np.meshgrid(
                *uncertain_parameter_meshgrid, indexing="ij"
            )
            uncertain_parameter_sample = np.vstack(
                [v.flatten() for v in uncertain_parameter_meshgrid]
            ).T
        print("Done!")
    else:
        uncertain_parameter_sample = None

    # Create Robust OED problem with kernel generated above
    uparam_configs = dict(
        parameter=uncertain_parameter,
        sampler=uncertain_parameter_sampler,
    )

    optimizer_configs = dict(
        maxiter=maxiter,
        outer_opt_maxiter=stoch_maxiter,
        solve_by_relaxation=solve_by_relaxation,
        baseline=stoch_baseline,
        stochastic_gradient_sample_size=stoch_grad_batch_size,
        def_stepsize=stoch_learning_rate,
        verbose=verbose,
        random_seed=random_seed,
    )
    oed_problem = robust_oed.RobustSensorPlacementBayesianInversionOED(
        configs=dict(
            inverse_problem=inverse_problem,
            optimizer_configs=optimizer_configs,
            uncertain_parameter=uncertain_parameter,
            uncertain_parameter_sampler=uncertain_parameter_sampler,
            verbose=verbose,
        )
    )

    # Register The optimality criterion and the OED evaluation method
    criterion_configs = {
        "oed_problem": oed_problem,
        "evaluation_method": oed_evaluation_method,
        **OED_RANDOMIZATION_SETTINGS,
    }
    oed_criterion = uf.find_criteria(
        oed_criterion,
    )(criterion_configs)
    oed_problem.register_optimality_criterion(oed_criterion)

    # Create (and register) an optional penalty function (with associated gradient; for relaxation approach)
    if False:
        penalty_function, penalty_function_gradient = create_penalty_function(
            name=penalty_function,
            budget=budget,
        )
        oed_problem.register_penalty_term(
            penalty_function=penalty_function,
            penalty_function_gradient=penalty_function_gradient,
            penalty_weight=penalty_weight,
        )

    # ======================================
    # Start carrying out the requested tasks
    # ======================================

    # ----------------------------------------
    # Solve the stochastic binary OED problem
    # ----------------------------------------
    # TODO: Proceed here with debugging (Need to debug stochastic optimization's Polyak, OED code, objectives, gradients, etc.)
    if solve_OED_problem:
        msg = "=" * 50 + "\n"
        msg += "***\nModel Grid:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.model.get_model_grid()
        )
        msg += "***\nObservation Grid:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_operator.get_observation_grid()
        )
        msg += "***\nForward Operator:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.model.get_model_array()
        )
        msg += "***\nPrior Covariance Matrix:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.prior.covariance_matrix()
        )
        msg += "***\nObservation Error Covariance Matrix:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_error_model.covariance_matrix()
        )
        msg += "***\nAssimilation Window:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.window
        )
        msg += "***\nObservation Time Points:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_times
        )
        msg += "=" * 50 + "\n\n"
        print(msg)

        with open(os.path.join(output_dir, "settings.txt"), "w") as f_id:
            f_id.write(msg)

        # Solve the OED problem, and plot results as needed
        oed_results = oed_problem.solve()
        if solve_inverse_problem:
            oed_problem.design = oed_results.optimal_design
            inverse_problem_results = oed_problem.solve_inverse_problem()
        else:
            inverse_problem_results = None

        if bruteforce:
            saveto = os.path.join(output_dir, BRUREFORCE_RESULTS_FILENAME)
            bruteforce_results = oed_problem.optimizer.update_bruteforce_results(
                bruteforce_results={
                    "uncertain_parameter_sample": uncertain_parameter_sample,
                },
                saveto=saveto,
            )
        else:
            bruteforce_results = None

        # Pickle/Write Results, and plot
        saveto = os.path.join(output_dir, OED_RESULTS_FILENAME)
        oed_results.write_results(saveto=saveto)

        # 2D exhaustive search
        if exhaustive_plots:
            saveto = os.path.join(output_dir, EXHAUSTIVE_SEARCH_RESULTS_FILENAME)
            exhaustive_parameter_search_results = (
                oed_problem.optimizer.exhaustive_parameter_search(
                    uncertain_parameter_sample=uncertain_parameter_sample,
                    saveto=saveto,
                )
            )
        else:
            exhaustive_parameter_search_results = None

        try:
            # oed_results.plot_results(output_dir=output_dir)
            oed_problem.plot_results(
                oed_results,
                bruteforce_results=bruteforce_results,
                exhaustive_parameter_search_results=exhaustive_parameter_search_results,
                output_dir=output_dir,
                overwrite=overwrite,
            )
        except Exception as err:
            print(f"Failed to plot OED results\n" f"Unexpected {err=} of {type(err)=}")
            raise

    else:
        oed_results = exhaustive_parameter_search_results = None

    if uncertain_parameter_nominal_value is not None:
        saveto = os.path.join(output_dir, "nominal_value_results.pcl")
        nominal_value_results = oed_problem.nominal_value_test(
            uncertain_parameter_nominal_value,
            learning_rate=stoch_learning_rate,
            stoch_baseline=stoch_baseline,
            stoch_maxiter=stoch_maxiter,
            batch_size=stoch_grad_batch_size,
            robust_oed_results=oed_results,
            saveto=saveto,
        )

    else:
        nominal_value_results = None

    return (
        oed_problem,
        oed_results,
        inverse_problem_results,
        exhaustive_parameter_search_results,
    )  # for debugging


# TODO:
#   - Do bruteforce with uncertain parameter sample created by MC by using:
#     a meshgrid over `uniform_obs_grid_size` and `observation_error_variance_bounds`.


def robust_OED_Dolfin_AD_2d(
    init_time=0.0,
    num_obs_time_points=15,
    #
    # Inverse Problem Settings
    prior_type="BiLaplacian",
    Laplacian_prior_params=(1, 16),
    #
    obs_oper_type="uniform",
    uniform_obs_grid_size=2,
    observation_error_variance=1e-4,
    obs_error_correlation=False,
    #
    # OED Settings
    solve_by_relaxation=False,
    oed_criterion="Robust Bayesian A-opt",
    oed_evaluation_method="randomized",
    penalty_function="l1",
    penalty_weight=0.0,
    budget=None,
    #
    # Binary Stochastic OED Settings
    stoch_grad_batch_size=32,
    stoch_learning_rate=0.25,
    stoch_maxiter=100,  # inexactly solve the outer problem
    stoch_baseline="optimal",
    maxiter=20,
    #
    # Robust OED Settings
    uncertain_parameter="observation-variance",
    uncertain_parameter_kernel="diagonal",
    uncertain_parameter_nominal_value=None,
    prior_variance_bounds=None,
    observation_error_variance_bounds=[(0.02**2, 0.04**2)],
    #
    # Tasks to Carry Out
    solve_OED_problem=True,
    solve_inverse_problem=False,
    bruteforce=True,
    bruteforce_with_MC=False,
    uncertain_parameter_num_disc_points=10,  # Number of iscretization points of the uncertain parameter in each dimension (for MC bruteforce)
    exhaustive_plots=True,
    # Reproduction and General Settings
    random_seed=RANDOM_SEED,
    #
    # Verbosity and Output Settings
    verbose=False,
    overwrite=True,
    model_name="AD-Dolfin-2D",
    output_dir=os.path.join(get_base_output_dir(), "Dolfin_AD-2D"),
):
    """
    Test Robust OED using a 2D Advection-Diffusion model
    (with Finite Elements discretization)
    """
    # Check if exhastive plots are not possible!
    if uniform_obs_grid_size != 2 and exhaustive_plots:
        exhaustive_plots = False

    ## discard keys
    discard_keys = ["verbose", "overwrite", "overwrite_results"]
    # Get the passed configurations/arguments
    configurations = locals()
    _configurations = configurations.copy()
    for key in discard_keys:
        _configurations.pop(key, None)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Check if saved file with matched configurations eist
    configs_file = os.path.abspath(os.path.join(output_dir, CONFIGS_FILENAME))
    found_match_configs = False
    if os.path.isfile(configs_file):
        _configs = pickle.load(open(configs_file, "rb"))
        for key in discard_keys:
            _configs.pop(key, None)
        if _configurations == _configs and not overwrite:
            found_match_configs = True
            print("Results configurations found; will try loading results")

        else:
            print(
                "Found file but the configurations are different than passed ones; will overwrite"
            )
            if overwrite is not True:
                overwrite = True
    else:
        print(
            f"Didn't find configurations file: {configs_file}\n"
            f"Running experiments..."
        )

    # Proceed
    if not found_match_configs:
        print(f"Dumping configurations to: {configs_file}")
        pickle.dump(configurations, open(configs_file, "wb"))

    # Create a local random number generator from the passed seed
    rng = np.random.default_rng(random_seed)

    # ------------------------------------------------------------------------
    # CREATE THE SIMULATION MODEL;
    #    Instantiate the Dolfin 2D AD model object, and
    #    extract true/reference initial condition and othe model entities
    # ------------------------------------------------------------------------
    model_time_step = 1.0
    model = fenics_models.create_AdvectionDiffusion2D_model(
        dt=model_time_step,
        output_dir=output_dir,
    )
    true_model_state = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # --------------------------------------
    # CREATE PRIOR MODEL and update its mean
    # --------------------------------------
    prior_mean = model.parameter_vector(init_val=0.0)

    Vh = model.parameter_dof
    if re.match(r"\AGaussian\Z", prior_type, re.IGNORECASE):
        configs = dict(
            mean=prior_mean,
            variance=prior_variance,
            random_seed=random_seed,
        )
        prior = Gaussian.GaussianErrorModel(configs)

    elif re.match(r"\ALaplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinLaplacianErrorModel(configs)

    elif re.match(r"\ABi(| |-|_)*Laplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinBiLaplacianErrorModel(configs)

    else:
        print("Unrecognized Prior Type")
        raise ValueError

    # ---------------------------------------------------
    # CREATE OBSERVATION GRID and an Observation operator
    # ---------------------------------------------------
    obs_oper_type = obs_oper_type.strip().lower()
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        observation_operator = (
            fenics_observation_operators.create_DolfinPointWise_observation_operator(
                model,
                num_obs_points=uniform_obs_grid_size,
            )
        )
        observation_grid = observation_operator.get_observation_grid()

    else:
        print(f"Unsupporrted observation operator type: '{obs_oper_type}'")
        raise ValueError
    observation_grid = observation_operator.get_observation_grid()

    # --------------------------------------------
    # CREATE AN OBSERVATION ERROR MODEL (Gaussian)
    # --------------------------------------------
    observation_size = observation_grid.shape[0]  # Observation vector size
    obs_err_cov = utility.create_covariance_matrix(
        size=observation_size,
        corr=obs_error_correlation,
        stdev=np.sqrt(observation_error_variance),
    )
    obs_err_configs = dict(
        size=observation_size,
        mean=0.0,
        variance=obs_err_cov,
        random_seed=random_seed,
    )
    obs_err_model = Gaussian.GaussianErrorModel(configs=obs_err_configs)

    # -------------------------------------------------------------
    # CREATE AN INVERSE PROBLEM (here use FourDVar/Smoother), then:
    #   - Create and assign noisy/synthetic observations
    # -------------------------------------------------------------
    checkpoints = np.arange(
        init_time,
        init_time + model_time_step * (num_obs_time_points + 0.5),
        model_time_step,
    )
    fdvar_configs = dict(
        window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior=prior,
        observation_operator=observation_operator,
        observation_error_model=obs_err_model,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=fdvar_configs)

    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[1:],
    )
    # perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)

    # ---------------------------------------------------------------
    # CREATE ROBUST OED PROBLEM (with specified uncertain parameter):
    # ---------------------------------------------------------------

    # Specify the uncertain parameter configurations
    if re.match(r"\Aprior(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE):
        parameter_size = model_state_size
        hyperparameter_def_value = np.sqrt(prior_variance)
        hyperparameter_bounds = (
            None
            if prior_variance_bounds is None
            else [np.sqrt(v) for v in prior_variance_bounds]
        )

    elif re.match(
        r"\Aobservation(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE
    ):
        parameter_size = observation_size
        hyperparameter_def_value = np.sqrt(observation_error_variance)
        hyperparameter_bounds = (
            None
            if observation_error_variance_bounds is None
            else [np.sqrt(v) for v in observation_error_variance_bounds]
        )

    else:
        print("Unsupported uncertain parameter {0}".format(uncertain_parameter))
        raise ValueError

    # Create the variance/covariance sampler
    uncertain_parameter_sampler = robust_oed.CovarianceSampler(
        configs=dict(
            kernel=uncertain_parameter_kernel,
            size=parameter_size,
            hyperparameter_def_value=hyperparameter_def_value,
            hyperparameter_bounds=hyperparameter_bounds,
        ),
    )

    # Discretize the grid of the uncertain parameter (for Max-Min estimate with Monte Carlo)
    if bruteforce and bruteforce_with_MC:
        print("Discretizing the domain of the uncertain parameter", end=" ")
        if uncertain_parameter_sampler.hyperparameter_bounds is None:
            print(
                f"No bounds found in the uncertain parameter sampler configurations! "
                f"Cannot do MC-based bruteforce without uncertain parameter sampler bounds."
            )
            uncertain_parameter_sample = None
        else:
            uncertain_parameter_meshgrid = [
                np.linspace(lb, ub)
                for lb, ub in uncertain_parameter_sampler.hyperparameter_bounds
            ]
            uncertain_parameter_meshgrid = np.meshgrid(
                *uncertain_parameter_meshgrid, indexing="ij"
            )
            uncertain_parameter_sample = np.vstack(
                [v.flatten() for v in uncertain_parameter_meshgrid]
            ).T
        print("Done!")
    else:
        uncertain_parameter_sample = None

    print("Creaging the OED problem...", end=" ")
    # Create Robust OED problem with kernel generated above
    uparam_configs = dict(
        parameter=uncertain_parameter,
        sampler=uncertain_parameter_sampler,
    )
    optimizer_configs = dict(
        maxiter=maxiter,
        outer_opt_maxiter=stoch_maxiter,
        solve_by_relaxation=solve_by_relaxation,
        baseline=stoch_baseline,
        stochastic_gradient_sample_size=stoch_grad_batch_size,
        def_stepsize=stoch_learning_rate,
        verbose=verbose,
        random_seed=random_seed,
    )
    oed_problem = robust_oed.RobustSensorPlacementBayesianInversionOED(
        configs=dict(
            inverse_problem=inverse_problem,
            optimizer_configs=optimizer_configs,
            uncertain_parameter_sampler_configs=uparam_configs,
            verbose=verbose,
        )
    )
    print("Done!")

    # -------------------------------------------------------------------------
    # CREATE & REGISTER THE OPTIMALITY CRITERION and the OED evaluation method
    # -------------------------------------------------------------------------
    print("Registering the Utility function (optimality criterion)...", end=" ")
    criterion_configs = {
        "oed_problem": oed_problem,
        "evaluation_method": oed_evaluation_method,
        **OED_RANDOMIZATION_SETTINGS,
    }
    oed_criterion = uf.find_criteria(
        oed_criterion,
    )(criterion_configs)
    oed_problem.register_optimality_criterion(oed_criterion)
    print("Done!")

    # Create (and register) an optional penalty function (with associated gradient; for relaxation approach)
    penalty_function, penalty_function_gradient = create_penalty_function(
        name=penalty_function,
        budget=budget,
    )
    oed_problem.register_penalty_term(
        penalty_function=penalty_function,
        penalty_function_gradient=penalty_function_gradient,
        penalty_weight=penalty_weight,
    )

    # Calculate a scaling factor for the OED objective so that penalty parameter can be better determined
    d = oed_problem.inverse_problem.observation_error_model.design
    d[:] = 0
    p = uncertain_parameter_sampler.hyperparameter_def_value
    scale = oed_problem.objective_function_value(design=d, uncertain_parameter_val=p)
    offset = -scale
    if verbose:
        print(f"Setting the scale of the OED objective function to: {scale}")
    oed_problem.oed_objective_offset = offset

    # =======================================
    # START CARRYING OUT THE REQUESTED TASKS:
    # =======================================

    # ----------------------
    # Solve the OED problem:
    # ----------------------
    if solve_OED_problem:

        msg = "=" * 50 + "\n"
        msg += "***\nAssimilation Window:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.window
        )
        msg += "***\nObservation Time Points:\n***\n{0}\n\n".format(
            oed_problem.inverse_problem.observation_times
        )
        msg += "=" * 50 + "\n\n"
        print(msg)

        oed_results = None
        saveto = os.path.join(output_dir, OED_RESULTS_FILENAME)

        if not overwrite and os.path.isfile(saveto):
            try:
                print("Trying to load file")
                oed_results = oed_problem.load_results(saveto)
            except:
                print(
                    "Failed to load OED results from file; will resolve and overwrite"
                )
                overwrite = True

        ## No OED results; (re)solve the OED problem
        if oed_results is None:
            # Solve the OED problem, and plot results as needed
            oed_results = oed_problem.solve()
            if solve_inverse_problem:
                oed_problem.design = oed_results.optimal_design
                inverse_problem_results = oed_problem.solve_inverse_problem()
            else:
                inverse_problem_results = None

            # Pickle/Write Results, and plot
            oed_results.write_results(saveto=saveto)

            if bruteforce:
                print(">>> Starting Bruteforce Search...")
                saveto = os.path.join(output_dir, BRUREFORCE_RESULTS_FILENAME)
                bruteforce_results = oed_problem.optimizer.update_bruteforce_results(
                    bruteforce_results={
                        "uncertain_parameter_sample": uncertain_parameter_sample,
                    },
                    saveto=saveto,
                )
                print(">>> Bruteforce Search DONE <<<")
            else:
                bruteforce_results = None

        ## 2D exhaustive search
        if exhaustive_plots:
            saveto = os.path.join(output_dir, EXHAUSTIVE_SEARCH_RESULTS_FILENAME)
            exhaustive_parameter_search_results = None
            if not overwrite and os.path.isfile(saveto):
                try:
                    print("Trying to load file")
                    exhaustive_parameter_search_results = pickle.load(
                        open(saveto, "rb")
                    )
                except:
                    print(
                        "Failed to load OED results from file; will resolve and overwrite"
                    )

            # Plot surfaces with exhaustive search
            print(">>> Starting Exhaustive Parameter Search for 2D problem...")
            if exhaustive_parameter_search_results is None:
                exhaustive_parameter_search_results = (
                    oed_problem.optimizer.exhaustive_parameter_search(
                        uncertain_parameter_sample=uncertain_parameter_sample,
                        saveto=saveto,
                    )
                )
            print(">>> Exhaustive Parameter Search DONE <<<")
        else:
            exhaustive_parameter_search_results = None

        # Plot the OED problem results
        try:
            # oed_results.plot_results(output_dir=output_dir)
            oed_problem.plot_results(
                oed_results,
                bruteforce_results=bruteforce_results,
                exhaustive_parameter_search_results=exhaustive_parameter_search_results,
                output_dir=output_dir,
                overwrite=overwrite,
            )
        except Exception as err:
            print(f"Failed to plot OED results\n" f"Unexpected {err=} of {type(err)=}")
            raise

    else:
        oed_results = exhaustive_parameter_search_results = None

    if uncertain_parameter_nominal_value is not None:
        saveto = os.path.join(output_dir, "nominal_value_results.pcl")
        nominal_value_results = oed_problem.nominal_value_test(
            uncertain_parameter_nominal_value,
            learning_rate=stoch_learning_rate,
            stoch_baseline=stoch_baseline,
            stoch_maxiter=stoch_maxiter,
            batch_size=stoch_grad_batch_size,
            robust_oed_results=oed_results,
            saveto=saveto,
        )

    else:
        nominal_value_results = None

    return (
        oed_problem,
        oed_results,
        inverse_problem_results,
        exhaustive_parameter_search_results,
    )  # for debugging


def robust_oed_paper_main(
    toy_model=False,
    ad_model=False,
    overwrite=False,
    random_seed=RANDOM_SEED,
    verbose=False,
):
    """
    The main driver for generating results for the OED paper.

    :param bool toy_model: if `True`, run the experiments involving the linear 2D toy experiments
    :parm bool ad_model: if `True` run experiments involving the 2D AD dolfin-based model
    :param bool overwite_results: if `True` and results exist corresponding experiments will run and results will be overwritten
    :param bool verbose: screen verbosity

    :remarks: `overwrite` inspects the settings file `settings.pcl` and check if the experiment exists with the same settings, if so, it dosn't run the experiments
    """
    num_sensors = [2, 5, 10, 25, 50, 100]
    penalty_functions = ["l0", "l1"]
    penalty_weights = np.arange(21) * 5
    budgets = [None, 1, 3, 8]

    # Run experiments involving teh 2d Toy linear model
    if toy_model:
        base_output_dir = f"__Results/__TimeDependent_TOYModel_Robust_OED_Results"
        penalty_weights = np.arange(0, -21, -5)

        for penalty_weight in penalty_weights:
            for budget in [None, 1]:
                if budget is not None and penalty_weight == 0:
                    continue
                for solve_by_relaxation in [False, True]:
                    output_subdir = f"Budget_{str(budget)}_Penalty_{str(penalty_weight)}/{'Relaxation' if solve_by_relaxation else 'Stochastic_RL'}"
                    output_dir = os.path.join(base_output_dir, output_subdir)
                    try:
                        robust_OED_time_dependent_toy_linear(
                            solve_by_relaxation=solve_by_relaxation,
                            penalty_weight=penalty_weight,
                            budget=budget,
                            output_dir=output_dir,
                            solve_OED_problem=True,
                            bruteforce=True,
                            exhaustive_plots=True,
                            random_seed=random_seed,
                            overwrite=overwrite,
                            verbose=verbose,
                        )
                    except:
                        print("This experiment failed; passing...")
                        raise

    if ad_model:
        base_output_dir = f"__Results/__Dolfin_AD-2D_Robust_OED_Results"
        penalty_weights = np.arange(0, -21, -5)

        for penalty_weight in penalty_weights:
            for budget in [None, 1]:
                if budget is not None and penalty_weight == 0:
                    continue
                for solve_by_relaxation in [False, True]:
                    output_subdir = f"Budget_{str(budget)}_Penalty_{str(penalty_weight)}/{'Relaxation' if solve_by_relaxation else 'Stochastic_RL'}"
                    output_dir = os.path.join(base_output_dir, output_subdir)
                    try:
                        robust_OED_Dolfin_AD_2d(
                            solve_by_relaxation=solve_by_relaxation,
                            penalty_weight=penalty_weight,
                            budget=budget,
                            output_dir=output_dir,
                            solve_OED_problem=True,
                            bruteforce=True,
                            exhaustive_plots=True,
                            random_seed=random_seed,
                            overwrite=overwrite,
                            verbose=verbose,
                        )
                    except:
                        print("This experiment failed; passing...")
                        # raise


def _solve_inverse_problem(
    oed_problem,
    design,
    uncertain_parameter_value,
    reset_param=False,
    verbose=False,
):
    """Solve the inverse problem for the passed design and uncertain parameter value"""

    # Set the design and the uncertain parameter
    # Load the keys of the passed uncertain parameter, save current values, and update to the passed ones
    inverse_problem = oed_problem.inverse_problem
    inverse_problem.verbose = verbose

    prior_cov = None  # Placeholder if reset is needed
    obs_err_cov = None  # Placeholder if reset is needed

    uncertain_parameter = oed_problem.uncertain_parameter
    uncertain_parameter_sampler = oed_problem.uncertain_parameter_sampler

    # Update the uncertain parameter (cov mat)
    if re.match(r"\Aprior(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE):
        covmat = uncertain_parameter_sampler.evaluate(uncertain_parameter_value)
        prior_cov = inverse_problem.prior.unweighted_covariance_matrix()
        inverse_problem.prior.update_covariance_matrix(covmat)

    elif re.match(
        r"\Aobservation(-| |_)*variance\Z", uncertain_parameter, re.IGNORECASE
    ):
        covmat = uncertain_parameter_sampler.evaluate(uncertain_parameter_value)
        obs_err_cov = (
            inverse_problem.observation_error_model.unweighted_covariance_matrix()
        )
        inverse_problem.observation_error_model.update_covariance_matrix(covmat)

    else:
        print(
            "The passed parameter '{0}' is not recognized or *NOT-YET* supported!".format(
                uncertain_parameter
            )
        )
        raise ValueError

    # Update the design of the observation error model
    # Get copies of current design vectors (to reset later)
    obs_oper_design = inverse_problem.observation_operator.design
    obs_err_cov_design = inverse_problem.observation_error_model.design

    # Rest design and uncertain parameter
    inverse_problem.observation_operator.design = design
    inverse_problem.observation_error_model.design = design
    # Solve the inverse problem, and get the posterior (map/mean and covariance if possible)
    ip_results = dict(
        mean=None,
        covariance=None,
        design=design,
        unertain_parameter=uncertain_parameter,
        uncertain_parameter_value=uncertain_parameter_value,
    )
    try:
        init_guess = inverse_problem.prior.mean
        map_point = inverse_problem.solve(
            init_guess,
            skip_map_estimate=False,
            update_posterior=False,
        )
        post_cov = inverse_problem.posterior_covariance(
            map_point,
        )
        ip_results.update(
            dict(
                mean=map_point,
                covariance=post_cov,
                precision=np.linalg.inv(post_cov),
            )
        )

    except:
        print("***\nFailed to solve the inverse problem\n***")
        raise
        # pass

    if reset_param:
        # Rest design and uncertain parameter
        inverse_problem.observation_operator.design = obs_oper_design
        inverse_problem.observation_error_model.design = obs_err_cov_design
        if prior_cov is not None:
            inverse_problem.prior.update_covariance_matrix(prior_cov)
        if obs_err_cov is not None:
            inverse_problem.observation_error_model.update_covariance_matrix(
                obs_err_cov
            )

    return ip_results


def _find_global_optimum(oed_results):
    if oed_results.bruteforce:
        bruteforce_results = oed_results.bruteforce_results

        if bruteforce_results["cluster_by_acitve"]:
            global_optimum = np.nan
            global_optimum_design = None

        else:
            # Retrieve an estimate of the global optimum (over the given sample of the uncertain parameter)
            # For every design find the objective value for all values of the uncertain parameter (lambda)
            #   and calculate the minimum;
            #   The robust optimal design is the one that has the maximum value of thosee minima
            num_designs = np.power(2, design_size)
            all_lambdas = [
                bruteforce_results["results"][k]["parameter"]
                for k in bruteforce_results["results"].keys()
            ]
            num_lambdas = len(all_lambdas)

            all_objvals = np.empty((num_lambdas, num_designs))
            for lam_ind in range(num_lambdas):
                all_objvals[lam_ind, :] = bruteforce_results["results"][lam_ind][
                    "objective_values"
                ]

            min_per_design = np.min(all_objvals, 0)
            best_design_ind = np.argmax(min_per_design)
            global_optimum = min_per_design[best_design_ind]
            best_lambda_ind = np.where(
                all_objvals[:, best_design_ind] == global_optimum
            )[0]
            global_optimum_design_index = best_design_ind + 1
            global_optimum_design = utility.index_to_binary_state(
                global_optimum_design_index, design_size
            )

    else:
        global_optimum_design = global_optimum_value = None

    return global_optimum_design, global_optimum_value


def analyze_inverse_problem_solution(
    oed_problem,
    oed_results,
    sample_size=25,
    results_file=None,
    random_seed=RANDOM_SEED,
    budget=None,
    verbose=True,
):
    """
    Given the OED problem instance, and the oed_results, and optional number of active sensors,

    The inverse problem is solved for both the global optimum solution (against random samples
    of the uncertain parameter), the solution returned by the robust OED
    optimization algorithm.
    The soltuion is also obtained for all designs of the passed budget (or random designs).
    Box plots are plotted as well.
    """

    # Create a local random number generator from the passed seed
    rng = np.random.default_rng(random_seed)

    if results_file is not None and os.path.isfile(results_file):
        results = pickle.load(open(results_file, "rb"))
    else:
        results = {}

    # solve for the optimal solution
    optimal_design = oed_results.optimal_design
    try:
        optimal_design_results = results["optimal_design_results"]
    except KeyError:
        results.update({"optimal_design_results": {}})
        optimal_design_results = results["optimal_design_results"]

    for i in range(len(optimal_design_results), sample_size):
        uncertain_parameter_value = oed_problem.uncertain_parameter_sampler.sample()[0][
            0
        ]
        ip_results = _solve_inverse_problem(
            oed_problem=oed_problem,
            design=optimal_design,
            uncertain_parameter_value=uncertain_parameter_value,
            verbose=verbose,
        )
        optimal_design_results.update({i: ip_results})

        if results_file is not None:
            print(
                f"Overwriting Optimal design: [{i+1}/{sample_size}] results {results_file}"
            )
            pickle.dump(results, open(results_file, "wb"))

    # solve for the global optimal solution (using the same parameter sample used above for the optimal design)
    global_optimal_design = _find_global_optimum(oed_results)
    try:
        global_optimal_design_results = results["global_optimal_design_results"]
    except KeyError:
        results.update({"global_optimal_design_results": {}})
        global_optimal_design_results = results["global_optimal_design_results"]

    for i in range(len(global_optimal_design_results), sample_size):
        uncertain_parameter_value = optimal_design_results[i][
            "uncertain_parameter_value"
        ]
        ip_results = _solve_inverse_problem(
            oed_problem=oed_problem,
            design=global_optimal_design,
            uncertain_parameter_value=uncertain_parameter_value,
            verbose=verbose,
        )
        global_optimal_design_results.update({i: ip_results})

        if results_file is not None:
            print(
                f"Overwriting Global Optimal design: [{i+1}/{sample_size}] results {results_file}"
            )
            pickle.dump(results, open(results_file, "wb"))

    #
    all_binary_designs = utility.enumerate_binary(oed_problem.design_size)
    if budget is not None:
        all_designs = []
        for design in all_designs:
            if np.count_nonzero(design):
                all_designs.append(design)

    else:
        rng
        all_designs = [
            all_binary_desings[i] for i in rng.choice(sample_size, replace=False)
        ]

    try:
        enumerated_designs_results = results["enumerated_designs_results"]
    except KeyError:
        results.update({"enumerated_designs_results": {}})
        enumerated_designs_results = results["enumerated_designs_results"]

    for _d, design in enumerate(all_designs):
        ind = utility.index_from_binary_state(design)
        try:
            enumerated_designs_results[ind]
        except KeyError:
            enumerated_designs_results[ind] = {}

        for i in range(len(global_optimal_design_results), sample_size):
            uncertain_parameter_value = optimal_design_results[i][
                "uncertain_parameter_value"
            ]
            ip_results = _solve_inverse_problem(
                oed_problem=oed_problem,
                design=design,
                uncertain_parameter_value=uncertain_parameter_value,
                verbose=verbose,
            )
            enumerated_designs_results[ind].update({i: ip_results})

            if results_file is not None:
                print(
                    f"Overwriting Enumerated Optimal design: design[{_d}/{len(all_designs)}]; sample [{i+1}/{sample_size}] results {results_file}"
                )
                pickle.dump(results, open(results_file, "wb"))

    # Plot results (number of designs x # of uncertain param x 2) last index for rmse and FIM-trace
    summary_results = np.array((len(all_designs), sample_size, 2))
    truth = oed_problem.inverse_problem.model.create_initial_condition()
    for d, design in enumerate(all_designs):
        ind = utility.index_from_binary_state(design)
        for i in range(sample_size):
            # RMSE
            rmse = utility.calculate_rmse(
                enumerated_designs_results[ind][i]["mean"], truth
            )
            summary_results[d, i, 0] = rmse

            # FIM Trace
            tr = utility.matrix_trace(enumerated_designs_results[ind][i]["precision"])
            summary_results[d, i, 1] = tr

    # Box plots

    return results


if __name__ == "__main__":
    robust_oed_paper_main(
        toy_model=True,
        ad_model=False,
        overwrite=False,
    )
