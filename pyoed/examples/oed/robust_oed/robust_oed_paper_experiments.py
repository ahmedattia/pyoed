
"""
All experiments used in the robust OED paper
"""

from pyoed.examples.oed.robust_oed import *

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator


_BASE_OUTPUT_DIR = "__Robust_OED_Paper_Results_Refined"


def inversion_results_analyzer():
    """"""
    results_dir='__Robust_OED_Paper_Results_Refined/Dolfin_AD-2D_Robust_OED/NumSensors_10/L0_Penalty_Budget_3_Weight_-50/Stochastic_RL'

    oed_results_file  = os.path.join(results_dir, 'oed_results.pcl')
    oed_settings_file = os.path.join(results_dir, 'settings.pcl')
    results_file      = os.path.join(results_dir, 'inversion_analysis_results.pcl')

    oed_problem = create_problem_from_settings(oed_settings_file)
    oed_results = oed_problem.load_results(oed_results_file)

    analyze_inverse_problem_solution(oed_problem=oed_problem,
                                     oed_results=oed_results,
                                     results_file=results_file,
                                     budget=3,
                                     )


def scalability_results(cardinality=[2, 5, 10, 25, 50, 100, 120],
                        penalty_weight=-10,
                        penalty_function="l0",
                        stoch_learning_rate=1e-3,
                        maxiter=100,
                        stoch_maxiter=5,
                        random_seed=None,
                        #
                        ignore_settings=False,
                        #
                        overwrite_results=False,
                        linewidth=2,
                        plot_maxiter=None,
                        show_axis_grids=True,
                        ):
    """
    Run experiments to create scalability plots using the AD model

    1- For increasing cardinality, run the AD experiment and plot results
        (with new function evaluations),
    2- First plot: x-axis iteration number; y-axis: a curve for each problem dimension
    3- Second plot: x-axis problem dimension: y-axis redundancy ratio
        (1 - number of unique samples / number of samples)
    """
    base_output_dir = os.path.join(_BASE_OUTPUT_DIR,
                                   f"Dolfin_AD-2D_Robust_OED/Scalability_Experiments",
                                   )
    # Run Experiment
    new_func_evals    = []
    redundancy_ratios = []
    outer_redundancy_ratios = []

    for nsens in cardinality:
        output_dir   = os.path.join(base_output_dir, f"NumSensors_{nsens}")
        setup_file   = os.path.join(output_dir, "settings.pcl")
        results_file = os.path.join(output_dir, "oed_results.pcl")

        # Initialize OEDResults object, and try to retrieve it
        oed_results = None
        if not overwrite_results and os.path.isfile(setup_file) \
                and os.path.isfile(results_file):

            # Load settings from file, and check settings vs those passed
            args = pickle.load(open(setup_file, 'rb'))
            match_cond = ignore_settings or \
              (
                args['penalty_weight'] == penalty_weight and \
                args['penalty_function'] == penalty_function and \
                args['stoch_learning_rate'] == stoch_learning_rate and \
                args['maxiter'] == maxiter and \
                args['stoch_maxiter'] == stoch_maxiter and \
                args['random_seed'] == random_seed
              )

            # If settings match existing file, try loading
            if match_cond:
                args['solve_OED_problem']     = False
                args['solve_inverse_problem'] = False
                args['output_dir']            = output_dir

                try:
                    # Create the OED problem and load results (don't solve)
                    oed_problem, _, _ = robust_OED_Dolfin_AD_2d(**args)
                    oed_results = oed_problem.load_results(results_file)
                    print(f"\n** Loaded Results for Robust OED experiment for {nsens} Sensors")

                except:
                    print(f"\n** Failed to load results from: {results_file}")
        else:
            pass


        if oed_results is None:
            print(f"\n**\nCreating and solving Robust OED experiment for {nsens} Sensors\n**\n")

            _, oed_results, _ = robust_OED_Dolfin_AD_2d(
                penalty_function=penalty_function,
                penalty_weight=penalty_weight,
                stoch_maxiter=stoch_maxiter,
                stoch_learning_rate=stoch_learning_rate,
                maxiter=maxiter,
                random_seed=random_seed,
                #
                uniform_obs_grid_size=nsens,
                #
                output_dir=output_dir,
                solve_OED_problem=True,
                solve_by_relaxation=False,
                bruteforce=True,
                exhaustive_plots=True if nsens==2 else False,
                validate_OED_gradient=False,
                overwrite_results=overwrite_results,
                verbose=False,
            )

        # Calculate redundancy ratio for the outer optimization problem (to discard the inner LBFGS comp)
        _s = 0.0
        _t = 0.0
        outer_optimization_results = oed_results.optimization_results['outer_optimization_results']
        for i in range(len(outer_optimization_results)):
            for d in outer_optimization_results[i]['sampled_design_indexes']:
                _s += len(set(d))
                _t += len(d)
        outer_redundancy_ratios.append(_s/_t)

        redundancy_ratios.append(oed_results.optimization_results['redundancy_ratio'])
        target = oed_results.optimization_results['objective_value_evaluation_tracker_per_iteration']
        func_evals_per_iter = [
            len(target[k]) for k in range(len(target))
        ]
        _func_evals_per_iter = np.zeros(len(oed_results.optimization_trajectory_objval)-1)
        _func_evals_per_iter[: len(func_evals_per_iter)] = func_evals_per_iter
        func_evals_per_iter = _func_evals_per_iter

        new_func_evals.append(func_evals_per_iter)

    # Create Plots
    print("Plotting results...")

    utility.plots_enhancer(usetex=True)

    fig = plt.figure(figsize=(12, 6))
    ax  = fig.add_subplot(111)
    if show_axis_grids:
        utility.show_axis_grids(ax)
    ax.plot(cardinality, redundancy_ratios,'-o', lw=linewidth, markersize=2*linewidth, )

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel(r"Number of Candidate Sensors")
    ax.set_ylabel(r"Redundancy Ratio")
    ax.set_ylim(-0.05, 1.05)

    # save plot
    saveto = os.path.join(base_output_dir, "Total_Redundancy_Ratio_Plot.pdf")
    print("Saving/Plotting Results to: {0}".format(saveto))
    fig.savefig(saveto, dpi=600, bbox_inches='tight')
    plt.close(fig)


    fig = plt.figure(figsize=(12, 6))
    ax  = fig.add_subplot(111)
    if show_axis_grids:
        utility.show_axis_grids(ax)
    ax.plot(cardinality, outer_redundancy_ratios,'-o',
            lw=linewidth, markersize=2*linewidth,
            label=r"Outer problem",
            )

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel(r"Number of Candidate Sensors")
    ax.set_ylabel(r"Redundancy Ratio")
    ax.set_ylim(-0.05, 1.05)

    # save plot
    saveto = os.path.join(base_output_dir, "OuterOptimization_Redundancy_Ratio_Plot.pdf")
    print("Saving/Plotting Results to: {0}".format(saveto))

    # Add overall redundancy
    ax.plot(cardinality, redundancy_ratios,'--d',
            lw=linewidth, markersize=2*linewidth,
            label=r"Overall",
            )
    ax.legend(loc='best', framealpha=0.75, )
    ax.set_ylim(-0.05, 1.05)
    saveto = os.path.join(base_output_dir, "Redundancy_Ratio_Plot.pdf")
    print("Saving/Plotting Results to: {0}".format(saveto))
    fig.savefig(saveto, dpi=600, bbox_inches='tight')
    plt.close(fig)


    fig = plt.figure(figsize=(12, 6))
    ax  = fig.add_subplot(111)
    if show_axis_grids:
        utility.show_axis_grids(ax)
    maxiter = 0
    for nsens, nevals in zip(cardinality, new_func_evals):
        maxiter = max(maxiter, len(nevals))
        ax.plot(np.arange(1, len(nevals)+1), nevals, '-o',
                lw=linewidth, markersize=2*linewidth,
                label=r"$n_{\rm s}={%d}$"%nsens,
                )

    if plot_maxiter is not None:
        ax.set_xlim([0, plot_maxiter])

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.set_xlabel(r"Optimization Iteration")
    ax.set_ylabel(r"New Function Evaluations")

    ax.legend(loc='best', framealpha=0.75, ncol=2, title=r"Cardinality")

    # save plot
    saveto = os.path.join(base_output_dir, "New_Function_Evaluations.pdf")
    print("Saving/Plotting Results to: {0}".format(saveto))
    fig.savefig(saveto, dpi=600, bbox_inches='tight')
    plt.close(fig)

def nominal_value_test(nsens=2,
                       penalty_weight=-10,
                       penalty_function="l1",
                       stoch_learning_rate=1e-3,
                       maxiter=100,
                       stoch_maxiter=5,
                       random_seed=None,
                       #
                       ignore_settings=False,
                       #
                       overwrite_results=False,
                       linewidth=3,
                       n_grid_points=30,
                       n_bins=None,
                       show_axis_grids=True,
                       ):
    """
    Use the AD model with two candidate sensors to create nominal-value-test plots

    The nominal-value test experiments works as follows:
        1- Solve the OED problem for a nominal value of the uncertain parametor to get <an optimal design for that nominal value of the uncertain parameter>
        2- Solve the Robust OED problem to get <an optimal robust design against all values of the uncertain parameter>

        3- Plot the value of the objective (deterministic) over the space of the uncertain parameter for each of the two designs optained;
            This yields two surfaces, so the plot won't be really useful here.
            Thus, will generate Barplot for positive and negative values of the difference between objectives (for the two designs) for each value of the uncertain parameter in the domain

    :remarks:
        - This is experimental; once fully developed, it will be moved to robust_oed.py module
    """
    # TODO: If worth it, consider the nominal value optional
    nominal_value = np.ones(nsens) * 0.03

    base_output_dir            = os.path.join(_BASE_OUTPUT_DIR,
                                              f"Dolfin_AD-2D_Robust_OED/NominalValue_Experiments",
                                              )
    output_dir                 = os.path.join(base_output_dir, f"NumSensors_{nsens}")
    setup_file                 = os.path.join(output_dir, "settings.pcl")
    oed_results_file           = os.path.join(output_dir, "Robust_OED_Results", "oed_results.pcl")
    nominal_value_results_file = os.path.join(output_dir, "NominalValue_OED_Results", "oed_results.pcl")
    nominal_value_test_results_file = os.path.join(output_dir, "nominal_value_test_results.pcl")

    # Initialize and attempt loading existing results
    nominal_value_test_results = None
    if os.path.isfile(nominal_value_test_results_file) and not overwrite_results:
        try:
            nominal_value_test_results = pickle.load(open(nominal_value_test_results_file, 'rb'))
            print(f"Loaded Nominal Value Test Results From: '{nominal_value_test_results_file}'")
        except:
            print(f"Failed to Load Nominal Value Test Results From: '{nominal_value_test_results_file}'")

    # If nominal_value_test_results is not valid, regenerate
    if nominal_value_test_results is None:
        # Initialize
        oed_problem = oed_results = nominal_value_results = None

        # Try Loading each of the results files
        if not overwrite_results \
                and os.path.isfile(setup_file):

            # Load settings from file, and check settings vs those passed
            args = pickle.load(open(setup_file, 'rb'))
            match_cond = ignore_settings or \
              (
                args['penalty_weight'] == penalty_weight and \
                args['penalty_function'] == penalty_function and \
                args['stoch_learning_rate'] == stoch_learning_rate and \
                args['maxiter'] == maxiter and \
                args['stoch_maxiter'] == stoch_maxiter and \
                args['random_seed'] == random_seed
              )

            # If settings match existing file, try loading
            if match_cond:
                args['solve_OED_problem']     = False
                args['solve_inverse_problem'] = False
                args['output_dir']            = output_dir

                # Create the OED problem and load results (don't solve)
                oed_problem, _, _ = robust_OED_Dolfin_AD_2d(**args)
                try:
                    oed_results = oed_problem.load_results(oed_results_file)
                    print(f"\n** Loaded Robust OED Results for experiment with {nsens} Sensors")

                except:
                    print(f"\n**\nFailed to load results from: '{oed_results_file}'\n**")

                try:
                    nominal_value_results = oed_problem.load_results(nominal_value_results_file)
                    print(f"\n** Loaded Nominal Value Results for OED experiment for {nsens} Sensors")

                except:
                    print(f"\n**\nFailed to load Nomainale Value Results from: '{nominal_value_results_file}'\n**")

        else:
            pass

        if nominal_value_results is None:
            if oed_problem is None:
                oed_problem, _ , _ = robust_OED_Dolfin_AD_2d(
                    penalty_function=penalty_function,
                    penalty_weight=penalty_weight,
                    stoch_maxiter=stoch_maxiter,
                    stoch_learning_rate=stoch_learning_rate,
                    maxiter=maxiter,
                    random_seed=random_seed,
                    #
                    uniform_obs_grid_size=nsens,
                    #
                    output_dir=os.path.dirname(nominal_value_results_file),
                    solve_OED_problem=False,
                    solve_by_relaxation=False,
                    bruteforce=True,
                    exhaustive_plots=True if nsens==2 else False,
                    validate_OED_gradient=False,
                    overwrite_results=overwrite_results,
                    verbose=False,
                )
            nominal_value_results = oed_problem.solve_oed_problem(
                nominal_value=nominal_value,
                learning_rate=stoch_learning_rate,
                stoch_maxiter=2*maxiter,
                maxiter=maxiter,
                bruteforce=True,
            )

            nominal_value_results.write_results(saveto=nominal_value_results_file)
            #
        try:
            nominal_value_results.plot_results(output_dir=os.path.dirname(nominal_value_results_file))
        except:
            print("Failed to plot nominal value results")
            raise


        if oed_results is None:
            print(f"\n**\nCreating and solving Robust OED experiment for {nsens} Sensors\n**\n")

            oed_problem, oed_results, _ = robust_OED_Dolfin_AD_2d(
                penalty_function=penalty_function,
                penalty_weight=penalty_weight,
                stoch_maxiter=stoch_maxiter,
                stoch_learning_rate=stoch_learning_rate,
                maxiter=maxiter,
                random_seed=random_seed,
                #
                uniform_obs_grid_size=nsens,
                #
                output_dir=os.path.dirname(oed_results_file),
                solve_OED_problem=True,
                solve_by_relaxation=False,
                bruteforce=True,
                exhaustive_plots=True if nsens==2 else False,
                validate_OED_gradient=False,
                overwrite_results=overwrite_results,
                verbose=False,
            )


        # Plot Robust OED Results
        plot_robust_oed_results(
            settings_file=setup_file,
            output_dir=os.path.dirname(oed_results_file),
        )


        # Initialize a dictionary to hold collective nominal-value test results
        nominal_value_test_results = dict()

        # Create Plots
        # Evaluate the objective value at the robust optimal design & the nominal design
        nominal_value_design, nominal_value_uncertainparam  = nominal_value_results.optimal_design, nominal_value
        robust_oed_design, robust_oed_uncertainparam        = oed_results.optimization_traject[-1]

        nominal_value_objval = oed_problem.oed_objective(design=nominal_value_design,
                                                         uncertain_param_val=nominal_value_uncertainparam,
                                                         )
        robust_oed_objval    = oed_problem.oed_objective(design=robust_oed_design,
                                                         uncertain_param_val=robust_oed_uncertainparam,
                                                         )

        # Create a meshgrid, and evaluate the value of the objective at each, then find the difference with both nominal value solution and robust oed solution
        uncertainparam_bounds = oed_problem.uncertain_parameter_sampler.configurations['hyperparam_bounds']
        uncertainparam_grids = [np.linspace(bounds[0], bounds[1], n_grid_points) for bounds in uncertainparam_bounds]
        uncertainparam_grids = np.meshgrid(*uncertainparam_grids)

        # Update results dictionary
        nominal_value_test_results.update(
            {'nominal_value_design':nominal_value_design,
             'nominal_value_uncertainparam':nominal_value_uncertainparam,
             'robust_oed_design':robust_oed_design,
             'robust_oed_uncertainparam':robust_oed_uncertainparam,
             'nominal_value_objval':nominal_value_objval,
             'robust_oed_objval':robust_oed_objval,
             'uncertainparam_grids':uncertainparam_grids,
            }
        )

        # Evaluate:
        print("Evaluating objval at all grid points in the uncertain partameter domain")
        # 1- objective value at gridpoints of the uncertain parameter with (nominal-value optimal design)
        # 2- objective value at gridpoints of the uncertain parameter with (robust OED-value optimal design)
        grid_objval_nominal = np.empty_like(uncertainparam_grids[0])
        grid_objval_robust  = np.empty_like(uncertainparam_grids[0])
        shape = grid_objval_nominal.shape
        num_points = np.prod(shape)

        # Create an iterator to loop over all points
        it = np.nditer(grid_objval_nominal, flags=['multi_index'])
        with it:
            cntr = 1
            while not it.finished:
                print(f"\rEvaluating objective: {cntr}/{num_points}", end=" ")
                uncerntainparam_val = [grid[it.multi_index] for grid in uncertainparam_grids]

                grid_objval_nominal[it.multi_index] = oed_problem.oed_objective(design=nominal_value_design,
                                                                                uncertain_param_val=uncerntainparam_val,
                                                                                )
                grid_objval_robust[it.multi_index] = oed_problem.oed_objective(design=robust_oed_design,
                                                                               uncertain_param_val=uncerntainparam_val,
                                                                               )
                it.iternext()
                cntr += 1

        print("\nDone")

        # Update Dictionary
        nominal_value_test_results.update(
            {'grid_objval_nominal':grid_objval_nominal,
             'grid_objval_robust':grid_objval_robust,
            }
        )
        #
        print(f"Saving Nominal Value Test Results to: '{nominal_value_test_results_file}'")
        pickle.dump(nominal_value_test_results, open(nominal_value_test_results_file, 'wb'))


    # Extract information from loaded/created dictionary
    grid_objval_nominal = nominal_value_test_results['grid_objval_nominal']
    grid_objval_robust  = nominal_value_test_results['grid_objval_robust']


    # Evaluate difference between evaluated objective values, and bin results
    data = np.ravel(grid_objval_robust - grid_objval_nominal)
    heights, bins, _ = plt.hist(data, bins=n_bins)

    bin_width = np.diff(bins)[0]
    bin_pos = bins[:-1] + bin_width / 2

    # Create Bar Plot
    utility.plots_enhancer(usetex=True)

    fig = plt.figure(figsize=(10,6))
    ax = fig.add_subplot(111)
    mask = (bin_pos >= 0)

    # plot data in two steps
    ax.bar(bin_pos[mask], heights[mask], width=bin_width, color='C1')
    ax.bar(bin_pos[~mask], heights[~mask], width=bin_width, color='C0')

    ax.set_xlabel(r"$ \mathcal{J}(\zeta^{\rm robust},\lambda) - \mathcal{J}(\zeta^{\rm nominal},\lambda) $")
    ax.set_ylabel(r"Frequency")
    ax.axvline(x=0, linestyle='--',linewidth=linewidth, color='grey')

    ylim = ax.get_ylim()
    ax.set_ylim((ylim[0], ylim[1]*1.1))

    xlim = np.max(np.abs(ax.get_xlim()))
    ax.set_xlim((-xlim, xlim))

    # save ploti
    saveto = os.path.join(output_dir, "NominalValueTest_BarPlot.pdf")
    print("Saving/Plotting Results to: {0}".format(saveto))
    fig.savefig(saveto, dpi=600, bbox_inches='tight')
    plt.close(fig)


    # return oed_problem, oed_results, nominal_value_results, nominal_value_test_results




def robust_oed_paper_main(toy_model=True,
                          ad_model=True,
                          penalty_weights=np.arange(0, -101, -5),
                          penalty_functions=['l0', 'l1'],
                          budgets=[None, 1, 3, 8],
                          relax_formulation=[False],
                          num_sensors=[2, 5, 10, 25, 50, 100],
                          bruteforce=None,
                          plot_settings=True,
                          overwrite_results=False,
                          base_results_dir=_BASE_OUTPUT_DIR,
                          verbose=False,
                          ):
    """
    This is teh main driver to recreate the numerical experiments/results published in teh robust OED paper.

    :param bool toy_model: if `True`, run the experiments involving the linear 2D toy experiments
    :parm bool ad_model: if `True` run experiments involving the 2D AD dolfin-based model
    :param bool overwite_results: if `True` and results exist corresponding experiments will run and results will be overwritten
    :param bool verbose: screen verbosity

    :remarks: `overwrite_results` inspects the settings file `settings.pcl` and check if the experiment
        exists <with the same settings>, if so, it dosn't run the experiments
    """

    if not (toy_model or ad_model):
        print("Nothing to do; neither toy model nor AD model experiments are requeted!")
        return


    # Run the two-dimensional toy model experiments (No need since we run the AD model with two sensors already!)
    if toy_model:
        base_output_dir = os.path.join(base_results_dir, f"TimeDependent_TOYModel_Robust_OED_Results")

        # if budget is not None and penalty_weight == 0: continue
        for budget in budgets:
            for penalty_weight in penalty_weights:
                for penalty_function in penalty_functions:
                    for solve_by_relaxation in relax_formulation:

                        # Sub-Directory
                        output_subdir = f"{penalty_function.title()}_Penalty_Budget_{str(budget)}_Weight_{str(penalty_weight)}/{'Relaxation' if solve_by_relaxation else 'Stochastic_RL'}"
                        # Results-Directory
                        output_dir = os.path.join(base_output_dir, output_subdir)

                        # Run Experiment
                        try:
                            robust_OED_time_dependent_toy_linear(solve_by_relaxation=solve_by_relaxation,
                                                                 penalty_weight=penalty_weight,
                                                                 penalty_function=penalty_function,
                                                                 budget=budget,
                                                                 output_dir=output_dir,
                                                                 solve_OED_problem=True,
                                                                 bruteforce=True,
                                                                 exhaustive_plots=True,
                                                                 validate_OED_gradient=False,
                                                                 overwrite_results=overwrite_results,
                                                                 verbose=verbose,
                                                                 )
                        except:
                            print("**** This experiment failed; Skipping ****")
                            raise


    if ad_model:
        for nsens in num_sensors:
            base_output_dir = os.path.join(base_results_dir, f"Dolfin_AD-2D_Robust_OED/NumSensors_{nsens}/")

            # if budget is not None and penalty_weight == 0: continue
            for budget in budgets:
                for penalty_weight in penalty_weights:
                    for penalty_function in penalty_functions:
                        for solve_by_relaxation in relax_formulation:

                            # Sub-Directory
                            output_subdir = f"{penalty_function.title()}_Penalty_Budget_{str(budget)}_Weight_{str(penalty_weight)}/{'Relaxation' if solve_by_relaxation else 'Stochastic_RL'}"
                            # Results-Directory
                            output_dir = os.path.join(base_output_dir, output_subdir)

                            # Exhaustive search
                            if bruteforce is None:
                                bruteforce = False if nsens > 10 else True
                            exhaustive_plots = True if nsens == 2 else False

                            # Run Experiment
                            try:
                                stoch_learning_rate = 0.01 if nsens == 2 else 0.001
                                robust_OED_Dolfin_AD_2d(solve_by_relaxation=solve_by_relaxation,
                                                        penalty_weight=penalty_weight,
                                                        penalty_function=penalty_function,
                                                        budget=budget,
                                                        output_dir=output_dir,
                                                        uniform_obs_grid_size=nsens,
                                                        stoch_learning_rate=stoch_learning_rate,
                                                        solve_OED_problem=True,
                                                        bruteforce=bruteforce,
                                                        exhaustive_plots=exhaustive_plots,
                                                        validate_OED_gradient=False,
                                                        overwrite_results=overwrite_results,
                                                        verbose=verbose,
                                                        )
                                settings_file = os.path.join(output_dir, 'settings.pcl')

                                if plot_settings:
                                    plot_AD_settings(settings_file)

                            except:
                                print("**** This experiment failed; Skipping ****")
                                raise

def update_plots(plot_setup=False, overwrite=True):
    """
    Regenerate all plots for the results in the '_BASE_OUTPUT_DIR'
    """
    all_files = utility.get_list_of_files(_BASE_OUTPUT_DIR,
                                          recursive=True,
                                          extension='pcl',
                                          ignore_special_dirs=False,
                                          )
    oed_results_files = []
    for f in all_files:
        if os.path.split(f)[-1] == 'oed_results.pcl' and 'Scalability' not in f:
            oed_results_files.append(f)

    for i, f in enumerate(oed_results_files):
        print(f"Generating plots for results file: {i+1}/{len(oed_results_files)} ")

        plot_robust_oed_results(
            settings_file=None,
            output_dir=os.path.dirname(f),
            recursive=False,
            plot_setup=plot_setup,
            overwrite=overwrite,
            )


if __name__ == '__main__':
    args = sys.argv[1: ]

    if len(args) == 0:
        # Run everything
        toy_model         = True
        ad_model          = True
        penalty_weights   = np.arange(0, -101, -5)
        penalty_functions = ['l0', 'l1']
        budgets           = [None, 1, 3, 8]
        relax_formulation = [True, False]
        num_sensors       = [2, 5, 10, 25, 50, 100]

    elif len(args) == 1:

        target = args[0]

        if re.match(r"\Aauto\Z", target, re.IGNORECASE):
            import socket
            host_name = socket.gethostname().split('-')
            if len(host_name) == 3:
                if re.search(r"\Acompute\Z", host_name[0], re.IGNORECASE):
                    try:
                        target = int(host_name[-1])
                    except (ValueError):
                        print(f"Auto detection of host failed; skipping")
                        sys.exit()
            elif len(host_name) == 2:
                if re.search(r"\Acompute\Z", host_name[0], re.IGNORECASE):
                    try:
                        target = int(host_name[-1])
                    except (ValueError):
                        print(f"Auto detection of host failed; skipping")
                        sys.exit()
            else:
                # local machine run toy model only
                target = -1

        elif re.match(r"\Aall\Z", target, re.IGNORECASE):
            target = 0

        elif re.search(r"\A-[0-9]\Z", target, re.IGNORECASE):
            target = int(target)

        elif re.search(r"[0-9]\Z", target, re.IGNORECASE):
            target = int(target.strip('compute - '))

        else:
            print(f"I don't understand the input!")
            print(f"received target: {target}")
            sys.exit()


        if target == 1:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-75, -100]
            penalty_functions = ['l0']
            budgets           = [3]
            relax_formulation = [False]
            num_sensors       = [5]
            bruteforce        = True

        elif target == 2:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-75, -100]
            penalty_functions = ['l1']
            budgets           = [3]
            relax_formulation = [False]
            num_sensors       = [5]
            bruteforce        = True


        elif target == 3:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-10]
            penalty_functions = ['l0']
            budgets           = [None]
            relax_formulation = [False]
            num_sensors       = [5]
            bruteforce        = True

        elif target == 4:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-10]
            penalty_functions = ['l1']
            budgets           = [None]
            relax_formulation = [False]
            num_sensors       = [5]
            bruteforce        = True

        elif target == 5:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-5]
            penalty_functions = ['l1', 'l0']
            budgets           = [None]
            relax_formulation = [False]
            num_sensors       = [10]
            bruteforce        = True

        elif target == 6:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-50]
            penalty_functions = ['l1']
            budgets           = [3, 8]
            relax_formulation = [False]
            num_sensors       = [10]
            bruteforce        = True

        elif target == 7:
            toy_model         = False
            ad_model          = True
            penalty_weights   = [-50]
            penalty_functions = ['l0']
            budgets           = [3, 8]
            relax_formulation = [False]
            num_sensors       = [10]
            bruteforce        = True


        else:
            print(f"The target '{target}' is not recognized")
            print("To use this driver, run: \n\t$ python roust_oed_paper_experiments <CELS MC #, 'auto', 'all'>")
            sys.exit()

    else:
        print("To use this driver, run: \n\t$ python roust_oed_paper_experiments <CELS MC #, 'auto', 'all'>")
        sys.exit()

    sepp = "*" * 50
    msg = f"""{sepp}\n Running Robust OED experiments with settings:
        \r{sepp}
        \r target experiment: {target}
        \r toy_model        : {toy_model}
        \r ad_model         : {ad_model}
        \r penalty_weights  : {penalty_weights}
        \r penalty_functions: {penalty_functions}
        \r budgets          : {budgets}
        \r relax_formulation: {relax_formulation}
        \r num_sensors      : {num_sensors}
        \r bruteforce       : {bruteforce}
        \r{sepp}
        """
    print(msg)

    # Run with specified settings above
    robust_oed_paper_main(toy_model=toy_model,
                          ad_model=ad_model,
                          penalty_weights=penalty_weights,
                          penalty_functions=penalty_functions,
                          budgets=budgets,
                          relax_formulation=relax_formulation,
                          num_sensors=num_sensors,
                          bruteforce=bruteforce,
                          )


