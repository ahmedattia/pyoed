#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


# Add PyOED root path for proper access (without installing PyOED)
import sys, os

pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)
import pyoed


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility
INVERSE_PROBLEM_SETTINGS = {
    "num_candidate_sensors": 2,
    "num_obs_time_points": 1,
}

OPTIMIZATION_CONFIGS = {
    "maxiter": None,
    "stochastic_gradient_sample_size": 32,
    "learning_rate": 0.25,
}


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "OED/OED_AD_FE/")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


def main(
    solve_stochastic_oed=True,
    solve_relaxed_oed=True,
):
    """
    The main function to test OED approaches (relaxation vs. staochastic optimization with
    standard settings.
    This used to be a module-level
    Import statements are inserted in the main function for illustration purposes.

    :param bool solve_stochastic_oed: solve the OED problem using the stochastic formulation
        (for binary optimization)
    :param bool solve_relaxed_oed: solve the OED problem using (pointwise) relaxation
    """
    if not (solve_stochastic_oed or solve_relaxed_oed):
        sys.exit(
            "Both 'solve_stochastic_oed' and 'solve_relaxed_oed' flags are turned off;"
            " nothing to be done!"
        )

    # Create the simulation model (AD with FE discretization)
    from pyoed.models.simulation_models import fenics_models

    model_timestep = 1.0
    model = fenics_models.create_AdvectionDiffusion2D_model(dt=model_timestep)

    # Plot the domain
    model.plot_domain(
        save_to_file=os.path.join(get_base_output_dir(), "AD_Model_Domain.pdf")
    )
    model.plot_velocity_field(
        save_to_file=os.path.join(get_base_output_dir(), "AD_Model_VelocityField.pdf")
    )

    # Ground truth of the inversion parameter (initial condition here)
    true_model_state = model.create_initial_condition()

    # Prior (Laplacian)
    from pyoed.models.error_models import Laplacian

    configs = dict(
        Vh=model.parameter_dof,
        mean=model.parameter_vector(init_val=0.5),
        gamma=1.0,
        delta=16,
        random_seed=RANDOM_SEED,
    )
    prior = Laplacian.DolfinBiLaplacianErrorModel(configs)

    # Observation operator (`NUM_CANDIDATE_SENSORS` candidate sensor locations/gridpoints)
    from pyoed.models.observation_operators.fenics_observation_operators import (
        create_DolfinPointWise_observation_operator,
    )

    obs_oper = create_DolfinPointWise_observation_operator(
        model=model,
        Vh=model.state_dof,
        num_obs_points=INVERSE_PROBLEM_SETTINGS["num_candidate_sensors"] or 10,
    )

    # Observation error model
    # NOTE: use `Gaussian.PointwiseWeightedGaussianErrorModel` to allow relaxation
    from pyoed.models.error_models import Gaussian
    obs_noise = Gaussian.PointwiseWeightedGaussianErrorModel(
        configs={
            "size": obs_oper.shape[0],
            "variance": 0.1,
            "random_seed": RANDOM_SEED,
        },
    )

    # Inversion/assimilation object (4DVar smoother with 5 observation time points)
    import numpy as np
    from pyoed.assimilation.smoothing import fourDVar

    num_obs_time_points = INVERSE_PROBLEM_SETTINGS["num_obs_time_points"] or 5
    checkpoints = np.arange(
        0, model_timestep * (num_obs_time_points + 0.5), model_timestep
    )
    DA_configs = dict(
        window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior=prior,
        observation_operator=obs_oper,
        observation_error_model=obs_noise,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=DA_configs)

    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[1:],
    )

    # Perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = obs_oper.apply(y)
        yobs = obs_noise.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)

    # Import Sensor placement OED class
    from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED

    # Create & solve the OED problem(s)
    if solve_stochastic_oed:

        # Create OED problem (stochastic optimizer)
        oed_problem = SensorPlacementBayesianInversionOED(
            configs=dict(
                inverse_problem=inverse_problem,
                criterion="Bayesian A-opt",
                optimizer="BinaryReinforceOptimizer",
                optimizer_configs={
                    "maxiter": OPTIMIZATION_CONFIGS["maxiter"],
                    "def_stepsize": OPTIMIZATION_CONFIGS["learning_rate"],
                    "stochastic_gradient_sample_size": OPTIMIZATION_CONFIGS["stochastic_gradient_sample_size"],
                },
                problem_is_linear=True,
            )
        )

        # Register the utility function: trace of the FIM (A-optimality)
        oed_problem.oed_criterion.update_configurations(
            evaluation_method="randomized",
            use_FIM=False,
        )

        # Register penalty/regularization term (with desired budge of only 4 active sensor)
        penalty_f = lambda design: np.abs(np.count_nonzero(design) - 4)
        oed_problem.register_penalty_term(
            penalty_function=penalty_f,
            penalty_weight=-1e8,  # Negative as the objective (trace of the FIM below)
        )

        # Solve the OED problem
        oed_results = oed_problem.solve()
        if oed_problem.design_size == 2:
            bruteforce = True
        else:
            bruteforce = False

        # Create standard plots of the OED results
        oed_problem.plot_results(
            oed_results, bruteforce=bruteforce,
            output_dir=get_base_output_dir(),
        )

    if solve_relaxed_oed:

        # Create OED problem (relaxed optimizer)
        oed_problem = SensorPlacementBayesianInversionOED(
            configs=dict(
                inverse_problem=inverse_problem,
                criterion="Bayesian A-opt",
                optimizer="ScipyOptimizer",
                optimizer_configs={
                    "bounds": (0, 1),
                    "options": {
                        "maxiter": OPTIMIZATION_CONFIGS["maxiter"],
                    }
                },
                problem_is_linear=True,
            )
        )

        oed_problem.oed_criterion.update_configurations(
            evaluation_method="randomized",
            use_FIM=False,
        )

        # Add penalty (differentiable function and gradient)
        budget = 2
        penalty_f_l2 = lambda design: np.power(np.sum(design) - budget, 2)
        penalty_f_l2_grad = (
            lambda design: 2 * (np.sum(design) - budget) * np.ones_like(design)
        )
        oed_problem.register_penalty_term(
            penalty_weight=1,
            penalty_function=penalty_f_l2,
            penalty_function_gradient=penalty_f_l2_grad,
        )
        oed_results = oed_problem.solve()


if __name__ == "__main__":
    main()

