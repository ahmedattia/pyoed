#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


# Add PyOED root path for proper access (without installing PyOED)
import sys, os

pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)
import pyoed

# Import standard libraries
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Import simulation model, observation oeprator, error (prior and observation noise) model, and DA
from pyoed.models.simulation_models.toy_linear import ToyLinearTimeDependent
from pyoed.models.observation_operators.identity import Identity
from pyoed.models.error_models.Gaussian import GaussianErrorModel
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar

# OED
from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED


# Import pyoed utility module
from pyoed import utility

utility.plots_enhancer(usetex=True)


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "OED/Stochastic_Learning")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


def main(
    output_dir=os.path.join(get_base_output_dir(), "TimeDependentToyLinearModel"),
):
    # Create the simulation model
    model = ToyLinearTimeDependent(
        configs={"nx": 2, "dt": 0.1, "random_seed": RANDOM_SEED}
    )

    # Create observation operator
    obs_oper = Identity(configs={"model": model})

    # Create prior and observation error model
    prior = GaussianErrorModel(
        configs={
            "size": model.state_size,
            "mean": 1,
            "variance": 1,
            "random_seed": RANDOM_SEED,
        }
    )
    obs_noise = GaussianErrorModel(
        configs={
            "size": obs_oper.shape[0],
            "variance": 0.01,
            "random_seed": RANDOM_SEED,
        }
    )

    # 4DVar DA object, and register its elements
    inverse_problem = VanillaFourDVar()
    inverse_problem.register_model(model)
    inverse_problem.register_observation_operator(obs_oper)
    inverse_problem.register_prior(prior)
    inverse_problem.register_observation_error_model(obs_noise)

    # Set the assimilation/simulation time window
    tspan = (0, 0.3)
    inverse_problem.register_window(tspan)

    # Create truth (true initial state and trajectory)
    true_IC = model.create_initial_condition()
    checkpoints = [0.1, 0.2, 0.3]
    _, true_traject = model.integrate_state(
        true_IC, tspan=tspan, checkpoints=checkpoints
    )

    # Create synthetic data (perturbation to truth) and register them
    for t, state in zip(checkpoints, true_traject):
        obs = obs_noise.add_noise(obs_oper(state))
        inverse_problem.register_observation(t=t, observation=obs)

    # Solve the inverse problem (retrieve the truth)
    inverse_problem.solve_inverse_problem(
        init_guess=prior.mean.copy(), update_posterior=True
    )

    prior_rmse = utility.calculate_rmse(true_IC, inverse_problem.prior.mean)
    posterior_rmse = utility.calculate_rmse(true_IC, inverse_problem.posterior.mean)
    print(f"Prior RMSE: {prior_rmse}")
    print(f"Posterrior RMSE: {posterior_rmse}")

    # Calculate prior and posterior trajectory and evaluate RMSE
    checkpoints, true_traject = model.integrate_state(true_IC, tspan=tspan)
    _, prior_traject = model.integrate_state(inverse_problem.prior.mean, tspan=tspan)
    _, posterior_traject = model.integrate_state(
        inverse_problem.posterior.mean, tspan=tspan
    )
    prior_rmse = [
        utility.calculate_rmse(xp, xt) for xp, xt in zip(prior_traject, true_traject)
    ]
    posterior_rmse = [
        utility.calculate_rmse(xp, xt)
        for xp, xt in zip(posterior_traject, true_traject)
    ]

    # Create output directory if it does not exist
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Plot RMSE (for prior and posterior/analysis)
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111)
    _ = ax.plot(checkpoints, prior_rmse, "-", lw=4, label="Prior")
    _ = ax.plot(checkpoints, posterior_rmse, "-", lw=4, label="Posterior")
    ax.set_xlabel("Time")
    ax.set_ylabel("RMSE")
    ax.legend(loc="best")
    utility.show_axis_grids(ax)
    saveto = os.path.join(output_dir, "4DVar_RMSE.pdf")
    fig.savefig(saveto, dpi=600, bbox_inches="tight", transparent=True)

    # IP posterrior covariance matrix
    post_cov = inverse_problem.posterior.covariance_matrix()

    print("*****postcov****")
    print(post_cov)

    # Exact (from R, B, F)
    F = model.get_model_array()
    Rinv = obs_noise.precision_matrix()
    Binv = prior.precision_matrix()
    A = np.zeros_like(Binv)
    for i in range(1, 4):
        M = np.linalg.matrix_power(F, i)
        A += np.dot(M.T, np.dot(Rinv, M))
    A = np.linalg.inv(Binv + A)
    Aerr = np.abs(post_cov - A)  # Error/mismatch

    # Plot
    fig, axes = plt.subplots(1, 3, figsize=(12, 12))

    im = axes[0].imshow(post_cov)
    divider = make_axes_locatable(axes[0])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im, orientation="vertical", cax=cax)

    im = axes[1].imshow(A)
    divider = make_axes_locatable(axes[1])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im, orientation="vertical", cax=cax)

    im = axes[2].imshow(Aerr)
    divider = make_axes_locatable(axes[2])
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(im, orientation="vertical", cax=cax)

    # Adjust space between subplots
    plt.subplots_adjust(
        wspace=1.25,
        hspace=0.25,
    )

    saveto = os.path.join(output_dir, "4DVar_PostCov.pdf")
    fig.savefig(saveto, dpi=600, bbox_inches="tight", transparent=True)

    # Create OED problem & solve it
    stochastic_oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            inverse_problem=inverse_problem,
            problem_is_linear=True,
            criterion="Bayesian A-Opt",
            verbose=True,
        )
    )
    stochastic_oed_results = stochastic_oed_problem.solve()

    # Plot results
    stochastic_oed_problem.plot_results(
        stochastic_oed_results,
        output_dir=output_dir,
        bruteforce=True,
        overwrite=True,
    )


if __name__ == "__main__":
    main()

