# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module contains all numerical experiments carried out for the paper:
    Ahmed Attia. "Probabilistic Approch to Black Box Binary Optimization with Budget Constraints: Application to Sensor Placement," 2024.
"""
import os, sys
import numpy as np
import pickle
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MaxNLocator
import matplotlib.patches as mpatches

import pyoed
from pyoed import utility
from pyoed.models.simulation_models import fenics_models
from pyoed.models.error_models import Laplacian
from pyoed.models.error_models.Gaussian import PointwiseWeightedGaussianErrorModel
from pyoed.models.observation_operators.fenics_observation_operators import (
    create_DolfinPointWise_observation_operator,
)
from pyoed.assimilation.smoothing import fourDVar
from pyoed.oed.sensor_placement import binary_oed
from pyoed.optimization.binary_optimization import (
    BinaryReinforceOptimizer,
    ConstrainedBinaryReinforceOptimizer,
)
from pyoed.optimization.test_functions.binary_test_functions import (
    AlternatingSignOptimizationFunction,
    WeightedAlternatingSignOptimizationFunction,
)


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility
VERBOSE = False
INVERSE_PROBLEM_SETTINGS = {
    "num_candidate_sensors": 100,
    "model_timestep": 0.5,
    "init_obs_time": 2,
    "num_obs_time_points": 2,
    "obs_err_variance": 1e-4,
}

OPTIMIZATION_SETTINGS = {
    "maximize": True,
    "def_stepsize": 0.5,
    "stochastic_gradient_sample_size": 100,
    "baseline": "optimal",
    "optimal_sample_size": 100,
    "maxiter": 100,
    "pgtol": 1e-12,
    "monitor_iterations": True,
    "random_seed": RANDOM_SEED,
    "antithetic": True,
    "output_dir": None,

}
OED_RANDOMIZATION_SETTINGS = {
    "sample_size": 32,
    "optimize_sample_size": False,
    "min_sample_size": 10,
    "max_sample_size": 100,
    "distribution": "Rademacher",
    "randomization_vectors": None,
    "random_seed": RANDOM_SEED,
    "accuracy": 1e-2,
}

# LOCAL Variables
OPTIMIZER_CONFIGURATIONS_FILE_NAME = "optimizer_configurations.pcl"
OPTIMIZATION_RESULTS_FILE_NAME = "optimization_results.pcl"
UNIFORM_RANDOM_SAMPLE_SIZE = 100
PLOTS_FORMAT = "png"
PLOTS_FONTSIZE = 24
WEIGHTED_BILINEAR_OBJECTIVE = False


## General Functionality
def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR,
        "PROBABILISTIC_OPTIMIZATION",
        f"Maxiter_{OPTIMIZATION_SETTINGS['maxiter']}",
        f"DefStepsize_{OPTIMIZATION_SETTINGS['def_stepsize']}",
    )
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


## Visualization Functions
def plot_weights_functions(logscale=False, output_dir=get_base_output_dir()):
    """
    Create plots for the domain/range of the bernoulli weights and related
    functions
    """
    utility.plots_enhancer(usetex=True, fontweight='normal', fontsize=PLOTS_FONTSIZE, )

    p = np.linspace(0, 1, 1000)[1: -1]
    w = p / (1-p)

    fig = plt.figure(figsize=(16, 3.5))

    ax = fig.add_subplot(131)
    if logscale:
        ax.semilogy(p, w, linewidth=2.5, )
    else:
        ax.plot(p, w, linewidth=2.5, )
    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"$w=\frac{\theta}{1-\theta}$")
    #
    ax = fig.add_subplot(132)
    if logscale:
        ax.semilogy(p, (1+w)**2/w, linewidth=2.5, )
    else:
        ax.plot(p, (1+w)**2/w, linewidth=2.5, )
    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"$\frac{(1+w)^2}{w}=\frac{1}{\theta(1-\theta)}$")
    fig.subplots_adjust(wspace=0.35, )
    #
    ax = fig.add_subplot(133)
    if logscale:
        ax.semilogy(p, (w**2-1)/(1+w)**2, linewidth=2.5, )
    else:
        ax.plot(p, (w**2-1)/(1+w)**2, linewidth=2.5, )
    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"$\frac{w^2-1}{1+w^2}$")
    #
    saveto = os.path.join(output_dir, f"Bernoulli_weights{'_LOG' if logscale else ''}.{PLOTS_FORMAT}")
    fig.savefig(saveto, bbox_inches='tight', dpi=700)
    print(f"Plot saved to {saveto}")
    plt.close(fig)

def plot_optimal_baseline_variance():
    """
    Visualize the variance of the gradient with/without optimal baseline
    & antithetic variates
    """
    raise NotImplementedError(
        f"TODO: Visualize the variance of the gradient with/without optimal baseline"
        f" & antithetic variates..."
    )

def find_and_update_results_plots(root_dir=None, ):
    """Look up all results files and update the corresponding plots"""
    if root_dir is None:
        root_dir = os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR,
        "PROBABILISTIC_OPTIMIZATION",
        )
    all_dirs = list(
        set(
            [
                os.path.dirname(f) for f in utility.get_list_of_files(
                    root_dir=root_dir,
                    recursive=True,
                    return_abs=True,
                    extension='pcl'
                )
            ]
        )
    )

    bilinear_results_dir = []
    oed_results_dir = []

    for results_dir in all_dirs:
        if 'bilinear' in results_dir.lower():
            bilinear_results_dir.append(results_dir)
        elif 'oed' in results_dir.lower():
            oed_results_dir.append(results_dir)
        else:
            pass
            # raise ValueError(
            #     f"Unexpected results path name '{results_dir}'!"
            # )

    for results_dir in bilinear_results_dir:
        plot_bilinear_optimization_experiment_results(results_dir=results_dir)
    for results_dir in oed_results_dir:
        plot_oed_optimization_experiment_results(results_dir=results_dir)


def plot_multirun_results(
        root_dir,
        show_axis_grids=True,
        axis_grids_alpha=(0.25, 0.4),
):
    """
    Look for multirun results, collect them, and create proper visualization

    """

    # Lookup all results directories.
    if root_dir is None:
        root_dir = os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR,
        "PROBABILISTIC_OPTIMIZATION",
        )
    root_dir = os.path.abspath(root_dir)
    all_dirs = list(
        set(
            [
                os.path.dirname(f) for f in utility.get_list_of_files(
                    root_dir=root_dir,
                    recursive=True,
                    return_abs=True,
                    extension='pcl'
                )
            ]
        )
    )
    # Extract parent directories of runs
    all_runs_dirs = list(set([os.path.dirname(d) for d in all_dirs if 'Run_' in d]))

    # Filenaming
    stats_filename = f"runs_stats.{PLOTS_FORMAT}"
    optimal_sol_filename = f"optimal_solutions.{PLOTS_FORMAT}"

    if len(all_runs_dirs) == 0:
        print(f"Didn't find an folders with runs under them!")
        return
    else:
        print(f"Found {len(all_runs_dirs)} folders with multi runs under them")

    # enhance plots and get list of colors
    line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
    utility.plots_enhancer(usetex=True, fontweight='normal', fontsize=PLOTS_FONTSIZE, )

    for runs_dir in all_runs_dirs:
        # Each of those dirs `d` has runs inside it.
        # Find number of runs under this folder
        runs_subdirs = [
            v for v in
            utility.get_list_of_subdirectories(
                root_dir=runs_dir,
                recursive=False,
                ignore_root=True,
                return_abs=True,
                ignore_special=True,
            )
            if 'Run_' in v and os.path.isfile(os.path.join(v, OPTIMIZATION_RESULTS_FILE_NAME))
        ]
        num_runs = len(runs_subdirs)
        print(f"Plotting {num_runs} runs under {runs_dir}")

        # Results Placeholders
        runs_samples_stoch_objvals_lb = []
        runs_samples_stoch_objvals_mean = []
        runs_samples_stoch_objvals_ub = []
        runs_best_along_the_route_objval = []
        runs_optimal_design_objval = []
        runs_num_iters = []
        sample_size = None

        # Loop over directories with runs under them
        for run_id in range(1, num_runs+1):
            run_dir = os.path.join(runs_dir, f"Run_{run_id}", )
            print(f"Collecting results from {run_dir}")

            results_filename = os.path.join(run_dir, OPTIMIZATION_RESULTS_FILE_NAME)
            results = pickle.load(open(results_filename, 'rb'))

            # File naming
            output_dir = os.path.join(runs_dir, "Runs_PLOTS")
            if not os.path.isdir(output_dir): os.makedirs(output_dir)

            # Extract sample objective values, and get minimum, maximum, and mean
            sampled_design_indexes = results['sampled_design_indexes']
            objective_value_tracker = results['objective_value_tracker']
            num_iters = len(sampled_design_indexes)
            runs_num_iters.append(num_iters)

            # Check sample size
            if sample_size is None:
                sample_size = len(sampled_design_indexes[0])

            # Sample size and number of iterations
            _sample_size = len(sampled_design_indexes[0])
            if len(sampled_design_indexes[0]) != sample_size:
                print(
                    f"Inconsistent sample size in results under {run_dir}!\n"
                    f"Expected sample size {sample_size}; found {_sample_size}"
                )

            # Use sampled design indexes to construct range of objective values for samples
            samples_stoch_objvals = [
                [
                    objective_value_tracker[d]
                    for d in sample
                ] for sample in sampled_design_indexes
            ]
            samples_stoch_objvals = np.vstack(samples_stoch_objvals)

            # Stats (over all iterations)
            runs_samples_stoch_objvals_lb.append(np.min(samples_stoch_objvals, axis=1))
            runs_samples_stoch_objvals_mean.append(np.mean(samples_stoch_objvals, axis=1))
            runs_samples_stoch_objvals_ub.append(np.max(samples_stoch_objvals, axis=1))

            # Best along the route
            runs_best_along_the_route_objval.append(results['best_along_the_route']['objval'])

            # Optimal objective value
            runs_optimal_design_objval.append(results['optimal_design_objval'])


        # Create arrays with unified sizes
        num_iters = max(runs_num_iters)
        _runs_samples_stoch_objvals_lb = np.empty((num_runs, num_iters))
        _runs_samples_stoch_objvals_mean = np.empty((num_runs, num_iters))
        _runs_samples_stoch_objvals_ub = np.empty((num_runs, num_iters))
        _runs_best_along_the_route_objval = np.empty(num_iters)
        _runs_optimal_design_objval = np.empty(num_iters)
        # Initialize with NaN
        _runs_samples_stoch_objvals_lb[...] = np.nan
        _runs_samples_stoch_objvals_mean[...] = np.nan
        _runs_samples_stoch_objvals_ub[...] = np.nan
        _runs_best_along_the_route_objval[...] = np.nan
        _runs_optimal_design_objval[...] = np.nan
        #
        for i in range(num_runs):
            _runs_samples_stoch_objvals_lb[i, :runs_num_iters[i]] = runs_samples_stoch_objvals_lb[i]
            _runs_samples_stoch_objvals_mean[i, :runs_num_iters[i]] = runs_samples_stoch_objvals_mean[i]
            _runs_samples_stoch_objvals_ub[i, :runs_num_iters[i]] = runs_samples_stoch_objvals_ub[i]
            _runs_best_along_the_route_objval = np.vstack(runs_best_along_the_route_objval)
            _runs_optimal_design_objval = np.vstack(runs_optimal_design_objval)
        # Overwrite
        runs_samples_stoch_objvals_lb = _runs_samples_stoch_objvals_lb
        runs_samples_stoch_objvals_mean = _runs_samples_stoch_objvals_mean
        runs_samples_stoch_objvals_ub = _runs_samples_stoch_objvals_ub
        runs_best_along_the_route_objval = _runs_best_along_the_route_objval
        runs_optimal_design_objval = _runs_optimal_design_objval


        ## Plot solutions over multiple runs
        ## Create Plot with 2 subplots;
        # 1- mean sample objective over all iterations;
        # 1- maximum sample objective over all iterations;
        # 1- minumum sample objective over all iterations;
        # 2- optimal solution, and best along the route over consecutive runs

        ## Start plotting
        xvals = np.arange(num_iters)

        # One plot with two subplots. First has all stats on it, and second has violin plots
        fig, axes = plt.subplots(1, 2, figsize=(14, 6), width_ratios=[8.5, 1.5])

        # Plot stats on first subplot
        ax = axes[0]
        ax.plot(
            xvals,
            np.nanmean(runs_samples_stoch_objvals_lb, axis=0),
            color=line_colors[0],
            linewidth=2.0,
            alpha=0.65,
            label=None,
            zorder=50,
        )
        ax.fill_between(
            xvals,
            np.nanmin(runs_samples_stoch_objvals_lb, axis=0),
            np.nanmax(runs_samples_stoch_objvals_lb, axis=0),
            color=line_colors[0],
            linewidth=2.0,
            alpha=0.65,
            label=r"Min",
            zorder=50,
        )
        #
        ax.plot(
            xvals,
            np.nanmean(runs_samples_stoch_objvals_mean, axis=0),
            color=line_colors[1],
            linewidth=2.0,
            alpha=0.55,
            label=None,
            zorder=55,
        )
        ax.fill_between(
            xvals,
            np.nanmin(runs_samples_stoch_objvals_mean, axis=0),
            np.nanmax(runs_samples_stoch_objvals_mean, axis=0),
            color=line_colors[1],
            linewidth=2.0,
            alpha=0.55,
            label=r"Mean",
            zorder=55,
        )
        #
        ax.plot(
            xvals,
            np.nanmean(runs_samples_stoch_objvals_ub, axis=0),
            color=line_colors[2],
            linewidth=2.0,
            alpha=0.45,
            label=None,
            zorder=60,
        )
        ax.fill_between(
            xvals,
            np.nanmin(runs_samples_stoch_objvals_ub, axis=0),
            np.nanmax(runs_samples_stoch_objvals_ub, axis=0),
            color=line_colors[2],
            linewidth=2.0,
            alpha=0.45,
            label=r"Max",
            zorder=60,
        )

        # Lables
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        ax.set_xlabel(r"Optimization Iteration")
        ax.set_ylabel(r"Objective Value")

        # Plot violinplots with both optimal solution and best along the route
        ax = axes[1]
        opt_design_vl = ax.violinplot(
            [runs_optimal_design_objval.flatten()],
            positions=[1],
            showmedians=True,
            showmeans=True,
            showextrema=True,
        )
        bart_design_vl = ax.violinplot(
            [runs_best_along_the_route_objval.flatten()],
            positions=[2],
            showmedians=True,
            showmeans=True,
            showextrema=True,
        )

        # Remove ticklabels (Keep ticks for showing the grid)
        ax.set_xticklabels([])
        ax.set_yticklabels([])

        # Set colors
        facecolor_alpha = 0.60
        opt_design_vl_color = line_colors[4]
        opt_design_vl['bodies'][0].set_color(opt_design_vl_color)
        opt_design_vl['bodies'][0].set_alpha(facecolor_alpha)
        opt_design_vl['cmeans'].set_color(opt_design_vl_color)
        opt_design_vl['cmaxes'].set_color(opt_design_vl_color)
        opt_design_vl['cmins'].set_color(opt_design_vl_color)
        opt_design_vl['cmedians'].set_color(opt_design_vl_color)
        opt_design_vl['cbars'].set_color(opt_design_vl_color)
        #
        bart_design_vl_color = line_colors[5]
        bart_design_vl['bodies'][0].set_color(bart_design_vl_color)
        bart_design_vl['bodies'][0].set_alpha(facecolor_alpha)
        bart_design_vl['cmeans'].set_color(bart_design_vl_color)
        bart_design_vl['cmaxes'].set_color(bart_design_vl_color)
        bart_design_vl['cmins'].set_color(bart_design_vl_color)
        bart_design_vl['cmedians'].set_color(bart_design_vl_color)
        bart_design_vl['cbars'].set_color(bart_design_vl_color)


        ###### ADUST PLOTS ######

        ## Set limits
        lim_0 = axes[0].get_ylim()
        lim_1 = axes[1].get_ylim()
        min_y = min(lim_0[0], lim_1[0])
        max_y = max(lim_0[1], lim_1[1])

        # Unify y-axis
        for ax in axes:
            ax.set_ylim(min_y, max_y)

        # Squeeze horizontally
        fig.subplots_adjust(wspace=0.01)

        ## Add Figure legend
        # Empty patch to modify legend
        empty_patch = mpatches.Patch(
            color="white",
            alpha=0,
            label=r" "
        )
        # Get current legend handles (from ax0) and add patch for violins
        d1_patch = mpatches.Patch(
            color=opt_design_vl["bodies"][0].get_facecolor().flatten(),
            label=r"Optimal Solution"
        )
        d2_patch = mpatches.Patch(
            color=bart_design_vl["bodies"][0].get_facecolor().flatten(),
            label=r"Best Along the Route",
        )

        handles, _ = axes[0].get_legend_handles_labels()
        handles.append(empty_patch)
        handles.append(d1_patch)
        handles.append(d2_patch)
        axes[0].legend(
            handles=handles,
            bbox_to_anchor=(0, 1.02, 1.18, 0.2), loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=3,
        )

        # Show Axis Grids
        if show_axis_grids:
            for ax in axes:
                utility.show_axis_grids(
                    ax=ax,
                    major_alpha=axis_grids_alpha[0],
                    minor_alpha=axis_grids_alpha[1],
                )

        ######

        saveto = os.path.join(output_dir, stats_filename)
        print("Saving/Plotting Results to: {0}".format(saveto))
        fig.savefig(saveto, dpi=700, bbox_inches="tight")
        # plt.close(fig)

    return fig, axes


###############################################
#        Bilinear Optimization Problem        #
###############################################
def unconstrained_bilinear_binary_optimization(
    size=20,
    num_resets=0,
    keep_plots=False,
    overwrite=False,
    output_dir=None,
):
    """
    Carry unconstrained binary optimization analysis
    """
    if output_dir is None:
        output_dir=os.path.join(
            get_base_output_dir(),
            "WeightedBilinear" if WEIGHTED_BILINEAR_OBJECTIVE else "Bilinear",
            f"SIZE_{size}_Unconstrained",
        )
    if not os.path.isdir(output_dir): os.makedirs(output_dir)

    results_file = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
    if os.path.isfile(results_file) and not overwrite:
        print("Found results. Terminating...")
        return

    # Define a test function
    if WEIGHTED_BILINEAR_OBJECTIVE:
        obj_fun = WeightedAlternatingSignOptimizationFunction(n=size)
    else:
        obj_fun = AlternatingSignOptimizationFunction(n=size)

    # Define the optimizer
    optimizer = BinaryReinforceOptimizer(
        configs=dict(
            size=size,
            obj_fun = obj_fun,
            decay_step=False,
            random_seed=RANDOM_SEED,
            #
            baseline=OPTIMIZATION_SETTINGS['baseline'],
            maximize=OPTIMIZATION_SETTINGS['maximize'],
            def_stepsize=OPTIMIZATION_SETTINGS['def_stepsize'],
            stochastic_gradient_sample_size=OPTIMIZATION_SETTINGS['stochastic_gradient_sample_size'],
            optimal_baseline_batch_size=OPTIMIZATION_SETTINGS['stochastic_gradient_sample_size'],
            optimal_sample_size=OPTIMIZATION_SETTINGS['optimal_sample_size'],
            maxiter=OPTIMIZATION_SETTINGS['maxiter'],
            pgtol=OPTIMIZATION_SETTINGS['pgtol'],
        )
    )

    # Solve
    results = optimizer.solve(num_resets=num_resets,)

    # Add objective for uniform random sample
    uniform_random_sample_objval = [
        optimizer.objective_function_value(s) for s in optimizer.generate_sample(
            theta=np.ones(size)*0.5,
            sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
        )
    ]
    results.update({'uniform_random_sample_objval': uniform_random_sample_objval})

    # Save results
    try:
        saveto = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Use optimizer plotter for visualization
    bruteforce = size <= 20
    figures = optimizer.plot_results(
        results,
        bruteforce=bruteforce,
        overwrite=overwrite,
        keep_plots=keep_plots,
        plots_format=PLOTS_FORMAT,
        verbose=True,
        output_dir=os.path.join(
            output_dir,
            "PLOTS",
        ),
    )

    # Write down optimizer configurations and results
    optimizer_configs = optimizer.configurations
    _ = optimizer_configs.pop('obj_fun')
    results.update(
        {
            'objective_value_tracker': optimizer_configs['objective_value_tracker']
        }
    )

    try:
        saveto = os.path.join(output_dir, OPTIMIZER_CONFIGURATIONS_FILE_NAME)
        pickle.dump(optimizer_configs, open(saveto, 'wb'))
        print(f"Optimizer configuations saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Configurations!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Overwrite results
    try:
        saveto = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )


    # Return for further inspection as needed
    return optimizer, results, figures

def constrained_bilinear_binary_optimization(
    size=20,
    budgets=10,
    num_resets=0,
    keep_plots=False,
    overwrite=False,
    output_dir=None,
):
    """
    Carry unconstrained binary optimization analysis
    """
    if utility.isnumber(budgets):
        budgets_str = str(budgets)
    elif utility.isiterable(budgets):
        budgets_str = f"RANGE_{str(max(budgets))}"
    else:
        budgets_str = f"UNKNOWN"
    if output_dir is None:
        output_dir=os.path.join(
            get_base_output_dir(),
            "WeightedBilinear" if WEIGHTED_BILINEAR_OBJECTIVE else "Bilinear",
            f"SIZE_{size}_Budgets_{budgets_str}",
        )
    if not os.path.isdir(output_dir): os.makedirs(output_dir)

    results_file = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
    if os.path.isfile(results_file) and not overwrite:
        print("Found results. Terminating...")
        return

    # Define a test function
    if WEIGHTED_BILINEAR_OBJECTIVE:
        obj_fun = WeightedAlternatingSignOptimizationFunction(n=size)
    else:
        obj_fun = AlternatingSignOptimizationFunction(n=size)

    # Define the optimizer
    optimizer = ConstrainedBinaryReinforceOptimizer(
        configs=dict(
            size=size,
            budgets=budgets,
            obj_fun = obj_fun,
            decay_step=False,
            random_seed=RANDOM_SEED,
            #
            baseline=OPTIMIZATION_SETTINGS['baseline'],
            maximize=OPTIMIZATION_SETTINGS['maximize'],
            def_stepsize=OPTIMIZATION_SETTINGS['def_stepsize'],
            stochastic_gradient_sample_size=OPTIMIZATION_SETTINGS['stochastic_gradient_sample_size'],
            optimal_baseline_batch_size=OPTIMIZATION_SETTINGS['stochastic_gradient_sample_size'],
            optimal_sample_size=OPTIMIZATION_SETTINGS['optimal_sample_size'],
            maxiter=OPTIMIZATION_SETTINGS['maxiter'],
            pgtol=OPTIMIZATION_SETTINGS['pgtol'],
        )
    )

    # Solve
    results = optimizer.solve(num_resets=num_resets,)

    # Add objective for uniform random sample
    uniform_random_sample_objval = [
        optimizer.objective_function_value(s) for s in optimizer.generate_sample(
            theta=np.ones(size)*0.5,
            sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
            enforce_constraint=False,  # Let it get handled by sampler
        )
    ]
    results.update({'uniform_random_sample_objval': uniform_random_sample_objval})

    # Save results
    try:
        saveto = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Use optimizer plotter for visualization
    bruteforce = size<=20
    figures = optimizer.plot_results(
        results,
        bruteforce=bruteforce,
        overwrite=overwrite,
        keep_plots=keep_plots,
        verbose=True,
        plots_format=PLOTS_FORMAT,
        output_dir=os.path.join(
            output_dir,
            "PLOTS",
        )
    )

    # Write down optimizer configurations and results
    optimizer_configs = optimizer.configurations
    _ = optimizer_configs.pop('obj_fun')
    results.update(
        {
            'objective_value_tracker': optimizer_configs['objective_value_tracker']
        }
    )

    try:
        saveto = os.path.join(output_dir, OPTIMIZER_CONFIGURATIONS_FILE_NAME)
        pickle.dump(optimizer_configs, open(saveto, 'wb'))
        print(f"Optimizer configuations saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Configurations!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Overwrite results
    try:
        saveto = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Return for further inspection as needed
    return optimizer, results, figures

def run_Bilinear_experiment():
    unconstrained_bilinear_binary_optimization(size=20, )
    constrained_bilinear_binary_optimization(size=20, budgets=10)
    constrained_bilinear_binary_optimization(size=20, budgets=range(11))

    # Go up in size
    constrained_bilinear_binary_optimization(size=100, budgets=10)
    constrained_bilinear_binary_optimization(size=100, budgets=range(11))

    # Go upper in size
    constrained_bilinear_binary_optimization(size=200, budgets=10)
    constrained_bilinear_binary_optimization(size=200, budgets=range(11))

def create_bilinear_experiment_from_results(
    results_dir,
):
    """
    Lookup the results dir for optimizer configurations and results, and
    use both to regenerate/recreate the problem.
    """
    settings_file = os.path.join(
        results_dir,
        OPTIMIZER_CONFIGURATIONS_FILE_NAME
    )
    results_file = os.path.join(
        results_dir,
        OPTIMIZATION_RESULTS_FILE_NAME
    )

    # Load settings:
    if os.path.isfile(settings_file):
        optimizer_configs = pickle.load(open(settings_file, 'rb'))
        size = optimizer_configs['size']
        try:
            offset = optimizer_configs['obj_fun_offset']
        except(KeyError):
            offset = 0.0
    else:
        raise IOError(
            f"The optimization settings file `{settings_file}` is not FOUND!"
        )


    # Define a test function
    if 'WeightedBilinear' in results_dir:
        obj_fun = WeightedAlternatingSignOptimizationFunction(n=size)
    else:
        obj_fun = AlternatingSignOptimizationFunction(n=size)
    optimizer_configs.update({'obj_fun': obj_fun})

    ## Create the optimization object (optimizer)
    print(f"Creating Optimizer...", end=" ")
    if 'budgets' in optimizer_configs:
        optimizer = ConstrainedBinaryReinforceOptimizer(
            configs=optimizer_configs
        )
    else:
        optimizer = BinaryReinforceOptimizer(
            configs=optimizer_configs,
        )
    print(f"done")

    print(f"Loading Optimization Results...", end=" ")
    # Load results
    if os.path.isfile(results_file):
        results = pickle.load(open(results_file, 'rb'))

        # Update optimizer configuration dictionary with objective value tracker
        optimizer._CONFIGURATIONS['objective_value_tracker'].update(
            results['objective_value_tracker']
        )

    else:
        results = None
        print(
            f"No results found for this problem. Setting results to None in the return"
        )
    print(f"done")

    return optimizer, results

def plot_bilinear_optimization_experiment_results(results_dir):
    """Lookup results directory, regenerate problem, and plot results"""
    ## Recreate the problem & load results
    optimizer, results = create_bilinear_experiment_from_results(
        results_dir=results_dir
    )
    optimizer_configs = optimizer.configurations
    size = optimizer_configs['size']

    # Update (old) files with random samples objective values
    if results is not None and 'uniform_random_sample_objval' not in results:
        print(f"Updating results with a unfirm random sample", end=" ")
        # Add objective for uniform random sample
        obj_fun = optimizer_configs['obj_fun']
        try:
            uniform_random_sample_objval = [
                obj_fun(s) for s in optimizer.generate_sample(
                    theta=np.ones(size)*0.5,
                    sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
                    enforce_constraint=False,  # Let it get handled by sampler
                )
            ]
        except:
            uniform_random_sample_objval = [
                obj_fun(s) for s in optimizer.generate_sample(
                    theta=np.ones(size)*0.5,
                    sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
                )
            ]
        results.update(
            {
                'uniform_random_sample_objval': uniform_random_sample_objval,
            },
        )
        print(f"done")

        # Overwrite results
        results_file = os.path.join(
            results_dir,
            OPTIMIZATION_RESULTS_FILE_NAME
        )
        print(f"Calculated random sample objective value; updating results file '{results_file}'")
        pickle.dump(results, open(results_file, 'wb'))

    if results is not None:
        ## Plot results
        bruteforce = size <= 20
        output_dir = os.path.join(results_dir, "PLOTS")
        try:
            optimizer.plot_results(
                results,
                bruteforce=bruteforce,
                overwrite=True,
                output_dir=output_dir,
                verbose=True,
                plots_format=PLOTS_FORMAT,
                keep_plots=False,
            )
        except Exception as err:
            print(
                f"Optimization Plotter Failed!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )

        # Update results if bruteforce is carried out (just in case)
        # Write down optimizer configurations and results
        optimizer_configs = optimizer.configurations
        _ = optimizer_configs.pop('obj_fun')
        results.update(
            {
                'objective_value_tracker': optimizer_configs['objective_value_tracker']
            }
        )

        try:
            saveto = os.path.join(results_dir, OPTIMIZER_CONFIGURATIONS_FILE_NAME)
            pickle.dump(optimizer_configs, open(saveto, 'wb'))
            print(f"Optimizer configuations saved to {saveto}")
        except Exception as err:
            print(
                f"Failed to save Optimization Configurations!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )

        # Overwrite results
        try:
            saveto = os.path.join(results_dir, OPTIMIZATION_RESULTS_FILE_NAME)
            pickle.dump(results, open(saveto, 'wb'))
            print(f"Optimization results saved to {saveto}")
        except Exception as err:
            print(
                f"Failed to save Optimization Results!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )

    return optimizer, results

def Bilinear_experiment_multiruns(
    num_runs=20,
    sizes=[100],
    budgets=10,
    weighted_bilinear=False,
    overwrite=False,
    random_seed=None,
):
    """
    Run the Bilinear experiment mulitple times and store results for each run
    """
    # Modify random seed and weighted objective
    global RANDOM_SEED, WEIGHTED_BILINEAR_OBJECTIVE
    _RANDOM_SEED = RANDOM_SEED
    RANDOM_SEED = random_seed

    _WEIGHTED_BILINEAR_OBJECTIVE = WEIGHTED_BILINEAR_OBJECTIVE
    WEIGHTED_BILINEAR_OBJECTIVE = weighted_bilinear

    if utility.isnumber(budgets):
        budgets_str = str(budgets)
    elif utility.isiterable(budgets):
        budgets_str = f"RANGE_{str(max(budgets))}"
    else:
        budgets_str = f"UNKNOWN"

    for size in sizes:
        for run_id in range(num_runs):
            output_dir = os.path.join(
                get_base_output_dir(),
                "WeightedBilinear" if WEIGHTED_BILINEAR_OBJECTIVE else "Bilinear",
                f"SIZE_{size}_Budgets_{budgets_str}",
                f"Run_{run_id+1}"
            )
            constrained_bilinear_binary_optimization(
                size=size,
                budgets=budgets,
                num_resets=0,
                keep_plots=False,
                overwrite=overwrite,
                output_dir=output_dir,
            )

    # Reset
    RANDOM_SEED = _RANDOM_SEED
    WEIGHTED_BILINEAR_OBJECTIVE = _WEIGHTED_BILINEAR_OBJECTIVE


###############################################
# OED (Sensor Placement) Optimization Problem #
###############################################
def create_oed_problem(num_candidate_sensors=INVERSE_PROBLEM_SETTINGS["num_candidate_sensors"]):
    """
    Create an OED problem with a simulation model (Advection-Diffusion) to provide
    an objective function for the optimization.

    :returns: 'oed_problem': a sensor placement oed problem with access to model, oed, etc.
    """
    model_timestep = INVERSE_PROBLEM_SETTINGS["model_timestep"]
    print(f"Creating simulation model...", end=" ")
    # Create the simulation model (AD with FE discretization)
    model = fenics_models.create_AdvectionDiffusion2D_model(dt=model_timestep)

    # Plot the domain
    ax = model.plot_domain(
        save_to_file=os.path.join(get_base_output_dir(), "OED_AD_FE", f"AD_Model_Domain.{PLOTS_FORMAT}")
    )
    plt.close(ax.figure)
    ax = model.plot_velocity_field(
        save_to_file=os.path.join(get_base_output_dir(), "OED_AD_FE", f"AD_Model_VelocityField.{PLOTS_FORMAT}")
    )
    plt.close(ax.figure)

    # Ground truth of the inversion parameter (initial condition here)
    true_model_state = model.create_initial_condition()
    print(f"done")


    print(f"Creating prior...", end=" ")
    ## Prior (BiLaplacian)
    configs = dict(
        Vh=model.parameter_dof,
        mean=model.parameter_vector(init_val=0.5),
        gamma=1.0,
        delta=16,
        random_seed=RANDOM_SEED,
        verbose=VERBOSE,
    )
    prior = Laplacian.DolfinBiLaplacianErrorModel(configs)
    print(f"done")


    print(f"Creating observation operator...", end=" ")
    ## Observation operator (`NUM_CANDIDATE_SENSORS` candidate sensor locations/gridpoints)
    obs_oper = create_DolfinPointWise_observation_operator(
        model=model,
        Vh=model.state_dof,
        num_obs_points=num_candidate_sensors,
    )
    print(f"done")

    print(f"Creating observation error/noise model...", end=" ")
    ## Observation error model
    obs_noise = PointwiseWeightedGaussianErrorModel(
        configs=dict(
            size=obs_oper.shape[0],
            variance=INVERSE_PROBLEM_SETTINGS["obs_err_variance"],
            design_weighting_scheme="covariance",
            random_seed=RANDOM_SEED,
            verbose=VERBOSE,
        ),
    )
    print(f"done")


    print(f"Creating inverse problem (DA) object...", end=" ")
    ## Inversion/assimilation object (4DVar smoother with 5 observation time points)
    init_obs_time = INVERSE_PROBLEM_SETTINGS["init_obs_time"]
    num_obs_time_points = INVERSE_PROBLEM_SETTINGS["num_obs_time_points"]
    checkpoints = np.arange(
        0, init_obs_time + model_timestep * (num_obs_time_points + 0.5), model_timestep
    )
    DA_configs = dict(
        assimilation_window=(checkpoints[0], checkpoints[-1]),
        model=model,
        prior_model=prior,
        observation_operator=obs_oper,
        observation_error_model=obs_noise,
        verbose=VERBOSE,
    )
    inverse_problem = fourDVar.VanillaFourDVar(configs=DA_configs)
    print(f"done")


    print(f"Registering observations to the DA object...", end=" ")
    # Create and register observations (perturb observation from true model trajectory)
    obs_times, true_obs = model.integrate_state(
        true_model_state,
        tspan=(checkpoints[0], checkpoints[-1]),
        checkpoints=checkpoints[checkpoints>=init_obs_time],
    )

    # Perturb with noise and register with the inverse problem
    for t, y in zip(obs_times, true_obs):
        y = obs_oper.apply(y)
        yobs = obs_noise.add_noise(y)
        inverse_problem.register_observation(t=t, observation=yobs)
    print(f"done")


    print(f"Creating OED object...", end=" ")
    oed_problem = binary_oed.SensorPlacementBinaryOED(
        configs=dict(
            inverse_problem=inverse_problem,
            def_stepsize=OPTIMIZATION_SETTINGS["def_stepsize"],
            stochastic_gradient_sample_size=OPTIMIZATION_SETTINGS[
                "stochastic_gradient_sample_size"
            ],
            verbose=VERBOSE,
        )
    )

    # Register the utility function: trace of the FIM (A-optimality)
    oed_problem.register_optimality_criterion("A-opt")
    oed_problem.oed_criterion.update_configurations(
        evaluation_method="randomized",
        use_FIM=True,
        **OED_RANDOMIZATION_SETTINGS,
    )
    print(f"done")

    return oed_problem

def create_oed_optimizer(
    oed_problem=None,
    size=INVERSE_PROBLEM_SETTINGS["num_candidate_sensors"],
    budgets=range(INVERSE_PROBLEM_SETTINGS["num_candidate_sensors"]//10+1),
    maxiter=OPTIMIZATION_SETTINGS["maxiter"],
    def_stepsize=OPTIMIZATION_SETTINGS["def_stepsize"],
    baseline=OPTIMIZATION_SETTINGS["baseline"],
    output_dir=os.path.join(get_base_output_dir(), "OPTIMIZER"),
):
    """
    Creat a constrained probabilistic optimizer

    :returns:
        - 'optimizer': an optimization object
        - 'offset': the offset (positive) subtracted from the optimizer's objective (zero if None)
    """
    ## If no OED problem is passed, create it.
    if oed_problem is None:
        oed_problem = create_oed_problem(num_candidate_sensors=size, )

    # Calculate a scaling factor for the OED objective to shift magnitude down
    d = oed_problem.inverse_problem.observation_error_model.design
    d[:] = 0
    offset = oed_problem.oed_objective(design=d)
    print(f"Setting the scale (by shifting) of the OED objective function to: {-offset}")

    print(f"Extracting optimization objective...", end=" ")
    ## Define the objective
    obj_fun = lambda d: oed_problem.oed_objective(d, verbose=VERBOSE, ) - offset
    print(f"done")

    print(f"Creating Optimizer...", end=" ")
    ## Create the optimization object (optimizer)
    configs = dict(
        obj_fun=obj_fun,
        size=oed_problem.design_size,
        maximize=oed_problem.oed_criterion.use_FIM,
        def_stepsize=def_stepsize,
        maxiter=maxiter,
        baseline=baseline,
        stochastic_gradient_sample_size=OPTIMIZATION_SETTINGS["stochastic_gradient_sample_size"],
        optimal_sample_size=OPTIMIZATION_SETTINGS["optimal_sample_size"],
        pgtol=OPTIMIZATION_SETTINGS["pgtol"],
        monitor_iterations=OPTIMIZATION_SETTINGS["monitor_iterations"],
        random_seed=RANDOM_SEED,
        antithetic=OPTIMIZATION_SETTINGS["antithetic"],
        output_dir=output_dir,
        verbose=VERBOSE,
        #
        budget_enforcement_approach="sum-as-variable",
        budgets=budgets,
    )
    optimizer = ConstrainedBinaryReinforceOptimizer(
        configs=configs
    )
    print(f"done")

    return oed_problem, optimizer, offset

def create_oed_optimization_experiments_from_results(
    results_dir,
):
    """
    Lookup the results dir for optimizer configurations and results, and
    use both to regenerate/recreate the problem.
    """
    settings_file = os.path.join(
        results_dir,
        OPTIMIZER_CONFIGURATIONS_FILE_NAME
    )
    results_file = os.path.join(
        results_dir,
        OPTIMIZATION_RESULTS_FILE_NAME
    )

    # Load settings:
    if os.path.isfile(settings_file):
        optimizer_configs = pickle.load(open(settings_file, 'rb'))
        size = optimizer_configs['size']
        try:
            offset = optimizer_configs['obj_fun_offset']
        except(KeyError):
            offset = 0.0
    else:
        raise IOError(
            f"The optimization settings file `{settings_file}` is not FOUND!"
        )

    # Create the problem with those settings
    oed_problem = create_oed_problem(num_candidate_sensors=size )

    # Calculate a scaling factor for the OED objective to shift magnitude down
    print(f"Setting the scale (by shifting) of the OED objective function to: {-offset}")

    print(f"Extracting optimization objective...", end=" ")
    ## Define the objective
    obj_fun = lambda d: oed_problem.oed_objective(d, verbose=VERBOSE, ) - offset
    print(f"done")

    # Add the offset-adjusted objective to the optimizer configurations dictionary
    optimizer_configs.update({'obj_fun': obj_fun})

    print(f"Creating Optimizer...", end=" ")
    ## Create the optimization object (optimizer)
    optimizer = ConstrainedBinaryReinforceOptimizer(
        configs=optimizer_configs
    )
    print(f"done")

    print(f"Loading Optimization Results...", end=" ")
    # Load results
    if os.path.isfile(results_file):
        results = pickle.load(open(results_file, 'rb'))

        # Update optimizer configuration dictionary with objective value tracker
        optimizer._CONFIGURATIONS['objective_value_tracker'].update(
            results['objective_value_tracker']
        )

    else:
        results = None
        print(
            f"No results found for this problem. Setting results to None in the return"
        )
    print(f"done")

    return oed_problem, optimizer, results

def plot_oed_optimization_experiment_results(results_dir):
    """Lookup results directory, regenerate problem, and plot results"""
    ## Recreate the problem & load results
    oed_problem, optimizer, results = create_oed_optimization_experiments_from_results(
        results_dir=results_dir
    )
    optimizer_configs = optimizer.configurations
    size = optimizer_configs['size']

    # Update (old) files with random samples objective values
    if results is not None and 'uniform_random_sample_objval' not in results:
        print(f"Updating results with a unfirm random sample", end=" ")
        # Add objective for uniform random sample
        obj_fun = optimizer_configs['obj_fun']
        try:
            uniform_random_sample_objval = [
                obj_fun(s) for s in optimizer.generate_sample(
                    theta=np.ones(size)*0.5,
                    sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
                    enforce_constraint=False,  # Let it get handled by sampler
                )
            ]
        except:
            uniform_random_sample_objval = [
                obj_fun(s) for s in optimizer.generate_sample(
                    theta=np.ones(size)*0.5,
                    sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
                )
            ]
        results.update(
            {
                'uniform_random_sample_objval': uniform_random_sample_objval,
            },
        )
        print(f"done")

        # Overwrite results
        results_file = os.path.join(
            results_dir,
            OPTIMIZATION_RESULTS_FILE_NAME
        )
        print(f"Calculated random sample objective value; updating results file '{results_file}'")
        pickle.dump(results, open(results_file, 'wb'))

    ## Plot results
    if results is not None:
        bruteforce = size <= 20
        output_dir=os.path.join(results_dir, "PLOTS", )
        try:
            optimizer.plot_results(
                results,
                bruteforce=bruteforce,
                overwrite=True,
                output_dir=output_dir,
                verbose=True,
                plots_format=PLOTS_FORMAT,
                keep_plots=False,
            )
        except Exception as err:
            print(
                f"Optimization Plotter Failed!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )

        # Write down optimizer configurations and results
        optimizer_configs = optimizer.configurations
        _ = optimizer_configs.pop('obj_fun')
        results.update(
            {
                'objective_value_tracker': optimizer_configs['objective_value_tracker']
            }
        )

        try:
            saveto = os.path.join(results_dir, OPTIMIZER_CONFIGURATIONS_FILE_NAME)
            pickle.dump(optimizer_configs, open(saveto, 'wb'))
            print(f"Optimizer configuations saved to {saveto}")
        except Exception as err:
            print(
                f"Failed to save Optimization Configurations!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )

        # Overwrite results
        try:
            saveto = os.path.join(results_dir, OPTIMIZATION_RESULTS_FILE_NAME)
            pickle.dump(results, open(saveto, 'wb'))
            print(f"Optimization results saved to {saveto}")
        except Exception as err:
            print(
                f"Failed to save Optimization Results!"
                f"Unexpected: \n{err=} of {type(err)=}"
            )


    ## Plot sensors using the models & utility plotters
    if results is not None:

        # Get observation grid
        observation_grid = (
            oed_problem.inverse_problem.observation_operator.get_observation_grid()
        )

        # Plot the domain
        saveto = os.path.join(output_dir, f"AD_Model_Domain.{PLOTS_FORMAT}", )
        ax = oed_problem.inverse_problem.model.plot_domain(
            save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            fontsize=PLOTS_FONTSIZE,
        )
        plt.close(ax.figure)

        # Plot velocity field
        saveto = os.path.join(output_dir, f"AD_Model_VelocityField.{PLOTS_FORMAT}")
        ax = oed_problem.inverse_problem.model.plot_velocity_field(
            save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            fontsize=PLOTS_FONTSIZE,
        )
        plt.close(ax.figure)

        # Plot True initial condition (inference parameter)
        saveto = os.path.join(output_dir, f"AD_Model_True_IC.{PLOTS_FORMAT}")
        oed_problem.inverse_problem.model.plot_state(
            oed_problem.inverse_problem.model.true_initial_condition,
            save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            fontsize=PLOTS_FONTSIZE,
        )

        # Plot Candidate Sensor Locations (Full observation grid)
        for annotate_sensors in [False, True]:
            saveto = os.path.join(output_dir, f"AD_Model_FullObservationGrid{'_Annotated' if annotate_sensors else ''}.{PLOTS_FORMAT}", )
            ax = oed_problem.inverse_problem.model.plot_domain(
                save_to_file=saveto,
                boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
                sensors_coordinates=observation_grid,
                show_mesh=True,
                mesh_alpha=0.1,
                annotate_sensors=annotate_sensors,
                boxes_texts=[r'$B_1$', r'$B_2$'],
                fontsize=PLOTS_FONTSIZE,
            )
            plt.close(ax.figure)

        # Plot Optimal Design
        optimal_design = results['optimal_design']
        saveto = os.path.join(output_dir, f"optimal_design.{PLOTS_FORMAT}")
        sensors = observation_grid[np.where(optimal_design)[0], :]
        ax = oed_problem.inverse_problem.model.plot_domain(
            sensors_coordinates=sensors,
            show_mesh=True,
            mesh_alpha=0.1,
            # save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            # title="Optimum Design"
        )
        ax.set_aspect("equal")
        fig = ax.figure

        # Calculate triangulation grid
        x = observation_grid[:, 0]
        y = observation_grid[:, 1]
        triang = mtri.Triangulation(x, y)

        # Plot optimal policy
        optimal_policy = results['optimal_policy']
        trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)

        # Divide existing axes and create
        # divider = make_axes_locatable(ax)
        # cax = divider.append_axes("right", size="5%", pad=0.0, )
        fig.colorbar(trf, shrink=0.79, pad=0.0, )

        fig.savefig(saveto, bbox_inches='tight', dpi=700)
        plt.close(fig)
        print(f"Plot saved to {saveto}")

        # Plot Best along the route
        best_along_the_route = results['best_along_the_route']
        if best_along_the_route is not None:
            best_along_the_route_sample = best_along_the_route['designs']
            for i, design in enumerate(best_along_the_route_sample):
                saveto = os.path.join(
                    output_dir, f"best_along_the_route_{i+1}.{PLOTS_FORMAT}"
                )
                sensors = observation_grid[np.where(design)[0], :]
                ax = oed_problem.inverse_problem.model.plot_domain(
                    sensors_coordinates=sensors,
                    show_mesh=True,
                    mesh_alpha=0.1,
                    save_to_file=saveto,
                    boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
                    boxes_texts=[r'$B_1$', r'$B_2$'],
                    # title=f"Best Along the Route {i+1}/{len(best_along_the_route_sample)}"
                )
                ax.set_aspect("equal")
                fig = ax.figure
                trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)
                # divider = make_axes_locatable(ax)
                # cax = divider.append_axes("right", size="5%", pad=0.0, )
                fig.colorbar(trf, shrink=0.79, pad=0.0, )
                fig.savefig(saveto, bbox_inches='tight', dpi=700)
                print(f"Plot saved to {saveto}")
                plt.close(fig)

        # Plot optimal design sample
        optimal_policy_sample = results['optimal_policy_sample']
        for i, design in enumerate(optimal_policy_sample):
            saveto = os.path.join(
                output_dir, f"optimum_design_sample_{i+1}.{PLOTS_FORMAT}"
            )
            sensors = observation_grid[np.where(design)[0], :]
            ax = oed_problem.inverse_problem.model.plot_domain(
                sensors_coordinates=sensors,
                show_mesh=True,
                mesh_alpha=0.1,
                save_to_file=saveto,
                boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
                boxes_texts=[r'$B_1$', r'$B_2$'],
                # title=f"Sampled Design {i+1}/{len(optimal_policy_sample)}"
            )
            ax.set_aspect("equal")
            fig = ax.figure
            trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)
            # divider = make_axes_locatable(ax)
            # cax = divider.append_axes("right", size="5%", pad=0.0, )
            fig.colorbar(trf, shrink=0.79, pad=0.0, )
            fig.savefig(saveto, bbox_inches='tight', dpi=700)
            print(f"Plot saved to {saveto}")
            plt.close(fig)

    return oed_problem, optimizer, results

def run_oed_optimization_experiment(
    size,
    budgets,
    overwrite=False,
    output_dir=None,
):
    """Run the optimization experiment"""
    if utility.isnumber(budgets):
        budgets_str = str(budgets)
    elif utility.isiterable(budgets):
        budgets_str = f"RANGE_{str(max(budgets))}"
    else:
        budgets_str = f"UNKNOWN"

    if output_dir is None:
        output_dir=os.path.join(
            get_base_output_dir(),
            "OED_AD_FE",
            f"SIZE_{size}_BUDGETS_{budgets_str}",
        )
    if not os.path.isdir(output_dir): os.makedirs(output_dir)

    results_file = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
    if os.path.isfile(results_file) and not overwrite:
        print("Found results. Terminating...")
        return

    # Create optimizer
    oed_problem, optimizer, offset = create_oed_optimizer(
        size=size,
        budgets=budgets,
        output_dir=os.path.join(output_dir, "OPTIMIZER"),
    )

    # Write down optimizer configurations and results

    # Solve and save results
    results = optimizer.solve()
    results.update({'obj_fun_offset': offset})

    # Add objective for uniform random sample
    uniform_random_sample_objval = [
        optimizer.objective_function_value(s) for s in optimizer.generate_sample(
            theta=np.ones(size)*0.5,
            sample_size=UNIFORM_RANDOM_SAMPLE_SIZE,
            enforce_constraint=False,  # Let it get handled by sampler
        )
    ]
    results.update({'uniform_random_sample_objval': uniform_random_sample_objval})

    try:
        saveto = os.path.join(output_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Plot Results
    bruteforce = size <= 20
    output_dir=os.path.join(output_dir,"PLOTS", )
    try:
        figures = optimizer.plot_results(
            results,
            bruteforce=bruteforce,
            overwrite=overwrite,
            output_dir=output_dir,
            verbose=True,
            plots_format=PLOTS_FORMAT,
            keep_plots=False,
        )
    except Exception as err:
        figures = None
        print(
            f"Optimization Plotter Failed!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Write down optimizer configurations and results
    optimizer_configs = optimizer.configurations
    _ = optimizer_configs.pop('obj_fun')
    optimizer_configs.update({'obj_fun_offset': offset})
    results.update(
        {
            'objective_value_tracker': optimizer_configs['objective_value_tracker']
        }
    )

    try:
        saveto = os.path.join(results_dir, OPTIMIZER_CONFIGURATIONS_FILE_NAME)
        pickle.dump(optimizer_configs, open(saveto, 'wb'))
        print(f"Optimizer configuations saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Configurations!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Overwrite results
    try:
        saveto = os.path.join(results_dir, OPTIMIZATION_RESULTS_FILE_NAME)
        pickle.dump(results, open(saveto, 'wb'))
        print(f"Optimization results saved to {saveto}")
    except Exception as err:
        print(
            f"Failed to save Optimization Results!"
            f"Unexpected: \n{err=} of {type(err)=}"
        )

    # Plot sensors using the models & utility plotters
    # Plot Optimal Design
    observation_grid = (
        oed_problem.inverse_problem.observation_operator.get_observation_grid()
    )

    # Plot the domain
    saveto = os.path.join(output_dir, f"AD_Model_Domain.{PLOTS_FORMAT}", )
    ax = oed_problem.inverse_problem.model.plot_domain(
        save_to_file=saveto,
        boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
        boxes_texts=[r'$B_1$', r'$B_2$'],
        fontsize=PLOTS_FONTSIZE,
    )
    plt.close(ax.figure)

    # Plot velocity field
    saveto = os.path.join(output_dir, f"AD_Model_VelocityField.{PLOTS_FORMAT}")
    ax = oed_problem.inverse_problem.model.plot_velocity_field(
        save_to_file=saveto,
        boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
        boxes_texts=[r'$B_1$', r'$B_2$'],
        fontsize=PLOTS_FONTSIZE,
    )
    plt.close(ax.figure)

    # Plot True initial condition (inference parameter)
    saveto = os.path.join(output_dir, f"AD_Model_True_IC.{PLOTS_FORMAT}")
    oed_problem.inverse_problem.model.plot_state(
        oed_problem.inverse_problem.model.true_initial_condition,
        save_to_file=saveto,
        boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
        boxes_texts=[r'$B_1$', r'$B_2$'],
        fontsize=PLOTS_FONTSIZE,
    )

    # Plot Candidate Sensor Locations (Full observation grid)
    for annotate_sensors in [False, True]:
        saveto = os.path.join(output_dir, f"AD_Model_FullObservationGrid{'_Annotated' if annotate_sensors else ''}.{PLOTS_FORMAT}", )
        ax = oed_problem.inverse_problem.model.plot_domain(
            save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            sensors_coordinates=observation_grid,
            show_mesh=True,
            mesh_alpha=0.1,
            annotate_sensors=annotate_sensors,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            fontsize=PLOTS_FONTSIZE,
        )
        plt.close(ax.figure)

    # Plot Optimal design
    optimal_design = results['optimal_design']
    saveto = os.path.join(output_dir, f"optimal_design.{PLOTS_FORMAT}")
    sensors = observation_grid[np.where(optimal_design)[0], :]
    ax = oed_problem.inverse_problem.model.plot_domain(
        sensors_coordinates=sensors,
        show_mesh=True,
        mesh_alpha=0.1,
        # save_to_file=saveto,
        boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
        boxes_texts=[r'$B_1$', r'$B_2$'],
        # title="Optimum Design"
    )
    ax.set_aspect("equal")
    fig = ax.figure

    # Calculate triangulation grid
    x = observation_grid[:, 0]
    y = observation_grid[:, 1]
    triang = mtri.Triangulation(x, y)

    # Plot optimal policy
    optimal_policy = results['optimal_policy']
    trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)
    # divider = make_axes_locatable(ax)
    # cax = divider.append_axes("right", size="5%", pad=0.0, )
    fig.colorbar(trf, shrink=0.79, pad=0.0, )

    fig.savefig(saveto, bbox_inches='tight', dpi=700)
    plt.close(fig)
    print(f"Plot saved to {saveto}")

    # Plot Best along the route
    best_along_the_route = results['best_along_the_route']
    if best_along_the_route is not None:
        best_along_the_route_sample = best_along_the_route['designs']
        for i, design in enumerate(best_along_the_route_sample):
            saveto = os.path.join(
                output_dir, f"best_along_the_route_{i+1}.{PLOTS_FORMAT}"
            )
            sensors = observation_grid[np.where(design)[0], :]
            ax = oed_problem.inverse_problem.model.plot_domain(
                sensors_coordinates=sensors,
                show_mesh=True,
                mesh_alpha=0.1,
                # save_to_file=saveto,
                boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
                boxes_texts=[r'$B_1$', r'$B_2$'],
                # title=f"Best Along the Route {i+1}/{len(best_along_the_route_sample)}"
            )
            ax.set_aspect("equal")
            fig = ax.figure
            trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)
            # divider = make_axes_locatable(ax)
            # cax = divider.append_axes("right", size="5%", pad=0.0, )
            fig.colorbar(trf, shrink=0.79, pad=0.0, )
            fig.savefig(saveto, bbox_inches='tight', dpi=700)
            print(f"Plot saved to {saveto}")
            plt.close(fig)

    # Plot optimal design sample
    optimal_policy_sample = results['optimal_policy_sample']
    for i, design in enumerate(optimal_policy_sample):
        saveto = os.path.join(
            output_dir, f"optimum_design_sample{i+1}.{PLOTS_FORMAT}"
        )
        sensors = observation_grid[np.where(design)[0], :]
        ax = oed_problem.inverse_problem.model.plot_domain(
            sensors_coordinates=sensors,
            show_mesh=True,
            mesh_alpha=0.1,
            # save_to_file=saveto,
            boxes_coordinates=fenics_models.advection_diffusion._AD_20__BUILDINGS_COORDINATES,
            boxes_texts=[r'$B_1$', r'$B_2$'],
            # title=f"Sampled Design {i+1}/{len(optimal_policy_sample)}"
        )
        ax.set_aspect("equal")
        fig = ax.figure
        trf = ax.tricontourf(triang, optimal_policy, levels=np.linspace(0., 1., 11), zorder=-100)
        # divider = make_axes_locatable(ax)
        # cax = divider.append_axes("right", size="5%", pad=0.0, )
        fig.colorbar(trf, shrink=0.79, pad=0.0, )
        fig.savefig(saveto, bbox_inches='tight', dpi=700)
        print(f"Plot saved to {saveto}")
        plt.close(fig)

    # Return for further inspection as needed
    return oed_problem, optimizer, results, figures

def oed_experiment_multiruns(
    num_runs=20,
    sizes=[100],
    budgets=10,
    overwrite=False,
    random_seed=None,
):
    """
    Run the Bilinear experiment mulitple times and store results for each run
    """
    # Modify random seed
    global RANDOM_SEED
    _RANDOM_SEED = RANDOM_SEED
    RANDOM_SEED = random_seed

    if utility.isnumber(budgets):
        budgets_str = str(budgets)
    elif utility.isiterable(budgets):
        budgets_str = f"RANGE_{str(max(budgets))}"
    else:
        budgets_str = f"UNKNOWN"

    for size in sizes:
        for run_id in range(num_runs):
            output_dir = os.path.join(
                get_base_output_dir(),
                "OED_AD_FE",
                f"SIZE_{size}_Budgets_{budgets_str}",
                f"Run_{run_id+1}"
            )
            run_oed_optimization_experiment(
                size=size,
                budgets=budgets,
                overwrite=overwrite,
                output_dir=output_dir,
            )

    # Reset
    RANDOM_SEED = _RANDOM_SEED
    WEIGHTED_BILINEAR_OBJECTIVE = _WEIGHTED_BILINEAR_OBJECTIVE



###############################################
#  Additional Drivers                         #
###############################################
def tiny_size_oed_problem():
    """Optimization problem with tiny settings (for testing)"""
    run_oed_optimization_experiment(
        size=10,
        budgets=2,
    )

def small_size_oed_problem():
    """Optimization problem with small settings (for discussion in paper)"""
    for inclusion in [False, True]:
        run_oed_optimization_experiment(
            size=20,
            budgets=range(10) if inclusion else 10,
        )

def large_size_oed_roblem():
    """Optimization problem with large settings (for discussion in paper)"""
    for inclusion in [False, True]:
        run_oed_optimization_experiment(
            size=100,
            budgets=range(10) if inclusion else 10,
        )


# TODO:
# 1- Model a function that studies several runs with different seeds, and plots the envelop of objective values for a given setup
# 2- Model a function that analyzes the effect of varying stepsize (0.1, 0.2, 0.3, ..., 1) over a large number of maxiter (multiple runs on each)
# 3- ?!


def main():
    unconstrained_bilinear_binary_optimization()
    constrained_bilinear_binary_optimization()
    tiny_size_oed_problem()
    small_size_oed_problem()
    large_size_oed_problem()


if __name__ == "__main__":
    main()

