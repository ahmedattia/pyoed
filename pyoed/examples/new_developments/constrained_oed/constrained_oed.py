#!/usr/bin/env python
# coding: utf-8

# In[26]:


import numpy as np
import matplotlib.pyplot as plt

from pyoed.optimization import stochastic_optimization
from pyoed import utility
from pyoed.stats import bernoulli as s_bernoulli


# In[33]:


def objective(design):
    if np.all(design==[0, 0]):
        return 4
    elif np.all(design==[0, 1]):
        return 8
    elif np.all(design==[1, 0]):
        return 2
    elif np.all(design==[1, 1]):
        return 10
    else:
        raise TypeError("Unrecognized design")
        
def sum_probability(design, theta):
    k = np.sum(design)
    if k == 0:
        prob = (1-theta[0]) * (1-theta[1])
    elif k == 1:
        prob = theta[0] * (1-theta[1]) + theta[1] * (1-theta[0])
    elif k == 2:
        # prob = theta[0] * theta[1]
        prob = 0
    else:
        raise ValueError("Unrecognized k value")
    return prob


# In[42]:


def weighted_binary_stochastic_objective(objfun, theta, apply_weight=True, obj_val_tracker=None, verbose=True):
    
    theta = np.asarray(theta, dtype=float).flatten()
    num_candidates = theta.size
    
    assert(num_candidates==2), "Tailored for 2D only"

    # All possible combinations;
    # Better use combinations if num_active is None
    combs = 2**num_candidates

    if obj_val_tracker is None:
        obj_val_tracker = dict()

    if verbose:
        print(
            "Evaluating Stocahstic Objective Function E(U(design)) for all possible binary designs"
        )

    stoch_objval = 0.0

    # Full bruteforce
    for k in range(1, combs + 1):
        if k in obj_val_tracker:
            if verbose:
                print(
                    "\rCandidate [{0:5d}/{1:5d}]".format(k, combs),
                    end="...FOUND...     ",
                )
            j = obj_val_tracker[k]
        else:
            if verbose:
                print(
                    "\rCandidate [{0:5d}/{1:5d}]".format(k, combs),
                    end="...Evaluating...",
                )

            design = utility.index_to_binary_state(k, size=num_candidates)
            j = objfun(design)
            obj_val_tracker.update({k: j})

        # Calculate the weight (from the sum of Bernoulli random variables)
        if apply_weight:
            weight = sum_probability(design, theta)
        else:
            weight = 1.0
            
        # update objective with term U(design) * p(design)
        if np.sum(design) != 2:
            prob = s_bernoulli.pmf(design, theta)[1]
        else:
            prob = 0
        stoch_objval += weight * prob * j

    if verbose:
        print("\nDone.")
    return stoch_objval


# In[43]:


weighted_binary_stochastic_objective(objective, [0, 0])


# In[44]:


x = np.linspace(0, 1, 25)
X, Y = np.meshgrid(x, x)


# In[45]:


expect_no_weight = np.empty_like(X)
expect_with_weight = np.empty_like(X)

for i in range(x.size):
    for j in range(x.size):
        print(f"\r{i*x.size+j}/{X.size}", end="")
        theta = [x[i], x[j]]
        expect_no_weight[i, j] = weighted_binary_stochastic_objective(objective, theta, apply_weight=False)
        expect_with_weight[i, j] = weighted_binary_stochastic_objective(objective, theta, apply_weight=True)


# In[46]:


fig = plt.figure(figsize=(7, 7))

ax = fig.add_subplot(111, projection="3d")
ax.plot_surface(X, Y, expect_with_weight)
ax.plot_surface(X, Y, expect_no_weight)


# In[47]:


plt.show()


# In[ ]:




