# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Driver script to test the components of 4DVar data assimilation system with several simulation models and observation operators.
A user should use these drivers only as examples, and adapt them to their needs.
"""

# Essential imports
import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# Add PyOED root path for proper access
pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility


# Import PyOED  modules (model, observation operator, error models)
import pyoed
from pyoed import utility

# simulation model(s)
from pyoed.models.simulation_models import (
    toy_linear,
    advection_diffusion,
    bateman_burgers,
    lorenz,
    fenics_models,
)

# observation operators(s)
from pyoed.models.observation_operators import (
    identity,
    interpolation,
    fenics_observation_operators,
)

# error models
from pyoed.models.error_models import (
    Gaussian,
    Laplacian,
)

# data assimilation (inversion)
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar as FourDVar

# from pyoed.assimilation import reduced_order_modeling


# Set (module level) figures default formatting
utility.plots_enhancer(
    fontsize=20,
    fontweight="bold",
    usetex=True,
)
# Global(ish) variables
__PLOTS_FORMAT = "pdf"


def fourDVar_AD_1d(
    domain=(-1, 1),
    nu=0.05,
    c=0.75,
    nx=51,
    dt=0.01,
    checkpoints=np.linspace(0, 0.2, 15),
    observation_error_variance=1e-4,
    observation_operator_configs=dict(type="identity", nx=15),
    prior_variance=1e-2,
    gradient_fd_validation=True,
    Hessian_fd_validation=True,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-AD-1D"),
):
    """
    Test 4D-Var with 1D Advection-Diffusion model

    :param domain:
    :param nu:
    :param c:
    :param nx:
    :param dt:
    :param checkpoints: [t0,  t1, ..., tf] observations are made at t1, ...,  tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Advection-Diffusion 1D)
    # Instantiate the 1D Advection Diffusion object
    model_configs = dict(domain=domain, nu=nu, c=c, nx=nx, dt=dt)
    model = advection_diffusion.AdvectionDiffusion1D(model_configs)
    model_grid = model.get_model_grid()

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = x0 = u0_true + prior.generate_noise()

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        obs_nx = observation_operator_configs["nx"]
        if obs_nx == nx:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0, "at least one observation point is needed"
            observation_grid = np.linspace(domain[0], domain[1], obs_nx)
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    elif obs_operator_type == "random":
        # Randomly select set of the simulation gridpoints
        obs_nx = observation_operator_configs["nx"]
        if obs_nx == nx:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0, "at least one observation point is needed"
            state_size = np.size(model_grid, 0)
            locs = rng.choice(state_size, size=obs_nx, replace=False)
            locs.sort()
            observation_grid = model_grid[locs].copy()
            # TODO: there should be something simpler
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=observation_grid.size,
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )

    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=x0,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(x0, tspan=tspan, checkpoints=checkpoints)
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition
              """.format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")
        try:
            # fancy plot settings
            line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
            line_width = 2

            fig = plt.figure(figsize=(16, 8))
            ax = fig.add_subplot(121)
            for i in [
                0,
                len(checkpoints) // 2,
                -1,
            ]:  # indices for start, middle, end time points
                # range(np.min([10,len(checkpoints)])): #limit the number of max line plots for better interpretability
                lbl1 = r"True Trajectory" if i == 0 else None
                lbl2 = r"Background Trajectory" if i == 0 else None
                lbl3 = r"Analysis Trajectory" if i == 0 else None

                ax.plot(
                    model_grid,
                    true_traj[i],
                    linestyle="-",
                    linewidth=line_width,
                    color=line_colors[0],
                    label=lbl1,
                )
                ax.plot(
                    model_grid,
                    back_traj[i],
                    linestyle="--",
                    linewidth=line_width,
                    color=line_colors[1],
                    label=lbl2,
                )
                ax.plot(
                    model_grid,
                    assim_traj[i],
                    linestyle="-.",
                    linewidth=line_width,
                    color=line_colors[2],
                    label=lbl3,
                )

            ax.legend(loc="best", framealpha=0.5)
            ax.set_xlabel(r"$x$")
            ax.set_ylabel(r"$u(x, t)$")
            ax.set_title(r"Solution/Simulation", fontsize=20)

            # Plot rmse over time
            ax = fig.add_subplot(122)
            lbl2 = r"Background Trajectory"
            lbl3 = r"Analysis Trajectory"
            ax.plot(
                checkpoints,
                rmseb,
                linestyle="--",
                linewidth=line_width,
                color=line_colors[1],
                label=lbl2,
            )
            ax.plot(
                checkpoints,
                rmsea,
                linestyle="-.",
                linewidth=line_width,
                color=line_colors[2],
                label=lbl3,
            )

            ax.legend(loc="best", framealpha=0.5)
            ax.set_xlabel(r"Time $t$")
            ax.set_ylabel(r"RMSE")
            ax.set_title(r"Root-Mean-Squared-Error")
            fig.subplots_adjust(wspace=0.3)

            # save plot
            if not os.path.isdir(output_dir):
                os.makedirs(output_dir)
            saveto = os.path.join(output_dir, "AD_1D_4DVAR.{0}".format(__PLOTS_FORMAT))
            print("Saving/Plotting Results to: {0}".format(saveto))
            # plt.show()
            fig.savefig(saveto, dpi=600, bbox_inches="tight")
            plt.close(fig)
        except:
            plt.close("all")
            pass

    # Return the problem now for testing; remove later
    return problem


def fourDVar_AD_2d(
    domain=((0, 1), (0, 1)),
    nu=0.01,
    cx=0.75,
    cy=0.75,
    nx=26,
    ny=26,
    dt=0.05,
    checkpoints=np.linspace(0, 0.5, 6),
    observation_operator_configs=dict(type="identity", nx=5, ny=5),
    observation_error_variance=1e-4,
    prior_variance=1e-2,
    gradient_fd_validation=True,
    Hessian_fd_validation=True,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-AD-2D"),
):
    """
    Test 4D-Var with 2D Advection-Diffusion model

    :param domain:
    :param nu:
    :param c:
    :param nx:
    :param dt:
    :param checkpoints: [t0, t1, ..., tf] observations are made at t1, ..., tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Advection-Diffusion 2D)
    # Instantiate the 2D Advection Diffusion object
    model = advection_diffusion.AdvectionDiffusion2D(
        configs=dict(
            domain=domain,
            nu=nu,
            cx=cx,
            cy=cy,
            nx=nx,
            ny=ny,
            dt=dt,
        )
    )

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = u0_true + prior.generate_noise()
    x0 = (
        prior.mean
    )  # u0_true + prior.generate_noise()  # initial guess at t0; IC

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        obs_nx = observation_operator_configs["nx"]
        obs_ny = observation_operator_configs["ny"]
        if obs_nx == nx and obs_ny == ny:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx or obs_ny > ny:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0 and obs_ny > 0, "at least one observation point is needed"
            _x = rng.uniform(low=domain[0][0], high=domain[0][1], size=obs_nx)
            _y = rng.uniform(low=domain[1][0], high=domain[1][1], size=obs_ny)
            [xx, yy] = np.meshgrid(_x, _y)
            observation_grid = np.hstack([xx.reshape(-1, 1), yy.reshape(-1, 1)])
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    elif obs_operator_type == "random":
        # Randomly select set of the simulation gridpoints
        obs_nx = observation_operator_configs["nx"]
        if obs_nx == nx and obs_ny == ny:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx or obs_ny > ny:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0 and obs_ny > 0, "at least one observation point is needed"
            state_size = np.size(model_grid, 0)
            locs = rng.choice(state_size, size=obs_nx * ny, replace=False)
            locs.sort()
            observation_grid = model_grid[locs, :].copy()
            # TODO: there should be something simpler
            # There is a SelectionOperator now that should replace this
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=np.size(observation_grid, 0),
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=prior.mean,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(x0, tspan=tspan, checkpoints=checkpoints)
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")

        # Plot contour plots at some selected time point (start, middle, end)
        fig, ax = plt.subplots(nrows=3, ncols=3, figsize=(20, 15))
        colormap = "RdBu_r"
        ind = [
            0,
            len(checkpoints) // 2,
            -1,
        ]  # indices for start, middle, end time points
        nlvl = 120
        for i in range(3):
            ipl = ind[i]
            lvl0 = true_traj[ipl].min()
            lvl1 = true_traj[ipl].max()
            lvl = np.linspace(lvl0, lvl1, nlvl, endpoint=True)
            ctick = np.linspace(lvl0, lvl1, 5, endpoint=True)

            ct = ax[0][i].tricontourf(
                model_grid[:, 0],
                model_grid[:, 1],
                true_traj[ipl],
                lvl,
                cmap=colormap,
                extend="both",
            )
            ct.set_clim([lvl0, lvl1])
            fig.colorbar(ct, ax=ax[0][i], ticks=ctick, orientation="vertical")
            ax[0][i].set_title(
                r"\bf{True Trajectory at t = }" + str(checkpoints[ipl]), fontsize=20
            )
            ax[0][i].set_xlabel(r"$x$")
            ax[0][i].set_ylabel(r"$y$")

            cb = ax[1][i].tricontourf(
                model_grid[:, 0],
                model_grid[:, 1],
                back_traj[ipl],
                lvl,
                cmap=colormap,
                extend="both",
            )
            cb.set_clim([lvl0, lvl1])
            fig.colorbar(cb, ax=ax[1][i], ticks=ctick, orientation="vertical")
            ax[1][i].set_title(
                r"\bf{Background Trajectory at t = }" + str(checkpoints[ipl]),
                fontsize=20,
            )
            ax[1][i].set_xlabel(r"$x$")
            ax[1][i].set_ylabel(r"$y$")

            ca = ax[2][i].tricontourf(
                model_grid[:, 0],
                model_grid[:, 1],
                assim_traj[ipl],
                lvl,
                cmap=colormap,
                extend="both",
            )
            ca.set_clim([lvl0, lvl1])
            fig.colorbar(ca, ax=ax[2][i], ticks=ctick, orientation="vertical")
            ax[2][i].set_title(
                r"\bf{Analysis Trajectory at t = }" + str(checkpoints[ipl]), fontsize=20
            )
            ax[2][i].set_xlabel(r"$x$")
            ax[2][i].set_ylabel(r"$y$")

        fig.subplots_adjust(hspace=0.35, wspace=0.4)
        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "AD_2D_contour_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        # saveto = os.path.join(output_dir, "AD_2D_contour.{0}".format("png"))
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

        # line plot settings
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        line_width = 2

        gs = gridspec.GridSpec(2, 3)
        fig = plt.figure(figsize=(16, 10))

        # Plot u at specfic y-values [line plots]
        for j in range(3):
            ax = fig.add_subplot(gs[0, j])

            for i in range(
                np.min([10, len(checkpoints)])
            ):  # limit the number of max line plots for better interpretability
                lbl1 = r"True Trajectory" if i == 0 else None
                lbl2 = r"Background Trajectory" if i == 0 else None
                lbl3 = r"Analysis Trajectory" if i == 0 else None

                _j = (j + 1) * ny // 4
                _ind = [_i + _j * nx for _i in range(nx)]
                ax.plot(
                    model_grid[_ind, 0],
                    true_traj[i][_ind],
                    linestyle="-",
                    linewidth=line_width,
                    color=line_colors[0],
                    label=lbl1,
                )
                ax.plot(
                    model_grid[_ind, 0],
                    back_traj[i][_ind],
                    linestyle="--",
                    linewidth=line_width,
                    color=line_colors[1],
                    label=lbl2,
                )
                ax.plot(
                    model_grid[_ind, 0],
                    assim_traj[i][_ind],
                    linestyle="-.",
                    linewidth=line_width,
                    color=line_colors[2],
                    label=lbl3,
                )
                ax.set_xlabel(r"$x$")
                ax.set_ylabel(r"$u(x,y=" + str(model_grid[_ind[0], 1]) + ",t)$")

                if j == 1:
                    ax.legend(
                        loc="center",
                        framealpha=0.5,
                        bbox_to_anchor=(0.5, 1.15),
                        ncol=3,
                        fontsize=20,
                    )

        # Plot rmse over time
        ax = fig.add_subplot(gs[1, :])
        lbl2 = r"Background Trajectory"
        lbl3 = r"Analysis Trajectory"
        ax.plot(
            checkpoints,
            rmseb,
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax.plot(
            checkpoints,
            rmsea,
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )

        ax.legend(loc="best", framealpha=0.5)
        ax.set_xlabel(r"Time $t$")
        ax.set_ylabel(r"RMSE")
        ax.set_title(r"Root-Mean-Squared-Error")
        fig.subplots_adjust(wspace=0.4, hspace=0.4)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "AD_2D_lines_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

    # Return the problem now for testing; remove later
    return problem


def fourDVar_Brg_1d(
    domain=(0, 1),
    nu=0.005,
    nx=51,
    dt=0.01,
    checkpoints=np.linspace(0, 1, 5),
    observation_error_variance=0.01,
    observation_operator_configs=dict(type="uniform", nx=15),
    prior_variance=0.016,
    gradient_fd_validation=True,
    Hessian_fd_validation=False,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    scr_out_iter=0,
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Bateman-Burgers-1D"
    ),
):
    """
    Test 1D Bateman-Burgers

    :param domain:
    :param nu:
    :param nx:
    :param dt:
    :param checkpoints: [t0,  t1, ..., tf] observations are made at t1, ...,  tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Burgers 1D)
    # Instantiate the 1D Burgers object
    model = bateman_burgers.Burgers1D(configs=dict(domain=domain, nu=nu, nx=nx, dt=dt))

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = u0_true + prior.generate_noise()
    x0 = u0_true + prior.generate_noise()  # initial guess at t0; IC

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        obs_nx = observation_operator_configs["nx"]
        obs_nx = min(nx, obs_nx)  # More observations are truncated
        if obs_nx == nx:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0, "at least one observation point is needed"
            observation_grid = np.linspace(domain[0], domain[1], obs_nx)
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    elif obs_operator_type == "random":
        # Randomly select set of the simulation gridpoints
        obs_nx = observation_operator_configs["nx"]
        if obs_nx == nx:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0, "at least one observation point is needed"
            state_size = np.size(model_grid, 0)
            locs = rng.choice(state_size, size=obs_nx, replace=False)
            locs.sort()
            observation_grid = model_grid[locs].copy()
            # TODO: there should be something simpler
            # TODO: there is a SelectionOeprtor now that should replace this
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=observation_grid.size,
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation:
        print("Validating the 4D-VAR objective gradient...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=problem.prior.mean,
            use_noise=False,
            validate_gradient=True,
            validate_Hessian=False,
            create_plots=False,
        )
    if Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=problem.prior.mean,
            use_noise=True,
            validate_gradient=False,
            validate_Hessian=True,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(x0, tspan=tspan, checkpoints=checkpoints)
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                  \r - {0}:  with prior mean
                  \r - {1}:  with initial guess
                  \r - {2}:  with analysis (MAP)
                  \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")

        # fancy plot settings
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        line_width = 2

        fig = plt.figure(figsize=(16, 8))
        ax = fig.add_subplot(121)
        for i in [
            0,
            len(checkpoints) // 2,
            -1,
        ]:  # indices for start, middle, end time points
            # range(np.min([10,len(checkpoints)])): #limit the number of max line plots for better interpretability
            lbl1 = r"True Trajectory" if i == 0 else None
            lbl2 = r"Background Trajectory" if i == 0 else None
            lbl3 = r"Analysis Trajectory" if i == 0 else None

            ax.plot(
                model_grid,
                true_traj[i],
                linestyle="-",
                linewidth=line_width,
                color=line_colors[0],
                label=lbl1,
            )
            ax.plot(
                model_grid,
                back_traj[i],
                linestyle="--",
                linewidth=line_width,
                color=line_colors[1],
                label=lbl2,
            )
            ax.plot(
                model_grid,
                assim_traj[i],
                linestyle="-.",
                linewidth=line_width,
                color=line_colors[2],
                label=lbl3,
            )

        ax.legend(loc="best", framealpha=0.5)
        ax.set_xlabel(r"$x$")
        ax.set_ylabel(r"$u(x, t)$")
        ax.set_title(r"Solution/Simulation", fontsize=20)

        # Plot rmse over time
        ax = fig.add_subplot(122)
        lbl2 = r"Background Trajectory"
        lbl3 = r"Analysis Trajectory"
        ax.plot(
            checkpoints,
            rmseb,
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax.plot(
            checkpoints,
            rmsea,
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )

        ax.legend(loc="best", framealpha=0.5)
        ax.set_xlabel(r"Time $t$")
        ax.set_ylabel(r"RMSE")
        ax.set_title(r"Root-Mean-Squared-Error")
        fig.subplots_adjust(wspace=0.3)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(output_dir, "Brg_1D_4DVAR.{0}".format(__PLOTS_FORMAT))
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

    # Return the problem now for testing; remove later
    return problem


def fourDVar_Brg_2d(
    domain=((0, 1), (0, 1)),
    nu=0.02,
    nx=11,
    ny=11,
    dt=0.01,
    checkpoints=np.linspace(0, 0.5, 6),
    observation_operator_configs=dict(type="identity", nx=5, ny=5),
    observation_error_variance=0.01,
    prior_variance=0.1,
    gradient_fd_validation=True,
    Hessian_fd_validation=True,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Bateman-Burgers-2D"
    ),
):
    """
    Test 2D Bateman-Burgers

    :param domain:
    :param nu:
    :param nx:
    :param ny:
    :param dt:
    :param checkpoints: [t0,  t1, ..., tf] observations are made at t1, ...,  tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :pararm random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Burgers 1D)
    # Instantiate the 2D Burgers object
    model = bateman_burgers.Burgers2D(
        configs=dict(domain=domain, nu=nu, nx=nx, ny=ny, dt=dt)
    )

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = u0_true + prior.generate_noise()
    x0 = u0_true + prior.generate_noise()  # initial guess at t0; IC

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        obs_nx = observation_operator_configs["nx"]
        obs_ny = observation_operator_configs["ny"]
        obs_nx = min(nx, obs_nx)  # More observations are truncated
        obs_ny = min(ny, obs_ny)  # More observations are truncated
        if obs_nx == nx and obs_ny == ny:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx or obs_ny > ny:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0 and obs_ny > 0, "at least one observation point is needed"
            _x = rng.uniform(low=domain[0][0], high=domain[0][1], size=obs_nx)
            _y = rng.uniform(low=domain[1][0], high=domain[1][1], size=obs_ny)
            [xx, yy] = np.meshgrid(_x, _y)
            observation_grid = np.hstack([xx.reshape(-1, 1), yy.reshape(-1, 1)])
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    elif obs_operator_type == "random":
        # Randomly select set of the simulation gridpoints
        obs_nx = observation_operator_configs["nx"]
        if obs_nx == nx and obs_ny == ny:
            observation_grid = model_grid
            observation_operator = identity.Identity(dict(model=model))
        elif obs_nx > nx or obs_ny > ny:
            print(
                "Incompatible observation configurations\nCan't observe more than what"
                " you simulate!"
            )
            raise ValueError
        else:
            assert obs_nx > 0 and obs_ny > 0, "at least one observation point is needed"
            state_size = np.size(model_grid, 0)
            locs = rng.choice(state_size, size=obs_nx * ny, replace=False)
            locs.sort()
            observation_grid = model_grid[locs, :].copy()
            # TODO: there should be something simpler
            # TODO: replace withe the SelectionOperator
            obs_configs = dict(
                model_grid=model_grid,
                observation_grid=observation_grid,
            )
            observation_operator = interpolation.CartesianInterpolator(obs_configs)

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=np.size(observation_grid, 0),
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=prior.mean,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(x0, tspan=tspan, checkpoints=checkpoints)
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")

        # Plot contour plots at some selected time point (start, middle, end)
        fig, ax = plt.subplots(
            nrows=3, ncols=3, figsize=(20, 15), subplot_kw={"projection": "3d"}
        )

        colormap = "viridis"
        ind = [
            0,
            len(checkpoints) // 2,
            -1,
        ]  # indices for start, middle, end time points
        # nlvl = 120
        for i in range(3):
            ipl = ind[i]
            lvl0 = (true_traj[ipl][: nx * ny]).min()
            lvl1 = (true_traj[ipl][: nx * ny]).max()
            # lvl = np.linspace(lvl0, lvl1, nlvl, endpoint=True)
            ctick = np.linspace(lvl0, lvl1, 5, endpoint=True)

            ct = ax[0][i].plot_surface(
                model_grid[: nx * ny, 0].reshape([nx, ny]),
                model_grid[: nx * ny, 1].reshape([nx, ny]),
                (true_traj[ipl][: nx * ny]).reshape([nx, ny]),
                cmap=colormap,
                linewidth=0,
                antialiased=False,
            )
            ct.set_clim([lvl0, lvl1])
            fig.colorbar(ct, ax=ax[0][i], ticks=ctick, orientation="vertical")
            ax[0][i].set_title(
                r"\bf{True Trajectory at t = }" + str(checkpoints[ipl]), fontsize=20
            )
            ax[0][i].set_xlabel(r"$x$")
            ax[0][i].set_ylabel(r"$y$")
            ax[0][i].invert_xaxis()

            cb = ax[1][i].plot_surface(
                model_grid[: nx * ny, 0].reshape([nx, ny]),
                model_grid[: nx * ny, 1].reshape([nx, ny]),
                (back_traj[ipl][: nx * ny]).reshape([nx, ny]),
                cmap=colormap,
                linewidth=0,
                antialiased=False,
            )
            cb.set_clim([lvl0, lvl1])
            fig.colorbar(cb, ax=ax[1][i], ticks=ctick, orientation="vertical")
            ax[1][i].set_title(
                r"\bf{Background Trajectory at t = }" + str(checkpoints[ipl]),
                fontsize=20,
            )
            ax[1][i].set_xlabel(r"$x$")
            ax[1][i].set_ylabel(r"$y$")
            ax[1][i].invert_xaxis()

            ca = ax[2][i].plot_surface(
                model_grid[: nx * ny, 0].reshape([nx, ny]),
                model_grid[: nx * ny, 1].reshape([nx, ny]),
                (assim_traj[ipl][: nx * ny]).reshape([nx, ny]),
                cmap=colormap,
                linewidth=0,
                antialiased=False,
            )
            ca.set_clim([lvl0, lvl1])
            fig.colorbar(ca, ax=ax[2][i], ticks=ctick, orientation="vertical")
            ax[2][i].set_title(
                r"\bf{Analysis Trajectory at t = }" + str(checkpoints[ipl]), fontsize=20
            )
            ax[2][i].set_xlabel(r"$x$")
            ax[2][i].set_ylabel(r"$y$")
            ax[2][i].invert_xaxis()

        fig.subplots_adjust(hspace=0.35, wspace=0.4)
        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "Brg_2D_surface_u_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        saveto = os.path.join(output_dir, "Brg_2D_surface_u_4DVAR.{0}".format("png"))
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

        # line plot settings
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        line_width = 2

        gs = gridspec.GridSpec(2, 3)
        fig = plt.figure(figsize=(16, 10))

        # Plot u at specfic y-values [line plots]
        for j in range(3):
            ax = fig.add_subplot(gs[0, j])

            for i in range(
                np.min([10, len(checkpoints)])
            ):  # limit the number of max line plots for better interpretability
                lbl1 = r"True Trajectory" if i == 0 else None
                lbl2 = r"Background Trajectory" if i == 0 else None
                lbl3 = r"Analysis Trajectory" if i == 0 else None

                _j = (j + 1) * ny // 4
                _ind = [_i + _j * nx for _i in range(nx)]
                ax.plot(
                    model_grid[_ind, 0],
                    true_traj[i][_ind],
                    linestyle="-",
                    linewidth=line_width,
                    color=line_colors[0],
                    label=lbl1,
                )
                ax.plot(
                    model_grid[_ind, 0],
                    back_traj[i][_ind],
                    linestyle="--",
                    linewidth=line_width,
                    color=line_colors[1],
                    label=lbl2,
                )
                ax.plot(
                    model_grid[_ind, 0],
                    assim_traj[i][_ind],
                    linestyle="-.",
                    linewidth=line_width,
                    color=line_colors[2],
                    label=lbl3,
                )
                ax.set_xlabel(r"$x$")
                ax.set_ylabel(r"$u(x,y=" + str(model_grid[_ind[0], 1]) + ",t)$")

                if j == 1:
                    ax.legend(
                        loc="center",
                        framealpha=0.5,
                        bbox_to_anchor=(0.5, 1.15),
                        ncol=3,
                        fontsize=20,
                    )

        # Plot rmse over time
        ax = fig.add_subplot(gs[1, :])
        lbl2 = r"Background Trajectory"
        lbl3 = r"Analysis Trajectory"
        ax.plot(
            checkpoints,
            rmseb,
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax.plot(
            checkpoints,
            rmsea,
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )

        ax.legend(loc="best", framealpha=0.5)
        ax.set_xlabel(r"Time $t$")
        ax.set_ylabel(r"RMSE")
        ax.set_title(r"Root-Mean-Squared-Error")
        fig.subplots_adjust(wspace=0.4, hspace=0.4)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "Brg_2D_lines_u_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

    # Return the problem now for testing; remove later

    return problem


def fourDVar_Lorenz_63(
    sigma=10.0,
    rho=28.0,
    beta=8.0 / 3.0,
    ic=[1, 1, 1],
    dt=0.001,
    checkpoints=np.linspace(0, 10, 101),
    observation_error_variance=0.01,
    prior_variance=0.16,
    gradient_fd_validation=True,
    Hessian_fd_validation=True,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Lorenz-63"
    ),
):
    """
    Test Lorenz 63 model

    :param sigma:
    :param rho:
    :param beta:
    :param ic:
    :param dt:
    :param checkpoints: [t0,  t1, ..., tf] observations are made at t1, ...,  tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Lorenz 63)
    # Instantiate the Lorenz 63 object
    model = lorenz.Lorenz63(configs=dict(sigma=sigma, rho=rho, beta=beta, ic=ic, dt=dt))

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()

    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = u0_true + prior.generate_noise()
    x0 = u0_true + prior.generate_noise()  # initial guess at t0; IC

    # 3- Observation operator (identity), and observation error model
    observation_operator = identity.Identity(dict(model=model))

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=x0,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        tspan = (checkpoints[0], 2 * checkpoints[-1])
        time_t, true_traj = model.integrate_state(u0_true, tspan=tspan)
        time_b, back_traj = model.integrate_state(x0, tspan=tspan)
        time_a, assim_traj = model.integrate_state(xa, tspan=tspan)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        true_traj = np.array(true_traj)
        back_traj = np.array(back_traj)
        assim_traj = np.array(assim_traj)

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")

        # fancy plot settings
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        line_width = 2

        # Plot trajectories
        print("Plotting Results...")
        fig = plt.figure(figsize=(18, 14))
        gs = gridspec.GridSpec(4, 2)

        ax = fig.add_subplot(gs[:, 0], projection="3d")
        lbl1 = r"True Trajectory"
        lbl2 = r"Background Trajectory"
        lbl3 = r"Analysis Trajectory"
        ax.plot(
            true_traj[:, 0],
            true_traj[:, 1],
            true_traj[:, 2],
            linestyle="-",
            linewidth=line_width,
            color=line_colors[0],
            label=lbl1,
        )
        ax.plot(
            back_traj[:, 0],
            back_traj[:, 1],
            back_traj[:, 2],
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax.plot(
            assim_traj[:, 0],
            assim_traj[:, 1],
            assim_traj[:, 2],
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )
        ax.set_xlabel(r"$x(t)$")
        ax.set_ylabel(r"$y(t)$")
        ax.set_zlabel(r"$z(t)$")
        ax.legend(loc="best", framealpha=0.5)
        ax.set_title(r"\bf{Lorenz attractor}")

        for i, var in enumerate([r"$x(t)$", r"$y(t)$", r"$z(t)$"]):
            ax = fig.add_subplot(gs[i, 1])
            ax.plot(
                time_t,
                true_traj[:, i],
                linestyle="-",
                linewidth=line_width,
                color=line_colors[0],
                label=lbl1,
            )
            ax.plot(
                time_b,
                back_traj[:, i],
                linestyle="--",
                linewidth=line_width,
                color=line_colors[1],
                label=lbl2,
            )
            ax.plot(
                time_a,
                assim_traj[:, i],
                linestyle="-.",
                linewidth=line_width,
                color=line_colors[2],
                label=lbl3,
            )

            ax.set_xlabel(r"$t$")
            ax.set_ylabel(var)
            ax.legend(loc="best", framealpha=0.5)

        ax = fig.add_subplot(gs[3, 1])
        ax.plot(
            time_b,
            rmseb,
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax.plot(
            time_a,
            rmsea,
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )

        ax.legend(loc="best", framealpha=0.5)
        ax.set_xlabel(r"Time $t$")
        ax.set_ylabel(r"RMSE")
        ax.set_title(r"Root-Mean-Squared-Error")

        fig.subplots_adjust(wspace=0.3, hspace=0.5)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(output_dir, "Lorenz_63_4DVAR.{0}".format(__PLOTS_FORMAT))
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

    # Return the problem now for testing; remove later
    return problem


def fourDVar_Lorenz_96(
    nx=36,
    F=8.0,
    dt=0.01,
    checkpoints=np.linspace(0.1, 1, 10),
    observation_error_variance=0.01,
    prior_variance=0.16,
    gradient_fd_validation=True,
    Hessian_fd_validation=True,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Lorenz-96"
    ),
):
    """
    Test Lorenz 96 model

    :param n:
    :param F:
    :param dt:
    :param checkpoints: [t0,  t1, ..., tf] observations are made at t1, ...,  tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Lorenz 63)
    # Instantiate the Lorenz 63 object
    model = lorenz.Lorenz96(configs=dict(nx=nx, F=F, dt=dt))

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()

    prior = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=prior_variance,
            random_seed=random_seed,
        )
    )
    prior.mean = u0_true + prior.generate_noise()
    x0 = u0_true + prior.generate_noise()  # initial guess at t0; IC

    # 3- Observation operator (identity), and observation error model
    observation_operator = identity.Identity(dict(model=model))

    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=u0_true.size,
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )

    problem = FourDVar(
        configs=dict(
            window=(checkpoints[0], checkpoints[-1]),
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=x0,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            x0,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        tspan = (checkpoints[0], 4 * checkpoints[-1])
        time_t, true_traj = model.integrate_state(u0_true, tspan=tspan)
        time_b, back_traj = model.integrate_state(x0, tspan=tspan)
        time_a, assim_traj = model.integrate_state(xa, tspan=tspan)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        true_traj = np.array(true_traj)
        back_traj = np.array(back_traj)
        assim_traj = np.array(assim_traj)

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(x0)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        # Plot trajectories
        print("Plotting Results...")

        lbl1 = r"True Trajectory"
        lbl2 = r"Background Trajectory"
        lbl3 = r"Analysis Trajectory"

        # fancy plot settings
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        line_width = 2

        # contour plot
        fig, ax = plt.subplots(nrows=4, ncols=1, figsize=(12, 16))
        colormap = "seismic"
        nlvl = 120
        lvl0 = true_traj.min()
        lvl1 = true_traj.max()
        lvl = np.linspace(lvl0, lvl1, nlvl, endpoint=True)

        ax[0].contourf(
            time_t, np.arange(nx), true_traj.T, lvl, cmap=colormap, extend="both"
        )
        ax[0].set_title(lbl1)
        ax[1].contourf(
            time_t, np.arange(nx), true_traj.T, lvl, cmap=colormap, extend="both"
        )
        ax[1].set_title(lbl2)
        ax[2].contourf(
            time_t, np.arange(nx), true_traj.T, lvl, cmap=colormap, extend="both"
        )
        ax[2].set_title(lbl3)
        for i in range(3):
            ax[i].set_xlabel(r"$t$")
            # ax[0].set_ylabel(r'$x$')
            ax[i].set_yticks([0, 8, 17, 26, 35])
            ax[i].set_yticklabels(
                [r"$x_{" + str(j + 1) + "}$" for j in [0, 8, 17, 26, 35]]
            )

        ax[3].plot(
            time_b,
            rmseb,
            linestyle="--",
            linewidth=line_width,
            color=line_colors[1],
            label=lbl2,
        )
        ax[3].plot(
            time_a,
            rmsea,
            linestyle="-.",
            linewidth=line_width,
            color=line_colors[2],
            label=lbl3,
        )

        ax[3].legend(loc="best", framealpha=0.5)
        ax[3].set_xlabel(r"Time $t$")
        ax[3].set_ylabel(r"RMSE")
        ax[3].set_title(r"Root-Mean-Squared-Error")

        fig.subplots_adjust(hspace=0.6)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "Lorenz_96_contour_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

        # line plots
        fig, ax = plt.subplots(nrows=3, ncols=1, figsize=(12, 10))

        for i, j in enumerate([0, 17, 35], start=0):
            ax[i].plot(
                time_t,
                true_traj[:, j],
                linestyle="-",
                linewidth=line_width,
                color=line_colors[0],
                label=lbl1,
            )
            ax[i].plot(
                time_b,
                back_traj[:, j],
                linestyle="--",
                linewidth=line_width,
                color=line_colors[1],
                label=lbl2,
            )
            ax[i].plot(
                time_a,
                assim_traj[:, j],
                linestyle="-.",
                linewidth=line_width,
                color=line_colors[2],
                label=lbl3,
            )

            ax[i].set_xlabel(r"$t$")
            ax[i].set_ylabel(r"$x_{" + str(j + 1) + "}(t)$")
            ax[i].legend(loc="best", framealpha=0.5)

        fig.subplots_adjust(hspace=0.6)

        # save plot
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
        saveto = os.path.join(
            output_dir, "Lorenz_96_line_4DVAR.{0}".format(__PLOTS_FORMAT)
        )
        print("Saving/Plotting Results to: {0}".format(saveto))
        # plt.show()
        fig.savefig(saveto, dpi=600, bbox_inches="tight")
        plt.close(fig)

    # Return the problem now for testing; remove later
    return problem


def fourDVar_Dolfin_AD_2d(
    dt=0.2,
    checkpoints=np.arange(0, 1.01, 0.2),
    observation_operator_configs=dict(
        type="uniform",
        num_obs_points=25,
        exclude_boxes=[((0.25, 0.150), (0.50, 0.400)), ((0.60, 0.625), (0.75, 0.850))],
    ),
    observation_noise=0.01,
    prior_variance=1,
    prior_type="BiLaplacian",
    Laplacian_prior_params=(1, 16),
    gradient_fd_validation=True,
    Hessian_fd_validation=False,
    solve_inverse_problem=True,
    update_posterior=True,
    optimizer_configs={
        "maxiter": None,
    },
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Dolfin-AD-2D"
    ),
):
    """
    Test 4D-Var with 2D Dolfin-based Advection-Diffusion model

    :param dt:
    :param checkpoints: [t0, t1, ..., tf] observations are made at t1, ..., tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Advection-Diffusion 2D)
    # Instantiate the 2D Advection Diffusion object
    model = fenics_models.create_AdvectionDiffusion2D_model(
        dt=dt, output_dir=output_dir
    )

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # Create prior model and update its mean
    prior_mean = model.parameter_vector(init_val=0.0)

    Vh = model.parameter_dof
    if re.match(r"\AGaussian\Z", prior_type, re.IGNORECASE):
        configs = dict(
            mean=prior_mean,
            variance=prior_variance,
            random_seed=random_seed,
        )
        prior = Gaussian.GaussianErrorModel(configs)

    elif re.match(r"\ALaplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinLaplacianErrorModel(configs)

    elif re.match(r"\ABi(| |-|_)*Laplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior = Laplacian.DolfinBiLaplacianErrorModel(configs)

    else:
        print("Unrecognized Prior Type")
        raise ValueError
    # prior.mean = u0_true + prior.generate_noise()
    init_guess = prior_mean.copy()

    # 3- Observation operator, and observation error model
    # TODO: Consider Dolfin-pointwise observation

    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        num_obs_points = observation_operator_configs["num_obs_points"]
        exclude_boxes = observation_operator_configs["exclude_boxes"]
        observation_operator = (
            fenics_observation_operators.create_pointwise_observation_operator(
                model=model,
                Vh=model.state_dof,
                num_obs_points=num_obs_points,
                exclude_boxes=exclude_boxes,
            )
        )
        observation_grid = observation_operator.get_observation_grid()

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    _, traject = model.integrate_state(
        u0_true, tspan=(checkpoints[0], checkpoints[-1]), checkpoints=checkpoints[1:]
    )
    d = 0.0
    for x in traject:
        y = observation_operator(x)
        d += y
    d /= len(traject)
    obs_err_std = 1e-12 + observation_noise * np.linalg.norm(d, np.inf)
    observation_error_variance = obs_err_std**2
    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=observation_operator.shape[0],
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )
    window = (checkpoints[0], checkpoints[-1])
    problem = FourDVar(
        configs=dict(
            window=window,
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimizer_configs=optimizer_configs,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y, return_np=True)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=init_guess,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            init_guess,
            skip_map_estimate=False,
            update_posterior=update_posterior,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(
            init_guess, tspan=tspan, checkpoints=checkpoints
        )
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior.mean)
        obj_val_guess, _ = problem.objective(init_guess)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        print("Plotting truth, prior, and posterior modes")
        model.plot_state(
            u0_true,
            save_to_file=os.path.join(
                output_dir,
                f"True_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            init_guess,
            save_to_file=os.path.join(
                output_dir,
                f"InitGuess_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            prior.mean,
            save_to_file=os.path.join(
                output_dir,
                f"PriorMean_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            xa,
            save_to_file=os.path.join(
                output_dir,
                f"PosteriorMean_IC.{__PLOTS_FORMAT}",
            ),
        )

    # Return the problem now for testing; remove later
    return problem


def main(test):
    """
    Choose the test (4D-var data assimilation for a selected simulation mdoel)
        to run and call the proper function from this module

    :arg str test: name of the test to run; supported names are:
      - 'AD1' :  One-dimensional (1D) Advection-Diffusion (AD) model
      - 'AD2' :  Two-dimensional (2D) Advection-Diffusion (AD) model
      - 'D-AD2' : Dolfin-based Two-dimensional (2D) Advection-Diffusion (AD) model
      - 'Brg1': One-dimensional (1D) Bateman-Burgers model
      - 'Brg2': Two-dimensional (2D) Bateman-Burgers model
      - 'L63' : Lorenz 63 (three-variable) model
      - 'L96' : Lorenz 96 model

    :raises: ValueError is raised if the passed `test` value is not recognized
    """
    if re.match(r"\AAD(_|-| )*1\Z", test, re.IGNORECASE):
        # 1D AD model test
        print("\nTesting the 1D Advection-Diffusion Model with default settings")
        fourDVar_AD_1d()
    elif re.match(r"\AAD(_|-| )*2\Z", test, re.IGNORECASE):
        # 2D AD model test
        print("\nTesting the 2D Advection-Diffusion Model with default settings")
        fourDVar_AD_2d()
    elif re.match(r"\AD(_|-|)*AD(_|-| )*2\Z", test, re.IGNORECASE):
        # Dolfin-based 2D AD model test
        print(
            "\nTesting the Dolfin-based 2D Advection-Diffusion Model with default"
            " settings"
        )
        fourDVar_Dolfin_AD_2d()
    elif re.match(r"\ABRG(_|-| )*1\Z", test, re.IGNORECASE):
        # 1D Burgers model test
        print("\nTesting the 1D Bateman-Burgers Model with default settings")
        fourDVar_Brg_1d()
    elif re.match(r"\ABRG(_|-| )*2\Z", test, re.IGNORECASE):
        # 2D Burgers model test
        print("\nTesting the 2D Bateman-Burgers Model with default settings")
        fourDVar_Brg_2d()

    elif re.match(r"\AL(_|-| )*63\Z", test, re.IGNORECASE):
        # Lorenz 63 model test
        print("\nTesting the Lorenz 63 Model with default settings")
        fourDVar_Lorenz_63()

    elif re.match(r"\AL(_|-| )*96\Z", test, re.IGNORECASE):
        # Lorenz 96 model test
        print("\nTesting the Lorenz 96 Model with default settings")
        fourDVar_Lorenz_96()

    else:
        print("Unrecognized model/test-case: '{0}'!".format(test))
        raise ValueError


if __name__ == "__main__":
    # Select the model you want to test --- go up to change configuration as needed
    msg = f"""
        \r{'*'*80}\n\tTesting 4DVar Assimilation/Smoothing System\n{'*'*80}
        \rAvailable simulation models to use:
        \r{'-'*60}:
        \r - 'AD1'  :  One-dimensional (1D) Advection-Diffusion (AD) model
        \r - 'AD2'  :  Two-dimensional (2D) Advection-Diffusion (AD) model
        \r - 'D-AD2': Dolfin-based Two-dimensional (2D) Advection-Diffusion (AD) model
        \r - 'Brg1' : One-dimensional (1D) Bateman-Burgers model
        \r - 'Brg2' : Two-dimensional (2D) Bateman-Burgers model
        \r - 'L63'  : Lorenz 63 (three-variable) model
        \r - 'L96'  : Lorenz 96 model
        \r{'-'*60}:\n
        \r >> Input/Type the model you want to use with 4DVar (from the list above): """
    test = input(msg)
    main(test=test)
