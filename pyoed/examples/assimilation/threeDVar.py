# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Driver script to test the components of 4DVar data assimilation system
"""

import sys, os

# Add required paths
pyoed_path = os.path.abspath('../../')
if not pyoed_path in sys.path: sys.path.append(pyoed_path)

# Import pyoed modules (model, observation operator, error models)
import pyoed
from pyoed.models.simulation_models import (
    toy_linear,
    fenics_models,
)
from pyoed.models.error_models.Gaussian import (GaussianErrorModel,
                                                DolfinGaussianErrorModel)
from pyoed.models.error_models.Laplacian import (DolfinLaplacianErrorModel,
                                                 DolfinBiLaplacianErrorModel, )
from pyoed.models.observation_operators import (identity, interpolation, )
from pyoed.assimilation.filtering.threeDVar import (VanillaThreeDVar, DolfinThreeDVar)
from pyoed import utility

# Import standard libraries/modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import re

# Set figures default formatting
utility.plots_enhancer(fontsize=20, fontweight='bold', usetex=True)

# Global(ish) variables
__PLOTS_FORMAT = "pdf"


def threeDVar_toy_linear(parameter_size=8,
                         state_size=10,
                         observation_error_variance=0.04,
                         random_seed=1234,
                         observation_operator_configs=dict(type='identity', nx=10),
                         prior_variance=0.25,
                         gradient_fd_validation=True,
                         Hessian_fd_validation=True,
                         solve_inverse_problem=True,
                         update_posterior=True,
                         optimization_routine_options={'maxiter':30,},
                         output_dir=os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/3DVAR_LinearToy1D_TimeIndependent"),
                         verbose=True,
                         ):
    """
    Test 3D-Var with 1D Toy Linear Time-independent model ::math:`b=AX`, with
        ::math:`A` being a square matrix of size ::math:`(np, nx)`

    :param parameter_size: (::math:`np`) model parameter dimensionality (number of model parameters)
    :param state_size: (::math:`nx`) model state dimensionality (model state vector size)
    :param random_seed: None, or integer used to control reproduction
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param output_dir: location to save any log or results
    """
    # 1- Create the simulation model
    # Instantiate the 1D Advection Diffusion object
    model = toy_linear.create_time_independent_linear_model(parameter_size,
                                                            state_size=state_size,
                                                            random_seed=random_seed,
                                                            output_dir=output_dir)
    model_grid = model.get_model_grid()

    # 2- Extract true initial condition, create prior model, and create forecast state
    true_parameter = model.parameter_vector()
    true_parameter[:] = np.random.randn(true_parameter.size)
    true_state = model.solve_forward(true_parameter)
    prior = GaussianErrorModel(configs=dict(size=true_parameter.size,
                                                  mean=0.0,
                                                  random_seed=random_seed*2 if random_seed is not None else None,
                                                  variance=prior_variance))
    prior_mean = true_parameter + prior.generate_noise()
    prior.mean = prior_mean.copy()
    init_guess = prior_mean + prior.generate_noise()  # initial guess

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs['type']
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(configs={'model': model, })

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        obs_nx = observation_operator_configs['nx']
        if obs_nx == nx:
            observation_grid = model_grid
            observation_operator = identity.Identity(configs={'model': model, })
        elif obs_nx > nx:
            print("Incompatible observation configurations\nCan't observe more than what you simulate!")
            raise ValueError
        else:
            assert obs_nx > 0, "at least one observation point is needed"
            observation_grid = np.linspace(domain[0], domain[1], obs_nx)
            observation_operator = interpolation.CartesianInterpolator(
                configs={
                    'model_grid':model_grid,
                    'observation_grid':observation_grid,
                }
            )

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    obs_err_model = GaussianErrorModel(configs=dict(size=observation_grid.size,
                                                    mean=0.0,
                                                    random_seed=random_seed*3 if random_seed is not None else None,
                                                    variance=observation_error_variance))

    problem = VanillaThreeDVar(
        configs=dict(
            model=model,
            invert_for='parameter',
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    y = observation_operator.apply(true_state)  # True observation
    yobs = obs_err_model.add_noise(y)  # Noisy observation
    problem.register_observations(yobs)

    # 5- Test the 3D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 3D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(problem,
                                     eval_at=init_guess,
                                     use_noise=True,
                                     validate_gradient=gradient_fd_validation,
                                     validate_Hessian=Hessian_fd_validation,
                                     create_plots=True,
                                     output_dir=output_dir)

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 3D-VAR inverse problem...")
        xa = problem.solve(init_guess, update_posterior=update_posterior, )

        # Evaluate RMSE
        rmse_prior = utility.calculate_rmse(prior_mean, true_parameter)
        rmse_guess = utility.calculate_rmse(init_guess, true_parameter)
        rmse_assim = utility.calculate_rmse(xa, true_parameter)
        print("""Root-Mean-Squared Errors (RMSEs):
              \r - Prior Mean               : {0}
              \r - Initial Guess            : {1}
              \r - Analysis (Posterior Mode): {2}

        """.format(rmse_prior, rmse_guess, rmse_assim))

        print("Computing the true, prior, and posterior objective values...")
        obj_val_true,_  = problem.objective(true_parameter)
        obj_val_prior,_ = problem.objective(prior.mean)
        obj_val_guess,_ = problem.objective(init_guess)
        obj_val_assim,_ = problem.objective(xa)
        print("""The value of the 3D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition
              """.format(obj_val_prior,obj_val_guess,obj_val_assim,obj_val_true))

    # Return the problem (filter object) for further testing
    return problem

def threeDVar_subsurf(nx=21,
                      ny=21,
                      mesh_elements='Lagrange',
                      mesh_elements_degree=(1, 2),
                      observation_error_variance=0.01**2,
                      random_seed=1234,
                      observation_operator_configs=dict(type='identity', nx=10),
                      prior_variance=1.00,
                      prior_type='BiLaplacian',
                      Laplacian_prior_params=(1.0, 0.5),
                      gradient_fd_validation=True,
                      Hessian_fd_validation=False,
                      solve_inverse_problem=True,
                      update_posterior=True,
                      optimization_routine_options={'maxiter':300,},
                      output_dir=os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/3DVAR_Subsurf"),
                      verbose=True,
                      ):
    """
    Test 3D-Var with 2D Toy Poisson Problem ::math:`\\nabla \\cdot (k nabla u) = f`, with:\n
        - ::math:`k` being the conductivity field of size ::math:`(nx \\times ny)`
        - ::math:`u` being the temperature field of size ::math:`(nx \\times ny)`
        - ::math:`f` is the external forcing

    :param nx: model dimensionality in the x direction
    :param ny: model dimensionality in the y direction
    :param str mesh_elements: Mesh element type
    :param mesh_elements_degree: degree of the mesh elements
    :param random_seed: None, or integer used to control reproduction
    :param observation_error_variance:
    :param prior_variance:
    :param str prior_type: name of the prior to use: 'Gaussian', 'Laplacian', 'BiLaplacian'
    :param tuple Laplacian_prior_params: (gamma, delta) passed to Laplaciane-type error models
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param bool verbose:
    :param output_dir: location to save any log or results
    """
    # 1- Create the simulation model
    # Instantiate the Subsurface Model
    model = fenics_models.create_Subsurf2D_model(nx=nx,
                                           ny=ny,
                                           mesh_elements=mesh_elements,
                                           mesh_elements_degree=mesh_elements_degree,
                                           output_dir=output_dir)

    model_grid = model.get_model_grid()

    # 2- Extract true initial condition, create prior model, and create forecast state
    true_parameter = model.create_initial_condition(method='det')
    true_state     = model.solve_forward(true_parameter, )

    # Create prior model and update its mean
    prior_mean = model.parameter_vector(init_val=0.0)

    Vh         = model.parameter_dof
    if re.match(r"\AGaussian\Z", prior_type, re.IGNORECASE):
        configs = dict( mean=prior_mean,
                       variance=prior_variance,
                       random_seed=random_seed if random_seed is not None else None,
                       )
        prior = GaussianErrorModel(configs)

    elif re.match(r"\ALaplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(Vh=Vh,
                       mean=prior_mean,
                       gamma=Laplacian_prior_params[0],
                       delta=Laplacian_prior_params[1],
                       random_seed=random_seed if random_seed is not None else None,
                       )
        prior = DolfinLaplacianErrorModel(configs)

    elif re.match(r"\ABi(| |-|_)*Laplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(Vh=Vh,
                       mean=prior_mean,
                       gamma=Laplacian_prior_params[0],
                       delta=Laplacian_prior_params[1],
                       random_seed=random_seed if random_seed is not None else None,
                       )
        prior = DolfinBiLaplacianErrorModel(configs)

    else:
        print("Unrecognized Prior Type")
        raise ValueError

    # Initial guess (for verification and inversion)
    # init_guess  = prior_mean + prior.generate_noise()
    init_guess  = prior_mean.copy()

    # 3- Observation operator, and observation error model
    obs_oper_type = observation_operator_configs['type']
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(
            configs={'model': model},
        )

    else:
        raise ValueError(
            f"Unsupporrted observation operator type {obs_oper_type}"
        )


    observation_error_variance = observation_error_variance  * np.max(np.abs(true_state))**2
    obs_err_model = GaussianErrorModel(configs=dict(size=observation_grid.shape[0],
                                                    mean=0.0,
                                                    random_seed=random_seed*3 if random_seed is not None else None,
                                                    variance=observation_error_variance))

    problem = DolfinThreeDVar(
        configs=dict(
            model=model,
            invert_for='parameter',
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            verbose=verbose,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    y = observation_operator.apply(true_state)  # True observation
    if False:
        yobs = y
    else:
        yobs = obs_err_model.add_noise(y)  # Noisy observation
    problem.register_observations(yobs)

    # 5- Test the 3D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 3D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(problem,
                                     eval_at=init_guess,
                                     use_noise=True,
                                     validate_gradient=gradient_fd_validation,
                                     validate_Hessian=Hessian_fd_validation,
                                     create_plots=True,
                                     output_dir=output_dir)

    # 6- Solve the inverse problem, and plot results as needed
    #
    p0 = init_guess
    x0 = model.solve_forward(p0)
    #
    pb = prior.mean
    xb = model.solve_forward(pb)
    #
    if solve_inverse_problem:
        print("Solving the 3D-VAR inverse problem...")
        pa = problem.solve(init_guess, update_posterior=update_posterior, )

        # Evaluate RMSE
        rmse_prior = utility.calculate_rmse(prior_mean, true_parameter)
        rmse_guess = utility.calculate_rmse(init_guess, true_parameter)
        rmse_assim = utility.calculate_rmse(pa, true_parameter)
        print("""Root-Mean-Squared Errors (RMSEs):
              \r - Prior Mean               : {0}
              \r - Initial Guess            : {1}
              \r - Analysis (Posterior Mode): {2}

        """.format(rmse_prior, rmse_guess, rmse_assim))

        print("Computing the true, prior, and posterior objective values...")
        obj_val_true  = problem.objective_function_value(true_parameter)
        obj_val_misfit_true = problem.objective_function_value(true_parameter, data_misfit_only=True)
        obj_val_prior = problem.objective_function_value(prior.mean)
        obj_val_misfit_prior = problem.objective_function_value(prior.mean, data_misfit_only=True)
        obj_val_guess = problem.objective_function_value(init_guess)
        obj_val_misfit_guess = problem.objective_function_value(init_guess, data_misfit_only=True)
        obj_val_assim = problem.objective_function_value(pa)
        obj_val_misfit_assim = problem.objective_function_value(pa, data_misfit_only=True)

        print("""The value of the 3D-VAR objective function is:
                 \r - prior mean     : Total = {0:12.8f}, Data-Misfit = {1:12.8f}, Prior = {2:12.8f}
                 \r - initial guess  : Total = {3:12.8f}, Data-Misfit = {4:12.8f}, Prior = {5:12.8f}
                 \r - analysis (MAP) : Total = {6:12.8f}, Data-Misfit = {7:12.8f}, Prior = {8:12.8f}
                 \r - true parametr  : Total = {9:12.8f}, Data-Misfit = {10:12.8f}, Prior = {11:12.8f}
              """.format(obj_val_prior,
                         obj_val_misfit_prior,
                         obj_val_prior-obj_val_misfit_prior,
                         #
                         obj_val_guess,
                         obj_val_misfit_guess,
                         obj_val_guess-obj_val_misfit_guess,
                         #
                         obj_val_assim,
                         obj_val_misfit_assim,
                         obj_val_assim-obj_val_misfit_assim,
                         #
                         obj_val_true,
                         obj_val_misfit_true,
                         obj_val_true-obj_val_misfit_true,
                         ))

        # Plot
        model.plot(true_parameter, target='param', saveto=os.path.join(output_dir, "True_Parameter"))
        model.plot(true_state, target='state', saveto=os.path.join(output_dir, "True_State"))
        #
        model.plot(p0, target='param', saveto=os.path.join(output_dir, "Initial_Guess_Parameter"))
        model.plot(x0, target='state', saveto=os.path.join(output_dir, "Initial_Guess_State"))
        #
        model.plot(pb, target='param', saveto=os.path.join(output_dir, "Prior_Mean_Parameter"))
        model.plot(xb, target='state', saveto=os.path.join(output_dir, "Prior_Mean_State"))
        #
        xa = model.solve_forward(pa)
        model.plot(pa, target='param', saveto=os.path.join(output_dir, "Posterior_Mean_Parameter"))
        model.plot(xa, target='state', saveto=os.path.join(output_dir, "Posterior_Mean_State"))

    else:
        xa = pa = None

    #
    ip_results = dict(true_parameter=true_parameter,
                      true_state=true_state,
                      #
                      init_guess_parameter=p0,
                      init_guess_state=x0,
                      #
                      prior_mean_parameter=pb,
                      prior_mean_state=xb,
                      #
                      posterior_mean_parameter=pa,
                      posterior_mean_state=xa,
                      )

    # Return the problem (filter object) for further testing
    return problem, ip_results


def main(test):
    """
    Choose the test (4D-var data assimilation for a selected simulation mdoel)
        to run and call the proper function from this module

    :arg str test: name of the test to run; supported names are:
      - 'Toy' : One-dimensional (1D) Linear Toy Model
      - 'Poisson' or 'Subsurf':  Two-dimensional (2D) Subsurface Model

    :raises: ValueError is raised if the passed `test` value is not recognized
    """
    if re.match(r'\AToy\Z', test, re.IGNORECASE):
        # 1D Toy linear model
        print("\nTesting 3D-Var using 1D Toy Lienar model with default settings")
        threeDVar_toy_linear()

    elif re.match(r'\A(Poisson|subsurf)\Z', test, re.IGNORECASE):
        # 2D Subsurface Model
        print("\nTesting 3D-Var using 2D Subsurface model with default settings")
        threeDVar_subsurf()

    else:
        print("Unrecognized model/test-case: '{0}'!".format(test))
        raise ValueError


if __name__ == "__main__":
    # Select the model you want to test --- go up to change configuration as needed
    msg = """Available simulation models to use:
             \r - 'Toy':  One-dimensional (1D) Linear Toy Model
             \r - 'Poisson' or 'Subsurf':  Two-dimensional (2D) Subsurface Model\n
             \rInput/Type the model you want to test: """
    test = input(msg)
    main(test=test)

