# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Driver script to test the components of a goal-oriented version of 4DVar data assimilation system
with fenics-based advection diffusion model
"""

# Essential imports
import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility


# Import PyOED  modules (model, observation operator, error models)
import pyoed
from pyoed import utility

# simulation model(s)
from pyoed.models.simulation_models import (
    fenics_models,
)

# observation operators(s)
from pyoed.models.observation_operators import (
    fenics_observation_operators,
)

# error models
from pyoed.models.error_models import (
    Gaussian,
    Laplacian,
)

# data assimilation (inversion)
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar as FourDVar
from pyoed.assimilation.goal_oriented.linear_inverse_problem import (
    GoalOrientedLinearInverseProblem,
)
from pyoed.assimilation.goal_oriented.prediction_operators import (
    fenics_prediction_operators,
)


# Set (module level) figures default formatting
utility.plots_enhancer(
    fontsize=20,
    fontweight="bold",
    usetex=True,
)
# Global(ish) variables
__PLOTS_FORMAT = "pdf"


def goal_oriented_fourDVar_Dolfin_AD_2d(
    dt=0.2,
    checkpoints=np.arange(0, 1.01, 0.2),
    observation_operator_configs=dict(
        type="uniform",
        num_obs_points=25,
        exclude_boxes=[((0.25, 0.150), (0.50, 0.400)), ((0.60, 0.625), (0.75, 0.850))],
    ),
    observation_noise=0.01,
    prior_variance=1,
    prior_type="BiLaplacian",
    Laplacian_prior_params=(1, 16),
    gradient_fd_validation=False,
    Hessian_fd_validation=False,
    solve_inverse_problem=False,
    update_posterior=True,
    optimization_routine_options={
        "maxiter": None,
    },
    verbose=True,
    random_seed=RANDOM_SEED,
    output_dir=os.path.join(
        pyoed.SETTINGS.OUTPUT_DIR, "Data_Assimilation/4DVar-Dolfin-AD-2D"
    ),
):
    """
    Test 4D-Var with 2D Dolfin-based Advection-Diffusion model

    :param dt:
    :param checkpoints: [t0, t1, ..., tf] observations are made at t1, ..., tf
    :param observation_error_variance:
    :param prior_variance:
    :param bool gradient_fd_validation: validate the 4D-Var objective gradient using finite differences
    :param random_seed:
    :param output_dir:
    """
    # Create a random number generator
    rng = np.random.default_rng(random_seed)

    # 1- Create the simulation model (Advection-Diffusion 2D)
    # Instantiate the 2D Advection Diffusion object
    model = fenics_models.create_AdvectionDiffusion2D_model(
        dt=dt, output_dir=output_dir
    )

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # Create prior model and update its mean
    prior_mean = model.parameter_vector(init_val=0.0)

    Vh = model.parameter_dof
    if re.match(r"\AGaussian\Z", prior_type, re.IGNORECASE):
        configs = dict(
            mean=prior_mean,
            variance=prior_variance,
            random_seed=random_seed,
        )
        prior_model = Gaussian.GaussianErrorModel(configs)

    elif re.match(r"\ALaplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior_model = Laplacian.DolfinLaplacianErrorModel(configs)

    elif re.match(r"\ABi(| |-|_)*Laplac(ian)*\Z", prior_type, re.IGNORECASE):
        configs = dict(
            Vh=Vh,
            mean=prior_mean,
            gamma=Laplacian_prior_params[0],
            delta=Laplacian_prior_params[1],
            random_seed=random_seed,
        )
        prior_model = Laplacian.DolfinBiLaplacianErrorModel(configs)

    else:
        print("Unrecognized Prior Type")
        raise ValueError
    # prior_model.mean = u0_true + prior_model.generate_noise()
    init_guess = prior_mean.copy()

    # 3- Observation operator, and observation error model
    # TODO: Consider Dolfin-pointwise observation

    obs_oper_type = observation_operator_configs["type"]
    if re.match(r"\Aidentity\Z", obs_oper_type, re.IGNORECASE):
        # Observe everything
        observation_grid = model_grid
        observation_operator = identity.Identity(dict(model=model))

    elif re.match(r"\Auniform\Z", obs_oper_type, re.IGNORECASE):
        # Uniformly distributed observation points
        num_obs_points = observation_operator_configs["num_obs_points"]
        exclude_boxes = observation_operator_configs["exclude_boxes"]
        observation_operator = (
            fenics_observation_operators.create_pointwise_observation_operator(
                model=model,
                Vh=model.state_dof,
                num_obs_points=num_obs_points,
                exclude_boxes=exclude_boxes,
            )
        )
        observation_grid = observation_operator.get_observation_grid()

    else:
        print("Unsupporrted observation operator type {0}".format(obs_oper_type))
        raise ValueError

    _, traject = model.integrate_state(
        u0_true, tspan=(checkpoints[0], checkpoints[-1]), checkpoints=checkpoints[1:]
    )
    d = 0.0
    for x in traject:
        y = observation_operator(x)
        d += y
    d /= len(traject)
    obs_err_std = 1e-12 + observation_noise * np.linalg.norm(d, np.inf)
    observation_error_variance = obs_err_std**2
    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=observation_operator.shape[0],
            mean=0.0,
            variance=observation_error_variance,
            random_seed=random_seed,
        )
    )
    window = (checkpoints[0], checkpoints[-1])
    problem = FourDVar(
        configs=dict(
            assimilation_window=window,
            model=model,
            prior_model=prior_model,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
            optimization_routine="scipy-l-bfgs-b",
            optimization_routine_options=optimization_routine_options,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y, return_np=True)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    # 5- Test the 4D-Var objective gradient
    if gradient_fd_validation or Hessian_fd_validation:
        print("Validating the 4D-VAR objective gradient and Hessian...")
        utility.validate_inverse_problem_derivatives(
            problem,
            eval_at=init_guess,
            use_noise=True,
            validate_gradient=gradient_fd_validation,
            validate_Hessian=Hessian_fd_validation,
            create_plots=True,
            output_dir=output_dir,
        )

    # 6- Solve the inverse problem, and plot results as needed
    if solve_inverse_problem:
        print("Solving the 4D-VAR inverse problem...")
        xa = problem.solve_inverse_problem(
            init_guess,
            skip_map_estimate=False,
            update_posterior=update_posterior,
            verbose=verbose,
        )

        print("Computing the true, prior, and posterior trajectories...")
        _, true_traj = model.integrate_state(
            u0_true, tspan=tspan, checkpoints=checkpoints
        )
        _, back_traj = model.integrate_state(
            init_guess, tspan=tspan, checkpoints=checkpoints
        )
        _, assim_traj = model.integrate_state(xa, tspan=tspan, checkpoints=checkpoints)
        rmseb = [utility.calculate_rmse(x, y) for x, y in zip(back_traj, true_traj)]
        rmsea = [utility.calculate_rmse(x, y) for x, y in zip(assim_traj, true_traj)]

        print("Computing the true, prior, and posterior objective values...")
        obj_val_prior, _ = problem.objective(prior_model.mean)
        obj_val_guess, _ = problem.objective(init_guess)
        obj_val_assim, _ = problem.objective(xa)
        obj_val_true, _ = problem.objective(u0_true)
        print(
            """The value of the 4D-VAR objective function is:
                 \r - {0}:  with prior mean
                 \r - {1}:  with initial guess
                 \r - {2}:  with analysis (MAP)
                 \r - {3}:  with true initial condition""".format(
                obj_val_prior, obj_val_guess, obj_val_assim, obj_val_true
            )
        )

        print("Plotting truth, prior, and posterior modes")
        model.plot_state(
            u0_true,
            save_to_file=os.path.join(
                output_dir,
                f"True_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            init_guess,
            save_to_file=os.path.join(
                output_dir,
                f"InitGuess_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            prior_model.mean,
            save_to_file=os.path.join(
                output_dir,
                f"PriorMean_IC.{__PLOTS_FORMAT}",
            ),
        )
        model.plot_state(
            xa,
            save_to_file=os.path.join(
                output_dir,
                f"PosteriorMean_IC.{__PLOTS_FORMAT}",
            ),
        )


    if False:
        raise NotImplementedError(
            f"Proceed here with definition of the prediction time and prediction coordinates"
            f"Create a preconditioner (reduced order Hessian), and enforce CG-type solver..."
        )

    # Create goal-oriented iverse problem
    pred_coord = problem.model.get_model_grid()
    gooper = fenics_prediction_operators.LinearPredictionOperator(
        configs={
            'inverse_problem': problem,
            'prediction_time': 1,
            'prediction_coordinates': pred_coord,
        }
    )
    goal_ip = GoalOrientedLinearInverseProblem(
        configs={
            "inverse_problem": problem,
            "goal_operator": gooper,
        }
    )
    # Return the problem now for testing
    return goal_ip


def main():
    """
    """
    # Create inverse Problem
    return goal_oriented_fourDVar_Dolfin_AD_2d()


if __name__ == "__main__":
    main()

