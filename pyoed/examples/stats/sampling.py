# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Driver script to test Sampling various algorithms with simple PDFs.
"""

# Essential imports
import os
import sys
import numpy as np

# Add PyOED root path for proper access
pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Import PyOED  modules (model, observation operator, error models)
import pyoed
from pyoed import utility
from pyoed.stats.sampling.proposals import (
    create_Gaussian_proposal,
)
from pyoed.stats.sampling.rejection import (
    create_rejection_sampler,
)
from pyoed.stats.sampling.mcmc import (
    create_mcmc_sampler,
    create_hmc_sampler,
)

def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Stats/Sampling/")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir

def banana_potential_energy_value(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    Potential energy of the posterir. This is dependent on the target state, not the momentum.
    It is the negative the posterior-log, and MUST be implemented for each distribution
    """
    x, y = state[:]
    #
    pdf_val = 1 / (2 * (1 - rho**2))
    t1 = x**2 / a**2 + a**2 * (y - b * x**2 / a**2 - b * a**2) ** 2
    t2 = -2 * rho * x * (y - b * x**2 / a**2 - b * a**2)
    pdf_val = (t1 + t2) / (2 * (1 - rho**2))
    #
    return pdf_val


def banana_potential_energy_gradient(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    Gradient of the Potential energy of the posterir.
    """
    x, y = state.flatten()
    #
    pdf_grad = np.empty(2)
    pdf_grad[:] = 1 / (2 * (1 - rho**2))
    #
    t1_x = 2 * x / a**2 + 2 * a**2 * (y - b * x**2 / a**2 - b * a**2) * (
        -2 * b * x / a**2
    )
    t1_y = 2 * a**2 * (y - b * x**2 / a**2 - b * a**2)
    t1 = np.array([t1_x, t1_y])
    #
    t2_x = -2 * rho * y + 6 * b * rho * x**2 / a**2 + 2 * rho * b * a**2

    t2_y = -2 * rho * x
    t2 = np.array([t2_x, t2_y])
    #
    pdf_grad *= t1 + t2
    #
    return pdf_grad


def banana_log_density(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    The logarithm of the banana distribution PDF
    """
    return -banana_potential_energy_value(
        state,
        a=a,
        b=b,
        rho=rho,
    )


def banana_log_density_gradient(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    The gradient of the logarithm of the banana distribution PDF
    """
    return -banana_potential_energy_gradient(
        state,
        a=a,
        b=b,
        rho=rho,
    )


def mcmc_sample_banana_distribution(
    sample_size=1000,
    method="hmc",
):
    """ """
    initial_state = [0, 0]

    if method == "hmc":
        sampler = create_hmc_sampler(
            size=2,
            log_density=banana_log_density,
            log_density_grad=banana_log_density_gradient,
            output_dir=get_base_output_dir(),
        )
    elif method == "mcmc":
        sampler = create_mcmc_sampler(
            size=2,
            log_density=banana_log_density,
            output_dir=get_base_output_dir(),
        )
    else:
        print(f"Unrecognized sampling method '{method}'")
        raise ValueError

    # Sample and return full results
    results = sampler.start_MCMC_sampling(
        sample_size=sample_size,
        initial_state=initial_state,
        full_diagnostics=True,
    )

    return results


def rejection_sample_banana_distribution(
    sample_size=1000,
    proposal_mean=[0, 3],
    proposal_variance=2,
):
    sampler = create_rejection_sampler(
        size=2,
        log_density=lambda x: -banana_potential_energy_value(x),
        proposal=create_Gaussian_proposal(
            size=2,
            mean=proposal_mean,
            variance=proposal_variance,
        ),
        output_dir=get_base_output_dir(),
    )
    sample = sampler.sample(
        sample_size=sample_size,
        full_diagnostics=True,
    )
    return sample


def main(
    run_mcmc=True,
    run_hmc=True,
    run_rejection=True,
):
    """
    Main driver. Run all the sampler examples implemented in this module based on the passed flags

    :param bool run_mcmc: if `True`, call/run the function `mcmc_sample_banana_distribution`
    :param bool run_hmc: if `True`, call/run the function `hmc_sample_banana_distribution`
    :param bool run_rejection: if `True`, call/run the function `rejection_sample_banana_distribution`
    """
    if run_mcmc:
        mcmc_sample_banana_distribution(method="mcmc")

    if run_hmc:
        mcmc_sample_banana_distribution(method="hmc")

    if run_rejection:
        rejection_sample_banana_distribution()


if __name__ == "__main__":
    main()
