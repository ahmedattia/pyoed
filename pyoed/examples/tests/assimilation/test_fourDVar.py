# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example creator functions inside pyoed.examples.starters.creators.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.assimilation import fourDVar
from pyoed.examples.tests.fixtures import(
    not_raises,
)


def test_fourDVar_AD_1d():
    """  """
    with not_raises():
        fourDVar.fourDVar_AD_1d(
            nx=4,
            optimizer_configs={'maxiter':1,},
        )

def test_fourDVar_AD_2d():
    """  """
    with not_raises():
        fourDVar.fourDVar_AD_2d(
            nx=4,
            ny=4,
            optimizer_configs={'maxiter':1,},
        )

def test_fourDVar_Brg_1d():
    """  """
    with not_raises():
        fourDVar.fourDVar_Brg_1d(
            nx=5,
            checkpoints=[0.0, 0.02, 0.04, 0.06, 0.08, 0.1],
            gradient_fd_validation=True,
            Hessian_fd_validation=True,
            solve_inverse_problem=True,
            optimizer_configs={'maxiter':1,},
        )

def test_fourDVar_Brg_2d():
    """  """
    with not_raises():
        fourDVar.fourDVar_Brg_2d(
            nx=4,
            ny=4,
            checkpoints=[0.0, 0.02, 0.04, 0.06, 0.08, 0.1],
            gradient_fd_validation=True,
            Hessian_fd_validation=True,
            solve_inverse_problem=True,
            optimizer_configs={'maxiter':1,},
            update_posterior=False,
        )

def test_fourDVar_Dolfin_AD_2d():
    """  """
    with not_raises():
        fourDVar.fourDVar_Dolfin_AD_2d(
            optimizer_configs={'maxiter':1,},
            update_posterior=False,
        )

def test_fourDVar_Lorenz_63():
    """  """
    with not_raises():
        fourDVar.fourDVar_Lorenz_63(
            dt=0.001,
            checkpoints=[0.0, 0.01, 0.02, 0.03, 0.04, 0.05],
            optimizer_configs={'maxiter':1,},
        )

def test_fourDVar_Lorenz_96():
    """  """
    with not_raises():
        fourDVar.fourDVar_Lorenz_96(
            dt=0.01,
            checkpoints=[0.0, 0.02, 0.04, 0.06, 0.08, 0.1],
            optimizer_configs={'maxiter':1,},
            gradient_fd_validation=True,
            Hessian_fd_validation=True,
            update_posterior=False,
        )


