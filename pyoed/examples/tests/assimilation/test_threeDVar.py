# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example creator functions inside pyoed.examples.starters.creators.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.assimilation import threeDVar
from pyoed.examples.tests.fixtures import(
    not_raises,
)


def test_threeDVar_toy_linear():
    """  """
    with not_raises():
        threeDVar.threeDVar_toy_linear(
            gradient_fd_validation=True,
            Hessian_fd_validation=True,
            solve_inverse_problem=True,
            optimization_routine_options={'maxiter':1,},
        )

def test_threeDVar_Dolfin_AD_2d():
    """  """
    with not_raises():
        threeDVar.threeDVar_subsurf(
            nx=11,
            ny=11,
            gradient_fd_validation=True,
            Hessian_fd_validation=True,
            solve_inverse_problem=True,
            optimization_routine_options={'maxiter':1,},
            update_posterior=False,
        )

