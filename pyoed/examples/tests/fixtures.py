# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides fixtures for the examples tests, but those will
migrate to the base directory of the unittests upon refactoring of unittests.
"""

import pytest
pytestmark = pytest.mark.examples

from contextlib import contextmanager

@contextmanager
def not_raises():
  try:
    yield
  except Exception as err:
      raise pytest.fail(f"The function/creator Failed to run. An error raised: {repr(err)}")

