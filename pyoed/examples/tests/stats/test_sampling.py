# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example creator functions inside pyoed.examples.starters.creators.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.stats import sampling
from pyoed.examples.tests.fixtures import(
    not_raises,
)


def test_sampling_main():
    """
    Test the main function in the test_sampling module
    """
    with not_raises():
        sampling.main()

