# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example functions inside pyoed.examples.oed.stochastic_binary_oed.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.oed import stochastic_binary_oed
from pyoed.examples.tests.fixtures import(
    not_raises,
)


def test_stochastic_binary_oed_main():
    """
    Test the main function in the stochastic_binary_oed module
    """
    with not_raises():
        stochastic_binary_oed.main()

