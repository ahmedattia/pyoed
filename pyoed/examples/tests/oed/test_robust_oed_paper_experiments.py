# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example functions inside pyoed.examples.oed.robust_oed_paper_experiments.py
Because all functionality is actually tested in `test_robust_oed`, no need to add further tests here.
"""

