# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example functions inside pyoed.examples.oed.robust_oed.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.tests.fixtures import(
    not_raises,
)
from pyoed.examples.oed.robust_oed import robust_oed
robust_oed.OED_RANDOMIZATION_SETTINGS.update(
    {
        "sample_size": 5,
        "optimize_sample_size": False,
        "random_seed": robust_oed.RANDOM_SEED,
    }
)

@pytest.mark.parametrize("oed_evaluation_method", ["exact", "randomized"])
def test_robust_OED_time_dependent_toy_linear(oed_evaluation_method):
    """
    Test the function robust_oed.robust_OED_time_dependent_toy_linear
    """
    with not_raises():
        robust_oed.robust_OED_time_dependent_toy_linear(
            oed_evaluation_method=oed_evaluation_method,
            maxiter=1,
            stoch_maxiter=1,
            solve_OED_problem=True,
            solve_inverse_problem=True,
            bruteforce=oed_evaluation_method=="exact",
            exhaustive_plots=False,
        )

@pytest.mark.parametrize("oed_evaluation_method", ["randomized"])
def test_robust_OED_Dolfin_AD_2d(oed_evaluation_method):
    """
    Test the function robust_oed.robust_OED_time_dependent_toy_linear
    """
    with not_raises():
        robust_oed.robust_OED_Dolfin_AD_2d(
            oed_evaluation_method=oed_evaluation_method,
            maxiter=1,
            stoch_maxiter=1,
            stoch_grad_batch_size=3,
            num_obs_time_points=2,
            solve_OED_problem=True,
            solve_inverse_problem=False,
            bruteforce=False,
            exhaustive_plots=False,
        )

