# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example functions inside pyoed.examples.oed.relaxed_oed.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.tests.fixtures import(
    not_raises,
)
from pyoed.examples.oed import relaxed_oed
relaxed_oed.OED_RANDOMIZATION_SETTINGS.update(
    {
        "sample_size": 5,
        "optimize_sample_size": False,
        "random_seed": relaxed_oed.RANDOM_SEED,
    }
)


def test_relaxed_oed_relaxed_OED_time_dependent_toy_linear():
    """
    Test the function `relaxed_OED_time_dependent_toy_linear` in the relaxed_oed module
    """
    with not_raises():
        relaxed_oed.relaxed_OED_time_dependent_toy_linear()

def test_relaxed_oed_relaxed_OED_Dolfin_AD_2d():
    """
    Test the function `relaxed_OED_Dolfin_AD_2d` in the relaxed_oed module
    """
    with not_raises():
        relaxed_oed.relaxed_OED_Dolfin_AD_2d(
            num_obs_time_points=1,
            uniform_obs_grid_size=2,
            solve_by_relaxation=True,
            maxiter=1,
            verbose=True,
        )

