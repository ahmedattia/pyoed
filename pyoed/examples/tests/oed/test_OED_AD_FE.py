# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example functions inside pyoed.examples.oed.OED_AD_FE.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.oed import OED_AD_FE
from pyoed.examples.tests.fixtures import(
    not_raises,
)

def test_OED_AD_FE():
    """
    Test the main function in the OED_AD_FE  module
    """
    OED_AD_FE.INVERSE_PROBLEM_SETTINGS.update(
        {
            'num_candidate_sensors': 2,
            'num_obs_time_points': 1,
        }
    )
    OED_AD_FE.OPTIMIZATION_CONFIGS.update(
        {
            'maxiter': 2,
            'stochastic_gradient_sample_size': 2,
        }
    )

    with not_raises():
        OED_AD_FE.main()

