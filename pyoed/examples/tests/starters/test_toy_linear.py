# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example creator functions inside pyoed.examples.starters.creators.py
"""
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.starters import toy_linear
from pyoed.examples.tests.fixtures import(
    not_raises,
)


def test_toy_linear_main():
    """
    Test the main function in the test_toy_linear module
    """
    with not_raises():
        toy_linear.main()

