# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test all example creator functions inside pyoed.examples.starters.creators.py
"""
from inspect import getmembers, isfunction
import pytest
pytestmark = pytest.mark.examples

from pyoed.examples.starters import creators
from pyoed.examples.tests.fixtures import(
    not_raises,
)

def test_all_creators():
    """
    Load all creator functions, and test that each one runs properly.
    """
    # TODO: write down each function rather than looping in case we create
    # a function that does not start with 'create_*'

    def iscreator(func):
        return isfunction(func) and func.__name__.startswith('create_')

    all_functions = getmembers(creators, iscreator)
    for _, creator_function in all_functions:
        with not_raises():
            creator_function()

