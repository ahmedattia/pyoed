# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


All Unit tests for the examples to make sure they are upto date with package updates.
The whole point is to make sure all examples run, not that they produce correct results for their purposes.
