#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


# Add PyOED root path for proper access (without installing PyOED)
import sys, os

pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

plt.rcParams.update(
    {"text.usetex": True, "font.family": "sans-serif", "font.sans-serif": "Helvetica"}
)

# Import simulation model, observation oeprator, error (prior and observation noise) model, and DA
import pyoed
from pyoed.models.simulation_models.toy_quark import ToyQuark
from pyoed.models.observation_operators.interpolation import SelectionOperator
from pyoed.models.observation_operators.identity import Identity
from pyoed.models.error_models.Gaussian import GaussianErrorModel
from pyoed.assimilation.filtering.threeDVar import VanillaThreeDVar
from pyoed.stats.sampling import mcmc


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Starters/ToyQuarkModel")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir

def create_toy_quark_inverse_problem(
    true_param=np.array([1.0, 0.5, 2.5, 0.25, 0.1, 3.0]),
    x_grid=np.linspace(0.1, 0.9, 50),
    solve_inverse_problem=True,
    output_plots=True,
):
    # Create the simulation model
    model = ToyQuark(configs={"x_grid": x_grid, "Nq": 2})

    if output_plots:
        q1 = model.q_i(true_param, 1)
        q2 = model.q_i(true_param, 2)
        fig, ax = plt.subplots()
        ax.plot(x_grid, q1, linewidth=2, label="$q_1(x)$")
        ax.plot(x_grid, q2, linewidth=2, label="$q_2(x)$")
        ax.plot(x_grid, q1 + q2, linewidth=2, label="$\\sigma(x)$")
        ax.set_xlabel("x")
        ax.legend()
        saveto = os.path.join(get_base_output_dir(), "toy_quark_model.png")
        fig.savefig(saveto, bbox_inches="tight", dpi=512)

    obs_oper = Identity(configs={"model": model})

    # Create prior and observation error model
    prior = GaussianErrorModel(
        configs={
            "size": true_param.size,
            "mean": 1.0,
            "variance": 2.0,
            "random_seed": RANDOM_SEED,
        }
    )

    true_traject = model(true_param)
    obs_noise = GaussianErrorModel(
        configs={
            "size": obs_oper.shape[0],
            "variance": 0.01 * true_traject,
            "random_seed": RANDOM_SEED,
        }
    )

    # 4DVar DA object, and register its elements
    inverse_problem = VanillaThreeDVar(
        configs={
            "model": model,
            "invert_for": "parameter",
            "optimizer": "ScipyOptimizer",
        }
    )
    inverse_problem.register_observation_operator(obs_oper)
    inverse_problem.register_prior(prior)
    inverse_problem.register_observation_error_model(obs_noise)

    # Create truth (true initial state and trajectory)

    noisy_obs = obs_noise.add_noise(obs_oper(true_traject))
    inverse_problem.register_observations(noisy_obs)

    if output_plots:  # Visualize truth vs
        fig, ax = plt.subplots()
        ax.plot(x_grid, true_traject, linewidth=2, label="Truth")
        ax.plot(x_grid, noisy_obs, "o", linewidth=2, label="Noisy Data")

    if solve_inverse_problem:
        # Solve the inverse problem (retrieve the truth)
        inverse_problem.solve(
            init_guess=prior.mean.copy(),
            update_posterior=True,
        )
        if output_plots:
            map_traject = model(inverse_problem.posterior.mean)
            ax.plot(x_grid, map_traject, "--", linewidth=2, label="MAP")

    if output_plots:
        ax.set_xlabel("x")
        ax.set_ylabel("$\\sigma(x)$")
        ax.legend()
        saveto = os.path.join(get_base_output_dir(), "toy_quark_truth_vs_obs.png")
        fig.savefig(saveto, bbox_inches="tight", dpi=512)

    return inverse_problem, true_param, true_traject

def sample_posterior(
    inverse_problem,
    sample_size,
    random_seed=RANDOM_SEED,
):
    """
    Given the inverse pblem, generate samples from the posterior
    (given the underlying design) using HMC.
    The posterior is formulated based on the current design in the inverse pblem.
    """
    sampler = mcmc.create_mcmc_sampler(
        size=inverse_problem.prior.size,
        log_density=lambda x: -inverse_problem.objective_function_value(x),
        random_seed=random_seed,
    )
    posterior_samples = sampler.sample(
        sample_size=sample_size,
        initial_state=inverse_problem.posterior.mean,
    )
    return posterior_samples

def heatmap(
    data, row_labels, col_labels, ax=None, cbar_kw=None, cbarlabel="", **kwargs
):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (M, N).
    row_labels
        A list or array of length M with the labels for the rows.
    col_labels
        A list or array of length N with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if ax is None:
        ax = plt.gca()

    if cbar_kw is None:
        cbar_kw = {}

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(data.shape[1]), labels=col_labels)
    ax.set_yticks(np.arange(data.shape[0]), labels=row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right", rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - 0.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - 0.5, minor=True)
    ax.grid(which="minor", color="w", linestyle="-", linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar

def annotate_heatmap(
    im,
    data=None,
    valfmt="{x:.2f}",
    textcolors=("black", "white"),
    threshold=None,
    **textkw
):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max()) / 2.0

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center", verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts


def main():
    # Create the inverse problemm
    inverse_problem, true_param, true_traject = create_toy_quark_inverse_problem()

    labels = [
        "$N_1$",
        "$\\alpha_1$",
        "$\\beta_1$",
        "$N_2$",
        "$\\alpha_2$",
        "$\\beta_2$",
    ]

    # imshow the covariance matrix
    fig, ax = plt.subplots()
    cov = inverse_problem.posterior.covariance_matrix()
    im, cbar = heatmap(
        cov, labels, labels, ax=ax, cmap="YlGn", cbarlabel="Covariance Matrix Entries"
    )
    texts = annotate_heatmap(im, valfmt="{x:.2f}")
    fig.tight_layout()
    saveto = os.path.join(get_base_output_dir(), "toy_quark_covariance_heatmap.png")
    fig.savefig(saveto, bbox_inches="tight", dpi=512)

    samples = sample_posterior(inverse_problem, sample_size=1000, )

    try:
        import pandas as pd

        samples = pd.DataFrame(samples, columns=labels)

        fig, ax = plt.subplots()
        pd.plotting.scatter_matrix(samples, ax=ax)
        saveto = os.path.join(get_base_output_dir(), "toy_quark_posterior_samples.png")
        fig.savefig(saveto, bbox_inches="tight", dpi=512)

        corr_mat = samples.corr()
    except:
        print("Pandas not installed. Skipping scatter matrix plots.")
        samples = np.array(samples)
        corr_mat = np.corrcoef(samples, rowvar=False)

    fig, ax = plt.subplots()
    im, cbar = heatmap(
        corr_mat,
        labels,
        labels,
        ax=ax,
        cmap="YlGn",
        cbarlabel="MCMC Samples correlation matrix Entries",
    )
    texts = annotate_heatmap(im, valfmt="{x:.2f}")
    fig.tight_layout()
    saveto = os.path.join(get_base_output_dir(), "toy_quark_sample_corr_heatmap.png")
    fig.savefig(saveto, bbox_inches="tight", dpi=512)


if __name__ == "__main__":
    main()
