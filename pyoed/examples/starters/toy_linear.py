#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


# Add PyOED root path for proper access (without installing PyOED)
import sys, os
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility

# Import simulation model, observation oeprator, error (prior and observation noise) model, and DA
import pyoed
from pyoed.models.simulation_models.toy_linear import ToyLinearTimeDependent
from pyoed.models.observation_operators.identity import Identity
from pyoed.models.error_models import Gaussian
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar
from pyoed import utility
from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED


def get_base_output_dir():
    """
    Base output dir that enables modifying base output dir by changing pyoed settings class
    This is very helpful to globally steer output e.g., when running unittests.
    """
    output_dir = os.path.join(pyoed.SETTINGS.OUTPUT_DIR, "Starters/")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_dir


def create_linear_inverse_problem(
    state_size=3,
    dt=0.1,
    tspan=(0, 0.3),
    checkpoints=[0.1, 0.2, 0.3],
    solve_inverse_problem=True,
):
    # Create the simulation model
    model = ToyLinearTimeDependent(
        configs={
            "nx": state_size,
            "dt": dt,
            "random_seed": RANDOM_SEED,
        }
    )

    # Create (linear/identity) observation operator
    obs_oper = Identity(configs={"model": model})

    # Create prior and observation error model
    prior = Gaussian.GaussianErrorModel(
        configs={
            "size": model.state_size,
            "mean": 0.25,
            "variance": 1,
            "random_seed": RANDOM_SEED,
        }
    )
    obs_noise = Gaussian.PointwiseWeightedGaussianErrorModel(
        configs={
            "size": obs_oper.shape[0],
            "variance": 0.01,
            "random_seed": RANDOM_SEED,
        }
    )

    # 4DVar DA object, and register its elements
    inverse_problem = VanillaFourDVar()
    inverse_problem.register_model(model)
    inverse_problem.register_observation_operator(obs_oper)
    inverse_problem.register_prior(prior)
    inverse_problem.register_observation_error_model(obs_noise)

    # Set the assimilation/simulation time window
    inverse_problem.register_window(tspan)

    # Create truth (true initial state and trajectory)
    true_IC = model.create_initial_condition()
    _, true_traject = model.integrate_state(
        true_IC, tspan=tspan, checkpoints=checkpoints
    )

    # Create synthetic data (perturbation to truth) and register them
    for t, state in zip(checkpoints, true_traject):
        obs = obs_noise.add_noise(obs_oper(state))
        inverse_problem.register_observation(t=t, observation=obs)

    if solve_inverse_problem:
        # Solve the inverse problem (retrieve the truth)
        inverse_problem.solve(
            init_guess=prior.mean.copy(), update_posterior=True
        )

        prior_rmse = utility.calculate_rmse(true_IC, inverse_problem.prior.mean)
        posterior_rmse = utility.calculate_rmse(true_IC, inverse_problem.posterior.mean)
        print(f"Prior RMSE: {prior_rmse}")
        print(f"Posterrior RMSE: {posterior_rmse}")

        # Calculate prior and posterior trajectory and evaluate RMSE
        checkpoints, true_traject = model.integrate_state(true_IC, tspan=tspan)
        _, prior_traject = model.integrate_state(
            inverse_problem.prior.mean, tspan=tspan
        )
        _, posterior_traject = model.integrate_state(
            inverse_problem.posterior.mean, tspan=tspan
        )
        prior_rmse = [
            utility.calculate_rmse(xp, xt)
            for xp, xt in zip(prior_traject, true_traject)
        ]
        posterior_rmse = [
            utility.calculate_rmse(xp, xt)
            for xp, xt in zip(posterior_traject, true_traject)
        ]

        utility.plots_enhancer(usetex=True)

        # Plot RMSE (for prior and posterior/analysis)
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(111)
        _ = ax.plot(checkpoints, prior_rmse, "-", lw=4, label="Prior")
        _ = ax.plot(checkpoints, posterior_rmse, "-", lw=4, label="Posterior")
        ax.set_xlabel("Time")
        ax.set_ylabel("RMSE")
        ax.legend(loc="best")
        utility.show_axis_grids(ax)
        saveto = os.path.join(get_base_output_dir(), "ToyLinear4DVar_RMSE.pdf")
        fig.savefig(saveto, dpi=600, bbox_inches="tight", transparent=True)

        # IP posterrior covariance matrix
        post_cov = inverse_problem.posterior.covariance_matrix()

        # Exact (from R, B, F)
        F = model.get_model_array()
        Rinv = obs_noise.precision_matrix()
        Binv = prior.precision_matrix()
        A = np.zeros_like(Binv)
        for i in range(1, 4):
            M = np.linalg.matrix_power(F, i)
            A += np.dot(M.T, np.dot(Rinv, M))
        A = np.linalg.inv(Binv + A)
        Aerr = np.abs(post_cov - A)  # Error/mismatch

        # Plot
        fig, axes = plt.subplots(1, 3, figsize=(12, 12))
        im = axes[0].imshow(post_cov)
        divider = make_axes_locatable(axes[0])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, orientation="vertical", cax=cax)

        im = axes[1].imshow(A)
        divider = make_axes_locatable(axes[1])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, orientation="vertical", cax=cax)

        im = axes[2].imshow(Aerr)
        divider = make_axes_locatable(axes[2])
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, orientation="vertical", cax=cax)

        # Adjust space between subplots
        plt.subplots_adjust(
            wspace=1.25,
            hspace=0.25,
        )

        saveto = os.path.join(get_base_output_dir(), "ToyLinear4DVar_PostCov.pdf")
        fig.savefig(saveto, dpi=600, bbox_inches="tight", transparent=True)

    return inverse_problem, true_IC

def create_relaxed_oed_problem(
    inverse_problem=None,
    solve_oed_problem=True,
):
    """"""
    if inverse_problem is None:
        inverse_problem, _ = create_linear_inverse_problem()

    oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="relaxed bayesian a-opt",
            optimizer="ScipyOptimizer",
            inverse_problem=inverse_problem,
            problem_is_linear=True,
        )
    )
    # Set optimizer's bounds to (0, 1)
    oed_problem.optimizer.update_configurations(bounds=(0, 1))

    if solve_oed_problem:
        oed_results = oed_problem.solve()
        print(f"Optimal design: {str(oed_results.optimal_design)}")

    else:
        oed_results = None

    return oed_problem, oed_results

def create_binary_oed_problem(
    inverse_problem=None,
    solve_oed_problem=True,
):
    """"""
    if inverse_problem is None:
        inverse_problem, _ = create_linear_inverse_problem()

    oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=inverse_problem,
            problem_is_linear=True,
        )
    )

    if solve_oed_problem:
        oed_results = oed_problem.solve()

        # Plot results
        oed_problem.plot_results(oed_results, bruteforce=True, overwrite=True)

    else:
        oed_results = None
    return oed_problem, oed_results


def main():
    # Create the inverse problemm
    inverse_problem, true_IC = create_linear_inverse_problem()

    # Trying Relaxed OED (no penalty)
    relaxed_oed_problem, _ = create_relaxed_oed_problem(
        inverse_problem=inverse_problem,
        solve_oed_problem=True,
    )

    # Trying Stochastic Binary OED (no penalty)
    binary_oed_problem, _ = create_binary_oed_problem(
        inverse_problem=inverse_problem,
        solve_oed_problem=True,
    )

    # Trying Relaxed OED (AGAIN) with more flexibility
    # Play more with the relaxed oed problem; test gradient, add a penalty function, & resolve
    budget = 1
    penalty_f_l2 = lambda design: np.power(np.sum(design) - budget, 2)
    penalty_f_l2_grad = (
        lambda design: 2 * (np.sum(design) - budget) * np.ones_like(design)
    )
    relaxed_oed_problem.register_penalty_term(
        penalty_weight=0,
        penalty_function=penalty_f_l2,
        penalty_function_gradient=penalty_f_l2_grad,
    )

    # Test the gradient
    d = np.ones(relaxed_oed_problem.design_size) * 0.75
    utility.validate_function_gradient(
        fun=relaxed_oed_problem.oed_objective,
        state=d,
        gradient=relaxed_oed_problem.objective_function_gradient(d)
    )

    # Now, solve the problem
    relaxed_oed_results = relaxed_oed_problem.solve()


if __name__ == "__main__":
    main()
