# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This modules contain essential functions to create simple functions to create/construct
basic elements of the OED probcess, including creating error models, inverse problems,
OED problems, utility functions, optimizers, etc.
The functions created have oversimplified structure which is essential for unittesting
and expose all elements of the problem at various complexities but all withing simplified
settings such as spatiotemporal dimensionality and assumptions (linearity, Gaussianity, etc.)

.. :note::
    This starts as a module, but it may evolve into a small subpackage
"""

# #######
# Imports
# #######
# PyOED Path
import sys, os

pyoed_path = os.path.join(os.path.dirname(__file__), "../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Import PyOED components
from pyoed.models.simulation_models import toy_linear
from pyoed.models.observation_operators import identity, power
from pyoed.models.error_models import Gaussian
from pyoed.assimilation.filtering import threeDVar
from pyoed.assimilation.smoothing import fourDVar
from pyoed import utility
from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED

# ########
# Settings
# ########
STATE_SIZE = 5  # model state size
PARAMETER_SIZE = 3  # model parameter size
OBSERVATION_SIZE = 5  # observation vector size
PRIOR_MEAN = 2  # Mean of the inversion prior
PRIOR_VARIANCE = 0.16  # Prior variance/covariance
OBS_NOISE_VARIANCE = 0.01  # Observation error/noise variance
RANDOM_SEED = 1011  # For reproducibility
GROUND_TRUTH = 1  # Ground truth to retrieve (of size equal to inference parameter)
MODEL_STEPSIZE = 0.2  # Stepsize of time integration schemes of the simple models

# Time Dependent settings
WINDOW = [0.0, 0.6]
OBS_CHECKPOINTS = [0.2, 0.4, 0.6]


# ################
# Inverse Problems
# ################
def create_toy_linear_time_independent_inverse_problem():
    """
    Simulation model: toy linear time independent
    Observation operator: Identity
    Prior: Gaussian
    Noise: Gaussian
    Inversion: 3DVar (Filtering) invert for model parameter

    :returns:
        - ip: the inverse problem
    """
    ## Simulation model
    model = toy_linear.create_ToyLinearTimeIndependent_model(
        parameter_size=PARAMETER_SIZE,
        state_size=STATE_SIZE,
        random_seed=RANDOM_SEED,
    )

    ## Observation Operator
    obs_oper = identity.create_Identity_observation_operator(
        model=model,
    )

    ## Prior
    prior = Gaussian.create_GaussianErrorModel(
        size=PARAMETER_SIZE,
        mean=PRIOR_MEAN,
        variance=PRIOR_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Noise (Observation Error)
    obs_noise = Gaussian.create_PointwiseWeightedGaussianErrorModel(
        size=obs_oper.shape[0],
        mean=0.0,
        variance=OBS_NOISE_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Inverse Problem (ip) & Synthetic Data
    ip = threeDVar.VanillaThreeDVar(
        configs={
            "model": model,
            "invert_for": "parameter",
            "prior": prior,
            "observation_operator": obs_oper,
            "observation_error_model": obs_noise,
        }
    )

    # Create and register observations (from ground truth)
    truth = model.parameter_vector()
    truth[:] = GROUND_TRUTH
    obs = obs_oper.apply(model.solve_forward(truth))  # True observation
    obs = obs_noise.add_noise(obs)  # Noisy observation
    ip.register_observations(obs)

    return ip


def create_toy_linear_time_dependent_inverse_problem():
    """
    Simulation model: toy linear time dependent
    Observation operator: Identity
    Prior: Gaussian
    Noise: Gaussian
    Inversion: 4DVar (Smoothing) invert for model state (initial condition)
    """
    ## Simulation model
    model = toy_linear.create_ToyLinearTimeIndependent_model(
        parameter_size=STATE_SIZE,
        state_size=STATE_SIZE,
        random_seed=RANDOM_SEED,
    )

    model = toy_linear.create_ToyLinearTimeDependent_model(
        nx=STATE_SIZE,
        dt=MODEL_STEPSIZE,
        random_seed=RANDOM_SEED,
    )

    ## Observation Operator
    obs_oper = identity.create_Identity_observation_operator(
        model=model,
    )

    ## Prior
    prior = Gaussian.create_GaussianErrorModel(
        size=STATE_SIZE,
        mean=PRIOR_MEAN,
        variance=PRIOR_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Noise (Observation Error)
    obs_noise = Gaussian.create_PointwiseWeightedGaussianErrorModel(
        size=obs_oper.shape[0],
        mean=0.0,
        variance=OBS_NOISE_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Inverse Problem (ip) & Synthetic Data
    ip = fourDVar.VanillaFourDVar(
        configs={
            "model": model,
            "prior": prior,
            "observation_operator": obs_oper,
            "observation_error_model": obs_noise,
            "window": WINDOW,
        }
    )

    # Create and register observations
    truth = model.state_vector()
    truth[:] = GROUND_TRUTH
    tspan = (WINDOW[0], WINDOW[1])
    obs_times, true_states = model.integrate_state(
        truth,
        tspan=tspan,
        checkpoints=OBS_CHECKPOINTS,
    )

    for t, state in zip(obs_times, true_states):
        obs = obs_noise.add_noise(obs_oper.apply(state))
        ip.register_observation(
            t=t,
            observation=obs,
        )

    return ip


def create_toy_nonlinear_time_independent_inverse_problem():
    """
    Simulation model: toy linear time independent
    Observation operator: Power
    Prior: Gaussian
    Noise: Gaussian
    Inversion: 3DVar (Filtering) invert for model parameter
    """
    ## Simulation model
    model = toy_linear.create_ToyLinearTimeIndependent_model(
        parameter_size=PARAMETER_SIZE,
        state_size=STATE_SIZE,
        random_seed=RANDOM_SEED,
    )

    ## Observation Operator
    obs_oper = power.create_Power_observation_operator(
        model=model,
        power=3,
    )

    ## Prior
    prior = Gaussian.create_GaussianErrorModel(
        size=PARAMETER_SIZE,
        mean=PRIOR_MEAN,
        variance=PRIOR_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Noise (Observation Error)
    obs_noise = Gaussian.create_PointwiseWeightedGaussianErrorModel(
        size=obs_oper.shape[0],
        mean=0.0,
        variance=OBS_NOISE_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Inverse Problem (ip) & Synthetic Data
    ip = threeDVar.VanillaThreeDVar(
        configs={
            "model": model,
            "invert_for": "parameter",
            "prior": prior,
            "observation_operator": obs_oper,
            "observation_error_model": obs_noise,
        }
    )

    # Create and register observations (from ground truth)
    truth = model.parameter_vector()
    truth[:] = GROUND_TRUTH
    obs = obs_oper.apply(model.solve_forward(truth))  # True observation
    obs = obs_noise.add_noise(obs)  # Noisy observation
    ip.register_observations(obs)

    return ip


def create_toy_nonlinear_time_dependent_inverse_problem():
    """
    Simulation model: toy linear time dependent (for smoothing)
    Observation operator: Power
    Prior: Gaussian
    Noise: Gaussian
    Inversion: 4DVar (Smoothing) invert for model state (initial condition)
    """
    ## Simulation model
    model = toy_linear.create_ToyLinearTimeDependent_model(
        nx=STATE_SIZE,
        dt=MODEL_STEPSIZE,
        random_seed=RANDOM_SEED,
    )

    ## Observation Operator
    obs_oper = power.create_Power_observation_operator(
        model=model,
        power=3,
    )

    ## Prior
    prior = Gaussian.create_GaussianErrorModel(
        size=STATE_SIZE,
        mean=PRIOR_MEAN,
        variance=PRIOR_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Noise (Observation Error)
    obs_noise = Gaussian.create_PointwiseWeightedGaussianErrorModel(
        size=obs_oper.shape[0],
        mean=0.0,
        variance=OBS_NOISE_VARIANCE,
        random_seed=RANDOM_SEED,
    )

    ## Inverse Problem (ip) & Synthetic Data
    ip = fourDVar.VanillaFourDVar(
        configs={
            "model": model,
            "prior": prior,
            "observation_operator": obs_oper,
            "observation_error_model": obs_noise,
            "window": WINDOW,
        }
    )

    # Create and register observations
    truth = model.state_vector()
    truth[:] = GROUND_TRUTH
    tspan = (WINDOW[0], WINDOW[1])
    obs_times, true_states = model.integrate_state(
        truth,
        tspan=tspan,
        checkpoints=OBS_CHECKPOINTS,
    )

    for t, state in zip(obs_times, true_states):
        obs = obs_noise.add_noise(obs_oper.apply(state))
        ip.register_observation(
            t=t,
            observation=obs,
        )

    return ip


# ############
# OED Problems
# ############
def create_toy_linear_time_independent_relaxed_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="relaxed bayesian a-opt",
            optimizer="ScipyOptimizer",
            inverse_problem=create_toy_linear_time_independent_inverse_problem(),
            problem_is_linear=True,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_nonlinear_time_independent_relaxed_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="relaxed bayesian a-opt",
            optimizer="ScipyOptimizer",
            inverse_problem=create_toy_nonlinear_time_independent_inverse_problem(),
            problem_is_linear=False,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_linear_time_dependent_relaxed_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="relaxed bayesian a-opt",
            optimizer="ScipyOptimizer",
            inverse_problem=create_toy_linear_time_dependent_inverse_problem(),
            problem_is_linear=True,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_nonlinear_time_dependent_relaxed_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="relaxed bayesian a-opt",
            optimizer="ScipyOptimizer",
            inverse_problem=create_toy_nonlinear_time_dependent_inverse_problem(),
            problem_is_linear=False,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_linear_time_independent_binary_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=create_toy_linear_time_independent_inverse_problem(),
            problem_is_linear=True,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_nonlinear_time_independent_binary_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=create_toy_nonlinear_time_independent_inverse_problem(),
            problem_is_linear=False,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_linear_time_dependent_binary_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=create_toy_linear_time_dependent_inverse_problem(),
            problem_is_linear=True,
            random_seed=RANDOM_SEED,
        )
    )


def create_toy_nonlinear_time_dependent_binary_OED_problem():
    """ """
    return SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=create_toy_nonlinear_time_dependent_inverse_problem(),
            problem_is_linear=False,
            random_seed=RANDOM_SEED,
        )
    )

