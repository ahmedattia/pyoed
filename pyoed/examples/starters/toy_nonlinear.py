#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


# Add PyOED root path for proper access (without installing PyOED)
import sys, os
import pdb

pyoed_path = os.path.abspath("../../../")
if not pyoed_path in sys.path:
    sys.path.append(pyoed_path)

# Settings
# ========
RANDOM_SEED = 1011  # For reproducibility

# Import simulation model, observation oeprator, error (prior and observation noise) model, and DA
from pyoed.models.simulation_models.toy_linear import ToyLinearTimeDependent
from pyoed.models.observation_operators.power import Exponential, Power
from pyoed.models.observation_operators.interpolation import SelectionOperator
from pyoed.models.observation_operators.identity import Identity
from pyoed.models.error_models import Gaussian
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar
from pyoed.oed.sensor_placement import SensorPlacementBayesianInversionOED
from pyoed.stats.sampling import mcmc


def create_nonlinear_inverse_problem(
    state_size=3,
    dt=0.1,
    tspan=(0, 0.3),
    checkpoints=[0.1, 0.2, 0.3],
    solve_inverse_problem=True,
):
    # Create the simulation model
    model = ToyLinearTimeDependent(
        configs={
            "nx": state_size,
            "dt": dt,
            "random_seed": RANDOM_SEED,
        }
    )

    # Create (nonlinear/here, Power) observation operator
    obs_oper = Power(
        configs={
            "model": model,
            "power": 3,
        }
    )

    # Create prior and observation error model
    prior = Gaussian.GaussianErrorModel(
        configs={
            "size": model.state_size,
            "mean": 0.25,
            "variance": 1,
            "random_seed": RANDOM_SEED,
        }
    )
    obs_noise = Gaussian.PointwiseWeightedGaussianErrorModel(
        configs={
            "size": obs_oper.shape[0],
            "variance": 0.01,
            "random_seed": RANDOM_SEED,
        }
    )

    # 4DVar DA object, and register its elements
    inverse_problem = VanillaFourDVar()
    inverse_problem.register_model(model)
    inverse_problem.register_observation_operator(obs_oper)
    inverse_problem.register_prior(prior)
    inverse_problem.register_observation_error_model(obs_noise)

    # Set the assimilation/simulation time window
    inverse_problem.register_window(tspan)

    # Create truth (true initial state and trajectory)
    true_IC = model.create_initial_condition()
    _, true_traject = model.integrate_state(
        true_IC, tspan=tspan, checkpoints=checkpoints
    )

    # Create synthetic data (perturbation to truth) and register them
    for t, state in zip(checkpoints, true_traject):
        obs = obs_noise.add_noise(obs_oper(state))
        inverse_problem.register_observation(t=t, observation=obs)

    if solve_inverse_problem:
        # Solve the inverse problem (retrieve the truth)
        inverse_problem.solve(
            init_guess=prior.mean.copy(),
            update_posterior=True,
        )

    return inverse_problem, true_IC


def create_oed_problem(
    inverse_problem=None,
    problem_is_linear=False,
    solve_oed_problem=False,
    plot_results=True,
):
    """
    Create an OED problem given an inverse problem
    and potententially solve it and plot results
    """
    if inverse_problem is None:
        inverse_problem, _ = create_nonlinear_inverse_problem()

    # Create OED problem
    stochastic_oed_problem = SensorPlacementBayesianInversionOED(
        configs=dict(
            criterion="bayesian a-opt",
            optimizer="BinaryReinforceOptimizer",
            inverse_problem=inverse_problem,
            problem_is_linear=problem_is_linear,
        )
    )

    # Solve the OED problem
    if solve_oed_problem:
        stochastic_oed_results = stochastic_oed_problem.solve()

        if plot_results:
            stochastic_oed_problem.plot_results(
                stochastic_oed_results,
                bruteforce=True,
                overwrite=True,
            )
    else:
        stochastic_oed_results = None
    return stochastic_oed_problem, stochastic_oed_results

def create_posterior_log_density(inverse_problem):
    """
    Given the inverse problem create and return a function
    that evaluates the log density (unnormalized)
    at a given parameter/state
    """

    def log_density(state):
        """
        Evaluate the log desnity of the posterior distribution at the passed state.
        Note that the posterior log density is the negative of the 4D-Var objective
        """
        return -inverse_problem.objective_function_value(
            state,
        )

    return log_density

def create_posterior_log_density_gradient(inverse_problem):
    """
    Given the inverse problem create and return a function
    that evaluates the gradient of the log density (unnormalized)
    at a given parameter/state
    """

    def log_density_gradient(state):
        """
        Evaluate the gradient of the log desnity of the posterior distribution at the passed state.
        """
        return -inverse_problem.objective_function_gradient(
            state,
        )

    return log_density_gradient

def sample_posterior(
    inverse_problem,
    sample_size,
    # Other parameters with default kwargs; we can play with those for better performance
    burn_in=100,
    mix_in=5,
    symplectic_integrator="3-stage",
    symplectic_integrator_stepsize=0.001,
    symplectic_integrator_num_steps=20,
    mass_matrix=0.1,
    random_seed=RANDOM_SEED,
):
    """
    Given the inverse pblem, generate samples from the posterior
    (given the underlying design) using HMC.
    The posterior is formulated based on the current design in the inverse pblem.
    """
    sampler = mcmc.create_hmc_sampler(
        size=inverse_problem.prior.size,
        log_density=create_posterior_log_density(inverse_problem),
        log_density_grad=create_posterior_log_density_gradient(inverse_problem),
        # Other parameters with default kwargs; we can play with those for better performance
        burn_in=burn_in,
        mix_in=mix_in,
        symplectic_integrator=symplectic_integrator,
        symplectic_integrator_stepsize=symplectic_integrator_stepsize,
        symplectic_integrator_num_steps=symplectic_integrator_num_steps,
        mass_matrix=mass_matrix,
        random_seed=random_seed,
    )
    posterior_samples = sampler.sample(sample_size=sample_size)
    return posterior_samples


def main():
    # Create the inverse problemm
    inverse_problem, true_IC = create_nonlinear_inverse_problem()

    # Trying Stochastic Binary OED (no penalty)
    stochastic_oed_problem, stochastic_oed_results = create_oed_problem(
        inverse_problem=inverse_problem,
        problem_is_linear=False,
        solve_oed_problem=True,
        plot_results=True,
    )


if __name__ == "__main__":
    main()
