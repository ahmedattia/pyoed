# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides access to functions to perform plotting and visualization operations.
"""
import os
import shutil
import warnings

from typing import Callable

import numpy as np
import scipy as sp
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import mpl_toolkits.mplot3d.art3d as art3d

try:
    import pandas as pd
except ImportError:
    pd = None
try:
    import seaborn as sns
except ImportError:
    sns = None

__all__ = [
    "plots_enhancer",
    "show_axis_grids",
    "unique_colors",
    "draw_curly_brace",
    "plot_sampling_results",
    "plot_sensors",
    "add_circle_to_axis",
    "axis_cartesian_dimensions",
    "add_box_to_axis",
    "add_cube_to_axis",
    "add_3D_cylinder_to_axis",
    "ridgeline",
]


# Plotting & Visualization:
# -------------------------
def plots_enhancer(
    fontsize=22,
    fontfamily="sans-serif",
    fontweight="bold",
    usetex=True,
):
    """
    Basic manipultation of matplotlib plotting parameters
    """
    font = {"family": fontfamily, "weight": fontweight, "size": fontsize}
    matplotlib.rc("font", **font)
    tex_available = shutil.which("latex")
    if usetex and not tex_available:
        warnings.warn(
            f"TeX is not available on this machine. "
            f"Turning `usetex` off in utility.plots_enhancer"
        )
        usetex = False

    if usetex:
        matplotlib.rcParams["text.latex.preamble"] = r"\usepackage{amsmath}"
        matplotlib.rcParams["text.latex.preamble"] = r"\boldmath"
    matplotlib.rc("text", usetex=usetex)


def show_axis_grids(
    ax,
    show_major=True,
    show_minor=True,
    major_color="black",
    minor_color="gray",
    major_alpha=0.75,
    minor_alpha=0.50,
):
    """
    Given an axis, show the major/minor axis grids
    """
    # Don't allow the axis to be on top of your data
    ax.set_axisbelow(True)

    if show_major:
        # Customize the major grid
        ax.grid(
            which="major",
            linestyle="-",
            linewidth="0.5",
            color=major_color,
            alpha=major_alpha,
        )

    if show_minor:
        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()
        # Customize the minor grid
        ax.grid(
            which="minor",
            linestyle=":",
            linewidth="0.5",
            color=minor_color,
            alpha=minor_alpha,
        )

    return ax


def unique_colors(size, random_seed=2456, gradual=False):
    """
    get uniqe random colors on the format (R, G, B) to be used for plotting with many lines
    """
    # Get the current state of Numpy.Random, and set the seed
    if gradual:
        G_vals = np.linspace(0.0, 0.5, size, endpoint=False)
        R_vals = np.linspace(0.0, 0.3, size, endpoint=False)
        B_vals = np.linspace(0.5, 1, size, endpoint=False)
    else:
        # Create random number generator from the passed seed
        rng = np.random.default_rng(random_seed)

        G_vals = np.linspace(0.0, 1.0, size, endpoint=False)
        R_vals = rng.choice(range(size), size=size, replace=False) / float(size)
        B_vals = rng.choice(range(size), size=size, replace=False) / float(size)

    return [(r, g, b) for r, g, b in zip(R_vals, G_vals, B_vals)]


def draw_curly_brace(
    ax,
    span,
    draw_on="x",
    location="low",
    padding=None,
    padding_scale=0.11,
    brace_scale=0.04,
    curl_scale=0.12,
    linewidth=3,
    color="black",
    text=None,
    text_color="black",
    text_rotation=None,
    text_position=None,
    text_padding_scale=0.1,
):
    """
    Draw a curly brace with optional text on a given range on either x or y axis.
    This is very useful to define double axes, and range of values, etc.

    All positions and sizes are in axes units (i.e., following x-values or y-values based on`draw_on`)

    :param ax: matplotlib axis to draw curly brace on (one of it's plot axes (x/y))
    :param tuple span: (from, to) points to draw the curly brace (on either x or y axis)
    :param str draw_on: either 'x' to draw on x-axis or 'y' for y-axis
    :param str location: either 'low' or 'high' to draw at the lower level of values or the
        high levels; on x-axis this is bottom vs. top of the figure
        Note that if the axis is flipped (e.g., imshow is used, you
        need to invert the axis first, then invert it againQ)
    :param padding: exact padding; values to push the brace away from the axis
    :param float padding_scale: fraction used to push the brace away from the axis;
        padding value is evaluated as a fraction of the opposite axis span/values
        ignored if `padding` is not None
    :param float brace_scale: scale the brace by a factor after calculation if needed; default 1.0
    :param float curl_scale: fraction of the opposit axis to use for calculating the curl/radius
        (rounded curve) of the brace.
    :param float linewidth: linewidth parameter for line plotting
    :param color: line color
    :param text: text to add to the brace (can be put anywhere) if not `None`
    :param text_color: color of the text
    :param text_rotation: rotation angle of the text;
        default is 0 if draw_on is 'x', 90 if 'draw_on' is 'y'
    :param text_position: position (x, y) for the text
    :param text_padding_scale: used if text_position is None; padding is this percentage (from text center)
        multiplied by the hight (e.g., for x-axis plotting) of the brace
    """

    def calculate_padding(draw_on, padding, padding_scale):
        if padding is None:
            if padding_scale is None or padding_scale <= 0:
                raise ValueError(
                    "padding scale must be positive vlaue when padding is None"
                )
            if draw_on == "x":
                ax_lim = ax.get_ylim()
            else:
                ax_lim = ax.get_xlim()
            padding = padding_scale * (ax_lim[1] - ax_lim[0])
        return padding

    def calculate_resolution(min_v, max_v, draw_on):
        """
        Given the minimum and maximum value to draw the curly brace (start, end),
            and the axis to draw on, calculate proper resolution (# points) to evaluate
            the curly brace points at for drawing
        """
        if draw_on == "x":
            ax_min, ax_max = ax.get_xlim()
        else:
            ax_min, ax_max = ax.get_ylim()
        return (
            int(abs(max_v - min_v) / abs(ax_max - ax_min) * 100) * 2 + 1
        )  # two sides of the brace + center point

    def calculate_curl_radius(draw_on, scale=0.05):
        """Based on the range of the other axis (not the one to draw on) induce the brace curl/radius"""
        if draw_on == "x":
            ax_min, ax_max = ax.get_ylim()
        else:
            ax_min, ax_max = ax.get_xlim()
        return scale * (ax_max - ax_min)

    def calculate_curl_points(
        min_v, max_v, draw_on, brace_scale, curl_scale, padding, location
    ):
        """
        Given the minimum and maximum value to draw the curly brace (start, end),
            and the axis to draw on, calculate proper resolution (# points) to evaluate
            the curly brace points at for drawing
        """
        resolution = calculate_resolution(min_v=min_v, max_v=max_v, draw_on=draw_on)

        # Assume curl is centered at (0, 0), evaluate points, then shift
        curl_size = max_v - min_v
        if curl_size <= 0:
            raise ValueError("Curl size is not positive; invalid 'min_v', max_v!")

        beta = calculate_curl_radius(draw_on, scale=curl_scale)
        if draw_on == "x":
            x_vals = np.linspace(-curl_size / 2.0, curl_size / 2.0, resolution)
            x_vals_ps = x_vals[
                : int(resolution / 2) + 1
            ]  # positive-side of the brace (discarding center point)
            y_vals_ps = 1.0 / (
                1.0 + np.exp(-beta * (x_vals_ps - x_vals_ps[0]))
            ) + 1.0 / (1.0 + np.exp(-beta * (x_vals_ps - x_vals_ps[-1])))
            y_vals = np.concatenate([y_vals_ps, y_vals_ps[-2::-1]])

            # Adjust the tip of the brace to be at the center (0, 0)
            min_y, max_y = y_vals.min(), y_vals.max()
            y_vals /= max_y - min_y
            y_vals -= max_y
            y_vals *= -brace_scale * (ax.get_ylim()[1] - ax.get_ylim()[0])

            if location == "low":
                # Shift the brace (center point) to the passed padding value
                y_vals = y_vals + ax.get_ylim()[0] - padding
            else:
                # Flip
                y_vals *= -1

                # Shift the brace (center point) to the passed padding value
                y_vals = y_vals + ax.get_ylim()[1] + padding

            x_vals += (max_v + min_v) / 2.0

        else:
            y_vals = np.linspace(-curl_size / 2.0, curl_size / 2.0, resolution)
            y_vals_ps = y_vals[
                : int(resolution / 2) + 1
            ]  # positive-side of the brace (discarding center point)
            x_vals_ps = 1.0 / (
                1.0 + np.exp(-beta * (y_vals_ps - y_vals_ps[0]))
            ) + 1.0 / (1.0 + np.exp(-beta * (y_vals_ps - y_vals_ps[-1])))
            x_vals = np.concatenate([x_vals_ps, x_vals_ps[-2::-1]])

            # Adjust the tip of the brace to be at the center (0, 0)
            min_x, max_x = x_vals.min(), x_vals.max()
            x_vals /= max_x - min_x
            x_vals -= max_x
            x_vals *= -brace_scale * (ax.get_xlim()[1] - ax.get_xlim()[0])

            if location == "low":
                # Shift the brace (center point) to the passed padding value
                x_vals = x_vals + ax.get_xlim()[0] - padding
            else:
                # Flip
                x_vals *= -1

                # Shift the brace (center point) to the passed padding value
                x_vals = x_vals + ax.get_xlim()[1] + padding

            # Shift the brace (center point) to the passed padding value
            y_vals += (max_v + min_v) / 2.0

        return x_vals, y_vals

    def calculate_text_position(
        text, text_position, draw_on, x_vals, y_vals, location, text_padding_scale=0.1
    ):
        """
        Based on the draw_on axis, and the passed position, evalute proper text_position coordinates
        """
        if text_position is not None:
            pass
        elif draw_on == "x":
            x_pos = np.mean(x_vals)
            if location == "low":
                y_pos = np.min(y_vals) - text_padding_scale * (
                    np.max(y_vals) - np.min(y_vals)
                )
            else:
                y_pos = np.max(y_vals) + text_padding_scale * (
                    np.max(y_vals) - np.min(y_vals)
                )

        else:
            y_pos = np.mean(y_vals)
            if location == "low":
                x_pos = np.min(x_vals) - text_padding_scale * (
                    np.max(x_vals) - np.min(x_vals)
                )
            else:
                x_pos = np.max(x_vals) + text_padding_scale * (
                    np.max(x_vals) - np.min(x_vals)
                )

        return x_pos, y_pos

    def calculate_text_rotation(text_rotation, draw_on):
        """
        Based on the draw_on axis, and the passed rotation, evalute proper text_rotation angle
        """
        if text_rotation is not None:
            pass
        elif draw_on == "x":
            text_rotation = 0
        else:
            text_rotation = 90
        return text_rotation

    # Validate input
    draw_on = draw_on.lower().strip()
    if draw_on not in ["x", "y"]:
        raise ValueError(
            f"draw_on parameter received value '{draw_on}' which is unacceptable\n"
            f"Either pass 'x' to draw on x-axis or 'y' to draw on y-axis"
        )

    location = location.lower().strip()
    if location not in ["low", "high"]:
        raise ValueError(
            f"location parameter received value '{lcoation}' which is unacceptable\n"
            f"Either pass 'low' or 'high'"
        )

    padding = calculate_padding(draw_on, padding, padding_scale)

    # Get current axis limits to enforce after plotting
    xlim, ylim = ax.get_xlim(), ax.get_ylim()

    # Start Plotting
    x_vals, y_vals = calculate_curl_points(
        min_v=span[0],
        max_v=span[1],
        draw_on=draw_on,
        brace_scale=brace_scale,
        curl_scale=curl_scale,
        padding=padding,
        location=location,
    )
    ax.plot(
        x_vals,
        y_vals,
        color=color,
        lw=linewidth,
        clip_on=False,
    )

    # Add the text if passed
    if text is not None:
        text_position = calculate_text_position(
            text=text,
            text_position=text_position,
            draw_on=draw_on,
            x_vals=x_vals,
            y_vals=y_vals,
            location=location,
            text_padding_scale=text_padding_scale,
        )
        ax.text(
            text_position[0],
            text_position[1],
            text,
            ha="center",
            va="center",  # horisontal (ha) & vertical (va) alignment
            color=text_color,
            rotation=calculate_text_rotation(text_rotation, draw_on),
        )

    # Reset figure limits
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)


def plot_sampling_results(
    sample,
    log_density=None,
    #
    map_estimate=None,
    xlim=None,
    ylim=None,
    title=None,
    grid_size=200,
    labels=None,
    linewidth=0.5,
    markersize=2,
    fontsize=18,
    fontweight="bold",
    keep_plots=False,
    output_dir=None,
    filename_prefix="Sampling_Results",
    verbose=False,
):
    """
    Visualize a sample collected from a specific distribution with the passed log_density function

    :param list sample: a list of sample points
    :param callable log_density: the logarithm of the density function where sample is collected from
    :param map_estimate: a sample point that estimates the maximum aposteriori (MA) point
    :param xlim: x-limit of plots (only used for 2D case)
    :param ylim: y-limit of plots (only used for 2D case)
    :param title: title to be added to created plot(s)
    :param grid_size: number of discretization grid points to create 2D mesh for plots (only used for 2D case)
    :param labels: iterable of labels to be used for the variables on plots (X_i auto used if not passed)
    :param linewidth: width of lines (e.g., for contour plots)
    :param markersize: markers in scatter plots
    :param fontsize: general font size on all plots
    :param fontweight: font weighti
    :param keep_plots: if `False` all plots will be closed by calling :py:meth:`matplotlib.pyplot.close('all')`
    :param output_dir: location to save all generated files. If `None`, the current working directory is used.
    :param filename_prefix: Prefix for file naming
    :param bool verbose: screen verbosity

    :remarks: Set `keep_plots` to `True`, for example, if you are doing interactive plotting or
        using a notebook to show plots.
    """
    plots_enhancer(
        fontsize=fontsize,
        fontweight=fontweight,
        usetex=True,
    )
    if output_dir is None:
        output_dir = os.getcwd()
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # Shape the sample
    sample = np.vstack(sample)
    sample_size, size = sample.shape

    # PDF value evaluator
    if log_density is None:
        evaluate_pdf = None
    else:
        evaluate_pdf = lambda x: np.exp(log_density(x))

    # Plots for any dimension
    if labels is None:
        labels = [f"X{i+1}" for i in range(size)]
    columns = [l for l in labels]

    if verbose:
        print(f"***Creating Pair Plots...")

    # Create dataframe object and plot
    if sns is None:
        print("seaborn is not installed; pair plots are not generated")
    elif pd is None:
        print("pandas is not installed; pair plots are not generated")
    else:
        df = pd.DataFrame(
            sample,
            columns=columns,
        )
        g = sns.pairplot(
            df,
            diag_kind="kde",
            corner=True,
        )
        g.map_lower(sns.kdeplot, linewidths=linewidth, color=".4")
        fig1 = g.figure
        if title is not None:
            fig1.suptitle(title + " PairPlot")

        # save to file
        filename = os.path.join(output_dir, f"{filename_prefix}_PairPlot.pdf")
        g.savefig(
            filename, dpi=900, bbox_inches="tight", facecolor="white", format="pdf"
        )
        if verbose:
            print(f"Saved plot to {filename}")
        if not keep_plots:
            plt.close(fig1)

    # Additional (special) plots for 2D case
    if size == 2:
        if verbose:
            print("\n Constructing 2D plot information... ")

        # Mesh the domain and evalaute PDF everywhere
        if xlim is not None:
            x_min, x_max = xlim
        else:
            x_min = np.min(sample, axis=0)[0]
            x_max = np.max(sample, axis=0)[0]
            offset = abs(x_max - x_min) * 0.1
            x_min -= offset
            x_max += offset

        if ylim is not None:
            y_min, y_max = ylim
        else:
            y_min = np.min(sample, axis=0)[1]
            y_max = np.max(sample, axis=0)[1]
            offset = abs(y_max - y_min) * 0.1
            y_min -= offset
            y_max += offset

        x_size = y_size = grid_size
        x = np.linspace(x_min, x_max, x_size)
        y = np.linspace(y_min, y_max, y_size)
        x, y = np.meshgrid(x, y)

        # Evaluate PDF on the discretized grid
        posterior_pdf_vals = np.empty((x_size, y_size))
        posterior_pdf_vals[...] = np.nan
        for i in range(x_size):
            for j in range(y_size):
                state_tmp = np.array([x[i][j], y[i][j]])
                posterior_pdf_vals[i, j] = evaluate_pdf(state_tmp)

        z_min = np.max(posterior_pdf_vals) - 0.01
        z_max = np.max(posterior_pdf_vals) + 0.03

        #
        # Plot prior, likelihood, posterior, and histogram
        fig2 = plt.figure(figsize=(14, 5), facecolor="white")

        # plot contuour of the posterior and scatter of the ensemble
        ax1 = fig2.add_subplot(1, 2, 1)
        ax1.set_xlabel(r"$%s$" % labels[0], fontsize=fontsize)
        ax1.set_ylabel(r"$%s$" % labels[1], fontsize=fontsize)
        ax1.set_xlim(x_min, x_max)
        ax1.set_ylim(y_min, y_max)
        CS1 = ax1.contour(x, y, posterior_pdf_vals)
        ax1.scatter(sample[:, 0], sample[:, 1], alpha=0.75, s=15)

        # Add map estimate if passed:
        if map_estimate is not None:
            ax1.scatter(
                map_estimate[0],
                map_estimate[1],
                marker="^",
                alpha=0.65,
                s=45,
                label="MAP",
                zorder=10,
            )
        ax1.set_aspect("auto")

        #
        # Plot autocorrelation:
        ax2 = fig2.add_subplot(1, 2, 2)
        if pd is None:
            print("pandas is not installed; autocorrelation plot is not generated")
        else:
            pd.plotting.autocorrelation_plot(posterior_pdf_vals, ax=ax2)
            ax2.set_aspect("auto")

        fig2.subplots_adjust(wspace=0.35)

        if title is not None:
            fig2.suptitle(title + " Diagnostics")
        #
        filename = os.path.join(output_dir, f"{filename_prefix}_diagnostics.pdf")
        fig2.savefig(
            filename, dpi=900, bbox_inches="tight", facecolor="white", format="pdf"
        )
        if verbose:
            print(f"Saved plot to {filename}")
        if not keep_plots:
            plt.close(fig2)


def plot_sensors(
    sensors_coordinates,
    ax=None,
    clean_xy_ticks=False,
    xlim=None,
    ylim=None,
    zlim=None,
    title=None,
    legend_title=None,
    fontsize=16,
    show_grid=True,
    grid_linewidth=0.01,
    grid_linecolor="#000C67",
    grid_alpha=0.5,
    active_sensors_coordinates=None,
    annotate_sensors=False,
    show_axis_frame=True,
    show_legend=True,
    save_to_file=None,
    keep_plots=True,
    verbose=False,
):
    """
    Plot the domain, with optional buildings, and optional sensor locations
    and prediction coordinates (for goal-oriented work)

    :param sensors_coordinates: list of 2-len entries [(x, y)] of sensor
        locations.
    :param ax: either `None` or a matplotlib axis to plot on. If `None`, a new figure is created.
    :param bool clean xy_ticks: whether to show the x and y ticks. Default
        is `True`
    :param xlim: tuple of (xmin, xmax) to set the x-axis limits. Default is
        `None`
    :param ylim: tuple of (ymin, ymax) to set the y-axis limits. Default is
        `None`
    :param zlim: tuple of (zmin, zmax) to set the z-axis limits.
    :param title: string to set the title of the plot. Default is `None`
    :param bool show_grid: whether to show the mesh. Default is `True`
    :param float grid_linewidth: linewidth of the mesh. Default is `0.4`
    :param str grid_linecolor: color of the mesh. Default is `#000C67`
    :param float grid_alpha: alpha value of the mesh. Default is `0.5`
    :param int fontsize: fontsize of the plot. Default is `16`
    :param active_sensors_coordinates: a list of 2-len entries [(x, y)] of
        active sensor locations to highlight.
    :param bool annotate_sensors: whether to annotate the sensors. Default
        is None.
    :param bool show_axis_frame: whether to show the axis frame. Default is
        `False`
    :param bool show_legend: Default `True`
    :param save_to_file: string of the path to save the plot to. Default is `None`
    :param keep_plots: if `False` all plots will be closed by calling :py:meth:`matplotlib.pyplot.close('all')`
    :param bool verbose: screen verbosity

    :returns: `ax`, the axis on which the plot is created. This is set to `None` if `keep_plots` is set to `False`
    """

    if save_to_file is None and not keep_plots:
        raise ValueError(
            f"{keep_plots=} & {save_to_file=}\n"
            f"With these settings, any plot will be lost!"
        )

    # Sensor coordinates --> Dimension
    sensors_coordinates = np.asarray(sensors_coordinates).squeeze()
    ndim = sensors_coordinates.ndim
    if ndim <= 1:
        if ndim == 0:
            sensors_coordinates = sensors_coordinates.flatten()

        # convert to 2D with 0 y-values
        sensors_coordinates = np.array([[v, 0] for v in sensors_coordinates])
        pass
    elif ndim == 2:
        pass
    elif ndim == 3:
        raise NotImplementedError("3D Sensor plotter is not yet implemented")
    else:
        raise TypeError(
            f"Invalid sensors coordinates {sensors_coordinates=}\n"
            f"with {sensors_coordinates.ndim=}"
        )

    if active_sensors_coordinates is not None:

        active_sensors_coordinates = np.asarray(active_sensors_coordinates).squeeze()
        ndim = active_sensors_coordinates.ndim
        if ndim <= 1:
            if ndim == 0:
                active_sensors_coordinates = active_sensors_coordinates.flatten()
            # convert to 2D with 0 y-values
            active_sensors_coordinates = np.array(
                [[v, 0] for v in active_sensors_coordinates]
            )
            if legend_title is None and title is not None:
                legend_title = title
                title = None
        elif ndim == 2:
            pass
        elif ndim == 3:
            raise NotImplementedError("3D Sensor plotter is not yet implemented")
        else:
            raise TypeError(
                f"Invalid active sensors coordinates {active_sensors_coordinates=}\n"
                f"with {active_sensors_coordinates.ndim=}"
            )

    # Create (or get a handle of the passed) figure, plot, and return axis handle
    if ax is None:
        fig = plt.figure(facecolor="white", figsize=(10, 10))
        ax = fig.add_subplot(111)
    else:
        if not isinstance(ax, plt.Axes):
            raise TypeError(
                f"The passed axis {ax=} of {type(ax)=} which is not supported here!/\n"
                f"Only matplotlib.axes instance is used here!"
            )

        # Good to go;
        fig = ax.get_figure()

    if show_grid:
        show_axis_grids(
            ax=ax,
            show_major=True,
            show_minor=True,
            major_color=grid_linecolor,
            minor_color=grid_linecolor,
            major_alpha=grid_alpha,
            minor_alpha=0.50 * grid_alpha,
        )

    lbl_ind = 0
    label = "All Sensors"
    for coord in sensors_coordinates:
        ax = add_circle_to_axis(
            ax,
            origin=coord,
            radius=15,
            facecolor="#86BE3C",
            edgecolor="#86BE3C",
            label=label,
            fill=True,
            alpha=0.75,
            zorder=2,
        )
        if label is not None:
            label = None
        if annotate_sensors:
            lbl_ind += 1
            ax.annotate(str(lbl_ind), coord, fontsize=14)

    label = "Active Sensors"
    if active_sensors_coordinates is not None:
        for coord in active_sensors_coordinates:
            ax = add_circle_to_axis(
                ax,
                origin=coord,
                radius=20,
                facecolor=None,
                edgecolor="red",
                label=label,
                fill=False,
                alpha=0.55,
                zorder=-2,
            )
            if label is not None:
                label = None

    # set plot ticks and limits
    if clean_xy_ticks:
        ax.set_xticks([])
        ax.set_yticks([])

    # Adjust xylim
    offset_margin = 0.2
    if xlim is None:
        xlim = list(ax.get_xlim())
        width = max(1, xlim[1] - xlim[0])
        offset = offset_margin * width
        xlim[0] -= offset
        xlim[1] += offset

    if ylim is None:
        ylim = list(ax.get_ylim())
        width = max(1, ylim[1] - ylim[0])
        offset = offset_margin * width
        ylim[0] -= offset
        ylim[1] += offset

    if zlim is None:
        try:
            zlim = list(ax.get_zlim())
            width = max(1, zlim[-1] - zlim[0])
            offset = offset_margin * width
            zlim[0] -= offset
            zlim[-1] += offset
        except:
            zlim = None

    # set plot ticks and limits
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    if zlim is not None:
        ax.set_zlim(zlim)

    try:
        ax.set_aspect("equal")
    except NotImplementedError:
        pass

    if title is not None:
        fig.suptitle(title, fontsize=fontsize)

    if show_legend:
        ax.legend(
            title=legend_title,
            bbox_to_anchor=(0, 1.02, 1, 0.2),
            loc="lower left",
            mode="expand",
            borderaxespad=0,
            ncol=2,
        )
    ax.set_frame_on(show_axis_frame)

    if save_to_file is not None:
        save_to_file = os.path.abspath(save_to_file)
        dir_name = os.path.dirname(save_to_file)
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        fig.savefig(save_to_file, dpi=800, bbox_inches="tight", transparent=True)
        if verbose:
            print(f"Plot saved to: '{save_to_file}'")

    # Close the figure if not required to keep. Set ax to None if plot is closed
    if not keep_plots:
        plt.close(fig)
        ax = None

    if not verbose:
        print("Sensors plots created.")
    return ax


def add_circle_to_axis(
    ax,
    origin,
    radius,
    facecolor="#FF0000",
    edgecolor="#000000",
    fill=True,
    label=None,
    alpha=1,
    circle_hatch=None,
    zorder=1,
):
    """
    Add a circle with the passed specificication to axis.

    :param matplotlib.pyplot.Axes ax: the axis whose dimensions are to be checked.
    :param origin: a tuble containing the three coordinates of the origin (x0, y0, ...)
    :param radius:
    :param facecolor:
    :param edgecolor:
    :param fill:
    :param label:
    :param alpha:
    :param circle_hatch:
    :param zorder:

    :returns: the passed axis `ax`

    :raises: :py:class:`TypeError` if the pssed axis `ax` is not an instance
        of :py:class:`matplotlib.pyplot.Axes`.
    :raises: :py:class:`TypeError` if the `origin` does not match the dimensions/projection
        of the plot. `origin` is expected to be a iterable (e.g., tuble of length 2 if
        projection is 2d, and a tuple of length 3 for 3d projection. Some flexibility is
        allowed here; see the note below.

    :note:
        We allow some flexibility in the `origin` where origin of length 2 can be passed
        for 3d plot. In this case, we set the third entry to 1.
    """
    if not isinstance(ax, plt.Axes):
        raise TypeError(
            f"The passed axis {ax=} of {type(ax)=} which is not supported here!/\n"
            f"Only matplotlib.Axes instance is used here!"
        )

    projection = axis_cartesian_dimensions(ax)
    if projection == 2:
        if len(origin) == 2:
            x0, y0 = origin
        elif len(origin) == 3:
            x0, y0, _ = origin
        else:
            raise TypeError(
                f"wrong origin size; {origin=}; expected iterable of size 2 "
            )

        # NOTE: patches.Circle does not allow flexible control of labels; thus, I am using ax.plot instead.
        ax.plot(
            [x0],
            [y0],
            marker="o",
            color="w",
            label=label,
            markerfacecolor=facecolor if fill else "None",
            markeredgecolor=edgecolor,
            markersize=radius,
        )

    elif projection == 3:
        # Plot 2d circle in a 3d plot
        if len(origin) == 2:
            x_center, y_center = origin
            elevation = 0

        elif projection == 3:
            x_center, y_center, elevation = origin

        else:
            raise TypeError(
                f"wrong origin size; {origin=}; expected iterable of size 2 "
            )

        ax = add_3D_cylinder_to_axis(
            ax=ax,
            x_center=x_center,
            y_center=y_center,
            radius=radius,
            elevation=elevation,
            height=0,
            color=facecolor,
            alpha=alpha,
            lable=label,
            zorder=zorder,
        )

    else:
        raise TypeError(
            f"Projection must be either 2 or 3. "
            f"axis_cartesian_dimensions returned {projection=} which is unexpected!"
        )

    return ax


def axis_cartesian_dimensions(ax):
    """
    Given an axis (instance of ``matplotlib.pyplot.Axes``), check the number of dimensions,
    assuming a cartesian grid. This checks if the axis has `get_zlim` attribute or not,
    since there is no built-in function to do that.

    :param matplotlib.pyplot.Axes ax: the axis whose dimensions are to be checked.

    :returns: an integer set to either 2 (if 2d projection), or 3 (if 3d projection) as
        the number of cartesian dimensions of the axis

    :raises: :py:class:`TypeError` if the pssed axis `ax` is not an instance
        of :py:class:`matplotlib.pyplot.Axes`.
    """
    if not isinstance(ax, plt.Axes):
        raise TypeError(
            f"The passed axis {ax=} of {type(ax)=} which is not supported here!/\n"
            f"Only matplotlib.Axes instance is used here!"
        )

    if hasattr(ax, "get_zlim"):
        return 3
    else:
        return 2


def add_box_to_axis(
    ax,
    origin,
    width,
    depth,
    height=0,  # only if 3d projection is requested
    facecolor="#111111",  # '#4B8BBE'
    edgecolor="#000000",
    fill=True,
    label=None,
    box_hatch="x",
    box_text=None,
    round_corners=False,
    alpha=1,
    zorder=1,
):
    """
    construct the 4 faces of a box-base given it's origin and
    width (across x axis), depth (across y axix).
    The constructed rectangle is to be added the axis of the passed figure handle

    :param matplotlib.pyplot.Axes ax: the axis whose dimensions are to be checked.
    :param origin: a tuble containing the three coordinates of the origin (x0, y0, ...)
    :parm width:
    :param depth:
    :param height:
    :param facecolor:
    :param edgecolor:
    :param fill:
    :param label:
    :param box_hatch:
    :param box_text:
    :param round_corners:
    :param alpha:
    :param zorder:

    :returns: the passed axis `ax`

    :raises: :py:class:`TypeError` if the pssed axis `ax` is not an instance
        of :py:class:`matplotlib.pyplot.Axes`.
    :raises: :py:class:`TypeError` if the `origin` does not match the dimensions/projection
        of the plot. `origin` is expected to be a iterable (e.g., tuble of length 2 if
        projection is 2d, and a tuple of length 3 for 3d projection. Some flexibility is
        allowed here; see the note below.

    :note:
        We allow some flexibility in the `origin` where origin of length 2 can be passed
        for 3d plot. In this case, we set the third entry to 1.
    """
    if not isinstance(ax, plt.Axes):
        raise TypeError(
            f"The passed axis {ax=} of {type(ax)=} which is not supported here!/\n"
            f"Only matplotlib.Axes instance is used here!"
        )

    projection = axis_cartesian_dimensions(ax)
    if projection == 2:
        if len(origin) == 2:
            x0, y0 = origin
        elif len(origin) == 3:
            x0, y0, _ = origin
        else:
            raise TypeError(
                f"wrong origin size; {origin=}; expected iterable of size 2 or 3 "
            )

        if round_corners:
            rectangle_func = patches.FancyBboxPatch
        else:
            rectangle_func = patches.Rectangle
        rect = rectangle_func(
            (x0, y0),
            width,
            depth,
            facecolor=facecolor,
            edgecolor=edgecolor,
            alpha=alpha,
            fill=fill,
            label=label,
            hatch=box_hatch,
        )
        ax.add_patch(rect)
        if box_text is not None:
            ax.text(
                x0 + 0.2 * width,
                y0 + 0.3 * depth,
                box_text,
                fontsize=24,
                color="white",
            )

    elif projection == 3:

        # Embed 2d point in 3d space
        if len(origin) == 2:
            origin = (origin[0], origin[1], 0.0)

        ax = add_cube_to_axis(
            ax,
            origin=origin,
            width=width,
            depth=depth,
            height=height,
            color=facecolor,
            alpha=alpha,
            plot_corners=False,
            zorder=zorder,
        )

    else:
        raise TypeError(
            f"Projection must be either 2 or 3. "
            f"axis_cartesian_dimensions returned {projection=} which is unexpected!"
        )

    return ax


def add_cube_to_axis(
    ax,
    origin,
    width,
    depth,
    height,
    color="blue",
    alpha=0.75,
    plot_corners=False,
    zorder=1,
):
    """
    construct the 8 faces of a cube given it's origin and
    width (across x axis), depth (across y axix), and height (across z axis).
    The constructed cube is to be added the axis of the passed figure handle

    :param matplotlib.pyplot.Axes ax: the axis whose dimensions are to be checked.
    :param origin: a tuble containing the three coordinates of the origin (x0, y0, ...)
    :parm width:
    :param depth:
    :param height:
    :param color:
    :param alpha:
    :param plot_corners:
    :param zorder:

    :returns: the passed axis `ax`

    :raises: :py:class:`TypeError` if the pssed axis `ax` is not an instance
        of :py:class:`matplotlib.pyplot.Axes`.
    :raises: :py:class:`TypeError` if the projection of the pssed axis `ax` is not '3d'.
    :raises: :py:class:`TypeError` if the `origin` does not match the dimensions/projection
        of the plot. `origin` is expected to be a iterable (e.g., tuble of length 2 if
        projection is 2d, and a tuple of length 3 for 3d projection. Some flexibility is
        allowed here; see the note below.

    :note:
        We allow some flexibility in the `origin` where origin of length 2 can be passed
        for 3d plot. In this case, we set the third entry to 1.
    """
    if not isinstance(ax, plt.Axes):
        raise TypeError(
            f"The passed axis {ax=} of {type(ax)=} which is not supported here!/\n"
            f"Only matplotlib.Axes instance is used here!"
        )

    if len(origin) == 3:
        x0, y0, z0 = origin
    else:
        raise TypeError(
            f"wrong origin size; {origin=}; expected iterable of size 2 or 3."
        )

    # construct meshgrids and plot the 6 faces, and the 8 corners:
    X = np.array([[x0, x0 + width], [x0, x0 + width]])
    Y = np.array([[y0, y0 + depth], [y0, y0 + depth]])
    Z = np.array([[z0, z0 + height], [z0, z0 + height]])

    ax.plot_surface(X, y0, Z.T, alpha=alpha, color=color)
    ax.plot_surface(X, y0 + depth, Z.T, alpha=alpha, color=color)

    ax.plot_surface(x0, Y, Z.T, alpha=alpha, color=color)
    ax.plot_surface(x0 + width, Y, Z.T, alpha=alpha, color=color)

    ax.plot_surface(X, Y.T, z0, alpha=alpha, color=color)
    ax.plot_surface(X, Y.T, z0 + height, alpha=alpha, color=color)

    if plot_corners:  # plot the 8 corners of the cube if requires
        cube_corners = np.array(
            [
                [x0, y0, z0],
                [x0, y0 + depth, z0],
                [x0 + width, y0, z0],
                [x0 + width, y0 + depth, z0],
                [x0, y0, z0 + height],
                [x0, y0 + depth, z0 + height],
                [x0 + width, y0, z0 + height],
                [x0 + width, y0 + depth, z0 + height],
            ]
        )
        ax.scatter3D(
            cube_corners[:, 0], cube_corners[:, 1], cube_corners[:, 2], zorder=zorder
        )

    return ax


def add_3D_cylinder_to_axis(
    ax,
    x_center,
    y_center,
    radius,
    height,
    elevation=0,
    resolution=50,
    color="r",
    alpha=0.75,
    rstride=20,
    cstride=10,
    lable=None,
    zorder=1,
):
    """
    Plot/Add 3D cylinder to the passed asix (ax)

    :parm ax: axis handle
    :parm x_center:
    :parm y_center:
    :parm radius:
    :parm height:
    :parm elevation:
    :parm resolution:
    :parm color:
    :parm rstride:
    :parm cstride:
    :parm label: string label to add on top of the cylinder

    :returns: the passed axis `ax`

    :raises: :py:class:`TypeError` if the pssed axis `ax` is not an instance
        of :py:class:`matplotlib.pyplot.Axes`.
    :raises: :py:class:`TypeError` if the projection of the pssed axis `ax` is not '3d'.
    """
    #
    x = np.linspace(x_center - radius, x_center + radius, resolution)
    z = np.linspace(elevation, elevation + height, resolution)
    X, Z = np.meshgrid(x, z)

    # Pythagorean theorem to get y coordinates
    y_diff = (radius**2) - np.square(X - x_center)
    y_diff[y_diff < 0] *= -1  # negative may happen for tiny values; cancellation error
    Y = np.sqrt(y_diff) + y_center

    ax.plot_surface(
        X, Y, Z, linewidth=0, color=color, alpha=alpha, rstride=rstride, cstride=cstride
    )
    ax.plot_surface(
        X,
        (2 * y_center - Y),
        Z,
        linewidth=0,
        color=color,
        alpha=alpha,
        rstride=rstride,
        cstride=cstride,
    )

    # Fill cylinder face, floor, and ceiling with given color specifications
    # Floor
    floor = Circle((x_center, y_center), radius, color=color, zorder=zorder)
    ax.add_patch(floor)
    art3d.pathpatch_2d_to_3d(floor, z=elevation, zdir="z")

    # Ceiling
    ceiling = Circle((x_center, y_center), radius, color=color, zorder=zorder)
    handle = ax.add_patch(ceiling)
    if lable is not None:
        ax.text(x_center, y_center, elevation + height, str(lable))

    # 2D --> 3D
    art3d.pathpatch_2d_to_3d(ceiling, z=elevation + height, zdir="z")

    return ax


def ridgeline(ax, data, overlap=0, fill=True, labels=None, n_points=150):
    """
    Creates a standard ridgeline plot.

    :param ax: matplotlib axis to plot on.
    :param data: iterable of iterables, this can be a list of lists, a list of arrays,
        or a 2D numpy array, etc.
    :param overlap: overlap between distributions. 1 max overlap, 0 no overlap.
        Default is 0.
    :param fill: matplotlib color to fill the distributions. Default is True.
    :param labels: list of values to place on the y axis to describe the
        distributions.
    :param n_points: number of points to evaluate each distribution function. Default is
        150.
    """
    if overlap > 1 or overlap < 0:
        raise ValueError("Overlap must be in [0,1]")
    concat_data = np.concatenate(data)
    xx = np.linspace(np.min(concat_data), np.max(concat_data), n_points)
    ys = []
    for i, d in enumerate(data):
        y = i * (1.0 - overlap)
        ys.append(y)
        curve = sp.stats.gaussian_kde(d)(xx)
        if fill:
            ax.fill_between(
                xx,
                np.ones(n_points) * y,
                curve + y,
                zorder=len(data) - i + 1,
                color=fill,
            )
        ax.plot(xx, curve + y, c="k", zorder=len(data) - i + 1)
    if labels:
        ax.set_yticks(ys, labels=labels)
    return ax
