# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides access to functions to perform various linear algebra, calculus etc. operations.
"""

import os
import sys
import re
import warnings
import itertools
from functools import reduce
from collections import abc
import decimal
import operator as op
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg


import matplotlib.pyplot as plt

from . import misc, stats, plot

__python_version = (
    sys.version_info.major,
    sys.version_info.minor,
    sys.version_info.micro,
)
if __python_version >= (3, 0, 0):
    raw_input = input
else:
    range = xrange

# define all functions here so it can be easily aggregated in __init__.py of utility package
__all__ = [
    "validate_inverse_problem_derivatives",
    "inverse_problem_linearity_test",
    "validate_function_gradient",
    "finite_differences_gradient",
    "asarray",
    "matrix_trace",
    "matrix_logdet",
    "matrix_diagonal",
    "matrix_vecmult",
    "factorize_spsd_matrix",
    "truncate",
    "remainder",
    "ncr",
    "enumerate_binary",
    "sample_binary_states",
    "index_to_binary_state",
    "index_from_binary_state",
    "threshold_relaxed_design",
    "group_by_num_active",
]


# Math & Linear Algebra:
# ----------------------
def validate_inverse_problem_derivatives(
    problem,
    eval_at,
    gradient=None,
    validate_gradient=True,
    validate_Hessian=True,
    data_misfit_only=False,
    method="fd",
    fd_eps=None,
    use_noise=True,
    create_plots=False,
    plots_format="pdf",
    random_seed=1011,
    output_dir=None,
):
    """
    Given a problem (filtering/smoothing instance), use finite differences to validate
    the gradient and/or the Hessian of the objective function associated with the
    passed problem; the `problem` must provide the following methods:

    a) :py:meth:`objective_function_value` to calculate the objective function value and the
       gradient at a given state/parameter;

    b) :py:meth:`objective_function_gradient` to calculate the first-order derivative (gradient)
       of the objective function at a given state/parameter `eval_at`;

    c) :py:meth:`Hessian_matvec` to evaluate the product of the second-order derivative
       (Hessian) of the objective function at a given state/parameter `eval_at`;

    :param problem: problem (filtering/smoothing)
    :param eval_at: state/parameter to evaluate derivatives at
    :param bool validate_gradient: apply validation for first order-derivative
    :param bool validate_Hessian: apply validation for first order-derivative
    :param bool data_misfit_only: ignore any prior term in the objective function if `True`
    :param str method: the method used to validate derivatives. Supported methods are

        * 'fd': standard (right-sided) finite-difference validation
        * 'fd-central': double-sided finite-difference validation

    :param fd_eps: epsilon used for finite difference validation; automatically chosen if `None`

        - if `use_noise` is `True`, a range from 1e-12 to 1 is sampled
        - if `use_noise` is `False`, 1e-7 is used

    :param bool use_noise: use white noise (multiplied by derivatives) to validate
        gradients and create log-log plots (if `create_plots` is `True`)
    :param output_dir: path to folder/location to save plots (if `create_plots ` is
        `True`)
    :param random_seed: random seed to feed random number generator for reproducibility

    :returns: relative errors w.r.t FD approximation; size of returned items vary based
        the used approach (use noise vs. not)
        - grad_err: relative error of the gradient comapred to finite differences
        - Hessian_err: relative error of Hessian matvec effect comapred to finite differences

    :raises:
        - `ValueError` is raised if both `validate_gradient` and `validate_Hessian` are set to `False`
        - `ValueError` is raised if `method` is not supported/recognized
        - `TypeError` is raised if the type of `fd_eps` is not supported
    """
    if not (validate_gradient or validate_Hessian):
        raise ValueError(
            f"At least one of validate_gradient or validate_Hessian must be True!"
        )

    if create_plots:
        plots_format = plots_format.strip(" .")
        # Check output directory (create if needed)
        if output_dir is None:
            output_dir = os.getcwd()
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)

    # Evaluate the gradient
    if gradient is None:
        gradient = problem.objective_function_gradient(
            eval_at, data_misfit_only=data_misfit_only
        )

    # Unify the finite-difference epsilon/step
    if fd_eps is None:
        if use_noise:
            fd_eps = np.logspace(-12, 0, min(gradient.size, 17))
        else:
            fd_eps = 1e-7
    if misc.isnumber(fd_eps):
        fd_eps = np.ones_like(gradient, dtype=float) * fd_eps
    elif misc.isiterable(fd_eps):
        fd_eps = np.asarray(fd_eps, dtype=float)
        if fd_eps.size > gradient.size:
            raise TypeError(
                f"fd_eps size exceeds gradient size!"
            )
    else:
        raise TypeError(
            f"Unrecognized fd_eps type {type(fd_eps)}"
        )

    # Gradient Validation
    if validate_gradient:
        x = np.asarray(eval_at)
        gradient = np.asarray(gradient)

        if use_noise:
            # Objective value at passed state/parameter
            f0 = problem.objective_function_value(x, data_misfit_only=data_misfit_only)

            # White-noise vector, and dot-product with the Hessian
            # TODO: Consider generating multiple noise realizations instead of only one!
            rng = np.random.default_rng(random_seed)
            noise = rng.standard_normal(x.size)
            gradient_dot_noise = noise @ gradient

            # gradient error (same size as fd_eps)
            grad_err = np.zeros_like(fd_eps)
            grad_err[:]

            # Calculate gradient error vs. FD gradient
            for i, eps in enumerate(fd_eps):
                # Value of the objective at the perturbed state/parameter x+eps
                prtrb = eps * noise
                f1 = problem.objective_function_value(
                    x + prtrb, data_misfit_only=data_misfit_only
                )
                fd_deriv = (f1 - f0) / eps
                grad_err[i] = abs((fd_deriv - gradient_dot_noise) / fd_deriv)

        else:
            # Standard/Central Finite-Difference validation of the gradient
            if re.match(r"\Afd", method, re.IGNORECASE):
                if re.match(r"central", method, re.IGNORECASE):
                    fd_central = True
                else:
                    fd_central = False
                fun = lambda x: problem.objective_function_value(
                    x, data_misfit_only=data_misfit_only
                )
                finite_differences_gradient, grad_err = validate_function_gradient(
                    fun, x, gradient, fd_central=fd_central, fd_eps=fd_eps[0]
                )  # TODO: fix

            elif re.match(
                r"\A(ad|automatic( |-|_)*differentiation)\Z", method, re.IGNORECASE
            ):
                raise NotImplementedError(
                    "Validation using Automatic Differentiation is not yet implemented"
                )

            else:
                raise ValueError(
                    f"Unrecognized validation method {str(method)}"
                )

        if create_plots:
            # log-log plot
            plot.plots_enhancer(fontsize=18, usetex=True)
            fig = plt.figure(facecolor="white")
            ax = fig.add_subplot(111)
            plot.show_axis_grids(ax)
            lw = 3
            if use_noise:
                ax.loglog(fd_eps, grad_err, "-ob", lw=lw, label=r"Noisy FD Error")
                ax.loglog(
                    fd_eps,
                    fd_eps * (grad_err[0] / fd_eps[0]),
                    "-g",
                    lw=lw,
                    label=r"Reference",
                )
                ax.set_xlabel(r"$\epsilon$")
            else:
                ax.semilogy(
                    np.arange(grad_err.size), grad_err, "-ob", lw=lw, label=r"FD Error"
                )
                ax.set_xlabel(r"Gradient Entries")

            ax.legend(loc="best")
            ax.set_ylabel(r"Relative Absolute Error")

            # save plot to fil
            save_to = os.path.join(
                output_dir, "validate_function_gradient.{0}".format(plots_format)
            )
            fig.savefig(save_to, dpi=600, bbox_inches="tight")
            print("FD Gradient check plot saved to: {0}".format(save_to))
            plt.close(fig)

    else:
        grad_err = None

    # Hessian Validation
    if validate_Hessian:
        x = np.asarray(eval_at)

        if not use_noise:
            raise NotImplementedError(
                "Hessian validation without noise is not yet implemented"
            )

        else:
            # White-noise vector, and dot-product with the Hessian
            Hessian_err = np.empty_like(fd_eps)
            Hessian_err[:] = np.nan

            # TODO: Consider generating multiple noise realizations instead of only one!
            rng = np.random.default_rng(random_seed)
            noise = rng.standard_normal(x.size)

            H_noise = problem.Hessian_matvec(
                noise, eval_at=x, data_misfit_only=data_misfit_only
            )

            # Calculate gradient error vs. FD gradient
            for i, eps in enumerate(fd_eps):
                prtrb = eps * noise

                # Value of the gradient at the perturbed state/parameter x+eps
                g0 = problem.objective_function_gradient(
                    x - prtrb, data_misfit_only=data_misfit_only
                )
                g1 = problem.objective_function_gradient(
                    x + prtrb, data_misfit_only=data_misfit_only
                )

                Hess_i_fd = (g1 - g0) / (2 * eps)
                rel_err = np.abs((Hess_i_fd - H_noise) / Hess_i_fd)
                # print("RelError: ", Hess_i_fd, H_noise, rel_err)
                Hessian_err[i] = np.linalg.norm(rel_err, ord=np.inf)

            if create_plots:
                plot.plots_enhancer(fontsize=18, usetex=True)
                fig = plt.figure(facecolor="white")
                ax = fig.add_subplot(111)
                plot.show_axis_grids(ax)
                lw = 3
                ax.loglog(fd_eps, Hessian_err, "-ob", lw=lw, label=r"Noisy FD Error")
                ax.loglog(
                    fd_eps,
                    fd_eps * (Hessian_err[0] / fd_eps[0]),
                    "-g",
                    lw=lw,
                    label=r"Reference",
                )

                ax.set_xlabel(r"$\epsilon$")
                ax.set_ylabel(r"Relative Absolute Error")
                ax.legend(loc="best")

                save_to = os.path.join(
                    output_dir, "Hessian_Validation.{0}".format(plots_format)
                )
                fig.savefig(save_to, dpi=600, bbox_inches="tight")
                print("FD Hessian check plot saved to: {0}".format(save_to))
                plt.close(fig)
    else:
        Hessian_err = None

    return grad_err, Hessian_err


def inverse_problem_linearity_test(inverse_problem):
    """
    Check if the passed inverse problem is linear not by testing the forward model
    and the observation operator.
    The inverse problem is tested for linearity by testing linearity of:

      1. The observation operator: each observation operator must provide
         :py:meth:`Jacobian_T_matvec` method which requires valid (not `None`) value
         of `eval_at` argument. If it is possible to evaluate this method without
         `eval_at` then the observation operator is linear, otherwise it throws
         a `ValueError` which indicates the operator is nonlinear.
      2. The simulation model: similar to the observation operator, but the model
         linearity is test through one of :py:meth:`Jacobian_T_matvec` or
         :py:meth:`solve_adjoint`
         based one which one is available.

    If none of the methods above is available where expected, this method
    throws a :py:class:`TypeError`

    :returns: a flag indicating whether the underlying inverse/OED problem is linear.

    :raises: :py:class:`TypeError` if the methods to be tested are not available
    """
    # Try the Jacobians of the observation operator and the model with no `eval_at`
    model = inverse_problem.model
    obs_oper = inverse_problem.observation_operator

    ## 1- Observation operator
    if hasattr(obs_oper, "Jacobian_T_matvec"):
        obs = obs_oper.observation_vector(init_val=0.5)
        try:
            _ = obs_oper.Jacobian_T_matvec(obs, eval_at=None)
        except ValueError:
            return False
    else:
        raise TypeError(
            "Unable to determine whether the problem is linear or not! \n"
            "The observation operator does not provide `Jacobian_T_matvec`"
            " method!"
        )

    ## 2- Simulation Model
    x = model.state_vector()
    if hasattr(model, "Jacobian_T_matvec"):
        test = model.Jacobian_T_matvec
    elif hasattr(model, "solve_adjoint"):
        test = model.solve_adjoint
    else:
        raise TypeError(
            "Unable to determine whether the problem is linear or not\n"
            " The simulation model does not provide `Jacobian_T_matvec` "
            "nor `solve_adjoint` methods!"
        )

    try:
        _ = test(x, eval_at=None)
    except ValueError:
        return False

    # If everything passed, the Jacobians/Adjoints were solved properly without `eval_at`
    return True


def validate_function_gradient(
    fun,
    state,
    gradient,
    fd_eps=1e-8,
    bounds=None,
    verbose=False,
    fd_central=False,
    fd_complex=False,
):
    """
    Validate the `gradient` of an objective function `fun` using finite differences.
    Complex finite differences are supported, and utilized if `fd_complex` is
    True

    :param Callable fun: The objective function to validate its gradient
    :param state: The state/parameter at which to validate the gradient
    :param gradient: The gradient to validate
    :param float fd_eps: The finite difference perturbation. Default is 1e-8.
    :param bounds: either None, or a list of 2-sized iterables on the form
        `[(lb, ub), (lb, ub), ....]` defining lower bounds `lb` and upper
        bounds `ub` of each entry of hte state. The length of `bounds` must
        be equal to the size of state.
    :param bool verbose: If True, print validation statistics.
    :param bool fd_central: If True, use central finite differences. Default is
        False, and therefore standard (right-sided) finite differences are used.
    :param bool fd_complex: If True, use complex finite differences. Default is
        False.

    .. Warning:: Complex finite differences are not yet implemented!

    :raises TypeError: If `fun` is not callable.
    :raises AssertionError: If `state` and `gradient` are not of equal sizes.
    :raises AssertionError: If `fd_eps` is not a real number.
    :raises AssertionError: If `fd_eps` is zero.
    :raises NotImplementedError: If `fd_complex` is True, as complex finite
        differences are not implemented yet.
    :raises ValueError: If the finite difference throws an exception for some
        reason.

    :returns: A tuple containing a numpy array of the finite difference gradient
        of `fun` at `state` and its corresponding gradient error.

    :rtype: tuple[np.ndarray, np.ndarray]
    """
    if not callable(fun):
        raise TypeError(
            f"The passed function `fun` is not callable! " f"Received {type(fun)=}"
        )

    # Use finite differences to validate the Gradient (if needed):
    # Finite Difference Validation:
    np_state = np.asarray(state)
    np_gradient = np.asarray(gradient)

    assert (
        np_state.size == np_gradient.size
    ), "state and gradient must be of equal sizes!"
    assert misc.isnumber(fd_eps), "fd_eps must be a real number"
    assert fd_eps != 0, "fd_eps can't be zero!"

    try:
        # Two copies of objective value
        f0 = _f0 = fun(np_state)
    except Exception as err:
        raise ValueError(
            f"The passed function `fun` throws an exception when passed an numpy "
            f"representation of state! 'fun' must accept numpy arrays. "
            f"Trace the error"
            f" below\nUnexpected {err=}, of {type(err)=}"
        )

    # Validate bounds
    if bounds is not None:
        bounds = asarray(bounds)
        if np.ndim(bounds) != 2:
            raise TypeError(
                f"Expected bounds to be iterable/list of [(lb, ub), ...]. "
                f"Received {bounds=} of {bounds.shape=}"
            )
        if bounds.shape != (np_state.size, 2):
            raise TypeError(
                f"The passed bounds do not have the right shape/size/format "
                f"The shape of bounds must be ({np_state.size}, 2), but received "
                f"bounds with {bounds.shape=}"
            )

    sep = "\n" + "~" * 80 + "\n"
    # print some statistics for monitoring:
    if verbose:
        if fd_central:
            print(sep + "Central FD Validation of the Gradient" + sep)
        else:
            print(sep + "Standard (right-sided) FD Validation of the Gradient" + sep)
        print(
            f"  + Maximum gradient entry : {np_gradient.max()}\n"
            f"  + Minimum gradient entry : {np_gradient.min()}"
        )

    # Finite difference Gradient
    fd_grad = np.zeros_like(np_gradient)
    grad_err = np.zeros_like(np_gradient)

    if fd_complex:
        raise NotImplementedError(
            "Complex-valued finite difference validation is not yet implemented"
        )

    else:
        # Allocate memory for purterbabtions and perturbed states
        prtrb = np.zeros_like(np_state).astype(float)

        for i in range(np_state.size):
            prtrb[:] = 0.0
            prtrb[i] = fd_eps

            # Check bounds
            if bounds is not None:
                lb, ub = bounds[i]
                s_i = np_state[i]

                # Create FD stencil
                a, b, c = s_i - fd_eps, s_i, s_i + fd_eps
                if lb <= a < b < c <= ub:
                    # all points in (lb, ub); cfd or rfd or lfd valid
                    f1 = fun(np_state + prtrb)
                    if fd_central:
                        f0 = fun(np_state - prtrb)
                        fd_grad[i] = (f1 - f0) / (2.0 * fd_eps)
                    else:
                        fd_grad[i] = (f1 - _f0) / (fd_eps)

                elif a < lb <= b < c <= ub:
                    # Only RFD (f(c)-f(b))/eps
                    if fd_central:
                        warnings.warn(
                            f"Central FD is not possible due to bounds violation "
                            f"at entry {i=}. Switching temporarily to RFD"
                        )
                    f1 = fun(np_state + prtrb)
                    fd_grad[i] = (f1 - _f0) / (fd_eps)

                elif lb <= a < b <= ub < c:
                    # Only RFD (f(c)-f(b))/eps
                    if fd_central:
                        warnings.warn(
                            f"Central FD is not possible due to bounds violation "
                            f"at entry {i=}. Switching temporarily to RFD"
                        )
                    f1 = fun(np_state - prtrb)
                    fd_grad[i] = (_f0 - f1) / (fd_eps)

                else:
                    raise ValueError(
                        f"Bounds violation at entry {i} of the state: \n"
                        f"Bounds (lower, upper): ({lb=}, {ub=}) \n"
                        f"FD-Stencil: {state[i]-fd_eps=}; {state[i]=}; "
                        f" {state[i]+fd_eps=}"
                    )

            else:

                f1 = fun(np_state + prtrb)
                if fd_central:
                    f0 = fun(np_state - prtrb)
                    fd_grad[i] = (f1 - f0) / (2.0 * fd_eps)
                else:
                    fd_grad[i] = (f1 - f0) / (fd_eps)

            # Relative error; don't scale if difference is zero
            err = abs((np_gradient[i] - fd_grad[i]))
            if np_gradient[i] == 0:
                warnings.warn(
                    f"True gradient at entry {i} is zero. "
                    f"Can't produce relative error. "
                    f"Keeping absolute error instead!"
                )
            else:
                err /= np_gradient[i]

            if verbose:
                print(
                    "%4d| Grad = %+8.6e\t FD-Grad = %+8.6e\t Rel-Err = %+8.6e"
                    % (i, np_gradient[i], fd_grad[i], err)
                )
            grad_err[i] = err

    return fd_grad, grad_err


def finite_differences_gradient(
    fun,
    state,
    fd_eps=1e-8,
    bounds=None,
    verbose=False,
    fd_central=False,
    fd_complex=False,
):
    """
    Evaluate the `gradient` of an objective function `fun` using
    finite differences.
    Complex finite differences are supported, and utilized if
    `fd_complex` is True

    :param Callable fun: The objective function to validate its gradient
    :param state: The state/parameter at which to validate the gradient
    :param float fd_eps: The finite difference perturbation. Default is 1e-8.
    :param bounds: either None, or a list of 2-sized iterables on the form
        `[(lb, ub), (lb, ub), ....]` defining lower bounds `lb` and upper
        bounds `ub` of each entry of hte state. The length of `bounds` must
        be equal to the size of state.
    :param bool verbose: If True, print validation statistics.
    :param bool fd_central: If True, use central finite differences. Default is
        False, and therefore standard (right-sided) finite differences are used.
    :param bool fd_complex: If True, use complex finite differences. Default is
        False.

    .. Warning:: Complex finite differences are not yet implemented!

    :raises TypeError: If `fun` is not callable.
    :raises AssertionError: If `fd_eps` is not a real number.
    :raises AssertionError: If `fd_eps` is zero.
    :raises NotImplementedError: If `fd_complex` is True, as complex finite
        differences are not implemented yet.
    :raises TypeError: If bounds are passed (not `None`) but have wrong
        shape/format
    :raises ValueError: If the finite difference throws an exception for
        some reason.

    :return: A numpy array of the finite difference gradient of `fun` at `state`
    :rtype: np.ndarray
    """
    if not callable(fun):
        raise TypeError(f"The passed function `fun` is not callable!")

    assert misc.isnumber(fd_eps), "fd_eps must be a real number"
    assert fd_eps != 0, "fd_eps can't be zero!"

    # local copy of the state
    np_state = asarray(state)

    # Validate bounds
    if bounds is not None:
        bounds = asarray(bounds)
        if np.ndim(bounds) != 2:
            raise TypeError(
                f"Expected bounds to be iterable/list of [(lb, ub), ...]. "
                f"Received {bounds=} of {bounds.shape=}"
            )
        if bounds.shape != (np_state.size, 2):
            raise TypeError(
                f"The passed bounds do not have the right shape/size/format "
                f"The shape of bounds must be ({np_state.size}, 2), but received "
                f"bounds with {bounds.shape=}"
            )

    # Initiate the finite difference Gradient
    fd_grad = np.zeros_like(np_state)

    ## Finite Difference Validation

    # Function value at passed state
    try:
        # Two copies of objective value
        f0 = _f0 = fun(np_state)
    except(Exception) as err:
        raise ValueError(
            f"The passed function `fun` throws an exception when passed an numpy"
            f" representation of state! 'fun' must accept numpy arrays.\n"
            f"Unexpected {err=} of {type(err)=}"
        )

    if fd_complex:
        raise NotImplementedError(
            f"TODO: Complex-valued finite difference validation "
            f"is not yet implemented"
        )

    else:
        # Allocate memory for perturbations and perturbed states
        prtrb = np.zeros_like(np_state)

        for i in range(np_state.size):
            prtrb[:] = 0.0
            prtrb[i] = fd_eps

            # Check bounds
            if bounds is not None:
                lb, ub = bounds[i]
                s_i = np_state[i]

                # Create FD stencil
                a, b, c = s_i - fd_eps, s_i, s_i + fd_eps
                if lb <= a < b < c <= ub:
                    # all points in (lb, ub); cfd or rfd or lfd valid
                    f1 = fun(np_state + prtrb)
                    if fd_central:
                        f0 = fun(np_state - prtrb)
                        fd_grad[i] = (f1 - f0) / (2.0 * fd_eps)
                    else:
                        fd_grad[i] = (f1 - _f0) / (fd_eps)

                elif a < lb <= b < c <= ub:
                    # Only RFD (f(c)-f(b))/eps
                    if fd_central:
                        warnings.warn(
                            f"Central FD is not possible due to bounds violation "
                            f"at entry {i=}. Switching temporarily to RFD"
                        )
                    f1 = fun(np_state + prtrb)
                    fd_grad[i] = (f1 - _f0) / (fd_eps)

                elif lb <= a < b <= ub < c:
                    # Only RFD (f(c)-f(b))/eps
                    if fd_central:
                        warnings.warn(
                            f"Central FD is not possible due to bounds violation "
                            f"at entry {i=}. Switching temporarily to RFD"
                        )
                    f1 = fun(np_state - prtrb)
                    fd_grad[i] = (_f0 - f1) / (fd_eps)

                else:
                    raise ValueError(
                        f"Bounds violation at entry {i} of the state: \n"
                        f"Bounds (lower, upper): ({lb=}, {ub=}) \n"
                        f"FD-Stencil: {state[i]-fd_eps=}; {state[i]=}; "
                        f" {state[i]+fd_eps=}"
                    )

            else:

                f1 = fun(np_state + prtrb)
                if fd_central:
                    f0 = fun(np_state - prtrb)
                    fd_grad[i] = (f1 - f0) / (2.0 * fd_eps)
                else:
                    fd_grad[i] = (f1 - f0) / (fd_eps)

    return fd_grad


def asarray(
    A,
    shape=None,
    dtype=None,
):
    """
    Return a copy of the passed array-like object as a numpy array.  This requires the
    input to by scalar, numpy array, scipy sparse matrix, or a callable/linear operator.

    :param float | np.ndarray | sp.spmatrix | abc.Callable[np.ndarray] A: The
        array-like object to be converted to a numpy array.
    :param shape: The shape of the array to be returned. This is only used if A is a
        callable/linear operator.

    :raises TypeError: If `A` is not a scalar, numpy array, scipy sparse matrix, or a
        collable/linear operator.
    :raises ValueError: If `A` is a callable/linear operator, and `shape` is not given.

    :rtype: np.ndarray
    """
    if misc.isnumber(A):
        np_A = np.asarray(A, dtype=dtype).flatten()

    elif isinstance(A, np.ndarray):
        np_A = A.astype(dtype=(dtype or A.dtype), copy=True)

    elif sp.issparse(A):
        np_A = np.empty(A.shape, dtype=(dtype or A.dtype))
        np_A = A.toarray(out=np_A)

        # This shouldn't bee needed; just-in case!
        if np_A.dtype != dtype and dtype != None:
            np_A = np_A.astype(dtype)

    elif callable(A):
        if shape is None:
            try:
                shape = A.shape
            except:
                ValueError(
                    "For non-array A, the size (domain of A) must be passed"
                )

        e_vec = np.empty(shape[-1])
        np_A = np.empty(shape)
        for j in range(e_vec.size):
            e_vec[:] = 0.0
            e_vec[j] = 1.0
            np_A[..., j] = matrix_vecmult(A, e_vec)

    elif misc.isiterable(A):
        np_A = np.array([v for v in A], dtype=dtype)

    else:
        raise TypeError(
            f"Couldn't transform passed object 'A' of type {type(A)} to an array!"
        )

    return np_A


def matrix_trace(
    A: np.ndarray | sp.spmatrix | abc.Callable[[np.ndarray], np.ndarray],
    size: int | None = None,
    method: str = "exact",
    randomization_vectors: list[np.ndarray] | None = None,
    return_randomization_vectors: bool = False,
    sample_size: int = 100,
    optimize_sample_size: bool = False,
    min_sample_size: int = 10,
    max_sample_size: int = 100,
    distribution: str = "Rademacher",
    random_seed=None,
    accuracy: float = 1e-2,
) -> float | tuple[float, list[np.ndarray]]:
    """
    Evaluate/approximate the trace of a given matrix or operator

    :param A: Linear operator to compute trace of.
    :param size: size of the domain of A; required if A is callable.
    :param method: Method of evaluation. If "exact", we simply sum the diagonal of A. If
        "randomized", a Hutchinson style trace estimator, as described in
        :py:meth:`pyoed.utility.trace_estimator`, is employed. All of the futher keyword
        arguments are in the case of method="randomized" and are otherwise ignored if
        method="exact".
    :param randomization_vectors: The randomization vectors drawn to perform the
        computation. If not passed, they will be sampled.
    :param sample_size: The number of samples to use. :param optimize_sample_size: if
        `True`, `sample_size` is replaced with an optimized version, where the optimal
        sample size minimizes the variance of the estimator and is between
        `min_sample_size`, and `max_sample_size`.
    :param min_sample_size: Minimum number of random samples.
    :param max_sample_size: Maximum number of random samples.
    :param distribution: The probability distribution used for random sampling. Both
        'Rademacher' and 'Gaussian' are supported.
    :param random_seed: The random seed to use for the distribution samples.
    :param accuracy: convergence criterion.

    :return: exact/approximate value of the trace of A. If
        `return_randomization_vectors` is True it also, additionally, returns the
        samples drawn during the randomized method.
    """
    if re.match(r"\Aexact\Z", method, re.IGNORECASE):
        diag = matrix_diagonal(A, size=size)
        tr = np.sum(diag)

    elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
        if randomization_vectors is not None:
            tr = 0.0
            for v in randomization_vectors:
                tr += v @ matrix_vecmult(A, v)
            tr /= len(randomization_vectors)
        else:
            converged, tr, randomization_vectors, _ = stats.trace_estimator(
                matvec_fun=lambda x: matrix_vecmult(A, x),
                vector_size=size,
                sample_size=sample_size,
                optimize_sample_size=optimize_sample_size,
                min_sample_size=min_sample_size,
                max_sample_size=max_sample_size,
                distribution=distribution,
                random_seed=random_seed,
                accuracy=accuracy,
            )
            if not converged:
                print("Estimator didn't converge; results are unreliable...")
            if return_randomization_vectors:
                return tr, randomization_vectors
    else:
        raise ValueError(f"Unsupported evaluation method '{method}'")
    return tr


def matrix_logdet(
    A,
    size=None,
    method="exact",
    randomization_vectors: list[np.ndarray] | None = None,
    return_randomization_vectors: bool = False,
    sample_size: int = 100,
    optimize_sample_size: bool = False,
    min_sample_size: int = 10,
    max_sample_size: int = 100,
    distribution: str = "Rademacher",
    random_seed=None,
    accuracy: float = 1e-2,
    sigma_bounds=[None, None],
    n_deg=14,
) -> float | tuple[float, list[np.ndarray]]:
    """
    Evaluate/approximate the log-det of a given matrix or operator

    :param A: Linear operator to compute logdet of.
    :param size: size of the domain of A; required if A is callable.
    :param method: Method of evaluation. If "exact", we simply sum the diagonal of A.
        If "randomized", a chebyshev-approximation type-method is employed from
        :py:meth:`pyoed.utility.logdet_estimator`. All of the futher keyword arguments
        are in the case of method="randomized" and are otherwise ignored if
        method="exact".
    :param randomization_vectors: The randomization vectors drawn to perform the
        computation. If not passed, they will be sampled.
    :param sample_size: The number of samples to use. :param optimize_sample_size: if
        `True`, `sample_size` is replaced with an optimized version, where the optimal
        sample size minimizes the variance of the estimator and is between
        `min_sample_size`, and `max_sample_size`.
    :param min_sample_size: Minimum number of random samples.
    :param max_sample_size: Maximum number of random samples.
    :param distribution: The probability distribution used for random sampling. Both
        'Rademacher' and 'Gaussian' are supported.
    :param random_seed: The random seed to use for the distribution samples.
    :param accuracy: convergence criterion.

    :return: exact/approximate value of the logdet of the covariance matrix
        of the underlying model. If `return_randomization_vectors` is
        `True`, instead returns tuple of the form `(logdet, randomization_vectors)`
    """
    if re.match(r"\Aexact\Z", method, re.IGNORECASE):
        A_mat = asarray(A) if size is None else asarray(A, shape=(size, size))
        sign, logdet = np.linalg.slogdet(A_mat)
    elif re.match(r"\Arandom(ized)*\Z", method, re.IGNORECASE):
        converged, logdet, randomization_vectors, _ = stats.logdet_estimator(
            matvec_fun=lambda x: matrix_vecmult(A, x),
            vector_size=size,
            randomization_vectors=randomization_vectors,
            sample_size=sample_size,
            optimize_sample_size=optimize_sample_size,
            min_sample_size=min_sample_size,
            max_sample_size=max_sample_size,
            distribution=distribution,
            random_seed=random_seed,
            accuracy=accuracy,
            sigma_bounds=sigma_bounds,
            n_deg=n_deg,
        )
        if not converged:
            print("Estimator didn't converge; results are unreliable...")
        if return_randomization_vectors:
            return logdet, randomization_vectors
    else:
        raise ValueError(f"Unsupported evaluation method '{method}'")
    return logdet


def matrix_diagonal(A, size=None):
    """
    Return a copy the diagonal of a given matrix, i.e., the variances for a
    covariance matrix.

    :param np.ndarray | sp.spmatrix | abc.Callable[[np.ndarray],np.ndarray] A: Matrix
        to compute diagonal of.
    :param int size: shape of A; required if A is callable. Default is None.

    :raises ValueError: if A is callable and size is not passed.

    :return: np.ndarray holding the diagonal of the underlying matrix.
        If the size of the underlying distribution is 1, this returns a scalar (variance)

    :rtype: np.ndarray | float
    """
    if isinstance(A, np.ndarray):
        diag = np.diagonal(A)
    elif sp.issparse(A):
        diag = A.diagonal()
    else:
        if size is None:
            try:
                size = A.shape[0]
            except:
                raise ValueError(
                    "For non-array A, the size (domain of A) must be passed"
                )
        diag = np.empty(size)
        e_vec = np.empty(size)
        for i in range(size):
            e_vec[:] = 0.0
            e_vec[i] = 1.0
            diag[i] = matrix_vecmult(A, e_vec)[i]
    return diag


def matrix_vecmult(A, x, in_place=False):
    """
    Evaluate the product of a matrix by a vector `x`.

    :param np.ndarray | sp.spmatrix | abc.Callable[[np.ndarray],np.ndarray] A:
        Operator to apply to `x`.
    :param float | np.ndarray x | sp.spmatrix: vector to act on.
    :param bool in_place: overwrite the passed vector `x` (if `True`).  If set to
        `False`, a new instance is created and returned. Default is `False`.

    .. note::
        `in_place` is ignored if `x` is a non-iterable, i.e., a scalar.

    :return: `Ax`
    :rtype: np.ndarray
    """
    if sp.issparse(x):
        x = x.toarray()
        x_size = x.size
        if in_place:
            print(
                "Will not allow in-place writing of the passed sparse representation of"
                " a vector."
            )
            print("Returned instance is not the same as the passed.")
        else:
            in_place = True  # overwrite the numpy array created above
    else:
        try:
            x = np.asarray(x).flatten()
            x_size = x.size
        except:
            raise TypeError(
                f"Unsupported type ({type(x)}) of passed vector x"
            )

    # Multiply x by the covariance matrix
    out_x = x if in_place else x.copy()
    if isinstance(A, np.ndarray) or sp.issparse(A):
        out_x[:] = A @ out_x
    elif callable(A):
        result = asarray(A(out_x))
        if result.size != out_x.size and in_place:
            warnings.warn("in_place flag is ignored as the marix is non-square")
            out_x = result
        elif result.size != out_x.size:
            out_x = result
        else:
            out_x[:] = result
    else:
        raise TypeError(
            f"Unsupported type ({type(A)}) of passed matrix A"
        )
    if in_place and out_x is not x:
        raise RuntimeError(
            "In-place multiplication somehow failed!"
        )
    return out_x


def factorize_spsd_matrix(A, shape=None, lower=True):
    """
    Genereate Cholesky decomposition of the passed symmetric positive semidefinite
    matrix.

    :param float | np.ndarray | sp.spmatrix | abc.Callable[[np.ndarray],np.ndarray] A:
        matrix to factorize.

    :param tuple[int] shape:
        shape of the matrix `A`. Necessary if `type(A)==abc.Callable[np.ndarray]`. Default
        is `None`.

    :param bool lower: whether to return the lower triangular portion of the
        Cholesky factorization. Default is `True`.

    :return: Square root of `A` if scalar, otherwise, lower triangular portion
        of the matrix Cholesky decomposition.  Return type is same as `A` type
        unless a callable is passed. In this case, a scipy linear operator is
        returned.

    :raise TypeError: if `A` is not of the specified types.
    """
    if misc.isnumber(A):
        std = np.sqrt(A)

    elif isinstance(A, np.ndarray):
        n = A.shape[0]
        assert A.ndim == 2 and A.shape == (n, n), "Must be a square 2D matrix!"

        # Eliminate rows/columns with all zeros
        valid_inds = np.asarray(
            [
                i
                for i in range(n)
                if not (np.count_nonzero(A[i, :]) == np.count_nonzero(A[:, i]) == 0)
            ]
        )
        zero_inds = np.setdiff1d(np.arange(n), valid_inds)

        if valid_inds.size == 0:
            std = np.zeros_like(A, dtype=A.dtype)
        else:
            A_reduced = A[valid_inds, :][:, valid_inds]

            # Cholesky factor (for nonzero rows/columns)
            std_reduced = np.linalg.cholesky(A_reduced)
            if not lower:
                std_reduced = std_reduced.T

            std = std_reduced
            if zero_inds.size > 0:
                for i in zero_inds:
                    for axis in [0, 1]:
                        std = np.insert(std, i, 0.0, axis=axis)

    elif sp.issparse(A):
        n = A.shape[0]
        assert A.ndim == 2 and A.shape == (n, n), "Must be 2D square matrix!"

        # make sure the matrix is indexible
        if A.format in ["dia", "coo"]:
            A = A.tocsc()

        # Eliminate rows/columns with all zeros
        valid_inds = np.asarray(
            [i for i in range(n) if not (A[[i], :].nnz == A[:, [i]].nnz == 0)]
        )
        zero_inds = np.setdiff1d(np.arange(n), valid_inds)

        if valid_inds.size == 0:
            std = sp.csc_matrix((n, n), dtype=A.dtype)
        else:
            A_reduced = A[valid_inds, :][:, valid_inds]

            # Sparse LU (for the reduced version)
            try:
                LU = splinalg.splu(A_reduced, diag_pivot_thresh=0)
            except RuntimeError:
                print(
                    "Failed to use efficient LU factorization for sparse matrices; "
                    "Trying dense!"
                )
                std = factorize_spsd_matrix(A.toarray(), shape=shape, lower=lower)
                std = sp.csc_matrix(std, dtype=A.dtype)

            else:
                # Check the matrix is positive semi definite:
                if np.all(LU.perm_r == np.arange(valid_inds.size)) and np.all(
                    LU.U.diagonal() > 0
                ):
                    std_reduced = LU.L @ (sp.diags(LU.U.diagonal() ** 0.5))
                    if not lower:
                        std_reduced = std_reduced.T
                    std_reduced = std_reduced.tocsc()

                    # Insert zeros into std.
                    if zero_inds.size == 0:
                        std = std_reduced
                    else:
                        # Expand
                        std = sp.lil_matrix((n, n), dtype=A.dtype)
                        for i in range(valid_inds.size):
                            std[valid_inds[i], valid_inds] = std_reduced[i, :]
                            # std[:, valid_inds[i]] = std_reduced[i]

                        std = std.tocsc()

                else:
                    print(
                        "Failed to use efficient LU factorization for sparse matrices;"
                    )
                    print("Trying dense!")
                    std = factorize_spsd_matrix(A.toarray(), shape=shape, lower=lower)
                    std = sp.csc_matrix(std, dtype=A.dtype)

    elif callable(A):
        # TODO: need more efficient version that this!
        L = factorize_spsd_matrix(asarray(A, shape=shape), lower=lower)
        matvec = lambda x: L @ x
        std = splinalg.LinearOperator(L.shape, matvec=matvec)

    else:
        raise TypeError(
            f"A must be scalar, numpy array,scipy sparse matrix or a linear operator,"
            f" but recieved {type(A)}"
        )

    return std


def truncate(x, lb=0, ub=1):
    """
    Return a numpy array representation of the passed object `x` with values truncated
    based on the lower bound `lb` and the upper bound `ub`.  All values in `x` below
    `lb` are replaced with `lb` and all values above `ub` are replaced with `ub`

    :param float | np.ndarray x: object to truncate.
    :param float lb: lower bound. Default is 0.
    :param float ub: upper bound. Default is 1.

    :return: np.ndarray representation of `x` with truncated values.
    """
    x = asarray(x).flatten()
    assert misc.isnumber(lb), "`lb` must be a number/scalar!"
    assert misc.isnumber(ub), "`ub` must be a number/scalar!"
    x[x < lb] = lb
    x[x > ub] = ub
    return x


# Numbers and Enumeration:
# ------------------------
def remainder(x, y, num_decimals=100):
    """
    Return the remainder of dividing x by y, all rounded to the passed number of
    decimals (if any)

    :param float | np.ndarray x: dividend.
    :param float | np.ndarray y: divisor.
    :param int num_decimals: number of decimals to round to. Default is 100.

    .. Warning::
        If `x` and `y` are arrays, they must be conformable. That is, they must
        either be of the same size or one of them must be of size 1. If `x` and
        `y` are arrays of different sizes, the function will raise a TypeError.

    :rtype: np.ndarray
    """
    x = asarray(x)
    y = asarray(y)
    if x.size == y.size:
        pass
    elif x.size == 1 < y.size:
        x = np.repeat(x, y.size)
    elif y.size == 1 < x.size:
        y = np.repeat(y, x.size)
    elif x.size == 0:
        pass
    else:
        raise TypeError(
            f"The passed arrays/values are not conformable! "
            f"{x.size=}; {y.size=}"
        )

    if x.size == 0:
        rem = np.zeros(0)

    else:
        # Round
        if num_decimals is not None:
            x = np.round(x, decimals=num_decimals)
            y = np.round(y, decimals=num_decimals)
        rem = x - np.round(
            np.array(np.round(x / y, decimals=num_decimals), dtype=int) * y,
            decimals=num_decimals,
        )

    return rem


def ncr(n, r):
    r"""
    Calculate combinations efficiently :math:`nCr = \frac{n}{ r! \times (n-r)! }`.

    :param int n: non-negative integer
    :param int r: non-negative integer

    :return: :math:`nCr`
    :rtype: int
    """
    if not (0 <= r <= n):
        raise ValueError(
            f"nCr requires 0 <= r <= n; received r={r} > n={n}"
        )

    r = min(r, n - r)
    if r == 0:
        return 1
    numer = reduce(op.mul, range(n, n - r, -1))
    denom = reduce(op.mul, range(1, r + 1))
    nCr = numer // denom
    return nCr


def enumerate_binary(size: int, num_active: int | None = None) -> list[np.ndarray]:
    """
    Return a list of numpy arrays of all possible binary arrays of given size.

    :param size: the size of binary arrays to enumerate.
    :param num_active: number of active entries in the binary arrays.
        If `None`, all possible binary arrays of size `size` are returned.
    """
    if size < 1:
        raise ValueError("Size must be a positive integer!")
    if (num_active is not None) and ((num_active < 0) or (num_active > size)):
        raise ValueError(
            "num_active must be non-negative and less than or equal to size!"
        )
    if num_active is None:
        return [
            np.array(c).flatten() for c in itertools.product([False, True], repeat=size)
        ]
    result = []
    for active in itertools.combinations(range(size), num_active):
        vector = np.zeros(size, dtype=bool)
        for idx in active:
            vector[idx] = True
        result.append(vector)
    return result


def index_to_binary_state(k, size, dtype=bool):
    """
    Return the binary state=:math:`(v_1, v_2, \\dots)` of dimension=size, with index k,
    where the index k is defined by the unique mapping:

    .. math::
       k = 1 + \\sum_{i=1}^{size} v_i 2^{i-1}

    :param int k: index of the binary state.
    :param int size: dimension of the binary state.
    :param dtype: data type of the returned numpy array. Default is bool.

    :rtype: np.ndarray
    """
    assert k == int(k), "k must be an integer!"
    k = int(k)
    assert k > 0, "k must be positive integer/indes; received %d" % k
    # _state = np.fromstring(np.binary_repr(k - 1)[::-1], dtype="S1").astype(dtype)
    # Convert result to integer (from bit type) then convert it to passed `dtype`
    # NOTE: first, we convert to in (in case float state is passed with
    # binary values) then to decimal. Abhi caught this issue due to change in behaviour of `astype` in numpy!
    _state = (
        np.frombuffer(np.binary_repr(k - 1)[::-1].encode("ascii"), dtype="S1")
        .astype(int)
        .astype(dtype)
    )
    if _state.size == size:
        state = _state
    elif _state.size < size:
        state = np.zeros(size, dtype=dtype)
        state[: _state.size] = _state
    else:
        raise ValueError(
            f"Passed index k exceeds the dimension set by size"
        )

    return state


def index_from_binary_state(state):
    """
    Reverse of "index_to_binary_state"
    Return the index k corresponding to the passed state (of dimension=size),
    where the index k is defined by the unique mapping:

    .. math::
       k = 1 + \\sum_{i=1}^{size} v_i 2^{i-1}

    :param np.ndarray state: binary state of dimension=size.

    :raises: :py:class:`ValueError` if the passed state contains inf or nan values

    .. note::
        Entries of the passed state are interpreted as True/False by conversion to bool

    :rtype: int
    """
    state = np.asarray(state).flatten()
    if not set.issubset(set(state), {0, 1}):
        raise ValueError(
            "The passed state is not a valid binary state/array!\n"
            f"Passed state: {repr(state)}"
        )

    # Convert state to decimal data type to allow very large numbers
    # first, we convert to in (in case float state is passed with binary values) then to decimal
    state = state.astype(int).astype(decimal.Decimal)

    # Assert, and convert to binary
    # k = 1 + state.dot(2**np.arange(state.size))
    k = 1
    for s in range(state.size):
        k += state[s] * 2**s
    return k


def sample_binary_states(
    size: int,
    num_samples: int,
    num_active: int | None = None,
    random_seed_or_rng: int | np.random.Generator | None = None,
) -> np.ndarray:
    """
    Uniformly sample random binary states of given size and number of active entries.

    :param size: dimension of the binary state.
    :param num_samples: number of binary states to sample.
    :param num_active: number of active entries in the binary states. If `None`, then
        no restriction is placed on the number of active entries.
    :param random_seed_or_rng: random seed or random number generator to use for
        sampling. Default is None.
    :return: a 2D numpy array of shape (num_samples, size) containing the sampled
        binary states.
    """
    rng = np.random.default_rng(random_seed_or_rng)
    if num_active is None:
        return rng.choice([False, True], size=(num_samples, size))
    states = np.zeros((num_samples, size), dtype=bool)
    states[:, :num_active] = True
    for i in range(num_samples):
        rng.shuffle(states[i, :])
    return states


def threshold_relaxed_design(
    design,
    method="vanishing",
    num_active=1,
    vanishing_threshold=1e-3,
    shuffle_extra_max=False,
    random_seed=None,
    candidates=None,
):
    """
    Given a non-binary state/design, find the winner entries based on the passed method, and convert
    the passed design/state to a binary state.
    This is useful to OED applications where a relaxed design is usually rounded-off to a binary design.

    If num_active is 2, and method is 'max', the maximum two design weights are selected as winners
    return the number of winners, their indexes in the design, and their coordinates  (if coordinates) are passed.
    The number of winners returned <=  num_active based on the availability in
    the design.

    For example, if no sufficient entries win from the design, e.g. very sparse,
    the maximum number of positive values is picked.

    :param design: a one dimensional array-like iterable to be thresholded
    :param str method: The following rules are followed based on the
        thresholding method:

        - 'vanishing': any sensor with design value above (or equal) the
          `vanishing_threshold` is activated; num_active is ignored.
        - 'median': any sensor above (or equal) the median value is activated;
          num_active and vanishing_threshold are ignored.
        - 'max':

            + If `shuffle_extra_max` is `True`, they maximum `num_active` sensors are returned;
              if there are multiple entries with equal values, extra sensors are shuffled
            + If `shuffle_extra_max` is False, design entries are ranked (in decending order),
              and any entry equal to or exceeds the maximum `num_active` entry
              is activated.

    :param int num_active: number of sensors to activate (if `method` is 'max')
    :param float vanishing_threshold: weight point over which sensor is activated;
        used if `method` is 'vanishing'
    :param bool shuffle_extra_max: used only for `method` 'max'; see above
    :param None|int random_seed: used to initialize the random number generator to do shuffling if `shuffle_extra_max` is `True`; useful for reproducibility
    :param candidates: 2d array with coordinates of the observation gridpoints with each,

    :returns:
        - `num`: number of active sensors/entries
        - `indexes`: indexes of the design vector that are activated
        - `coords`: The coordinate entries from `candidates` that are activated by thresholding;
          `None` if `candidates` is `None`

    """
    design = np.array(design).flatten()
    active = np.where(design >= vanishing_threshold)[0]
    inactive = np.where(design < vanishing_threshold)[0]
    coordinates = candidates

    if design.min() < 0:
        print("The design seems to contain negative values!")
        raise ValueError
    num = 0
    indexes = []
    coords = []
    if re.match(r"\Amax\Z", method, re.IGNORECASE):
        sorter = np.argsort(design)[::-1]  # from largest to smallest
        msg = (
            "Number of active sensors must be between 1 and the design size %d"
            % design.size
        )
        assert 0 < num_active <= design.size, msg

        if shuffle_extra:
            local_threshold = design[sorter[num_active - 1]]
            active = np.where(design >= local_threshold)[0]
            # Truncate extra
            num_extra = active.size - num_active
            if num_extra > 0:
                min_val = np.min(design[active])
                excedant_indexes = np.where(design > min_val)[0]
                replica_indexes = np.where(design == min_val)[0]
                num_keep = replica_indexes.size - num_extra
                rng = np.random.default_rng(random_seed)
                shuffled_indexes = rng.choice(replica_indexes, num_keep, replace=False)
                indexes = np.union1d(excedant_indexes, shuffled_indexes)
            else:
                indexes = active
        else:
            local_threshold = design[sorter[num_active - 1]]
            active = np.where(design >= local_threshold)[0]
            num_active = active.size
            indexes = sorter[:num_active]

        indexes.sort()
        indexes = np.asarray(indexes, dtype=int)
        num = indexes.size
        if coordinates is not None:
            try:
                coords = coordinates[indexes, :]
            except:
                print("Failed to extract coordinates!")
    elif re.match(r"\Avanish(ing)*\Z", method, re.IGNORECASE):
        vanishing_threshold
        indexes = np.where(design >= vanishing_threshold)[0]
        num = indexes.size
        if coordinates is not None:
            try:
                coords = coordinates[indexes, :]
            except:
                print("Failed to extract coordinates!")
    elif re.match(r"\A(nonzero|active)\Z", method, re.IGNORECASE):
        indexes = active
        num = indexes.size
        if coordinates is not None:
            try:
                coords = coordinates[indexes, :]
            except:
                print("Failed to extract coordinates!")
    elif re.match(r"\Amedian\Z", method, re.IGNORECASE):
        indexes = np.where(design > np.median(design))[0]
        num = indexes.size
        if coordinates is not None:
            try:
                coords = coordinates[indexes, :]
            except:
                print("Failed to extract coordinates!")
    else:
        print("Unknown thresholding method: '{0}' ".format(method))
        raise ValueError

    if num == 0:
        print(
            "**\nWARNING: No active sensors were found with weights above threshold!   "
            "          \nMaximum weight found is: {0}\nPassed threshold is: {1}\n**\n".format(
                design.max(), vanishing_threshold
            )
        )
    return num, indexes, coords


def group_by_num_active(
    results_dict: dict[int, float],
    design_size: int,
) -> tuple[list[int], list[list[float]]]:
    """
    Given a dictionary (results_dict), with keys representing binary index,
    group the values by the size of the design corresponding to each design key

    :param results_dict: dictionary of results
    :param design_size: size of the design

    :raises TypeError: if results_dict is not a dictionary
    """
    if not isinstance(results_dict, dict):
        raise TypeError("results_dict must be a dictionary!")

    sizes = []
    values = []
    for k in results_dict:
        if not misc.isnumber(k):
            continue
        design = index_to_binary_state(k, size=design_size)
        active = int(np.linalg.norm(design, ord=0))
        value = results_dict[k]

        if active in sizes:
            ind = sizes.index(active)
            values[ind].append(value)
        else:
            sizes.append(active)
            values.append([value])
    return sizes, values
