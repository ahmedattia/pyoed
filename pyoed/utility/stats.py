# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides access to functions to perform various statistical operations.
"""
# TODO: convert all docstrings to REST format

import sys
import re
import warnings
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg

from . import misc

__python_version = (
    sys.version_info.major,
    sys.version_info.minor,
    sys.version_info.micro,
)
if __python_version >= (3, 0, 0):
    raw_input = input
else:
    range = xrange


# define all functions here so it can be easily aggregated in __init__.py of utility package
__all__ = [
    "sample_standard_Normal",
    "sample_Rademacher",
    "trace_estimator",
    "logdet_estimator",
    "lanczos",
    "rsvd",
    "calculate_rmse",
    "factorize_covariances",
    "create_covariance_matrix",
    "posterior_covariance_matrix",
    "calculate_oed_criterion",
    "create_complex_covariance_matrices",
    "real_to_complex_covariances",
    "complex_to_real_covariances",
    "complex_sample_covariance",
]


# Samplers:
# ---------
def sample_standard_Normal(size, random_seed=None):
    """
    Sample a standard normal random variable, of shape=size
    """
    # Create a random number generator instance from the passed seed
    rng = np.random.default_rng(random_seed)

    size = tuple(list(np.array(size).flatten()))
    sample = rng.standard_normal(size)

    return sample


def sample_Rademacher(size, dtype=float, random_seed=None):
    """
    Sample a Rademacher distribution, and return a sample of the passed size (Numpy array is returned for any size)
    """
    # Create a random number generator instance from the passed seed
    rng = np.random.default_rng(random_seed)

    # assertion on size, will be handled by numpy.random
    r = rng.binomial(1, 0.5, size=size)
    sample = np.ones(size, dtype=int).astype(dtype)
    sample[r == 0] = -1

    return sample


# Randomization:
# --------------
def trace_estimator(
    matvec_fun,
    vector_size,
    sample_size=100,
    optimize_sample_size=False,
    min_sample_size=10,
    max_sample_size=500,
    distribution="Rademacher",
    random_seed=None,
    accuracy=1e-2,
    verbose=False,
):
    """
    Given a `matvec_fun` that returns the result of multiplying the matrix
    (e.g., underlying covariance matrix) by a vector, estimate the trace of the matrix using randomization

    :param matvec_fun: a function that returns the result of multiplying the matrix
        (e.e., underlying covariance matrix) by a vector, estimate the trace of the matrix using randomization
    :param vector_size: sample size to use for randomized trace estimation
    :param bool optimize_sample_size: if `True`, `sample_size` is replaced with an optimized version,
        where the optimal sample size minimizes the variance of the estimator, and must be between `min_sample_size`,
        and `max_sample_size`
    :param int min_sample_size:
    :param int max_sample_size:
    :param str distribution: The probability distribution used for random sampling. Both 'Rademacher' and 'Gaussian' are supported.
    :param float accuracy: convergence criterion
    :param bool verbose: screen verbosity of progress

    :returns:
        - `converged`: (bool) a flag indicating whether the estimator converged or not
        - `trace`: randomization estimate of the trace of the matrix whose product by a vector is evaulated by `matvec_fun`
        - `sample_history`
        - `trace_history`
    """
    assert callable(matvec_fun), "'matvec_fun' must be a callable function!"

    if not optimize_sample_size:
        min_sample_size = max_sample_size = sample_size

    assert (
        min_sample_size <= sample_size <= max_sample_size
    ), "Improper sample sizing: min, max!"

    if re.match(r"\Arademacher\Z", distribution, re.IGNORECASE):
        sampler = sample_Rademacher

    elif re.match(r"\A(Gaussian|Normal)\Z", distribution, re.IGNORECASE):
        sampler = sample_standard_Normal

    else:
        raise ValueError(f"Probability distribution '{distribution}' is not supported!")

    trace_history = []
    sample_history = []
    trace = 0.0

    sum_tr = 0.0
    sum_tr2 = 0.0
    _iter = 0

    nfailures = 0
    while _iter < min_sample_size:
        # Update random seed for different samples
        if random_seed is not None:
            random_seed += 1

        if verbose:
            print(
                (
                    "\rDEBUG: UTILITY-FUNCTION: Estimating Matrix Trace:"
                    f" {_iter+1}/{min_sample_size}"
                ),
                end="",
            )
        z = sampler(vector_size, random_seed=random_seed)
        try:
            tr = np.dot(z, matvec_fun(z))
        except:
            if nfailures < 10:
                print("Failed to apply matvec; retrying")
                nfailures += 1
                continue
            else:
                print("Failed to apply matvec many times!")
                raise
        sample_history.append(z)
        sum_tr += tr
        sum_tr2 += tr**2
        #
        _iter += 1
        trace_history.append(sum_tr / _iter)

    exp_tr = sum_tr / _iter
    exp_tr2 = sum_tr2 / _iter
    var_tr = abs(
        exp_tr2 - exp_tr**2
    )  # added abs to avoid warnings due to numerical errors

    converged = True
    nfailures = 0
    while (np.sqrt(var_tr) > accuracy * exp_tr) and _iter < max_sample_size - 1:
        # Update random seed for different samples
        if random_seed is not None:
            random_seed += 1

        if verbose:
            print(
                (
                    "\rDEBUG: UTILITY-FUNCTION: Estimating Matrix Trace:"
                    f" {_iter+1}/{max_sample_size}"
                ),
                end="",
            )
        z = sampler(vector_size, random_seed=random_seed)

        try:
            tr = np.dot(z, matvec_fun(z))
        except:
            if nfailures < 10:
                print("Failed to apply matvec; retrying")
                nfailures += 1
                continue
            else:
                print("Failed to apply matvec many times!")
                raise
        sample_history.append(z)
        sum_tr += tr
        sum_tr2 += tr**2
        #
        _iter += 1
        exp_tr = sum_tr / _iter
        exp_tr2 = sum_tr2 / _iter
        var_tr = abs(
            exp_tr2 - exp_tr**2
        )  # added abs to avoid warnings due to numerical errors
        #
        trace_history.append(exp_tr)
        #
        if _iter >= max_sample_size:
            if optimize_sample_size:
                converged = False
            break

    trace = exp_tr
    return converged, trace, sample_history, trace_history


def logdet_estimator(
    matvec_fun,
    vector_size,
    lanczos_degree=16,
    sample_size=32,
    randomization_vectors=None,
    distribution="Rademacher",
    random_seed=None,
    verbose=False,
    **kwargs,
) -> tuple[bool, float, list[np.ndarray], list[np.ndarray]]:
    """
    Given a `matvec_fun` that returns the result of multiplying the matrix (e.g.,
    underlying covariance matrix) by a vector, estimate the logarithm of the determinant
    (logdet) of the matrix using a stochastic Lanczos approximatioa stochastic Lanczos
    approximationn.

    .. note::
        We implement the algorithm described in the following paper:


    :param matvec_fun: a function that returns the result of multiplying the matrix
        (e.e., underlying covariance matrix) by a vector. **NOTE**: It is assumed that
        `matvec_fun` represents a symmetric positive definite linear operator. No check
        is performed to verify this.
    :param vector_size: size of the vector to use for the matvec function
    :param lanczos_degree: degree of the Lanczos approximation. Default is 16.
    :param sample_size: number of random vectors to use for the stochastic Lanczos
        approximation. Default is 32. Ignored if `randomization_vectors` is provided.
    :param randomization_vectors: a list of random vectors to use for the stochastic
        Lanczos approximation. If provided, `sample_size` is ignored. Default is `None`.
    :param distribution: The probability distribution used for random sampling. Both
        'Rademacher' and 'Gaussian' are supported. Default is 'Rademacher'.
    :param random_seed: random seed for random number generator. Default is `None`.
    :param verbose: screen verbosity of progress

    :returns: A tuple containing the following elements:

        - `converged`: a flag indicating whether the estimator converged or not.
        - `logdet`: randomization estimate of the logdet of the matrix whose product by a vector is evaulated by `matvec_fun`
        - `sample_history`
        - `logdet_history`
    """
    printer = lambda s: print(s) if verbose else None

    assert callable(matvec_fun), "'matvec_fun' must be a callable function!"
    A = splinalg.LinearOperator((vector_size, vector_size), matvec_fun)

    if randomization_vectors is not None:
        sample_size = len(randomization_vectors)

        rnd_vec_idx = 0

        def sampler(s, *args, **kwargs):
            nonlocal rnd_vec_idx
            rnd_vec_idx += 1
            return randomization_vectors[rnd_vec_idx - 1]

    elif re.match(r"\Arademacher\Z", distribution, re.IGNORECASE):
        sampler = sample_Rademacher
    elif re.match(r"\A(Gaussian|Normal)\Z", distribution, re.IGNORECASE):
        sampler = sample_standard_Normal
    else:
        raise ValueError("Probability distribution %s is not supported!" % distribution)

    if lanczos_degree + 1 >= vector_size:
        printer(
            "Degree of Lanczos approximation is larger than matrix dimension!\n"
            "Reducing it to matrix dimension.\n"
            "Consider using a smaller degree or a direct method instead."
        )
        lanczos_degree = vector_size - 1

    # TODO: Add trace estimator tricks to improve convergence
    converged = True

    sample_history = []
    logdet_history = []
    logdet = 0.0

    def generate_sample(l):
        seed = None if random_seed is None else random_seed + l
        u_l = sampler(vector_size, random_seed=seed)
        v_l = u_l / np.linalg.norm(u_l)
        T, _ = lanczos(A, v_l, k=lanczos_degree + 1)
        T, Y = np.linalg.eigh(T)
        ld_l = vector_size * np.sum(
            [Y[0, i] ** 2 * np.log(T[i]) for i in range(len(T))]
        )
        return u_l, ld_l

    samples = [generate_sample(l) for l in range(sample_size)]
    sample_history, logdet_history = zip(*samples)
    logdet = np.average(logdet_history)

    return converged, logdet, sample_history, logdet_history


def lanczos(
    H: np.ndarray | sp.spmatrix | splinalg.LinearOperator,
    v0: np.ndarray,
    k: int = 20,
    termination_tol: float = 1.0e-10,
) -> tuple[np.ndarray, np.ndarray]:
    """
    Perform `k` iterations of Lanczos' algorithm.

    .. note::
        See chapter 10 of the following reference for the implemented algorithm:

        Golub, G. H., & Van Loan, C. F. (2013). Matrix computations. The Johns Hopkins University Press.

    :param np.ndarray | sp.spmatrix | splinalg.LinearOperator H: Matrix to perform
        Lanczos iterations on. Does not need to be of these types, but must implement
        the .shape property, .T method and @ operator.
    :param psi0: Initial vector for the Lanczos iteration.
    :param k: Number of iterations to perform.
    :param termination_tol: Tolerance for early termination of the Lanczos iteration.

    :returns: A tuple containing the following elements:

        - `T`: Tridiagonal matrix of size `k x k` containing the Lanczos iterates.
        - `vecs`: Matrix of size `n x k` containing the Lanczos vectors.
    """
    if v0.ndim != 1:
        raise ValueError("v0 should be a vector!")
    if H.shape[1] != v0.shape[0]:
        raise ValueError("H and v0 do not have compatible dimensions!")

    v0 = v0 / np.linalg.norm(v0)
    vecs = [v0]
    T = np.zeros((k, k))

    vi = H @ v0
    alpha = T[0, 0] = np.inner(v0.conj(), vi).real
    vi = vi - alpha * vecs[-1]
    for i in range(1, k):
        beta = np.linalg.norm(vi)
        if beta < termination_tol:  # Terminate early if beta is too small
            T = T[:i, :i]
            break
        vi /= beta
        vecs.append(vi)
        vi = H @ vi - beta * vecs[-2]
        alpha = np.inner(vecs[-1].conj(), vi).real
        vi = vi - alpha * vecs[-1]
        T[i, i] = alpha
        T[i - 1, i] = T[i, i - 1] = beta
    return T, np.array(vecs).T


def rsvd(
    A,
    k,
    q=1,
    compute_uv=True,
    Omega=None,
    p=20,
    distribution="Gaussian",
    random_seed=None,
    verbose=False,
):
    """
    Perform a Singular Value Decomposition (SVD) of a matrix A using a randomized SVD
    (rSVD) algorithm.

    .. note::
        See the following paper for more details of the algorithm:

        Halko, N., Martinsson, P. G., & Tropp, J. A. (2011). Finding Structure with
        Randomness: Probabilistic Algorithms for Constructing Approximate Matrix
        Decompositions. In SIAM Review (Vol. 53, Issue 2, pp. 217–288). Society for
        Industrial & Applied Mathematics (SIAM). https://doi.org/10.1137/090771806

    :param np.ndarray | sp.spmatrix | splinalg.LinearOperator A: Matrix to decompose.
        Does not need to be of these types, but must implement the .shape property, .T
        method and @ operator.
    :param int k: Number of singular values/vectors to compute.
    :param int q: Number of randomized subspace iterations to perform. Default is 1.
    :param bool is_symmetric: If `True`, the matrix is assumed to be symmetric, and
        the algorithm is optimized for this case. Default is `False`.
    :param bool compute_uv: If `True`, the left and right singular vectors are computed
        and returned in `U` and `Vh`, respectively. Default is `True`.
    :param np.ndarray | sp.spmatrix Omega: Random matrix to use for the rSVD algorithm.
        If not specified, a random matrix is generated internally. Default is `None`.
    :param int p: Number of columns of `Omega` to use. If Omega is provided, this
        setting is ignored. Default is 20.
    :param str distribution: The probability distribution used for random sampling. Both
        'Rademacher' and 'Gaussian' are supported. Default is 'Gaussian'.
    :param int random_seed: random seed for random number generator. Default is `None`.
    :param bool verbose: if `True`, print debug information.
    """
    m, n = A.shape
    l = k + p
    if Omega is not None:
        l = Omega.shape[1]
        p = l - k
    elif re.match(r"\Arademacher\Z", distribution, re.IGNORECASE):
        Omega = np.reshape(sample_Rademacher(n * l, random_seed=random_seed), (n, l))
    elif re.match(r"\A(Gaussian|Normal)\Z", distribution, re.IGNORECASE):
        Omega = np.reshape(
            sample_standard_Normal(n * l, random_seed=random_seed), (n, l)
        )
    else:
        raise ValueError(f"Probability distribution {distribution} is not")

    # Stage (1): Randomized Subspace Iteration
    Y = A @ Omega
    Q, _ = np.linalg.qr(Y, mode="reduced")

    for i in range(q):
        Y = A.T @ Q
        Q, _ = np.linalg.qr(Y, mode="reduced")
        Y = -A @ Q
        Q, _ = np.linalg.qr(Y, mode="reduced")

    # Stage (2): Approximate SVD
    B = (A.T @ Q).T
    U_tilde, S, Vh = np.linalg.svd(B, full_matrices=False, compute_uv=compute_uv)
    if compute_uv:
        U = Q @ U_tilde[:, :k]
        return U, S[:k], Vh[:k, :]
    return S[:k]


# Bayesian Analysis:
# ------------------
def factorize_covariances(covariance, lower=True):
    """
    Genereate Cholesky decomposition of the passed covariance matrix/kernel.
    For scalar covariance, this is the standard deviation.
    For matrices, this is the lower triangular matrix by default.

    :param covariance: `float`, or 2d `numpy.ndarray` or `scipy.spmatrix`

    :returns: square root of `covariance` if scalar, otherwise, lower triangular portion of
        the matrix Cholesky decomposition.
        Return type is same as `covariance` type unless a callable is passed. In this case, a scipy linear operator is returned.
    """
    print(
        "This function is not longer available. Use math.factorize_spsd_matrix instead."
    )
    raise NotImplementedError


def create_covariance_matrix(
    size=2, corr=False, stdev=None, return_factor=False, random_seed=None
):
    """
    construct a numpy covariance matrix of dimension (size x size),
    with/without correlations
    if return factor is True, the Cholesky factor (lower) is returned,
    otherwise the covariance matrix is returned

    The passed standard deviations `stdev` are not exactly the same as square root of the variances as they are used to set the Cholesky factor of the covariance matrix in the presence of correlations.
    """
    # TODO: Maybe consider sparse versions as well
    if size < 1:
        print("covariance size must be at least 1")
        raise ValueError

    # Create a random number generator instance from the passed seed
    rng = np.random.default_rng(random_seed)

    if stdev is not None:
        stdev = np.array(stdev).flatten()
        assert 0 <= stdev.min(), "standard deviations must nonnegative!"

    else:
        stdev = 1e-5 + rng.random(size)  # 0<1e-5<= standard deviations <= 1

    if 1 == stdev.size < size:
        stdev = np.ones(size) * stdev

    elif stdev.size == size:
        pass

    else:
        print("size mismatches length of passed standard deviations")
        raise ValueError

    # Construct reasonable correlation matrix; Start with the cholesky factor
    if corr:
        # non-Diagonal covariance matrix
        L = np.diag(stdev)
        for i in range(size - 1):
            coeff = stdev[i] * 0.95 * rng.random(size - (i + 1))  # 0<=coeff<=95
            L[i, i + 1 :] = coeff
            L[i + 1 :, i] = coeff

    else:
        L = np.diag(stdev)  # Diagonal covariance matrix

    # Either return the lower factor, or the full matrix (assured to be spsd)
    cov = L if return_factor else L.dot(L.T)

    return cov


def posterior_covariance_matrix(B, R, F=None, w=None):
    """
    Assuming Gaussianity, and given prior covariance matrix,
    and observation covariance matrix, construct the posterior covariance matrix

    :param B: Prior Covariance matrix
    :param R: Observation error covariance matrix
    :param F: Forward model: :math:`y = F x + \\eta; \\eta \\sim N(0, R)`; F is set to I if None is passed
    :param w: Design variable; set to 1 if None is Passed

    :returns: A: Posterior covariance matrix :math:`(F^T R^{-1/2} W R^{-1/2} F + B^{-1})^{-1}`

    """
    # Asert and get sizes
    if F is None:
        if B.shape != R.shape:
            print(
                """here, B, R must be of similar shapes;
                  otherwise observation operator will be required"""
            )
        raise ValueError
        Ns, No = B.shape
        if Ns != No:
            print("Covariance matrix must be square!")
            raise ValueError
    else:
        No, Ns = F.shape
        assert B.shape == (Ns, Ns), "Prior covariance shape is incorrect"
        assert R.shape == (No, No), "Observation error covariance shape is incorrect"

    # 1: prior covariance inverse
    Binv = np.linalg.inv(B)

    if w is None:
        w = np.ones(No)
    else:
        w = np.array(w).flatten()
        if w.size == 1:
            w = np.ones(No) * w[0]
    if w.size != No:
        print("design size must be %d, found %d" % (No, w.size))
        raise TypeError

    L = np.linalg.inv(factorize_covariances(R, lower=True))
    W = np.diag(w)
    H = F.T.dot(L.T)
    for o_ind in np.arange(No):
        H[o_ind, :] *= np.sqrt(w[o_ind])
    weighted_Hmisfit = H.dot(H.T)

    # Posterior covariance
    A = np.linalg.inv(Binv + weighted_Hmisfit)

    return A


def complex_sample_covariance(
    sample,
    ddof=1,
):
    """
    Evaluate sample covariances and pseudo covariances for a given sample from
    the distribution of a complex-valued random variable/vector.
    Sample covariances are evaluated based on conjugate transpose, but pseudo covariances
    are evaluated based on transpose of the deviations.

    :parm sample: ArrayLike iterable of two dimensions. Each row represetns
        a sample of the underlying distribution
    :param int ddof: `ddof=1` will return unbiased estimate since in the covariance formula,
        we divide by sample_size-ddof.

    :returns:
        - `covariances`: sample-based covariance matrix
        - `pseudo_covariances`: sample-based pseudo covariance matrix
    """
    # Extract sample size and probability space dimension
    sample = np.asarray(sample).astype(np.cdouble)
    if np.ndim(sample) == 1:
        np.reshape(sample, (sample.size, 1))
    sample_size, dimension = sample.shape

    # Mean and Innovations (x-mu)
    mean = np.mean(
        sample,
        axis=0,
    )
    innovations = sample.copy()
    for i in range(sample_size):
        innovations[i, :] -= mean

    # Covariances and Pseudo Covariances (Relations)
    covariances = innovations.T.dot(np.conjugate(innovations)).T / (sample_size - ddof)
    pseudo_covariances = innovations.T.dot(innovations) / (sample_size - ddof)

    return covariances, pseudo_covariances


def create_complex_covariance_matrices(size=2, stdev=None, random_seed=None):
    """
    construct numpy covariance matrices of dimension (size x size) for
    complex random variables/vectors, with/without correlations and/or
    cross-correlations between real and imaginary parts.

    This function calculates covariances and pseudo covariances by sampling
    real and imaginary parts, and then calculate covariances from the sample

    The passed standard deviations `stdev` are not exactly the same as
    square root of the variances; they are just used to sample real and
    imaginary parts which are then aggregated.
    """
    if size < 1:
        raise ValueError("covariance size must be at least 1")

    # Create a random number generator instance from the passed seed
    rng = np.random.default_rng(random_seed)

    if stdev is not None:

        # Check stdev
        stdev = np.array(stdev).flatten()
        assert 0 <= stdev.min(), "standard deviations must nonnegative!"

        if 1 == stdev.size < size:
            stdev = np.ones(size * 2) * stdev
        elif stdev.size == size * 2:
            pass
        elif stdev.size == size:
            stdev = np.concatenate(stdev, stdev).flatten()
        else:
            raise ValueError("size mismatches length of passed standard deviations")

    else:
        stdev = 1e-5 + rng.random(2 * size)  # 0<1e-5<= standard deviations <= 1

    # Smaple and calculate covariances
    sample_size = 2 * size * 10
    sample = rng.standard_normal((2 * size, sample_size))
    for j in range(2 * size):
        sample[:, j] *= stdev
    sample = sample[:size, :] + j * sample[size:, :]

    # TODO: consider controlling covariances/correlations
    return complex_sample_covariance(sample.T)


def real_to_complex_covariances(
    C_RR,
    C_IR,
    C_RI,
    C_II,
):
    """
    Convert covariances and cross covariances or real and imaginary parts to
    covariances and pseudo-covariances (relations) of complex random variable/vector

    :param C_RR: covariances of the real part of a complex random vector/variable
    :param C_IR: cross-covariances of the real with the imaginary part of a complex random vector/variable
    :param C_RI: cross-covariances of the imaginary with the real part of a complex random vector/variable
    :param C_II: covariances of the imaginary part of a complex random vector/variable

    :returns:
        - K: the covariances of the complex variable
        - J: the pseudo covariances
    """
    C_RR = asarray(C_RR)
    C_II = asarray(C_II)
    C_RI = asarray(C_RI)
    C_IR = asarray(C_IR)

    assert (
        C_RR.shape == C_IR.shape == C_RI.shape == C_II.shape
    ), "Arrays must be of the same shape!"
    shape = C_RR.shape
    assert (
        np.ndim(C_RR) == 2 and shape[0] == shape[1]
    ), "Covariance arrays/matrices must be square of two dimensions"

    K = C_RR + C_II + 1j * (C_IR - C_RI)
    J = C_RR - C_II + 1j * (C_IR + C_RI)
    return K, J


def complex_to_real_covariances(
    K,
    J,
    ignore_design=False,
):
    """
    Convert covariances and cross covariances or real and imaginary parts to
    covariances and pseudo-covariances (relations) of a complex random variable/vector

    :param K: the covariances of the complex variable
    :param J: the pseudo covariances
    :param bool ignore_design: if `False` the internal design is ignored, otherwise, the matrices
        are projected on the space spanned by the active entries of the design.

    :returns:
        - C_RR: covariances of the real part of a complex random vector/variable
        - C_IR: cross-covariances of the real with the imaginary part of a complex random vector/variable
        - C_RI: cross-covariances of the imaginary with the real part of a complex random vector/variable
        - C_II: covariances of the imaginary part of a complex random vector/variable

    """
    assert K.shape == J.shape, "Arrays must be of the same shape!"
    shape = K.shape
    assert (
        np.ndim(K) == 2 and shape[0] == shape[1]
    ), "Covariance arrays/matrices must be square of two dimensions"

    C_RR = 0.5 * (np.real(K) + np.real(J))
    C_II = 0.5 * (np.real(K) - np.real(J))

    C_IR = 0.5 * (np.imag(J) + np.imag(K))
    C_RI = 0.5 * (np.imag(J) - np.imag(K))

    return C_RR, C_IR, C_RI, C_II


# Statistical Error:
# ------------------
def calculate_rmse(first, second, ignore_nan=True):
    """
    Calculate the root mean squared error between two vectors of the same type.

    :param first: first array/vector
    :param second: second array/vector
    :param ignore_nan: if `True` np.nan values are ignored from the comparison and counting, otherwise any nan value in the difference will result in nan accounted for.

    :returns: rmse: the corresponding root mean squared error

    """
    if not isinstance(first, type(second)):
        print("in Utility.calculate_rmse: Type of first entry: %s " % type(first))
        print("in Utility.calculate_rmse: Type of second entry: %s " % type(second))
        raise TypeError(
            " The two vectors must be of the same type!First: %s\nSecond: %s"
            % (repr(type(first)), repr(type(first)))
        )
    first = np.array(first).flatten()
    second = np.array(second).flatten()
    if first.size != second.size:
        print("Vectors must be of equal size!")
        raise TypeError

    diff = np.absolute(first - second)
    if ignore_nan:
        diff = diff[~np.isnan(diff)]
    err = np.sum(diff**2)
    rmse = np.sqrt(err) / np.sqrt(diff.size)
    return rmse


# Optimal Design of Experiments (OED):
# ------------------------------------
def calculate_oed_criterion(post_cov, oed_criterion="A"):
    """
    Given a  matrix post_cov, calculate (exactly) the optimality criterion

    :param post_cov: the posterior covariance matrix, out of which optimality criterion is evaluated
    :param oed_criterion: 'A' for A-optimality (Trace(post_cov)), 'D' for D-optimality (log-det (post_cov))

    :returns: oed_objective: \n
        - Trace(post_cov) if oed_criterion is set to 'A',
        - log-det(post_cov) if oed_criterion is set to 'D'

    """
    assert np.ndim(post_cov) == 2, "Wrong size; two-dimensional matrix expected"
    assert post_cov.shape[0] == post_cov.shape[1], "Square matrix is expected"

    # Evaluate the OED criterion  value
    if re.match(r"\AA( |_|-)*(opt)*", oed_criterion, re.IGNORECASE):
        oed_objective = np.trace(post_cov)
    elif re.match(r"\AD( |_|-)*(opt)*", oed_criterion, re.IGNORECASE):
        oed_objective = np.log(np.linalg.det(post_cov))
    else:
        print("Unrecognized optimality criterion  %s" % oed_criterion)
        raise ValueError

    return oed_objective
