# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
Test math utility functions pyoed.utility.math.*
"""

import math
import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import LinearOperator
from itertools import product

from pyoed import utility
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    create_random_number_generator,
)

import pytest

pytestmark = pytest.mark.utility


# Fixtures:
# =========
def create_random_matrix(
    shape, symmetric=False, random_seed=SETTINGS.RANDOM_SEED or 1011
):
    """Create 2-dimensional random matrix of shape specified by `shape`"""

    # Create random number generator
    rng = np.random.default_rng(random_seed)

    assert 1 <= len(shape) <= 2, "This function is developed for 1/2D arrays only"

    _shape = tuple(np.asarray(shape).flatten().tolist())
    A = rng.random(shape)

    if symmetric:
        assert len(shape) == 2, "Symmetry is supported for 2D arrays only"
        assert all(
            [n == shape[0] for n in shape]
        ), "Matrix dimensions must all be equal to enforce symmetry"
        A = np.tril(A) + np.tril(A, -1).T

    return A


def create_function_and_gradient(n, random_seed=SETTINGS.RANDOM_SEED or 1011):
    """
    Create and return dummy function :math:`f(x)` along with its gradient, where
    :math:`x` is an :math:`n` dimensional vector/variable.

    The function takes the simple form: :math:`fun(x) := x^T A x + 1^T x^2`, thus the
    gradient is :math:`grad(x):= 2 A x + 2 x` where :math:`1` is a vector of ones and
    :math:`A` is a randomly generated matrix (using numpy.random.rand) with seed set
    based on `random_seed` for reproducibility.
    """
    A = create_random_matrix((n, n), random_seed=random_seed)
    # create the function 'fun' and gradient 'grad' (more complex functions should be similar)
    fun = lambda x: np.dot(x, A.dot(x))  # + np.dot (x, np.power(x, 2))
    grad = lambda x: A.T.dot(x) + A.dot(x)  # + 3.0 * np.power(x, 2)
    return fun, grad


# Tests:
# ======
@pytest.mark.parametrize("fd_central", [False, True])
def test_math_validate_function_gradient(fd_central):
    """pyoed.utility.math.validate_function_gradient"""

    # create random number generator
    rng = np.random.default_rng(SETTINGS.RANDOM_SEED or 1011)

    n = 10
    scl = 1.0
    x = rng.standard_normal(n) * scl
    fd_eps = 1e-8
    fun, grad = create_function_and_gradient(n=n)
    g = grad(x)
    fd_grad, fd_err = utility.math.validate_function_gradient(
        fun=fun, state=x, gradient=g, fd_eps=fd_eps, fd_central=fd_central
    )
    assert np.linalg.norm(fd_err, ord=np.inf) < np.sqrt(fd_eps)


@pytest.mark.parametrize("fd_central", [False, True])
def test_math_finite_differences_gradient(fd_central):
    """pyoed.utility.math.finite_differences_gradient"""

    # create random number generator
    rng = np.random.default_rng(SETTINGS.RANDOM_SEED or 1011)

    n = 10
    scl = 1.0
    x = rng.standard_normal(n) * scl
    fd_eps = 1e-8
    fun, grad = create_function_and_gradient(n=n)
    g = grad(x)
    fd_grad = utility.math.finite_differences_gradient(
        fun=fun, state=x, fd_eps=fd_eps, fd_central=fd_central
    )
    fd_err = np.abs(fd_grad - g) / g
    assert np.linalg.norm(fd_err, ord=np.inf) < np.sqrt(fd_eps)


@pytest.mark.parametrize("n, m", product([10], [10]))
def test_math_asarray(n, m):
    """pyoed.utility.math.asarray"""
    scl = 10
    rtol = 1e-5

    # matrix shape
    shape = (n, m)

    # Test evaluating trace of a Numpy array
    np_A = scl * create_random_matrix(shape, symmetric=False) + 1e-5

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A)

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_A = utility.math.asarray(A)
        assert_allclose(eval_A, np_A, rtol=rtol) and not (eval_A is np_A)


@pytest.mark.parametrize("n, m", product([10], [10]))
def test_math_matrix_diagonal(n, m):
    """pyoed.utility.math.matrix_diagonal"""
    scl = 10
    rtol = 1e-5

    # matrix shape
    shape = (n, m)

    # Test evaluating trace of a Numpy array
    np_A = scl * create_random_matrix(shape, symmetric=(n == m)) + 1e-5
    exact_diag = np.diagonal(np_A)

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A)

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_diag = utility.math.matrix_diagonal(A, size=m)
        assert_allclose(eval_diag, exact_diag, rtol=rtol)


@pytest.mark.parametrize("n, m", product([10], [10]))
def test_math_matrix_trace(n, m):
    """pyoed.utility.math.matrix_trace"""
    scl = 10
    rtol = 1e-5
    rnd_err_lvl = 1e-1  # 10% of the trace value is allowed

    # matrix shape
    shape = (n, m)

    # Test evaluating trace of a Numpy array
    np_A = scl * create_random_matrix(shape, symmetric=(n == m)) + 1e-5
    d = np.diagonal(np_A)
    exact_trace = np.sum(d)

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A)

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_trace = utility.math.matrix_trace(A, method="exact", size=m)
        assert np.absolute((exact_trace - eval_trace) / exact_trace) < rtol
        if n == m:
            rnd_traces = []
            for i in range(100):
                # NOTE: randomization might require other validation approach
                rnd_trace = utility.math.matrix_trace(
                    A,
                    method="randomized",
                    size=n,
                    random_seed=(SETTINGS.RANDOM_SEED or 1011) + i,
                    sample_size=1000,
                    distribution="gaussian",
                )
                rnd_traces.append(rnd_trace)
            rnd_trace_mean = np.mean(rnd_traces)
            assert np.abs(exact_trace - rnd_trace_mean) < rnd_err_lvl * exact_trace


@pytest.mark.parametrize("n", [10])
def test_math_matrix_logdet(n):
    """pyoed.utility.math.matrix_logdet"""
    scl = 10
    rtol = 1e-5
    rnd_err_lvl = 5e-2  # 5% of the logdet value is allowed

    # matrix shape
    shape = (n, n)

    # Test evaluating logdet of a Numpy array
    np_A = scl * create_random_matrix(shape, symmetric=True) + 1e-5
    np_A = np_A.T @ np_A
    _, exact_logdet = np.linalg.slogdet(np_A)

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A)

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_logdet = utility.math.matrix_logdet(A, method="exact", size=n)
        assert np.absolute((exact_logdet - eval_logdet) / exact_logdet) < rtol

        rnd_logdets = []
        for i in range(25):
            # NOTE: randomization might require other validation approach
            rnd_logdet = utility.math.matrix_logdet(
                A,
                method="randomized",
                size=n,
                random_seed=(SETTINGS.RANDOM_SEED or 1011) + i,
            )
            rnd_logdets.append(rnd_logdet)
        rnd_logdet_mean = np.mean(rnd_logdets)
        assert np.abs(exact_logdet - rnd_logdet_mean) < rnd_err_lvl * exact_logdet


@pytest.mark.parametrize("n, m", product([10], [10]))
def test_math_matrix_vecmult(n, m):
    """pyoed.utility.math.matrix_vecmult"""

    # create random number generator
    rng = np.random.default_rng(SETTINGS.RANDOM_SEED or 1011)

    scl = 10
    rtol = 1e-5

    # matrix shape
    shape = (n, m)
    x = rng.standard_normal(m) * scl

    # Test evaluating trace of a Numpy array
    np_A = scl * create_random_matrix(shape, symmetric=(n == m)) + 1e-5
    exact_matvec = np.dot(np_A, x)

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A)

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_matvec = utility.math.matrix_vecmult(A, x)
        assert_allclose(eval_matvec, exact_matvec, rtol=rtol)


@pytest.mark.parametrize(
    "zero_on_diag, zero_all, n",
    product(
        [False, True],
        [False, True],
        [2, 4],
    ),
)
def test_math_factorize_spsd_matrix(zero_on_diag, zero_all, n):
    """pyoed.utility.math.factorize_spsd_matrix"""

    # create random number generator
    rng = np.random.default_rng(SETTINGS.RANDOM_SEED or 1011)

    scl = 10
    rtol = 1e-5

    shape = (n, n)

    # Create a random array of shape (n, n), and extract lower triangle part (set upper to zero)
    if zero_all:
        np_A_L = np.zeros(shape)
    else:
        np_A_L = scl * create_random_matrix(shape, symmetric=False)
        for i in range(n):
            np_A_L[i, i + 1 :] = 0.0

        # Randomly choose a row and set it to zero (SPSD)
        if zero_on_diag:
            if np.count_nonzero(np.diag(np.dot(np_A_L, np_A_L.T))) == n:
                rc = rng.choice(n)
                np_A_L[rc, :] = 0.0
                np_A_L[:, rc] = 0.0

    # L LT
    np_A = np.dot(np_A_L, np_A_L.T)
    exact_L = np_A_L

    # Sparse version of A
    sp_A = sp.csc_matrix(np_A, shape=(n, n))

    # LinearOperator version of A
    matvec = lambda x: np.dot(np_A, x)
    lo_A = LinearOperator(shape, matvec=matvec)

    for A in [np_A, sp_A, lo_A]:
        eval_L = utility.math.factorize_spsd_matrix(A, lower=True)
        assert_allclose(utility.math.asarray(eval_L), exact_L, rtol=rtol)

        eval_L = utility.math.factorize_spsd_matrix(A, lower=False)
        assert_allclose(utility.math.asarray(eval_L), exact_L.T, rtol=rtol)


def test_math_validate_inverse_problem_derivatives():
    """pyoed.utility.math.validate_inverse_problem_derivatives"""
    # TODO
    ...


def test_math_inverse_problem_linearity_test():
    """pyoed.utility.math.inverse_problem_linearity_test"""
    # TODO: Test for a linear inverse problem
    ...
    # TODO: Test for a nonlinear inverse problem (either or both model and observation operator are nonlinear)
    ...


def test_math_truncate():
    """pyoed.utility.math.truncate"""

    # create random number generator
    rng = np.random.default_rng(SETTINGS.RANDOM_SEED or 1011)

    lb = -3
    ub = 20

    # list/array
    x = rng.standard_normal(100) * 100
    t_x = utility.math.truncate([l for l in x], lb=lb, ub=ub)
    assert isinstance(
        t_x, np.ndarray
    ), f"returned type expected to by numpy array; found {type(t_x)}"
    assert (
        t_x.size == x.size
    ), f"Incorrect size of the returned array; expected {x.size}, returned {t_x.size}"
    x[x < lb] = lb
    x[x > ub] = ub
    assert_allclose(t_x, x, rtol=0)

    # scalar
    x = rng.standard_normal() * 100
    t_x = utility.math.truncate(x, lb=lb, ub=ub)
    assert isinstance(
        t_x, np.ndarray
    ), f"returned type expected to by numpy array; found {type(t_x)}"
    assert (
        t_x.size == 1
    ), f"Incorrect size of the returned array; expected 1, returned {t_x.size}"
    if x < lb:
        x = lb
    if x > ub:
        x = ub
    assert_allclose(t_x, x, rtol=0)


def test_math_remainder():
    """pyoed.utility.math.remainder"""
    # TODO
    ...


@pytest.mark.parametrize("n, r", product([10, 6], [8, 6, 1, 0]))
def test_math_ncr(n, r):
    """pyoed.utility.math.ncr"""
    if n >= r >= 0:
        res = utility.math.ncr(n, r)
        exp_res = math.factorial(n) / math.factorial(r) / math.factorial(n - r)
        assert (
            res == exp_res
        ), "Invalid results in nCr; {n}C{4}={exp_res}; received {res}"
    else:
        with pytest.raises(ValueError):
            utility.math.ncr(n, r)


def test_math_enumerate_binary():
    """pyoed.utility.math.enumerate_binary"""
    # TODO
    ...


def test_math_index_to_binary_state():
    """pyoed.utility.math.index_to_binary_state"""
    # TODO
    ...


def test_math_index_from_binary_state():
    """pyoed.utility.math.index_from_binary_state"""
    # TODO
    ...


def test_math_threshold_relaxed_design():
    """pyoed.utility.math.threshold_relaxed_design"""
    # TODO
    ...


def test_math_group_by_num_active():
    """pyoed.utility.math.group_by_num_active"""
    # TODO
    ...
