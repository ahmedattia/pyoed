# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
General tests for the utility modules*
"""

import inspect
import pytest

from pyoed import utility

# Mark the test
pytestmark = pytest.mark.utility


# Fixtures
# ========
class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


# Compatibility tests
# ===================
module_name_test = lambda name: inspect.ismodule(name) and "test" not in str(name)


@pytest.mark.parametrize(
    "module", [v[1] for v in inspect.getmembers(utility, module_name_test)]
)
def test__all__(module):
    """
    Test that the __all__ variable is properly configured with all functions
    added for each module
    """
    # Retrieve the function names added to `__all__`
    modeled__all__ = set(module.__all__)

    # Inspect all functions and classes actually defined in the model (retrieve only names)
    predicate = lambda x: inspect.isclass(x) or inspect.isfunction(x)
    inspected__all__ = set(
        [
            f[0] for f in inspect.getmembers(
                module,
                predicate=predicate,
            )
        ]
    )

    # Compare/Assert
    assert_val = modeled__all__ == inspected__all__
    if not assert_val:

        modeled_not_inspected = set.difference(modeled__all__, inspected__all__)
        inspected_not_modeled = set.difference(inspected__all__, modeled__all__)

        err_msg = f"\nThe '__all__' variable is NOT properly configuered in the module '{module}'"
        err_msg += f"\n**Modeled in '__all__':"
        for f in modeled__all__:
            err_msg += f"\n {bcolors.WARNING} {f}{bcolors.ENDC}"

        err_msg += f"\n\n**Inspected in the module {module}:"
        for f in inspected__all__:
            err_msg += f"\n  {bcolors.OKGREEN} {f}{bcolors.ENDC}"

        err_msg += f"\n\n>>>>Inspected but NOT Modeled:"
        for f in set.difference(inspected__all__, modeled__all__):
            err_msg += f"\n  {bcolors.OKGREEN} {f}{bcolors.ENDC}"

        err_msg += f"\n\n>>>>Modeled but NOT Inspected:"
        for f in set.difference(modeled__all__, inspected__all__):
            err_msg += f"\n  {bcolors.FAIL} {f}{bcolors.ENDC}"

    else:
        # This should never be used
        err_msg = "Clear"
    assert assert_val, err_msg

