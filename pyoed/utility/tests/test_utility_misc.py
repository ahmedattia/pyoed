# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test misc utility functions pyoed.pyoed.utility.misc.*
"""

import numpy as np
from pyoed.utility import misc
import pytest
from pyoed.tests import (
    assert_allequal,
    create_random_number_generator,
)

pytestmark = pytest.mark.utility

# Create local random number generator
_RNG = create_random_number_generator()


def test_misc_isnumber():
    """pyoed.pyoed.utility.misc.isnumber"""

    # Numbers
    n = 50
    scl = 100
    numbers = _RNG.standard_normal(n) * scl
    numbers[0] = np.nan
    numbers[-1] = np.inf
    assert all([misc.isnumber(x) for x in numbers])

    # Strings
    _strings = ["1", "x", "ax", "12.x"]
    strings = [s for s in _strings]
    strings += ["%s" % s for s in strings]
    strings += [f"%s" % s for s in strings]
    assert not any([misc.isnumber(s) for s in strings])

    # Iterables: list, sets, dicts, np arrays,
    assert not any(misc.isnumber(l) for l in [[], [1], ["1"]])
    assert not any(misc.isnumber(l) for l in [set([]), set([1]), set(["1"])])
    assert not any(misc.isnumber(l) for l in [{}, {1: 1}, {"1": 1}])
    assert not any(
        misc.isnumber(l) for l in [_RNG.random(1), _RNG.random(4), _RNG.random((4, 3))]
    )

    # Others:
    # TODO: Add more as we go...


def test_misc_isstring():
    """pyoed.pyoed.utility.misc.isstring"""

    # Numbers
    n = 50
    scl = 100
    numbers = _RNG.standard_normal(n) * scl
    numbers[0] = np.nan
    numbers[-1] = np.inf
    assert not any([misc.isstring(x) for x in numbers])

    # Strings
    _strings = ["1", "x", "ax", "12.x"]
    strings = [s for s in _strings]
    strings += ["%s" % s for s in strings]
    strings += [f"%s" % s for s in strings]
    assert all([misc.isstring(s) for s in strings])

    # Iterables: list, sets, dicts, np arrays,
    assert not any(misc.isstring(l) for l in [[], [1], ["1"]])
    assert not any(misc.isstring(l) for l in [set([]), set([1]), set(["1"])])
    assert not any(misc.isstring(l) for l in [{}, {1: 1}, {"1": 1}])
    assert not any(
        misc.isstring(l) for l in [_RNG.random(1), _RNG.random(4), _RNG.random((4, 3))]
    )


def test_misc_isiterable():
    """pyoed.pyoed.utility.misc.isiterable"""
    ...
    # Numbers
    n = 50
    scl = 100
    numbers = _RNG.standard_normal(n) * scl
    numbers[0] = np.nan
    numbers[-1] = np.inf
    assert not any([misc.isiterable(x) for x in numbers])

    # Strings
    _strings = ["1", "x", "ax", "12.x"]
    strings = [s for s in _strings]
    strings += ["%s" % s for s in strings]
    strings += [f"%s" % s for s in strings]
    assert all([misc.isiterable(s) for s in strings])
    assert misc.isiterable("".join(strings))

    # Iterables: list, sets, dicts, np arrays,
    assert misc.isiterable(strings)
    assert misc.isiterable(numbers)
    assert all(misc.isiterable(l) for l in [[], [1], ["1"]])
    assert all(misc.isiterable(l) for l in [set([]), set([1]), set(["1"])])
    assert all(misc.isiterable(l) for l in [{}, {1: 1}, {"1": 1}])
    assert all(
        misc.isiterable(l)
        for l in [_RNG.random(1), _RNG.random(4), _RNG.random((4, 3))]
    )


def test_misc_aggregate():
    """pyoed.pyoed.utility.misc.aggregate"""
    r = (_RNG.random(12),)
    d0 = dict()
    d1 = {
        1: 10,
        2: 18.12,
        3: "str",
        4: r,
        5: (np.inf, np.nan, 13, "x"),
        "a": [np.inf, np.nan, 13, "x"],
        "b": {"a": np.inf, "b": np.nan, "c": 13, "d": {}},
        "c": {"a": None, "b": {}},
    }
    d3 = {
        1: 10,
        2: 18.12,
        3: "str",
        4: r,
        5: (np.inf, np.nan, 13, "x"),
        "a": [np.inf, np.nan, 13, "x"],
    }
    d4 = {
        1: r,
        "a": [np.inf, np.nan, 13, "x"],
        "b": {"a": np.inf, "b": np.nan, "c": 13, "d": {}},
        "c": {"a": None, "b": {}},
    }

    def _equal_dicts(d1, d2):
        return all([v1 == v2 for v1, v2 in zip(d1, d2)])

    # test aggregation in place
    configs = d3.copy()
    def_configs = d1.copy()
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=True)
    assert _equal_dicts(result, def_configs) and (result is configs)

    # test aggregation with copying
    configs = d3.copy()
    def_configs = d1.copy()
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=False)
    assert _equal_dicts(result, def_configs) and not (
        result is configs or result is def_configs
    )

    # test aggregation of dict with None
    configs = None
    def_configs = d1.copy()
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=True)
    assert _equal_dicts(result, def_configs) and not (
        result is configs or result is def_configs
    )
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=False)
    assert _equal_dicts(result, def_configs) and not (
        result is configs or result is def_configs
    )

    # test aggregation of dict with empty dictionary
    configs = d0.copy()
    def_configs = d1.copy()
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=True)
    assert _equal_dicts(result, def_configs) and (result is configs)

    configs = d0.copy()
    def_configs = d1.copy()
    result = misc.aggregate(configs=configs, def_configs=def_configs, in_place=False)
    assert _equal_dicts(result, def_configs) and not (
        configs is result or def_configs is result
    )

    # test aggregation of different dictionaries
    result = misc.aggregate(configs=d3, def_configs=d4, in_place=False)
    assert _equal_dicts(result, d1)
    result = misc.aggregate(configs=d4, def_configs=d3, in_place=False)
    assert not _equal_dicts(result, d1)


@pytest.mark.skip(reason="Not implemented")
def test_misc_configs_to_dataclass():
    """pyoed.pyoed.utility.misc.configs_to_dataclass"""
    # TODO
    ...


def test_misc_print_configs():
    """pyoed.pyoed.utility.misc.print_configs"""
    # TODO
    ...


def test_misc_path_is_accessible():
    """pyoed.pyoed.utility.misc.path_is_writable"""
    ...


def test_misc_get_list_of_files():
    """pyoed.pyoed.utility.misc.get_list_of_files"""
    # TODO
    ...


def test_misc_get_list_of_subdirectories():
    """pyoed.pyoed.utility.misc.get_list_of_subdirectories"""
    # TODO
    ...


def test_misc_try_file_name():
    """pyoed.pyoed.utility.misc.try_file_name"""
    # TODO
    ...


def test_misc_validate_Cartesian_grid():
    """
    pyoed.pyoed.utility.misc.validate_Cartesian_grid
    """
    # WIP
    # Parameters are: grid, points_as_rows=True, points_ndim_test=True

    # test 1D grids:
    # ~~~~~~~~~~~~~~
    num_gridpoints = 10

    for num_prog_vars in [1, 3]:
        for points_as_rows in [True, False]:
            # adjacent gridpoints
            reference_grid = list(range(num_gridpoints)) * num_prog_vars
            reference_indexes = [
                np.arange(i * num_gridpoints, (i + 1) * num_gridpoints)
                for i in range(num_prog_vars)
            ]
            reference_shape = (
                (len(reference_grid), 1) if points_as_rows else (1, len(reference_grid))
            )

            out_grid, out_indexes = misc.validate_Cartesian_grid(
                reference_grid, points_as_rows=points_as_rows
            )
            assert isinstance(out_grid, np.ndarray)
            assert out_grid.shape == reference_shape
            assert len(out_indexes) == num_prog_vars
            for i, inds in enumerate(out_indexes):
                assert isinstance(inds, np.ndarray)
                assert_allequal(inds, reference_indexes[i])

            # non-adjacent gridpoints
            # TODO

    # test 2-3D grids:
    # ~~~~~~~~~~~~~~~~
    # TODO


def test_misc_gridpoint_to_index():
    """pyoed.pyoed.utility.misc.gridpoint_to_index"""

    # 1d grid
    grid_1d = np.array([1, 3, 3, 4, 5.5, 5.5, 6, 8])
    assert misc.gridpoint_to_index(1, grid_1d, return_all=False) == 0
    assert misc.gridpoint_to_index([6], grid_1d, return_all=False) == 6
    assert misc.gridpoint_to_index(8, grid_1d, return_all=False) == 7
    assert misc.gridpoint_to_index(np.array(5.5), grid_1d, return_all=False) == 4
    assert misc.gridpoint_to_index(12, grid_1d, return_all=True) is None
    assert misc.gridpoint_to_index(12, grid_1d, return_all=False) is None
    assert_allequal(misc.gridpoint_to_index(3, grid_1d, return_all=True), [1, 2])
    with pytest.raises(Exception) as e_info:
        misc.gridpoint_to_index([1, 3], grid_1d)
    assert e_info.type is AssertionError

    # 2d grid
    grid_2d = np.array(
        [
            [1, 3, 3, 4, 5.5, 5.5, 6, 8],
            [1, 3, 3, 4, 5.5, 5.8, 6, 8],
        ]
    ).T
    assert misc.gridpoint_to_index([1, 1], grid_2d, return_all=False) == 0
    assert misc.gridpoint_to_index([6, 6], grid_2d, return_all=False) == 6
    assert misc.gridpoint_to_index((8, 8), grid_2d, return_all=False) == 7
    res = misc.gridpoint_to_index((8, 8), grid_2d, return_all=True)
    assert isinstance(res, np.ndarray) and res.size == 1 and res[0] == 7
    assert misc.gridpoint_to_index(np.array([5.5, 5.8]), grid_2d, return_all=False) == 5
    assert misc.gridpoint_to_index([1, 12], grid_2d, return_all=True) is None
    assert misc.gridpoint_to_index([1, 12], grid_2d, return_all=False) is None
    assert_allequal(misc.gridpoint_to_index([3, 3], grid_2d, return_all=True), [1, 2])
    for n in [1, 3]:
        with pytest.raises(Exception) as e_info:
            misc.gridpoint_to_index(_RNG.random(n), grid_2d)
        assert e_info.type is AssertionError

    # 3d grid
    grid_3d = np.array(
        [
            [1, 3, 3, 4, 5.5, 5.5, 6, 8],
            [1, 3, 3, 4, 5.5, 5.8, 6, 8],
            [2, 3, 3, 5, 6.5, 6.8, 7, 9],
        ]
    ).T

    assert misc.gridpoint_to_index([1, 1, 2], grid_3d, return_all=False) == 0
    assert misc.gridpoint_to_index([6, 6, 7], grid_3d, return_all=False) == 6
    assert misc.gridpoint_to_index((8, 8, 9), grid_3d, return_all=False) == 7
    res = misc.gridpoint_to_index((8, 8, 9), grid_3d, return_all=True)
    assert isinstance(res, np.ndarray) and res.size == 1 and res[0] == 7
    assert (
        misc.gridpoint_to_index(np.array([5.5, 5.8, 6.8]), grid_3d, return_all=False)
        == 5
    )
    assert misc.gridpoint_to_index([1, 1, 12], grid_3d, return_all=True) is None
    assert misc.gridpoint_to_index([1, 1, 12], grid_3d, return_all=False) is None
    assert_allequal(
        misc.gridpoint_to_index([3, 3, 3], grid_3d, return_all=True), [1, 2]
    )

    for n in [1, 2, 4]:
        with pytest.raises(Exception) as e_info:
            misc.gridpoint_to_index(_RNG.random(n), grid_3d)
        assert e_info.type is AssertionError
