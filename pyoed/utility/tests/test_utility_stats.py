# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Test stats utility functions pyoed.pyoed.utility.stats.*
"""
import numpy as np
from pyoed import utility
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    create_random_number_generator,
)
import pytest

pytestmark = pytest.mark.utility


# Create local random number generator
_RNG = create_random_number_generator()


# Fixtures:
# =========
def create_random_matrix(
    shape, symmetric=False, random_seed=SETTINGS.RANDOM_SEED or 1011
):
    """Create 2-dimensional random matrix of shape specified by `shape`"""

    assert 1 <= len(shape) <= 2, "This function is developed for 1/2D arrays only"

    # Create random number generator
    rng = np.random.default_rng(random_seed)

    _shape = tuple(np.asarray(shape).flatten().tolist())
    A = rng.random(shape)

    if symmetric:
        assert len(shape) == 2, "Symmetry is supported for 2D arrays only"
        assert all(
            [n == shape[0] for n in shape]
        ), "Matrix dimensions must all be equal to enforce symmetry"
        A = np.tril(A) + np.tril(A, -1).T

    return A


def test_stats_sample_standard_Normal():
    """pyoed.pyoed.utility.stats.sample_standard_Normal"""
    # TODO
    ...


def test_stats_sample_Rademacher():
    """pyoed.pyoed.utility.stats.sample_Rademacher"""
    # TODO
    ...


def test_stats_trace_estimator():
    """pyoed.pyoed.utility.stats.trace_estimator"""
    # TODO
    ...


@pytest.mark.parametrize("n", [10])
def test_stats_logdet_estimator(n):
    """pyoed.pyoed.utility.stats.logdet_estimator"""
    scl = 10
    rnd_err_lvl = 5e-2  # 5% of the true value is allowed

    shape = (n, n)

    A = scl * create_random_matrix(shape, symmetric=True) + 1e-5
    A = A.T @ A
    _, exact_logdet = np.linalg.slogdet(A)

    from scipy.sparse.linalg import aslinearoperator

    A = aslinearoperator(A)

    rnd_logdets = []
    for i in range(25):
        converged, rnd_logdet, _, _ = utility.stats.logdet_estimator(
            A,
            n,
            random_seed=(SETTINGS.RANDOM_SEED or 10) * (i + 1),
        )
        assert converged
        rnd_logdets.append(rnd_logdet)
    rnd_logdet_mean = np.mean(rnd_logdets)
    assert np.abs(exact_logdet - rnd_logdet_mean) < rnd_err_lvl * exact_logdet


@pytest.mark.parametrize("n", [100, 1000])
@pytest.mark.parametrize("m", [100, 1000])
def test_stats_rsvd(n, m):
    """pyoed.pyoed.utility.stats.rsvd"""
    rnd_err_lvl = 1e-3

    rank = min(n, m) // 10
    A = create_random_matrix((n, rank))
    B = np.eye(rank)
    C = create_random_matrix((rank, m))
    D = A @ B @ C

    from scipy.sparse.linalg import aslinearoperator

    D_lo = aslinearoperator(D)

    rnd_U, rnd_S, rnd_Vh = utility.stats.rsvd(D_lo, rank)
    err = np.linalg.norm(D - rnd_U @ (np.diag(rnd_S) @ rnd_Vh)) / np.linalg.norm(D)
    assert err < rnd_err_lvl


def test_stats_calculate_rmse():
    """pyoed.pyoed.utility.stats.calculate_rmse"""
    # TODO
    ...


# This function `factorize_covariances` has been removed
# def test_stats_factorize_covariances():
#     """pyoed.pyoed.utility.stats.factorize_covariances"""
#     # TODO
#     ...


# TODO: This ought to be changed to use pytest.mark.parametrize
def test_stats_create_covariance_matrix():
    """
    pyoed.pyoed.utility.stats.create_covariance_matrix
    >>>
    """
    # Create proper matrix and test for symmetric positive definiteness (with and without stdev)
    for random_seed in [None, SETTINGS.RANDOM_SEED or 1011]:
        for size in [2, 10]:
            for stdev in [None, 4, np.round(_RNG.random(size), 1)]:
                for corr in [False, True]:
                    for return_factor in [False, True]:
                        # call twice and make sure they match if random seed is not None
                        result = utility.stats.create_covariance_matrix(
                            size=size,
                            corr=corr,
                            stdev=stdev,
                            return_factor=return_factor,
                            random_seed=random_seed,
                        )
                        # test type and shape
                        assert isinstance(result, np.ndarray)
                        assert result.shape == (size, size)

                        # test regeneration with the same random seed
                        if random_seed is not None:
                            result_2 = utility.stats.create_covariance_matrix(
                                size=size,
                                corr=corr,
                                stdev=stdev,
                                return_factor=return_factor,
                                random_seed=random_seed,
                            )
                            assert_allclose(result_2, result)

                        # Test everything about result (covariance matrix or its Cholesky factor)
                        # test SPSD by calling the Cholesky factorization in math utility module
                        if return_factor:
                            Chol_fac = result
                            cov = np.dot(Chol_fac, Chol_fac.T)
                        else:
                            cov = result
                            Chol_fac = utility.math.factorize_spsd_matrix(
                                cov, lower=True
                            )

                        # Variances (if provided)
                        if stdev is not None and not corr:
                            assert_allclose(np.diag(cov), np.power(stdev, 2))

                        # Correlations
                        if corr:
                            nnz_vars = np.count_nonzero(np.diag(cov))
                            if nnz_vars > 1:
                                assert np.count_nonzero(cov) >= nnz_vars + 2
                            else:
                                assert np.count_nonzero(cov) == nnz_vars


def test_stats_posterior_covariance_matrix():
    """pyoed.pyoed.utility.stats.posterior_covariance_matrix"""
    # TODO
    ...


def test_stats_create_complex_covariance_matrices():
    """pyoed.pyoed.utility.stats.create_complex_covariance_matrices"""
    # TODO
    ...


def test_stats_real_to_complex_covariances():
    """pyoed.pyoed.utility.stats.real_to_complex_covariances"""
    # TODO
    ...


def test_stats_complex_to_real_covariances():
    """pyoed.pyoed.utility.stats.complex_to_real_covariances"""
    # TODO
    ...


def test_stats_complex_sample_covariance():
    """pyoed.pyoed.utility.stats.complex_sample_covariance"""
    # TODO
    ...


def test_stats_calculate_oed_criterion():
    """pyoed.pyoed.utility.stats.calculate_oed_criterion"""
    # TODO
    ...
