# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved
"""
Test math utility functions pyoed.utility.math.*
"""
import numpy as np
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.tests import (
    assert_allequal,
    SETTINGS,
    create_random_number_generator,
)

import pytest
pytestmark = pytest.mark.utility

def test_RandomNumberGenerationMixin():
    """Test functionality of the Mixin class `pyoed.utility.mixins.RandomNumberGenerationMixin`"""

    class DummyClassA(object):
        ...

    class DummyClassB(DummyClassA, RandomNumberGenerationMixin):
        ...

    # Create object with access to the Mixin features
    obj = DummyClassB()

    # Test attributes and their types
    err_msg = (
        lambda a: f"The `RandomNumberGenerationMixin` failed to provide `{a}` attribute!"
    )
    for a in ["_RNG", "update_random_number_generator", "random_number_generator"]:
        assert hasattr(obj, a), err_msg(a)

    assert (
        obj.random_number_generator is obj._RNG
    ), f"`random_number_generator` does not point to `_RNG`"
    assert isinstance(
        obj._RNG, np.random.Generator
    ), f"Expected instance of 'numpy.random.Generator'; `_RNG` is of type '{type(obj._RNG)}'"

    # Create random number generator (to act as a twin) with specific random Seed
    random_seed = SETTINGS.RANDOM_SEED
    rng = np.random.default_rng(random_seed)

    ## Test object functionality
    # test update_random_number_generator()
    obj.update_random_number_generator(random_seed=random_seed)
    assert_allequal(
        obj._RNG.standard_normal(10),
        rng.standard_normal(10),
        err_msg="`_RNG` returned incorrect random sequence",
    )
    assert_allequal(
        obj._RNG.random((10, 10)),
        rng.random((10, 10)),
        err_msg="`_RNG` returned incorrect random sequence",
    )

    assert_allequal(
        obj._RNG.exponential((1, 10)),
        rng.exponential((1, 10)),
        err_msg="`_RNG` returned incorrect random sequence",
    )

    # Two different objects have different generators
    obj_1 = DummyClassB()
    obj_2 = DummyClassB()
    assert (
        obj_1.random_number_generator is not obj_2.random_number_generator
    ), "Two different objects are assocaited with the same random number generator!"
