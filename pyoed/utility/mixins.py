# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides access to multiple Mixin classes useful to provide optional functionality to objects which repeatedly uses that functionality.
"""

import numpy as np

# define all functions here so it can be easily aggregated in __init__.py of utility package
__all__ = [
    "RandomNumberGenerationMixin",
]


class RandomNumberGenerationMixin:
    """
    Mixin class that provides a unified interface to random number generation functionality through
    Numpy Random Number Generator.

    This Mixin introduces the following to the object inheriting this Mixin class:

        - **Attributes**:
            * `_RNG`: a random number generator created by calling :py:meth:`numpy.random.default_rng()`.

        - **Properties**:
            * `random_number_generator`: a reference to the underlying random number generator `_RNG`.

        - **Methods**:
            * `update_random_number_generator`: reset/Update the underlying random_number generator
                by resetting it's `random_seed`

    .. note::
        When you use this Mixin, make sure you put it as the last class in the inheritance tuple.
    """
    def __init__(self, random_seed=None, ):
        """Initialize the random number generator"""
        self._RNG = np.random.default_rng(random_seed)

    def update_random_number_generator(
        self,
        random_seed,
    ):
        """
        Reset/Update the underlying random_number generator by resetting it's `random_seed`.
        This actually replaces the current random number generator with a new one created from
        the given seed

        :param int|None random_seed: an integer (or None) to be used to reset the random sequence

        :remarks: In the future we may provide an argument to enable changing the generator/algorithm
        """
        self._RNG = np.random.default_rng(random_seed)

    @property
    def random_number_generator(self):
        """Return a handle to the underlying random number generator"""
        return self._RNG
