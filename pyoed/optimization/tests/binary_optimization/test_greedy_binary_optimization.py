# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
A module that tests all functionality in the module
``pyoed.optimization.binary_optimization.constrained_binary_optimization``
"""
# Add pytest marker
import pytest

pytestmark = pytest.mark.optimization

import numpy as np
import pyoed
from pyoed import utility
from pyoed.tests import (
    assert_allclose,
    assert_allequal,
)
from pyoed.configs import PyOEDConfigsValidationError
from pyoed.optimization.binary_optimization import (
    GreedyBinaryOptimizer,
    GreedyBinaryOptimizerConfigs,
    GreedyBinaryOptimizerResults,
)
from pyoed.optimization.optimization_utils import (
    pseudo_random_binary_objective_function,
)
from pyoed.optimization.test_functions.binary_test_functions import FacilityFunction


class SETTINGS:
    SIZE = 16
    BUDGETS = [2, 4]
    MAXIMIZE = True
    FUN = pseudo_random_binary_objective_function
    OUTPUT_DIR = pyoed.SETTINGS.OUTPUT_DIR


def create_greedy_binary_optimizer():
    """
    Create the binary optimizer instance and enumerate all possible values
    of the objective functions (for all possible binary solutions).

    :returns:
        - An instance of :py:class:`pyoed.optimization.binary_optimization.GreedyBinaryOptimizer`
        - A dictionary holding configurations of the optimizer
        - A dictionary indexed by unique integer indexes of all possible binary designs,
            and values equal to the value of the objective function at that design
    """
    # Create the optimizer with the objective function in the settings
    optimizer_configs = dict(
        size=SETTINGS.SIZE,
        budgets=SETTINGS.BUDGETS,
        fun=SETTINGS.FUN,
        maximize=SETTINGS.MAXIMIZE,
        output_dir=SETTINGS.OUTPUT_DIR,
    )
    optimizer = GreedyBinaryOptimizer(configs=optimizer_configs.copy())

    # Enumerate all possible objective values
    fun = optimizer_configs["fun"]
    size = optimizer_configs["size"]
    budgets = optimizer_configs["budgets"]

    # All possible combinations;
    combs = 2**size
    states = {
        k: utility.index_to_binary_state(k, size=size) for k in range(1, combs + 1)
    }
    states = {k: v for k, v in states.items() if sum(v) in budgets}
    obj_vals = {k: fun(v) for k, v in states.items()}

    # Return the optimizer and the bruteforce results
    return optimizer, optimizer_configs, obj_vals


def test_GreedyBinaryOptimizer():
    """
    Test functionality of the greedy binary optimizer class
    :py:class:`pyoed.optimization.binary_optimization.GreedyBinaryOptimizer`
    """
    (optimizer, optimizer_configs, bruteforce_objective_values) = (
        create_greedy_binary_optimizer()
    )
    assert isinstance(
        optimizer, GreedyBinaryOptimizer
    ), f"Invalid optimizer of {type(optimizer)=}"

    ## Check the configurations
    configs = optimizer.configurations
    assert isinstance(
        configs, GreedyBinaryOptimizerConfigs
    ), f"{type(configs)=} is invalid"

    # update configurations with valid &/or invalid configs
    modified_configs = dict(
        size=SETTINGS.SIZE + 1,
        budgets=3,
        maximize=not SETTINGS.MAXIMIZE,
        output_dir="PYOED_TEST",
    )
    optimizer.update_configurations(**modified_configs)
    configs = optimizer.configurations.asdict()
    for k in modified_configs.keys():
        if k == "budgets":
            assert configs[k] == [
                modified_configs[k]
            ], f"Settings/Configurations mismatch at configs key: '{k}'"
        else:
            assert (
                configs[k] == modified_configs[k]
            ), f"Settings/Configurations mismatch at configs key: '{k}'"

    # Try invalid objective
    for fun in [None, 3, "s"]:
        with pytest.raises(PyOEDConfigsValidationError):
            optimizer.update_configurations(fun=fun)

    # Try invalid budget
    for budgets in [None, 0.5, "s"]:
        with pytest.raises(PyOEDConfigsValidationError):
            optimizer.update_configurations(budgets=budgets)

    ## Reset the configurations of the optimizer and make sure all configs are set
    optimizer.update_configurations(**optimizer_configs)

    ## Check all methods and attributes

    # objective_function_value()
    for k in bruteforce_objective_values.keys():
        v1 = bruteforce_objective_values[k]
        v2 = optimizer.objective_function_value(
            utility.index_to_binary_state(k, size=SETTINGS.SIZE)
        )
        assert v1 == v2, f"Objective value mismatch {v1=} != {v2=}"

    # bruteforce_objective_values(fun=None)
    results = optimizer.bruteforce_objective_values(num_active=optimizer.budgets)
    assert_allequal(
        np.sort(list(set(results.keys()))),
        np.sort(list(set(bruteforce_objective_values.keys()))),
        err_msg=f"Bruteforce function failed",
    )
    for k, v1 in bruteforce_objective_values.items():
        v2 = results[k]
        assert_allclose(
            v1, v2, err_msg=f"Bruteforce Objective value mismatch {v1=} != {v2=}"
        )

    ## Solve with maximization and minimization and compare to brute force
    # Maximization
    optimizer.update_configurations(maximize=True)
    results = optimizer.solve()
    assert isinstance(
        results, GreedyBinaryOptimizerResults
    ), f"Invalid {type(results)=}"

    # Minimization
    optimizer.update_configurations(maximize=False)
    results = optimizer.solve()
    assert isinstance(
        results, GreedyBinaryOptimizerResults
    ), f"Invalid {type(results)=}"


def test_submodular_GreedyBinaryOptimizer():
    M = np.random.rand(SETTINGS.SIZE, SETTINGS.SIZE)
    f = FacilityFunction(M=M)

    optimizer_configs = dict(
        size=SETTINGS.SIZE,
        budgets=max(SETTINGS.BUDGETS),
        fun=f,
        maximize=True,
        output_dir=SETTINGS.OUTPUT_DIR,
    )
    optimizer = GreedyBinaryOptimizer(configs=optimizer_configs.copy())

    # Enumerate all possible objective values
    size = optimizer_configs["size"]
    budget = optimizer_configs["budgets"]

    # All possible combinations;
    combs = 2**size
    states = {
        k: utility.index_to_binary_state(k, size=size) for k in range(1, combs + 1)
    }
    states = {k: v for k, v in states.items() if sum(v) == budget}
    obj_vals = {k: f(v) for k, v in states.items()}

    # Solve with maximization and compare to brute force
    results = optimizer.solve()

    bruteforce_max = max(obj_vals, key=obj_vals.get)
    to_beat = (1 - 1 / np.e) * obj_vals[bruteforce_max]
    assert results.fun >= to_beat, "Greedy optimization didn't obtain its guarantee!"

    optimizer.update_configurations(use_stochastic_greedy=True)
    avg_f = np.average([optimizer.solve().fun for _ in range(16)])

    to_beat = (1 - 1 / np.e - optimizer.stochastic_epsilon) * obj_vals[bruteforce_max]
    assert (
        avg_f >= to_beat
    ), "Stochastic Greedy optimization didn't obtain its guarantee!"
