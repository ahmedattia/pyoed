# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
A module that tests all functionality in the module
``pyoed.optimization.binary_optimization.constrained_binary_optimization``
"""
# Add pytest marker
import pytest
pytestmark = pytest.mark.optimization

import numpy as np
import pyoed
from pyoed import utility
from pyoed.tests import(
    assert_allclose,
    assert_allequal,
)
from pyoed.configs import PyOEDConfigsValidationError
from pyoed.optimization.binary_optimization import (
    ConstrainedBinaryReinforceOptimizer,
    ConstrainedBinaryReinforceOptimizerConfigs,
    ConstrainedBinaryReinforceOptimizerResults,
)
from pyoed.optimization.optimization_utils import (
    pseudo_random_binary_objective_function,
)
from pyoed.stats.distributions.conditional_bernoulli import (
    GeneralizedConditionalBernoulli,
)

class SETTINGS:
    SIZE = 5
    BUDGETS = [2, 4]
    FUN = pseudo_random_binary_objective_function
    INITIAL_POLICY = [0.5] * 5
    MAXIMIZE = True
    DEF_STEPSIZE = 0.05
    DECAY_STEP = False
    STOCHASTIC_GRADIENT_SAMPLE_SIZE = 32
    BASELINE = "optimal"
    OPTIMAL_BASELINE_BATCH_SIZE = 12
    OPTIMAL_SAMPLE_SIZE = 5
    MAXITER = 50
    PGTOL = 1e-9
    MONITOR_ITERATIONS = True
    RANDOM_SEED = 1011
    ANTITHETIC = True
    OUTPUT_DIR = pyoed.SETTINGS.OUTPUT_DIR
    # Test values to be update after creating the RNG
    TEST_SAMPLE_SIZE = 10000
    RNG = None
    TEST_BERNOULLI_PROBABILITY = None,
    TEST_BINARY_STATE = None,

# Update test values to settings
SETTINGS.RNG = np.random.default_rng(SETTINGS.RANDOM_SEED)  # Random number generator
SETTINGS.TEST_BERNOULLI_PROBABILITY = SETTINGS.RNG.uniform(low=0, high=1, size=SETTINGS.SIZE)  # policy parameter
SETTINGS.TEST_BINARY_STATE = SETTINGS.RNG.choice([0, 1], size=SETTINGS.SIZE)  # binary design


def create_constrained_binary_reinforce_optimizer():
    """
    Create the binary reinforce optimizer instance and enumerate all possible values
    of the objective functions (for all possible binary solutions).

    :returns:
        - An instance of :py:class:`pyoed.optimization.binary_optimization.ConstrainedBinaryReinforceOptimizer`
        - A dictionary holding configurations of the optimizer
        - A dictionary indexed by unique integer indexes of all possible binary designs, and values
          equal to the value of the objective function at that design
    """
    # Create the optimizer with the objective function in the settings
    optimizer_configs = dict(
        size = SETTINGS.SIZE,
        budgets = SETTINGS.BUDGETS,
        fun = SETTINGS.FUN,
        initial_policy = SETTINGS.INITIAL_POLICY,
        maximize = SETTINGS.MAXIMIZE,
        def_stepsize = SETTINGS.DEF_STEPSIZE,
        decay_step = SETTINGS.DECAY_STEP,
        stochastic_gradient_sample_size = SETTINGS.STOCHASTIC_GRADIENT_SAMPLE_SIZE,
        baseline = SETTINGS.BASELINE,
        optimal_baseline_batch_size = SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
        optimal_sample_size = SETTINGS.OPTIMAL_SAMPLE_SIZE,
        maxiter = SETTINGS.MAXITER,
        pgtol = SETTINGS.PGTOL,
        monitor_iterations = SETTINGS.MONITOR_ITERATIONS,
        random_seed = SETTINGS.RANDOM_SEED,
        antithetic = SETTINGS.ANTITHETIC,
        output_dir = SETTINGS.OUTPUT_DIR,
    )
    optimizer = ConstrainedBinaryReinforceOptimizer(configs=optimizer_configs.copy())

    # Enumerate all possible objective values
    fun = optimizer_configs['fun']
    size = optimizer_configs['size']
    budgets = optimizer_configs['budgets']

    # All possible combinations;
    # Better use combinations if num_active is None
    combs = 2**size
    obj_vals = dict()

    # Full bruteforce
    for k in range(1, combs+ 1):
        design = utility.index_to_binary_state(k, size=size)
        if np.sum(design) not in budgets: continue
        val = fun(design)
        obj_vals.update({k: val})

    # Return the optimizer and the bruteforce results
    return optimizer, optimizer_configs, obj_vals


def test_ConstrainedBinaryReinforceOptimizer():
    """
    Test functionality of the binary reinforce optimization class
    :py:class:`pyoed.optimization.binary_optimization.ConstrainedBinaryReinforceOptimizer`
    """
    ## Create the optimizer
    (
        optimizer,
         optimizer_configs,
        bruteforce_objective_values
    ) = create_constrained_binary_reinforce_optimizer()

    ## Check the type(s) of the optimizer and random number generators under the hood
    rng = optimizer.random_number_generator
    bernoulli_sampler = optimizer.Bernoulli_random_sampler
    assert isinstance(optimizer, ConstrainedBinaryReinforceOptimizer), f"Invalid optimizer of {type(optimizer)=}"
    assert isinstance(rng, np.random._generator.Generator), f"Invalid RNG of {type(rng)=}"
    assert isinstance(bernoulli_sampler, GeneralizedConditionalBernoulli), f"Invalid distribution {type(bernoulli_sampler)=}"

    ## Check the configurations
    configs = optimizer.configurations
    assert isinstance(configs, ConstrainedBinaryReinforceOptimizerConfigs), f"{type(configs)=} is invalid"
    configs = configs.asdict()
    for k in optimizer_configs.keys():
        if k in ['initial_policy', 'budgets']:
            assert_allequal(
                configs[k],
                optimizer_configs[k],
                err_msg=f"Settings/Configurations mismatch at configs key: '{k}'",
            )
        else:
            assert configs[k] == optimizer_configs[k], f"Settings/Configurations mismatch at configs key: '{k}'"

    # update configurations with valid &/or invalid configs
    modified_configs = dict(
        size = SETTINGS.SIZE + 1,
        budgets = 3,
        initial_policy = None,
        maximize = not SETTINGS.MAXIMIZE,
        def_stepsize = SETTINGS.DEF_STEPSIZE * 2,
        decay_step = not SETTINGS.DECAY_STEP,
        stochastic_gradient_sample_size = SETTINGS.STOCHASTIC_GRADIENT_SAMPLE_SIZE + 2,
        baseline = None,
        optimal_baseline_batch_size = SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE + 2,
        optimal_sample_size = SETTINGS.OPTIMAL_SAMPLE_SIZE + 2,
        maxiter = SETTINGS.MAXITER + 2,
        pgtol = SETTINGS.PGTOL * 2,
        monitor_iterations = not SETTINGS.MONITOR_ITERATIONS,
        random_seed = 1,
        antithetic = not SETTINGS.ANTITHETIC,
        output_dir = "PYOED_TEST",
    )
    optimizer.update_configurations(**modified_configs)
    configs = optimizer.configurations.asdict()
    for k in modified_configs.keys():
        if k=='initial_policy':
            assert_allequal(
                configs[k],
                [0.5] * modified_configs['size'],
                err_msg=f"Settings/Configurations mismatch at configs key: '{k}'",
            )
        else:
            assert configs[k] == modified_configs[k], f"Settings/Configurations mismatch at configs key: '{k}'"

    ## Update random seed in configurations and check its effect on the associated two RNGs
    optimizer.update_configurations(random_seed=11)
    s1 = optimizer.random_number_generator.random(100)
    optimizer.update_configurations(random_seed=11)
    s2 = optimizer.random_number_generator.random(100)
    assert_allclose(s1, s2, err_msg=f"Resetting random seed for random number generator failed!")

    optimizer.update_configurations(random_seed=11)
    bernoulli_sampler = optimizer.Bernoulli_random_sampler
    s1 = bernoulli_sampler.sample(100)
    optimizer.update_configurations(random_seed=11)
    bernoulli_sampler = optimizer.Bernoulli_random_sampler
    s2 = bernoulli_sampler.sample(100)
    assert_allclose(s1.astype(int), s2.astype(int), err_msg=f"Resetting random seed for random number generator failed!")

    # Try invalid objective
    for fun in [None, 3, 's']:
        with pytest.raises(Exception) as e_info:
            optimizer.update_configurations(fun=fun)
        assert e_info.type is PyOEDConfigsValidationError

    ## Reset the configurations of the optimizer and make sure all configs are set
    optimizer.update_configurations(**optimizer_configs)

    ## Check all methods and attributes

    # update_random_number_generators()
    optimizer.update_random_number_generators(random_seed=12)
    s1 = optimizer.random_number_generator.random(100)
    s1b = optimizer.Bernoulli_random_sampler.sample(100)
    optimizer.update_random_number_generators(random_seed=12)
    s2 = optimizer.random_number_generator.random(100)
    s2b = optimizer.Bernoulli_random_sampler.sample(100)
    assert_allclose(s1.astype(int), s2.astype(int), err_msg=f"Resetting random seed for random number generator failed!")
    assert_allclose(s1b.astype(int), s2b.astype(int), err_msg=f"Resetting random seed for random number generator failed!")

    # objective_function_value()
    for k in bruteforce_objective_values.keys():
        v1 = bruteforce_objective_values[k]
        v2 = optimizer.objective_function_value(
            utility.index_to_binary_state(k, size=SETTINGS.SIZE)
        )
        assert v1 == v2, f"Objective value mismatch {v1=} != {v2=}"

    # bruteforce_objective_values(fun=None)
    results = optimizer.bruteforce_objective_values()
    assert_allequal(
        list(set(results.keys())),
        list(set(bruteforce_objective_values.keys())),
        err_msg=f"Bruteforce function failed",
    )
    for k, v1 in bruteforce_objective_values.items():
        v2 = results[k]
        assert_allclose(v1, v2, err_msg=f"Bruteforce Objective value mismatch {v1=} != {v2=}")

    # generate_sample(sample_size)
    sample_size = SETTINGS.TEST_SAMPLE_SIZE
    optimizer.update_random_number_generators(11)
    sample = optimizer.generate_sample(theta=SETTINGS.TEST_BERNOULLI_PROBABILITY, sample_size=sample_size,)
    sample = np.asarray(sample)

    ## Create a Bernoulli instance to compare bernoulli PMF values and gradients
    bernoulli_dist = GeneralizedConditionalBernoulli(
        dict(
            budgets=SETTINGS.BUDGETS,
            parameter=SETTINGS.TEST_BERNOULLI_PROBABILITY,
            random_seed=SETTINGS.RANDOM_SEED,
        )
    )

    # evaluate_pmf(design, theta)
    v1 = optimizer.evaluate_pmf(design=SETTINGS.TEST_BINARY_STATE, theta=SETTINGS.TEST_BERNOULLI_PROBABILITY)
    v2 = bernoulli_dist.pmf(SETTINGS.TEST_BINARY_STATE, )
    assert_allclose(v1, v2, err_msg=f"eval_pmf() returned {v1=}, expected {v2=}")

    # evaluate_grad_log_pmf(design, theta)
    v1 = optimizer.evaluate_grad_log_pmf(design=SETTINGS.TEST_BINARY_STATE, theta=SETTINGS.TEST_BERNOULLI_PROBABILITY)
    v2 = bernoulli_dist.grad_log_pmf(SETTINGS.TEST_BINARY_STATE, )
    assert_allclose(v1, v2, err_msg=f"eval_grad_log_pmf() returned {v1=}, expected {v2=}")

    # evaluate_grad_pmf(design, theta)
    v1 = optimizer.evaluate_grad_pmf(design=SETTINGS.TEST_BINARY_STATE, theta=SETTINGS.TEST_BERNOULLI_PROBABILITY)
    v2 = bernoulli_dist.grad_pmf(SETTINGS.TEST_BINARY_STATE, )
    assert_allclose(v1, v2, err_msg=f"eval_grad_pmf() returned {v1=}, expected {v2=}")
    ## Properties
    assert isinstance(optimizer.configurations.antithetic, bool), "Invalid property {type(optimizer.antitheic)=}"
    antithetic = optimizer.configurations.antithetic
    optimizer.antithetic = not antithetic
    assert optimizer.antithetic is not antithetic, "Failed to update antithetic flag"
    optimizer.configurations.antithetic = antithetic  # Rest


    ## Calculate the expected policy (exactly), and its gradient
    bernoulli_dist = GeneralizedConditionalBernoulli(
        dict(
            budgets=SETTINGS.BUDGETS,
            parameter=SETTINGS.TEST_BERNOULLI_PROBABILITY,
            random_seed=SETTINGS.RANDOM_SEED,
        )
    )
    # objective_function_value()
    for k in range(1, 2**SETTINGS.SIZE+1):
        design = utility.index_to_binary_state(k, size=SETTINGS.SIZE)
        if sum(design) in optimizer.budgets:
            v1 = bernoulli_dist.pmf(design)
            v2 = optimizer.evaluate_pmf(design, theta=SETTINGS.TEST_BERNOULLI_PROBABILITY)
            assert v1 == v2, f"Objective value mismatch {v1=} != {v2=}"
        else:
            v = optimizer.evaluate_pmf(design, theta=SETTINGS.TEST_BERNOULLI_PROBABILITY)
            assert v==0, f"Nonzero probability for impropable design {v=}, {design=}, {optimizer.budgets=}"


    exact_objval  = 0.0
    exact_objval_grad = 0.0
    for k, objval in bruteforce_objective_values.items():
        design = utility.index_to_binary_state(k, size=SETTINGS.SIZE)
        pmf = bernoulli_dist.pmf(design, )
        exact_objval += objval * pmf
        #
        grad_pmf = bernoulli_dist.grad_pmf(design, )
        exact_objval_grad += objval * grad_pmf

    # stochastic_policy_objective_value(theta)
    opt_exact_objval = optimizer.stochastic_policy_objective_value(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        exact=True,
    )
    opt_approx_objval = optimizer.stochastic_policy_objective_value(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        sample_size=SETTINGS.TEST_SAMPLE_SIZE,
        exact=False,
    )
    assert_allclose(
        opt_exact_objval,
        exact_objval,
        rtol=1e-7,
        err_msg=f"Optimizer returned wrong exact policy expected value {opt_exact_objval=} != {exact_objval=}",
    )
    assert_allclose(
        opt_approx_objval,
        exact_objval,
        rtol=5e-2,
        err_msg=f"Optimizer returned bad approximation of the policy expected value {opt_approx_objval=} !~ {exact_objval=}",
    )

    # stochastic_policy_objective_gradient(theta)
    opt_exact_grad = optimizer.stochastic_policy_objective_gradient(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        exact=True,
    )
    opt_approx_grad = optimizer.stochastic_policy_objective_gradient(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        sample_size=SETTINGS.TEST_SAMPLE_SIZE,
        exact=False,
    )
    assert_allclose(
        opt_exact_grad,
        exact_objval_grad,
        rtol=1e-7,
        err_msg=f"Optimizer returned wrong exact policy expected value gradient {opt_exact_grad=} != {exact_objval_grad=}",
    )
    assert_allclose(
        opt_approx_grad,
        exact_objval_grad,
        rtol=5e-1,
        err_msg=f"Optimizer returned bad approximation of the policy expected value gradient {opt_approx_grad=} !~ {exact_objval_grad=}",
    )

    # optimal_baseline_estimate(theta, sample=None)
    # we test that it works, returns a scalar, and close values produced if full sample is passed.
    optimizer.update_random_number_generators(SETTINGS.RANDOM_SEED)
    optimal_baseline_sample = optimizer.generate_sample(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        sample_size=SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
    )
    v1 = optimizer.optimal_baseline_estimate(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        sample=optimal_baseline_sample,
        batch_size=SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
    )
    optimizer.update_random_number_generators(SETTINGS.RANDOM_SEED)
    v2 = optimizer.optimal_baseline_estimate(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        sample=optimal_baseline_sample,
        batch_size=SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
    )
    assert_allclose(v1, v2, rtol=4e-1, err_msg=f"Optimal baseline estimate failed {v1=} !~ {v2=}")

    # Check effect on variance of the estimate
    optimizer.update_random_number_generators(SETTINGS.RANDOM_SEED)
    v1 = optimizer.optimal_baseline_estimate(
        SETTINGS.TEST_BERNOULLI_PROBABILITY,
        batch_size=SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
    )

    num_iter = 32
    no_baseline_grads = np.empty((SETTINGS.SIZE, num_iter))
    optimal_baseline_grads = np.empty_like(no_baseline_grads)
    for i in range(num_iter):
        test_sample = optimizer.generate_sample(
            SETTINGS.TEST_BERNOULLI_PROBABILITY,
            sample_size=SETTINGS.OPTIMAL_BASELINE_BATCH_SIZE,
        )
        no_baseline_grads[:, i] = optimizer.stochastic_policy_objective_gradient(
            theta=SETTINGS.TEST_BERNOULLI_PROBABILITY,
            sample=test_sample,
            exact=False,
        )

        optimal_baseline_grads[:, i] = optimizer.stochastic_policy_objective_gradient(
            theta=SETTINGS.TEST_BERNOULLI_PROBABILITY,
            fun=lambda x: optimizer.objective_function_value(x) - v1,
            sample=test_sample,
            exact=False,
        )

    v_optimalbaseline = np.sum(np.var(optimal_baseline_grads, axis=0))
    v_nobaseline = np.sum(np.var(no_baseline_grads, axis=0))
    assert  v_optimalbaseline < v_nobaseline, f"Optimal baseline gradient variance {v_optimalbaseline=} not less than no-baseline variance{v_nobaseline=}"

    # full_Hessian_estimate(theta)
    H = optimizer.full_Hessian_estimate(SETTINGS.TEST_BERNOULLI_PROBABILITY)
    assert isinstance(H, np.ndarray) and H.shape == (SETTINGS.SIZE, SETTINGS.SIZE), f"Invalid Hessian estimate value/type/shape! {H=} of {type(H)=}"

    # Hessian_estimate_vector_prod(theta, x,)
    # NOTE: This is already utilized inside `full_Hessian_estimate`. If it doesn't work, `full_Hessian_estimate` fails
    # pass

    ## Reset the configurations of the optimizer and make sure all configs are set
    optimizer.update_configurations(**optimizer_configs)

    ## Solve with maximization and minimization and compare to brute force
    max_val = max(bruteforce_objective_values.values())
    min_val = min(bruteforce_objective_values.values())
    # Maximization
    optimizer.update_configurations(maximize=True)
    results = optimizer.solve()
    assert isinstance(results, ConstrainedBinaryReinforceOptimizerResults), f"Invalid {type(results)=}"
    optimal_design_objval = results.optimal_design_objval
    assert optimal_design_objval == max_val, f"Maximization; failed to reach global {optimal_design_objval=} != {max_val=}"

    # Minimization
    optimizer.update_configurations(maximize=False)
    results = optimizer.solve()
    assert isinstance(results, ConstrainedBinaryReinforceOptimizerResults), f"Invalid {type(results)=}"
    optimal_design_objval = results.optimal_design_objval
    assert optimal_design_objval == min_val, f"Minimization; failed to reach global {optimal_design_objval=} != {min_val=}"

