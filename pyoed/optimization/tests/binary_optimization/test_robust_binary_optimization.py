# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
A module that tests all functionality in the module
``pyoed.optimization.binary_optimization.robust_binary_optimization``
"""
# Add pytest marker
import pytest
pytestmark = pytest.mark.optimization

import numpy as np

from pyoed.tests import(
    create_random_number_generator,
    assert_allequal,
)

import pyoed
from pyoed.optimization.binary_optimization import (
    BinaryReinforceOptimizer,
    RobustBinaryReinforceOptimizer,
    RobustBinaryReinforceOptimizerConfigs,
    RobustBinaryReinforceOptimizerResults,
)


# Create settings class
_RNG = create_random_number_generator(pyoed.tests.SETTINGS.RANDOM_SEED)
class SETTINGS:
    SIZE = 5
    UNCERTAIN_PARAMETER_SIZE = 3
    FUN = lambda x, y: _RNG.uniform()
    MAXITER = 10
    OUTER_OPT_MAXITER = 10
    INNER_OPT_MAXITER = 10
    UNCERTAIN_PARAMETER_SAMPLE = [[0, 1, 1], ]
    OUTPUT_DIR = "__PYTES_RESULTS__"
    RANDOM_SEED = pyoed.tests.SETTINGS.RANDOM_SEED
    RNG = _RNG


def test_RobustBinaryReinforceOptimizer():
    """
    Test functionality of the binary reinforce optimization class
    :py:class:`pyoed.optimization.binary_optimization.RobustBinaryReinforceOptimizer`

    .. note::
        This is a seriously minimal example that only makes sure the optimizer works
        without errors.
    """
    ## Create optimizer
    optimizer = RobustBinaryReinforceOptimizer(
        configs = {
            'size': SETTINGS.SIZE,
            'uncertain_parameter_size': SETTINGS.UNCERTAIN_PARAMETER_SIZE,
            'fun': SETTINGS.FUN,
            'maxiter': SETTINGS.MAXITER,
            'inner_opt_maxiter': SETTINGS.INNER_OPT_MAXITER,
            'outer_opt_maxiter': SETTINGS.OUTER_OPT_MAXITER,
            'uncertain_parameter_sample': SETTINGS.UNCERTAIN_PARAMETER_SAMPLE,
            'output_dir': SETTINGS.OUTPUT_DIR,
            'random_seed': SETTINGS.RANDOM_SEED,
        }
    )

    ## Check optimizer type
    assert isinstance(
        optimizer,
        RobustBinaryReinforceOptimizer
    ), f"Invalid optimizer {type(optimizer)=}"

    # Test configurations
    assert isinstance(
        optimizer.configurations,
        RobustBinaryReinforceOptimizerConfigs
    ), f"Invalid optimizer {type(optimizer.configurations)=}"
    assert optimizer.size == optimizer.configurations.size, f"Invalid{optimizer.size=}"
    assert optimizer.configurations.size == SETTINGS.SIZE
    assert optimizer.configurations.uncertain_parameter_size == SETTINGS.UNCERTAIN_PARAMETER_SIZE
    assert optimizer.configurations.fun is SETTINGS.FUN
    assert optimizer.configurations.maxiter == SETTINGS.MAXITER
    assert optimizer.configurations.outer_opt_maxiter == SETTINGS.OUTER_OPT_MAXITER
    assert optimizer.configurations.inner_opt_maxiter == SETTINGS.INNER_OPT_MAXITER
    assert_allequal(
        optimizer.configurations.uncertain_parameter_sample,
        SETTINGS.UNCERTAIN_PARAMETER_SAMPLE,
        err_msg=f"Ivalid {optimizer.configurations.uncertain_parameter_sample=}"
    )
    assert optimizer.configurations.output_dir == SETTINGS.OUTPUT_DIR
    assert optimizer.configurations.random_seed == SETTINGS.RANDOM_SEED


    ## Test all methods

    # `create_stochastic_optimizer`
    stoch_optimizer = optimizer.create_stochastic_optimizer()
    assert isinstance(stoch_optimizer, BinaryReinforceOptimizer), f"Invalid {type(stoch_optimizer)=}"
    assert stoch_optimizer.size == stoch_optimizer.configurations.size, f"Invalid{stoch_optimizer.size=}"
    assert stoch_optimizer.configurations.maxiter == SETTINGS.MAXITER
    assert stoch_optimizer.configurations.output_dir == SETTINGS.OUTPUT_DIR
    assert stoch_optimizer.configurations.random_seed == SETTINGS.RANDOM_SEED


    # `generate_design_sample`
    sample = optimizer.generate_design_sample(sample_size=1)
    assert len(sample) == 1 and len(sample[0]) == SETTINGS.SIZE, f"Invalid {sample=} size/shape/type"
    assert all([v in [0, 1] for v in sample[0]]), f"Invalid sample (nonbinary) {sample=}"

    # `generate_uncertain_parameter_sample`
    sample = optimizer.generate_uncertain_parameter_sample(sample_size=1)
    assert len(sample) == 1 and len(sample[0]) == SETTINGS.UNCERTAIN_PARAMETER_SIZE, f"Invalid uncertain parameter {sample=} size/shape/type"

    ## Solve and check return type
    for solve_by_relaxation in [False, True]:
        optimizer.update_configurations(solve_by_relaxation=solve_by_relaxation)
        results = optimizer.solve()
        assert isinstance(
            results,
            RobustBinaryReinforceOptimizerResults,
        ), f"Invalid results with {solve_by_relaxation=}; {type(results)=}"

    ## Plot results
    optimizer.plot_results(results)

