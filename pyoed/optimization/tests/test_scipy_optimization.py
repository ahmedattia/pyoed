# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
A module that tests all functionality in the module
``pyoed.optimization.scipy_optimization``
"""
# Add pytest marker
import pytest
pytestmark = pytest.mark.optimization

from itertools import product
import numpy as np
import pyoed
from pyoed import utility
from pyoed.tests import(
    assert_allclose,
    assert_allequal,
)
from pyoed.configs import PyOEDConfigsValidationError
from pyoed.optimization.scipy_optimization import (
    ScipyOptimizer,
    ScipyOptimizerConfigs,
    ScipyOptimizerResults,
)

class SETTINGS:
    SIZE = 3
    FUN = lambda x: sum(x**2)
    BOUNDS = (1, 4)


@pytest.mark.parametrize("maximize,bounds", product([True, False], [None, SETTINGS.BOUNDS]))
def test_ScipyOptimizer(maximize, bounds):
    """
    Test functionality of the binary reinforce optimization class
    :py:class:`pyoed.optimization.scipy_optimization.ScipyOptimizer`
    """
    ## Create the optimizer
    optimizer = ScipyOptimizer(
        configs = {
            'fun': SETTINGS.FUN,
            'x0': [1] * SETTINGS.SIZE,
            'maximize': maximize,
            'bounds': bounds,
        }
    )

    ## Check the type of the optimizer
    assert isinstance(optimizer, ScipyOptimizer), f"Invalid optimizer of {type(optimizer)=}"

    ## Check the configurations
    configs = optimizer.configurations
    assert isinstance(configs, ScipyOptimizerConfigs), f"{type(configs)=} is invalid"

    assert optimizer.configurations.fun is SETTINGS.FUN, f"Invalid {optimizer.configurations.fun=}"
    assert optimizer.configurations.maximize == maximize, f"Invalid {optimizer.configurations.maximize=}"
    assert optimizer.configurations.jac is None, f"Invalid {optimizer.configurations.jac=}"
    assert optimizer.configurations.hess is None, f"Invalid {optimizer.configurations.hess=}"
    assert optimizer.configurations.hessp is None, f"Invalid {optimizer.configurations.hessp=}"
    assert optimizer.configurations.bounds is bounds, f"Invalid {optimizer.configurations.bounds=}"

    x0 = optimizer.configurations.x0
    assert isinstance(x0, np.ndarray) and x0.ndim ==1 and x0.size == SETTINGS.SIZE, f"Invalid {x0=} of {type(x0)=}"

    ## Solve
    results = optimizer.solve()
    assert isinstance(results, ScipyOptimizerResults), f"Invalid {type(results)=}"

    # if bounds is not None:
    if bounds is not None:
        assert results.success == True, f"Optimizer did not succeed {results.success=}"
        if maximize:
            # The objective is quadratic; maximum is upper bound
            assert_allclose(results.x, max(SETTINGS.BOUNDS), )
        else:
            # The objective is quadratic; minimum is lower bound
            assert_allclose(results.x, min(SETTINGS.BOUNDS), )

    else:
        # No Bounds
        if maximize:
            # Unbounded optimization problem
            assert results.success == False, f"Optimizer converged even for unbounded problem! {results.success=}"
        else:
            # The objective is quadratic; minimum is 0
            assert results.success == True, f"Optimizer did not succeed {results.success=}"
            assert_allclose(0, results.x, )

