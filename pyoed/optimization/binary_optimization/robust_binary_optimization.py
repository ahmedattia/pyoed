# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
A module providing access to multiple Robust Binary optimization routines.
"""

import os
import warnings
import numpy as np
import re
import hashlib
from scipy import optimize as sp_optimize
import copy
import pickle
from dataclasses import dataclass, field
from typing import Iterable, Callable, Union, Tuple

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.lines import Line2D

from ...configs import (
    PyOEDConfigsValidationError,
    validate_key,
    aggregate_configurations,
    set_configurations,
    remove_unknown_keys,
    SETTINGS as pyoed_settings,
)
from ... import utility
from ...stats.distributions.bernoulli import Bernoulli
from pyoed.optimization.optimization_utils import standardize_domain_bounds
from .stochastic_binary_optimization import (
    BinaryReinforceOptimizer,
    BinaryReinforceOptimizerConfigs,
)
from ..core import (
    OptimizerResults,
)


@dataclass(kw_only=True, slots=True)
class RobustBinaryReinforceOptimizerConfigs(BinaryReinforceOptimizerConfigs):
    """
    Configuration dataclass for the :py:class:`RobustBinaryReinforceOptimizer`.
    This inherits all keys/configurations/attributes of :py:class:`BinaryReinforceOptimizerConfigs`
    with the additional keys/attributes described below.
    The :py:class:`RobustBinaryReinforceOptimizer` is a modification from Polyak's
    algorithm developed for stochastic optimization with proper maxmin formulation.

    :param uncertain_parameter_size: The dimension/size of the uncertain parameter
    :param maxmin: if `True`, a max-min optimization prolem is assumed,
        otherwise, a min-max problem is considere.
    :param fun_grad_parameter: If this is callable, it is (similar to
        `fun` configuration required by the parent class,) a function
        that accepts a binary design, and a realization of the the uncertain
        parameter (design, parameter).
        This is a function that calculates the gradient of of the objective
        function 'fun' with respect to the uncertainty paramter.
        If `None`, finite differences is used to calculate such gradient,
        though this will be expensive.
    :param fun_grad_design: if given, a function that calculates the gradient
        of ``fun`` with respect to the relaxed design;
        similar to ``fun``, this function accepts two paramters (design, param).
    :param maxiter: maximum number of iterations of Polyak algorithm.
        This controls the number of times the algorithms alternates between updating
        the policy/design and the uncertain parameter.
        The updates for each of those is carried out by the corresponding optimizers
        which have their own number of iterations set by `outer_opt_maxiter` and
        `inner_opt_maxiter`, respectively.
    :param outer_opt_maxiter: maximum number of iterations of the optimizaer
        solving the outer optimization problem (maximization of the design/policy).
        This sets the maximum number of iterations of the optimization algorithm
        that updates the policy (outer optimization).
        This controls `maxiter` passed to the stochastic (or relaxed) optimization
        algorithm that optimizes for a design or policy parameters.
    :param inner_opt_maxiter: maximum number of iterations of the inner
        optimization problem. This is passed to the inner loop optimizer that solves
        the inner optimization for an optimal uncertain parameter.
    :param uncertain_parameter_sample: sample from the uncertain parameter space (param).
        This is used as the initializer of the tracker used while solving the optimization problem.
    :param uncertain_parameter_bounds: bounds/domain of the uncertain parameter.
    :param solve_by_relaxation: whether to allow relaxation of the design
        or use stochastic binary approach if `True`, then `fun_grad_design` should
        be given, otherwise finite differences is used to approximate it.
    :param deterministic_optimization_routine: Name of the optimization routine to
        be used for the inner optimization problem (over the uncertain parameter.
        This is also used for the outer optimization problem is `solve_by_relaxation`
        is `True`.
    :param tol: tolerance used for termination of inner optimization steps
        (over the uncertain parameter).
        This is different from `pgtol` used for the outer stochastic binary optimization
        termination.
    :parm uncertain_parameter_max_num_digits: Maximum number of digits used in uncertain parameter
        (round function applied with this maximum number of digits) for hashing.
        This is because we need to limit number of digits to get proper hashing, otherwise too-similar
        parameters (up to precision error) will end up with different hashes.

    .. note::
        The user MUST specify the size of the design space and the
        uncertain parameter space to guarantee consistency

    .. note::
        We allow ``solve_by_relaxation`` fo analysis purposes, however, we have shown that
        such optimization problem (relaxation of the outer optimization problem) yields
        optimal subset that is different from the binary optimal set.
    """

    uncertain_parameter_size: int | None = None
    maxmin: bool = True
    fun_grad_parameter: Callable | None = None
    fun_grad_design: Callable | None = None
    uncertain_parameter_sample: Iterable[np.ndarray] = field(default_factory=lambda: [])
    uncertain_parameter_bounds: Tuple[type(None), type(None)] | Iterable[Tuple[Union[float,type(None)], Union[float,type(None)]]] | None = None
    solve_by_relaxation: bool = False
    reset_initial_guess: bool = False
    deterministic_optimization_routine: str = "Scipy-L-BFGS-B"
    tol: float = 1.0e-12
    maxiter: int = 100
    outer_opt_maxiter: int = 1000
    inner_opt_maxiter: int = 10000
    uncertain_parameter_max_num_digits: int = 12
    name: str = "RobustBinaryReinforceOptimizer: Robust Binary Optimization via Stochastic Learning"


@dataclass(kw_only=True, slots=True)
class RobustBinaryReinforceOptimizerResults(OptimizerResults):
    """
    Container for results/data generated by the :py:class:`RobustBinaryReinforceOptimizer`.

    :param optimal_policy: the parameters of the optimal policy (probability of activation
        of each entry of the design)
    :param optimal_policy_sample: design sample generated by sampling the optimal policy
        (Bernoulli samples with probababilities set to the optimal policy parameters
        `optimal_policy_parameters`)
    :param optimal_policy_sample_objval: value of the objective function :math:`J` evaluated
        at each design in `optimal_sample`
    :param optimal_design: the optimal design (binary vector) sampled from the `optimal_policy`.
    :param optimal_design_objval: the value of the objective function evaluated at the
        above optimal design.
    :param uncertain_parameter_sample: sample from the uncertain parameter space (param) expanded
        by the solution algorithm
    :param optimal_uncertain_parameter: the value of the uncertain parameter corresponding the
        the optimal solution.
    :param optimization_trajectory: trajectory (consecutive parameters) of the policy parameters
        generated along the optimization iterations; populated only if `monitor_iterations`
        configuration is set to `True`.
        The optimization trajectory is on the form
        ``[(d0, p0), (d1, p1), ...]`` where `di` is either the design (binary) variable or the
        Bernoulli parameters, and ``pi`` is the uncertain parameter.
    :param optimization_trajectory_objval: value of the **deterministic** objective
        function, that is :math:`J` along the optimization trajectory; populated only if
        `solve_by_relaxation` configuration is set to `True`.
        This is the fomr ``[J(d0, p0), J(d1, p1), ...]`` where ```J`` is the `fun` in the
        configurations.
    :param optimization_trajectory_stoch_objval: value (MC estimate) of the **stochastic** objective
        function, that is :math:`E[J]` along the optimization trajectory; populated only if
        `solve_by_relaxation` configuration is set to `False`.
        This is the fomr ``[E(J(d0, p0)), E(J(d1, p1)), ...]`` where the expection ``E`` is estimated
        by averaging (monte carlo estimate).
    :param inner_optimization_results: a list holding results for each of the inner optimization iterations.
    :param outer_optimization_results: a list holding results for each of the outer optimization iterations.
    :param converged: `True` if the algorithm converged, `False` otherwise.
    :param msg: a string message describing the final status of the algorithm
    :param objective_value_tracker: a dictionary containing the value of the objective function
        evaluated through the course of the algorithm.
        The dictionary is indexed by the integer indexes of the generated binary states.
    :param configurations: a dictionary holding problem configurations. This can be obtained by
        calling :py:meth:`asdict()` of the configurations object used by the optimizer, e.g.,
        :py:class:`BinaryReinforceOptimizerConfigs`.
    """
    # TODO: Revisit hints of inner_optimization_results and outer_optimization_results...

    optimal_policy: np.ndarray | None = None
    optimal_policy_sample: np.ndarray | None = None
    optimal_policy_sample_objval: np.ndarray | None = None
    optimal_design: np.ndarray = field(default_factory=lambda: np.empty(0, dtype=bool))
    optimal_design_objval: float | None = None
    uncertain_parameter_sample: list[np.ndarray] | None = None
    optimal_uncertain_parameter: np.ndarray | None = None
    optimization_trajectory: list[(np.ndarray, np.ndarray)] | None  = None
    optimization_trajectory_objval: list[float] | None = None
    optimization_trajectory_stoch_objval: list[float] | None = None
    inner_optimization_results: list[dict] | None = None
    outer_optimization_results: list[dict] | None = None
    converged: bool = False
    maxiter_reached: bool = False
    message: str = ""
    objective_value_tracker: dict | None = None
    configurations: None | dict = None

    @property
    def x(self) -> np.ndarray:
        """The optimal solution of the optimization problem."""
        return self.optimal_design

    @property
    def success(self) -> bool:
        """Whether or not the optimizer exited successfully."""
        return self.converged

    @property
    def message(self) -> str:
        """Description of the cause of the termination."""
        return self.message

    @property
    def fun(self) -> float:
        """Value of objective function at the optimal solution `x`."""
        return self.optimal_design_objval

    @property
    def jac(self) -> np.ndarray:
        """Value of the Jacobian of the objective function at the optimal solution `x` (if available)."""
        return None

    @property
    def hess(self) -> object:
        """Value of the Hessian of the objective function at the optimal solution `x` (if available)."""
        return None

    @property
    def hess_inv(self) -> object:
        """Value of the inverse of Hessian of the objective function at the optimal solution `x` (if available)."""
        return None

    @property
    def nfev(self) -> int:
        """Number of evaluations of the objective functions."""
        if self.sampled_design_indexes is not None:
            nfev = 0
            for inds in self.sampled_design_indexes:
                nfev += len(inds)
        return nfev

    @property
    def njev(self) -> int:
        """Number of evaluations of the the Jacobian of the objective function."""
        return 0

    @property
    def nhev(self) -> int:
        """Number of evaluations of the the Hessian of the objective function."""
        return 0

    @property
    def nit(self) -> int:
        """Number of iterations performed by the optimizer."""
        return len(self.optimization_trajectory)-1




@set_configurations(configurations_class=RobustBinaryReinforceOptimizerConfigs)
class RobustBinaryReinforceOptimizer(BinaryReinforceOptimizer):
    r"""
    Robust Binary Optimization by stochastic learning.
    The objective is to solve one of the following optimization
    problems :math:`\max_{d} E[ \min_{\lambda} J(d, \lambda)]` or
    :math:`\min_{d} E[ \max_{\lambda} J(d, \lambda)]`
    based on the value of the configuration key `maxmin`, where :math:`J` is an
    objective function (specified in the configurations dictionary by `fun`)
    that accepts a binary design vector :math:`d` and an uncertain parameter
    :math:`\lambda`.

    Additional details of the approach underlying this implementation can be found
    in :ref:`[1] <robust_oed_paper_ref>`.

    :param RobustBinaryReinforceOptimizerConfigs | dict | None configs:  Configurations
        of the :py:class:`RobustBinaryReinforceOptimizer`, either as an instance of
        :py:class:`RobustBinaryReinforceOptimizerConfigs`, a dictionary, or `None`.
        The default is `None`, in which case, the default configurations provided by
        :py:class:`RobustBinaryReinforceOptimizerConfigs` are used.

    ..note::
        The optimization is assumed to be unconstrained and any soft (e.g., budget)
        constraints are already absorbed into `fun` passed in the configurations
        dictionary.


    **References:**

    .. _robust_oed_paper_ref:

    1. Ahmed Attia, Sven Leyffer, and Todd S. Munson.
       "Robust A-optimal experimental design for Bayesian inverse problems."
       arXiv preprint arXiv:2305.03855 (2023).
    """
    def __init__(self, configs: RobustBinaryReinforceOptimizerConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        ## Additional settings
        self.configurations.uncertain_parameter_bounds = standardize_domain_bounds(
            size=self.uncertain_parameter_size,
            bounds=self.configurations.uncertain_parameter_bounds,
        )
        self.configurations.uncertain_parameter_sample = self._standardize_uncertain_parameter_sample(
            self.configurations.uncertain_parameter_sample
        )

        # Create a stochastic optimization object for the outer optimization problem
        self._STOCHASTIC_BINARY_OPTIMIZER = self.create_stochastic_optimizer()

    def validate_configurations(
        self,
        configs: dict | BinaryReinforceOptimizerConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check the passed configuratios and make sure they are conformable with each
        other, and with current configurations once combined.  This guarantees that any
        key-value pair passed in configs can be properly used

        .. note::
            Here only the locally-defined configurations in
            :py:class:`RobustBinaryReinforceOptimizerConfigs` are validated.
            Finally, super classes validators are called.

        :param configs: full or partial (subset) configurations to be validated
        :param raise_for_invalid: if `True` raise :py:class:`TypeError` for invalid
            configrations type/key. Default `True`
        :returns: flag indicating whether passed configurations dictionary is valid or not

        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`RobustBinaryReinforceOptimizerConfigs`.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self, configs=configs, configs_class=self.configurations_class
        )


        ## Validate specific entries/configs

        # Local tests
        is_bool = lambda x: utility.isnumber(x) and (x == bool(x))
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)
        is_callable = lambda x: callable(x)
        is_none_or_callable = lambda x: x is None or callable(x)

        def is_valid_sample(sample):
            if utility.isiterable(sample):
                if len(sample) <1 :
                    return False
                    print("Empty!")
                else:
                    if all(
                            [
                                np.asarray(s).flatten().size==aggregated_configs.uncertain_parameter_size
                                for s in sample
                            ]
                    ):
                        return True
                    else:
                        print(
                            sample,
                            aggregated_configs.uncertain_parameter_size,
                            [
                                np.asarray(s).flatten()
                                for s in sample

                            ],
                            [
                                np.asarray(s).flatten().size==aggregated_configs.uncertain_parameter_size
                                for s in sample
                            ]
                        )
                        return False
            else:
                return False

        def is_valid_bounds(bounds):
            size = utility.asarray(
                aggregated_configs.uncertain_parameter_size
            ).flatten().size
            try:
                standardize_domain_bounds(
                    bounds=bounds, size=aggregated_configs.uncertain_parameter_size,
                )
            except (Exception) as err:
                print(
                    f"\n****\nChecking the bouds failed;\n**** \n\t"
                    f"Unexpected {err=} of {type(err)=}\n****\n"
                )
                return False
            else:
                return True

        def is_valid_opt_routine(routine):
            if isinstance(routine, str):
                if re.match(
                    r"\A(sp|scipy)*( |-|_)*L( |-|_)*BFGS( |-|_)*(B)*\Z",
                    routine,
                    re.IGNORECASE,
                ):
                    return True
                else:
                    return False
            else:
                return False

        # Validate parameters with positive integer values
        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=param,
                    test=is_positive_int,
                    message=f"{param} must be a positive integer!",
                    raise_for_invalid=raise_for_invalid,
                )
                for param in [
                    "uncertain_parameter_size",
                    "maxiter",
                    "outer_opt_maxiter",
                    "inner_opt_maxiter",
                    "uncertain_parameter_max_num_digits",
                ]
            ]
        ):
            return False

        # Validate parameters with boolean values
        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=param,
                    test=is_bool,
                    message=f"{param} must be a boolean True/False!",
                    raise_for_invalid=raise_for_invalid,
                )
                for param in [
                    "maxmin",
                    "solve_by_relaxation",
                    "reset_initial_guess",
                ]
            ]
        ):
            return False


        # Validate functions/callable (also allowed to be None -> FD approximation when needed)
        if any(
            [
                not validate_key(
                    current_configs=aggregated_configs,
                    new_configs=configs,
                    key=param,
                    test=is_none_or_callable,
                    message=f"{param} must be a None or valid function/callable!",
                    raise_for_invalid=raise_for_invalid,
                )
                for param in [
                    "fun_grad_parameter",
                    "fun_grad_design",
                ]
            ]
        ):
            return False

        # Validate `tol`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="tol",
            test=lambda x: is_float(x) and x>=0,
            message=f"'tol' must be a non-negative float!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Validate `deterministic_optimization_routine`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="deterministic_optimization_routine",
            test=is_valid_opt_routine,
            message=f"'deterministic_optimization_routine' is not acceptable/valid!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # Validate `uncertain_parameter_sample`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="uncertain_parameter_sample",
            test=is_valid_sample,
            message=f"'uncertain_parameter_sample' is not acceptable/valid!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # Validate `uncertain_parameter_bounds`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="uncertain_parameter_bounds",
            test=is_valid_bounds,
            message=f"'uncertain_parameter_bounds' is not acceptable/valid!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Check parent for further validation (if any)
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid)

    def _standardize_uncertain_parameter_sample(self, sample):
        """
        Convert acceptable sample of the uncertin parameter into valid format

        :returns: list of the form ``[u1, u2, ...]`` where each entry is a 1d numpy array of
            size equal to the size of the uncertian parameter, that is
            `self.configurations.uncertain_parameter_size`.
        """
        if utility.isiterable(sample):
            if len(sample) <1 :
                raise TypeError(
                    f"Uncertain parameter sample cannot be empty!"
                )
            else:
                sample = [np.asarray(s).flatten() for s in sample]
                if not all(
                        [
                            s.size==self.uncertain_parameter_size
                            for s in sample
                        ]
                ):
                    raise TypeError(
                        f"Each sample point in the uncertain parameter sample is expected to be "
                        f"of size {self.uncertain_parameter_size}; received sample "
                        f"with inconsistend and/wrong sizes"
                    )
        else:
            raise TypeError(
                f"Each sample point in the uncertain parameter sample is expected to be "
                f"of size {self.uncertain_parameter_size}; received sample "
                f"with inconsistend and/wrong sizes"
            )
        return sample

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Check if 'uncertain_parameter_bounds' is passed
        if 'uncertain_parameter_bounds' in kwargs:
            self.configurations.uncertain_parameter_bounds = standardize_domain_bounds(
                size=self.uncertain_parameter_size,
                bounds=kwargs['uncertain_parameter_bounds'],
            )

        # Check if 'uncertain_parameter_sample' is passed; update configurations and local tracker
        if 'uncertain_parameter_sample' in kwargs:
            self.configurations.uncertain_parameter_sample = self._standardize_uncertain_parameter_sample(
                kwargs['uncertain_parameter_sample']
            )

    def create_stochastic_optimizer(self):
        return BinaryReinforceOptimizer(
            configs=self._get_stochastic_optimization_configurations()
        )

    def generate_design_sample(
        self,
        theta=None,
        sample_size=1,
        dtype=bool,
    ):
        if theta is None:
            return self.Bernoulli_random_sampler.sample(
                sample_size=sample_size,
                antithetic=self.configurations.antithetic,
                dtype=dtype,
            )
        else:
            return super().generate_sample(
                theta=theta,
                sample_size=sample_size,
            )

    def generate_uncertain_parameter_sample(
        self,
        sample_size=1,
    ):
        """
        Create a sample (list) of randomly generated uncertain parameter values based on
        the uncertain parameter configurations.

        ..note::
            This function is not supposed (does not need to be) directly called by the user.
            It is only created to help validation/verification of configurations.
        """
        domain = [v for v in self.uncertain_parameter_bounds]
        for i, (lb, ub) in enumerate(domain):
            if (lb is ub is None) or (np.isinf(lb) and np.isinf(ub)):
                lb = np.inf
                ub = np.inf
            elif lb is None or np.isinf(lb):
                lb = ub - 1.0
            elif ub is None or np.isinf(ub):
                ub = lb + 1.0
            else:
                pass
            # Update
            domain[i] = (lb, ub)

        sample = []
        center = np.mean(domain, 1)
        for _ in range(sample_size):
            sample.append(
                center
                + self.random_number_generator.uniform(
                    size=self.uncertain_parameter_size
                )
            )
        return sample

    def objective_function_value(
        self,
        design,
        param,
    ):
        """
        A function to prevent redundant computations of the objective (utility function)
        by looking up tracker before evaluation

        :param design: binary state/design variable to evaluate objective value at
        :param param: a realization of the unceratin parameter

        :returns: value of the objective/utility function at the passed design

        :raises: :py:class:`TypeError` if the passed design is not of the right size
        """
        design = utility.asarray(design, dtype=bool)
        param = utility.asarray(param, dtype=float)
        sh = self._hash_dp(d=design, p=param)
        try:
            objval = self.objective_value_tracker[sh]["objective_value"]
            if self.debug:
                print("DEBUG: Objective function value; Found; loaded", design, param)

        except KeyError:
            if self.debug:
                print(
                    "DEBUG: Objective function value; Not Found; Evaluating",
                    design,
                    param,
                )

            # Evaluate and update tracker
            objval = self.configurations.fun(design, param)
            objval_dict = {
                sh: {
                    "design": design,
                    "uncertain_parameter": param,
                    "objective_value": objval,
                },
            }
            self.objective_value_tracker.update(objval_dict)

        return objval

    def objective_function_grad_design(
        self,
        design,
        param,
    ):
        """
        A function to evaluate the gradient of the objective function at the passed design,
        and uncertain parameter value, with respect to the *relaxed* design.
        This function is used only for relaxation approach of the binary design,
        and is used only for comparison with the *incorrect* relaxation approach
        for the outer optimization problem.

        ..note::
            If no valid function is registered in the configurations, finite differences is used to
            approximate the gradient

        :param design: binary state/design variable to evaluate objective value at
        :param param: a realization of the unceratin parameter

        :returns: derivative of the objective/utility function with respect to teh design at the passed design

        :raises: :py:class:`TypeError` if the passed design and/or uncertain parmaeter are not of the right sizes

        """
        fun_grad_design = self.configurations.fun_grad_design
        if fun_grad_design is None:
            return utility.finite_differences_gradient(
                fun=lambda d: self.objective_function_value(design=d, param=param),
                state=design,
            )
        else:
            return fun_grad_design(design, param)

    def objective_function_grad_parameter(
        self,
        design,
        param,
    ):
        """
        A function to evaluate the gradient of the objective function at the passed design,
        and uncertain parameter value with respect to the uncertain parameter.

        ..note::
            If no valid function is registered in the configurations, finite differences is used to
            approximate the gradient

        :param design: binary state/design variable to evaluate objective value at
        :param param: a realization of the unceratin parameter

        :returns: derivative of the objective/utility function with respect to teh design at the passed design

        :raises: :py:class:`TypeError` if the passed design and/or uncertain parmaeter are not of the right sizes

        """
        fun_grad_parameter = self.configurations.fun_grad_parameter
        if fun_grad_parameter is None:
            return utility.finite_differences_gradient(
                fun=lambda p: self.objective_function_value(design=design, param=p),
                state=param,
            )
        else:
            return fun_grad_parameter(design, param)

    def objective_function_sample_min(
        self,
        design,
        parameter_sample,
    ):
        """
        Find minium value of the utility/objective function `fun(d, p)`
        over finite sample of the uncertainty parameter `parameter_sample`; for the given `design`.
        """
        return min(
            [
                self.objective_function_value(
                    design=design,
                    param=p,
                )
                for p in parameter_sample
            ]
        )

    def objective_function_sample_min_grad_design(
        self,
        design,
        parameter_sample,
    ):
        """
        Find the generalized gradient of `deterministic_objective_min` with respect to the design,
        which is the gradient at the param value that minimizes `deterministic_objective`
        over the parameter sampler.

        ..note::
            This is used for the relaxation approach only.
        """
        objvals = [
            self.objective_function_value(
                design=design,
                param=p,
            )
            for p in parameter_sample
        ]
        # Evaluate the gradient at the parameter that corresponds to the minimum objective value in the sample
        return self.objective_function_grad_design(
            design=design,
            param=parameter_sample[np.argmin(objvals)],
        )

    def stochastic_objective_estimate(self, param, theta=None, design_sample=None):
        """
        Estimate the value of the the stochastic objective function :math:`(E(U(d, p)))`, where :math:`U`
        is the utility/objective function, :math:`d` is the design, and :math:`p` is the uncertain
        parmater value.
        The estimate is found by Monte Carlo, that is by  averaging over finite sample (design_sample)
        sampled from Bernoulli distribution with parameter `theta`.
        This is the objctive of the inner minimization problem
        """
        if theta is design_sample is None:
            raise ValueError(
                f"Must either pass the Bernoulli parmeter or the sampled desings!"
            )

        if design_sample is None:
            stoch_sample_size = self.configurations.stochastic_gradient_sample_size
            self.Bernoulli_random_sampler.success_probability = theta
            design_sample = self.Bernoulli_random_sampler.sample(
                sample_size=stoch_sample_size,
                antithetic=self.configurations.antithetic,
                dtype=int,
            )
        objval = 0.0
        for design in design_sample:
            objval += self.objective_function_value(design=design, param=param)
        objval /= len(design_sample)
        return objval

    def stochastic_objective_estimate_grad_parameter(self, param, design_sample):
        """
        Gradient of the function `stochastic_objective_estimate` with respect to the uncertain parameter `param`.
        This is the gradient of the inner minimization problem
        """
        grad = 0.0
        for d in design_sample:
            grad += self.objective_function_grad_parameter(
                design=d,
                param=param,
            )
        grad /= len(design_sample)
        return grad

    def stochastic_objective_estimate_min(
        self,
        parameter_sample,
        theta=None,
        design_sample=None,
    ):
        """
        Find minium of (estimates of the) stochastic objective :math:`(E(U(d, p)))` over finite sample
        of the uncertainty parameter; for the same sample of designs.
        This is the objective of the outer maximization problem in stochastic settings; No gradient of
        this is required as REINFORCE is used.
        """
        if theta is design_sample is None:
            raise ValueError(
                f"Must either pass the Bernoulli parmeter or the sampled desings!"
            )

        if design_sample is None:
            stoch_sample_size = self.configurations.stochastic_gradient_sample_size
            self.Bernoulli_random_sampler.success_probability = theta
            design_sample = self.Bernoulli_random_sampler.sample(
                sample_size=stoch_sample_size,
                antithetic=self.configurations.antithetic,
                dtype=int,
            )
        stoch_objvals = [
            self.stochastic_objective_estimate(
                param=p,
                theta=theta,
                design_sample=design_sample,
            )
            for p in parameter_sample
        ]
        return np.min(stoch_objvals)

    def _hash_dp(self, d, p):
        """
        Create a uniqueu hashed value of design d and parameter p so it can be used for indexing
        This is only internal, and is used to create a dictionary for objective values to track.
        The dictionary values will hold both the design and the uncertain parameter and design as the hash is
        not reversible
        """
        dec = self.configurations.uncertain_parameter_max_num_digits
        dp = np.concatenate(
            (
                np.round(utility.asarray(d, dtype=float), dec),
                np.round(utility.asarray(p, dtype=float), dec),
            )
        )
        sh = hashlib.sha256(dp.tobytes()).hexdigest().upper()
        return sh

    def _check_convergence(
        self,
        tol,
        curr_obj=None,
        prev_obj=None,
        curr_design=None,
        prev_design=None,
        curr_uncertain_param=None,
        prev_uncertain_param=None,
    ):
        """
        A function that checks convergence of the optimization algorithm based on the following:
          - Change in the objective value, the design, and the uncertain parameter at the
            current iteration compared to the previous

        :returns:
            - a boolean `converged` : True if the algorithm has converged to a local optimum
              (no change), and returns False otherwise
            - string msg: message describing which of the comparisons is met
        """
        # Initialize the flag to return (convergence)
        converged = False

        # Check objective value change (if update is withing passed tolerance then no notable change happens)
        if not (curr_obj is None or prev_obj is None):
            objval_changed = abs(curr_obj - prev_obj) >= tol

        # Check change in the outer problem variable (relaxed design or activation probability)
        if not (curr_design is None or prev_design is None):
            design_changed = np.linalg.norm(curr_design - prev_design) >= tol

        # Check change in the inner problem variable (uncertain param)
        if not (curr_uncertain_param is None or prev_uncertain_param is None):
            uncertain_parameter_changed = (
                np.linalg.norm(curr_uncertain_param - prev_uncertain_param) >= tol
            )

        if objval_changed or design_changed or uncertain_parameter_changed:
            converged = False
        else:
            converged = True

        msg = f"Converged: {converged}\n"
        msg += f"Ojective Changed: {objval_changed}\n"
        msg += f"Design Changed: {design_changed}\n"
        msg += f"Uncertain Parameter Changed: {uncertain_parameter_changed}\n"

        return converged, msg

    def stochastic_policy_objective_value(
        self,
        theta,
        param,
        fun=None,
        sample=None,
        sample_size=32,
        exact=False,
    ):
        """
        Evaluate the value (exact expectation or approximate using MC sampling) of
        the stochastic objective function reformulation of the binary optimization problem.

        Here, we define the stochastic objective function :math:`E(J(x, p))` where :math:`U`
        is the deterministic objective `fun` (utility function) defined in the configurations,
        and :math:`x` is a binary design variable following a multivariate Bernoulli
        distribution with parameter (success probability) :math:`\\theta`, and :math:`p` is the
        realization of the uncertain parametr.

        If `exact` is `False, this function returns a Monte Carlo (MC) approximation of
        the stochastic objective (stochastic average approximation).
        `If `exact` is `True``, this function evaluates the expectation by enumeration
        (over all possible designs) and returns the expected value.
        This option is provided for testing purposes.

        ..note::
            This function is a wrapper around the same function in the super class
            :py:meth:`stochastic_policy_objective_value` which just modifies the objective
            function by setting the uncertain parameter before calling super's functionality.

        :param numpy.ndarray|iterable theta: the policy parameters (success probability of
            the underlying Bernoulli distribution)
        :param param: the uncertain parameter
        :param callable fun: the utility/objective function to calculate the
            stochastic objective. If `None`, the `fun` from the configurations dictionary
            is used (that is obtained by calling :py:meth:`self.objective_function_value`).
            This argument is provided to enable altering the objective while calculating
            the derivatives (e.g., by adding a baseline) to reduce variance of the estimator(s).
        :param numpy.ndarray sample: a sample generated from the passed policy (provided
            in case samples are generated by other routines). This is discarded if `exact`
            is `True`. Each row is one sample point.
        :param int sample_size: number of sample points to generate (if `sample` is `None`
            and `exact` is `False`). This is ignored if `sample` is not `None` (`sample_size`
            is updated to the length of the provided sample in this case ) or `exact` is `True`
        :param bool exact: whether to evaluate the exact (expected value over all possible
            binary designs) objective value or a Monte Carlo approximation (stochastic average
            approximation) of the objective/expectation.

        :returns: value of the stochastic objective value (exact or approximate)
        :rtype: float
        """
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().stochastic_policy_objective_value(
            theta=theta,
            fun=lambda d: fun(d, param),
            sample=sample,
            sample_size=sample_size,
            exact=exact,
        )

    def stochastic_policy_objective_gradient(
        self,
        theta,
        param,
        fun=None,
        sample=None,
        sample_size=32,
        exact=False,
    ):
        """
        Evaluate the gradient (exact expectation gradient or approximate using MC sampling) of
        the stochastic objective function reformulation of the binary optimization problem.
        This evaluates the gradient of :py:meth:`stochastic_policy_objective_value` with respect
        to the policy parameter `theta`, that is the success/activate probability.
        The uncertain parameter `param` is assumed to be identical/fixed (for both function and gradient)

        If `exact` is `False, this function returns a Monte Carlo (MC) approximation of
        the gradient of the stochastic objective (stochastic average approximation).
        `If `exact` is `True`, this function evaluates the expectation gradient by enumeration
        (over all possible designs) and returns the gradient of the expected value.
        This option is provided for testing purposes.

        ..note::
            This function is a wrapper around the same function in the super class
            :py:meth:`stochastic_policy_objective_gradient` which just modifies the objective
            function by setting the uncertain parameter before calling super's functionality.

        :param numpy.ndarray|iterable theta: the policy parameters (success probability of
            the underlying Bernoulli distribution)
        :param param: the uncertain parameter
        :param callable fun: the utility/objective function to calculate the
            stochastic objective. If `None`, the `fun` from the configurations dictionary
            is used (that is obtained by calling :py:meth:`self.objective_function_value`).
            This argument is provided to enable altering the objective while calculating
            the derivatives (e.g., by adding a baseline) to reduce variance of the estimator(s).
        :param numpy.ndarray sample: a sample generated from the passed policy (provided
            in case samples are generated by other routines). This is discarded if `exact`
            is `True`. Each row is one sample point.
        :param int sample_size: number of sample points to generate (if `sample` is `None`
            and `exact` is `False`). This is ignored if `sample` is not `None` (`sample_size`
            is updated to the length of the provided sample in this case ) or `exact` is `True`
        :param dict obj_val_tracker: optional dictionary holding values of the
            objective function (will be updated in-place if passed). Default is
            `None`.
        :param bool apply_projection: if `True`, the gradient is projected (by truncation)
            onto the box [-1,1]. Default is `False`.
        :param bool exact: whether to evaluate the exact (expected value over all possible
            binary designs) objective value or a Monte Carlo approximation (stochastic average
            approximation) of the objective/expectation.

        :returns: gradient of the stochastic objective value (exact or approximate)
        :rtype: numpy.ndarray
        """
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().stochastic_policy_objective_gradient(
            theta=theta,
            fun=lambda d: fun(d, param),
            sample=sample,
            sample_size=sample_size,
            exact=exact,
        )

    def optimal_baseline_estimate(
        self,
        theta,
        param,
        fun=None,
        sample=None,
        batch_size=32,
        epochs=10,
    ):
        """
        Use the passed sample to calculate a sample based estimate of the optimal
        baseline value.

        ..note::
            This function is a wrapper around the same function in the super class
            :py:meth:`optimal_baseline_estimate` which just modifies the objective
            function by setting the uncertain parameter before calling super's functionality.

        ..note::
            This function is not employed in the optimization at all.
            It is just added to avoid an confusion in arguments with the super class.


        :param numpy.ndarray|iterable theta: the policy parameters (success probability of
            the underlying Bernoulli distribution)
        :param param: the uncertain parameter
        :param callable fun: the utility/objective function to calculate the
            stochastic objective. If `None`, the `fun` from the configurations dictionary
            is used. This argument is provided to enable altering the objective while calculating
            the derivatives (e.g., by adding a baseline) to reduce variance of the estimator(s).
        :param sample: Given sample
        :param int batch_size: number of samples to use for the MC approximation.
            Default is 32.
        :param int epochs: number of epochs to use to approximate gradient. Default
            is 10.

        :remarks: If more than one epoch is required:

              - if sample is None, batch_size is generated for each epochs
              - if sample is not None, batch_size is derived as: `sample // epochs`

        :returns: optimal baseline estimate
        :rtype: ``float``
        """
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().optimal_baseline_estimate(
            theta=theta,
            fun=lambda d: fun(d, param),
            sample=sample,
            batch_size=batch_size,
            epochs=epochs,
        )

    def bruteforce_objective_values(
        self,
        param,
        fun=None,
        num_active=None,
    ):
        """
        Evaluate the value  of the objective (obj) at all possible binary combinations for a
        given realization of the uncertain parameter;
        """
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().bruteforce_objective_values(
            fun=lambda d: fun(d, param),
            num_active=num_active,
        )

    def full_Hessian_estimate(
        self,
        theta,
        fun=None,
        alpha=1.0,
        sample=None,
        sample_size=32,
    ):
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().full_Hessian_estimate(
            theta=theta,
            fun=lambda d: fun(d, param),
            alpha=alpha,
            sample=sample,
            sample_size=sample_size,
        )

    def Hessian_estimate_vector_prod(
        self,
        theta,
        param,
        x,
        fun=None,
        alpha=1.0,
        sample=None,
        sample_size=32,
    ):
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super().Hessian_estimate_vector_prod(
            theta=theta,
            x=x,
            fun=lambda d: fun(d, param),
            alpha=alpha,
            sample=sample,
            sample_size=sample_size,
        )

    def _Hessian_estimate_row(
        self,
        theta,
        param,
        row_ind,
        fun=None,
        sample=None,
        sample_size=32,
    ):
        # Check the objective function
        if fun is None:
            fun = self.objective_function_value

        return super()._Hessian_estimate_row(
            theta=theta,
            row_ind=row_ind,
            fun=lambda d: fun(d, param),
            sample=sample,
            sample_size=sample_size,
        )

    def _get_stochastic_optimization_configurations(
        self,
        uncertain_parameter_sample=None,
    ):
        """
        Create a configurations object with configurations from super().
        This is useful to pass proper configurations
        for the solver of the outer optimization if stochastic learning is used.
        Note that the objective value tracker used in the overall process is different from that
        used in the upper level process. Also, the objective used in super only accepts design.
        Such discrepancies are handled here...
        """
        # StochConfigsClass = self.configurations_class.__bases__[0]
        StochConfigsClass = BinaryReinforceOptimizerConfigs

        # If no uncertain parameter sample is passed, use current one
        if uncertain_parameter_sample is None or \
                len(uncertain_parameter_sample) == 0:
            uncertain_parameter_sample = self.configurations.uncertain_parameter_sample

        # Extract a dictionary (deep-copy) from current configurations with keys known to parent
        # NOTE: Some entries might be copied unnecessarily, but we need to make sure all configs are ported.
        stoch_configs = remove_unknown_keys(
            self.configurations.asdict(deep=True, ignore_error=True, ),
            StochConfigsClass,
        )

        # Update configurations (based on internal configs)
        stoch_configs.update(
            {
                'fun': lambda design: self.objective_function_sample_min(
                    design=design,
                    parameter_sample=uncertain_parameter_sample,
                ),
                'objective_value_tracker': {},
                'maxiter': self.configurations.outer_opt_maxiter,
                'maximize': self.configurations.maxmin,
            }
        )

        # Create Configs Object and return it
        return StochConfigsClass(**stoch_configs)

    def solve(
        self,
        initial_guess=None,
        parameter_sample=None,
    ):
        """
        Start solving the robust binary optimization problem using the stochastic
        or relaxed formulatioon based on the configurations.

        .. note::
            This function only makes a choice between :py:meth:`self._solve_by_relaxation`
            and `self._solve_by_stochastic_learning` based on the configuration attribute
            `self.configurations.solve_by_relaxation`.
            These methods all have the same signature/input/output

        :param initial_policy: `None or ``iterable`` to be used as the initial policy
            parameter of the underlying Bernoulli distribution
        :param parameter_sample: `None or ``iterable`` to be used as the initial
            ensemble of the uncertain parameter.
            If `None`, the uncertain parameter sample registered in the configurations
            dictionary under key `uncertain_parameter_sample` is used.

        :returns: an instance of :py:class:`RobustBinaryReinforceOptimizerResults`.

        :raises: :py:class:`TypeError` if the initial policy has wrong size or any of
          the values fall outside the interval [0, 1]
        """
        # Retrieve and assert/validate required configurations
        if self.configurations.solve_by_relaxation:
            return self._solve_by_relaxation(
                initial_guess=initial_guess,
                parameter_sample=parameter_sample,
            )
        else:
            return self._solve_by_stochastic_learning(
                initial_guess=initial_guess,
                parameter_sample=parameter_sample,
            )

    def _solve_by_relaxation(
        self,
        initial_guess=None,
        parameter_sample=None,
    ):
        """
        Start solving the robust binary optimization problem by relaxtion over
        the binary variable (outer optimization problem).

        The signature/input/output of this function is identical to :py:meth:`solve`

        .. note::
            This function is not to be called directly by user. Instead call :py:meth:`solve`.
            Only call this function directly if you want to ignore
            `self.configurations.solve_by_relaxation` flag.
        """
        # Retrieve and assert/validate required configurations
        maxmin = self.configurations.maxmin
        deterministic_optimization_routine = self.configurations.deterministic_optimization_routine
        reset_initial_guess = self.configurations.reset_initial_guess
        maxiter = self.configurations.maxiter
        outer_opt_maxiter = self.configurations.outer_opt_maxiter
        inner_opt_maxiter = self.configurations.inner_opt_maxiter
        tol = self.configurations.tol
        uncertain_parameter_bounds = self.configurations.uncertain_parameter_bounds

        # Initial guess (policy)
        if initial_guess is None:
            initial_guess = self.configurations.initial_policy

        # Uncertain parameter sample (copy) and its average
        if parameter_sample is None or \
                len(parameter_sample) == 0:
            parameter_sample = [
                utility.asarray(s) for s in self.configurations.uncertain_parameter_sample
            ]
        uncertain_parameter_sample = [utility.asarray(v).flatten() for v in parameter_sample]
        init_uncertain_param = 0.0
        for s in uncertain_parameter_sample:
            init_uncertain_param += np.asarray(s).flatten()
        init_uncertain_param /= len(uncertain_parameter_sample)
        opt_uncertain_param = init_uncertain_param.copy()

        # Design (outer optimization variable)
        init_design = utility.asarray(initial_guess).flatten()
        opt_design = init_design.copy()
        design_bounds = [(0, 1)] * opt_design.size

        # Initialize Trackers/Placeholders
        optimization_trajectory = []
        optimization_trajectory_objval = []
        optimization_trajectory_stoch_objval = []
        outer_optimization_results = []
        inner_optimization_results = []

        # Monitoring/Progress
        iter_counter = 0
        uncertain_parameter_update_norm = np.inf
        convergence_criterion_met = False
        convergence_message = "No Optimization Iterations Carried Out"

        # Start solving by iterating over Max/Min problems with/without relaxation
        if self.verbose:
            print("Solve the Robust OED MaxMin Optimization Problem by Relaxation")

        ## Loop over iterations (outer optimization iterations)
        sep = f"\n{'*'*50} \n"
        for itr_ind in range(maxiter):
            print(f"{sep}>> Robust Optimization Iteration: {itr_ind+1} / {maxiter}{sep}")

            # Initialize Trackers (first iteration)
            if itr_ind == 0:
                # Update the optimization trajectory
                init_det_objval = self.objective_function_sample_min(
                    design=init_design,
                    parameter_sample=[init_uncertain_param],
                )
                optimization_trajectory.append(
                    (init_design.copy(), init_uncertain_param.copy())
                )
                optimization_trajectory_stoch_objval.append(np.nan)
                optimization_trajectory_objval.append(init_det_objval)

            ## Solve the Outer Optimization (based on maxmin) using the det. opt. method
            print(
                f"---\n Solving OUTER ({'max' if maxmin else 'min'}) Problem;\n--- "
            )

            ## Update the definition of the objective function
            ##   (minimum over finite sample of uncertain parameter)
            ##   (given the Updated parameter_sample; see end of the loop)
            det_fun = lambda design: (
                -1.0 if maxmin else 1.0
            ) * self.objective_function_sample_min(
                design=design,
                parameter_sample=uncertain_parameter_sample,
            )

            det_fun_grad = lambda design: (
                -1.0 if maxmin else 1.0
            ) * self.objective_function_sample_min_grad_design(
                design=design,
                parameter_sample=uncertain_parameter_sample,
            )

            if self.verbose:
                callback = lambda x: print("Design; obj; grad: ", x, det_fun(x))
            else:
                callback = None

            if re.match(
                r"\A(sp|scipy)*( |-|_)*(L)*( |-|_)*BFGS( |-|_)*(B)*\Z",
                deterministic_optimization_routine,
                re.IGNORECASE,
            ):
                # Solve using scipy-based LBFGS-B optimizer
                res = sp_optimize.minimize(
                    fun=det_fun,
                    x0=opt_design,
                    jac=det_fun_grad,
                    method="L-BFGS-B",
                    bounds=design_bounds,
                    callback=callback,
                    options=dict(
                        disp=self.verbose,
                        maxiter=outer_opt_maxiter,
                        ftol=tol,
                    ),
                )

                # Update innter optimization results tracker
                # TODO: This needs to be udpated once a ScipyOptimizer interface is implemenetd.
                outer_optimization_results.append(res)

                # Extract optimization results
                opt_design = res["x"]

                if self.debug or self.verbose:
                    print(f"  Polyak's Outer Optimization Problem Results:")
                    print(f"\t  Success:   {res['success']}")
                    print(f"\t  Status:    {res['status']}")
                    print(f"\t  Message:   {res['message']}")
                    print(f"\t  Optimal design: {res['x']}")

            else:
                raise ValueError(
                    f"Unsupported optimization routine '{deterministic_optimization_routine}'"
                )

            # Update trajecgtory (monitor iterations for convergence tests and reporting)
            iter_det_objval = self.objective_function_sample_min(
                design=opt_design,
                parameter_sample=[opt_uncertain_param],
            )
            optimization_trajectory.append(
                (opt_design.copy(), opt_uncertain_param.copy())
            )
            optimization_trajectory_objval.append(iter_det_objval)
            optimization_trajectory_stoch_objval.append(np.nan)

            ##
            # Now, solve teh inner optimization problem (min if maxmin otherwise max)
            # Update the design signature of the inner optimization objective (and gradient) given the optimal design
            fun = lambda param: (
                1.0 if maxmin else -1.0
            ) * self.objective_function_value(
                param=param,
                design=opt_design,
            )
            parameter_grad = lambda param: (
                1.0 if maxmin else -1.0
            ) * self.objective_function_grad_parameter(
                design=opt_design,
                param=param,
            )

            # Solve inner min problem for the uncertain parameter given the new policy
            print(
                f"\n---\n Solving the INNER ({'min' if maxmin else 'max'}) Problem using"
                f" {deterministic_optimization_routine} \n---\n"
            )
            if self.debug:
                print(f"DEBUG: in Polyak inner: bounds: {uncertain_parameter_bounds}")
                print(f"DEBUG: in Polyak inner: init_param: {opt_uncertain_param}")
                print(f"DEBUG: Starting the optimization procedure...")

            if self.verbose:
                callback = lambda x: print(
                    "Parameter; obj; grad: ", x, fun(x), parameter_grad(x)
                )
            else:
                callback = None

            if re.match(
                r"\A(sp|scipy)*( |-|_)*(L)*( |-|_)*BFGS( |-|_)*(B)*\Z",
                deterministic_optimization_routine,
                re.IGNORECASE,
            ):
                res = sp_optimize.minimize(
                    fun=fun,
                    x0=opt_uncertain_param,
                    jac=parameter_grad,
                    method="L-BFGS-B",
                    bounds=uncertain_parameter_bounds,
                    callback=callback,
                    options=dict(
                        disp=self.verbose,
                        maxiter=outer_opt_maxiter,
                        ftol=tol,
                    ),
                )

                # Extract optimization results
                uncertain_parameter_update_norm = np.linalg.norm(
                    opt_uncertain_param - res["x"],
                    ord=2,
                )
                opt_uncertain_param = res["x"]

                # Update innter optimization results tracker
                inner_optimization_results.append(res)

                if self.debug or self.verbose:
                    print(f"  Polyak's Inner Optimization Problem Results:")
                    print(f"\t  Success:   {res['success']}")
                    print(f"\t  Status:    {res['status']}")
                    print(f"\t  Message:   {res['message']}")
                    print(f"\t  Optimal x: {res['x']}")

            else:
                raise ValueError(
                    f"Unsupported optimization routine '{deterministic_optimization_routine}'"
                )

            # Check if the optimal value of the uncertain parameter is inside the current sample; append if not
            found = False
            for p in uncertain_parameter_sample:
                if np.allclose(opt_uncertain_param, p):
                    found = True
                    break
            if not found:
                uncertain_parameter_sample.append(opt_uncertain_param.copy())

            if self.debug:
                if found:
                    print(
                        "Optimal value of the uncertain parameter found in the current"
                        " set!"
                    )
                else:
                    print(f"Expanded the parameter sample with {opt_uncertain_param}")

            # Update optimization trajectory
            iter_det_objval = self.objective_function_sample_min(
                design=opt_design,
                parameter_sample=[opt_uncertain_param],
            )
            optimization_trajectory.append(
                (opt_design.copy(), opt_uncertain_param.copy())
            )
            optimization_trajectory_stoch_objval.append(np.nan)
            optimization_trajectory_objval.append(iter_det_objval)

            # Check Convergence (Updates of design/policy, uncertainparameter, and objective value)
            convergence_criterion_met, convergence_message = self._check_convergence(
                tol=tol,
                curr_obj=optimization_trajectory_objval[-1],
                prev_obj=optimization_trajectory_objval[-2],
                curr_design=optimization_trajectory[-1][0],
                prev_design=optimization_trajectory[-2][0],
                curr_uncertain_param=optimization_trajectory[-1][-1],
                prev_uncertain_param=optimization_trajectory[-2][-1],
            )

            # Update counter
            iter_counter += 1

            # Break if converged
            if convergence_criterion_met:
                if self.verbose:
                    print("\n***Polyak's Algorithm Converged; Terminating...***")
                break

        convergence_message += f"Iteration: {iter_counter}/{maxiter}\n"
        if not convergence_criterion_met:
            print("Polyak's Algorithm did NOT converge to a local optimum point!")

        elif self.verbose:
            print(
                "Polyak's Algorithm has converged to a local optimum hyperparameter"
                " point"
            )

        # Set values of the stochastic approach as appropriate as possible
        optimal_policy = None
        optimal_policy_sample = None
        optimal_policy_sample_objval = None

        optimal_design = optimization_trajectory[-1][0]
        optimal_design_objval = optimization_trajectory_objval[-1]

        #
        # End solution following the relaxed OED formalism
        #

        if self.verbose:
            print("Done... Exiting Polyak's Algorithm...")

        # Prepare a results object
        results = RobustBinaryReinforceOptimizerResults(
            optimal_policy=optimal_policy,
            optimal_policy_sample=optimal_policy_sample,
            optimal_policy_sample_objval=optimal_policy_sample_objval,
            optimal_design=optimal_design,
            optimal_design_objval=optimal_design_objval,
            optimization_trajectory=optimization_trajectory,
            optimization_trajectory_objval=optimization_trajectory_objval,
            optimization_trajectory_stoch_objval=optimization_trajectory_stoch_objval,
            inner_optimization_results=inner_optimization_results,
            outer_optimization_results=outer_optimization_results,
            converged=convergence_criterion_met,
            maxiter_reached=(iter_counter==maxiter),
            message=convergence_message,
            uncertain_parameter_sample=uncertain_parameter_sample,
            optimal_uncertain_parameter=opt_uncertain_param,
            objective_value_tracker=self.objective_value_tracker.copy(),
            configurations=self.configurations.asdict(deep=True, ignore_error=True, ),
        )
        return results

    def _solve_by_stochastic_learning(
        self,
        initial_guess=None,
        parameter_sample=None,
    ):
        """
        Start solving the robust binary optimization problem by stochastic learning
        (probabilistic optimization) over the the binary variable (outer optimization problem).

        The signature/input/output of this function is identical to :py:meth:`solve`

        .. note::
            This function is not to be called directly by user. Instead call :py:meth:`solve`.
            Only call this function directly if you want to ignore
            `self.configurations.solve_by_relaxation` flag.
        """
        # Retrieve and assert/validate required configurations
        maxmin = self.configurations.maxmin
        deterministic_optimization_routine = self.configurations.deterministic_optimization_routine
        reset_initial_guess = self.configurations.reset_initial_guess
        maxiter = self.configurations.maxiter
        outer_opt_maxiter = self.configurations.outer_opt_maxiter
        inner_opt_maxiter = self.configurations.inner_opt_maxiter
        tol = self.configurations.tol
        uncertain_parameter_bounds = self.configurations.uncertain_parameter_bounds

        # Initial guess (policy)
        if initial_guess is None:
            initial_guess = self.configurations.initial_policy
        init_policy = utility.asarray(initial_guess).flatten()
        optimal_policy = init_policy.copy()

        # Uncertain parameter sample (copy) and its average
        if parameter_sample is None or \
                len(parameter_sample) == 0:
            parameter_sample = [
                utility.asarray(s) for s in self.configurations.uncertain_parameter_sample
            ]
        uncertain_parameter_sample = [utility.asarray(v).flatten() for v in parameter_sample]
        init_uncertain_param = 0.0
        for s in uncertain_parameter_sample:
            init_uncertain_param += np.asarray(s).flatten()
        init_uncertain_param /= len(uncertain_parameter_sample)
        opt_uncertain_param = init_uncertain_param.copy()

        # Design (outer optimization variable)
        init_design = utility.asarray(initial_guess).flatten()
        opt_design = init_design.copy()
        design_bounds = [(0, 1)] * opt_design.size

        # Initialize Trackers/Placeholders
        optimization_trajectory = []
        optimization_trajectory_objval = []
        optimization_trajectory_stoch_objval = []
        outer_optimization_results = []
        inner_optimization_results = []

        # Monitoring/Progress
        iter_counter = 0
        uncertain_parameter_update_norm = np.inf
        convergence_criterion_met = False
        convergence_message = "No Optimization Iterations Carried Out"

        # Start solving by iterating over Max/Min problems with/without relaxation
        if self.verbose:
            print(
                f"Solve the Robust OED MaxMin Optimization Problem by "
                f"using the Probabilistic/Stochastic Formulation"
            )

        ## Solve following the stochastic/probabilistic optimization formalism
        # Initialize convergence criterion flag, and iterate over outer loop
        sep = f"\n{'*'*100} \n"
        for itr_ind in range(maxiter):
            print(f"{sep}>> Robust Optimization Iteration: {itr_ind+1} / {maxiter}{sep}")

            # Initialize Trackers (first iteration)
            if itr_ind == 0:
                # Update the optimization trajectory
                init_stoch_objval = self.stochastic_objective_estimate_min(
                    parameter_sample=[init_uncertain_param],
                    theta=init_policy,
                )
                optimization_trajectory.append(
                    (init_policy.copy(), init_uncertain_param.copy())
                )
                optimization_trajectory_objval.append(np.nan)
                optimization_trajectory_stoch_objval.append(init_stoch_objval)

            # Solve the MAXIMIZATION problem using the stochastic Reinforce algorithm
            print(
                f"---\n Solving OUTER ({'max' if maxmin else 'min'}) Problem;\n--- "
            )

            # Update optimizer configurations
            self.stochastic_binary_optimizer.update_configurations(
                **self._get_stochastic_optimization_configurations(
                    uncertain_parameter_sample=uncertain_parameter_sample,
                ).asdict()
            )
            stoch_opt_results = self.stochastic_binary_optimizer.solve(
                initial_policy=init_policy.copy()
                if reset_initial_guess
                else optimal_policy.copy()
            )

            # Update innter optimization results tracker
            outer_optimization_results.append(stoch_opt_results)

            if self.verbose:
                print(
                    f"Outer Maximization problem Converged? {stoch_opt_results.converged}\n"
                    f">> {stoch_opt_results.message}"
                )

            # Extract the optimal policy (optimal Bernoulli parameters) and optimal design (binary/outer variable)
            optimal_policy = stoch_opt_results.optimal_policy
            opt_design = stoch_opt_results.optimal_design

            # Update trajecgtory (monitor iterations for convergence tests and reporting)
            iter_stoch_objval = self.stochastic_objective_estimate_min(
                parameter_sample=[opt_uncertain_param],
                theta=optimal_policy,
            )
            optimization_trajectory.append(
                (optimal_policy.copy(), opt_uncertain_param.copy())
            )
            optimization_trajectory_objval.append(np.nan)
            optimization_trajectory_stoch_objval.append(iter_stoch_objval)

            ## Solve the inner optimization to expand the set of uncertain parameter values
            # Update the design signature of the inner optimization objective (and gradient) given the optimal design
            fun = lambda param: (
                1.0 if maxmin else -1.0
            ) * self.objective_function_value(
                param=param,
                design=opt_design,
            )
            parameter_grad = lambda param: (
                1.0 if maxmin else -1.0
            ) * self.objective_function_grad_parameter(
                design=opt_design,
                param=param,
            )

            # Solve inner min problem for the uncertain parameter given the new policy
            print(
                f"\n---\n Solving the INNER ({'min' if maxmin else 'max'}) Problem using"
                f" {deterministic_optimization_routine} \n---\n"
            )
            if self.debug:
                print(f"DEBUG: in Polyak inner: bounds: {uncertain_parameter_bounds}")
                print(f"DEBUG: in Polyak inner: init_param: {opt_uncertain_param}")
                print(f"DEBUG: Starting the optimization procedure...")

            if self.verbose:
                callback = lambda x: print(
                    "Parameter; obj; grad: ", x, fun(x), parameter_grad(x)
                )
            else:
                callback = None

            # TODO: This needs to be udpated once a ScipyOptimizer interface is implemenetd.
            if re.match(
                r"\A(sp|scipy)*( |-|_)*(L)*( |-|_)*BFGS( |-|_)*(B)*\Z",
                deterministic_optimization_routine,
                re.IGNORECASE,
            ):
                res = sp_optimize.minimize(
                    fun=fun,
                    x0=opt_uncertain_param,
                    jac=parameter_grad,
                    method='L-BFGS-B',
                    bounds=uncertain_parameter_bounds,
                    callback=callback,
                    options=dict(
                        disp=self.verbose,
                        maxiter=inner_opt_maxiter,
                        ftol=tol,
                    ),
                )

                # Update innter optimization results tracker
                inner_optimization_results.append(res)

                # Extract optimization results
                uncertain_parameter_update_norm = np.linalg.norm(
                    opt_uncertain_param - res["x"],
                    ord=2,
                )
                opt_uncertain_param = res["x"]

                if self.debug or self.verbose:
                    print(f"  Polyak's Inner Optimization Problem Results:")
                    print(f"\t  Success:   {res['success']}")
                    print(f"\t  Status:    {res['status']}")
                    print(f"\t  Message:   {res['message']}")
                    print(f"\t  Optimal x: {res['x']}")
            else:
                raise ValueError(
                    f"Unsupported optimization routine '{deterministic_optimization_routine}'"
                )

            # Check if the optimal value of the uncertain parameter is inside the current sample; append if not
            found = False
            for p in uncertain_parameter_sample:
                if np.allclose(opt_uncertain_param, p):
                    found = True
                    break
            if not found:
                uncertain_parameter_sample.append(opt_uncertain_param.copy())

            if self.debug:
                if found:
                    print("Optimal value of the uncertain parameter found in the current set!")
                else:
                    print(f"Expanded the parameter sample with {opt_uncertain_param}")

            # Update trajecgtory (monitor iterations for convergence tests and reporting)
            iter_stoch_objval = self.stochastic_objective_estimate_min(
                parameter_sample=[opt_uncertain_param],
                theta=optimal_policy,
            )
            optimization_trajectory.append(
                (optimal_policy.copy(), opt_uncertain_param.copy())
            )
            optimization_trajectory_objval.append(np.nan)
            optimization_trajectory_stoch_objval.append(iter_stoch_objval)

            if self.debug:
                print("DEBUG: Update from", optimization_trajectory[-2])
                print("DEBUG: Obj Estimate", optimization_trajectory_stoch_objval[-2])

                print("DEBUG: Update to", optimization_trajectory[-1])
                print("DEBUG: Obj Estimate", optimization_trajectory_stoch_objval[-1])

            # Check Convergence (Updates of design/policy, uncertainparameter, and objective value)
            convergence_criterion_met, convergence_message = self._check_convergence(
                tol=tol,
                curr_obj=optimization_trajectory_stoch_objval[-1],
                prev_obj=optimization_trajectory_stoch_objval[-2],
                curr_design=optimization_trajectory[-1][0],
                prev_design=optimization_trajectory[-2][0],
                curr_uncertain_param=optimization_trajectory[-1][-1],
                prev_uncertain_param=optimization_trajectory[-2][-1],
            )

            # Break if converged
            if convergence_criterion_met:
                if self.verbose:
                    print("\n***Polyak's Algorithm Converged; Terminating...***")
                break

            # Update counter
            iter_counter += 1

        convergence_message += f"Iteration: {iter_counter}/{maxiter}\n"
        if not convergence_criterion_met:
            print("Polyak's Algorithm did NOT converge to a local optimum point!")

        elif self.verbose:
            print(
                "Polyak's Algorithm has converged to a local optimum hyperparameter"
                " point"
            )

        # Sample the final policy for the final value of the uncertain parameter
        if self.verbose:
            print("Preparing to return; sampling optimal policy")

        self.Bernoulli_random_sampler.success_probability = optimal_policy
        optimal_policy_sample= self.Bernoulli_random_sampler.sample(
            sample_size=self.configurations.optimal_sample_size,
            antithetic=self.configurations.antithetic,
            dtype=int,
        )
        optimal_policy_sample_objval = [
            self.objective_function_value(design=d, param=opt_uncertain_param)
            for d in optimal_policy_sample
        ]

        # Optimal design
        sorter = np.argmax if self.configurations.maxmin else np.argmin
        winner_ind = sorter(optimal_policy_sample_objval)
        optimal_design = optimal_policy_sample[winner_ind]
        optimal_design_objval = optimal_policy_sample_objval[winner_ind]

        #
        # End solution following the stochastic OED formalism
        #

        if self.verbose:
            print("Done... Exiting Polyak's Algorithm...")

        # Prepare a results object
        results = RobustBinaryReinforceOptimizerResults(
            optimal_policy=optimal_policy,
            optimal_policy_sample=optimal_policy_sample,
            optimal_policy_sample_objval=optimal_policy_sample_objval,
            optimal_design=optimal_design,
            optimal_design_objval=optimal_design_objval,
            optimization_trajectory=optimization_trajectory,
            optimization_trajectory_objval=optimization_trajectory_objval,
            optimization_trajectory_stoch_objval=optimization_trajectory_stoch_objval,
            inner_optimization_results=inner_optimization_results,
            outer_optimization_results=outer_optimization_results,
            converged=convergence_criterion_met,
            maxiter_reached=(iter_counter==maxiter),
            message=convergence_message,
            uncertain_parameter_sample=uncertain_parameter_sample,
            optimal_uncertain_parameter=opt_uncertain_param,
            objective_value_tracker=self.objective_value_tracker.copy(),
            configurations=self.configurations.asdict(deep=True, ignore_error=True, ),
        )
        return results

    def update_bruteforce_results(
        self,
        bruteforce_results=None,
        parameter_sample=None,
        num_active=None,
        saveto=None,
    ):
        """
        Calculate/Update the brute-force results. Bruteforce results are obtained
        here by enumerating all possible designs for each realization of the
        uncertain parameter in the uncertain parameter sample.
        The `bruteforce_results` dictionary (input/output) has the following structure:

            - `design_size`: int,
            - `uncertain_parameter_sample`: list,
            - `results`: dict structured as follows:

              -  "parameter": parameter_val,
              -  "design_size": self.oed_problem.design_size,
              -  "candidates": indexes of binary candidates,
              -  "objective_values": list of objective_values,

        :param dict bruteforce_results: the dictionary cotaining (all/no/partial)
            bruteforce results. If a dictionary is passed, it used as initializer
            of the bruteforce results dictionary to be returned.
        :param parameter_sample: `None or ``iterable`` to be used as the initial
            ensemble of the uncertain parameter.
            If `None`, the uncertain parameter sample is read from `bruteforce_results`
            if valid dictionary is passed. Otherwise, the sample registered in the
            configurations object `self.configurations.uncertain_parameter_sample` is used.
        :param num_active: number of non-zero entries to consider in the bruteforce
            search. If `None`, all realization of the binary variable are considered.
            This is useful for budget constraints.
        :param saveto: filepath to dump the bruteforce dictionary (if passed)

        :returns: an updated (or new) version of `bruteforce_results` with all
            bruteforce search results.
        """
        # Prepare file name and output directory
        if saveto is None:
            saveto = os.path.join(
                self.configurations.output_dir or os.getcwd(),
                "__robust_stochastic_optimization_bruteforce_results.pcl"
            )
        saveto = os.path.abspath(saveto)
        d, f = os.path.split(saveto)
        iter_saveto = os.path.join(d, f"__partial__{f}")
        output_dir = os.path.dirname(saveto)
        if not os.path.isdir(output_dir): os.makedirs(output_dir)

        # Check uncertain parameter sample and read appropriately
        uncertain_parameter_sample = None
        if parameter_sample is not None:
            uncertain_parameter_sample = self._standardize_uncertain_parameter_sample(
                parameter_sample
            )
        elif isinstance(bruteforce_results, dict):
            if 'uncertain_parameter_sample' in bruteforce_results:
                uncertain_parameter_sample = bruteforce_results['uncertain_parameter_sample']
                if uncertain_parameter_sample is not None and len(uncertain_parameter_sample) > 0:
                    uncertain_parameter_sample = self._standardize_uncertain_parameter_sample(
                        uncertain_parameter_sample
                    )
        if uncertain_parameter_sample is None or \
                len(uncertain_parameter_sample) == 0:
            uncertain_parameter_sample = [
                v.copy() for v in self.uncertain_parameter_sample
            ]

        # Initiate bruteforce_results dictionary if None, and update as necessary
        if bruteforce_results is None:
            design_size = self.size
            # uncertain_parameter_sample = self.uncertain_parameter_sample
            bruteforce_results = dict(
                design_size=self.size,
                uncertain_parameter_sample=uncertain_parameter_sample,
                results = dict(),
            )
        else:
            ## Make sure all entries/keys are in the dictionary
            # size/design_size
            if 'design_size' not in bruteforce_results:
                bruteforce_results.update({'design_size': self.size,})
            elif bruteforce_results['design_size'] != self.size:
                warnings.warn(
                    f"The passed bruteforce_results dictionary indicates a problem of size"
                    f"{bruteforce_results['design_size']}. Expected size to be {self.size} \n"
                    f"Updating size in bruteforce_results..."
                )
                bruteforce_results.update({'design_size': self.size,})
            # 'results'
            if 'results' not in bruteforce_results:
                bruteforce_results.update(
                    {
                        'results': dict(),
                    }
                )
            else:
                if not isinstance(bruteforce_results['results'], dict):
                    raise TypeError(
                        f"results entry in `bruteforce_results` is expected to be a dictionary "
                        f"found {type(bruteforce_results['results'])=}"
                    )
                keys = list(bruteforce_results['results'].keys())
                if keys not in [{}, {'parameter', 'design_size', 'candidates', 'objective_values'}]:
                    raise TypeError(
                        f"The structure of results entry in `bruteforce_results` dictionary is "
                        f"invalid. results is a dictionary with {keys=}"
                    )

        # Update the uncertain parmeter sample (standardized version even if present)
        bruteforce_results.update({'uncertain_parameter_sample': uncertain_parameter_sample,})

        ## Bruteforce dictionary ready for update
        if self.verbose:
            print(
                "\n***\nInitiating Brute Force Search***\n"
                "Temporarty/Partial results will be saved to {iter_saveto}\n"
                "Final results will be saved to {saveto}\n"
            )

        # Extract information from the corrected dictionary
        design_size = bruteforce_results["design_size"]
        uncertain_parameter_sample = bruteforce_results["uncertain_parameter_sample"]
        results = bruteforce_results["results"]

        # Loop over all parameter values (in the sample), and update results
        for p_ind, parameter_val in enumerate(uncertain_parameter_sample):
            if self.verbose:
                print(
                    f"\r  -> Enumeration for uncertainty parameter"
                    f" {p_ind+1} / {len(uncertain_parameter_sample)}",
                    end=" ",
                )

            p_bruteforce = self.bruteforce_objective_values(
                param=parameter_val,
                num_active=num_active,
            )

            # Get all keys and sort them, then extract corresponding objective values
            candidates = list(p_bruteforce.keys())
            candidates.sort()
            objective_values = [p_bruteforce[key] for key in candidates]

            # Update dictionary
            results.update({
                p_ind: {
                    "parameter": parameter_val,
                    "design_size": design_size,
                    "candidates": candidates,
                    "objective_values": objective_values,
                },
            })

            # Overwrite partial results (TODO: Maybe scale this up; e.g. x times file_output_iter)
            if ((p_ind+1) % self.configurations.file_output_iter) == 0:
                pickle.dump(bruteforce_results, open(iter_saveto, 'wb'))

        # Save results
        if saveto is not None:
            pickle.dump(bruteforce_results, open(saveto, 'wb'))
            print(f"Robust Binary Optimization: Bruteforce Results dumped/pickled to: {saveto}")
            print(f"Clearning up temporary file...", end="")
            if os.path.isfile(iter_saveto): os.remove(iter_saveto)
            print(f"done...")
        return bruteforce_results

    def exhaustive_parameter_search(
        self,
        uncertain_parameter_sample=None,
        num_test_points=22,
        saveto=None,
    ):
        """
        A function implemented to create data for plotting a 3D surface plot for
        the case when **two** candidate sensors are proposed.
        A surface (of the stochastic objective) is generated for each value
        of the uncertain parameter.

        ..note::
            This function is by no means general.
            It is developed for explainatory purposes and works only for two candidate sensors

        :param uncertain_parameter_sample: Sample of the uncertain parameter to create
            surfaces at. If `None`, the `uncertain_paramaeter_sample` associated with
            the object (in the configurations dictionary) is used.
        :param int num_test_points: number of points in [0, 1] to take for theta
            (success probability) or the relaxed design

        ..note::
            This function is developed for two-dimensional settings, that is when
            both the design is of size `2`.
            This is useful for surface plotting to visualize the optimization problem.

        :raises: :py:class:`TypeError` is raised if the design size is not equal to 2.
        :raises: :py:class:`TypeError` is raised if `oed_results` is not an instance
            of :py:class:`RobustOEDResults`
        :raises: :py:class:`TypeError` is raised if the `oed_results` is not properly
            configured, that is if any of the attributes is invalid (e.g., the OED problem
            is not solved properly before creating the `oed_results` object).

        :returns: a dictionary with the following keys:
            - `'uncertain_parameter_sample'`:
            - `'design_grid'`:
            - `'policy_grid'`:
            - `'stochastic_objective_values'`:
            - `'objective_values'`:
        """
        # Load information/settings & assert
        if self.size != 2:
            raise TypeError(
                "This is only for two candidate sensors. This problem has"
                f" {self.size=}, e.g., candidate sensor locations!"
            )

        solve_by_relaxation = self.configurations.solve_by_relaxation
        if uncertain_parameter_sample is None or \
                len(uncertain_parameter_sample) == 0:
            uncertain_parameter_sample = [
                v.copy() for v in self.uncertain_parameter_sample
            ]
        else:
            uncertain_parameter_sample = self._standardize_uncertain_parameter_sample(
                uncertain_parameter_sample
            )

        # Extract the uncertain parameter sample (sort it if one dimensional)
        sizes = np.array([np.size(s) for s in uncertain_parameter_sample])
        if np.all(sizes == 1):
            uncertain_parameter_sample.sort()
        num_uncertain_parameter_values = len(uncertain_parameter_sample)

        # Local function to evaluate the EXACT stochastic objective (E(U(d, p)));
        #   used with the stochastic approach (no-relaxation)
        fun = self.configurations.fun
        stoch_fun = lambda theta, p: self.stochastic_policy_objective_value(
            theta=theta,
            param=p,
            exact=True,
        )

        ## Creat Blaceholders:
        #=====================
        # Discretize the design/probability space
        thetas = np.linspace(0, 1, num_test_points, endpoint=True, )
        hyperparameter_candidates = np.empty(
            (
                num_test_points,
                num_test_points,
                self.size,
            )
        )
        (
            hyperparameter_candidates[:, :, 0],
            hyperparameter_candidates[:, :, 1],
        ) = np.meshgrid(thetas, thetas, indexing="ij", )

        # Blaceholder for the deterministic objective value (for relaxed designs)
        # Blaceholder for the (Expectation) stochastic objective value
        if solve_by_relaxation:
            design_grid = hyperparameter_candidates
            policy_grid = None
            objective_values = np.empty(
                (
                    num_test_points,
                    num_test_points,
                    num_uncertain_parameter_values,
                )
            )
            stochastic_objective_values = None
        else:
            design_grid = None
            policy_grid = hyperparameter_candidates
            objective_values = None
            stochastic_objective_values = np.empty(
                (
                    num_test_points,
                    num_test_points,
                    num_uncertain_parameter_values,
                )
            )

        print(
            f"Evaluating objective values (deterministic and stochastic) for each"
            f" value of the hyperparameter (for contour plots) \n"
            f"=====================================\n"
            f"THIS IS GONNA TAKE QUITE SOME TIME...\n"
            f"=====================================\n"
        )

        # Loop over the uncertain parameter (as it may take time to set it),
        #   then over each design/probability to evaluate objective
        for k in range(num_uncertain_parameter_values):
            p = uncertain_parameter_sample[k]

            print(
                f"\r\t>> Uncertain Parameter Realization:"
                f" [{k+1}/{num_uncertain_parameter_values}] ",
                end="",
            )
            for i in range(num_test_points):
                for j in range(num_test_points):

                    # d stands for relaxed design or probability (theta)
                    d = hyperparameter_candidates[i, j, :]

                    # Evaluate objective (deterministic/stochastic)
                    if solve_by_relaxation:
                        # Use `fun`
                        objective_values[i, j, k] = stoch_fun(d, p)

                    else:
                        # Use stoch_fun
                        stochastic_objective_values[i, j, k] = stoch_fun(d, p)

        print(f"\nDone...")

        results = dict(
            uncertain_parameter_sample=uncertain_parameter_sample,
            design_grid=design_grid,
            policy_grid=policy_grid,
            stochastic_objective_values=stochastic_objective_values,
            objective_values=objective_values,
        )

        if  saveto is not None:
            saveto = os.path.abspath(saveto)
            output_dir = os.path.dirname(saveto)
            if not os.path.isdir(output_dir): os.makedirs(output_dir)
            pickle.dump(results, open(saveto, "wb"))
            print(f"Robust Binary Optimization: Exhaustive search Results dumped/pickled to: {saveto}")
        #
        return results

    def plot_results(
        self,
        results,
        overwrite=False,
        bruteforce_results=None,
        num_active=None,
        uncertain_parameter_sample=None,
        exhaustive_parameter_search_results=None,
        show_legend=True,
        output_dir=None,
        keep_plots=False,
        fontsize=20,
        line_width=2,
        usetex=True,
        show_axis_grids=True,
        axis_grids_alpha=(0.25, 0.4),
        plots_format='pdf',
        **kwargs,
    ):
        """
        .. Warning::
          This is a Work-In-Progress method to visualize optimization results generated by
          :py:meth`pyoed.optimization.binary_optimization.RobustBinaryReinforceOptimizer.solve`.

        Given the results returned by :py:meth:`solve`, visualize optimization results.
        This function, creates the following plots:

          - Values of the objective function over the optimization trajectory

        .. note::
            Adding additional `kwargs` to enable high flexibility in calling
            this method.

        :param results: a dictionary or an instance of :py:class:`oed.oed.OEDResults` holding
          all results generated.
        :param line_width: width (in points) of lines plotted
        :param bool overwrite: if `True`, overwrite existing plots (if any)
        :param output_dir: path/dir name to spit out plots (created if non-existant);
          if `None`, current directory is used
        :param verbose: output information to screen

        :returns: a dictionary with the following entries/keys:

          - `figures` a dictionary indexed by plot identifier with value set to
            the corresponding figure handle (or `None` if `keep_plots` is set to False)
          - `stats`: a dictionary holding statistics including number of
            function evaluations, and percentage of space explored.
          - `extras`: additional results:

            -  `global_optimum`: an estimate (e.g., using MC) of the global optimum value
            -  `global_optimal_design`: an estimate (e.g., using MC) of the global optimal design

        """
        if isinstance(results, RobustBinaryReinforceOptimizerResults):
            results = results.asdict()
        elif isinstance(results, dict):
            pass
        else:
            raise TypeError(
                f"Expected results to be dictionary or `RobustBinaryReinforceOptimizerResults`; "
                f"received {results=} of {type(results)=}!"
            )

        if len(kwargs) > 0:
            print(
                f"Unknown keyword arguments encountered in "
                f"{self.__class__}.plot_results:\n{kwargs}\n"
                f"These argments/keywords are discarded"
            )

        # Extract results from the results dictionary
        try:
            size = results['configurations']['size']
            maxmin= results['configurations']['maxmin']

            optimal_policy = results['optimal_policy']
            optimal_policy_sample = results['optimal_policy_sample']
            optimal_policy_sample_objval = results['optimal_policy_sample_objval']
            optimal_design = results['optimal_design']
            optimal_design_objval = results['optimal_design_objval']
            optimization_trajectory = results['optimization_trajectory']
            optimization_trajectory_stoch_objval = results['optimization_trajectory_stoch_objval']
            objective_value_tracker = results['objective_value_tracker']
            outer_optimization_results = results['outer_optimization_results']
            inner_optimization_results = results['inner_optimization_results']
            uncertain_parameter_sample = results['uncertain_parameter_sample']
            converged = results['converged']

        except Exception as err:
            raise TypeError(
                f"Failed to extract entries from the passed `results`\n"
                f"Passed results of {type(results)=}\nTrace error below!\n"
                f"Unexpected {err=} of {type(err)=}"
            )

        # Initialize figures dictionary
        figures = dict()

        # Check number of active entries
        if num_active is None:
            pass
        elif isinstance(num_active, int):
            msg = "num_active must be a non-negative integer less than or equal to the number of possible candidates"
            assert 0 <= num_active <= size, msg
            num_active = [num_active]
        elif utility.isiterable(num_active):
            if all([utility.isnumber(v) and int(v)==v and (0<=v<=size) for v in num_active]):
                num_active = [int(v) for v in num_active]
            else:
                raise TypeError(
                    f"None, scalar, or an iterable with valid budgets is expected here; found {type(num_active)=}"
                )
        else:
            raise TypeError(
                f"None, scalar, or an iterable with integers is expected here; found {type(num_active)=}"
            )

        ## Start Plot results
        if self.verbose:
            print("\n***\nPlotting Robust Binary Optimization Results...\n***")

        # Prepare output directory
        if output_dir is None:
            output_dir = os.getcwd()
        output_dir = os.path.abspath(output_dir)
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)

        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]
        utility.plots_enhancer(fontsize=fontsize, usetex=usetex)

        ## File naming
        optname = f"RobustBinaryReinforce"
        plots_format = plots_format.strip(' .').lower()
        optimal_policy_filename = os.path.join(output_dir, f"{optname}_Optimal_Plicy.{plots_format}")
        optimal_policy_sample_filename = os.path.join(output_dir, f"{optname}_Optimal_Plicy_Sample.{plots_format}")
        opt_iter_filename = f"Optimization_Iterations.{plots_format}"
        opt_iter_funevals_filename = (
            f"Optimization_Iterations_FunctionEvaluations.{plots_format}"
        )
        bruteforce_scatter_filename = f"BruteForce_ScatterPlot.{plots_format}"

        if self.verbose:
            print("Trying to find an estimate of the global optimum (...SAMPLE-BASED ESTIMATE...)")

        # Check if bruteforce is obtained by robust_oed or not
        bruteforce = True if bruteforce_results is not None else False

        if not bruteforce:
            global_optimum = np.nan
            global_optimal_design_index = None
            global_optimal_design = None
            all_lambdas = None
            clustered_bruteforce_results = None

        else:
            # This is robust OED results (just confirming)
            # In this case, we know designs are not clustered for a fact
            assert (
                bruteforce_results["design_size"] == self.size
            ), f"The design size in results is inconsistent with the optimization problem!"

            # Retrieve an estimate of the global optimum (over the given sample of the uncertain parameter)
            # For every design find the objective value for all values of the uncertain parameter (lambda)
            #   and calculate the minimum;
            #   The robust optimal design is the one that has the maximum value of thosee minima
            if num_active is None:
                num_designs = np.power(2, self.size)

            else:
                num_designs = 0
                for n in num_active:
                    num_designs += utility.ncr(self.size, n)

            all_lambdas = bruteforce_results['uncertain_parameter_sample']
            num_lambdas = len(all_lambdas)

            all_objvals = np.empty((num_lambdas, num_designs))
            for lam_ind in range(num_lambdas):
                all_objvals[lam_ind, :] = bruteforce_results["results"][lam_ind][
                    "objective_values"
                ]

            #
            min_per_design = np.min(all_objvals, axis=0)
            best_design_ind = np.argmax(min_per_design)
            global_optimum = min_per_design[best_design_ind]
            best_lambda_ind = np.where(all_objvals[:, best_design_ind] == global_optimum)[0]
            global_optimal_design_index = best_design_ind + 1
            global_optimal_design = utility.index_to_binary_state(
                global_optimal_design_index, self.size
            )

            # create clustered objective values for plots
            clustered_objval_tracker = dict()
            clustered_bruteforce_results = copy.deepcopy(bruteforce_results)
            print()
            for lam_ind in range(num_lambdas):
                print(
                    f"\rUpdating bruteforce dictionary [{lam_ind+1}/{num_lambdas}]",
                    end="",
                )
                tracker = dict()
                for d, j in zip(
                    bruteforce_results["results"][lam_ind]["candidates"],
                    bruteforce_results["results"][lam_ind]["objective_values"],
                ):
                    tracker.update({d: j})

                _candidates, _objective_values = utility.group_by_num_active(
                    tracker, self.size
                )
                clustered_bruteforce_results["results"][lam_ind].update({
                    "candidates": _candidates,
                    "objective_values": _objective_values,
                })
            print()


        if self.verbose:
            if global_optimal_design is None and bruteforce:
                print(
                    "Failed to extract a global optimum estimate; no proper bruteforce"
                    " results found!"
                )
            elif bruteforce:
                print(
                    f"Found a global optimum value {global_optimum} for a robust design"
                    f" index {global_optimal_design_index}"
                )

        # Plotter enhancement; fontsize & tex rendering
        utility.plots_enhancer(fontsize=fontsize, usetex=usetex)

        # Start Plot results
        if self.verbose:
            print("\n***\nPlotting Binary OED Results...\n***")

        # fancy plot settings
        plots_format = plots_format.strip(" .").lower()
        line_colors = [c["color"] for c in plt.rcParams["axes.prop_cycle"]]

        # Files Naming
        # Optimization Iterations
        opt_iter_filename = f"Optimization_Iterations.{plots_format}"
        opt_iter_funevals_filename = (
            f"Optimization_Iterations_FunctionEvaluations.{plots_format}"
        )
        bruteforce_scatter_filename = f"BruteForce_ScatterPlot.{plots_format}"
        surfaces_filename = f"TwoD_Objective_Value_Surfaces_With_Path.{plots_format}"

        # Objective value (based on the formulation (stochastic vs. deterministic))
        optimization_trajectory_objval = target_objval = optimization_trajectory_stoch_objval
        target_sample_objval = optimal_policy_sample_objval

        #
        # Start Plotting Optimization Results
        # 1- Values of the objective function for the optimization trajectory
        saveto = os.path.join(output_dir, opt_iter_filename)
        if os.path.isfile(saveto) and not overwrite:
            print(f"Plot exists; skipping: {saveto}")

        else:
            ylabel = r"Objective Function Value"
            xlabel = r"Optimization Iteration"
            label = r"$\mathbf{E}(\mathcal{J})$"

            # Plot optimization iterations
            fig = plt.figure(figsize=(10, 5))
            ax = fig.add_subplot(111)
            ax.plot(
                np.arange(len(target_objval)),
                target_objval,
                "-*",
                linewidth=line_width,
                color=line_colors[0],
                markersize=10,
                markeredgewidth=3,
                label=label,
            )

            if not np.isnan(global_optimum):
                ax.plot(
                    np.arange(len(optimization_trajectory_objval)),
                    global_optimum * np.ones(len(optimization_trajectory_objval)),
                    "--",
                    linewidth=line_width,
                    color=line_colors[2],
                    label=r"Global Optimum Estimate",
                    zorder=+100,
                )

            # Add legend
            if show_legend:
                ax.legend(
                    bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
                    mode="expand", borderaxespad=0, ncol=2,
                )

            ax.xaxis.set_major_locator(MaxNLocator(integer=True))
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)

            # Update figures dictionary
            figures.update(
               {
                   saveto.rstrip(f'.{plots_format}').split(os.path.sep)[-1]: fig,
               }
            )

            # save plot
            if self.verbose:
                print("Saving/Plotting Results to: {0}".format(saveto))
            fig.savefig(saveto, dpi=600, bbox_inches="tight")
            if not keep_plots:
                plt.close(fig)

        # Optimal sample of designs (if no sampling is carried out)
        if optimal_policy_sample is None is not optimal_design:
            optimal_policy_sample = [optimal_design]
            target_sample_objval = [optimization_trajectory_objval[-1]]

        # 2- Plot design sample compared to brute-force solution
        if bruteforce and bruteforce_results is not clustered_bruteforce_results:
            saveto = os.path.join(output_dir, bruteforce_scatter_filename)
            if os.path.isfile(saveto) and not overwrite:
                print("Plot exists; skipping: {0}".format(saveto))

            else:
                uncertain_parameter_values = all_lambdas
                brfrce_colors = utility.unique_colors(size=num_lambdas, gradual=False)
                brfrce_markers = [m for m in Line2D.markers.keys()]

                fig = plt.figure(figsize=(10, 6))
                ax = fig.add_subplot(111)

                # Plot scatter plot for for each uncertain parameter (same color)
                for lam_ind in range(num_lambdas):
                    alpha = 1.0 - (lam_ind / num_lambdas / 2.0)
                    candidates = bruteforce_results['results'][lam_ind]['candidates']
                    lambda_label = None if num_lambdas > 4 else r"$\lambda_{{%d}}$" % lam_ind
                    ax.plot(
                        candidates,
                        all_objvals[lam_ind, :],
                        color=brfrce_colors[lam_ind],
                        linestyle="None",
                        marker=brfrce_markers[int(lam_ind // len(brfrce_markers))],
                        fillstyle="none",
                        markersize=10,
                        markeredgewidth=3,
                        alpha=alpha,
                        label=lambda_label,
                    )

                # Plot the optimal sample
                if optimal_policy_sample is not None:
                    lbl_set = False
                    for design, objval in zip(optimal_policy_sample, target_sample_objval):
                        if lbl_set:
                            lbl = None
                        else:
                            lbl = "Optimal Policy Sample"
                            lbl_set = True
                        design_ind = utility.index_from_binary_state(design)
                        ax.plot(
                            design_ind,
                            objval,
                            color="red",
                            linestyle="None",
                            marker="*",
                            fillstyle="none",
                            markersize=16,
                            markeredgewidth=line_width / 2.0,
                            label=lbl,
                        )

                # Add chosen optimal design
                if optimal_design is not None and optimal_design_objval is not None:
                    ax.plot(
                        utility.index_from_binary_state(optimal_design),
                        optimal_design_objval,
                        color="red",
                        linestyle="None",
                        marker="s",
                        fillstyle="none",
                        markersize=20,
                        markeredgewidth=line_width / 1.5,
                        label=r"Policy Optimal Design",
                    )

                # Add global optimum
                if not np.isnan(global_optimum):
                    xlim = ax.get_xlim()
                    ylim = ax.get_ylim()

                    ax.plot(
                        xlim,
                        (global_optimum, global_optimum),
                        "--",
                        linewidth=line_width,
                        color="black",
                        label=r"Global Optimum Estimate",
                    )
                    ax.set_xlim(xlim)
                    ax.set_ylim(ylim)
                if global_optimal_design is not None and global_optimum is not None:
                    ax.plot(
                        global_optimal_design_index,
                        global_optimum,
                        color="black",
                        linestyle="None",
                        marker="x",
                        fillstyle="none",
                        markersize=16,
                        markeredgewidth=line_width,
                        label=r"Global Optimal Design",
                    )

                ax.xaxis.set_major_locator(MaxNLocator(integer=True))
                ax.set_xlabel(r"Experimental Designs")
                ax.set_ylabel(r"Objective Function Value")

                if show_legend:
                    ax.legend(
                        bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
                        mode="expand", borderaxespad=0, ncol=2,
                    )

                # Update figures dictionary
                figures.update(
                   {
                       saveto.rstrip(f'.{plots_format}').split(os.path.sep)[-1]: fig,
                   }
                )

                # save plot
                if self.verbose:
                    print("Saving/Plotting Results to: {0}".format(saveto))
                fig.savefig(saveto, dpi=600, bbox_inches="tight")
                if not keep_plots:
                    plt.close(fig)

        if bruteforce and clustered_bruteforce_results is not bruteforce_results:
            saveto = os.path.join(output_dir, "Clustered_" + bruteforce_scatter_filename)
            if os.path.isfile(saveto) and not overwrite:
                print("Plot exists; skipping: {0}".format(saveto))

            else:
                uncertain_parameter_values = all_lambdas
                brfrce_colors = utility.unique_colors(size=num_lambdas, gradual=False)
                brfrce_markers = [m for m in Line2D.markers.keys()]

                fig = plt.figure(figsize=(10, 6))
                ax = fig.add_subplot(111)

                # Plot scatter plot for for each uncertain parameter (same color)
                for lam_ind in range(num_lambdas):
                    alpha = 1.0 - (lam_ind / num_lambdas / 2.0)
                    for d, objvals in zip(
                        clustered_bruteforce_results["results"][lam_ind]["candidates"],
                        clustered_bruteforce_results["results"][lam_ind][
                            "objective_values"
                        ],
                    ):
                        lambda_label = None if num_lambdas > 4 else r"$\lambda_{{%d}}$" % lam_ind
                        ax.plot(
                            [d] * len(objvals),
                            objvals,
                            color=brfrce_colors[lam_ind],
                            linestyle="None",
                            marker=brfrce_markers[int(lam_ind // len(brfrce_markers))],
                            fillstyle="none",
                            markersize=10,
                            markeredgewidth=3,
                            alpha=alpha,
                            label=lambda_label,
                        )

                # Plot the optimal sample
                if optimal_policy_sample is not None:
                    lbl_set = False
                    for design, objval in zip(optimal_policy_sample, target_sample_objval):
                        if lbl_set:
                            lbl = None
                        else:
                            lbl = "Optimal Policy Sample"
                            lbl_set = True
                        ax.plot(
                            np.count_nonzero(design),
                            objval,
                            color="red",
                            linestyle="None",
                            marker="*",
                            fillstyle="none",
                            markersize=16,
                            markeredgewidth=line_width / 2.0,
                            label=lbl,
                        )

                # Add chosen optimal design
                if optimal_design is not None and optimal_design_objval is not None:
                    ax.plot(
                        np.count_nonzero(optimal_design),
                        optimal_design_objval,
                        color="red",
                        linestyle="None",
                        marker="s",
                        fillstyle="none",
                        markersize=18,
                        markeredgewidth=line_width / 1.5,
                        label=r"Policy Optimal Design",
                    )

                # Add global optimum
                if not np.isnan(global_optimum):
                    xlim = ax.get_xlim()
                    ylim = ax.get_ylim()

                    ax.plot(
                        xlim,
                        (global_optimum, global_optimum),
                        "--",
                        linewidth=line_width,
                        color="black",
                        label=r"Global Optimum Estimate",
                    )
                    ax.set_xlim(xlim)
                    ax.set_ylim(ylim)

                if global_optimal_design is not None and global_optimum is not None:
                    ax.plot(
                        np.count_nonzero(global_optimal_design),
                        global_optimum,
                        color="black",
                        linestyle="None",
                        marker="x",
                        fillstyle="none",
                        markersize=16,
                        markeredgewidth=line_width,
                        label=r"Global Optimal Design",
                    )

                ax.xaxis.set_major_locator(MaxNLocator(integer=True))
                ax.set_xlabel(r"Experimental Designs")
                ax.set_ylabel(r"Objective Function Value")

                # Add legend
                if show_legend:
                    ax.legend(
                        bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
                        mode="expand", borderaxespad=0, ncol=2,
                    )

                # Update figures dictionary
                figures.update(
                   {
                       saveto.rstrip(f'.{plots_format}').split(os.path.sep)[-1]: fig,
                   }
                )

                # save plot
                if self.verbose:
                    print("Saving/Plotting Results to: {0}".format(saveto))
                fig.savefig(saveto, dpi=600, bbox_inches="tight")
                if not keep_plots:
                    plt.close(fig)

        if 'sampled_design_indexes' not in results:
            results.update({'sampled_design_indexes':[]})


        # Surfaces with optimization path for 2D case
        saveto = os.path.join(output_dir, surfaces_filename)
        if exhaustive_parameter_search_results is not None:
            if os.path.isfile(saveto) and not overwrite:
                print(f"Plot exists; skipping: {saveto}")
            else:
                # Plot surfaces, retrieve (ne legend, add path, and show legend!)
                ex_figures = self.plot_objective_surfaces(
                    exhaustive_parameter_search_results,
                    fontsize=fontsize,
                    usetex=usetex,
                    plots_format=plots_format,
                    #
                    output_dir=output_dir,
                    keep_plots=True,
                    show_legend=False,
                    all_legend_labels=False,  # don't add surfaces labels
                )
                fig = ex_figures[list(ex_figures.keys())[0]]
                ax = fig.gca()

                # Add optimization path
                xs = [t[0][0] for t in optimization_trajectory]
                ys = [t[0][1] for t in optimization_trajectory]
                zs = optimization_trajectory_objval
                ax.plot(
                    xs,
                    ys,
                    zs,
                    "-kd",
                    lw=4,
                    markersize=13,
                    label=r"Optimization Path",
                    zorder=+130,
                )
                ax.scatter(
                    xs[-1:],
                    ys[-1:],
                    zs[-1:],
                    s=26**2,
                    marker="o",
                    linewidth=4,
                    edgecolors="red",
                    facecolors="None",
                    label=r"Optimal Policy",
                    zorder=+140,
                )

                ax.scatter(
                    [optimal_design[0]],
                    [optimal_design[1]],
                    [optimal_design_objval],
                    s=26**2,
                    marker="s",
                    linewidth=4,
                    edgecolors="red",
                    facecolors="None",
                    label=r"Policy Optimal Design",
                    zorder=+150,
                )

                # Save before adding legend, then resave after
                # save plot
                fig.savefig(saveto, dpi=600, bbox_inches="tight", pad_inches=0.5)

                # Update figures dictionary
                figures.update(
                   {
                       saveto.rstrip(f'.{plots_format}').split(os.path.sep)[-1]: fig,
                   }
                )

                if show_legend:
                    ax.legend(
                        loc="lower left",
                        bbox_to_anchor=(0, 0.90, 1, 0.2),
                        mode="expand",
                        borderaxespad=-1.0,
                        ncol=3,
                    )

                    # save plot
                    if self.verbose:
                        print("Saving/Plotting Results to: {0}".format(saveto))
                    saveto = saveto.rstrip(f".{plots_format}") + f"_With_Legend.{plots_format}"
                    fig.savefig(saveto, dpi=600, bbox_inches="tight")

                    # Update figures dictionary
                    figures.update(
                       {
                           saveto.rstrip(f'.{plots_format}').split(os.path.sep)[-1]: fig,
                       }
                    )

                if not keep_plots:
                    plt.close(fig)

        # Try calling super to make other plots (if any); ignore failure
        try:
            plot_results = super().plot_results(
                results=results,
                overwrite=overwrite,
                bruteforce=False,
                num_active=num_active,
                output_dir=output_dir,
                keep_plots=keep_plots,
                fontsize=fontsize,
                line_width=line_width,
                usetex=usetex,
                show_axis_grids=show_axis_grids,
                axis_grids_alpha=axis_grids_alpha,
                plots_format=plots_format,
            )
        except Exception as err:
            print(
                f"Couldn't run/call super({self.__class__}, {self}).plot_results()\n"
                f"Unexpected {err=} of {type(err)=}\n"
                f"Discarding..."
            )
            plot_results = {
                "figures":{},
                "stats":{},
                "extras":{},
            }

        # Calculate statistics
        if num_active is None:
            space_cardinality = 2**size * len(uncertain_parameter_sample)
        else:
            if utility.isnumber(num_active): num_active = [num_active]
            space_cardinality = sum(
                [utility.ncr(size, n) for n in num_active]
            ) * len(uncertain_parameter_sample)


        # Percentage of space explored
        num_fun_evals = len(objective_value_tracker)
        exploration_percentatge = num_fun_evals / space_cardinality * 100

        sep = f"\n {'='*50} \n"
        stats = f"{sep}Solution Statistics{sep}"
        stats += f"Space (Robust) Exploration: {exploration_percentatge} % \n"
        stats += sep
        if self.verbose:
            print(stats)

        # Aggregate local results with one from top
        plot_results['figures'].update(figures)
        plot_results['stats'].update(
            {
                'num_fun_evals': num_fun_evals,
                'exploration_percentatge': exploration_percentatge,
            }
        )
        plot_results['extras'].update(
            {
                'global_optimum':global_optimum,
                'global_optimal_design':global_optimal_design,
            }
        )
        return plot_results

    def plot_objective_surfaces(
        self,
        results,
        fontsize=22,
        usetex=True,
        azim=20, # None
        elev=20, # None
        xyticks=np.arange(0, 1.001, 0.2),
        xylim=(-0.1, 1.1),
        xylabelpad=35,
        zlabelpad=20,
        show_xy_projection=False,
        offset_dist_factor=0.10,
        show_legend=True,
        all_legend_labels=False,
        show_colorbar=False,
        colorbar_orientation="horizontal",
        sample_uncertain_parameter=False,
        num_test_points=20,
        keep_plots=False,
        output_dir=None,
        plots_format="pdf",
    ):
        """
        Given the output of :py:meth:`exhaustive_parameter_search`, plot 3D surfaces
        and return the figure.

        :parm results:
        :parm fontsize:
        :parm azim:
        :parm elev:
        :parm xyticks:
        :parm xylim:
        :parm xylabelpad:
        :parm zlabelpad:
        :parm show_xy_projection:
        :parm offset_dist_factor:
        :parm show_legend:
        :param all_legend_labels:
        :parm show_colorbar:
        :parm colorbar_orientation:
        :parm sample_uncertain_parameter:
        :parm num_test_points:
        :param keep_plots:
        :param verbose:
        :parm output_dir:
        :parm plots_format:

        :remarks: This is valid only for Design space of size 2 (e.g., 2 candidate sensor locations).
        """
        def adjust_z_offset(ax):
            """Check the offset, and add properly"""
            # Adjust offset rotation#
            offset_text = ax.zaxis.get_offset_text()
            if len(offset_text.get_text().strip()) == 0:
                offset_val = 1
            else:
                offset_val = float(offset_text.get_text())
            offset_text.set_visible(False)
            if not np.allclose(offset_val, 1):
                exponent = int(
                    "{:.2e}".format(np.min(hyperparameter_candidates_stoch_objval)).split(
                        "e"
                    )[1]
                )
                ax.text(
                    ax.get_xlim()[1],
                    ax.get_ylim()[1],
                    ax.get_zlim()[1],
                    r"$\times\mathdefault{10^{%d}}\mathdefault{}$" % exponent,
                )

        # Check the exhaustive search results (dict) and regenerate if needed
        if results is None:
            print("No exhaustive search results passed; regenerating...")
            results = self.exhaustive_parameter_search(
                num_test_points=num_test_points,
            )

        # Load information from dictionary
        try:
            uncertain_parameter_values=results["uncertain_parameter_values"]
            design_grid=results["design_grid"]
            policy_grid=results["policy_grid"]
            stochastic_objective_values=results["stochastic_objective_values"]
            objective_values=results["objective_values"]
        except Exception as err:
            print(
                f"Failed to load exhaustive search results"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

        if (
            policy_grid is None and stochastic_objective_values is None and
            not (design_grid is None or objective_values is None)

        ):
            stochastic_optimization = False
        elif (
            design_grid is None and objective_values is None and
            not (policy_grid is None or stochastic_objective_values is None)
        ):
            stochastic_optimization = True
        else:
            print(design_grid, policy_grid, objective_values, stochastic_objective_values)
            raise ValueError(
                f"Incompatible results in the exhasutive search!\n"
                f"{(design_grid is None)=} \n"
                f"{(policy_grid is None)=} \n"
                f"{(objective_values is None)=} \n"
                f"{(stochastic_objective_values is None)=} "
            )

        if stochastic_optimization:
            grid = policy_grid
            objective_values = stochastic_objective_values
        else:
            grid = design_grid
            # objective_values = objective_values

        # Extract dimensions & assert
        if len(uncertain_parameter_values) == 0:
            raise TypeError(
                f"Empty uncertain parameter sample; nothing to plot!"
            )
        n1, n2, design_size = grid.shape
        if design_size != 2:
            raise TypeError(
                f"This function assumes design of size 2; found design of size{design_size} "
            )

        if not (np.ndim(objective_values) == np.ndim(grid) == 3):
            raise TypeError(
                f"Expected grid and objective value arrays to be 3 dimensional\n"
                f"{np.ndim(grid)=}, {np.ndim(objective_values)=}"
            )
        if objective_values.shape[2] != len(uncertain_parameter_values):
            raise TypeError(
                f"Last dimension of the objective values is different from "
                f"number of uncertain parameters in the sample\n"
                f"{objective_values.shape[2]=}; {len(uncertain_parameter_values)=}"
            )


        ##================
        ## Start plotting:
        ##================
        if self.verbose: print("Started plotting results...")

        # Limits
        if xylim is None:
            xlim = (0, 1)
            dist = offset_dist_factor * (xlim[1] - xlim[0])
            offset = xlim[0] - dist
            xlim = [xlim[0] - dist, xlim[1] + dist]
            ylim = xlim
        else:
            ylim = xlim = xylim
            offset = xlim[0]

        # Preprocessing; prepare output dirctory
        plots_format = plots_format.strip(". ")

        # Plotter enhancement; fontsize & tex rendering
        utility.plots_enhancer(
            fontsize=fontsize,
            usetex=usetex,
        )

        # File(s) & Figure(s) name(s)
        surfaces_plot_name = f"TwoD_Objective_Value_Surfaces.{plots_format}"

        # Dictionary to hold figures
        figures = {}

        # 1- Plot Objective Values
        fig = plt.figure(figsize=(12, 12))
        ax = fig.add_subplot(111, projection="3d")

        surfs = []
        for i in range(len(uncertain_parameter_values)):
            surf = ax.plot_surface(
                grid[:, :, 0],
                grid[:, :, 1],
                objective_values[:, :, i],
                alpha=0.3,
                label=r"$\lambda_{%d}$"%(i) if all_legend_labels else None,
            )
            surfs.append(surf)

        # Find lower bound on objective values and plot it
        lower_bound = np.min(objective_values, axis=-1)
        lb_wf = ax.plot_wireframe(
            grid[:, :, 0],
            grid[:, :, 1],
            lower_bound,
            alpha=0.75,
            color="green",
            zorder=+100,
            label=r"Lower Bound"
        )

        # Stretch z-axis
        # ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax), np.diag([0.75, 0.75, 1, 1]))

        # Set camera angles
        if azim is not None or elev is not None:
            ax.view_init(elev=elev, azim=azim)

        # Ticks & Labels
        xlabel = r"$p_1$" if stochastic_optimization else r"$\zeta_1$"
        ylabel = r"$p_2$" if stochastic_optimization else r"$zeta_2$"
        zlabel = r"Objective Function Value"

        ax.zaxis.set_rotate_label(False)
        ax.set_xlabel(xlabel, labelpad=xylabelpad)
        ax.set_ylabel(ylabel, labelpad=xylabelpad)
        ax.set_zlabel(zlabel, labelpad=zlabelpad, rotation=90)
        ax.ticklabel_format(style="scientific", axis="z", useMathText=True)
        adjust_z_offset(ax)
        ax.tick_params(pad=15)

        # Project the lower bound on xy coordinates
        if show_xy_projection:
            cset = ax.contourf(
                grid[:, :, 0],
                grid[:, :, 1],
                lower_bound,
                zdir="x",
                alpha=0.5,
                offset=offset,
            )

            cset = ax.contourf(
                grid[:, :, 0],
                grid[:, :, 1],
                lower_bound,
                zdir="z",
                alpha=0.5,
                offset=offset,
            )
        else:
            cset = None

        # Project onto z (low) coordinate
        cset_z = ax.contourf(
            grid[:, :, 0],
            grid[:, :, 1],
            lower_bound,
            zdir="z",
            alpha=0.75,
            offset=offset,
        )

        # Plot estimte of the global solution
        global_opt_val = np.max(lower_bound)
        global_opt_ind = np.where(lower_bound==global_opt_val)
        global_opt_sol = np.array(
            [
                grid[global_opt_ind[0], global_opt_ind[1], 0],
                grid[global_opt_ind[0], global_opt_ind[1], 1],
            ]
        )
        ax.scatter(
            global_opt_sol[0],
            global_opt_sol[1],
            global_opt_val,
            s=22**2,
            marker="s",
            linewidth=4,
            edgecolors="green",
            facecolors="None",
            label=r"Max-Min",
        )

        # Set limits
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        if xyticks is not None: ax.set_xticks(xyticks)
        zlim = ax.get_zlim()
        dist = offset_dist_factor * (zlim[1] - zlim[0])
        offset = zlim[0] - dist
        zlim = [zlim[0] - dist, zlim[1] + dist]

        if show_colorbar:
            fig.colorbar(
                lb_wf if cset is None else cset,
                ax=ax,
                orientation=colorbar_orientation,
            )

        if show_legend:
            if len(uncertain_parameter_values) <= 10 or not all_legend_labels :
                ax.legend(
                    loc="lower left",
                    bbox_to_anchor=(0, 1.02, 1, 0.2),
                    mode="expand",
                    borderaxespad=0,
                    ncol=3,
                )
            else:
                ax.legend(
                    loc="center left",
                    bbox_to_anchor=(1.04, 0.5),
                    framealpha=0.85,
                    borderaxespad=1.0,
                    ncol=int(len(uncertain_parameter_values) // 16 + 1),
                )

        # Hack to handle surfaces matplotlib issues
        for surf in surfs:
            try:
                surf._edgecolors2d = surf._edgecolor3d
                surf._facecolors2d = surf._facecolor3d
            except:
                pass

        # Add figures and save
        # Update figures dictionary
        figures.update(
           {
               surfaces_plot_name.rstrip(f".{plots_format}"): fig,
           }
        )

        # Save figures if output_dir is set
        if output_dir is not None:
            if not os.path.isdir(output_dir):
                os.makedirs(output_dir)

            # save plot
            saveto = os.path.join(output_dir, surfaces_plot_name)
            if self.verbose:
                print("Surfaces Plotter: Saving/Plotting Results to: {0}".format(saveto))
            fig.savefig(saveto, dpi=600, bbox_inches="tight", pad_inches=0.5)

        # Close plots if requested
        if not keep_plots:
            for key, fig in figures.items():
                plt.close(fig)
                # figures[key] = None

        return figures


    @property
    def stochastic_binary_optimizer(self):
        """
        The stochastic binary optimizer used to solve the outer binary optimization problem
        if the probabilistic approach is selected (that is when `self.configurations.solve_by_relaxation`
        is set to `False` which is the default value).
        """
        return self._STOCHASTIC_BINARY_OPTIMIZER


    ## Extra Properties
    @property
    def uncertain_parameter_size(self):
        """The size of the uncertain parameter (as an iterable)"""
        return self.configurations.uncertain_parameter_size

    @property
    def uncertain_parameter_sample(self):
        """The uncertain parameter expandable sample"""
        return self.configurations.uncertain_parameter_sample

    @uncertain_parameter_sample.setter
    def uncertain_parameter_sample(self, val):
        self.update_configurations(uncertain_parameter_sample=val)

    @property
    def uncertain_parameter_bounds(self):
        """The size of the uncertain parameter (as an iterable)"""
        return self.configurations.uncertain_parameter_bounds

    @uncertain_parameter_bounds.setter
    def uncertain_parameter_bounds(self, val):
        """Update/Reset the uncertain parameter domain (if any)"""
        self.update_configurations(uncertain_parameter_bounds=val)

