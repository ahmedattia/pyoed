# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from abc import ABC, abstractmethod
from dataclasses import dataclass
import inspect
import warnings
from typing import Type

import numpy as np
from pyoed import utility
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    PyOEDData,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
    aggregate_configurations,
    extract_unknown_keys,
    remove_unknown_keys,
)


@dataclass(kw_only=True, slots=True)
class OptimizerConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`Optimizer` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`OptimizerConfigs`
    provides the following fields:

    :param name: name of the optimizer. Default is `None`.
    :param size: dimension of the target/solution variable.
        Not all optimizers use this, but it is added for unification.
    :param screen_output_iter: iteration interval for screen output. Default is 1. Note
        that this should be a positive integer to enforce proper effect.
    :param file_output_iter: iteration interval for file output. Default is 1. Note
        that this should be a positive integer to enforce proper effect.
    """

    size: int | None = None
    name: str | None = None
    screen_output_iter: int = 1
    file_output_iter: int = 100


@set_configurations(OptimizerConfigs)
class Optimizer(ABC, PyOEDObject):
    """
    Base class for Optimizers (Optimization Routines).

    .. note::

        Each class derived from `Optimizer` should have its own `__init__` method
        in which the constructor just calls `super().__init__(configs=configs)`
        and then add any additional initialization as needed.

    :param dict | OptimizerConfigs | None configs: (optional) configurations for
        the optimization object

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """

    def __init__(self, configs: dict | OptimizerConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | OptimizerConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`OptimizerConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`OptimizerConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(
            configs=configs,
        )

        ## Validation Stage
        # `name`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="name",
            test=lambda v: isinstance(v, (str, type(None))),
            message=f"Model name is of invalid type. Expected None or string. ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `screen_output_iter`: positive integer >=1
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="screen_output_iter",
            test=lambda v: (utility.isnumber(v) and v == int(v) and v >= 1),
            message=f"Screen output iterations `screen_output_iter` is invalid. Expected postive integer >= 1",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `file_output_iter`: positive integer >=1
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="file_output_iter",
            test=lambda v: (utility.isnumber(v) and v == int(v) and v >= 1),
            message=f"File output iterations `file_output_iter` is invalid. Expected postive integer >= 1",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    @abstractmethod
    def solve(
        self,
        *args,
        **kwargs,
    ):
        """
        Solve the optimization problem.
        Actual implementation of this :py:meth:`solve` must be provided by each otpimization class/object.
        """
        ...


@dataclass(kw_only=True, slots=True)
class OptimizerResults(PyOEDData):
    """
    Base class (empty data container) to hold optimization data/results
    """

    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  PyOED Empty Optimizer Data Container\n{sep}\n"
        return msg

    # TODO: We might want to add general (minimal) results following Scipy approach
    # e.g.:
    # x: optimal solution
    # fun: optimal objective value
    # msg: message showing convergence (or not) details
    # converged (or success): boolean flag showing whether the algorithm has converged or not
    # TODO: Here, provide unified interface to extract optimization results/information.

    @property
    def x(self) -> np.ndarray:
        """The optimal solution of the optimization problem."""
        __raise_method_not_implemented(self)

    @property
    def success(self) -> bool:
        """Whether or not the optimizer exited successfully."""
        __raise_method_not_implemented(self)

    @property
    def message(self) -> str:
        """Description of the cause of the termination."""
        __raise_method_not_implemented(self)

    @property
    def fun(self) -> float:
        """Value of objective function at the optimal solution `x`."""
        __raise_method_not_implemented(self)

    @property
    def jac(self) -> np.ndarray:
        """Value of the Jacobian of the objective function at the optimal solution `x` (if available)."""
        __raise_method_not_implemented(self)

    @property
    def hess(self) -> object:
        """Value of the Hessian of the objective function at the optimal solution `x` (if available)."""
        __raise_method_not_implemented(self)

    @property
    def hess_inv(self) -> object:
        """Value of the inverse of Hessian of the objective function at the optimal solution `x` (if available)."""
        __raise_method_not_implemented(self)

    @property
    def nfev(self) -> int:
        """Number of evaluations of the objective functions."""
        __raise_method_not_implemented(self)

    @property
    def njev(self) -> int:
        """Number of evaluations of the the Jacobian of the objective function."""
        __raise_method_not_implemented(self)

    @property
    def nhev(self) -> int:
        """Number of evaluations of the the Hessian of the objective function."""
        __raise_method_not_implemented(self)

    @property
    def nit(self) -> int:
        """Number of iterations performed by the optimizer."""
        __raise_method_not_implemented(self)


def create_optimizer(
    optimizer: None | Optimizer | Type[Optimizer] = None,
    optimizer_configs: None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None,
    new_configs: None | dict = None,
):
    """
    Given an `optimizer` and `optimizer_configs` return the optimizer with
    configurations as in the passed object.

    The logic:
        1. The optimizer:

            1. None: If both `optimizer_configs` and `new_configs` are `None` return
               `None`. If any of the configurations is not None, throw a warning and
               also return `None`. This is added to enable resetting optimizers in
               objects at instantiation.
            2. instance derived from `pyoed.optimization.Optimizer`: use it as is
            3. subclass of `pyoed.optimization.Optimizer`: create an instance

        2. The optimizer_configs:

            1. None: convert to empty dictionary
            2. dict: remove unknown keys and throw warning if unknown keys found
            3. instance derived from `pyoed.optimization.OptimizerConfigs`: convert to
               dictionary
            4. subclass of `pyoed.optimization.OptimizerConfigs`: create an instance and
               convert to dictionary

        3. new_configs (if None replace with empty dictionary):

            1. dict: update/replace known keys into `optimizer_configs` and throw
               warning if unknown keys are passed

    The optimizer is instantiated (if a class is passed) with the aggregated
    configurations. If valid optimizer instance is passed, its configurations are
    updated with the aggregated configurations.

    :raises TypeError: if the passed arguments are of invalid types.
    :raises: an error (type varies) if the optimizer failed to instantiate
    """
    if optimizer is None:
        if not (optimizer_configs is new_configs is None):
            warnings.warn(
                f"The passed optimizer is `None`, but some configurations are not `None`!\n"
                f"It does not make sense to do that! \nDiscrding and returning `None`"
            )
        return None

    ## 1. Check the optimizer type: (optimizer class or optimizer object)
    if not (
        isinstance(optimizer, Optimizer)
        or (inspect.isclass(optimizer) and issubclass(optimizer, Optimizer))
    ):
        raise TypeError(f"Invalid {optimizer=} of {type(optimizer)=}")

    ## 2. Check the optimizer_configs type: (None, dictionary, configs class (or instance))
    if optimizer_configs is None:
        # None: convert to empty dict
        optimizer_configs = {}

    elif inspect.isclass(optimizer_configs):
        # Class derived from the base optimzer configs class `OptimizerConfigs`
        if issubclass(optimizer_configs, OptimizerConfigs):
            optimizer_configs = optimizer_configs().asdict()

        else:
            raise TypeError(
                f"Invalid {optimizer_configs=} of {type(optimizer_configs)=}\n"
                f"If a class is passed, it must be derived from `pyoed.optimization.core.OptimizerConfigs`!"
            )

    elif isinstance(optimizer_configs, OptimizerConfigs):
        # Object that inherits the base optimzer configs class
        optimizer_configs = optimizer_configs.asdict()

    elif isinstance(optimizer_configs, dict):
        pass

    else:
        raise TypeError(
            f"Invalid type of {optimizer_configs=} of "
            f"{type(optimizer_configs)} is not valid"
        )
    # Now, optimizer_configs is a dictionary

    ## 3. new_configs: None or dict
    if new_configs is None:
        new_configs = {}
    if not isinstance(new_configs, dict):
        raise TypeError(
            f"Invalid {new_configs=} of {type(new_configs)=}; "
            f"expected `None` or `dict`"
        )

    # Get the configurations class associated with the optimizer (if any)
    configs_dataclass = optimizer.get_configurations_class()
    if configs_dataclass is None:
        warnings.warn(
            f"The passed optimizer is not associated with a configurations class; will try to "
            f"blindly aggregate configurations"
        )
        optimizer_configs.update(new_configs)

    else:
        # Remove unknown keys from optimizer_configs
        unknown_keys = extract_unknown_keys(
            data=optimizer_configs,
            target_dataclass=configs_dataclass,
        )
        if len(unknown_keys) > 0:
            warnings.warn(
                f"Unknown keys found in the passed optimizer_configs: "
                f"{list(unknown_keys.keys())}"
            )
        optimizer_configs = remove_unknown_keys(
            data=optimizer_configs,
            target_dataclass=configs_dataclass,
        )

        # Remove unknown keys from new_configs
        unknown_keys = extract_unknown_keys(
            data=new_configs,
            target_dataclass=configs_dataclass,
        )
        if len(unknown_keys) > 0:
            warnings.warn(
                f"Unknown keys found in the passed new_configs: "
                f"{list(unknown_keys.keys())}"
            )
        new_configs = remove_unknown_keys(
            data=new_configs,
            target_dataclass=configs_dataclass,
        )

        # Update optimizer configs with new configs
        optimizer_configs.update(new_configs)

    ## Instantiate (or update) the optimizer
    if isinstance(optimizer, Optimizer):
        try:
            optimizer.update_configurations(**optimizer_configs)
        except Exception as err:
            print(
                f"Tried updating the optimizer with the passed *aggregated* "
                f"configurations but failed; see the error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

    elif inspect.isclass(optimizer) and issubclass(optimizer, Optimizer):
        try:
            optimizer = optimizer(optimizer_configs)
        except Exception as err:
            print(
                f"Tried creating the optimizer with the passed *aggregated* "
                f"configurations but failed; see the error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

    else:
        raise TypeError(
            f"Invalid {optimizer=} of {type(optimizer)=}\n"
            f"THIS SHOULD NEVER HAPPEN: PLEASE REPORT THIS AS A BUG!"
        )

    return optimizer


def __raise_method_not_implemented(obj: object):
    raise NotImplementedError(
        f"Each derived class need to raise it's own implementation of this method. "
        f"`{obj.__class__}` does not provide it."
    )
