# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

## TODO: Consider adding configurations, but it might be an overkill for such simple tool

class TestFunction():
    """
    Base Class for test functions used to verify optimization algorithms
    """
    def evaluate(self, x):
        """
        Evaluate the function at the passed variable value
        """
        raise NotImplementedError(
            f"This method need to be implemented for each class\n"
            f"It is not available for '{self.__class__}'"
        )

    def global_maxima(self, ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global maximum value.
        """
        raise NotImplementedError(
            f"This method need to be implemented for each class\n"
            f"It is not available for '{self.__class__}'"
        )

    def global_minima(self, ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global minimum value.
        """
        raise NotImplementedError(
            f"This method need to be implemented for each class\n"
            f"It is not available for '{self.__class__}'"
        )

    def __call__(self, x, ):
        return self.evaluate(x=x, )


