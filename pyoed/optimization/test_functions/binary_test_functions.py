# Copyright © 1023, UChicago Argonne, LLC
# All Rights Reserved

import warnings
from pyoed import utility
from ..core import (
    TestFunction,
)

import numpy as np


class IsolatedBinaryOptimizationFunction(TestFunction):
    # TODO: Update docstring once function is fully defined
    """
    .. note::
        This function is under development.

    A test function for binary optimization defined on the following form

    .. math::

        f(x) :=
            \\begin{cases}
                10 n a ; x[: n//2] = n//2, x[n//2: ] = 0
                10 n a ; x[: n//3] = n//3, x[n//3: ] = 0
                - 10 n a ; x[: n//2] = 0, x[n//2: ] = n//2
                - 10 n a ; x[: n//3] = 0, x[n//3: ] = n // 3
                5 \\lfloor \\frac{n}/2 \\rfloor - |x| * a ; & |x| > a  \\
                5 \\lfloor \\frac{n}/2 \\rfloor -|x| * a ; & |x| < a  \\
                5 \\lfloor \\frac{n}/2 \\rfloor * a ; & |x| = a  \\,,
            \\end{cases}

    where ::math:`x` is the variable, and ::math:`n` is its (integer) dimension,
    and both ::math:`a` and ::math:`n` are parameters passed upon instantiation.
    Here, `a` is assumed to be a postive number.

    The global maximum of this function is ::math:`2 n a` achieved by two
    vectors, and
    the global minimum of this function is ::math:`-2 n a`  achieved by
    two vectors.

    :parm a: positive number
    :parm n: positive integer representing dimension of the domain/variable
    """

    def __init__(self, a, n):
        warnings.warn(f"This function is not fully developed yet...")
        assert (
            utility.isnumber(n) and int(n) == n and n >= 1
        ), f"Expected positive integer n; received {n=} of {type(n)=}"
        assert (
            utility.isnumber(a) and a > 0
        ), f"Expected positive number for a; received {a=} of {type(a)=}"
        self.__a = a
        self.__n = int(n)

    def evaluate(self, x):
        """
        Evaluate the function at the passed `x` viewed as a boolean variable.
        """
        _x = utility.asarray(x, dtype=bool).flatten().astype(int)
        if _x.size != self.__n:
            raise TypeError(
                f"Invalid input type/shape. Expected x to be of size {self.__n}\n"
                f"Received {x=} of {type(x)=}"
            )
        else:
            x = _x

        #
        is_global_maximum = (
            sum(x[: int(self.__n // 2)]) == int(self.__n // 2)
            and sum(x[int(self.__n // 2) :]) == 0
        ) or (
            sum(x[: int(self.__n // 3)]) == int(self.__n // 3)
            and sum(x[int(self.__n // 3) :]) == 0
        )
        is_global_minimum = (
            sum(x[: int(self.__n // 2)]) == 0
            and sum(x[int(self.__n // 2) :]) == int(self.__n // 2)
        ) or (
            sum(x[: int(self.__n // 3)]) == 0
            and sum(x[int(self.__n // 3) :]) == int(self.__n // 3)
        )

        # Sum of entries (l0 norm) & weighted sum (enhanced by optimal entries)
        weighted_sum = np.arange(1, self.__n + 1).astype(float) / float(self.__n) * 10
        weighted_sum[self.__n // 2 :] *= -1
        weighted_sum = sum(weighted_sum * x)
        l0_norm = sum(x)

        if is_global_maximum:
            f = 10 * self.__n * self.__a + weighted_sum

        elif is_global_minimum:
            f = -10 * self.__n * self.__a - weighted_sum

        elif l0_norm > self.__a:
            # f = - 5 * abs(self.__n // 2 - l0_norm * self.__a) + self.__n/2
            f = -abs(self.__n // 2 - weighted_sum * self.__a) + self.__n / 2

        elif l0_norm <= self.__a:
            # f = 5 * abs(self.__n // 2 - 2*l0_norm * self.__a) + self.__n/2
            f = abs(self.__n // 2 - 2 * weighted_sum * self.__a) + self.__n / 2

        else:
            raise ValueError(
                f"This case is unexpected!\n" f"Found {l0_norm=} while a={self.__a}"
            )

        return f

    @property
    def global_maxima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global maximum value.
        """

        # First Optimal Solution
        n1 = int(self.__n // 2)
        x1 = np.zeros(
            self.__n,
            dtype=int,
        )
        x1[:n1] = 1

        # Initialize the solution list
        global_maxima = [
            x1,
        ]

        # Second Optimal Solution
        n2 = int(self.__n // 3)
        if 0 < n2 < n1:
            x2 = np.zeros(
                self.__n,
                dtype=int,
            )
            x2[:n2] = 1
            global_maxima.append(x2)

        return global_maxima

    @property
    def global_minima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global minimum value.
        """

        # First Optimal Solution
        n1 = int(self.__n // 2)
        x1 = np.zeros(
            self.__n,
            dtype=int,
        )
        x1[n1:] = 1

        # Initialize the solution list
        global_maxima = [
            x1,
        ]

        # Second Optimal Solution
        n2 = int(self.__n // 3)
        if 0 < n2 < n1:
            x2 = np.zeros(
                self.__n,
                dtype=int,
            )
            x2[n2:] = 1
            global_maxima.append(x2)

        return global_maxima


class AlternatingSignOptimizationFunction(TestFunction):
    """
    A weighting vector, of the same size as a passed vector `x`, is
    created on the form `(1, -1, 1, -1, ...)`.
    The weighting vector is pointwise multiplied by the passed vector and the
    objective is evaluated as the sum of the resulting vector.
    The global maximum (unique) is `(1, 0, 1, 0, ...)`
    The global minimum (unique) is `(0, 1, 0, 1, ...)`
    :parm n: positiveinteger representing dimension of the domain/variable
    """

    def __init__(self, n):
        assert (
            utility.isnumber(n) and int(n) == n and n >= 1
        ), f"Expected positive integer n; received {n=} of {type(n)=}"
        self.__n = int(n)

        # Create weighting vector
        self.__w = np.empty(self.__n)
        self.__w[0] = 1
        for i in range(1, self.__n):
            self.__w[i] = self.__w[i - 1] * -1

        # Global maximum and global minimum
        negative_indexes = np.where(self.__w < 0)[0]
        self.__global_max = np.ones_like(self.__w)
        self.__global_max[negative_indexes] = 0
        self.__global_min = 1 - self.__global_max

    def evaluate(self, x):
        """
        Evaluate the function at the passed `x` viewed as a boolean variable.
        """
        _x = utility.asarray(x, dtype=bool).flatten().astype(int)
        if _x.size != self.__n:
            raise TypeError(
                f"Invalid input type/shape. Expected x to be of size {self.__n}\n"
                f"Received {x=} of {type(x)=}"
            )
        else:
            x = _x

        return (x * self.__w).sum()

    @property
    def global_maxima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global maximum value.
        """
        return [self.__global_max]

    @property
    def global_minima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global minimum value.
        """
        return [self.__global_min]


class WeightedAlternatingSignOptimizationFunction(TestFunction):
    """
    A weighting vector, of the same size as a passed vector `x`, is
    created on the form `(1, -2, 3, -4, ...)`.
    The weighting vector is pointwise multiplied by the passed vector and the
    objective is evaluated as the sum of the resulting vector.
    The global maximum (unique) is `(1, 0, 1, 0, ...)`
    The global minimum (unique) is `(0, 1, 0, 1, ...)`
    :parm n: positiveinteger representing dimension of the domain/variable
    """

    def __init__(self, n):
        assert (
            utility.isnumber(n) and int(n) == n and n >= 1
        ), f"Expected positive integer n; received {n=} of {type(n)=}"
        self.__n = int(n)

        # Create weighting vector
        self.__w = np.empty(self.__n)
        self.__w[0] = 1
        for i in range(1, self.__n):
            self.__w[i] = self.__w[i - 1] * -1
        self.__w *= np.arange(1, self.__n + 1)

        # Global maximum and global minimum
        negative_indexes = np.where(self.__w < 0)[0]
        self.__global_max = np.ones_like(self.__w)
        self.__global_max[negative_indexes] = 0
        self.__global_min = 1 - self.__global_max

    def evaluate(self, x):
        """
        Evaluate the function at the passed `x` viewed as a boolean variable.
        """
        _x = utility.asarray(x, dtype=bool).flatten().astype(int)
        if _x.size != self.__n:
            raise TypeError(
                f"Invalid input type/shape. Expected x to be of size {self.__n}\n"
                f"Received {x=} of {type(x)=}"
            )
        else:
            x = _x

        return (x * self.__w).sum()

    @property
    def global_maxima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global maximum value.
        """
        return [self.__global_max]

    @property
    def global_minima(
        self,
    ):
        """
        A list of one or more values of the random variable `x`
        with f(x) being the global minimum value.
        """
        return [self.__global_min]


class FacilityFunction(TestFunction):
    r"""
    Given a matrix :math:`M \in \mathbb{R}^{m \times n}` with :math:`M_{i,j} \geq 0`, define the function

    .. math::

        f(x) := \sum_{i=1}^m \max M[i, x]

    for any binary vector :math:`x`. This function is guaranteed to be submodular.
    """

    def __init__(
        self, shape: tuple[int, int] | None = None, M: np.ndarray | None = None
    ):
        """
        Initialize the function with a matrix `M` or a shape to generate a random
        matrix.

        :param shape: The shape of the matrix to generate
        :param M: The matrix to use
        """
        if M is None:
            if shape is None:
                raise ValueError("Either M or shape must be provided")
            self._M = np.random.rand(*shape) + 1e-6
        else:
            self._M = M

    def evaluate(self, x: np.ndarray) -> float:
        """
        Evaluate the function at the passed `x` viewed as a boolean variable.

        :param x: The binary vector to evaluate
        """
        xi = x.astype(bool)
        if sum(xi) == 0:
            return 0
        return np.sum(np.max(self._M[:, xi], axis=1))
