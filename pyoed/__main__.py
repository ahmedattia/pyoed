# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from pyoed import app

if __name__ == "__main__":
    """
    """
    app.run()
