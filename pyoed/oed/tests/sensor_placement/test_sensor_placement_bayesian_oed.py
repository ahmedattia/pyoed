import pytest
import numpy as np

import pyoed
from pyoed.models.error_models.Gaussian import PointwiseWeightedGaussianErrorModel
from pyoed.tests import SETTINGS

pytestmark = pytest.mark.oed


@pytest.mark.usefixture("linear_smoother_sensor_placement_binary_oed")
def test_linear_sensor_placement_bayesian_binary_oed(linear_smoother_sensor_placement_binary_oed):

    oed_problem = linear_smoother_sensor_placement_binary_oed

    result = oed_problem.solve()
    print(f"{oed_problem=}")
    print(f"{type(result)=}")
    for k in result.optimization_results.items():
        print(f"{k=}")
    assert result.optimization_results.converged or result.optimization_results.maxiter_reached, "The OED problem did not converge!"


@pytest.mark.usefixture("linear_smoother_sensor_placement_binary_oed")
def test_linear_sensor_placement_bayesian_relaxed_oed(linear_smoother_sensor_placement_binary_oed):

    oed_problem = linear_smoother_sensor_placement_binary_oed

    # Update the observation error model to allow relaxation
    oed_problem.inverse_problem.register_observation_error_model(
        PointwiseWeightedGaussianErrorModel(
            oed_problem.inverse_problem.observation_error_model.configurations.asdict()
        )
    )

    # Update optimizer to solve by relaxation
    oed_problem.update_configurations(optimizer="ScipyOptimizer", optimizer_configs={'bounds': (0, 1)})

    result = oed_problem.solve()
    print(f"{oed_problem=}")
    print(f"{type(result)=}")
    for k in result.optimization_results.items():
        print(f"{k=}")
    assert result.optimization_results.success or result.optimization_results.maxiter_reached, "The OED problem did not converge!"

