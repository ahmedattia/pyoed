# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import pytest

pytestmark = pytest.mark.oed

from pyoed.oed.common.successive_laplace_oed import SuccessiveLaplaceOED


@pytest.mark.usefixtures("nonlinear_smoother_sensor_placement_binary_oed")
@pytest.mark.parametrize("prior_updating", [True, False])
def test_sloed(nonlinear_smoother_sensor_placement_binary_oed, prior_updating):
    """Test the BinaryOED class"""
    oed_problem = nonlinear_smoother_sensor_placement_binary_oed

    configs = {"oed_problem": oed_problem}

    with pytest.raises(Exception) as e_info:
        sloed_problem = SuccessiveLaplaceOED(configs)
    assert e_info.type is NotImplementedError

    if False:
        # TODO: Update this after fixing SuccessiveLaplaceOED
        result = sloed_problem.solve(
            max_sl_iterations=2,
            prior_updating=prior_updating,
        )
        assert all(
            [OEDR.converged for OEDR in result.OED_Results_iterates]
        ), "Not all OEDs converged!"
