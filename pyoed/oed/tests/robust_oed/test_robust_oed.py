import pytest
import numpy as np

import pyoed
from pyoed.tests import SETTINGS

pytestmark = pytest.mark.oed


@pytest.mark.usefixture("linear_smoother_robust_oed")
def test_linear_robust_sensor_placement_bayesian_binary_oed(
    linear_smoother_robust_oed
):
    oed_problem = linear_smoother_robust_oed

    result = oed_problem.solve()
    print(f"{oed_problem=}")
    print(f"{type(result)=}")
    for k in result.optimization_results.items():
        print(f"{k=}")
    assert result.optimization_results.converged or result.optimization_results.maxiter_reached, "The OED problem did not converge!"

