import numpy as np

from pyoed import utility
from pyoed.oed.utility_functions.alphabetic_criteria.d_opt import (
    BayesianInversionDOpt,
)
from pyoed.tests import assert_allclose

import pytest

pytestmark = pytest.mark.utility_functions


@pytest.mark.parametrize("evaluation_method", ["exact", "randomized"])
@pytest.mark.parametrize("use_FIM", [True, False])
def test_d_opt_linear_smoother(
    evaluation_method, use_FIM, linear_smoother_sensor_placement_binary_oed
):
    if not use_FIM and evaluation_method == "randomized":
        pytest.skip("Too slow to run.")
    oed_problem = linear_smoother_sensor_placement_binary_oed
    ip = oed_problem.inverse_problem
    d_opt = BayesianInversionDOpt(
        {
            "oed_problem": oed_problem,
            "evaluation_method": evaluation_method,
            "use_FIM": use_FIM,
        }
    )

    N_p = ip.prior.mean.size
    H = utility.asarray(ip.Hessian_matvec, (N_p, N_p))
    exact_val = np.log(np.linalg.det(H)) if use_FIM else np.log(np.linalg.det(np.linalg.inv(H)))

    # Test method: `evaluate`
    design = ip.observation_error_model.design
    if evaluation_method == "exact":
        assert_allclose(
            d_opt(design),
            exact_val,
            rtol=1e-07,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior ) value",
        )
    else:  # Average over multiple samples
        N_samples = 10
        values = []
        for _ in range(N_samples):
            values.append(d_opt(design))
            d_opt.update_configurations(randomization_vectors=None)
        assert_allclose(
            np.mean(values),
            exact_val,
            rtol=1e-01,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )


@pytest.mark.skip("Temporarily disabled")
@pytest.mark.parametrize("evaluation_method", ["exact", "randomized"])
@pytest.mark.parametrize("use_FIM", [True, False])
def test_d_opt_linear_filter(
    evaluation_method, use_FIM, linear_filter_sensor_placement_binary_oed
):
    if not use_FIM and evaluation_method == "randomized":
        pytest.skip("Too slow to run.")
    oed_problem = linear_filter_sensor_placement_binary_oed
    ip = oed_problem.inverse_problem
    d_opt = BayesianInversionDOpt(
        {
            "oed_problem": oed_problem,
            "evaluation_method": evaluation_method,
            "use_FIM": use_FIM,
        }
    )

    N_p = ip.prior.mean.size
    H = utility.asarray(ip.Hessian_matvec, (N_p, N_p))
    exact_val = np.log(np.linalg.det(H)) if use_FIM else np.log(np.linalg.det(np.linalg.inv(H)))

    # Test method: `evaluate`
    design = ip.observation_error_model.design
    if evaluation_method == "exact":
        assert_allclose(
            d_opt(design),
            exact_val,
            rtol=1e-07,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )
    else:  # Average over multiple samples
        N_samples = 10
        values = []
        for _ in range(N_samples):
            values.append(d_opt(design))
            d_opt.update_configurations(randomization_vectors=None)
        assert_allclose(
            np.mean(values),
            exact_val,
            rtol=1e-01,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )


@pytest.mark.skip("Temporarily disabled")
@pytest.mark.parametrize("evaluation_method", ["exact", "randomized"])
@pytest.mark.parametrize("use_FIM", [True, False])
def test_d_opt_nonlinear_filter(
    evaluation_method, use_FIM, nonlinear_filter_sensor_placement_binary_oed
):
    if not use_FIM and evaluation_method == "randomized":
        pytest.skip("Too slow to run.")
    oed_problem = nonlinear_filter_sensor_placement_binary_oed
    ip = oed_problem.inverse_problem
    d_opt = BayesianInversionDOpt(
        {
            "oed_problem": oed_problem,
            "evaluation_method": evaluation_method,
            "use_FIM": use_FIM,
        }
    )

    N_p = ip.prior.mean.size
    H = utility.asarray(lambda d: ip.Hessian_matvec(d, ip.posterior.mean), (N_p, N_p))
    exact_val = np.log(np.linalg.det(H)) if use_FIM else np.log(np.linalg.det(np.linalg.inv(H)))

    # Test method: `evaluate`
    design = ip.observation_error_model.design
    if evaluation_method == "exact":
        assert_allclose(
            d_opt(design),
            exact_val,
            rtol=1e-07,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )
    else:  # Average over multiple samples
        N_samples = 10
        values = []
        for _ in range(N_samples):
            values.append(d_opt(design))
            d_opt.update_configurations(randomization_vectors=None)
        assert_allclose(
            np.mean(values),
            exact_val,
            rtol=1e-01,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )


@pytest.mark.skip("Temporarily disabled")
@pytest.mark.parametrize("evaluation_method", ["exact", "randomized"])
@pytest.mark.parametrize("use_FIM", [True, False])
def test_d_opt_nonlinear_smoother(
    evaluation_method, use_FIM, nonlinear_smoother_sensor_placement_binary_oed
):
    if not use_FIM and evaluation_method == "randomized":
        pytest.skip("Too slow to run.")
    oed_problem = nonlinear_smoother_sensor_placement_binary_oed
    ip = oed_problem.inverse_problem
    d_opt = BayesianInversionDOpt(
        {
            "oed_problem": oed_problem,
            "evaluation_method": evaluation_method,
            "use_FIM": use_FIM,
        }
    )

    N_p = ip.prior.mean.size
    H = utility.asarray(lambda d: ip.Hessian_matvec(d, ip.posterior.mean), (N_p, N_p))
    exact_val = np.log(np.linalg.det(H)) if use_FIM else np.log(np.linalg.det(np.linalg.inv(H)))

    # Test method: `evaluate`
    design = ip.observation_error_model.design
    if evaluation_method == "exact":
        assert_allclose(
            d_opt(design),
            exact_val,
            rtol=1e-07,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )
    else:  # Average over multiple samples
        N_samples = 10
        values = []
        for _ in range(N_samples):
            values.append(d_opt(design))
            d_opt.update_configurations(randomization_vectors=None)
        assert_allclose(
            np.mean(values),
            exact_val,
            rtol=1e-01,
            atol=0,
            equal_nan=False,
            err_msg="Invalid 'evaluation_method' A-Opt (posterior log-det) value",
        )


@pytest.mark.skip("Temporarily disabled")
def test_d_opt_update_configurations(
    linear_smoother_sensor_placement_binary_oed,
    linear_filter_sensor_placement_binary_oed,
):
    """Test the `update_configurations` method of the BayesianInversionDOpt utility function"""
    oed_problem = linear_smoother_sensor_placement_binary_oed
    d_opt = BayesianInversionDOpt(
        {
            "oed_problem": oed_problem,
            "evaluation_method": "exact",
        }
    )

    ## Test method: `update_configurations`
    # update OED problem
    with pytest.raises(Exception) as e_info:
        d_opt.update_configurations(oed_problem=None)
    assert e_info.type is TypeError

    curr_oed_problem = d_opt.oed_problem
    new_oed_problem = linear_filter_sensor_placement_binary_oed
    d_opt.update_configurations(oed_problem=new_oed_problem)
    assert (
        d_opt.oed_problem is new_oed_problem is not curr_oed_problem
    ), "Failed to set the OED problem"
    # Reset OED problem
    d_opt.update_configurations(oed_problem=curr_oed_problem)

    # Test Updating the evaluation method
    curr_evaluation_method = d_opt.evaluation_method
    for evaluation_method in ["exact", "randomized"]:
        d_opt.update_configurations(evaluation_method=evaluation_method)
        assert (
            d_opt.evaluation_method == evaluation_method
        ), "Failed to set evalutaion method"
    d_opt.update_configurations(evaluation_method=curr_evaluation_method)

    # Test updating some or all evaluation settings and check randomization vectors
    with pytest.raises(Exception) as e_info:
        d_opt.update_configurations(evaluation_settings=None)
    assert e_info.type is TypeError
    for key, val in zip(
        [
            "sample_size",
            "optimize_sample_size",
            "min_sample_size",
            "max_sample_size",
            "distribution",
            "random_seed",
            "accuracy",
        ],
        [
            73,
            True,
            3,
            177,
            "Gaussian",
            123,
            1e-4,
        ],
    ):
        # Get current value, update, test, and reset
        curr_val = d_opt.evaluation_settings[key]
        d_opt.update_configurations(**{key: val})
        assert (
            d_opt.evaluation_settings[key] == val
        ), f"Failed to set evaluation config '{key}'"
        d_opt.update_configurations(**{key: curr_val})

    # Test updating randomization vectors
    curr_randomization_vectors = d_opt.evaluation_settings["randomization_vectors"]
    for randomization_vectors in [3, [12], (3, 4), ""]:
        with pytest.raises(Exception) as e_info:
            d_opt.update_configurations(randomization_vectors=randomization_vectors)
        assert e_info.type is TypeError
    assert (
        d_opt.evaluation_settings["randomization_vectors"] is curr_randomization_vectors
    )
