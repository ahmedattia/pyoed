import numpy as np
import pytest

from pyoed import utility
from pyoed.oed.utility_functions.lookup import (
    find_criteria,
)
from itertools import product
from pyoed.oed.utility_functions.alphabetic_criteria import (
    BayesianInversionAOpt,
    RobustBayesianInversionAOpt,
    RelaxedBayesianInversionAOpt,
)

pytestmark = pytest.mark.utility_functions


def test_find_criteria():
    a_opt_test_identifiers = [
        "".join(s) for s in product(["A", "a"], ["-", "_", " "], ["opt", "OPT"])
    ]
    for identifier in a_opt_test_identifiers:

        assert find_criteria("Bayesian " + identifier) == BayesianInversionAOpt

        assert find_criteria("Robust Bayesian" + identifier) == RobustBayesianInversionAOpt

        assert find_criteria("Relaxed Bayesian" + identifier) == RelaxedBayesianInversionAOpt




