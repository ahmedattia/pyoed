# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import numpy as np

from pyoed import utility
from pyoed.oed.utility_functions.alphabetic_criteria.d_opt import BayesianInversionDOpt
from pyoed.oed.utility_functions.information_criteria import BayesianInversionEIG
from itertools import product
from pyoed.tests import (
    assert_allclose,
)

import pytest

pytestmark = pytest.mark.utility_functions


## UPDATE the test once EIG module is updated
# TODO: Remove parallelization from EIG
# TODO: Fix the EIG, or atleast figure out what it isn't converging
@pytest.mark.skip("Temporarily disabled: EIG functionality needs work, currently broken!")
@pytest.mark.parametrize("ace", [False, True])
def test_eig_evaluation(ace, linear_filter_sensor_placement_binary_oed):
    """Test functionality of the EIG utility function"""
    oed_problem = linear_filter_sensor_placement_binary_oed

    eig = BayesianInversionEIG(
        {
            "oed_problem": oed_problem,
            "evaluation_method": "nmc",
            "inner_sample_size":256,
            "outer_sample_size":256,
            "adaptive_contrastive_estimation":ace,
            "random_seed":1011,
        }
    )

    design = oed_problem.design
    d_opt = BayesianInversionDOpt({"oed_problem": oed_problem, "use_FIM": False})
    prior_logdet = oed_problem.inverse_problem.prior.covariance_logdet()
    exact_value = 0.5 * (d_opt(design) + prior_logdet)

    nmc_estimate = eig(design)

    assert_allclose(
        nmc_estimate,
        exact_value,
        rtol=1e-02,
        atol=0,
        equal_nan=False,
        err_msg=f"Invalid valud of the posterior trace with evaluation_method 'NMC'",
    )
