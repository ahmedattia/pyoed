# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import numpy as np

from pyoed import utility
from pyoed.models.error_models.Gaussian import kl_divergence
from pyoed.models.error_models.Gaussian import GaussianErrorModel
from pyoed.oed.utility_functions.alphabetic_criteria.d_opt import BayesianInversionDOpt
from pyoed.oed.utility_functions.information_criteria import BayesianInversionKLDivergenceGaussian
from itertools import product
from pyoed.tests import (
    SETTINGS,
    assert_allclose,
)

import pytest

pytestmark = pytest.mark.utility_functions


@pytest.mark.parametrize("evaluation_method", ["exact", "random"])
def test_eig_evaluation(evaluation_method, linear_filter_sensor_placement_binary_oed):
    """Test functionality of the KL-DIV (between Gaussians) utility function"""
    oed_problem = linear_filter_sensor_placement_binary_oed

    # Inverse problem and prior
    ip = oed_problem.inverse_problem
    configs = ip.prior.configurations.asdict()
    configs.update({"random_seed": SETTINGS.RANDOM_SEED })
    prior = GaussianErrorModel(configs=configs)

    ip.solve(skip_map_estimate=False, update_posterior=True)

    # Posterior
    configs = ip.posterior.configurations.asdict()
    configs.update({"random_seed": SETTINGS.RANDOM_SEED })
    posterior = GaussianErrorModel(configs=configs)

    # Exact KL divergence
    exact_value = kl_divergence(
        prior,
        posterior,
        method="exact",
    )

    # Kill the posterior
    ip.register_prior(prior)

    ## EIG
    eig = BayesianInversionKLDivergenceGaussian(
        {
            "oed_problem": oed_problem,
            "evaluation_method": evaluation_method,
            "sample_size": 1000,
            "random_seed": SETTINGS.RANDOM_SEED,
        }
    )

    eig.oed_problem.inverse_problem.prior.random_seed = SETTINGS.RANDOM_SEED
    eig.oed_problem.inverse_problem.posterior.random_seed = SETTINGS.RANDOM_SEED
    calculated_value = eig(oed_problem.design)

    assert_allclose(
        calculated_value,
        exact_value,
        rtol=5e-02,
        atol=0,
        equal_nan=False,
        err_msg=f"Invalid KL Divergence Value",
    )
