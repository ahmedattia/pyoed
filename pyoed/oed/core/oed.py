# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements the base classes for OED objects.
These classes are not intended to be used by the user, and are only provided to
define the backbone of OED objects.
"""

import os
import pickle
import warnings
from contextlib import ContextDecorator

from dataclasses import dataclass
from typing import (
    Type,
    Iterable,
)

from pyoed import utility
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    PyOEDData,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
    aggregate_configurations,
)
from pyoed.optimization import (
    Optimizer,
    OptimizerResults,
    OptimizerConfigs
)
from .utility_functions import (
    Criterion,
)


@dataclass(kw_only=True, slots=True)
class OEDConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`OED` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`OEDConfigs`
    provides the following fields:

    :param name: name of the OED approach/method.
    :param optimizer: the optimization routine (optimizer) to be *registered*
        and later used for solving the OED problem.
        This can be one of the following:

        - `None`: In this case, no optimizer is registered, and the :py:meth:`solve`
          won't be functional until an optimization routine is registered.
        - An optimizer instance (object that inherits :py:class`Optimizer`).
          In this case, the optimizer is registered **as is** and is updated
          with the passed configurations if available.
        - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
          the optimizer.

    :param optimizer_configs: the configurations of the optimization routine.
        This can be one of the following:

        - `None`, in this case configurations are discarded, and whatever default configurations
          of the selected/passed optimizer are employed.
        - A `dict` holding full/partial configurations of the selected optimizer.
          These are either used to instantiate or update the optimizer configurations based on
          the type of the passed `optimizer`.
        - A class providing implementations of the configurations (this must be a subclass of
          :py:class:`OptimizerConfigs`.
        - An instance of a subclass of :py:class:`OptimizerConfigs` which is to set/udpate
          optimizer configurations.

    :param criterion: The optimality criterion to be optimized when solving the OED
        problem. This can be:

        - `None`; in this case, no criterion will be associated with the inversion
          OED instance (the object) upon instantiation.
          Thus, OED criterion association is lazy, and the user is allowed to
          assign the criterion after creating the inversion OED object by calling
          :py:meth:`register_optimality_criterion`.
        - an instance of :py:class:`Criterion` which provides access to
          an `evaluate` method which evaluates the value of the optimality criterion.
    """
    name: str | None = None
    design: None | Iterable = None
    optimizer : None | Optimizer | Type[Optimizer] = None
    optimizer_configs : None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None
    criterion: None | str | Criterion = None


@set_configurations(OEDConfigs)
class OED(PyOEDObject):
    """
    Base class for implementations of OED (Optimal Experimental Design) methods/approaches.

    :param dict | OEDConfigs | None configs: (optional) configurations for
        the optimization object

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """
    def __init__(self, configs: dict | OEDConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Each OED implementation should define its own design.
        design = self.update_design(self.configurations.design)
        if not self.is_valid_design(design):
            design = None
            if self.verbose:
                print(
                    f"The base OED problem couldn't set the design; "
                    f"See the exception details belwo (which is discarded) \n"
                    f"{err=} of {type(err)=}\n"
                    f"The *design* configuration is set to `None`."
                )
        self.configurations.design = design

        ## Update standardize configurations and and do post-creation tasks
        # Register the optimality criterion
        self.configurations.criterion = self.register_optimality_criterion(
            criterion=self.configurations.criterion,
        )

        # TODO: Consider an overkill in which `update_design` is called after setting
        #       the optimizer. This can be useful if `update_design` is to update the optimizer!


    def validate_configurations(
        self,
        configs: dict | OEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`OEDConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`OEDConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `name`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="name",
            test=lambda v: isinstance(v, (str, type(None))),
            message=f"Model name is of invalid type. Expected None or string. ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `design`: None or Iterable. More specific validation is added for each derived OED object
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="design",
            test=lambda v: v is None or utility.isiterable(v),
            message=f"Design is of invalid type. Expected None or an Iterable. ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `criterion`: str/Criterion/None
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="criterion",
            test=lambda x: x is None or isinstance(x, (str, Criterion)),
            message=f"criterion invalid type. Expected None or a callable/function.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Register the optimality criterion
        if "criterion" in kwargs:
            self.register_optimality_criterion(criterion=kwargs["criterion"])

        # Update the optimizer
        if "optimizer" in kwargs:
            optimizer = kwargs["optimizer"]
            optimizer_configs = kwargs.get('optimizer_configs', None)
            self.register_optimizer(optimizer=optimizer, optimizer_configs=optimizer_configs)
        else:
            if 'optimizer_configs' in kwargs:
                self.register_optimizer(
                    optimizer=self.optimizer,
                    optimizer_configs=kwargs['optimizer_configs'],
                )

        # Register the optimality criterion
        if "design" in kwargs:
            self.update_design(design)


    def register_optimality_criterion(
            self,
            criterion: Criterion | None = None,
    ) -> Criterion :
        """
        Validate and set the OED optimality criterion as described by `criterion`,
        update the configurations object with the criterion, and return the new
        criterion.

        :param criterion: This can be an
            instance that inherits from the base class :class:`.Criterion` and
            implements the method :meth:`.Criterion.evaluate` (or at-least
            duck-types it); or a string that describes a well-known optimality
            criterion.  Users can use the implementations provided in the module
            :mod:`..utility_functions` or implement their own.
            If `criterion` is `None`, no criterion is created. It is left as `None`.

        ..note::
            We allow `criterion` to be `None` initially since some optimality criteria
            implementations take an OED problem object as one of their configurations.
            This requires creating an OED problem first, and then creating the criterion
            and attaching it to the OED problem.
            Though, solving the OED problem won't be possible without a valid criterion.

        ..note::
            This **method** should be replaced with specific implementations (in derived classes)
            where the criterion is created and managed by the specific OED paradigm.
            In that case, more flexibility can be granted, such as passing string representation
            of the criterion.

        :reutrns: the registered criterion
        :raises TypeError: If `criterion` is not a valid input.
        """
        # Check the criterion and if legacy (string) is passed, replace it with utility
        # function objection
        if not isinstance(criterion, (type(None), Criterion)):
            raise TypeError(
                f"Expected criterion to be None or instance derived from pyoed.oed.Criterion\n"
                f"Received invalid {criterion=} of {type(criterion)=}"
            )

        # Update configurations and return
        self.configurations.criterion = criterion
        return criterion

    def register_optimizer(
        self,
        optimizer: None | Optimizer = None,
    ) -> None | Optimizer:
        """
        Register (and return) the passed optimizer.

        .. note::
            This method does not create a new optimizer instance.
            It just takes the created optimizer, makes sure it is an instance derived
            from the :py:class:`pyoed.optimization.Optimizer` and associates it with
            this assimilation (DA) object.

        .. note::
            A derived class is expected to create this optimizer, and pass it up
            by calling `super().register_optimizer(optimizer)` so that it can be
            registered properly.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer is not supported
        """
        if not isinstance(optimizer, (Optimizer, type(None))):
            raise TypeError(
                f"Unsupported optimizer {optimizer=} of {type(optimizer)=}\n"
                f"Expected an instance derived from the PyOED's optimizer base class "
                f"`pyeod.optimization.Optimizer`"
            )

        # Set the optimizer (and its configurations)
        # Update optimizer and configurations
        self.configurations.optimizer = optimizer
        if optimizer is None:
            if self.configurations.optimizer_configs is None:
                warnings.warn(
                    f"Optimizer `None` kills optimizer_configs"
                )
            self.configurations.optimizer_configs = None
        else:
            self.configurations.optimizer_configs = optimizer.configurations

        return self.configurations.optimizer

    def is_valid_design(self, design) -> bool:
        """
        Check if the passed design is valid. This method is provided to check whether
        the passed `design` has the right type/shape.
        This is important for validation.
        Here, no validation is really carried out.
        This method should overriden with proper validation/check of the passed design.

        :param design: a data structure containing the proposed experimental design.

        :returns: `True`/`False` indicating whether `design` should be accepted as
            a design or not.
        """
        return True

    def update_design(self, design):
        """
        Update the experimental design based on the passed `design` value.
        This method takes a new experimental design, validates its type/shape/...
        by calling :py:meth:`is_valid_design` and then update references to  that design.
        This method does not apply the design to the components of  the OED problem as
        each design is different in nature and applies to the OED problem based on
        the specific type of the OED problem at hand.
        Of course this is problem specific.

        .. note::

            This method is expected to be called by derived classes at the end of
            the implementation of the overriding method.
            The driven method provides specific implementation of the effect of the design
            on the components of the OED problem.
            For example, sensor placement OED problems apply the design to the observtion
            operator and/or the observation error model.

            In Bayesian inversion, and observational experimental design
            can change the observaiton vector, the observation operator,
            the parameter-to-observable-map, and/or the observation error
            covariance/precision matrix.

        .. note::
            This method udpates the *design* configurations so that proper access
            to aexperimental design is always guaranteed.

        :param design: the new experimental design.

        :returns: the passed design is returned

        :raises TypeError: if the design validation failes (i.e., the passed `design` is invalid)
        """
        if self.is_valid_design(design):
            self.configurations.design = design
        else:
            raise TypeError(
                f"Invalid design shape/type! "
                f"Received {design=} of {type(design)=}"
            )
        return design

    def design_manager(self, design):
        """
        Creat an return a context manager that enables updating the
        `design` to the passed value, execute needed code and then
        reset the design to the original value.

        Assuming `obj` is this object, and `val` is the passed design value,
        one wants to run the method `obj.run_code()`, the following code can be used

        .. code-block::

            with obj.design_manager(val) as mngr:
                mngr.run_code()

        :returns: a reference to `self` that enables calling any function under
            `self` and then automatically after executing the code, the design is reset.

        :raises TypeError: if the passed `design` is invalid (i.e., didn't
            pass :py:meth:`validate_design`).
        """
        if not self.is_valid_design(design):
            raise TypeError(
                f"Invalid design shape/type! "
                f"Received {design=} of {type(design)=}"
            )

        # NOTE: This can be moved outside this method if needed...
        class DesignManager(ContextDecorator):
            def __init__(self, oed_problem, design):
                # Keep track of the OED problem and the both current and new design values
                self.oed_problem = oed_problem
                self.original_design = oed_problem.design
                self.new_design = design

                # Call super's __init__
                super().__init__()

            def __enter__(self):
                # Set the design in the OED problem (before executing any code)
                self.oed_problem.design = self.new_design
                return self.oed_problem

            def __exit__(self, *exc):
                # Reset original design
                self.oed_problem.design = self.original_design

        return DesignManager(
            oed_problem=self,
            design=design,
        )


    def objective_function_value(self, *args, **kwargs, ):
        """
        Evaluate the value of the OED objective function given the passed arguments, and given the
        underlying configurations.
        """
        raise NotImplementedError(
            f"The object `{self}` does not provide a valid implementation of the "
            f"the `objective_function_value` method."
        )

    def oed_objective(self, *args, **kwargs, ):
        """
        Evaluate the value of the OED objective function given the passed arguments, and given the
        underlying configurations.

        .. warning::
            This method is added for backward compatibility, and it will be deprecated soon.
            User need to call :py:meth:`objective_function_value` instead.
        """
        warnings.warn(
            f"Calling `oed_objective` will be deprecated in the future. "
            f"This method will be replace with `objective_function_value`",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.objective_function_value(*args, **kwargs, )

    def plot_results(
        self,
        results,
        *args,
        **kwargs,
    ):
        """
        Generic plotting function for OED problems.
        Given the results returned by :py:meth:`solve`, visualize the results.
        Additional plotters can be added to this method or derived methods...

        :raises TypeError: if no valid optimizer is registered
        """
        if isinstance(results, OEDResults):
            _results = results.asdict()
        elif isinstance(results, dict):
            _results = results
            pass
        else:
            raise TypeError(
                f"Expected results to be dictionary or `InversionOEDResults`; "
                f"received {results=} of {type(results)=}!"
            )

        try:
            optimization_results = _results['optimization_results']
        except(KeyError) as err:
            raise TypeError(
                f"The passed results of {type(results)=} is not supported!"
                f"There is not `optimization_results` entry/key found! Trace the error below\n"
                f"Unexpected {err=} of {type(err)=}"
            )
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )
        plot_results = self.optimizer.plot_results(
            optimization_results,
            *args,
            **kwargs,
        )

        # TODO: Create additional plots (model domain, optimal locations, etc...)
        ...

        return plot_results

    def solve_oed_problem(self, *args, **kwargs):
        """
        Start solving the OED problem for the registered configuration with passed arguments.

        .. warning::
            This method is added for backward compatibility, and it will be deprecated soon.
            User need to call :py:meth:`solve` instead.
        """
        warnings.warn(
            f"Calling `solve_oed_problem` will be deprecated in the future. "
            f"This method will be replace with `solve`",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.solve(*args, **kwargs, )


    def solve(self, init_guess=None, ):
        """
        Start solving the OED optimization problem.

        .. note::
            This method needs to be replicated (rewritten) for any OED object so
            that it can replace the returned results object with teh appropriate one.

        :param init_guess: The initial guess of the design to be used as
            starting point of the optimization routine

        :returns: an instance of (derived from) OEDResults holding results
            obtained by solving the OED problem

        :raises TypeError: if no valid optimizer is registered
        """
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )

        # Solve the OED optimization problem, create a results object, and return it
        optimization_results = optimizer.solve(
            init_guess,
        )
        return OEDResults(
            {
                'oed_problem': self,
                'optimization_results': optimization_results,
            }
        )

    @property
    def optimizer(self):
        """
        Handle to the optimizer (optimization object) that solves the robust optimization
        problem
        """
        return self.configurations.optimizer

    @property
    def oed_criterion(self):
        """The OED optimality criterion"""
        return self.configurations.criterion
    @oed_criterion.setter
    def oed_criterion(self, val):
        """Set the OED optimality criterion (objective function to be optimized)"""
        return self.register_optimality_criterion(val)

    @property
    def design(self, ):
        """
        Return the underlying design
        """
        return self.configurations.design
    @design.setter
    def design(self, val):
        self.update_design(val)

    @property
    def design_size(self, ):
        """
        The dimension of the design space.
        Generally speaking, this should return the length/size of the design vector.
        Once can create a design vector then return its length, but the size should
        inferable without need to create a design vector.
        """
        warnings.warn(
            f"***The design size property `design_size` has not been defined for "
            f"the class: {self.__class__}***\n"
            f"Here, the length of the registered design (If not None) is used "
            f"which is inefficient!\n"
            f"Each OED object need to define the size of the design based on the "
            f"specific implementation. \nThe design is an abstract object that"
            f"can be seen as a vector, in this case, one can define and use its size."
        )
        design = self.configurations.design
        if design is None:
            return None
        else:
            return len(design)


@dataclass(kw_only=True, slots=True)
class OEDResults(PyOEDData):
    """
    Base class to hold OED data/results

    :param optimization_results: the results object returned from calling the optimizer.
    :param oed_problem: instance of a class derived from :py:class:`OED`, such
        as :py:class:`InversionOED`
    """
    oed_problem: Type[OED] | None = None
    optimization_results: Type[OptimizerResults] | None = None

    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  PyOED Empty OED Data Container\n{sep}\n"
        return msg

    def data_dictionary(self):
        """
        Return a dictionary containing picklable results without class dependencies

        .. warning::
            This method is added for backward compatibility, and it will be deprecated soon.
            User need to call :py:meth:`asdict` instead.
        """
        warnings.warn(
            f"Calling `data_dictionary` will be deprecated in the future. "
            f"This method will be replace with `asdict`",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.asdict(*args, **kwargs, )

    def write_results(self, saveto):
        """
        Save the underlying OED results to pickled dictionary.

        .. warning::
            This is a very elementary class method that requires all object attributes
            to be serializable (picklable).
            Each specific derived class should provide a more tailored implementation.

        :param saveto: name/path of the file to save data to.
        :raises TypeError: if `saveto` is not a valid file path
        :raises IOError: if writing failed
        """
        # Check output file path
        saveto = os.path.abspath(saveto)
        path = os.path.dirname(saveto)
        if not utility.path_is_accessible(path):
            raise IOError(
                f"The file path {saveto=} is not accessible!"
            )
        else:
            # Create directory if needed
            d, _ = os.path.split(saveto)
            if not os.path.isdir(d):
                os.makedirs(d)

        # Try writing results
        try:
            pickle.dump(self.asdict(), open(saveto, "wb"))
            if self.verbose:
                print(f"Results dumped/saved to: {saveto}")
        except Exception as err:
            print(
                f"\n************\n"
                f"Failed to write OEDResults, Check the error raised below "
                f"which is ignored intentionally"
                f"Unexpected{err=} of {type(err)=}"
            )

    @property
    def optimal_design(self):
        """
        A *copy* of the optimal design extracted from the `optimization` results object
        """
        try:
            return self.optimization_results.x
        except Exception as err:

            raise TypeError(
                f"Failed to extract optimal design from the optimization results;\n"
                f"{self.optimization_results=}\n"
                f"Unexpected {err=} of {type(err)=}"
            )

    @classmethod
    def load_results(cls, readfrom, ):
        """
        Inspect pickled file, and load OED results;

        .. warning::
            This is a very elementary class method that requires all object attributes
            to be serializable (picklable).
            Each specific derived class should provide a more tailored implementation.

        :raises IOError: if loading failed
        """
        try:
            data = pickle.load(open(readfrom, 'rb'))
        except Exception as err:
            raise IOError(
                f"Failed to load OEDResults from {readfrom} \n"
                f"Unexpected{err=} of {type(err)=}"
            )
        return cls(configs=data)

