# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides Base Classes for solving robust OED problems in general, and
for robust OED problems specialized for inversion OED (employing inverse problems).

.. note::

    We choose to make both :py:class:`RobustOED` and :py:class:`RobustInversionOED`
    independent (and assocaited configs classes) from each other rather than making
    :py:class:`RobustOED` multi-inherit both :py:class:`RobustOED` and
    :py:class:`InversionOED`.
    This is mainly becuase issue that we foresee resulting from multi-inheritance if
    we multi-inherit configs/dataclasses.

    Since the focus of PyOED is mainly on model-constrained OED problems, all robust
    OED we currently plan are actually going to inherit :py:class:`RobustInversionOED`,
    however, we add :py:class:`RobustOED` for future extensibility.
    General-purpose functionality will be elevated from :py:class:`RobustInversionOED`
    into :py:class:`RobustOED` as needed, and the inheritance pattern will be revisited
    without affecting the interface and in a way to preserve backward compatibility.
"""

from dataclasses import dataclass
from pyoed.configs import set_configurations

from .oed import (
    OED,
    OEDConfigs,
    OEDResults,
)

from .inversion_oed import (
    InversionOED,
    InversionOEDConfigs,
    InversionOEDResults,
)


@dataclass(kw_only=True, slots=True, )
class RobustOEDConfigs(OEDConfigs):
    """
    Configurations class for the :py:class:`RobustOED` class.  This class
    inherits functionality from :py:class:`OEDConfigs` and only adds new class-level
    variables which can be updated as needed.
    """
    ...

@set_configurations(configurations_class=RobustOEDConfigs)
class RobustOED(OED):
    """
    Base class for robust OED in general.
    """
    ...


@dataclass(kw_only=True, slots=True)
class RobustOEDResults(OEDResults):
    """
    Base class for objects holding results of :py:class:`RobustOED`
    """
    ...


@dataclass(kw_only=True, slots=True, )
class RobustInversionOEDConfigs(InversionOEDConfigs):
    """
    Configurations class for the :py:class:`RobustInversionOED` class.  This class
    inherits functionality from :py:class:`InversionOEDConfigs` and only adds new class-level
    variables which can be updated as needed.
    """
    ...


@set_configurations(configurations_class=RobustInversionOEDConfigs)
class RobustInversionOED(InversionOED):
    """
    Base class for robust OED for inverse problems.
    """
    ...


@dataclass(kw_only=True, slots=True)
class RobustInversionOEDResults(InversionOEDResults):
    """
    Base class for objects holding results of :py:class:`RobustInversionOED`
    """
    ...

