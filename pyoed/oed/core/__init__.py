# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This package implements the core components of OED algorithms.
Implementations here are to be extended by OED algoirthms are not intended to
be instantiated by the user directly.
"""

from .utility_functions import (
    CriterionConfigs,
    Criterion,
    # Aliases
    UtilityConfigs,
    Utility,
)

from .protocols import (
    SupportsRelaxedOptimization,
    SupportsRobustOptimization,
)

from .oed import (
    OEDConfigs,
    OED,
    OEDResults,
)

from .inversion_oed import (
    InversionOEDConfigs,
    InversionOED,
    InversionOEDResults,
    InversionCriterionConfigs,
    InversionCriterion,
    Linearization,
    LP_Behavior,
    BayesianInversionCriterionConfigs,
    BayesianInversionCriterion,
    # Aliases
    InversionUtilityConfigs,
    InversionUtility,
    BayesianInversionUtilityConfigs,
    BayesianInversionUtility,
)

from .sensor_placement_oed import (
    SensorPlacementInversionOEDConfigs,
    SensorPlacementInversionOED,
    SensorPlacementInversionOEDResults,
)

from .robust_oed import (
    RobustOEDConfigs,
    RobustOED,
    RobustOEDResults,
    RobustInversionOEDConfigs,
    RobustInversionOED,
    RobustInversionOEDResults,
)

__all__ = [v for v in dir()]
