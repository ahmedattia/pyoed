# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements base classes for all OED utility function (optimality criteria)
in PyOED. The baseclass :py:class:`Criterion` also aliased to :py:class:`Utility`
represent is the base class that need to be inherited by all specific OED optimality
criteria.

In general, the term optimality criterion is often used in the context of minimization
while the term utility function is associated with maximization. In PyOED, we use both
terms *utility functions*  and *optimality criterion* alternatively, to give the user
flexibility.
"""

from abc import abstractmethod
from dataclasses import dataclass

from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    set_configurations,
)


@dataclass(kw_only=True, slots=True)
class CriterionConfigs(PyOEDConfigs):
    """
    Configuration class for utility functions / optimality criteria.
    There are no default configurations at this stage.
    """

    ...


@set_configurations(CriterionConfigs)
class Criterion(PyOEDObject):
    """
    Base class for utility functions and optimality criteria used in OED.

    .. note::

        - The term 'utility function' is associated with rewards and is thus used in the
          context of maximization.  Examples include expected information gain, expected
          KL-divergence, Fisher content, etc.
        - Conversly, the term 'optimality criterion' is usually associated with a
          minimization problem.  Examples include posterior uncertainty; e.g., posterior
          covariance trace (A-opt), and log-det (D-opt).
        - instances of 'Utility' or its alias `OptimalityCriterion` can be used
          for both maximization and minimization equally. One has to be only aware of
          the difference and how the utility/criterion is developed.
    """

    def __init__(self, configs: dict | CriterionConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    @abstractmethod
    def evaluate(self, *args, **kwargs):
        """
        Evaluate the utility function / optimality criterion at the given point(s).
        Typically :code:`*args` will just be :math:`\\zeta` (the design variable) and
        :code:`**kwargs` will be empty.  However, some utility functions may require
        additional, hence the abstract interface is left open.
        """
        ...

    def __call__(self, *args, **kwargs):
        """This is a wrapper around :py:meth:evaluate"""
        return self.evaluate(*args, **kwargs)


## Alias so that utility functions and optimality criteria can be perceive equally
## Generally: OED Optimality <--> minimization; OED utility function <--> maximization
UtilityConfigs = CriterionConfigs
Utility = Criterion

