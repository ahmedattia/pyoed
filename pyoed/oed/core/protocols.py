# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides protocols related to utility functions.
These protocols check if a utility function supports a specific
type/approch to OED by inspecting availabitliy and suitability of
specific methods/attributes, etc.
"""

from typing import Protocol, runtime_checkable
from pyoed import utility


@runtime_checkable
class SupportsRelaxedOptimization(Protocol):
    """
    Protocol to check if a utility function supports relaxed optimization. To do so, an
    object must support the method `grad_design` along with getters/setters for
    `formulation_approach` and `pcw_scheme`. You can use regular python `isinstance` to
    check with this protocol.
    """

    def grad_design(self, design): ...

    @property
    def formulation_approach(self): ...

    @formulation_approach.setter
    def formulation_approach(self, approach): ...

    @property
    def pcw_scheme(self): ...

    @pcw_scheme.setter
    def pcw_scheme(self, approach): ...


@runtime_checkable
class SupportsRobustOptimization(Protocol):
    """
    Protocol to check if a utility function supports robust optimization.
    To do so, an object must support the method `grad_uncertainty`.
    """

    def evaluate(self, design, uncertain_param, reset_param=False): ...

    def grad_uncertainty(self, design, uncertain_param, reset_param=False):
        """
        Evaluate the derivative of the utility function with respect to the uncertain
        parameter. The utility function takes two arguments `(design,
        uncertain_param_val)` as its input. The design is fixed, and diferences are
        calculated over the uncertain parameter.

        :param design: an observational design vector conformable with the observation.
        :param uncertain_param_val: the value of the uncertain parameter.

        :return: The gradient of the utility function criterion with respect to the uncertain
            parameter.
        :rtype: np.ndarray
        """
        return utility.finite_differences_gradient(
            lambda p: self.evaluate(  # type: ignore
                design,
                p,
                reset_param=reset_param,
            ),
            uncertain_param,
        )

