# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides Basic Classes for solving OED problems in general.
Specialized OED implementations such as relaxed and binary stochastic OED
have their own implementations in separate modules e.g., `relaxed_oed`,
`binary_oed`, etc.

.. note::
    This module includes implementation of base classes for both OED objects
    and OED optimality criteria. We couldn't split those in two modules due to
    circular import as each would need to import the other.
"""

import os
import warnings
import re
from dataclasses import dataclass
from enum import StrEnum
from typing import Callable, Type

import numpy as np

from pyoed import utility
from pyoed.optimization import OptimizerResults
from pyoed.assimilation import InverseProblem
from pyoed.configs import (
    PyOEDData,
    PyOEDConfigsValidationError,
    validate_key,
    aggregate_configurations,
    set_configurations,
)
from .utility_functions import (
    CriterionConfigs,
    Criterion,
)
from .oed import (
    OED,
    OEDConfigs,
    OEDResults,
)


########################################################################################
# Inversion (Inverse Problems) OED Objects, Configs, & Results:
# -------------------------------------------------------------
# - InversionOEDConfigs
# - InversionOED
# - InversionOEDResults
########################################################################################


@dataclass(
    kw_only=True,
    slots=True,
)
class InversionOEDConfigs(OEDConfigs):
    """
    Configurations class for the :py:class:`InversionOED` class.  This class
    inherits functionality from :py:class:`OEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`OEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`InversionOEDConfigs`
    provides the following fields:

    :param inverse_problem: an instance of an inverse problem :py:class:`InverseProblem`.
        This can be a filter or a smoother object with access to underlying
        properties/methods/attributes of the inverse problems:

        - prior
        - posterior
        - solve_inverse_problem()
        - observation_operator
        - observation_error_model

    :param problem_is_linear: a flag that defines how to regard the inverse problem,
        and consequently the OED problem. The following values are accepted:

        - If not set (`None` is passed), it will be detected by testing
          the linearity of the inverse problem upon instantiation.
        - If set to `True`, the underlying forward problem (both simulation model and
          observation operator) are linear. In this case, the posterior is Gaussian, and
          the posterior uncertainties (covariances) are independent from the data. Thus,
          the inverse problem is solved once to find the MAP point (posterior
          mean/mode), and the posterior covariance is defined/constructed.
          Note that finding the MAP point is not necessary since posterior covariances
          are independent from it.
        - If 'False', either the simulation model, the observation operator or both
          are nonlinear; In this case, the posterior is non-Gaussian, and the posterior
          uncertainties (covariances) are dependent on the data (through the MAP
          estimate).
          To deal with this situation, we follow one of two approaches:

          - The inverse problem is solved to find the MAP point (posterior mean/mode),
            approximate the posterior covariance (assuming a Gaussian) around the MAP,
            find an optimal design, and repeat.
          - Use KL-divergence between prior and posterior using MC estimate of KL.

    :param criterion: The optimality criterion to be optimized when solving the OED
        problem. This overrides the `criterion` in the base OED class with additional type.
        Specifically, this can be:

        - `None`; in this case, no criterion will be associated with the inversion
          OED instance (the object) upon instantiation.
          Thus, OED criterion association is lazy, and the user is allowed to
          assign the criterion after creating the inversion OED object by calling
          :py:meth:`register_optimality_criterion`.
        - an instance of :py:class:`Criterion` which provides access to
          an `evaluate` method which evaluates the value of the optimality criterion.
        - a string holding the name of the optimality criterion (if widely popular and
          implemented by PyOED).
          For more about passing string representation of the criterion, check
          the documentation of :py:meth:`register_optimality_criterion`

    :param penalty_function: function to evaluate the regularization/sparsification
        penalty (soft constraint) in the *popular* regularized OED optimization formulation.
    :param penalty_function_gradient: gradient/derivative of the `penalty_function`
        with respect to the design. For example, this is used for the relaxation approach
        where a binary design is allowed to take values in the continuous interval ``[0, 1]``.
    :param penalty_weight: scalar penalty/regularization parameter multiplied by the
        `penalty_function` in the objective function.

    .. note::
        The OED optimization objective function is defined as the sum of optimality criterion
        plus a regularization term. The regularization term is the product of a penalty function
        with a pnalty weight.
    """

    inverse_problem: InverseProblem | None = None
    problem_is_linear: bool | None = None
    criterion: None | str | Criterion = None
    penalty_function: None | Callable = None
    penalty_function_gradient: None | Callable = None
    penalty_weight: None | float = 0.0


@set_configurations(configurations_class=InversionOEDConfigs)
class InversionOED(OED):
    """
    The main class for all implementations of OED for Inverse Problems (Bayesian of not).
    This class requires an inverse problem, an optimality criterion,
    and (optional) regularization term to configure the underlying OED optimization problem.

    .. note::
        The experimental design here is abstract and is only dectated by how the
        optimality criterion is defined. Thus, `InversionOED` is a generic class in
        which one can choose different experimental design.

    ..note::
        This class can be inherited and:

        1. update the :py:meth:`update_design` definition to define an
           experimental design of any nature.

        2. Implement :py:meth:`solve` method to solve the OED problem.

        All other functionality is not restricted to a specific type of design.

    .. warning::
        This class is very much gradual by construction, we will continue to
        change and improve.

    :param dict | OEDConfigs | None configs: (optional) configurations for
        the inversion OED object.

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """

    def __init__(self, configs: dict | OEDConfigs | None = None) -> None:

        # Create the configurations (if valid)
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        ## Set additional/local attributes
        self._PROBLEM_NUM_SOLVES = (
            0  # Number of times the inverse problem has been solved
        )

    def validate_configurations(
        self,
        configs: dict | OEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate the passed configurations object.

        :param configs: configurations to validate. If a :py:class:`OEDConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`OEDConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(
            configs=configs,
        )

        # Local tests
        is_bool = lambda x: utility.isnumber(x) and (x == bool(x))
        is_bool_or_None = lambda x: x is None or is_bool(x)
        is_float = lambda x: utility.isnumber(x) and (x == float(x))

        ## Validation Stage
        # `inverse_problem`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="inverse_problem",
            test=lambda v: isinstance(v, InverseProblem),
            message=f"inverse_problem is of invalid type. Expected instance of InverseProblem",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `problem_is_linear`: None/bool
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="problem_is_linear",
            test=is_bool_or_None,
            message=f"problem_is_linear invalid type. Expected None or bool.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `penalty_function`: Callable/None
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="penalty_function",
            test=lambda x: x is None or callable(x),
            message=f"penalty_function invalid type. Expected None or a callable/function.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `penalty_function_gradient`: Callable/None
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="penalty_function_gradient",
            test=lambda x: x is None or callable(x),
            message=f"penalty_function_gradient invalid type. Expected None or a callable/function.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `penalty_weight`: float/None
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="penalty_weight",
            test=is_float,
            message=f"penalty_weight invalid type. Expected None or float.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Resetting inverse problem? update problem_is_linear flag & reset number of solves
        if "inverse_problem" in kwargs:
            if "criterion" not in kwargs:
                warnings.warn(
                    f"Updating inverse problem without passing a new criterion "
                    f"can result in inconsistencies.\n"
                    f"One should create a new OED object if the inverse problem changes"
                )
            if "problem_is_linear" in kwargs:
                problem_is_linear = kwargs["problem_is_linear"]
            else:
                problem_is_linear = None

            self.configurations.problem_is_linear = problem_is_linear
            self._PROBLEM_NUM_SOLVES = 0

        # Register any of the compoennts of the penalty term (if passed)
        if (
            "penalty_function" in kwargs
            or "penalty_function_gradient" in kwargs
            or "penalty_weight" in kwargs
        ):
            # Extract what's passed, and use current if not passed, then register
            penalty_weight = kwargs.get(
                "penalty_function", self.configurations.penalty_weight
            )
            penalty_function = kwargs.get(
                "penalty_function", self.configurations.penalty_function
            )
            penalty_function = kwargs.get(
                "penalty_function_gradient",
                self.configurations.penalty_function_gradient,
            )
            self.register_penalty_term(
                penalty_function=penalty_function,
                penalty_function_gradient=penalty_function_gradient,
                penalty_weight=penalty_weight,
            )

    def register_penalty_term(
        self,
        penalty_function: None | Callable = None,
        penalty_function_gradient: None | Callable = None,
        penalty_weight: None | float = None,
    ) -> None:
        """
        Validate and set (in the configurations) the elements of the penalty term in the
        OED optimizaiton objective. These are (penalty function, the gradient of
        the penalty function, and regularization parameter)

        ..note::
            This method does the following:

            1. Transform the input to standardized
               formats/types. The penalty function and its gradient are assured to be callables,
               and the penalty weight is a float.
            2. Update the underlying configurations `self.configurations` object
            3. Return the standardized forms of the input.

        ..note::
            This method checks that the passed combination is compatible with each others
            and with the settings/configuration of this OED object.
            If the penalty_function is `None`, it is set to a zero-valued lambda function.
            If the penalty_function_gradient is `None`, it is calculated by using finite
            differences if called. If `penalty_function` is `None` the gradient
            `penalty_function_gradient` is set to a zero-valued delta function.
            If the penalty_weight is None, it is set to 0.

        :param penalty_function: function to evaluate the regularization/sparsification
            penalty (soft constraint)
        :param penalty_function_gradient: gradient of the penalty function.
        :param penalty_weight: scalar penalty/regularization

        :returns: standardized version of the passed arguments
            - penalty_function: callable
            - penalty_function_gradient: callable
            - penalty_weight: float

        :raises TypeError: if any of the passed arguments is of the wrong type
        """
        # Penalty weight
        if penalty_weight is None:
            penalty_weight = 0.0
        elif not utility.isnumber(penalty_weight):
            raise TypeError(
                f"Penalty weight is expected to be None or float; "
                f"received {penalty_wight=} of {type(penalty_weight)=}"
            )

        # Penalty function
        if penalty_function is penalty_function_gradient is None:
            # Neither is passed
            penalty_function = lambda x: 0.0
            penalty_function_gradient = lambda x: 0.0

        elif penalty_function_gradient is None:
            # Only penalty_function is passed
            if not callable(penalty_function):
                raise TypeError(
                    f"penalty_function must be a callable; "
                    f"received {penalty_function=} of {type(penalty_function)=}"
                )

            # Penalty function gradient with finite differences
            penalty_function_gradient = lambda x: utility.finite_differences_gradient(
                fun=penalty_function,
                state=x,
            )

        elif penalty_function_gradient is None:
            # Only penalty_function_gradient is passed
            raise TypeError(
                f"Passing penalty function gradient without the penalty function\n"
                f" is not allowed.\nUpdating penalty function gradient requires "
                f"updating the penalty function as well!"
            )

        else:
            if not callable(penalty_function):
                raise TypeError(
                    f"penalty_function must be a callable; "
                    f"received {penalty_function=} of {type(penalty_function)=}"
                )
            if not callable(penalty_function_gradient):
                raise TypeError(
                    f"penalty_function_gradient must be None or a callable; "
                    f"received {penalty_function_gradient=} of {type(penalty_function_gradient)=}"
                )

        # Update configurations
        self.configurations.penalty_weight = penalty_weight
        self.configurations.penalty_function = penalty_function
        self.configurations.penalty_function_gradient = penalty_function_gradient

        # Return (this is overdowing it. Added for clarity though)
        return penalty_function, penalty_function_gradient, penalty_weight

    def solve_inverse_problem(
        self,
        init_guess=None,
        skip_map_estimate=False,
        update_posterior=False,
    ):
        """
        Solve the underlying inverse problem, i.e., find the analysis state given the registered
        observation and prior information.

        .. note::
            This is a wrapper of (with the same signature as) the `solve_inverse_problem` method of
            the underlying inverse problem.
            This interface increments the counter `_PROBLEM_NUM_SOLVES` which keeps track of number
            of time the inverse problem is solved.

        :param init_guess: initial state/guess of the model state/parameter;
            if not passed (i.e., `None`), the solution starts at the prior mean as the initial guess
        :param bounds: `None` or valid interval bounds to pass to the optimization routine
        :param bool skip_map_estimate: use the prior mean as a map estimte (ignore observations) and upate posterior.
            This useful for OED when the model  is linear
        :param bool update_posterior: if True, the posterior mean and covariance operator are updated
            (given/around the solution of the inverse problem)

        :returns: the analysis state (MAP estimate of the posterior)

        :raises:
            - TypeError if any of the 4D-Var elements (model, observation operator, prior, solver, or data) is missing
            - ValueError if the underlying optimization routine is not supported yet
        """
        self._PROBLEM_NUM_SOLVES += 1
        return self.inverse_problem.solve(
            init_guess=init_guess,
            skip_map_estimate=skip_map_estimate,
            update_posterior=update_posterior,
        )

    def objective_function_value(
        self,
        design,
    ):
        """
        Evaluate the value of the OED objective function given the passed experimental design
        The objective function is defined as follows:

        .. math::
            \\mathcal{J}(d) = \\mathcal{U}(d) + \\alpha \\Phi(d)

        where the objective function :math:\\mathcal{J}`` is a real-valued function
        composed of
        :math:`\\mathcal{U}` is the registered optimality criterion (utility function),
        :math:`\\Phi` is the real-valued penalty function (e.g., sparsity promoting function),
        :math:`\\alpha` is the penalty parameter.

        .. note::
            The experimental design here is abstract and is only dectated by how the
            optimality criterion is defined.

        :param design: an experimental design to evaluate the objectiv function at.

        :returns: value of the *registered* OED optimality criterion
        :raises: TypeError is raised if no optimality criterion has been registered yet
        """
        # Update the design of the observation error model
        criterion = self.oed_criterion
        if criterion is None:
            raise TypeError(
                f"***No OED criterion has been registered yet!**.\n"
                f"A valid optimality criterion must be set before evaluting the objective. "
                f"To set a criterion call `register_optimality_criterion()` first!"
            )

        # Calculate the optimality criterion value by calling __call__ or `.evaluate()`
        obj_val = criterion.evaluate(design)

        # Add penalty term
        if self.penalty_weight != 0:
            obj_val += self.penalty_weight * self.penalty_function(design)

        # Return
        return obj_val

    def solve(
        self,
        init_guess=None,
    ):
        """
        Start solving the OED optimization problem.
        This methods SHOULD be overridden by child derived classes with
        additional functionality.
        Here, the registered optimizer is inspected and is used to solve the
        optimization problem (if registered.)

        :param init_guess: The initial guess of the design to be used as
            starting point of the optimization routine

        :returns: an instance of (derived from) :py:class:`InversionOEDResults`
            holding results obtained by solving the OED problem

        :raises TypeError: if no valid optimizer is registered
        """
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )

        # Solve the OED optimization problem, create a results object, and return it
        optimization_results = optimizer.solve(
            init_guess,
        )
        results = InversionOEDResults(
            {
                "oed_problem": self,
                "optimization_results": optimization_results,
            }
        )

    def plot_results(
        self,
        results,
        overwrite=False,
        bruteforce=False,
        show_legend=True,
        output_dir=None,
        keep_plots=False,
        fontsize=20,
        line_width=2,
        usetex=True,
        show_axis_grids=True,
        axis_grids_alpha=(0.25, 0.4),
        plots_format="pdf",
        **kwargs,
    ):
        """
        Plot results of solving the OED optimization problem.
        This function calls super's plotter (which calls the optimizer plotter),
        and then attempts to use the inverse problem to plot domain, observations,
        etc.

        The signature of this method is identical to that of the OED's base class
        method :py:meth:`pyoed.oed.OED.plot_results`.

        """
        if self.verbose:
            print("Creating Optimization Plots (using the optimizer plotter)")

        plot_results = super().plot_results(
            results,
            overwrite=overwrite,
            bruteforce=bruteforce,
            show_legend=show_legend,
            output_dir=output_dir,
            keep_plots=keep_plots,
            fontsize=fontsize,
            line_width=line_width,
            usetex=usetex,
            show_axis_grids=show_axis_grids,
            axis_grids_alpha=axis_grids_alpha,
            plots_format=plots_format,
            **kwargs,
        )

        # Plot optimal sensors/design vs global optimum design
        try:
            figures = self._plot_model_results(results)
            plot_results["figures"].update(figures)

        except Exception as err:
            print(
                f"Failed to create plots by employing inverse problem elements.\n"
                f"See the details below.\n"
                f"Unexpected{err=} of {type(err)=}\n"
                f"This is intentionally discarded..."
            )
            raise TypeError("TODO: clear this error after debugging ...")

        return plot_results

    def _plot_model_results(
        self,
        results,
    ):
        """
        Plot results by employing model and observation operator, etc. from
        the inverse problem
        """
        if self.verbose:
            print(
                "Creating Additional Plots (employing the inverse problem elements)"
            )

        try:
            optimal_design = results.x
        except(AttributeError):
            optimal_design = None
        try:
            global_optimal_design = results.global_optimal_design
        except(AttributeError):
            global_optimal_design = None

        try:
            optimal_policy_sample = results.optimal_policy_sample
        except(AttributeError):
            optimal_policy_sample = None

        # Figures dictionary
        figures = {}

        ip = self.inverse_problem
        if hasattr(ip.model, "plot_domain"):
            observation_grid = ip.observation_operator.get_observation_grid()

            if global_optimal_design is not None:
                saveto = _os.path.join(
                    output_dir, f"global_optimal_design.{plots_format}"
                )
                sensors = observation_grid[np.where(global_optimal_design)[0], :]
                ax = ip.model.plot_domain(
                    sensors_coordinates=sensors,
                    save_to_file=saveto,
                    title="Global Optimal Design",
                )

                # Update figures dictionary
                fig = ax.get_figure()
                figures.update(
                    {
                        saveto.rstrip(f".{plots_format}").split(os.path.sep)[
                            -1
                        ]: fig,
                    }
                )

            if optimal_design is not None:
                saveto = _os.path.join(output_dir, f"optimal_design.{plots_format}")
                sensors = observation_grid[np.where(optimal_design)[0], :]
                ax = ip.model.plot_domain(
                    sensors_coordinates=sensors,
                    save_to_file=saveto,
                    title="Optimal Design",
                )

                # Update figures dictionary
                fig = ax.get_figure()
                figures.update(
                    {
                        saveto.rstrip(f".{plots_format}").split(os.path.sep)[
                            -1
                        ]: fig,
                    }
                )

            # Plot optimal design sample
            if optimal_policy_sample is not None:
                for i, design in enumerate(optimal_policy_sample):
                    saveto = _os.path.join(
                        output_dir, f"optimal_design_sample{i+1}.{plots_format}"
                    )
                    sensors = observation_grid[np.where(design)[0], :]
                    ax = ip.model.plot_domain(
                        sensors_coordinates=sensors,
                        save_to_file=saveto,
                        title=f"Sampled Design {i+1}/{len(optimal_policy_sample)}",
                    )

                    # Update figures dictionary
                    fig = ax.get_figure()
                    figures.update(
                        {
                            saveto.rstrip(f".{plots_format}").split(os.path.sep)[
                                -1
                            ]: fig,
                        }
                    )

        else:

            observation_grid = ip.observation_operator.get_observation_grid()
            observation_grid, _ = utility.validate_Cartesian_grid(observation_grid)

            if global_optimal_design is not None:
                saveto = _os.path.join(
                    output_dir, f"global_optimal_design.{plots_format}"
                )
                sensors = observation_grid[np.where(global_optimal_design)[0], :]
                ax = utility.plot_sensors(
                    sensors_coordinates=observation_grid,
                    active_sensors_coordinates=sensors,
                    save_to_file=saveto,
                    title="Global Optimal Design",
                )

                # Update figures dictionary
                fig = ax.get_figure()
                figures.update(
                    {
                        saveto.rstrip(f".{plots_format}").split(os.path.sep)[
                            -1
                        ]: fig,
                    }
                )

            if optimal_design is not None:
                saveto = _os.path.join(output_dir, f"optimal_design.{plots_format}")
                sensors = observation_grid[np.where(optimal_design)[0], :]
                ax = utility.plot_sensors(
                    sensors_coordinates=observation_grid,
                    active_sensors_coordinates=sensors,
                    save_to_file=saveto,
                    title="Optimal Design",
                )

            # Plot optimal design sample
            if optimal_policy_sample is not None:
                for i, design in enumerate(optimal_policy_sample):
                    saveto = _os.path.join(
                        output_dir, f"optimal_design_sample{i+1}.{plots_format}"
                    )
                    sensors = observation_grid[np.where(design)[0], :]
                    ax = utility.plot_sensors(
                        sensors_coordinates=observation_grid,
                        active_sensors_coordinates=sensors,
                        save_to_file=saveto,
                        title=f"Sampled Design {i+1}/{len(optimal_policy_sample)}",
                    )

                    # Update figures dictionary
                    fig = ax.get_figure()
                    figures.update(
                        {
                            saveto.rstrip(f".{plots_format}").split(os.path.sep)[
                                -1
                            ]: fig,
                        }
                    )

        return figures


    @property
    def inverse_problem(self):
        return self.configurations.inverse_problem

    @property
    def problem_is_linear(self):
        """
        A flag that identifies linearity of the OED problem by checking
        linearity of the inverse problem
        """
        result = self.configurations.problem_is_linear
        if result is None:
            # Lazy update of the flag
            result = self.configurations.problem_is_linear = (
                utility.inverse_problem_linearity_test(self.inverse_problem)
            )
        return result

    @problem_is_linear.setter
    def problem_is_linear(self, val):
        """
        A flag that identifies linearity of the inverse problem

        .. warning::
            This should be used only if the user wants to assert (or is sure of)
            linearity of the problem to avoid checking test.
        """
        return self.update_configurations(problem_is_linear=val)

    @property
    def number_of_forward_solves(self):
        """Number of forward solves (simulation + observation) applied so far"""
        return self._PROBLEM_NUM_SOLVES

    @property
    def penalty_function(self):
        """Give access to the underlying inverse problem"""
        return self.configurations.penalty_function

    @penalty_function.setter
    def penalty_function(self, val):
        """Update the penalty function"""
        return self.register_penalty_term(
            penalty_function=val,
            penalty_function_gradient=None,
            penalty_weight=self.penalty_weight,
        )

    @property
    def penalty_function_gradient(self):
        """Give access to the underlying inverse problem"""
        return self.configurations.penalty_function_gradient

    @penalty_function_gradient.setter
    def penalty_function_gradient(self, val):
        """Update the gradient of the penalty function"""
        return self.register_penalty_term(
            penalty_function=self.penalty_function,
            penalty_function_gradient=val,
            penalty_weight=self.penalty_weight,
        )

    @property
    def penalty_weight(self):
        """Give access to the underlying inverse problem"""
        return self.configurations.penalty_weight

    @penalty_weight.setter
    def penalty_weight(self, val):
        """Update the penalty term weight/regularization parameter"""
        return self.register_penalty_term(
            penalty_function=self.penalty_function,
            penalty_function_gradient=self.penalty_function_gradient,
            penalty_weight=val,
        )

    @property
    def design_size(self):
        """Design size; e.g., number of candidate sensor locations"""
        return len(self.design)


@dataclass(kw_only=True, slots=True)
class InversionOEDResults(OEDResults):
    """
    An implementation for an object to properly store OED results.
    In addition to parameters/attributes in :py:class:`OEDResults`, this class provides
    the following attributes:

    :param oed_problem: instance of a class derived from :py:class:`OED`, such
        as :py:class:`InversionOED`
    """

    oed_problem: Type[InversionOED] | None = None


########################################################################################
# Inversion (Inverse Problems) Optimality Criteria (Utility Functions):
# ---------------------------------------------------------------------
# - InversionCriterionConfigs <--> InversionUtilityConfigs
# - InversionCriterion        <--> InversionUtility
########################################################################################


@dataclass(kw_only=True, slots=True)
class Linearization(PyOEDData):
    """
    Data class for linearization point (MAP estimate) and its corresponding design.

    :param linearization_point: MAP estimate (linearization point) of the posterior.
    :param design: design vector corresponding to the linearization point.
    """

    point: np.ndarray | None = None
    design: np.ndarray | None = None

    def __bool__(self):
        return (self.linearization_point is not None) and (self.design is not None)

    def __str__(self):
        return (
            "Linearization point: \n"
            f"MAP estimate: {self.linearization_point}\n"
            f"Design: {self.design}\n"
        )


class LP_Behavior(StrEnum):
    LINEAR = r"\Alinear\Z"
    MANUAL = r"\Amanual\Z"
    MAP = r"\Amap\Z"
    PRIOR = r"\Aprior\Z"


@dataclass(kw_only=True, slots=True)
class InversionCriterionConfigs(CriterionConfigs):
    """
    Configuration class for inversion utility functions / optimality criteria.

    :param oed_problem: OED problem for which the utility function is defined.
        This must be an instance of :py:class:`InversionOED`.
    :param lp_behavior: behavior of the linearization point (MAP, prior, manual).
        By default, it is set to MAP. However, if the oed problem is linear, the passed
        parameter is ignored and is changed to LINEAR instead. If you wish to forcefully
        choose the linearization point despite the linearity of the problem, set the
        field after instantiation.
    """

    oed_problem: InversionOED | None = None
    lp_behavior: LP_Behavior | str = LP_Behavior.MAP


@set_configurations(InversionCriterionConfigs)
class InversionCriterion(Criterion):
    """
    Class for utility functions / optimality criteria defined for inverse problems. This
    class is a subclass of :py:class:`Criterion` and thus inherits all its methods and
    attributes. In addition, it provides a method to to check/update the current
    linearization point.

    .. note::
        A linearization point is the point at which the model is linearized (if nonlinear)
        to construct a Laplacian approximation of the posterior.
    """

    def __init__(self, configs: dict | InversionCriterionConfigs | None = None):
        """
        Constructor for inversion utility functions / optimality criteria.

        :param dict configs: Object holding configurations for the criterion.
        """
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Create a linearization point (point at which forward model is linearized)
        self._LINEARIZATION = Linearization(design=self.oed_problem.design)
        if self.oed_problem.problem_is_linear:
            self.configurations.lp_behavior = LP_Behavior.LINEAR
        if isinstance(v := self.configurations.lp_behavior, str):
            for p in LP_Behavior:
                if re.match(p, v, re.IGNORECASE):
                    self.configurations.lp_behavior = p

    def validate_configurations(
        self,
        configs: dict | InversionCriterionConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        aggregated_configs = self.aggregate_configurations(configs)

        if not validate_key(
            aggregated_configs,
            configs,
            "oed_problem",
            test=lambda x: isinstance(x, InversionOED),
            message="Expected 'oed_problem' to be an instance of 'InversionOED'",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        def lp_behavior_test(x):
            if isinstance(x, str):
                return any(re.match(p, x, re.IGNORECASE) for p in LP_Behavior)
            return isinstance(x, LP_Behavior)

        if not validate_key(
            aggregated_configs,
            configs,
            "lp_behavior",
            test=lambda x: lp_behavior_test,
            message=(
                "Expected 'lp_behavior' to be an instance of 'LP_Behavior', ",
                "Or a string matching one of the LP_Behavior patterns.",
            ),
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        super().update_configurations(**kwargs)
        if "oed_problem" in kwargs:
            self.configurations.oed_problem = kwargs["oed_problem"]
            self._LINEARIZATION = Linearization(self.oed_problem.design)

        if "lp_behavior" in kwargs:
            value = kwargs["lp_behavior"]
            if isinstance(value, str):
                for p in LP_Behavior:
                    if re.match(p, value, re.IGNORECASE):
                        value = p
            self.configurations.lp_behavior = value

    def update_linearization(
        self,
        design=None,
        linearization_point=None,
    ) -> np.ndarray:
        """
        Update the point at which Gaussian approximation (linearization) is taken. This
        is carried out (if no linearization point is passed) by solving the inverse
        problem for maximum a posterior (MAP) estimate. Note that is needed only if the
        problem is nonlinear, thus this method should be called only if the problem is
        nonlinear, otherwise, evaluating MAP estimate is unnecessary.

        :param design: an observational design conformable with the observation models.
            Default is None.
        :param linearization_point: If passed, this is used as the linearization point.
            Default is None.

        :returns: the resulting linearization point (MAP estimate if computed).

        .. note::
            If the the `design` is `None` it is set to the current design associated
            with the OED problem `self.design`. If`linearization_point` is `None`, the
            inverse problem is solved (after setting the design of the OED problem to
            `design`), and the MAP estiamte is set as the linearization point
        """
        # Get current design from the OED problem, and set the design to the passed one
        design_bak = self.oed_problem.design
        if design is None:
            design = design_bak
        else:
            self.oed_problem.design = design

        if linearization_point is None:
            # Solve the inverse problem (to construct the posterior)
            linearization_point = self.oed_problem.solve_inverse_problem(
                self.oed_problem.inverse_problem.prior.mean,  # init_guess
                skip_map_estimate=False,
                update_posterior=False,
            )

        # Reset the design
        if design_bak is not design:
            self.oed_problem.design = design_bak

        # Set the internal linearization point, and return it
        self.linearization = Linearization(
            point=utility.asarray(linearization_point),
            design=utility.asarray(design),
        )
        return linearization_point

    def check_linearization(
        self,
        design,
    ) -> np.ndarray | None:
        """
        Check availability of linearization point if needed for evaluation of the
        utility function. If the problem is linear, the linearization point is not
        required and this method returns `None`. If the problem is nonlinear, and the
        tracked design is different from the passed one, a new linearization point is
        computed and returned.

        :param design: an observational design vector conformable with the observation.

        :returns: the map estimate (linearization point)
        """
        printer = lambda s: print(s) if self.verbose else None
        lpb = self.lp_behavior

        if lpb == LP_Behavior.MANUAL:
            printer("Using the manually set linearization point")
            return self.linearization.point
        elif lpb == LP_Behavior.LINEAR or self.oed_problem.problem_is_linear:
            printer("The problem is linear, no linearization point is required")
            return None
        elif lpb == LP_Behavior.PRIOR:
            printer("Using the prior mean as the linearization point")
            return self.oed_problem.inverse_problem.prior.mean
        elif np.all(design == self.linearization.design):
            printer("Reusing the existing linearization point (MAP) estimate")
            return self.linearization.point
        # Nonlinear problem, new design, need a new linearization point.
        printer("Calculating new linearization point via MAP estimate.")
        return self.update_linearization(design=design, )

    @property
    def configurations(self) -> InversionCriterionConfigs:
        return self._CONFIGURATIONS

    @property
    def linearization(self) -> Linearization:
        """Return the linearization data"""
        return self._LINEARIZATION

    @linearization.setter
    def linearization(self, value):
        assert isinstance(value, Linearization), (
            "Expected 'value' to be an instance of 'Linearization' "
            f"but got {type(value)=}"
        )
        self._LINEARIZATION = value

    @property
    def lp_behavior(self) -> LP_Behavior:
        """Return the linearization point behavior"""
        return self.configurations.lp_behavior

    @lp_behavior.setter
    def lp_behavior(self, value):
        self.update_configurations(lp_behavior=value)

    @property
    def oed_problem(self):
        """Return the underlying OED problem"""
        return self.configurations.oed_problem

    @oed_problem.setter
    def oed_problem(self, value):
        self.update_configurations(oed_problem=value)


# Add an alias so that utility functions and optimality criteria can be perceived
# equally
InversionUtilityConfigs = InversionCriterionConfigs
InversionUtility = InversionCriterion


########################################################################################
# Bayesian Inversion (Bayesian Inverse Problems) Optimality Criteria:
# -------------------------------------------------------------------
# - BayesianInversionCriterionConfigs  <--> BayesianInversionUtilityConfigs
# - BayesianInversionCriterion         <--> BayesianInversionUtility
########################################################################################


@dataclass(kw_only=True, slots=True)
class BayesianInversionCriterionConfigs(InversionCriterionConfigs):
    """
    Configuration class for Bayesian utility functions / optimality criteria.
    Subclass of :py:class:`InversionCriterionConfigs`, thus inherits all its attributes.
    """

    ...


@set_configurations(BayesianInversionCriterionConfigs)
class BayesianInversionCriterion(InversionCriterion):
    """
    Class for utility functions / optimality criteria defined for Bayesian inverse
    problems. This class is a subclass of :py:class:`InversionCriterion` and thus
    inherits all its methods and attributes.
    """

    def __init__(self, configs: dict | BayesianInversionCriterionConfigs | None = None):
        """
        Constructor for utility functions / optimality criteria.

        :param dict configs: Object holding configurations for the criterion.
        """
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | BayesianInversionCriterionConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        # TODO: Test if the OED problem is over a Bayesian inverse problem

        return super().validate_configurations(configs, raise_for_invalid)


# Add an alias so that utility functions and optimality criteria can be perceived
# equally
BayesianInversionUtilityConfigs = BayesianInversionCriterionConfigs
BayesianInversionUtility = BayesianInversionCriterion
