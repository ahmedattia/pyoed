# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides Base Classes for solving model-constrained OED problems
for sensor placement. The most common (but not the only one) is sensor placement for
Bayesian inverse problems.

.. note::

    A sensor placement OED problem is, by definition, a problem that involves an
    inverse problem (like most OED problem PyOED focuses on).
    Thus, we have only one :py:class:`SensorPlacementInversionOED` from which
    all sensor placement OED problems here are to be derived.
    We add the corresponding configuration base class
    ::
"""

from dataclasses import dataclass
from pyoed.configs import set_configurations

from .oed import (
    OED,
    OEDConfigs,
    OEDResults,
)

from .inversion_oed import (
    InversionOED,
    InversionOEDConfigs,
    InversionOEDResults,
)


@dataclass(kw_only=True, slots=True, )
class SensorPlacementInversionOEDConfigs(InversionOEDConfigs):
    """
    Configurations class for the :py:class:`SensorPlacementInversionOED` class.  This class
    inherits functionality from :py:class:`InversionOEDConfigs` and only adds new class-level
    variables which can be updated as needed.
    """
    ...


@set_configurations(configurations_class=SensorPlacementInversionOEDConfigs)
class SensorPlacementInversionOED(InversionOED):
    """
    Base class for sensor placement OED for inverse problems.
    """
    ...


@dataclass(kw_only=True, slots=True)
class SensorPlacementInversionOEDResults(InversionOEDResults):
    """
    Base class for objects holding results of :py:class:`SensorPlacementInversionOED`
    """
    ...

