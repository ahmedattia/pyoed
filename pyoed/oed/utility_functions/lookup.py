# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This *utility* module module provides functions to match a pattern/name
provided by the user to existing optimality criteria (utility functions).
For example, the user can create an OED object (instance of :py:class:`pyoed.oed.OED`)
with `criterion` configuration value `"Bayesian-A-opt"` and let functionality of this
module lookup the matching class that is matches this descriptiopn.

To use this functionality, just call the :py::`find_criteria("Bayesian A-Opt")`, and note
that the the name passed can be upper/lower case, and the dash can be replaced with a space
or an underscore or can be ommitted.

.. note::
    Since this module allows flexibility, some of the functionality are continually
    updated for best performance and greater flexiblity. Moreover, as more criteria
    are added to the `pyoed.oed.utility_functions` package, more criteria will be added to this
    module and the lookup functionality herein.
"""

import re
from enum import StrEnum
from typing import Type

from pyoed.oed.core import Criterion

# Import created classes in the utility_functions package as they are implemented/tested.
from .alphabetic_criteria.a_opt import (
    BayesianInversionAOpt,
    RelaxedBayesianInversionAOpt,
    RobustBayesianInversionAOpt,
)

from .alphabetic_criteria.d_opt import (
    BayesianInversionDOpt,
    RelaxedBayesianInversionDOpt,
    RobustBayesianInversionDOpt,
)

########################################################################################
# regex conveniences
########################################################################################

def __MATCHER(regex: str | list[str], name: str) -> bool:
    """
    Given a regular expression (or list of regular expressions) in `regex`,
    match that to the `name` of the optimality criterion expected and
    return a boolean flag `True`/`False` indicating whether `name`
    matches/similar to the criterion `regex`.

    :param regex: the expected formal/correct name of the criterion
    :param name: the regular expression containing string passed by user
    """
    if isinstance(regex, str):
        pass
    elif isinstance(regex, (list, tuple)):
        regex = r"(-| |_)*".join(regex)
    else:
        raise TypeError(
            f"The matcher `__MATCHER` received unexpected "
            f"{regex=} of {type(regex)=}"
        )

    if re.match(regex, name, re.IGNORECASE):
        return True
    else:
        return False

class __CRITERIA_REGEX_PATTERNS(StrEnum):
    """
    Collection of regex patterns for criteria.
    """

    BAYESIAN = r"(bayes|bayesian)"
    RELAXED = r"relaxed"
    ROBUST = r"robust"
    A_OPT = r"a(-|_| )*(opt|optimality)"
    D_OPT = r"d(-|_| )*(opt|optimality)"
    EIG = r"(eig|(expected(_|-|)information(_|-|)gain))"


########################################################################################
# Collection of criteria
########################################################################################

class AVAILABLE_CRITERIA(StrEnum):
    """
    Enum class for available criteria.
    """
    BayesianInversionAOpt = "BayesianInversionAOpt"
    BayesianInversionDOpt = "BayesianInversionDOpt"
    RelaxedBayesianInversionAOpt = "RelaxedBayesianInversionAOpt"
    RelaxedBayesianInversionDOpt = "RelaxedBayesianInversionDOpt"
    BayesianEIG = "BayesianEIG"
    RobustBayesianInversionAOpt = "RobustBayesianInversionAOpt"
    # RobustBayesianInversionDOpt = "RobustBayesianInversionDOpt"


def _match_criteria(kind: str) -> Type[Criterion]:
    ptrn = __CRITERIA_REGEX_PATTERNS
    if __MATCHER([ptrn.BAYESIAN, ptrn.A_OPT], kind):
        return BayesianInversionAOpt

    elif __MATCHER([ptrn.BAYESIAN, ptrn.D_OPT], kind):
        raise NotImplementedError("BayesianInversionDOpt is not implemented yet!")

    elif __MATCHER([ptrn.BAYESIAN, ptrn.EIG], kind):
        raise NotImplementedError("BayesianEIG is not implemented yet!")

    elif __MATCHER([ptrn.RELAXED, ptrn.BAYESIAN, ptrn.A_OPT], kind):
        return RelaxedBayesianInversionAOpt

    elif __MATCHER([ptrn.RELAXED, ptrn.BAYESIAN, ptrn.D_OPT], kind):
        raise NotImplementedError(
            "SensorPlacementBayesianInversionDOpt is not implemented yet!"
        )

    elif __MATCHER([ptrn.ROBUST, ptrn.BAYESIAN, ptrn.A_OPT], kind):
        return RobustBayesianInversionAOpt

    elif __MATCHER([ptrn.ROBUST, ptrn.BAYESIAN, ptrn.D_OPT], kind):
        return RobustBayesianInversionDOpt

    else:
        raise ValueError(
            f"Invalid criterion kind: {kind}!"
            f" Avaliable options are\n{', '.join(AVAILABLE_CRITERIA)}"
        )


########################################################################################
# Public lookup interface
########################################################################################


def get_available_criteria() -> list[str]:
    """
    Return a list of available criteria.
    """
    return [c for c in AVAILABLE_CRITERIA]


def find_criteria(criteria: str) -> Type[Criterion]:
    """
    Check if the given criteria is an instance of any criteria. If so, return the
    corresponding class; otherwise, return None.

    :param str | Criterion criteria: criteria to check. See
        :py:func:`get_available_criteria` for options.
    :raises ValueError: if the given kind is not a valid option
    :raises NotImplementedError: if the corresponding criteria is not implemented
    """
    return _match_criteria(criteria)
