# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This subpackage provides implementations of various utility functions and optimality
criteria used in optimal experimental design (OED).
"""
from . import (
    alphabetic_criteria,
    information_criteria,
)

from .lookup import (
    find_criteria,
    get_available_criteria,
)
