# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import re
from enum import StrEnum
import numpy as np
from typing import Sequence
from dataclasses import dataclass

from pyoed import utility
from pyoed.models.error_models.Gaussian import kl_divergence
from pyoed.configs import (
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
)
from pyoed.oed.core import (
    BayesianInversionCriterion,
    BayesianInversionCriterionConfigs,
)

class EvaluationMethod(StrEnum):
    EXACT = r"\Aexact\Z"
    RANDOMIZED = r"\A(random|randomized)\Z"


@dataclass(kw_only=True, slots=True)
class BayesianInversionKLDivergenceGaussianConfigs(BayesianInversionCriterionConfigs):
    """
    Configuration class for Bayesian KL-Divergence (between Gaussians) criterion.
    This configurations class holds configurations/attributes for configuring
    :py:class:`BayesianInversionKLDivergenceGaussia` utility functions.

    :param evaluation_method: A string describing the method to use for computing the
        criterion and its gradients. Supported are:

        - :code:`"exact"`: Compute the exact value of the criterion. Potentially
            intractable if problem is too large.
        - :code:`"randomized"`: Use randomized algorithms to compute the
            criterion value.  Potentially faster for large problems but may be
            inaccurate.

    :param sample_size: The number of samples to use for randomized algorithms. Default
        is 50.
    :param optimize_sample_size: If `True`, the sample size is optimized based on the
        problem size. Default is `False`.
    :param min_sample_size: The minimum sample size to use for randomized algorithms.
        Default is 10.
    :param max_sample_size: The maximum sample size to use for randomized algorithms.
        Default is 100.
    :param distribution: The distribution to use for randomization. Default is
        "Rademacher".
    :param randomization_vectors: The randomization vectors to use for randomized
        algorithms. Default is `None`. Note, if `None`, the randomization vectors are
        constructed and saved for future use during the first evaluation.
    :param random_seed: The random seed to use for randomized algorithms. Default is
        `None`.
    """
    evaluation_method: str = "exact"
    sample_size: int = 50
    optimize_sample_size: bool = False
    min_sample_size: int = 10
    max_sample_size: int = 100
    distribution: str = "Rademacher"
    randomization_vectors: None | np.ndarray | Sequence = None
    random_seed: None | int = None
    accuracy: float = 1e-2

@set_configurations(BayesianInversionKLDivergenceGaussianConfigs)
class BayesianInversionKLDivergenceGaussian(BayesianInversionCriterion):
    """
    An implementation of the Kullback-Leibler divergence criterion for Bayesian
    Optimal Design of Experiments. This particular version is under the assumption
    that BOTH the prior and the posterior are Gaussian. This is true if the inverse
    problem is linear, but in the case it isn't, this assumption corresponds to the
    Laplace approximation of the posterior. The criterion is defined as

    .. math::
       D_{\\text{KL}}\\left({\\mathcal {N}}_{0}\\parallel {\\mathcal {N}}_{1}\\right)
       ={\\frac {1}{2}}\\left(\\operatorname {tr} \\left(\\Sigma_{1}^{-1}\\Sigma_{0}\\right)
       +\\left(\\mu_{1}-\\mu_{0}\\right)^{\\mathsf{T}}\\Sigma_{1}^{-1}\\left(\\mu_{1}-\\mu_{0}\\right)
       -k+\\ln \\left({\\frac {\\det \\Sigma_{1}}{\\det \\Sigma_{0}}}\\right)\\right)

    .. note:: See https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence
    """
    def __init__(self, configs: dict | BayesianInversionKLDivergenceGaussianConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Make sure the evaluation method is named properly
        self.update_evaluation_method(self.configurations.evaluation_method)

    def validate_configurations(
        self,
        configs: dict | BayesianInversionKLDivergenceGaussianConfigs,
        raise_for_invalid: bool = True,
    ):
        """
        A method to check the passed configurations and make sure they are conformable
        with each other, and with current configurations (or default of not set)

        :param configs: an object holding key/value configurations
        :param raise_for_invalid: if `True` raise :py:class:`TypeError`
            for invalid configrations key

        :returns: True/False flag indicating whether passed configurations is valid or
            not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        is_nonnegative_float = lambda v: utility.isnumber(v) and np.allclose(float(v), v) and v >= 0
        is_int = lambda v: utility.isnumber(v) and int(v) == v
        is_positive_int = lambda v: is_int(v) and v > 0
        is_positive_int_or_None = lambda v: v is None or is_positive_int(v)

        def is_valid_method(method):
            if not isinstance(method, str):
                return False
            else:
                valid = False
                for p in EvaluationMethod:
                    if re.match(p, method, re.IGNORECASE):
                        valid = True
                        break
                return valid

        # `optimize_sample_size` are boolean
        for key in ["optimize_sample_size", ]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=lambda x: isinstance(x, bool),
                message="Expected '{key=}' to be Boolean.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # `distribution`,  are strings
        for key in ["distribution", ]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=lambda x: isinstance(x, str),
                message="Expected `{key=}` to be string.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # `evaluation_method`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="evaluation_method",
            test=is_valid_method,
            message="Invalid evaluation_method",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `accuracy` Non negative float
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="accuracy",
            test=is_nonnegative_float,
            message="Expected accuracy to non-negative number (float).",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `random_seed`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=is_positive_int_or_None,
            message="Expected random_seed to be positive integer or None.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `distribution`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="distribution",
            test=lambda x: isinstance(x, str),
            message="Expected evaluation method to be string",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # Samples sizes (positive integers)
        for key in ["sample_size", "min_sample_size", "max_sample_size"]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=is_positive_int,
                message=f"Expected '{key}' flag to be positive integer.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # TODO: Maybe validate the randomization vectors or leave them to the function
        # evaluations!
        # randomization_vectors (None or 2D array!)

        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_evaluation_method(self, evaluation_method):
        """
        Update the evaluation method.
        """
        if not isinstance(evaluation_method, str):
            raise PyOEDConfigsValidationError(
                f"Invlid {type(evaluation_metho)=}; expected string"
            )
        if re.match(EvaluationMethod.EXACT, evaluation_method, re.IGNORECASE):
            self.configurations.evaluation_method = "exact"

        elif re.match(EvaluationMethod.RANDOMIZED, evaluation_method, re.IGNORECASE):
            self.configurations.evaluation_method = "randomized"

        else:
            raise PyOEDConfigsValidationError(
                f"Invlid {evaluation_method=}; expected 'exact', or 'random', or 'randomized'"
            )

    def update_configurations(self, **kwargs) -> None:
        """
        Update one or more configurations given their name and value. Each configuration
        key/value is validated against current values (more precedence to passed, then
        current settings).

        If all passed configurations are conformable with each other, and with current
        configurations, the passed configurations updated the underlying configurations
        dictionary.
        """
        # Validate kwargs and update keys belonging to parents.
        super().update_configurations(**kwargs)

        if "evaluation_method" in kwargs:
            self.update_evaluation_method(kwargs["evaluation_method"])

        RESET_RANDOMIZATION_VECTORS = False
        if (v := kwargs.get("sample_size")) is not None:
            self.configurations.sample_size = v
            RESET_RANDOMIZATION_VECTORS = True
        if (v := kwargs.get("optimize_sample_size")) is not None:
            self.configurations.optimize_sample_size = v
            RESET_RANDOMIZATION_VECTORS = True
        if (v := kwargs.get("min_sample_size")) is not None:
            self.configurations.min_sample_size = v
            RESET_RANDOMIZATION_VECTORS = True
        if (v := kwargs.get("max_sample_size")) is not None:
            self.configurations.max_sample_size = v
            RESET_RANDOMIZATION_VECTORS = True
        if "distribution" in kwargs:
            self.configurations.distribution = kwargs["distribution"]
            RESET_RANDOMIZATION_VECTORS = True
        if "random_seed" in kwargs:
            self.configurations.random_seed = kwargs["random_seed"]
            RESET_RANDOMIZATION_VECTORS = True
        if ("randomization_vectors" in kwargs) or RESET_RANDOMIZATION_VECTORS:
            self.configurations.randomization_vectors = kwargs.get(
                "randomization_vectors"
            )


    def evaluate(self, design):
        """
        Evaluate the KL Divergence for the given OED problem at the given design.

        .. Warning::
           Currently, `oed.problem_is_linear` is ignored, which results in non-linear
           problems receiving the Gaussian approximation around the MAP point. This
           behavior is not guaranteed and may change in the future.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.

        :return: KL-Divergence between prior and (Gaussian) posterior.
        :rtype: float
        """
        ## Handle to OED and inverse problems
        oed_problem = self.oed_problem
        ip = oed_problem.inverse_problem

        ## Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design

        ## Solve the inverse problem and get posterior (Gaussian)
        ip.solve_inverse_problem(
            ip.prior.mean,
            skip_map_estimate=False,
            update_posterior=True,
        )

        # Reset the design
        oed_problem.design = current_design

        # Calculate the KL divergence between the two Gaussians (Prior || Posterior)
        return kl_divergence(
            ip.prior,
            ip.posterior,
            method=self.evaluation_method,
            sample_size=self.sample_size,
            optimize_sample_size=self.optimize_sample_size,
            min_sample_size=self.min_sample_size,
            max_sample_size=self.max_sample_size,
            distribution=self.distribution,
            random_seed=self.random_seed,
            accuracy=self.accuracy,
        )

    @property
    def evaluation_method(self):
        """
        The method used for computing the criterion value and its gradients. Two
        options:

        - 'exact': Compute the exact value. Potentially slow but accurate.
        - 'randomized': Use randomized algorithms. Fast but potentially inaccurate.
        """
        return self.configurations.evaluation_method

    @evaluation_method.setter
    def evaluation_method(self, value):
        self.update_configurations(evaluation_method=value)

    @property
    def sample_size(self):
        """Return the sample size for randomized algorithms"""
        return self.configurations.sample_size
    @sample_size.setter
    def sample_size(self, value):
        self.update_configurations(sample_size=value)

    @property
    def optimize_sample_size(self):
        """Return the flag indicating whether to optimize the sample size"""
        return self.configurations.optimize_sample_size
    @optimize_sample_size.setter
    def optimize_sample_size(self, value):
        self.update_configurations(optimize_sample_size=value)

    @property
    def min_sample_size(self):
        """Return the minimum sample size for randomized algorithms"""
        return self.configurations.min_sample_size
    @min_sample_size.setter
    def min_sample_size(self, value):
        self.update_configurations(min_sample_size=value)

    @property
    def max_sample_size(self):
        """Return the maximum sample size for randomized algorithms"""
        return self.configurations.max_sample_size
    @max_sample_size.setter
    def max_sample_size(self, value):
        self.update_configurations(max_sample_size=value)

    @property
    def distribution(self):
        """Return the distribution used for randomization"""
        return self.configurations.distribution
    @distribution.setter
    def distribution(self, value):
        self.update_configurations(distribution=value)

    @property
    def randomization_vectors(self):
        """
        Return the randomization vectors used for randomized algorithms;

        .. note::
            This assumes randomization is in the inference parameter space.
        """
        # Check randomization vectors if available; otherwise update them if needed
        r_vectors = self.configurations.randomization_vectors

        # Construct randomization vectors (Once)
        if re.match(r"\Arandom(ized|)\Z", self.evaluation_method, re.IGNORECASE):
            if r_vectors is None:
                # Holders
                op = self.oed_problem
                design = op.design
                ip = op.inverse_problem
                prior_size = ip.prior.size

                # Get the linearization point (if needed)
                lp = self.update_linearization(design)

                # Define the objective function to create linearization points for
                if self.use_FIM:
                    objfun = lambda x: ip.Hessian_matvec(x, eval_at=lp)
                else:
                    objfun = lambda x: ip.Hessian_inv_matvec(x, eval_at=lp)

                _, r_vectors = utility.matrix_trace(
                    objfun,
                    size=prior_size,
                    method=self.evaluation_method,
                    sample_size=self.sample_size,
                    optimize_sample_size=self.optimize_sample_size,
                    min_sample_size=self.min_sample_size,
                    max_sample_size=self.max_sample_size,
                    distribution=self.distribution,
                    random_seed=self.random_seed,
                    accuracy=self.accuracy,
                    #
                    return_randomization_vectors=True,
                )
                self.randomization_vectors = r_vectors

        # Now return valid linearization points
        return self.configurations.randomization_vectors

    @randomization_vectors.setter
    def randomization_vectors(self, value):
        self.update_configurations(randomization_vectors=value)

    @property
    def random_seed(self):
        """Return the random seed used for randomized algorithms"""
        return self.configurations.random_seed
    @random_seed.setter
    def random_seed(self, value):
        self.update_configurations(random_seed=value)

    @property
    def accuracy(self):
        """Return the accuracy used for randomized algorithms"""
        return self.configurations.accuracy
    @accuracy.setter
    def accuracy(self, value):
        self.update_configurations(accuracy=value)



