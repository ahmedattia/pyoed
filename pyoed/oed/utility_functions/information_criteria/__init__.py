# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This subpackage provides implementations of common information-based optimality criteria
(utility functions).
Implementations here are meant to be used for OED problems which by definition target
inverse problems.
"""

from .eig import (
    BayesianInversionEIGConfigs,
    BayesianInversionEIG
)

from .kl_divergence import (
    BayesianInversionKLDivergenceGaussianConfigs,
    BayesianInversionKLDivergenceGaussian,
)

