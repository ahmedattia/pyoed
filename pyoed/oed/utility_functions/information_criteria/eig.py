# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Module providing implementations of the EIG (expected information gain) OED criterion
for Bayesian inverse problems involving model-constrained inverse problems.
"""


import numpy as np
import re
from enum import StrEnum
from dataclasses import dataclass
import warnings
import pickle

from pyoed import utility
from pyoed.models.simulation_models import (
    TimeDependentModel,
    TimeIndependentModel,
)
from pyoed.configs import (
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
)
from pyoed.oed.core import (
    BayesianInversionCriterion,
    BayesianInversionCriterionConfigs,
)


########################################################################################
# regex conveniences
########################################################################################

class EvaluationMethod(StrEnum):
    NMC = r"\Anmc\Z"


@dataclass(kw_only=True, slots=True)
class BayesianInversionEIGConfigs(BayesianInversionCriterionConfigs):
    """
    Configuration class for Bayesian EIG criterion provided by
    :py:class:`BayesianInversionEIG`.
    This class provides access to all attributes defined by
    :py:class:`BayesianInversionCriterionConfigs` in addition to the following
    attributes/configurations.

    :param evaluation_method: name ofthe method to use for computing the criterion
        and its gradients. Supported are:

        - :code:`"nmc"`: Estimate the criterion using a nested Monte Carlo
            scheme.

    :param inner_sample_size: number of sample points for the inner loop of the
        double loop Monte-Carlo (MC) estimator.
    :param outer_sample_size: number of sample points for the outer loop of the
        double loop Monte-Carlo (MC) estimator.
    :param adaptive_contrastive_estimation: whether to apply adaptive contrastive
        estimation or not.
    :param laplace_importance_sampling: whether to carry out Laplace importance
        sampling for the estimate or not.
    :param random_seed: The random seed to use for randomized algorithms.
    """
    name: str = "EIG: Expected Information Gain utility function for Bayesian inverse problems"
    evaluation_method: str = "nmc"
    inner_sample_size: int = 256
    outer_sample_size: int = 256
    adaptive_contrastive_estimation: bool = False
    laplace_importance_sampling: bool = False
    random_seed: None | int = None


@set_configurations(BayesianInversionEIGConfigs)
class BayesianInversionEIG(BayesianInversionCriterion):
    """
    An implementation of the Expected Information Gain (EIG) criterion for Bayesian
    Optimal Design of Experiments.  The criterion is defined as the expectation of the
    KL divergence over the data, i.e.

    .. math::
        \\mathcal{U}(\\zeta)
        = \\int_\\mathcal{Y} p(\\mathbf{y} | \\zeta)
        \\left(
            \\int_\\Theta
            p(\\theta | \\mathbf{y}, \\zeta)
            \\log \\frac{ p(\\theta | \\mathbf{y}, \\zeta) }{p(\\theta)}
            d\\theta
        \\right)
        d\\mathbf{y}

    where :math:`\\zeta` is the design, :math:`\\mathbf{y}` is the data with associated
    space :math:`\\mathcal{Y}`, and :math:`\\theta` is the inference parameter with
    associated space :math:`\\Theta`. Likewise, :math:`p(\\theta)` is the prior density,
    :math:`p(\\theta | \\mathbf{y}, \\zeta)` is the posterior, and :math:`p(\\mathbf{y}
    | \\zeta)` is the marginal density of the observed data, i.e. :math:`p(\\mathbf{y}
    | \\zeta) = \\mathbb{E}_{\\theta \\sim p(\\theta)} [p(\\mathbf{y} | \\theta,
    \\zeta)]`.

    .. warning::
        Currently `laplace_importance_sampling` does not make any difference. It's effect will
        be implemented.
    """
    def __init__(self, configs: dict | BayesianInversionEIGConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Make sure the evaluation method is named properly
        self.update_evaluation_method(self.configurations.evaluation_method)

    def update_evaluation_method(self, evaluation_method):
        """
        Update the evaluation method.
        """
        if not isinstance(evaluation_method, str):
            raise PyOEDConfigsValidationError(
                f"Invlid {type(evaluation_metho)=}; expected string"
            )
        if re.match(EvaluationMethod.NMC, evaluation_method, re.IGNORECASE):
            self.configurations.evaluation_method = "nmc"

        else:
            raise PyOEDConfigsValidationError(
                f"Invlid {evaluation_method=}; expected one of " + str([v.strip(r'\A \Z') for v in EvaluationMethod])
            )

    def validate_configurations(
        self,
        configs: dict | BayesianInversionEIGConfigs,
        raise_for_invalid: bool = True,
    ):
        """
        A method to check the passed configurations and make sure they are conformable
        with each other, and with current configurations (or default of not set)

        :param configs: an object holding key/value configurations
        :param raise_for_invalid: if `True` raise :py:class:`TypeError`
            for invalid configrations key

        :returns: True/False flag indicating whether passed configurations is valid or
            not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        # Local tests
        is_bool = lambda x: utility.isnumber(x) and (x == bool(x))
        is_int = lambda v: utility.isnumber(v) and int(v) == v
        is_positive_int = lambda v: is_int(v) and v > 0
        is_positive_int_or_None = lambda v: v is None or is_positive_int(v)

        def is_valid_method(method):
            if not isinstance(method, str):
                return False
            else:
                valid = False
                for p in EvaluationMethod:
                    if re.match(p, method, re.IGNORECASE):
                        valid = True
                        break
                return valid

        # `evaluation_method`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="evaluation_method",
            test=is_valid_method,
            message="Invalid evaluation_method",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # `random_seed`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=is_positive_int_or_None,
            message="Expected random_seed to be positive integer or None.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Samples sizes (positive integers)
        for key in ["inner_sample_size", "outer_sample_size", ]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=is_positive_int,
                message=f"Expected '{key}' flag to be positive integer.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # Adaptive flags (bool)
        for key in ["adaptive_contrastive_estimation", "laplace_importance_sampling", ]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=is_bool,
                message=f"Expected '{key}' flag to be bool.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        # Validate parent's configurations
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs) -> None:
        """
        Update one or more configurations given their name and value. Each configuration
        key/value is validated against current values (more precedence to passed, then
        current settings).

        If all passed configurations are conformable with each other, and with current
        configurations, the passed configurations updated the underlying configurations
        dictionary.
        """
        # Validate kwargs and update keys belonging to parents.
        super().update_configurations(**kwargs)

        if "evaluation_method" in kwargs:
            self.update_evaluation_method(kwargs["evaluation_method"])

        if "adaptive_contrastive_estimation" in kwargs:
            self.configurations.adaptive_contrastive_estimation = bool(kwargs["adaptive_contrastive_estimation"])

        if "laplace_importance_sampling" in kwargs:
            self.configurations.laplace_importance_sampling = bool(kwargs["laplace_importance_sampling"])

    def evaluate(self, design) -> float:
        """
        Evaluate the EIG for the given OED problem at the given design.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.

        .. note::
            If you are intending to compare the EIG against different designs, it is
            recommended to set the random seed to a fixed value to ensure consistency
            amongst the internal sampling.

        :returns: the EIG value at the given design.
        """
        ## Unpack configurations
        # OED problem and inverse problem
        oed_problem = self.oed_problem
        ip = oed_problem.inverse_problem

        ## Get current design, and set the passed one in the inverse problem
        current_design = oed_problem.design
        oed_problem.design = design

        ## Evaluate the EIG estimate
        if re.match(r"\A(nmc)\Z", self.evaluation_method, re.IGNORECASE):
            # Update random seeds of the prior and the observation noise models
            prior = ip.prior
            noise = ip.observation_error_model

            if self.random_seed is not None:
                prior.update_random_number_generator(self.random_seed)
                noise.update_random_number_generator(self.random_seed)

            else:
                warnings.warn(
                    "Random seed is not set. These results may not be reproducible!"
                )

            if isinstance(ip.model, TimeDependentModel):
                ## Evaluate for Time-dependent models

                def data_sampler(theta):
                    """Define a data generation process (map parameter to observations)"""
                    Ftheta, _ = ip.apply_forward_operator(
                        theta,
                        save_states=False,
                        checkpoints=None,
                        scale_by_noise=False,
                    )
                    # Perturb by noise (additive)
                    for t in Ftheta:
                        Ftheta[t] += noise.sample()
                    return Ftheta

                def likelihood(y, theta):
                    "Wrapper funciton to calculate observation error model likelihood over all time points"
                    Ftheta, _ = ip.apply_forward_operator(
                        theta,
                        save_states=False,
                        checkpoints=None,
                        scale_by_noise=False,
                    )
                    prob = 0.0
                    for t in Ftheta:
                        prob += noise.pdf(
                            Ftheta[t] - y[t],
                            normalize=False,
                            log=True,
                        )
                    prob = np.exp(prob)
                    return prob

            elif isinstance(ip.model, TimeIndependentModel):
                ## Evaluate for Time-independent models

                def data_sampler(theta):
                    """Define a data generation process (map parameter to observations)"""
                    Ftheta = ip.apply_forward_operator(theta, scale_by_noise=False)
                    return Ftheta + noise.sample()

                def likelihood(y, theta):
                    "Wrapper funciton to calculate observation error model likelihood"
                    Ftheta = ip.apply_forward_operator(theta, scale_by_noise=False)
                    return noise.pdf(Ftheta - y, normalize=False, log=False)

            else:
                raise TypeError(
                    "Model must be either TimeDependentModel or TimeIndependentModel!"
                    f" Received {type(ip.model)} instead."
                )

            obj_val = self.evaluate_nmc_estimator(
                prior_sampler=prior.sample,
                data_sampler=data_sampler,
                likelihood=likelihood,
                outer_sample_size=self.outer_sample_size,
                inner_sample_size=self.inner_sample_size,
                adaptive_contrastive_estimation=self.adaptive_contrastive_estimation,
            )

        else:
            raise PyOEDConfigsValidationError(
                f"Unsupported evaluation method '{self.evaluation_method}'\nThis should"
                " never happen if proper validation is carried out on the evaluation"
                " method!\n"
                "***PLEASE REPORT THIS AS A BUG***"
            )

        ## Reset the design
        oed_problem.design = current_design

        return obj_val

    def evaluate_nmc_estimator(
        self,
        prior_sampler,
        data_sampler,
        likelihood,
        outer_sample_size=BayesianInversionEIGConfigs.outer_sample_size,
        inner_sample_size=BayesianInversionEIGConfigs.inner_sample_size,
        adaptive_contrastive_estimation=BayesianInversionEIGConfigs.adaptive_contrastive_estimation,
    ) -> float:
        """
        Estimate the EIG using a nested Monte Carlo scheme. It is given by

        .. math::
            \\hat{\\mathcal{U}}_{\\rm NMC}(\\zeta)
            = \\frac{1}{N} \\sum_{i=1}^N \\left[
                \\log(p(\\mathbf{y}_n | \\theta_{n,0}, \\zeta))
                - \\log \\left(
                    \\frac{1}{M} \\sum_{m=1}^M p(\\mathbf{y}_n | \\theta_{n,m}, \\zeta)
                \\right)
            \\right]

        where :math:`\\theta_{n,m} \\sim p(\\theta)` and :math:`\\mathbf{y}_n \\sim
        p(\\mathbf{y} | \\theta_{n,0}, \\zeta)`.
        The parameters :math:`outer_sample_size` and :math:`inner_sample_size`
        are set by the based on corresponding configurations  attribute.
        If wish to change them,  refer to
        Rainforth et. al `[1]`_ for additional reading, but briefly speaking,
        it is recommended to select :math:`outer_sample_size` and
        :math:`inner_sample_size` such that
        :math:`outer_sample_size \\approx inner_sample_size^2`.

        .. _[1]: https://proceedings.mlr.press/v80/rainforth18a.html

        :param callable prior_sampler: a function that returns a sample from the prior.
        :param callable data_sampler: a function that returns a sample from the data
            given a parameter sample.
        :param callable likelihood: a function that returns the likelihood of a data
            sample given a parameter sample.

        .. note::
            Note, **assuming likelihood is normally distributed**, the normalization
            factor cancels out in the above expression. Hence, one can save
            computational resources by not normalizing the likelihood.

        :param int outer_sample_size: the number of outer samples.
        :param int inner_sample_size: the number of inner samples.
        :param bool adaptive_contrastive_estimation: whether to use the Adaptive
            contrastive Estimation or not. This manifests as using :math:`\\theta_{n,0}`
            inside the inner Monte Carlo. This can be useful if the prior is not a good
            approximation of the posterior, as this may manifest as many prior samples
            missing the 'support' of the likelihood, resulting in infinities in the
            log-likelihood.  Note, this changes NMC into a lower bound of the EIG.
            Refer to Foster et. al `[2]`_ for additional reading. Default is `True`.

        .. _[2]: https://arxiv.org/abs/1911.00294

        :returns: An estimation of the EIG.
        """
        # Assertion on input
        is_int = lambda v: utility.isnumber(v) and int(v) == v
        is_positive_int = lambda v: is_int(v) and v > 0
        for v in [outer_sample_size, inner_sample_size]:
            assert is_positive_int(v), f"expected sample size to be positive integer; received {v}"

        def p_y_MC(y):
            """
            Mean likelihood (non-normalized) over all observations conditioned by the
            prior samples.
            """
            return np.mean([likelihood(y, prior_sampler()) for _ in range(inner_sample_size)])

        def p_y_ACE(y, l_0):
            """
            Same as p_y_MC, but with additionally using the likelihood of the prior
            sample used to generate y. Hence, it must be the case that the y and l_0
            are generated by the same prior sample, i.e.

            .. code::
                theta = prior_sampler()
                y = data_sampler(theta)
                l_0 = likelihood(y, theta)
            """
            v = [likelihood(y, prior_sampler()) for _ in range(inner_sample_size)]
            v.append(l_0)
            return np.mean(v)

        EIG_vals = np.zeros(outer_sample_size)
        for n in range(outer_sample_size):
            theta = prior_sampler()
            y = data_sampler(theta)
            l = likelihood(y, theta)
            p_y = p_y_ACE(y, l) if adaptive_contrastive_estimation else p_y_MC(y)
            EIG_vals[n] = np.log(l) - np.log(p_y)
        return np.mean(EIG_vals[np.isfinite(EIG_vals)])

    @property
    def evaluation_method(self):
        """
        The method used for computing the criterion value and its gradients. Two
        options:

        - 'nmc': Nested monte carlo estimation
        """
        return self.configurations.evaluation_method
    @evaluation_method.setter
    def evaluation_method(self, val):
        self.update_configurations(evaluation_method=val)

    @property
    def inner_sample_size(self):
        """Return the sample size used in the inner loop of the double loop MC estimate."""
        return self.configurations.inner_sample_size
    @inner_sample_size.setter
    def inner_sample_size(self, val):
        self.update_configurations(inner_sample_size=val)

    @property
    def outer_sample_size(self):
        """Return the sample size used in the outer loop of the double loop MC estimate."""
        return self.configurations.outer_sample_size
    @outer_sample_size.setter
    def outer_sample_size(self, val):
        self.update_configurations(outer_sample_size=val)

    @property
    def adaptive_contrastive_estimation(self):
        """Return the flag indicating whether to use adaptive contrastive estimation"""
        return self.configurations.adaptive_contrastive_estimation
    @adaptive_contrastive_estimation.setter
    def adaptive_contrastive_estimation(self, val):
        self.update_configurations(adaptive_contrastive_estimation=val)

    @property
    def laplace_importance_sampling(self):
        """Return the flag indicating whether to use laplace importance sampling"""
        return self.configurations.laplace_importance_sampling
    @laplace_importance_sampling.setter
    def laplace_importance_sampling(self, val):
        self.update_configurations(laplace_importance_sampling=val)

    @property
    def random_seed(self):
        """Return the random seed used for randomized algorithms"""
        return self.configurations.random_seed
    @random_seed.setter
    def random_seed(self, val):
        self.update_configurations(random_seed=val)


