# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Module providing implementations of the Bayesian D-optimality
criterion (utility function) for Bayesian inverse problems.
"""

from dataclasses import dataclass, field
import re
from functools import partial
from typing import Sequence, Union

from pyoed import utility
from pyoed.configs import (
    validate_key,
    set_configurations,
)
from ..a_opt import (
    EvaluationMethod,
    BayesianInversionAOptConfigs,
    BayesianInversionAOpt,
)


@dataclass(kw_only=True, slots=True)
class BayesianInversionDOptConfigs(BayesianInversionAOptConfigs):
    """
    Configuration class for Bayesian D-optimality criterion (utility functions).

    This mirrors the configurations in :py:class:`BayesianInversionAOptConfigs` with
    two additional configurations used for estimating (when `method` is `randomized`)
    the log-determinant of the matrix. These configurations are passed
    to the utility function `utility.math.matrix_logdet`:

    :param sigma_bounds: bounds on the singular values of the matrix.
        Default is `[None, None]`, which means that the singular values are estimated.
        THIS IS VERY COSTLY!
    :param n_deg: number of terms to use in Chebyshev expansion.
    """
    sigma_bounds: [Union[None, float], Union[None, float]] = field(
        default_factory=lambda: [None, None]
    )
    n_deg: int = 14


@set_configurations(BayesianInversionDOptConfigs)
class BayesianInversionDOpt(BayesianInversionAOpt):
    """
    General class implementing common functionality for Bayesian D optimality
    criterion (utility functions).

    Bayesian D-optimality is defined as the log-det of the Fisher information
    matrix (FIM) (the utility function to be maximized) or the posterior
    covariance (optimality criterion to be minimized) assuming a Gaussian
    posterior (or a Laplacian/Gaussian approximation of the posterior,
    usually obtained by linearization of the forward (simulation + observation)
    around an estimate of the ground truth).

    .. math::
        \\mathcal{\\Phi}_{\\rm A}(\\xi) = \\text{log det}(\\mathbf{C}_\\text{post}),
        \\text{ or  }
        \\mathcal{\\Phi}_{\\rm A}(\\xi) = \\text{log det}(\\mathbf{C}^{-1}_\\text{post})

    For a linear/linearized problem, the FIM is equal to the inverse of the posterior
    covariance.  The choise between the two formulation (FIM vs. posterior covariance)
    is decided based on the appropriate configurations parameter

    For a nonlinear inverse problem (non-linear solution operator/model and/or
    observation operator) the Hessian (inverse of the covariance operator) needs to be
    evaluated based on a linearization (tangent linear model) of the solution and the
    observation operator at the truth (or an approximation thereof) where the MAP
    estimate is used. Thus, the MAP estimate must be be available for nonlinear inverse
    problem.

    :param configs: an object holding configurations.
    """

    def __init__(self, configs: dict | BayesianInversionDOptConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)


    def validate_configurations(
        self,
        configs: dict | BayesianInversionDOptConfigs,
        raise_for_invalid: bool = True,
    ):
        """
        A method to check the passed configurations and make sure they are conformable
        with each other, and with current configurations (or default of not set)

        :param configs: an object holding key/value configurations
        :param raise_for_invalid: if `True` raise :py:class:`TypeError`
            for invalid configrations key

        :returns: True/False flag indicating whether passed configurations is valid or
            not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        # Local validators/functions
        is_int = lambda v: utility.isnumber(v) and int(v) == v
        is_positive_int = lambda v: is_int(v) and v > 0

        def is_valid_bounds(bounds):
            if not utility.isiterable(bounds):
                return False
            elif len(bounds) != 2:
                return False
            elif not (all([v is None or utility.isnumber(v) for v in bounds])):
                return False
            else:
                return True

        # `sigma_bounds` sequence (e.g., list) of two entries (each is float or None)
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="sigma_bounds",
            test=is_valid_bounds,
            message="Expected sigma_bounds to be iterable with two entris (float | None).",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `n_deg` Non negative float
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="n_deg",
            test=is_positive_int,
            message="Expected n_deg to positive integer.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Call parent for further validation
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def evaluate(self, design) -> float:
        """
        Evaluate the D-optimal criterion for the given OED problem at the given design.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.

        :return: D-optimal design criterion value.
        :rtype: float
        """

        # Define a helper function for verbose printing
        def vprint(x):
            if self.verbose: print(x)

        # Get proper references to objects
        oed_problem = self.oed_problem
        ip = oed_problem.inverse_problem

        # Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design

        # Get the linearization point (if needed)
        lp = self.check_linearization(design)

        # Define the evaluation function (log-det of the Hessian) linearized around lp
        if self.use_FIM:
            eval_func = lambda x: ip.Hessian_matvec(x, eval_at=lp)
        else:
            eval_func = lambda x: ip.Hessian_inv_matvec(x, eval_at=lp)

        # Create function that wraps the matrix log-det calculator
        U = partial(
            utility.matrix_logdet,
            method=self.evaluation_method,
            size=ip.prior.size,
            sample_size=self.sample_size,
            optimize_sample_size=self.optimize_sample_size,
            min_sample_size=self.min_sample_size,
            max_sample_size=self.max_sample_size,
            distribution=self.distribution,
            randomization_vectors=self.randomization_vectors,
            random_seed=self.random_seed,
            accuracy=self.accuracy,
        )
        if re.match(EvaluationMethod.RANDOMIZED, self.evaluation_method, re.IGNORECASE):
            if self.randomization_vectors is None:
                vprint("Evaluating obj and constructing randomization vectors (once)")
                obj_val, r_vectors = U(
                    eval_func,
                    return_randomization_vectors=True,
                )
                self.randomization_vectors = r_vectors
                vprint("Randomization vectors constructed")
            else:
                vprint("Randomization vectors found; evaluating obj")
                obj_val = U(eval_func)
        else:
            vprint("Evaluating exact obj")
            obj_val = U(eval_func)

        # Reset the design
        oed_problem.design = current_design
        return obj_val

    @property
    def n_deg(self):
        """n_deg in configurations"""
        return self.configurations.n_deg
    @n_deg.setter
    def n_deg(self, value):
        self.update_configurations(n_deg=value)

    @property
    def sigma_bounds(self):
        """sigma_bounds in configurations"""
        return self.configurations.sigma_bounds
    @sigma_bounds.setter
    def sigma_bounds(self, value):
        self.update_configurations(sigma_bounds=value)

