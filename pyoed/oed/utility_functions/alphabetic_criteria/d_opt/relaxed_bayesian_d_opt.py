# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from dataclasses import dataclass
import re
import numpy as np
from enum import StrEnum

from pyoed import utility
from pyoed.configs import (
    validate_key,
    set_configurations,
)
from pyoed.assimilation.assimilation_utils import (
    # apply_TLM_operator,
    apply_adjoint_operator,
)
from pyoed.assimilation.filtering import VariationalFilter
from pyoed.assimilation.smoothing import VariationalSmoother
from pyoed.models.error_models.Gaussian import (
    GaussianErrorModelConfigs,
    GaussianErrorModel,
    PointwiseWeightedGaussianErrorModelConfigs,
    PointwiseWeightedGaussianErrorModel,
    PrePostWeightedGaussianErrorModelConfigs,
    PrePostWeightedGaussianErrorModel,
    isGaussian,
)
from pyoed.stats.stats_utils.pcw_functions import (
    PCWFunction,
    get_pcw_function,
)
from .bayesian_d_opt import (
    BayesianInversionDOpt,
    BayesianInversionDOptConfigs,
)

from ..a_opt.relaxed_bayesian_a_opt import (
    FORMULATION_APPROACH,
    _validate_and_return_formulation_approach,
)


@dataclass(kw_only=True, slots=True)
class RelaxedBayesianInversionDOptConfigs(BayesianInversionDOptConfigs):
    """
    Configuration class for :py:class:`RelaxedBayesianInversionDOpt` criterion.
    See :py:class:`RelaxedBayesianInversionDOpt` for D-Opt specific configurations and
    :py:class:`RelaxedBayesianInversionCriterionConfigs` for those specific to sensor placement.
    """

    formulation_approach: str = FORMULATION_APPROACH.POINTWISE_COVARIANCE_MULTIPLICATION
    pcw_scheme: str | PCWFunction = "covariance-product"


@set_configurations(RelaxedBayesianInversionDOptConfigs)
class RelaxedBayesianInversionDOpt(BayesianInversionDOpt):
    """
    An implementation of the D-optimal design criterion for Bayesian Optimal Design of
    Experiments for Sensor Placement. This implementation extends the Bayesian
    D-optimality criterion functionality provided by
    :py:class:`pyoed.oed.utility_functions.alphabetic_criteria.BayesianInversionDOpt` by
    providing specific implementations that depend on the way the design enters the
    enters the inverse problem. Specifically, here we assume the design is used for
    sensor placement.

    Thus a design/control variable is a binary vector with each entry associated with a
    candidate sensor location with value 1 meaning on (activate the sensor) and zero
    meaning off (deactivate the sensor). In other words, in sensor placement, a binary
    (on/off) design is associated with the observation vector (or equivalently with the
    observation noise (error covariance) matrix).

    In the OED literature, some (relaxed/approximate) formulations apply the design to
    the precision matrix. The exact form for of relaxation affects the way the utility
    function is evaluated and is potentially differentiated, Thus, this implementation
    supports multiple approaches for formulating the OED utility function for sensor
    placement based on how the design is applied to the inverse problem. The design
    (binary indexes associated with candidate sensors) constructs a diagonal design
    matrix :math:`\\mathbf{D}` which changes the inverse problem as follows based on the
    formulation approach dictated by the `formulation_approach` configuration parameter
    with acceptable values: 'pre-post-covariance-multiplication',
    'pre-post-precision-multiplication', and 'pointwise-covariance-multiplication'.

    :param configs: an object containing configuarations for the criterion, see
        :py:class:`RelaxedBayesianInversionDOptConfigs` for more details.
    """

    def __init__(
        self, configs: dict | RelaxedBayesianInversionDOptConfigs | None = None
    ):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # todo: we need to make sure the formulation approach takes place...
        self.configurations.formulation_approach = _validate_and_return_formulation_approach(
            self.configurations.formulation_approach
        )

        pcw_scheme = self.configurations.pcw_scheme
        if isinstance(pcw_scheme, str):
            pcw_scheme = get_pcw_function(pcw_scheme)
        self.configurations.pcw_scheme = pcw_scheme


    def validate_configurations(
        self,
        configs: dict | RelaxedBayesianInversionDOptConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        aggregated_configs = self.aggregate_configurations(configs)

        # Local validators/functions
        is_int = lambda v: utility.isnumber(v) and int(v) == v
        is_positive_int = lambda v: is_int(v) and v > 0

        def test_pcw_scheme(x):
            if isinstance(x, str):
                return get_pcw_function(x) is not None
            elif isinstance(x, PCWFunction):
                return True
            return False

        def is_valid_bounds(bounds):
            if not utility.isiterable(bounds):
                return False
            elif len(bounds) != 2:
                return False
            elif not (all([v is None or utility.isnumber(v) for v in bounds])):
                return False
            else:
                return True

        # `sigma_bounds` sequence (e.g., list) of two entries (each is float or None)
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="sigma_bounds",
            test=is_valid_bounds,
            message="Expected sigma_bounds to be iterable with two entris (float | None).",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `n_deg` Non negative float
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="n_deg",
            test=is_positive_int,
            message="Expected n_deg to positive integer.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # The error mode in `oed_problem`
        if not validate_key(
            aggregated_configs,
            configs,
            "oed_problem",
            test=lambda x: isGaussian(x.inverse_problem.observation_error_model),
            message="Sensor placement problems require Gaussian noise models!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `formulation_approach`
        if not validate_key(
            aggregated_configs,
            configs,
            "formulation_approach",
            test=lambda x: isinstance(x, str),
            message="'formulation_approach' must be a string!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False
        if not validate_key(
            aggregated_configs,
            configs,
            "formulation_approach",
            test=lambda x: _validate_and_return_formulation_approach(x) is not None,
            message="'formulation_approach' was not recognized!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `pcw_scheme`
        if not validate_key(
            aggregated_configs,
            configs,
            "pcw_scheme",
            test=lambda x: isinstance(x, (str, PCWFunction)),
            message="Expected 'pcw_scheme' to be a string or a PCWFunction!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False
        if not validate_key(
            aggregated_configs,
            configs,
            "pcw_scheme",
            test=test_pcw_scheme,
            message="Passed pcw_scheme does not correspond to a valid PCW Function!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Update the configurations of the object.

        :param kwargs: configurations to be updated.
        """
        super().update_configurations(**kwargs)

        if "formulation_approach" in kwargs:
            self.register_formulation_approach(kwargs["formulation_approach"])

        if "pcw_scheme" in kwargs:
            pcw_scheme = kwargs["pcw_scheme"]
            if isinstance(pcw_scheme, str):
                pcw_scheme = get_pcw_function(pcw_scheme)
            self.configurations.pcw_scheme = pcw_scheme

    def register_formulation_approach(self, approach):
        self.validate_configurations({"formulation_approach": approach})

        ip = self.oed_problem.inverse_problem
        noise_model = ip.observation_error_model

        formulation_approach = _validate_and_return_formulation_approach(approach)
        self.configurations.formulation_approach = formulation_approach
        # TODO: Maybe a bad idea to update the noise model here.
        # Maybe this should be done in the OED problem class.
        if (
            formulation_approach
            == FORMULATION_APPROACH.POINTWISE_COVARIANCE_MULTIPLICATION
        ):
            if not isinstance(noise_model, PointwiseWeightedGaussianErrorModel):
                # Create a new noise model from existing one's statistics
                # TODO: Refactor the pcw_scheme to be a PCWFunction in the
                # GaussianErrorModel, but there's some discrepancy in the the error
                # model implementation so have to hold off temporarily.
                new_noise_configs = PointwiseWeightedGaussianErrorModelConfigs(
                    mean=noise_model.mean,
                    variance=noise_model.unweighted_covariance_matrix(),
                    random_seed=noise_model.configurations.random_seed,
                    design_weighting_scheme=FORMULATION_APPROACH.POINTWISE_COVARIANCE_MULTIPLICATION,
                    pointwise_weighting_kernel=self.pointwise_weighting_kernel,
                )
                new_noise_model = PointwiseWeightedGaussianErrorModel(new_noise_configs)
                ip.register_observation_error_model(new_noise_model)
            else:
                # Update existing model
                ip.observation_error_model.design_weighting_scheme = (
                    FORMULATION_APPROACH.POINTWISE_COVARIANCE_MULTIPLICATION
                )
                ip.observation_error_model.pointwise_weighting_kernel = self.pointwise_weighting_kernel
        elif (
            formulation_approach
            == FORMULATION_APPROACH.PRE_POST_COVARIANCE_MULTIPLICATION
        ):
            if not isinstance(noise_model, GaussianErrorModel):
                # Create a new noise model from existing one's statistics
                new_noise_configs = GaussianErrorModelConfigs(
                    mean=noise_model.mean,
                    variance=noise_model.unweighted_covariance_matrix(),
                    random_seed=noise_model.configurations.random_seed,
                )
                new_noise_model = GaussianErrorModel(new_noise_configs)
                ip.register_observation_error_model(new_noise_model)
        elif (
            formulation_approach
            == FORMULATION_APPROACH.PRE_POST_PRECISION_MULTIPLICATION
        ):
            if not isinstance(noise_model, PrePostWeightedGaussianErrorModel):
                # Create a new noise model from existing one's statistics
                new_noise_configs = PrePostWeightedGaussianErrorModelConfigs(
                    mean=noise_model.mean,
                    variance=noise_model.unweighted_covariance_matrix(),
                    random_seed=noise_model.configurations.random_seed,
                )
                new_noise_model = PrePostWeightedGaussianErrorModel(new_noise_configs)
                ip.register_observation_error_model(new_noise_model)

    def grad_design(self, design):
        """
        Derivative of the A Optimal design criterion with respect to the design assuming
        a relaxed (non-binary) design, and based on the registered formulation apprach.

        :param design: an observational design vector conformable with the
            observation operator and the observation error covariance matrix
            operator. For the purposes of this method, it is intended to be a
            relaxed design (over the interval [0, 1]),

        :returns: gradient w.r.t the relaxed design
        """
        # Pointwise-weighted covariance matrix

        approach = self.formulation_approach

        if re.match(
            r"\Apointwise( |_|-)*covariance( |_|-)*multiplication\Z",
            approach,
            re.IGNORECASE,
        ):
            return self._pointwise_weighted_covariance_grad_design(
                design=design,
            )

        elif re.match(
            r"\Apre( |_|-)*post( |_|-)*covariance( |_|-)*multiplication\Z",
            approach,
            re.IGNORECASE,
        ):
            return self._prepost_weighted_covariance_grad_design(
                design=design,
            )

        elif re.match(
            r"\Apre( |_|-)*post( |_|-)*precision( |_|-)*multiplication\Z",
            approach,
            re.IGNORECASE,
        ):
            return self._prepost_weighted_precision_grad_design(
                design=design,
            )

        else:
            raise ValueError(
                f"Unsupported OED for sensor placement formulation approach '{approach}'!"
            )

    def _pointwise_weighted_covariance_grad_design(self, design):
        """
        Derivative of the A Optimal design criterion with respect to the design
        assuming a pointwise-produce covariance weighting of the observation error model.

        :param design: an observational design vector conformable with the
            observation operator and the observation error covariance matrix
            operator. For the purposes of this method, it is intended to be a
            relaxed design (over the interval [0, 1]),

        :returns: gradient w.r.t the relaxed design
        """
        # TODO: This is not supposed to be limited to smoothers. Need to update this!

        # Unpack and validate the OED problem
        oed_problem = self.oed_problem

        # Unpack and validate the inverse problem
        ip = oed_problem.inverse_problem
        if not isinstance(ip, (VariationalFilter, VariationalSmoother)):
            raise NotImplementedError(
                f"Only VariationalFilter/VariationalSmoother inverse problems are acceptable "
                "for inverse problems this time.\n"
                f"Received '{type(ip)}' which is not yet supported"
            )

        # Get the pointwise weighting kernel
        # Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design

        active = np.where(design)[0]

        # MARK: TIME-DEPENDENT
        if isinstance(ip, VariationalSmoother):
            obs_tspan = ip.observation_times
        elif isinstance(ip, VariationalFilter):
            # Creating fake time instances for consistency here (and to utilize smoother code with loop)
            obs_tspan = [None]
        else:
            raise ValueError(
                "This is not possible!; "
                "Expected only either VariationalFilter/VariationalSmoother instance\n"
                "Please report this as a bug!"
            )

        # Get the linearization point (if needed)
        lp = self.update_linearization(design)
        R = utility.asarray(ip.observation_error_model.unweighted_covariance_matrix())

        # Evaluate the gradient
        grad = np.zeros(ip.observation_error_model.size)

        if re.match(r"\Arandom(ized|)\Z", self.evaluation_method, re.IGNORECASE):
            raise NotImplementedError(
                "TODO: Design relaxation for randomized log-det does have a design"
                " gradient implemented yet."
            )

        else:

            # MARK: Time-DEPENDENT
            for t in obs_tspan:

                # Construct parameter to observable map at this time (maybe not the most efficient!)
                F_adj = utility.asarray(
                    lambda obs: apply_adjoint_operator(
                        ip=self.oed_problem.inverse_problem,
                        obs=obs,
                        obs_time=t,
                        eval_at=lp,
                        save_all=False,
                        scale_by_noise=True,
                    ),
                    shape=(ip.prior.size, ip.observation_operator.shape[0]),
                )

                if active.size != 0:
                    for i in range(grad.size):
                        # Weight (pointwise multiply) the unweighted covariance matrix
                        # by the derivative of the weighting function
                        if i not in active:
                            continue

                        # Vector of derivatives
                        eta = np.array(
                            [
                                self.pointwise_weighting_kernel_grad(
                                    design,
                                    i,
                                    j,
                                    j
                                ) for j in range(design.size)
                            ]
                        )
                        eta[i] *= 0.5
                        Wprime_col = eta[active] * utility.asarray(R[active, i])

                        # Apply model adjoint (backward over all observation times till
                        # t0) to in_vec with precision applied (weight by noise)
                        # Then apply forward model again (with precision applied);
                        # extract contribution from checked observations
                        adj = F_adj.dot(Wprime_col)

                        if not self.use_FIM:
                            # Insert Hessian inverse matvec twice here
                            adj = ip.Hessian_inv_matvec(
                                adj,
                                lp,
                            )

                        y = F_adj.T.dot(adj)
                        contrib = y[np.where(active == i)[0]]

                        # Update gradient entry
                        grad[i] += contrib

            # Multiply by 2 and flip the sign if the objective is Trace(FIM)
            scl = -2 if self.use_FIM else 2
            grad *= scl

        # Reset the design
        oed_problem.design = current_design

        return grad

    def _prepost_weighted_precision_grad_design(self, design):
        """
        Derivative of the A Optimal design criterion with respect to the design
        assuming a pre- and post- multiplication of the of the precision matrix of the
        observation error model.

        :param design: an observational design vector conformable with the
            observation operator and the observation error covariance matrix
            operator. For the purposes of this method, it is intended to be a
            relaxed design (over the interval [0, 1]),

        :returns: gradient w.r.t the relaxed design
        """
        # Unpack and validate the OED problem
        oed_problem = self.oed_problem

        # Unpack and validate the inverse problem
        ip = oed_problem.inverse_problem
        if not isinstance(ip, (VariationalFilter, VariationalSmoother)):
            raise NotImplementedError(
                f"Only VariationalFilter/VariationalSmoother inverse problems are acceptable "
                "for inverse problems this time.\n"
                f"Received '{type(ip)=}' which is not yet supported"
            )

        # Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design

        active = np.where(design)[0]

        if isinstance(ip, VariationalSmoother):
            obs_tspan = ip.observation_times
        elif isinstance(ip, VariationalFilter):
            # Creating fake time instances for consistency here (and to utilize smoother code with loop)
            obs_tspan = [None]
        else:
            raise ValueError(
                "This is not possible!; "
                "Expected only either VariationalFilter/VariationalSmoother instance\n"
                "Please report this as a bug!"
            )

        # Get the linearization point (if needed)
        lp = self.update_linearization(design)

        # Unscaled observation noise matrix (Rinv) will be re-used multiple times
        Rinv = np.linalg.inv(
            utility.asarray(ip.observation_error_model.unweighted_covariance_matrix())
        )

        # Evaluate the gradient
        grad = np.zeros(ip.observation_error_model.size)

        if re.match(r"\Arandom(ized|)\Z", self.evaluation_method, re.IGNORECASE):
            raise NotImplementedError(
                "TODO: Design gradient for randomized D-optimality with pre-post "
                "precision product is not yet implemented"
            )

        else:

            for t in obs_tspan:

                # Construct parameter to observable map at this time (maybe not the most efficient!)
                F_adj = utility.asarray(
                    lambda obs: apply_adjoint_operator(
                        ip=self.oed_problem.inverse_problem,
                        obs=obs,
                        obs_time=t,
                        eval_at=lp,
                        save_all=False,
                        scale_by_noise=False,
                    ),
                    shape=(ip.prior.size, ip.observation_operator.shape[0]),
                )

                for i in range(grad.size):
                    active = np.where(design)[0]
                    if active.size == 0 or i not in active:
                        continue

                    # Apply model adjoint (backward over all observation times till
                    # t0) to in_vec with precision
                    # Then apply forward model again (with precision applied);
                    # extract contribution from checked observations
                    adj = F_adj.dot(design[active] * Rinv[active, i])

                    if not self.use_FIM:
                        # Insert Hessian inverse matvec twice here
                        adj = ip.Hessian_inv_matvec(
                            adj,
                            lp,
                        )

                    y = F_adj.T.dot(adj)
                    contrib = y[np.where(active == i)[0]]

                    # Update gradient entry
                    grad[i] += contrib

            # Multiply by 2 and flip the sign if the objective is Trace(FIM)
            scl = 2 if self.use_FIM else -2
            grad *= scl

        # Reset the design
        oed_problem.design = current_design

        return grad

    def _prepost_weighted_covariance_grad_design(self, design):
        """
        Derivative of the A Optimal design criterion with respect to the design
        assuming a pre- and post- multiplication of the of the covariance matrix of the
        observation error model.

        :param design: an observational design vector conformable with the
            observation operator and the observation error covariance matrix
            operator. For the purposes of this method, it is intended to be a
            relaxed design (over the interval [0, 1]),

        :returns: gradient w.r.t the relaxed design
        """
        # TODO: This is formulated for FIM trace with pointwise OED. We need specialized
        # forms of the gradient to be called based on the formulation approach and whether
        # FIM is used or the posterior covariance matrix!
        raise NotImplementedError("TODO...")


    @property
    def configurations(self) -> RelaxedBayesianInversionDOptConfigs:
        """Return the configurations object"""
        return self._CONFIGURATIONS

    @property
    def formulation_approach(self):
        """Return the formulation approach"""
        return self.configurations.formulation_approach

    @formulation_approach.setter
    def formulation_approach(self, approach):
        self.update_configurations(formulation_approach=approach)

    @property
    def pcw_scheme(self) -> PCWFunction:
        """Return the PCW scheme"""
        return self.configurations.pcw_scheme

    @pcw_scheme.setter
    def pcw_scheme(self, scheme: str | PCWFunction):
        self.update_configurations(pcw_scheme=scheme)

    @property
    def pointwise_weighting_kernel(self):
        return self.pcw_scheme.kernel

    @property
    def pointwise_weighting_kernel_grad(self):
        return self.pcw_scheme.kernel_grad

    @property
    def n_deg(self):
        """n_deg in configurations"""
        return self.configurations.n_deg
    @n_deg.setter
    def n_deg(self, value):
        self.update_configurations(n_deg=value)

    @property
    def sigma_bounds(self):
        """sigma_bounds in configurations"""
        return self.configurations.sigma_bounds
    @sigma_bounds.setter
    def sigma_bounds(self, value):
        self.update_configurations(sigma_bounds=value)


