# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from dataclasses import dataclass
import re
import numpy as np

from pyoed import utility
from pyoed.configs import (
    validate_key,
    set_configurations,
)
from pyoed.assimilation.assimilation_utils import (
    apply_TLM_operator,
    apply_adjoint_operator,
)
from pyoed.assimilation.filtering import VariationalFilter
from pyoed.assimilation.smoothing import VariationalSmoother
from .bayesian_d_opt import (
    BayesianInversionDOpt,
    BayesianInversionDOptConfigs,
)


@dataclass(kw_only=True, slots=True)
class RobustBayesianInversionDOptConfigs(BayesianInversionDOptConfigs):
    """
    Configuration class for :py:class:`RobustBayesianInversionDOpt` criterion.
    See :py:class:`BayesianInversionDOptConfigs` for D-Opt specific configurations.
    """


@set_configurations(RobustBayesianInversionDOptConfigs)
class RobustBayesianInversionDOpt(BayesianInversionDOpt):
    """
    This class introduces an uncertain parameter over :py:class:`BayesianInversionDOpt`.
    """

    def __init__(
        self, configs: dict | RobustBayesianInversionDOptConfigs | None = None
    ):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | RobustBayesianInversionDOptConfigs,
        raise_for_invalid: bool = True,
    ):
        """
        Configuration validation for RobustBayesianInversionDOpt criterion.

        :param dict configs: configurations to validate.
        :param bool raise_for_invalid: if `True` raise an exception for invalid
            configurations.
        """
        return super().validate_configurations(configs, raise_for_invalid)

    def evaluate(self, design, uncertain_parameter_val, reset_parameter_val=False) -> float:
        """
        Evaluate the optimality criterion (D-optimality for sensor placement with
        uncertain parameter).

        The criterion is the D-optimality criterion (Trace of the FIM or the posterior
        covariance) with respect to the uncertainty hyperparamater. Which hyperparameter
        is used is set internally in the OED problem.

        :param design: a design vector conformable with the oed problem.
        :param uncertain_parameter_val: the value of the uncertain parameter.
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value

        :return: D-optimal design criterion value.
        """
        with self.oed_problem.uncertain_parameter_manager(uncertain_parameter_val):
            result = super().evaluate(design)
        return result

    def grad_uncertainty(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
    ) -> np.ndarray:
        """
        Evaluate the gradient of the D-optimality criterion (Trace of the FIM) with
        respect to the uncertainty hyperparamater. Which hyperparameter is used is set
        internally in the OED problem.

        :param design: a design conformable with the underlying oed problem.
        :param uncertain_parameter_val: the value of the uncertain parameter.
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value

        :return: The gradient of the D-optimal criterion with respect to the uncertain
            parameter.
        """
        oed_problem = self.oed_problem
        uncertain_parameter_name = oed_problem.uncertain_parameter_name

        # Evaluate the gradient; this depends on the optimality criterion and on the
        # uncertain parameter;
        if re.match(
            r"\Aprior(-| |_)*variance\Z", uncertain_parameter_name, re.IGNORECASE
        ):
            grad = self._grad_prior_variance(
                design=design,
                uncertain_parameter_val=uncertain_parameter_val,
                reset_parameter_val=reset_parameter_val,
            )

        elif re.match(
            r"\Aobservation(-| |_)*variance\Z", uncertain_parameter_name, re.IGNORECASE
        ):
            grad = self._grad_observation_variance(
                design=design,
                uncertain_parameter_val=uncertain_parameter_val,
                reset_parameter_val=reset_parameter_val,
            )

        elif re.match(
            (
                r"\A(observation(-| |_)*(and)*(-| |_)*prior|prior(-| |_)*(and)*(-|"
                r" |_)*observation)(-| _)*variance\Z"
            ),
            uncertain_parameter_name,
            re.IGNORECASE,
        ):
            # grad_prior = self._grad_prior_variance(
            #    design=design,
            #    uncertain_parameter_val=uncertain_parameter_val,
            #    reset_parameter_val=reset_parameter_val,
            # )
            # grad_observation = self._grad_observation_variance(
            #    design=design,
            #    uncertain_parameter_val=uncertain_parameter_val,
            #    reset_parameter_val=reset_parameter_val,
            # )
            # grad = np.concatenate([grad_prior, grad_observation])
            raise NotImplementedError(
                "This is not implemented yet!; "
                "Expected either prior variance or observation variance only."
            )
        else:
            raise ValueError(
                f"Invalid uncertain parameter '{uncertain_parameter_name}'."
            )
        return grad

    def _grad_prior_variance(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
    ) -> np.ndarray:
        """
        """
        raise NotImplementedError("TODO...")


    def _grad_observation_variance(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
        # force_update=True,
    ):
        """
        """
        raise NotImplementedError("TODO...")


