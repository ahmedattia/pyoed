# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This subpackage provides implementations of common optimality criteria (utility
functions) known as the alphabetic OED criteria. Implementations here are meant to be
used for OED problems which by definition target inverse problems.
This package provides the following:
"""

from . import (
    a_opt,
    d_opt,
)

from .a_opt import *
from .d_opt import *
