# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from dataclasses import dataclass
import re
from functools import partial
import numpy as np

from pyoed import utility
from pyoed.configs import (
    validate_key,
    set_configurations,
)
from pyoed.assimilation.assimilation_utils import (
    apply_TLM_operator,
    apply_adjoint_operator,
)
from pyoed.assimilation.filtering import VariationalFilter
from pyoed.assimilation.smoothing import VariationalSmoother
from pyoed.oed.core import SupportsRobustOptimization
from .bayesian_a_opt import (
    BayesianInversionAOpt,
    BayesianInversionAOptConfigs,
)


@dataclass(kw_only=True, slots=True)
class RobustBayesianInversionAOptConfigs(BayesianInversionAOptConfigs):
    """
    Configuration class for RobustBayesianInversionAOpt criterion.
    See :py:class:`BayesianInversionAOptConfigs` for A-Opt specific configurations.
    """


@set_configurations(RobustBayesianInversionAOptConfigs)
class RobustBayesianInversionAOpt(BayesianInversionAOpt):
    """
    This class introduces an uncertain parameter over :py:class:`BayesianInversionAOpt`.
    """

    def __init__(
        self, configs: dict | RobustBayesianInversionAOptConfigs | None = None
    ):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | RobustBayesianInversionAOptConfigs,
        raise_for_invalid: bool = True,
    ):
        """
        Configuration validation for RobustBayesianInversionAOpt criterion.

        :param dict configs: configurations to validate.
        :param bool raise_for_invalid: if `True` raise an exception for invalid
            configurations.
        """
        return super().validate_configurations(configs, raise_for_invalid)

    def evaluate(self, design, uncertain_parameter_val, reset_parameter_val=False) -> float:
        """
        Evaluate the optimality criterion (A-optimality for sensor placement with
        uncertain parameter).

        The criterion is the A-optimality criterion (Trace of the FIM or the posterior
        covariance) with respect to the uncertainty hyperparamater. Which hyperparameter
        is used is set internally in the OED problem.

        :param design: a design vector conformable with the oed problem.
        :param uncertain_parameter_val: the value of the uncertain parameter.
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value

        :return: A-optimal design criterion value.
        """
        with self.oed_problem.uncertain_parameter_manager(
                uncertain_parameter_val=uncertain_parameter_val
        ):
            result = super().evaluate(design)
        return result

    def grad_uncertainty(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
    ) -> np.ndarray:
        """
        Evaluate the gradient of the A-optimality criterion (Trace of the FIM) with
        respect to the uncertainty hyperparamater. Which hyperparameter is used is set
        internally in the OED problem.

        :param design: a design conformable with the underlying oed problem.
        :param uncertain_parameter_val: the value of the uncertain parameter.
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value

        :return: The gradient of the A-optimal criterion with respect to the uncertain
            parameter.
        """
        oed_problem = self.oed_problem
        uncertain_parameter_name = oed_problem.uncertain_parameter_name

        # Evaluate the gradient; this depends on the optimality criterion and on the
        # uncertain parameter;
        if re.match(
            r"\Aprior(-| |_)*variance\Z", uncertain_parameter_name, re.IGNORECASE
        ):
            grad = self._grad_prior_variance(
                design=design,
                uncertain_parameter_val=uncertain_parameter_val,
                reset_parameter_val=reset_parameter_val,
            )

        elif re.match(
            r"\Aobservation(-| |_)*variance\Z", uncertain_parameter_name, re.IGNORECASE
        ):
            grad = self._grad_observation_variance(
                design=design,
                uncertain_parameter_val=uncertain_parameter_val,
                reset_parameter_val=reset_parameter_val,
            )

        elif re.match(
            (
                r"\A(observation(-| |_)*(and)*(-| |_)*prior|prior(-| |_)*(and)*(-|"
                r" |_)*observation)(-| _)*variance\Z"
            ),
            uncertain_parameter_name,
            re.IGNORECASE,
        ):
            # grad_prior = self._grad_prior_variance(
            #    design=design,
            #    uncertain_parameter_val=uncertain_parameter_val,
            #    reset_parameter_val=reset_parameter_val,
            # )
            # grad_observation = self._grad_observation_variance(
            #    design=design,
            #    uncertain_parameter_val=uncertain_parameter_val,
            #    reset_parameter_val=reset_parameter_val,
            # )
            # grad = np.concatenate([grad_prior, grad_observation])
            raise NotImplementedError(
                "This is not implemented yet!; "
                "Expected either prior variance or observation variance only."
            )
        else:
            raise ValueError(
                f"Invalid uncertain parameter '{uncertain_parameter_name}'."
            )
        return grad

    def _grad_prior_variance(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
    ) -> np.ndarray:
        """
        Evaluate the gradient of the A-optimality criterion (Trace of the FIM) with
        respect to the prior variance hyperparamater. This is given by:

        .. math::
            -\\sum_{i=1}^{n_s} \\text{tr} \\left(
            \\mathbf{C}_{\\rm pr}^{-1}(\\lambda^{\\rm pr})
            \\frac{
                \\partial \\mathbf{C}_{\\rm pr}(\\lambda^{\\rm pr})
            }{\\partial \\lambda_i^{\\rm pr}}
            \\mathbf{C}_{\\rm pr}^{-1}(\\lambda^{\\rm pr})
            \\right) \\mathbf{e}_i

        .. note::
            See Equation (3.8) in the robust OED paper.

        .. note::
            It is expected that, if you're using this function, that the OED problem
            you've constructed has the `uncertain_parameter_sampler` field, with
            functionality of the `CovarianceSampler` object.

        :param design: design (constant) at which gradient is evaluated
        :param uncertain_parameter_val: the value of the uncertain parameter at which gradient is evaluated
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value

        :returns: gradient of the A-optimality criterion w.r.t prior variance
            hyperparameter
        """

        oed_problem = self.oed_problem
        ip = oed_problem.inverse_problem
        try:
            uncertain_parameter_sampler = oed_problem.uncertain_parameter_sampler
        except AttributeError as exc:
            raise AttributeError(
                "Expected OED problem to have `uncertain_parameter_sampler` field!"
            ) from exc

        # Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design
        prior_size = ip.prior.size

        # TODO: the following MUST be refactored for efficiency; We can't just construct
        # prior covariances in full.
        #
        # First, enable sampler (CovarianceSampler) to return a handler to apply the
        # parameter (and its derivative) by a vector (matrix-vector product;
        # matrix-free)
        #
        # Second, create a function/handle to apply the form of the matrix (inside the
        # trace operator by a vector),
        #
        # Third, pass this function to utility.matrix_trace(), and choose the evaluation
        # approach you like!

        hyperparam_val = utility.asarray(uncertain_parameter_val)
        covmat = utility.asarray(uncertain_parameter_sampler.evaluate(hyperparam_val))
        covmat_inv_sqrd = np.linalg.inv(covmat)
        covmat_inv_sqrd = np.dot(covmat_inv_sqrd, covmat_inv_sqrd)

        # Update linearization point
        lp = self.update_linearization(design)

        # If randomized, make sure randomized_vectors are constructed.
        # If not, construct and store them.

        if self.use_FIM:
            objfun = lambda x: ip.Hessian_matvec(x, eval_at=lp)
        else:
            objfun = lambda x: ip.Hessian_inv_matvec(x, eval_at=lp)

        if re.match(r"\Arandom(ized|)\Z", self.evaluation_method, re.IGNORECASE):
            if self.randomization_vectors is None:
                # Get the linearization point
                _, r_vectors = utility.matrix_trace(
                    objfun,
                    size=prior_size,
                    method=self.evaluation_method,
                    sample_size=self.sample_size,
                    optimize_sample_size=self.optimize_sample_size,
                    min_sample_size=self.min_sample_size,
                    max_sample_size=self.max_sample_size,
                    distribution=self.distribution,
                    random_seed=self.random_seed,
                    accuracy=self.accuracy,
                    #
                    return_randomization_vectors=True,
                )
                self.randomization_vectors = r_vectors

        # Build the gradient (entry by entry)
        grad = np.empty(prior_size)

        if self.use_FIM:
            for i in range(grad.size):
                kernel_deriv = utility.asarray(
                    uncertain_parameter_sampler.evaluate_derivative(hyperparam_val, i)
                )
                grad[i] = - utility.matrix_trace(
                    np.dot(covmat_inv_sqrd, kernel_deriv),
                    size=grad.size,
                    method=self.evaluation_method,
                    sample_size=self.sample_size,
                    optimize_sample_size=self.optimize_sample_size,
                    min_sample_size=self.min_sample_size,
                    max_sample_size=self.max_sample_size,
                    randomization_vectors=self.randomzation_vectors,
                    distribution=self.distribution,
                    random_seed=self.random_seed,
                    accuracy=self.accuracy,
                    #
                )

        else:
            raise NotImplementedError("TODO")

        # Reset the design
        oed_problem.design = current_design

        # There is nothting to do; the uncertain parameter does not change the problem
        # elements
        if reset_parameter_val:
            pass

        return grad

    def _grad_observation_variance(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=False,
        # force_update=True,
    ):
        r"""
        Evaluate the gradient of the A-optimality criterion (Trace of the FIM) with
        respect to the observation error covariance hyperparamater

        .. math::
            -\sum_{i=1}^{n_s} \text{tr} \left(
                \mathbf{W}_{\Gamma}(\zeta; \lambda^{\rm obs})
                \mathbf{F} \mathbf{F}^*
                \mathbf{W}_{\Gamma}(\zeta; \lambda^{\rm obs})
                \left[
                    \mathbf{W}(\zeta) \odot
                    \frac{
                        \partial \Gamma (\lambda^{\rm obs})
                    }{
                        \partial \lambda_i^{\rm obs}
                    }
                \right]
            \right) \mathbf{e}_i

        .. note::
            See Equation (A.7) in the robust OED paper.

        .. note::
            It is expected that, if you're using this function, that the OED problem
            you've constructed has the `uncertain_parameter_sampler` field, with
            functionality of the `CovarianceSampler` object.

        :param design: design (constant) at which gradient is evaluated
        :param uncertain_parameter_val: the value of the uncertain parameter at which
            gradient is evaluated
        :param bool reset_parameter_val: if `True` reset the uncertain parameter value to
            its original value
        :param bool force_update: Only relevant for non-linear models. If True, the
            internally tracked linearization point is updated to one computed with the
            passed design. Otherwise, the linearization point is updated only if it
            hadn't been computed before. Default is True.

        :returns: gradient of the A-optimality criterion w.r.t observation error
            covariance model hyperparameter
        :rtype: numpy.ndarray
        """
        # Retrieve the underlying inverse problem and update the uncertain parameter
        oed_problem = self.oed_problem
        ip = oed_problem.inverse_problem

        try:
            uncertain_parameter_sampler = oed_problem.uncertain_parameter_sampler
        except Exception as err:
            raise AttributeError(
                f"Expected OED problem to have `uncertain_parameter_sampler` field!"
                f"See the details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )

        if isinstance(ip, VariationalSmoother):
            obs_tspan = ip.observation_times

        elif isinstance(ip, VariationalFilter):
            # Creating fake time instances for consistency here
            # (and to utilize smoother code with loop)
            obs_tspan = [None]

        else:
            raise NotImplementedError(
                f"Only VariationalFilter/VariationalSmoother inverse problems are acceptable "
                "for inverse problems this time.\n"
                f"Received '{type(ip)}' which is not yet supported"
            )

        # Uncertain parameter in the OED problem
        uncertain_parameter_val = utility.asarray(uncertain_parameter_val).flatten()
        # uncertain_parameter_val = uncertain_parameter_sampler.evaluate(hyperparam_val)

        # Get current design from the OED problem, and set the design to the passed one
        current_design = oed_problem.design
        oed_problem.design = design

        # Get the linearization point (if needed)
        lp = self.update_linearization(design)

        if self.use_FIM:
            objfun = lambda x: ip.Hessian_matvec(x, eval_at=lp)
        else:
            objfun = lambda x: ip.Hessian_inv_matvec(x, eval_at=lp)

        # Create function that wraps the matrix log-det calculator
        U = partial(
            utility.matrix_trace,
            method=self.evaluation_method,
            size=ip.prior.size,
            sample_size=self.sample_size,
            optimize_sample_size=self.optimize_sample_size,
            min_sample_size=self.min_sample_size,
            max_sample_size=self.max_sample_size,
            distribution=self.distribution,
            randomization_vectors=self.randomization_vectors,
            random_seed=self.random_seed,
            accuracy=self.accuracy,
        )

        # Forward and adjoint settings (time points, etc.)
        active = np.where(design)[0]  # get only active entries to save cost

        # Initialize gradient
        grad = np.zeros(uncertain_parameter_val.size)

        def gradient_matrix_matvec(x, A):
            """
            Each entry in the the gradient is equal to the trace of a matrix.
            The matrix includes the application of the derivative of the noise
            covariance matrix (here A).
            This function applies this matrix to a vector `x`.
            """
            out = 0.0
            for t in obs_tspan:

                y = apply_TLM_operator(
                    ip=self.oed_problem.inverse_problem,
                    state=x,
                    obs_time=t,
                    eval_at=lp,
                    save_all=False,
                    scale_by_noise=True,
                )

                # Apply the intermediate matrix A
                y = utility.matrix_vecmult(A, y)

                adj = apply_adjoint_operator(
                    ip=self.oed_problem.inverse_problem,
                    obs=y,
                    obs_time=t,
                    eval_at=lp,
                    save_all=False,
                    scale_by_noise=True,
                )

                if not self.use_FIM:
                    # If the Inverse of the FIM is used, apply it (posterior cov)
                    adj = objfun(adj)

                out += adj
            return out


        # Calculate derivative for the given uncertain parameter
        with self.oed_problem.uncertain_parameter_manager(
                uncertain_hyperparameter_val=uncertain_parameter_val
        ):

            # For each entry evaluate the derivative of the covariance matrix
            for i in range(grad.size):
                # Evaluate kernel derivative and weight by the weighting kernel/matrix
                # in the reduced (active) subspace
                weighted_kernel_deriv = \
                    ip.observation_error_model.apply_weighting_matrix(
                        design=design,
                        variance=utility.asarray(
                            uncertain_parameter_sampler.evaluate_derivative(
                                uncertain_parameter_val, i
                            ),
                        )
                    )[active, :][:, active]

                grad[i] = U(
                    lambda x: gradient_matrix_matvec(x, weighted_kernel_deriv),
                )

            # Flip the sign (-Trace...) if use_FIM
            if self.use_FIM:
                grad *= -1

        # Reset the design
        self.oed_problem.design = current_design

        return grad

