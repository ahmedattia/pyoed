# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Approaches for solving the sensor placement OED problem
for Bayesian inversion.
Implementations provided here are general enough to be
adopted for more general formulations, and documentations
will be updated accordingly.
"""

# import copy
import os
import re
import inspect
import numpy as np
import warnings
import pickle
from dataclasses import dataclass
from typing import Type
import matplotlib.pyplot as plt

from pyoed import utility
from pyoed.configs import (
    PyOEDConfigs,
    validate_key,
    set_configurations,
)
from pyoed.utility.mixins import RandomNumberGenerationMixin
from pyoed.stats.distributions.bernoulli import Bernoulli
from pyoed.assimilation import (
    InverseProblem,
)
from pyoed.optimization import (
    create_optimizer,
    Optimizer,
    OptimizerConfigs,
    ScipyOptimizer,
)
from pyoed.optimization.binary_optimization import (
    GreedyBinaryOptimizer,
    BinaryReinforceOptimizer,
    ConstrainedBinaryReinforceOptimizer,
    #
    BinaryReinforceOptimizerConfigs,
    # ConstrainedBinaryReinforceOptimizerConfigs,
)
from pyoed.optimization import optimization_utils as opt_utils
from pyoed.models.error_models import Gaussian
from pyoed.oed import utility_functions as uf
from pyoed.oed.core import (
    Criterion,
    SensorPlacementInversionOED,
    SensorPlacementInversionOEDConfigs,
    SensorPlacementInversionOEDResults,
)


@dataclass(kw_only=True, slots=True)
class SensorPlacementBayesianInversionOEDConfigs(SensorPlacementInversionOEDConfigs):
    """
    Configurations class for the :py:class:`SensorPlacementBayesianInversionOED` class.
    This class inherits functionality from :py:class:`SensorPlacementInversionOEDConfigs`
    and only adds the class-level variables below which can be updated as needed:

    :param optimizer: the optimization routine (optimizer) to register.
        This can be one of the following:

        - An optimizer instance (object that inherits :py:class`Optimizer`).
          In this case, the optimizer is registered **as is** and is updated
          with the passed configurations if available.
        - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
          the optimizer.
          To see available *built-in* optimization routines, see/call
          :py:meth:`show_supported_optimizers`.
        - The name of the optimizer (`str`). This has to match the name of one
          of the available optimization routine.
          To see available *built-in* optimization routines, see/call
          :py:meth:`show_supported_optimizers`.

    :param optimizer_configs: the configurations of the optimization routine.
        This can be one of the following:

        - `None`, in this case configurations are discarded, and whatever default configurations
          of the selected/passed optimizer are employed.
        - A `dict` holding full/partial configurations of the selected optimizer.
          These are either used to instantiate or update the optimizer configurations based on
          the type of the passed `optimizer`.
        - A class providing implementations of the configurations (this must be a subclass of
          :py:class:`OptimizerConfigs`.
        - An instance of a subclass of :py:class:`OptimizerConfigs` which is to set/udpate
          optimizer configurations.

    :param random_seed: random seed to be used to globally set seeds of the underlying
        random number generators.
        This affects both the optimizer and the uncertain parameter sampler (if accepting
        random seed configuration).
    """
    criterion: None | str | Criterion = 'Bayesian A-opt'
    optimizer : str | Optimizer | Type[Optimizer] = BinaryReinforceOptimizer
    optimizer_configs : None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None
    random_seed: int | None = None


@set_configurations(SensorPlacementBayesianInversionOEDConfigs)
class SensorPlacementBayesianInversionOED(SensorPlacementInversionOED, RandomNumberGenerationMixin):
    """
    This class provides a general implementation for sensor-placement OED
    in Bayesian inverse problems.
    The two main components those must be set (in the configurations)
    or by calling the proper registeration method, are the optimality
    `criterion` and the `optimizer`.
    Default values are set in the default configurations passed to the
    constructor `__init__`, but the user can pass others, or change either of
    them later.

    .. note::
        The implementation here is independent from the choise of the
        optimality criterion and/or the optimizer.
        Thus, both relaxation, and binary optimization methods apply.
    """
    # Hard-coded optimizers we support out of the box
    _SUPPORTED_OPTIMIZERS = [
        "ScipyOptimizer",
        "GreedyBinaryOptimizer",
        "BinaryReinforceOptimizer",
        "ConstrainedBinaryReinforceOptimizer",
    ]


    def __init__(self, configs: SensorPlacementBayesianInversionOEDConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Update the design
        if self.design is None:
            self.update_design(self.inverse_problem.observation_operator.design)

        ## Add Internally preserved (local) variables

        ## Register elements properly
        # The ptimization routine
        self.configurations.optimizer = self.register_optimizer(
            optimizer=self.configurations.optimizer,
            optimizer_configs=self.configurations.optimizer_configs,
        )

        # Random seed
        self.update_random_number_generators(self.configurations.random_seed)

        # Additionals
        self._OBJECTIVE_SCALING_FACTOR = 1.0  # Scales (multiplies by) result of the objective function and its derivatives
        self._OBJECTIVE_OFFSET = 0  # Shifts (adds to) result of the objective function

    def validate_configurations(
        self,
        configs: dict | SensorPlacementBayesianInversionOEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`SensorPlacementBayesianInversionOEDConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # Local tests
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        def is_valid_optimizer(optimizer):
            if inspect.isclass(optimizer):
                return issubclass(optimizer, Optimizer)
            elif isinstance(optimizer, Optimizer):
                return True
            elif isinstance(optimizer, str):
                return optimizer in self.supported_optimizers
            else:
                return False

        # `random_seed`: None or int>=0
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda v: v is None or (isinstance(v, int) and v>=0),
            message=f"random_seed must be None or a non-negative integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `optimizer`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="optimizer",
            test=is_valid_optimizer,
            message=f"Invalid optimizer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Check if "random_seed" is passed; update random number generator
        if "random_seed" in kwargs:
            self.update_random_number_generators(random_seed=kwargs["random_seed"])

        # Optimizer and/or optimizer configurations
        if "optimizer" in kwargs:
            self.register_optimizer(
                optimizer=kwargs["optimizer"],
                optimizer_configs=kwargs.get("optimizer_configs", None),
            )
        elif "optimizer_configs" in kwargs:
            optimizer_configs = kwargs["optimizer_configs"]
            if isinstance(optimizer_configs, dict):
                pass
            elif isinstance(optimizer_configs, OptimizerConfigs):
                optimizer_configs = optimizer_configs.asdict()
            else:
                raise TypeError(
                    f"Updating optimizer confiurations requires passing a dictionary or instance "
                    f"of proper Configs class that the optimizer accepts (here "
                    f"{self.optimizer.configurations_class}).\n"
                    f"Passed {optimizer_configs=} of {type(optimizer_configs)}"
                )
            self.optimizer.update_configurations(**optimizer_configs)

    def register_optimizer(
        self,
        optimizer: str | Optimizer | Type[Optimizer] = "BinaryReinforceOptimizer",
        optimizer_configs: None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None,
    ) -> Optimizer:
        """
        Register (and return) an optimization routine, and make sure the
        objective function in the optimization routine is set to the objctive function of
        this objective :py:meth:`objective_function_value`.
        The objective function of the optimizer (and its derivatives) are set to the
        objective function and derivatives implemented here **unless the objective function is
        passed explicitly in the configurations**.
        This method sets a reference to the optimizer and update the underlying configurations
        accordingly.

        .. note::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            and valid configurations `optimizer_configs` is passed, the optimizer is
            updated with the passed configurations by calling
            `optimizer.update_configurations(optimizer_configs)`.

        .. warning::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            the responsibility is on the developer/user to validate the contents of the
            optimizer.

        :param optimizer: the optimization routine (optimizer) to register.
            This can be one of the following:

            - An optimizer instance (object that inherits :py:class`Optimizer`).
              In this case, the optimizer is registered **as is** and is updated
              with the passed configurations if available.
            - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
              the optimizer.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.
            - The name of the optimizer (`str`). This has to match the name of one
              of the available optimization routine.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.

        :param optimizer_configs: the configurations of the optimization routine.
            This can be one of the following:

            - `None`, in this case configurations are discarded, and whatever default
              configurations of the selected/passed optimizer are employed.
            - A `dict` holding full/partial configurations of the selected optimizer.
              These are either used to instantiate or update the optimizer configurations
              based on the type of the passed `optimizer`.
            - A class providing implementations of the configurations (this must be
              a subclass of :py:class:`OptimizerConfigs`.
            - An instance of a subclass of :py:class:`OptimizerConfigs` which is to
              set/udpate optimizer configurations.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer and/or configurations
            are/is not supported
        """
        if isinstance(optimizer, str):
            if optimizer in self.supported_optimizers:
                # Convert class name to class
                optimizer = eval(optimizer)
            else:
                raise TypeError(
                    f"Invalid optimizer name {optimizer=}"
                )

        # Create a dictionary with configurations to be forced
        new_configs = {
            "fun": self.objective_function_value,
            "jac": self.objective_function_grad_design,
            "size": self.design_size,
            "verbose": self.verbose,
        }

        print("Optimizer", optimizer, type(optimizer))
        print("Optimizer Configs", optimizer_configs, type(optimizer_configs))

        try:

            optimizer = create_optimizer(
                optimizer=optimizer,
                optimizer_configs=optimizer_configs,
                new_configs=new_configs,
            )
        except Exception as err:
            raise TypeError(
                "Failed to create/update the optimizer! "
                f"See the error below for additional details\n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

        # Update optimizer and configurations properly
        return super().register_optimizer(
            optimizer=optimizer,
        )


    def update_design(self, design):
        """
        Update the components of the OED (and inverse) problem with the passed
        `design`. This method updates both observation operator and observation
        error model of the underlying inverse problem with the passed design.

        .. note::
            This method need to be replaced with design-specific impelentation
            for any design different from observational experimental design.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.

        """
        if design is not None:
            # Assertion on design
            design = utility.asarray(design)
            assert design.ndim == 1, "design must be castable to 1d array"

            # Design of the observation error model (boolean for binary or float for relaxed)
            try:
                self.inverse_problem.observation_error_model.design = design
            except Exception as err:
                print(
                    f"Tried updating the experimental design of the observation error model\n"
                    f"The error model is {self.inverse_problem.observation_error_model=}\n"
                    f"Unexpected {err=} or {type(err)=}"
                )
                raise

            # Design of the observation operator (boolean, but the operator will take care of type)
            try:
                self.inverse_problem.observation_operator.design = design
            except Exception as err:
                print(
                    f"Tried updating the experimental design of the observation operator\n"
                    f"The error model is {self.inverse_problem.observation_operator=}\n"
                    f"Unexpected {err=} or {type(err)=}"
                )
                raise

        # Call super to update the design attribute
        return super().update_design(design)


    def update_random_number_generators(self, random_seed):
        """
        Update random seeds of underlying random number gnerators
        (both local and optimizer if supported)
        """
        self.update_random_number_generator(random_seed)
        try:
            self.optimizer.update_configurations(random_seed=random_seed)
        except Exception as err:
            print(
                f"Expected the optimizer to support random seed update\n"
                f"Ignoring {err=} of {type(err)=}"
            )
            pass

    def register_optimality_criterion(
            self,
            criterion: Criterion | str | None = None,
    ) -> None:
        """
        Validate and set the OED Bayesian optimality criterion as described by
        `criterion`, update the configurations object with the criterion,
        and return the new criterion.

        .. note::
            This method only creates a valid criterion (instance of :py:class`Criterion` if
            a string is passed (name of the criterion)), and then calls super class (parent)
            to set the criterion.
            For additional details see e.g.,
            :py:class:`pyoed.oed.InversionOED.register_optimality_criterion`
        """
        # Check the criterion and if legacy (string) is passed, replace it with utility
        # function objection
        if isinstance(criterion, str):
            criterion = uf.find_criteria(criterion)
            criterion = criterion(configs={"oed_problem": self})

        # Pass up to put the criterion in the right place
        return super().register_optimality_criterion(criterion=criterion)

    def objective_function_value(
        self,
        design,
    ):
        """
        Evaluate the value of the OED objective function (regularized utility
        function) given the passed design.
        :py:meth:`SensorPlacementInversionOED.objective_function_value`.
        updated first, then the objective is calculated, and then the uncertain
        parameter is reset to the original if needed

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.
            This method accepts a design that is either ``binary`` or ``relaxed`` (over the
            interval [0, 1]), and uses the design to properly relax the OED optimization
            objective (with the right limits at the boundary).
        :param uncertain_parameter_val: value of the uncertain parameter; this must match
            the registered uncertain parameter and sampler.
        :param bool reset_parameter_val: if `True`, the current values of the uncertain parameter
            are restored after evaluating the objective function value

        :returns: value of the *registered* OED optimality criterion

        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter` is registered.
        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter_sampler` is
            registered with a valid type. Only instances of :py:class:`ParameterSampler`
            are accepted.
        :raises: :py:class:`TypeError` is raised if no optimality criterion has been
            registered yet
        :raises: :py:class:`TypeError` is raised if the either the uncertain parameter
            or the associated sampler is missing.
        """
        if self.oed_criterion is None:
            raise TypeError(
                f"No optimality criterion has been registered yet! Use"
                f" `register_optimality_criterion()`"
            )

        # Evaluate the criterion/utility function objective value
        obj_val = self.oed_criterion.evaluate(
            design,
        )

        # Check if artificial scaling is registered
        if self.objective_function_scaling_factor is not None:
            obj_val *= self.objective_function_scaling_factor
        if self.objective_function_offset is not None:
            obj_val += self.objective_function_offset

        # Update objective with penalty/regularization term (if registered)
        if self.penalty_weight not in [0, None]:
            obj_val += self.penalty_function(design) * self.penalty_weight

        # Return the objective value
        return obj_val

    def objective_function_grad_design(
        self,
        design,
    ):
        """
        Derivative of the objective function (that is the regularized utility/criterion,
        e.g., trace of FIM for A-opt) with respect to the relaxed design. This is used
        when the OED optimization problem is solved with relaxation of the design allowing
        the design to take any value in the interval [0, 1] and thus following a gradient
        descent/ascent direction for optimization over the design.
        The gradient is thus :math:`\\nabla_{d} \\mathcal{J(d)}` where
        :math:`d` is the design, and :math:`\\mathcal{J}` is the OED
        objective/criterion (A-opt etc.) evaluated by :py:meth:`objective_function_value`.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.
            This is intended to be a relaxed design (over the interval [0, 1]),

        :returns: gradient of the the function defined by :py:meth:`objective_function_value`

        :raises: :py:class`TypeError` is raised if penalty function/term is not properly set.
        :raises: :py:class`TypeError` is raised if no valid optimality `criterion` object is
            set in the configurations dictionary.
        :raises: :py:class`TypeError` is raised if a `criterion` is registerd but it does not
            provide a :py:meth:grad_design` function.
        """
        # Validate Penalty term components (if any)
        penalty_weight = self.penalty_weight
        if penalty_weight not in [0, None] and (
            self.penalty_function is None or self.penalty_function_gradient is None
        ):
            raise TypeError(
                f"For nonzero penalty, you MUST set proper function for the penalty"
                f" term along with associate gradient\nUse register_penalty_term()"
                f" method before calling this function"
            )

        # Verify the OED criterion (utility function) is properly set to A-opt
        criterion = self.oed_criterion
        if criterion is None:
            raise TypeError(
                f"You must register/set an OED optimality criterion first. Use/Call"
                f" register_optimality_criterion()"
            )
        elif not hasattr(criterion, "grad_design"):
            # Finite differences
            grad = utility.finite_differences_gradient(
                fun=criterion.evaluate,
                state=design,
                bounds=[(0, 1)]*design.size,
            )
        else:

            # Evaluate the gradient of the optimality criterion (the utility function)
            grad = criterion.grad_design(design, )

        # Apply the artificial scaling (if registered)
        if self.objective_function_scaling_factor is not None:
            grad *= self.objective_function_scaling_factor

        # Adde derivative of the penalty term (if registered)
        if self.penalty_weight not in [0, None]:
            grad += self.penalty_function_gradient(design) * self.penalty_weight

        # Return the gradient
        return grad
    # Alias
    objective_function_gradient = objective_function_grad_design


    def solve(self, init_guess=None, ):
        """
        Solve the OED optimization problem by using the registered
        optimization procedure.

        :param init_guess: initial guess (e.g., initial design, initial policy parameters)

        :returns: an instance of :py:class:`SensorPlacementBayesianInversionOEDResults`
             holding results obtianed by solving the OED optimization problem.

        :raises TypeError: if no valid optimizer is registered
        """
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )

        if self.verbose:
            print(
                "Stared Solving the OED problem "
            )

        if init_guess is None:
            init_guess = 0.5 * np.ones(self.design_size)

        # Solve the OED optimization problem, create a results object, and return it
        optimization_results = self.optimizer.solve(
            init_guess,
        )

        # Create and retuen a data-structure holding the OED results
        return SensorPlacementBayesianInversionOEDResults(
            oed_problem=self,
            optimization_results=optimization_results,
        )


    @staticmethod
    def show_supported_optimizers():
        """
        Print out optimization routines available to choose from.
        In addition to those shown here, the user can register fully instantiated
        optimizer (instance of :py:class:`Optimizer`) by calling
        :py:meth:`register_optimizer`.

        :returns: a list of supported optimization routines names
        """
        sep = "*" * 80
        msg = f"{sep}\n  Supoorted Optimization Routines: \n{sep}\n"
        for opt in self.supported_optimizers:
            msg += f" - {opt} \n"
        msg += f"{sep}\n"
        print(msg)

    @staticmethod
    def show_supported_uncertain_parameters():
        """
        Print out uncertain parameters available to choose from.
        """
        sep = "*" * 80
        msg = f"{sep}\n  Supoorted Uncertain Parameters: \n{sep}\n"
        for par in self.supported_uncertain_parameters:
            msg += f" - {par} \n"
        msg += f"{sep}\n"
        print(msg)

    @property
    def objective_function_offset(self):
        """Numeric offset added to the value of the objective (shifting)"""
        return self._OBJECTIVE_OFFSET

    @objective_function_offset.setter
    def objective_function_offset(self, value):
        """Numeric offset added to the value of the objective (shifting)"""
        if value is not None:
            assert utility.isnumber(
                value
            ), "The scaling factor must be a number (or None)"
        self._OBJECTIVE_OFFSET = float(value)

    @property
    def objective_function_scaling_factor(self):
        """A scaling factor of (to be multiplied by) the objective function self.objective_function"""
        return self._OBJECTIVE_SCALING_FACTOR

    @objective_function_scaling_factor.setter
    def objective_function_scaling_factor(self, value):
        """A scaling factor of (to be multiplied by) the objective function self.objective_function"""
        if value is not None:
            assert utility.isnumber(
                value
            ), "The scaling factor must be a number (or None)"
        self._OBJECTIVE_SCALING_FACTOR = float(value)

    @property
    def supported_optimizers(self, ):
        return self._SUPPORTED_OPTIMIZERS



@dataclass(kw_only=True, slots=True)
class SensorPlacementBayesianInversionOEDResults(SensorPlacementInversionOEDResults):
    """
    Base class for objects holding results of
    :py:class:`SensorPlacementBayesianInversionOED`
    """
    ...



## Helper functions
def create_sensor_placement_binary_oed_problem(inverse_problem):
    """
    Given an inverse problem, create a standard OED problem with binary
    sensor placment capability. All designs can be binary, so, only make sure
    the optimizer is suitable for binary optimization.
    """
    assert isinstance(inverse_problem, InverseProblem), f"{type(inverse_problem)=} is unacceptable"

    return SensorPlacementBayesianInversionOEDConfigs(
        {
            "ivnerse_problem": inverse_problem,
            "optimizer": "BinaryReinforceOptimizer",
        }
    )

def create_sensor_placement_binary_oed_problem(inverse_problem):
    """
    Given an inverse problem, create an OED problem with binary or relaxed
    sensor placment capability. This requires the observation error model
    to allow relaxed design.
    Also the optimizer is set to one suitable for non-binary optimization.
    """
    assert isinstance(inverse_problem, InverseProblem), f"{type(inverse_problem)=} is unacceptable"
    if not inverse_problem.observation_error_model.supports_relaxtion:
        raise TypeError(
            f"The {inverse_problem.observation_error_model=} does not support relaxation"
        )
    return SensorPlacementBayesianInversionOEDConfigs(
        {
            "ivnerse_problem": inverse_problem,
            "optimizer": "ScipyOptimizer",
            "optimizer_configs": {"bounds": (0, 1)}
        }
    )

