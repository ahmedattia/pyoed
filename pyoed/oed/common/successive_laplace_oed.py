# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Oversimplified implementaiton of OED for nonlinear problems vis Successive Laplacian Approximation (SLA)
"""


import warnings
import os
import numpy as np
import pickle
from dataclasses import dataclass
from copy import deepcopy
import matplotlib.pyplot as plt

from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    PyOEDData,
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
)
from pyoed import utility
from pyoed.models.error_models.Gaussian import GaussianErrorModel
from pyoed.oed.core import (
    OED,
    Linearization,
    OEDResults,
)


@dataclass(kw_only=True, slots=True)
class SuccessiveLaplaceOEDConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`SuccessiveLaplaceOED` class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and
    only adds new class-level variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of
    this class along with a few additional fields.
    Otherwise :py:class:`SuccessiveLaplaceOEDConfigs` provides the
    following fields:

    :param oed_problem: The OED problem (instance derived from
        :py:class:`pyoed.oed.core.OED`)
    """

    oed_problem: OED | None = None


# TODO: This needs a refactor. The optimal design need to be accessible (in oed results structure) so we can solve the OED problem for the design ...
@set_configurations(SuccessiveLaplaceOEDConfigs)
class SuccessiveLaplaceOED(PyOEDObject):
    """
    Solving non-linear OED problems with successive Laplace approximations (SLOED).

    :param configs: configurations to initiate the instance of
        :py:class:`SuccessiveLaplaceOED` with
    """

    def __init__(
        self, configs: dict | SuccessiveLaplaceOEDConfigs | None = None
    ) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        raise NotImplementedError(
            f"Revisit after updating OED results objects to provide "
            f"unified interface to optimal design."
        )

    def validate_configurations(
        self,
        configs: dict | SuccessiveLaplaceOEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Check and validate the passed configurations and make sure they
        are conformable with each other, and with current configurations
        once combined.

        :param configs: configurations to validate.
            If a :py:class:`SuccessiveLaplaceOEDConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does
            not exist in the optimizer configurations :py:class:`PyOEDConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(
            configs=configs,
        )

        ## Validation Stage
        # `oed_problem`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="oed_problem",
            test=lambda v: isinstance(v, OED),
            message=f"Invalid `oed_problem`. Expected instance derived from `pyoed.oed.core.OED` ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    # TODO: The parameters here should be class-level configurations
    def solve(
        self,
        initial_design=None,
        max_sl_iterations=5,
        prior_updating=False,
        covariance_inflation_factor=1.0,
        stopping_criterion=None,
        oed_problem_kwargs={},
    ):
        """
        Start solving the binary OED with successive Laplace (SL) approximations problem
        for the registered configuration.

        :param np.ndarray initial_design: initial design to start the SL iterations
            with. If None, a design with all sensors turned on is used.
        :param int max_sl_iterations: maximum number of SL iterations. Default is 5.
        :param bool prior_updating: if True, update the prior distribution of the
            inverse problem with the Laplace approximation of the posterior distribution
            of the uncertain parameter at each SL iteration. Default is False.
        :param float covariance_inflation_factor: inflation factor; a number >=1 to inflate
            prior covariances. This is utilized only if `prior_updating` is `True`.

            .. note::
                If set to `True`, the `SLOEDREsults` object will be bugged in that it
                will lose the intermediate prior distributions.
                They can, however, still be inferred from the intermediate posterior
                distributions.

        :param str stopping_criterion: stopping criterion for the SL iterations.
            The following options are accepted:

            - `None` or `'maxiter'`: the SL iterations will continue until the maximum
              number of iterations is reached
            - `'design'`: the SL iterations stop when the optimal design does not change
              from one iteration to the next.
            - `'objval'`: the SL iterations stop when the optimal objective value does not
              change from one iteration to the next.

            Default is None, which means that .

        :param dict oed_problem_kwargs: a dictionary of keyword arguments to pass to the
            :class:`OEDProblem` constructor. If None, the default values are used.
            The following keyword arguments are supported:

            - ``batch_size``: sample size for stochastic gradient estimation
            - ``baseline``: Either None, or a string representation of the baseline to
              use; heuristic, optimal are supported
            - ``init_theta``: initial policy parameter
            - ``learning_rate``: if None, 1/observation size is selected
            - ``maxiter``: maximum number of iterations for the optimization algorithm.
            - ``decay_step``: reduce the step size (learning rate) as the algorithm
              proceeds
            - ``solve_inverse_problem``: solve the inverse problem given the optimal
              design (if a nominal value is given) (as well as the robust value of the
              uncertain parameter if no nominal value is passed)
            - ``bruteforce``: enumerate all possible designs, and find the best; this
              should be only used with small problems for testing purposes. Note that
              here, we evaluate brute force over the sample from the posterior
              distribution of the uncertain parameter, and not over the prior
              distribution.
            - ``cluster_bruteforce``: cluster the designs by the number of active
              sensors, and evaluate the objective function only on the best design in
              each cluster. This is useful for large problems, where the number of
              designs is too large to evaluate the objective function on all of them.
              Note that this is only used if ``bruteforce`` is True.
            - ``maxiter``: maximum number of iterations for the optimization algorithm
              (REINFORCE)

        :returns: A list holding the results of each OED iteration. The indices
            represent the iteration number, and the values are OEDResults instances
            holding the results of the OED problem for that iteration.
        :rtype: list[OEDResults]
        """
        # Validate arguments
        if stopping_criterion not in [None, "maxiter", "objval", "design"]:
            raise ValueError(f"Unknown stopping criterion '{stopping_criterion}' !")

        if (
            not utility.isnumber(covariance_inflation_factor)
            or covariance_inflation_factor < 1
        ):
            raise ValueError(
                f"Invalid prior inflation factor {covariance_inflation_factor}\n"
                f"Inflation factor must >= 1"
            )

        # Retrieve pointers
        op = self.oed_problem  # OED problem
        ip = self.oed_problem.inverse_problem  # Inverse problem
        criterion = (
            self.oed_problem.criterion
        )  # Optimality criterion (utility function)

        # Check the OED problem
        if op.problem_is_linear:
            warnings.warn(
                "The problem is linear, successive laplace approximations are"
                " uncessary, as a singular Laplace approximation is exact!"
                " Proceeding... but consider using regular"
                " solve() method associated with the linear OED problem  instead."
            )

        # Get a copy of design, and prior to reset after solving the OED problem
        passed_data = {
            "design": op.design,
            "prior_mean": op.inverse_problem.prior.mean,
            "prior_covariance": op.inverse_problem.prior.covariance_matrix(),
        }

        # Initialize the design
        if initial_design is None:
            initial_design = np.ones(op.design_size, dtype=bool)
        current_design = initial_design.copy()

        # The utility function (optimality criterion)
        if criterion is None:
            raise ValueError("No criterion is registered with the OED problem!")

        # The linearization point is udpated at each iteration manually
        # Set initial linearization point to the prior mean
        criterion.update_configurations(lp_behavior="manual")
        criterion.update_linearization(design=op.design, point=ip.prior.mean)

        SLOED_results = SLOEDResults()
        idx = 0
        while idx < max_sl_iterations:
            if self.verbose:
                print(f"Starting SL iteration {idx + 1}!")

            # Update OED problem design & linearization point (MAP estimate)
            op.design = current_design

            # Solve the OED problem
            OED_results = op.solve(**oed_problem_kwargs)

            # Solve the inverse problem & update the posterior (at the optimal design)
            op.design = OED_results.optimal_design
            map_point = ip.solve(
                skip_map_estimate=False,
                update_posterior=True,
            )

            # Create linearization point & update the OED problem with it
            criterion.update_linearization(point=map_point, design=OED_results.design)

            # TODO: Revisit after checking `SLOEDResults` implementation
            # TODO: Discuss whether we actually need a copy!
            SLOED_results.add_result(deepcopy(OED_results))

            # Process stopping criteria
            if stopping_criterion == "design" and np.all(
                current_design == OED_results.optimal_design
            ):
                # Design does not change
                break

            elif (
                idx > 0
                and stopping_criterion == "objval"
                and np.isclose(
                    SLOED_results.OED_Results_iterates[-1].optimal_objective_value,
                    SLOED_results.OED_Results_iterates[-2].optimal_objective_value,
                )
            ):
                break

            elif stopping_criterion in [None, "maxiter"]:
                pass
            else:
                raise ValueError(
                    f"This should never happen! Invalid criterion '{stopping_criterion}'; "
                    f"Please report this!"
                )

            # Get current design
            current_design = OED_results.optimal_design

            if prior_updating:
                # TODO: Visit after `InverseProblem` objects are refactored with `InverseProblemResults` objects...
                map_est = OED_results.inverse_problem_results["mean"]
                post_cov = OED_results.inverse_problem_results["covariance"]

                if covariance_inflation_factor > 1:
                    # TODO: Abhi; need to talk about covariance inflation, then you/we can fix this...
                    raise NotImplementedError(
                        f"Fix this: covariance inflation DOES not work this way. "
                        f"It is spreading out but not magnifying correlations"
                    )
                    post_cov *= covariance_inflation_factor

                op.inverse_problem.prior.update_mean(map_est)
                op.inverse_problem.prior.update_covariance_matrix(post_cov)
            idx += 1

        # Reset the OED problem to its orignal status (prior, posterior, and design)
        op.update_design(passed_data["design"])
        op.inverse_problem.prior.update_mean(passed_data["prior_mean"])
        op.inverse_problem.prior.update_covariance_matrix(
            passed_data["prior_covariance"]
        )

        return SLOED_results

    @property
    def oed_problem(self):
        """
        The OED problem to solve.
        """
        return self.configurations.oed_problem

    @oed_problem.setter
    def oed_problem(self, value):
        self.update_configurations(oed_problem, value)


# TODO: Abhi: Consider refactoring to subclass OEDResults (also update the return type ins SuccessiveLaplaceOED.solve accordingly)
class SLOEDResults:
    """
    Results of a SLA OED problem.
    """

    def __init__(self):
        self._OEDR_iterates = []

    def add_result(self, OEDResult):
        """
        Add a result to the list of results.
        """
        assert isinstance(OEDResult, OEDResults), "Expected an OEDResults instance!"
        # NOTE: We could append `OEDResults.data_dictionary()` instead!
        self._OEDR_iterates.append(OEDResult)

    def plot_results(
        self,
        line_width=2,
        output_dir=None,
        overwrite=True,
        plots_format="pdf",
    ):
        """
        Plot the results of the SLA iterations.
        """
        # TODO: Fix this!
        raise NotImplementedError("Not implemented yet!")
        OEDR_iterates = self.OEDR_iterates
        N_SL = len(OEDR_iterates)
        assert N_SL > 0, "No results to plot!"

        obj_vals = []
        design_vals = []
        for i in range(N_SL):
            obj_vals.append(OEDR_iterates[i].optimal_objective_value)
            design_vals.apppend(
                utility.index_from_binary_state(OEDR_iterates[i].optimal_design)
            )

        fig, ax = plt.subplots()
        ax.set_xlabel("SLA iteration")
        ax.plot(obj_vals, linewidth=line_width, label=r"$J(\\theta(\\zeta))$")
        ax.plot(design_vals, linewidth=line_width, label=r"\\zeta")
        fig.legend()
        fig.savefig("Test.pdf")

    def data_dictionary(self):
        """
        Return a dictionary containing picklable results without class dependencies
        """
        data = [results.data_dictionary() for results in self.OED_Results_iterates]
        return data

    def write_results(self, saveto=None):
        """
        Save the underlying OED results to pickled file
        """
        data = self.data_dictionary()

        if saveto is not None:
            saveto = os.path.abspath(saveto)
            d, f = os.path.split(saveto)
            if not os.path.isdir(d):
                os.makedirs(d)

        print("Results dumped/saved to: {0}".format(saveto))
        try:
            pickle.dump(data, open(saveto, "wb"))
        except:
            print("Failed to write OEDResults; Returning...")
            raise

    @classmethod
    def load_results(cls, readfrom, oed_problem=None):
        """
        Inspect pickled file, and load OED results;
        If `oed_problem` is not None, an instance of :py:class:`OEDResults` is created
        and returned, otherwise the loaded dictionary is returned.
        """
        raise NotImplementedError("TODO")

    @property
    def OED_Results_iterates(self):
        """
        A list of OEDResults instances, one for each SLA iteration.
        """
        return self._OEDR_iterates
