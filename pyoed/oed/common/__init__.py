# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This package provides implementations of OED methods applied to experimental design problems
(for inverse problems) regardless of how the design enters the inverse problems.
"""

from . import (
    successive_laplace_oed,
    # binary_oed,
)

