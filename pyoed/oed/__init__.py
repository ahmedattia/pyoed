# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This package provides implementations of optimal experimental design for inverse problems.
Implementations include general approaches, and implementations tailored to special cases,
such as sensor placement.
"""

from . import (
    core,
    utility_functions,
    robust_oed,
    common,
    sensor_placement,
)


