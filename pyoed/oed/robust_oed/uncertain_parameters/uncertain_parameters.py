# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Abstract classes for uncertain parameters in the robust OED formulations.
"""
import warnings
import re
from abc import ABC, abstractmethod, abstractproperty
from dataclasses import dataclass
from typing import Type
import inspect

import numpy as np
import scipy as sp

from pyoed import utility
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    validate_key,
    set_configurations,
    extract_unknown_keys,
    remove_unknown_keys,
)


@dataclass(kw_only=True, slots=True, )
class ParameterSamplerConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`ParameterSampler` abstract base class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and only adds
    new class-level variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`ParameterSamplerConfigs`
    provides the following fields:

    :param name: name of the parameter (for verbosity if needed)
    :param random_seed: random seed used for pseudo random number generation
    """
    name : str | None = None


@set_configurations(ParameterSamplerConfigs)
class ParameterSampler(PyOEDObject):
    """
    Template class that provides infrastructure for samplers objects to be
    designed to generate sample(s) from the uncertain paetmeter(s), based on a
    prescriped noise system/model, in an inverse (or optimal experimental
    design) problem.  The distribution of the parameter is prescribed by a set
    of hyperparameters defined based on the description of the uncertainty
    model.

    For example, if the uncertain parameter is the prior variance, one can
    describe the uncertainty using a Chi-squared distribution, and the
    parameters of that distribution are the hyperparameters used to set the PDF
    and the sampler.

    For efficiency, one can choose to store the uncertain parameter in a
    compressed format, e.g., if the uncertain parameter is the covariance and is
    assumed to take the form :math:`\\sigma^2 \\mathbf{I}` where
    :math:`\\sigma^2` is a constant variance and :math:`\\mathbf{I}` is a
    diagonal matrix; in such case, one can store only the variance as the
    uncertain parameter, however, it must always be communicated in the full
    format.

    When a class extends this one, a good description of the underlying
    uncertainty model and the hyperparameters SHOULD be given so users and other
    developers understand what's going on.


    .. note::

        Each class derived from `ParameterSampler` should have its own `__init__` method
        in which the constructor just calls `super().__init__(configs=configs)`
        and then add any additiona initialization as needed.
        The validation :py:meth:`self.validate_configurations` is carried out at the
        initialization time by the base class :py:class:`ParameterSampler`.

    :param dict | ParameterSampler | None configs: (optional) configurations for
        the parameter
    """
    def __init__(self, configs: dict | ParameterSamplerConfigs | None = None) -> None:
        super().__init__(
            configs=self.configurations_class.data_to_dataclass(configs),
        )

    def validate_configurations(
        self,
        configs: dict | ParameterSamplerConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each uncertain parameter **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        :param configs: configurations to validate. If a :py:class:`ParameterSamplerConfigs`
            object is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `name`: None or string
        if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="name",
                test=lambda v: isinstance(v, (str, type(None))),
                message=f"ParameterSampler name is of invalid type. Expected None or string. ",
                raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )


    @abstractmethod
    def sample(self, sample_size):
        """
        Generate a samples of size `sample_size` from the predefined distribution
        of this parameter/object

        :param sample_size: size of the sample to generate
        """
        ...

    @abstractmethod
    def evaluate(self, param_val):
        """
        Evaluate the parameter (e.g., the kernel), given the value of the hyperparameter;

        :param param_val: value of the parameter
        :returns:
        """
        ...

    @abstractmethod
    def evaluate_derivative(self, param_val, i=None):
        """
        Evaluate the derivative of the kernel w.r.t to the ith entry of the
        hyperparameter vector, evaluated at the passed hyperparameter value.
        the parameter `i` is ignored if the hyperparameter is a scalar (1D)

        :param param_val: value of the parameter
        :param i: `None` or integer index of the entry to find derivative with respect to.
        :returns:
        """
        ...

    @abstractmethod
    def is_valid(self, hyperparam_val):
        """
        Check validity of the passed hyperparameter value; test whether it is in
        the right shape/type/etc., and that it complies to the predefined bounds
        (if any).

        :param hyperparam_val: value of the hyperparameter
        :returns: `True` if `hyperparam_val` is valid (within the hyperparameter
            bounds), `False` otherwise.
        """
        ...

    @abstractmethod
    def add_noise(self, param_val, std=0.1):
        """
        Add (truncated Gaussian) random perturbation to the passed parameter value

        :param param_val: value of the parameter
        :returns: `True` if `param_val` is valid (within the parameter bounds), `False` otherwise.
        """
        ...

    @abstractproperty
    def size(self):
        """
        A postive integer defining the dimension/size of the parameter.
        """
        ...


def create_sampler(
    sampler: None | ParameterSampler | Type[ParameterSampler] = None,
    sampler_configs: None | dict | ParameterSamplerConfigs | Type[ParameterSamplerConfigs] = None,
    new_configs: None | dict = None,
):
    """
    Given an `sampler` and `sampler_configs` return the sampler with configurations
    as in the passed object.

    The logic:
        1. The sampler:
            1. None: If both `sampler_configs` and `new_configs` are `None` return `None`.
               If any of the configurations is not None, throw a warning and also return `None.
               This is added to enable resetting samplers in objects at instantiation.
            1. instance derived from `ParameterSampler`: use it as is
            2. subclass of `ParameterSampler`: create an instance

        2. The sampler_configs:
            1. None: convert to empty dictionary
            2. dict: remove unknown keys and throw warning if unknown keys found
            3. instance derived from `ParameterSamplerConfigs`: convert to dictionary
            4. subclass of `ParameterSamplerConfigs`: create an instance and convert to dictionary

        3. new_configs (if None replace with empty dictionary):
            1. dict: update/replace known keys into `sampler_configs` and throw warning if
               unknown keys are passed

    The sampler is instantiated (if a class is passed) with the aggregated configurations.
    If valid sampler instance is passed, its configurations are updated with the aggregated
    configurations.

    :raises TypeError: if the passed arguments are of invalid types.
    :raises: an error (type varies) if the sampler failed to instantiate
    """
    if sampler is None:
        if not(sampler_configs is new_configs is None):
            warnings.warn(
                f"The passed sampler is `None`, but some configurations are not `None`!\n"
                f"It does not make sense to do that! \nDiscrding and returning `None`"
            )
        return None

    ## 1. Check the sampler type: (sampler class or sampler object)
    if not (
            isinstance(sampler, ParameterSampler) or
            (inspect.isclass(sampler) and issubclass(sampler, ParameterSampler))
    ):
        raise TypeError(
            f"Invalid {sampler=} of {type(sampler)=}"
        )

    ## 2. Check the sampler_configs type: (None, dictionary, configs class (or instance))
    if sampler_configs is None:
        # None: convert to empty dict
        sampler_configs = {}

    elif inspect.isclass(sampler_configs):
        # Class derived from the base optimzer configs class `ParameterSamplerConfigs`
        if issubclass(sampler_configs, ParameterSamplerConfigs):
            sampler_configs = sampler_configs().asdict()

        else:
            raise TypeError(
                f"Invalid {sampler_configs=} of {type(sampler_configs)=}\n"
                f"If a class is passed, it must be derived from `ParameterSamplerConfigs`!"
            )

    elif isinstance(sampler_configs, ParameterSamplerConfigs):
        # Object that inherits the base optimzer configs class
        sampler_configs = sampler_configs.asdict()

    elif isinstance(sampler_configs, dict):
        pass

    else:
        raise TypeError(
            f"Invalid type of {sampler_configs=} of "
            f"{type(sampler_configs)} is not valid"
        )
    # Now, sampler_configs is a dictionary

    ## 3. new_configs: None or dict
    if new_configs is None: new_configs = {}
    if not isinstance(new_configs, dict):
        raise TypeError(
            f"Invalid {new_configs=} of {type(new_configs)=}; "
            f"expected `None` or `dict`"
        )

    # Get the configurations class associated with the sampler (if any)
    configs_dataclass = sampler.get_configurations_class()
    if configs_dataclass is None:
        warnings.warn(
            f"The passed sampler is not associated with a configurations class; will try to "
            f"blindly aggregate configurations"
        )
        sampler_configs.update(new_configs)

    else:
        # Remove unknown keys from sampler_configs
        unknown_keys = extract_unknown_keys(
            data=sampler_configs,
            target_dataclass=configs_dataclass,
        )
        if len(unknown_keys) > 0:
            warnings.warn(
                f"Unknown keys found in the passed sampler_configs: "
                f"{list(unknown_keys.keys())}"
            )
        sampler_configs = remove_unknown_keys(
            data=sampler_configs,
            target_dataclass=configs_dataclass,
        )

        # Remove unknown keys from new_configs
        unknown_keys = extract_unknown_keys(
            data=new_configs,
            target_dataclass=configs_dataclass,
        )
        if len(unknown_keys) > 0:
            warnings.warn(
                f"Unknown keys found in the passed new_configs: "
                f"{list(unknown_keys.keys())}"
            )
        new_configs = remove_unknown_keys(
            data=new_configs,
            target_dataclass=configs_dataclass,
        )

        # Update sampler configs with new configs
        sampler_configs.update(new_configs)

    ## Instantiate (or update) the sampler
    if isinstance(sampler, ParameterSampler):
        try:
            sampler.update_configurations(**sampler_configs)
        except Exception as err:
            print(
                f"Tried updating the sampler with the passed *aggregated* "
                f"configurations but failed; see the error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

    elif inspect.isclass(sampler) and issubclass(sampler, ParameterSampler):
        try:
            sampler = sampler(sampler_configs)
        except Exception as err:
            print(
                f"Tried creating the sampler with the passed *aggregated* "
                f"configurations but failed; see the error details below: \n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

    else:
        raise TypeError(
            f"Invalid {sampler=} of {type(sampler)=}\n"
            f"THIS SHOULD NEVER HAPPEN: PLEASE REPORT THIS AS A BUG!"
        )

    return sampler



