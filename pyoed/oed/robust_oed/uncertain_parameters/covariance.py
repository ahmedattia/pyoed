# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""Uncertain parameter implementation for Robust OED research"""

from abc import (
    ABC as _ABC,
    abstractmethod as _abstractmethod,
)

import re
import numpy as np
import scipy as sp
import warnings as _warnings
from dataclasses import dataclass, replace
from typing import Iterable, Tuple, Union

from pyoed import utility
from pyoed.optimization.optimization_utils import standardize_domain_bounds
from pyoed.utility.mixins import RandomNumberGenerationMixin
from .uncertain_parameters import (
    ParameterSampler,
    ParameterSamplerConfigs,
)
from pyoed.configs import (
    PyOEDConfigs,
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
    remove_unknown_keys,
)


@dataclass(kw_only=True, slots=True)
class CovarianceSamplerConfigs(ParameterSamplerConfigs):
    """
    Configurations class for the :py:class:`CovarianceSampler` class.  This class
    inherits functionality from :py:class:`ParameterSamplerConfigs` and only
    adds new class-level variables which can be updated as needed.

    See :py:class:`ParameterSamplerConfigs` for more details on the functionality
    of this class along with a few additional fields.
    Otherwise :py:class:`CovarianceSamplerConfigs` provides the following fields:

    :param random_seed: random seed used for pseudo random number generation
    :param name: name of the parameter (for verbosity if needed)
    :param kernel: name of the kernel to be used to define the covariance matrix.

        In this implementation, we assume the variance-covariance matrix is defined
        as a kernel :math:`K(x_i, x_j)` where the specific form of :math:`K`
        depends on the kernel name.

        Currently, in :py:class:`CovarianceSampler`, the following kernels are
        supported (more to be added as needed; e.g., 'RBF', 'matern', ...):

        - `'constant'`: in this case, the kernel is given by:
            :math:`K(x_i, x_j) = \\delta_{i,j} \\sigma^2`, that
            is :math:`K = \\sigma\\mathbf{I}` where :math:`\\sigma` is a vactor holding
            standard deviations.
            In this case, the hyperparameter is the scalar standard deviation
            :math:`\\sigma`.

        - `'diagonal'`: in this case, the kernel is given by:
            :math:`K(x_i, x_j) = \\delta_{i,j} \\sigma_{i}^2`,
            that is :math:`K={\\rm diag}(\\sigma^2\\mathbf{I})` where :math:`\\sigma` is
            a vector holding standard deviations.
            The hyperparameter is :math:`\\sigma`, the square root of the diagonal of
            the kernel, that is the standard deviations vector.

        - `'banded-1'`: here, the kernel is a banded covariance matrix with diagonal
            elements :math:`K(x_i, x_i) := 2+ \\sigma_{i}^2`+\\sigma_{i}`, and off-diagonal
            entries :math:`K(x_i, x_j):=\\delta_{j, i+1}\\sigma_{i}^2 \\sigma_{j}^2`,
            that is only one off diagonal is nonzero and each variable (entry) is
            correlated only with the adjacent ones.
            The hyperparameter is vector :math:`\\sigma`; here it is NOT exactly the
            standard deviations as noted from the definition of the kernel.

    :param size: the size of the parmaeter (variance/covariance matrix dimension);
    :param hyperparameter_def_value: default value of the variance (constant value on
        the diagonal of the variance-covariance matrix)
    :hyperparameter_bounds: lower and upper bound on `hyperparameter_def_value` used for
        sampling; this will be replicated/reshaped based on the size of the
        hyperparameter default values.
        Below are the acceptable formats to define lower bound and upper bounds of
        each entry of the hyperparmeter vector (based on hyperparameter size):

        - `None`: no lower or upper bounds defined on the hyperparameter
        - ``(lb, ub)`` or ``[(lb, ub)]``: all hyperparameter entries are given
          the same lower/upper bounds defined by ``lb``, and ``ub``, respectively.
        - ``[(lb1, ub1), (lb2, ub2), ...]``: entry ``i`` of the hyperparameter
          vector is assigned lower/upper boundary ``(lb, ub)``.
    """

    random_seed: int | None = None
    name: str = "CovarianceSampler: Build and sample parametric covariance matrix"
    kernel: str = "constant"

    size: None | int = None
    hyperparameter_def_value: float | Iterable[float] = 1.0
    hyperparameter_bounds: Union[
        None,
        Tuple[Union[type(None), float], Union[type(None), float]],
        Iterable[Tuple[Union[type(None), float], Union[type(None), float]]],
    ] = (1e-5, 1.0)


@set_configurations(CovarianceSamplerConfigs)
class CovarianceSampler(ParameterSampler, RandomNumberGenerationMixin):
    """
    A class that provides means to sample the variance/covariance (of some
    quantity/parameter/variable) based on the prespecified configurations.

    Specifically, this class provides an implementation of the covariance matrix
    of some parameter having a Gaussian noise. The covariance matrix is
    parameterized by some hyperparmater which enables evaluating, sampling, and
    differentitating the covariance matrix with respect to the hyperparameter.
    Generally, we regard the variance/covariance matrix as a ''parameter'', and
    the entities used to formulate the covariance matrix as the
    ''hyperparameter''.

    :param dict | CovarianceSamplerConfigs | None configs: (optional) configurations for
        the model

    .. note::
        This is not a general-purpose sampler; this merely provides what
        we need for our robust OED research, however, it can be extended to more
        general cases as needed.
    """
    _SUPPORTED_KERNELS = ["constant", "diagonal", "banded-1"]

    def __init__(self, configs: dict | CovarianceSamplerConfigs | None = None) -> None:
        super().__init__(
            configs=self.standardize_configurations(configs),
        )

        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.configurations.random_seed)


    def standardize_configurations(
        self,
        configs: dict | CovarianceSamplerConfigs,
    ) -> CovarianceSamplerConfigs:
        """
        Check the configurations of the error model (kernel, size,
        hyperparameter_def_value, and hyperparameter_bounds ),
        and make sure they are consistent.

        :raises PyOEDConfigsValidationError: if the validation of the configurations fail
        """
        # First Aggregate and convert to the proper (expected) type
        configs = self.configurations_class.data_to_dataclass(configs)

        # Extract Distribution Elements from Configurations:
        size = configs.size
        kernel = configs.kernel
        hyperparameter_def_value = configs.hyperparameter_def_value
        hyperparameter_bounds = configs.hyperparameter_bounds

        # Check the size
        is_positive_int = lambda x: utility.isnumber(x) and (x == int(x)) and (x > 0)
        if is_positive_int(size):
            size = int(size)
        else:
            raise PyOEDConfigsValidationError(
                f"Failed to standardize CovarianceSampler configurations! \n"
                f"{size=} of {type(size)=}is invalid. Expected positive integer "
            )

        # Check kernel
        if not isinstance(kernel, str):
            raise PyOEDConfigsValidationError(
                f"Failed to standardize CovarianceSampler configurations! \n"
                f"Expected 'kernel' to be 'str'; received {kernel=} of {type(kernel)=}"
            )
        valid_kernel = False
        for sk in self.supported_kernels:
            if re.match(rf"{sk}", kernel, re.IGNORECASE):
                kernel = sk
                valid_kernel = True
                break
        if not valid_kernel:
            raise PyOEDConfigsValidationError(
                f"Failed to standardize CovarianceSampler configurations! \n"
                f"{kernel=} is not supported. kernel name must be one of the "
                f"supported kernels: {self.supported_kernels}"
            )

        # Get the expected hyperparameter size based on the kernel
        if kernel == "constant":
            hyperparameter_size = 1
        elif kernel == "diagonal":
            hyperparameter_size = size
        elif kernel == "banded-1":
            hyperparameter_size = size
        else:
            raise PyOEDConfigsValidationError(
                f"THIS IS A BUG; PLEASE REPORT\n"
                f"UNEXPECTED {kernel=} !!!"
            )

        # Check the default hyperparameter value
        hyperparameter_def_value = utility.asarray(hyperparameter_def_value).flatten()
        if hyperparameter_def_value.size == 1 < hyperparameter_size:
            # Replicate to match size (this provides some flexibility)
            hyperparameter_def_value = hyperparameter_def_value.repeat(hyperparameter_size)

        elif hyperparameter_def_value.size != hyperparameter_size:
            raise PyOEDConfigsValidationError(
                f"Failed to standardize CovarianceSampler configurations! \n"
                f"{hyperparameter_def_value=} of {hyperparameter_def_value.size=} "
                f"is not acceptable. Expected scalar (size 1) or 1d array of "
                f"size {hyperparameter_size=}"
            )

        # Check and standardize the hyperparmater_bounds
        try:
            hyperparameter_bounds = standardize_domain_bounds(
                bounds=hyperparameter_bounds,
                size=hyperparameter_size
            )
        except(Exception) as err:
            raise PyOEDConfigsValidationError(
                f"Failed to standardize CovarianceSampler configurations! \n"
                f"The {hyperparameter_bounds=} couldn't be standardized!"
                f"See the error details below:\n"
                f"Unexpected {err=} of {type(err)=}"
            )

        return replace(
            configs,
            size=size,
            kernel=kernel,
            hyperparameter_def_value=hyperparameter_def_value,
            hyperparameter_bounds=hyperparameter_bounds,
        )

    def validate_configurations(
        self,
        configs: dict | CovarianceSamplerConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each distribution **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        :param configs: configurations to validate.
            If a :py:class:`CovarianceSamplerConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # Local tests
        is_bool = lambda x: utility.isnumber(x) and (x == bool(x))
        is_float = lambda x: utility.isnumber(x) and (x == float(x))
        is_positive_float = lambda x: is_float(x) and (x > 0)
        is_float_or_none = lambda x: x is None or is_float(x)
        is_float_iterable = lambda x: utility.isiterable(x) and all([is_float(_) for _ in x])
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        def size_from_kernel(kernel):
            if kernel == "constant":
                size = 1
            elif kernel == "diagonal":
                size = aggregated_configs.size
            elif kernel == "banded-1":
                size = aggregated_configs.size
            else:
                raise ValueError(
                    f"This is a Bug; \n"
                    f"unexpected {self.configurations.kernel=} passed validation! \n"
                    f"Please report!"
                )
            return size

        def is_valid_bounds(bounds):
            size = size_from_kernel(aggregated_configs.kernel)
            try:
                standardize_domain_bounds(bounds=bounds, size=size)
                return True
            except (Exception) as err:
                print(
                    f"\n****\nChecking the bouds failed;\n**** \n\t"
                    f"Unexpected {err=} of {type(err)=}\n****\n"
                )
                return False

        def is_valid_def_val(val):
            """Check size compliance (discard bounds initially...)"""
            size = size_from_kernel(aggregated_configs.kernel)

            # Cast & check size and bounds
            val = utility.asarray(val).flatten()
            return val.size == size

        # `random_seed`: None or int
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda v: v is None or (isinstance(v, int) and v>=0),
            message=f"random_seed must be None or a non-negative integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `kernel`: in the supported kernels
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="kernel",
            test=lambda x: x in self.supported_kernels,
            message=f"kernels is not suppored. Expected one of {self.supported_kernels}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `size`: positive integer
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="size",
            test=is_positive_int,
            message=f"size is invalid. Expected positive integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `hyperparameter_def_value`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="hyperparameter_def_value",
            test=is_valid_def_val,
            message=f"Invalid hyperparameter default value hyperparam_def_val. Expected float or iterable of floats",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `hyperparameter_bounds`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="hyperparameter_bounds",
            test=is_valid_bounds,
            message=f"Invalid hyperparam_bounds",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(
            **self.standardize_configurations(
                self.aggregate_configurations(configs=kwargs)
            ).asdict()
        )

        # Check if "random_seed" is passed; update random number generator
        if "random_seed" in kwargs:
            self.update_random_number_generator(random_seed=kwargs["random_seed"])

    def is_valid(self, hyperparameter_val):
        """
        Check validity of the passed hyperparameter value; test whether it is in
        the right shape/type/etc., and that it complies to the predefined bounds
        (if any).

        :param hyperparameter_val: value of the parameter

        :returns: `True` if `hyperparameter_val` is valid. This is achieved if
            the passed value is of the right size (based on the defined kernel),
            and within the parameter bounds. `False` is returned otherwise.
        """
        hyperparameter_val = utility.asarray(hyperparameter_val).flatten()

        # Check size and bounds
        if hyperparameter_val.size != self.hyperparameter_size:
            return False

        hyperparameter_bounds = utility.asarray(
            standardize_domain_bounds(
                size=self.hyperparameter_size,
                bounds=self.hyperparameter_bounds,
            )
        )

        # compare to bounds
        if any(
                [
                    (v<lb or v>ub)
                    for v, (lb, ub) in zip(
                        hyperparameter_val,
                        hyperparameter_bounds,
                    )
                ]
        ):
            return False
        else:
            return True

    @property
    def size(self):
        """The size of the random variable for which covariance is modeled; this is the size"""
        return self.configurations.size

    @property
    def hyperparameter_size(self):
        """
        Return the expectes size of the hyperparameter (based on the defined kernel)
        """
        kernel = self.configurations.kernel
        if kernel == "constant":
            size = 1
        elif kernel == "diagonal":
            size = self.size
        elif kernel == "banded-1":
            size = self.size
        else:
            raise ValueError(
                f"This is a Bug; \n"
                f"unexpected {self.configurations.kernel=} passed validation! \n"
                f"Please report!"
            )
        return size

    @property
    def hyperparameter_bounds(self):
        """
        List of bounds (lb, ub) of each of the hyperparameter entris.
        """
        return self.configurations.hyperparameter_bounds

    @property
    def hyperparameter_def_value(self):
        """
        Default value of the kernl hyperparmeter
        """
        return self.configurations.hyperparameter_def_value

    @property
    def kernel(self):
        """
        Kernel (name) used for generating covariances given the value of the hyperparameter.
        """
        return self.configurations.kernel

    @property
    def supported_kernels(self, ):
        """
        Return a dictionary holding the names (as keys) of supported kernels
        and the defalut settings of the kernel (as values of the dictionary keys).
        """
        return self._SUPPORTED_KERNELS


    def sample(self, sample_size=1, sparse=True):
        """
        Generate a smaple (a list) of the parameter (variance/covariance
        matrices) of size/length `sample_size`. This generates a sample of the
        hyperparameter based on the predefined bounds in the configurations
        dictionary. The sample is sampled uniformly, and is then used to
        evaluate the covariance matrix, and return it.

        .. note::
            Here, we assume the hyperparamer is uniformly distributed within
            the predefined bounds.

        :param int sample_size: number of sample points (covariance matrices)
        :param bool sparse: the variance-covariance matrices generated are returned
            as sparse matrices, otherwise 2D numpy arrays (dense) are returned

        :returns: both hyperparameter sample and the corresponding parameter sample
            each is an iterable of length eaual to the sample size
            - `hyperparameter_sample`
            - `parameter_sample`

        :remarks: If any of the bounds is undefined (None/inf), the bounds
            it is replaced with a *very* large value; here 1e100.
            This case is not intended and is unreliable, though!
        """
        assert (
            utility.isnumber(sample_size)
            and int(sample_size) == sample_size
            and sample_size > 0
        ), "Invalid sample_size; it must be positive integer!"

        hyperparameter_size = self.hyperparameter_size
        hyperparameter_bounds = utility.asarray(
            standardize_domain_bounds(
                size=self.hyperparameter_size,
                bounds=self.hyperparameter_bounds,
            )
        )

        # If any of the bounds is infinity, replace it with *very* large number
        infval = 1e100
        inf_locs = np.where(np.isinf(hyperparameter_bounds))
        hyperparameter_bounds[inf_locs] = np.sign(hyperparameter_bounds[inf_locs]) * infval

        # Sample the hyperparameter
        hyperparameter_sample = [
            utility.asarray(
                self.random_number_generator.uniform(
                    low=hyperparameter_bounds[:, 0],
                    high=hyperparameter_bounds[:, 1],
                    size=self.hyperparameter_size,
                )
            )
            for _ in range(sample_size)
        ]

        # Construct the parameter sampel (given the sampled hyperparameters)
        parameter_sample = [
            self.evaluate(hp, sparse=sparse) for hp in hyperparameter_sample
        ]

        return hyperparameter_sample, parameter_sample

    def evaluate(self, hyperparameter_val, sparse=True):
        """
        Evaluate the parameter, given the passed value of the hyperparameter
        Here, this method evaluates the covariance matrix/kernel given the
        covariance kernel parameter or hyperparameter

        :param hyperparameter_val: value of the hyperparameter at which
            kernel/covariance matrix is evaluated/constructed
        :param sparse: if `True`, a sparse array is returned (from scipy.sparse),
            otherwise, a dense (numpy) array is returned
        """
        # Validate arguments
        assert self.is_valid(hyperparameter_val), f"The passed {hyperparameter_val=} is invalid!"
        hyperparameter_val = utility.asarray(hyperparameter_val).flatten()

        # Shortname
        size = self.size

        # This conditional is to be updated when more kernels are added
        if self.kernel == "constant":
            if sparse:
                kernel_val = sp.sparse.diags(
                    np.ones(size) * np.power(hyperparameter_val, 2), format="csc"
                )
            else:
                kernel_val = np.diag(
                    np.ones(size) * np.power(hyperparameter_val, 2)
                )

        elif self.kernel == "diagonal":
            if sparse:
                kernel_val = sp.sparse.diags(np.power(hyperparameter_val, 2), format="csc")
            else:
                kernel_val = np.diag(np.power(hyperparameter_val, 2))

        elif self.kernel == "banded-1":
            diag_vals = 2 + np.power(hyperparameter_val, 2) + hyperparameter_val
            off_diag_vals = 2 * hyperparameter_val[:-1] * hyperparameter_val[1:]
            kernel_val = sp.sparse.diags(
                [diag_vals, off_diag_vals, off_diag_vals],
                offsets=[0, 1, -1],
                format="csc",
            )
            if not sparse:
                kernel_val = kernel_val.toarray()

        else:
            raise ValueError(
                f"This is a BUG; Please report!\n"
                f"Unsupported (Co)variance kernel {kernel_name}"
            )

        return kernel_val

    def evaluate_derivative(self, hyperparameter_val, i, sparse=True):
        """
        Evaluate the derivative of the kernel/parameter w.r.t to the ith entry
        of the hyperparameter vector, evaluated at the passed hyperparameter
        value.

        :param hyperparameter_val: value of the hyperparameter at which
            gradient/derivative is evaluated
        :param bool sparse: the variance-covariance matrices generated are
            returned as sparse matrices, otherwise 2D numpy arrays (dense) are returned
        """
        # Validate arguments
        assert self.is_valid(hyperparameter_val), "The passed parameter value is invalid!"
        assert (
            0 <= i < self.hyperparameter_size
        ), f"index 'i' must be an integer in [0, {self.hyperparameter_size-1}]"
        hyperparameter_val = utility.asarray(hyperparameter_val).flatten()

        # Shortname
        size = self.size
        hyperparameter_size = self.hyperparameter_size

        # This conditional is to be updated when more kernels are added
        if self.kernel == "constant":
            if sparse:
                kernel_val = sp.sparse.diags(
                    np.ones(size) * 2.0 * hyperparameter_val, format="csc"
                )
            else:
                kernel_val = np.diag(np.ones(size) * 2.0 * hyperparameter_val)

        elif self.kernel == "diagonal":
            if sparse:
                kernel_val = sp.sparse.lil_matrix((size, size))
                kernel_val[i, i] = 2.0 * hyperparameter_val[i]
                kernel_val = kernel_val.tocsc()
            else:
                kernel_val = np.zeros((size, size))
                kernel_val[i, i] = 2.0 * hyperparameter_val[i]

        elif self.kernel == "banded-1":
            kernel_val = sp.sparse.lil_matrix((size, size))
            if i == 0:
                # First index
                kernel_val[i, i] = 2 * hyperparameter_val[i] + 1
                kernel_val[i, i + 1] = 2 * hyperparameter_val[i + 1]
                kernel_val[i + 1, i] = 2 * hyperparameter_val[i + 1]

            elif i == hyperparameter_size - 1:
                # Last index
                kernel_val[i - 1, i] = 2 * hyperparameter_val[i - 1]
                kernel_val[i, i - 1] = 2 * hyperparameter_val[i - 1]
                kernel_val[i, i] = 2 * hyperparameter_val[i] + 1
            else:
                # all others
                kernel_val[i - 1, i] = 2 * hyperparameter_val[i - 1]
                kernel_val[i, i - 1] = 2 * hyperparameter_val[i - 1]
                kernel_val[i, i] = 2 * hyperparameter_val[i] + 1
                kernel_val[i, i + 1] = 2 * hyperparameter_val[i + 1]
                kernel_val[i + 1, i] = 2 * hyperparameter_val[i + 1]

            if sparse:
                kernel_val = kernel_val.tocsc()
            else:
                kernel_val = kernel_val.toarray()

        else:
            raise ValueError(
                f"This is a BUG; Please report!\n"
                f"Unsupported (Co)variance kernel {kernel_name}"
            )

        return kernel_val

    def add_noise(self, hyperparameter_val, std=0.1):
        """
        Add (truncated Gaussian) random perturbation to the passed hyperparameter
        value with noise sampled from the predefined bounds (if any))

        :param param_val: value of the hyperparameter
        """
        assert self.is_valid(hyperparameter_val), "The passed parameter value is invalid!"
        hyperparameter_val = utility.asarray(hyperparameter_val).flatten()
        bounds = utility.asarray(self.hyperparameter_bounds)

        # Sample and clip (if bounds are available)
        out = hyperparameter_val + self.random_number_generator.normal(
            size=self.hyperparameter_size,
            scale=std
        )

        # Trim if needed
        bel_flg = out < bounds[:, 0]
        exc_flg = out > bounds[:, 1]
        out[bel_flg] = bounds[bel_flg, 0]
        out[exc_flg] = bounds[bel_flg, 1]

        return out

