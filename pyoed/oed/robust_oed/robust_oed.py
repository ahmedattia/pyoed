# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Approaches to solving the robust sensor placement OED problem
assuming a parameterized uncertainty (robustness) model.
This is the backbone of the results published in:

1. Ahmed Attia, Sven Leyffer, and Todd S. Munson.
   "Robust A-optimal experimental design for Bayesian inverse problems."
   arXiv preprint arXiv:2305.03855 (2023).
"""

# import copy
import os
import re
import inspect
import numpy as np
import warnings
import pickle
from contextlib import ContextDecorator
from dataclasses import dataclass
from typing import Type
import matplotlib.pyplot as plt

from pyoed import utility
from pyoed.configs import (
    PyOEDConfigs,
    validate_key,
    set_configurations,
)
from pyoed.utility.mixins import RandomNumberGenerationMixin
from pyoed.stats.distributions.bernoulli import Bernoulli
from pyoed.assimilation import (
    Filter,
    Smoother,
)
from pyoed.optimization import (
    create_optimizer,
    Optimizer,
    OptimizerConfigs
)
from pyoed.optimization.binary_optimization import (
    BinaryReinforceOptimizer,
    RobustBinaryReinforceOptimizer,
    RobustBinaryReinforceOptimizerConfigs,
    RobustConstrainedBinaryReinforceOptimizer,
    RobustConstrainedBinaryReinforceOptimizerConfigs,
)
from pyoed.optimization import optimization_utils as opt_utils
from pyoed.models.error_models import Gaussian
from pyoed.oed import utility_functions as uf
from pyoed.oed.core import (
    Criterion,
    RobustInversionOED,
    RobustInversionOEDConfigs,
    RobustInversionOEDResults,
)
from .uncertain_parameters import (
    create_sampler,
    ParameterSampler,
    ParameterSamplerConfigs,
    CovarianceSampler,
    CovarianceSamplerConfigs,
)



# NOTE: Now, that we have `pyoed.oed.sensor_placement.sensor_placement_bayesian_oed,
#       the implementation here should derive such functionality (I think!)!`
@dataclass(kw_only=True, slots=True)
class RobustSensorPlacementBayesianInversionOEDConfigs(RobustInversionOEDConfigs):
    """
    Configurations class for the :py:class:`RobustSensorPlacementBayesianInversionOED` class.
    This class inherits functionality from :py:class:`InversionOEDConfigs` and only adds
    the class-level variables below which can be updated as needed:

    :param optimizer: the optimization routine (optimizer) to register.
        This can be one of the following:

        - An optimizer instance (object that inherits :py:class`Optimizer`).
          In this case, the optimizer is registered **as is** and is updated
          with the passed configurations if available.
        - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
          the optimizer.
          To see available *built-in* optimization routines, see/call
          :py:meth:`show_supported_optimizers`.
        - The name of the optimizer (`str`). This has to match the name of one
          of the available optimization routine.
          To see available *built-in* optimization routines, see/call
          :py:meth:`show_supported_optimizers`.

    :param optimizer_configs: the configurations of the optimization routine.
        This can be one of the following:

        - `None`, in this case configurations are discarded, and whatever default configurations
          of the selected/passed optimizer are employed.
        - A `dict` holding full/partial configurations of the selected optimizer.
          These are either used to instantiate or update the optimizer configurations based on
          the type of the passed `optimizer`.
        - A class providing implementations of the configurations (this must be a subclass of
          :py:class:`OptimizerConfigs`.
        - An instance of a subclass of :py:class:`OptimizerConfigs` which is to set/udpate
          optimizer configurations.

    :param random_seed: random seed to be used to globally set seeds of the underlying
        random number generators.
        This affects both the optimizer and the uncertain parameter sampler (if accepting
        random seed configuration).

    :param uncertain_parameter: name of the uncertain parameter.
        Currently the following parameters are supported by
        :py:class:`RobustSensorPlacementBayesianInversionOED`:

          - `'observation-variance'`: the uncertain parameter is a *hyperparameter* that
            defines the observation error covariance matrix
          - `'prior-variance'`: the uncertain parameter is a *hyperparameter* that
            defines the prior covariance matrix

    :param uncertain_sampler_sampler: A sampler object (derived from :py:class:`ParameterSampler`)
        that enables evaluating and sampling the uncertain parameter given a hyperparameter value.
    :param uncertain_parameter_initial_sample_size: The sample size used to create an initial sample of the
        uncertain parameter which is passed to the robust optimizer.
    """
    criterion: None | str | Criterion = 'Robust Bayesian A-opt'
    optimizer : str | Optimizer | Type[Optimizer] = RobustBinaryReinforceOptimizer
    optimizer_configs : None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None
    random_seed: int | None = None
    uncertain_parameter: str = "observation-variance"
    uncertain_parameter_sampler: str | ParameterSampler | Type[ParameterSampler] = "CovarianceSampler"
    uncertain_parameter_sampler_configs: None | dict | ParameterSamplerConfigs | Type[ParameterSamplerConfigs] = None
    uncertain_parameter_initial_sample_size: int = 10


@set_configurations(RobustSensorPlacementBayesianInversionOEDConfigs)
class RobustSensorPlacementBayesianInversionOED(RobustInversionOED, RandomNumberGenerationMixin):
    """
    This is the main implementation of researech development on robust OED
    presented in :ref:`[1] <[1]>`.

    This formulation of robust OED considers an optimal design as the one that
    maximizes (minimizing) a predefined utility function against works case
    scenario posed by some uncertain parameter. The optimization problem is thus
    defined as a max-min (or min-max) where the outer optimization problem is
    optimizes (maximize/minimize) the utility with respect (w.r.t.) to the design,
    and the inner problem optimizes (minimizes/maximizes) the utility function
    w.r.t. the uncertain parameter.

    The parent class :py:class:`BinaryOED` is general as it applies to any
    registered criterion with binary design (regardless of how it affects
    the inverse problem) or utility function
    (see :py:class:`pyoed.oed.utility_functions.UtilityFunction`).
    This implementation, on the other hand, specializes the design to the case
    of sensor placement, and thus the design is binary by definition, and the
    criterion (utility function) is created upon instantiation based on the passed
    configurations.
    The uncertain parameter can be technically anythin parameter in the inverse
    problem (prior mean, prior variance, observation noise parameters, etc.).
    Though, the user must provide (in the utility function implementation) proper
    gradients (of the utility function with respect to the uncertain parameter).


    **References:**

    .. _[1]:

    1. Ahmed Attia, Sven Leyffer, and Todd S. Munson.
       "Robust A-optimal experimental design for Bayesian inverse problems."
       arXiv preprint arXiv:2305.03855 (2023).
    """
    # Hard-coded optimizers we support out of the box
    _SUPPORTED_OPTIMIZERS = [
        "RobustBinaryReinforceOptimizer",
        "RobustConstrainedBinaryReinforceOptimizer",
    ]
    _SUPPORTED_UNCERTAIN_PARAMETERS = [
        "observation-variance",
        "prior-variance",
    ]
    _SUPPORTED_UNCERTAIN_PARAMETER_SAMPLERS = [
        "CovarianceSampler",
    ]

    def __init__(self, configs: RobustSensorPlacementBayesianInversionOEDConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Update the design
        if self.design is None:
            self.update_design(self.inverse_problem.observation_operator.design)

        ## Add Internally preserved (local) variables
        self.configurations.uncertain_parameter_sampler = self.register_uncertain_parameter_sampler(
            sampler=self.configurations.uncertain_parameter_sampler,
            sampler_configs=self.configurations.uncertain_parameter_sampler_configs,
            update_optimizer=False,
        )
        ## Register elements properly
        # The ptimization routine
        self.configurations.optimizer = self.register_optimizer(
            optimizer=self.configurations.optimizer,
            optimizer_configs=self.configurations.optimizer_configs,
        )

        # Random seed
        self.update_random_number_generators(self.configurations.random_seed)

        # Additionals
        self._OBJECTIVE_SCALING_FACTOR = 1.0  # Scales (multiplies by) result of the objective function and its derivatives
        self._OBJECTIVE_OFFSET = 0  # Shifts (adds to) result of the objective function

    def validate_configurations(
        self,
        configs: dict | RobustSensorPlacementBayesianInversionOEDConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`RobustSensorPlacementBayesianInversionOEDConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # Local tests
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        def is_valid_sampler(sampler):
            if inspect.isclass(sampler):
                return issubclass(sampler, ParameterSampler)
            elif isinstance(sampler, ParameterSampler):
                return True
            elif isinstance(sampler, str):
                return sampler in self.supported_uncertain_parameter_samplers
            else:
                return False

        def is_valid_optimizer(optimizer):
            if inspect.isclass(optimizer):
                return issubclass(optimizer, Optimizer)
            elif isinstance(optimizer, Optimizer):
                return True
            elif isinstance(optimizer, str):
                return optimizer in self.supported_optimizers
            else:
                return False

        # `random_seed`: None or int>=0
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda v: v is None or (isinstance(v, int) and v>=0),
            message=f"random_seed must be None or a non-negative integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `optimizer`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="optimizer",
            test=is_valid_optimizer,
            message=f"Invalid optimizer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `uncertain_parameter`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="uncertain_parameter",
            test=lambda x: x in self.supported_uncertain_parameters,
            message=f"Invalid uncertain_parameter; Expected one of {self.supported_uncertain_parameters}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `uncertain_parameter_sampler`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="uncertain_parameter_sampler",
            test=is_valid_sampler,
            message=f"Invalid uncertain_parameter_sampler; str or instance/type of ParameterSampler",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # `uncertain_parameter_initial_sample_size`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="uncertain_parameter_initial_sample_size",
            test=is_positive_int,
            message=f"Invalid uncertain_parameter_initial_sample_size. Expected positive int",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Check if "random_seed" is passed; update random number generator
        if "random_seed" in kwargs:
            self.update_random_number_generators(random_seed=kwargs["random_seed"])

        # Update uncertain parameter
        if "uncertain_parameter" in kwargs:
            if "uncertain_parameter_sampler" not in kwargs:
                warnings.warn(
                    f"Updating the uncertain parameter without passing a new uncertain parameter sampler "
                    f"will likely result in inconsistency!\n"
                    f"If this the intended behavior, feel free to ignore this warning"
                )

        if "uncertain_parameter_sampler" in kwargs:
            self.register_uncertain_parameter_sampler(
                samplerer=kwargs["uncertain_parameter_sampler"],
                sampler_configs=kwargs.get("uncertain_parameter_sampler", None),
            )
        elif "uncertain_parameter_sampler_configs" in kwargs:
            uncertain_parameter_sampler_configs = kwargs["uncertain_parameter_sampler_configs"]
            if isinstance(uncertain_parameter_sampler_configs, dict):
                pass
            elif isinstance(uncertain_parameter_sampler_configs, ParameterSamplerConfigs):
                uncertain_parameter_sampler_configs = uncertain_parameter_sampler_configs.asdict()
            else:
                raise TypeError(
                    f"Updating uncertain parameter confiurations requires passing a dictionary or instance "
                    f"of proper Configs class that the uncertain parameter sampler accepts (here "
                    f"{self.uncertain_parameter_sampler.configurations_class}).\n"
                    f"Passed {uncertain_parameter_sampler_configs=} of "
                    f"{type(uncertain_parameter_sampler_configs)}"
                )
            self.uncertain_parameter_sampler.update_configurations(**uncertain_parameter_sampler_configs)

        if "optimizer" in kwargs:
            self.register_optimizer(
                optimizer=kwargs["optimizer"],
                optimizer_configs=kwargs.get("optimizer", None),
            )
        elif "optimizer_configs" in kwargs:
            optimizer_configs = kwargs["optimizer_configs"]
            if isinstance(optimizer_configs, dict):
                pass
            elif isinstance(optimizer_configs, OptimizerConfigs):
                optimizer_configs = optimizer_configs.asdict()
            else:
                raise TypeError(
                    f"Updating optimizer confiurations requires passing a dictionary or instance "
                    f"of proper Configs class that the optimizer accepts (here "
                    f"{self.optimizer.configurations_class}).\n"
                    f"Passed {optimizer_configs=} of {type(optimizer_configs)}"
                )
            self.optimizer.update_configurations(**optimizer_configs)

        if "uncertain_parameter_initial_sample_size" in kwargs:
            self.optimizer.update_configurations(
                uncertain_parameter_sample=self.uncertain_parameter_sampler.sample(
                    sample_size=self.configurations.uncertain_parameter_initial_sample_size,
                )[0],
            )

    def register_optimizer(
        self,
        optimizer: str | Optimizer | Type[Optimizer] = "RobustBinaryReinforceOptimizer",
        optimizer_configs: None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None,
    ) -> Optimizer:
        """
        Register (and return) an optimization routine, and make sure the
        objective function in the optimization routine is set to the objctive function of
        this objective :py:meth:`objective_function_value`.
        The objective function of the optimizer (and its derivatives) are set to the
        objective function and derivatives implemented here **unless the objective function is
        passed explicitly in the configurations**.
        This method sets a reference to the optimizer and update the underlying configurations
        accordingly.

        .. note::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            and valid configurations `optimizer_configs` is passed, the optimizer is
            updated with the passed configurations by calling
            `optimizer.update_configurations(optimizer_configs)`.

        .. warning::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            the responsibility is on the developer/user to validate the contents of the
            optimizer.

        :param optimizer: the optimization routine (optimizer) to register.
            This can be one of the following:

            - An optimizer instance (object that inherits :py:class`Optimizer`).
              In this case, the optimizer is registered **as is** and is updated
              with the passed configurations if available.
            - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
              the optimizer.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.
            - The name of the optimizer (`str`). This has to match the name of one
              of the available optimization routine.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.

        :param optimizer_configs: the configurations of the optimization routine.
            This can be one of the following:

            - `None`, in this case configurations are discarded, and whatever default
              configurations of the selected/passed optimizer are employed.
            - A `dict` holding full/partial configurations of the selected optimizer.
              These are either used to instantiate or update the optimizer configurations
              based on the type of the passed `optimizer`.
            - A class providing implementations of the configurations (this must be
              a subclass of :py:class:`OptimizerConfigs`.
            - An instance of a subclass of :py:class:`OptimizerConfigs` which is to
              set/udpate optimizer configurations.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer and/or configurations
            are/is not supported
        """
        if isinstance(optimizer, str) and optimizer in self.supported_optimizers:
            # Convert class name to class
            optimizer = eval(optimizer)

        # Create a dictionary with configurations to be forced
        new_configs = {
            "fun": self.objective_function_value,
            "fun_grad_design": self.objective_function_grad_design,
            "fun_grad_parameter": self.objective_function_grad_parameter,
            "size": self.design_size,
            "uncertain_parameter_size": self.uncertain_parameter_size,
            "uncertain_parameter_bounds": self.uncertain_parameter_bounds,
            "uncertain_parameter_sample": self.uncertain_parameter_sampler.sample(
                sample_size=self.configurations.uncertain_parameter_initial_sample_size,
            )[0],
        }
        print("????????????")
        print(self.configurations)
        print(self.oed_criterion.configurations)
        print(self.uncertain_parameter_sampler.configurations)
        print(f"{new_configs=}" )
        try:
            optimizer = create_optimizer(
                optimizer=optimizer,
                optimizer_configs=optimizer_configs,
                new_configs=new_configs,
            )
        except Exception as err:
            raise TypeError(
                "Failed to create/update the optimizer! "
                f"See the error below for additional details\n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

        # Update optimizer and configurations properly
        return super().register_optimizer(
            optimizer=optimizer,
        )

    def register_uncertain_parameter_sampler(
        self,
        sampler: str | ParameterSampler | Type[ParameterSampler] = "CovarianceSampler",
        sampler_configs: None | dict | ParameterSamplerConfigs | Type[ParameterSamplerConfigs] = None,
        update_optimizer=True,
    ) -> Type[ParameterSampler]:
        """
        Register (and return ) the uncertain parameter sampler and update the optimizer
        settings if available.
        """
        if isinstance(sampler, str) and sampler in self.supported_uncertain_parameter_samplers:
            # Convert class name to class
            sampler = eval(sampler)

        if isinstance(sampler, ParameterSampler) and sampler_configs is None:
            sampler_configs = sampler.configurations

        # Create a dictionary with configurations to be forced
        new_configs = {
            "size": self.inverse_problem.observation_error_model.size,
            "verbose": self.verbose,
        }

        try:
            sampler = create_sampler(
                sampler=sampler,
                sampler_configs=sampler_configs,
                new_configs=new_configs,
            )
        except Exception as err:
            raise TypeError(
                "Failed to create/update the uncertain parameter sampler! "
                f"See the error below for additional details\n"
                f"Unexpected {err=} of {type(err)=}"
            )

        # Update sampler and configurations
        self.configurations.uncertain_parameter_sampler = sampler
        self.configurations.uncertain_parameter_sampler_configs = sampler.configurations

        if update_optimizer:
            optimizer = self.optimizer
            uncertain_parameter_sample, _ = sampler.sample(
                sample_size=self.configurations.uncertain_parameter_initial_sample_size,
            )
            try:
                optimizer.update_configurations(
                    uncertain_parameter_size=sampler.hyperparameter_size,
                    uncertain_parameter_bounds=sampler.hyperparameter_bounds,
                    uncertain_parameter_sample=uncertain_parameter_sample,
                )
            except Exception as err:
                raise TypeError(
                    "Failed to update the optimizer's configurations! "
                    f"See the error below for additional details\n"
                    f"Unexpected {err=} of {type(err)=}"
                )

        return sampler

    def update_design(self, design):
        """
        Update the components of the OED (and inverse) problem with the passed
        `design`. This method updates both observation operator and observation
        error model of the underlying inverse problem with the passed design.

        .. note::
            This method need to be replaced with design-specific impelentation
            for any design different from observational experimental design.

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.

        """
        if design is not None:
            # Assertion on design
            design = utility.asarray(design)
            assert design.ndim == 1, "design must be castable to 1d array"

            # Design of the observation error model (boolean for binary or float for relaxed)
            try:
                self.inverse_problem.observation_error_model.design = design
            except Exception as err:
                print(
                    f"Tried updating the experimental design of the observation error model\n"
                    f"The error model is {self.inverse_problem.observation_error_model=}\n"
                    f"Unexpected {error=} or {type(err)=}"
                )
                raise

            # Design of the observation operator (boolean, but the operator will take care of type)
            try:
                self.inverse_problem.observation_operator.design = design
            except Exception as err:
                print(
                    f"Tried updating the experimental design of the observation operator\n"
                    f"The error model is {self.inverse_problem.observation_operator=}\n"
                    f"Unexpected {error=} or {type(err)=}"
                )
                raise

        # Call super to update the design attribute
        return super().update_design(design)


    def update_random_number_generators(self, random_seed):
        """
        Update random seeds of underlying random number gnerators
        (both local and optimizer if supported)
        """
        self.update_random_number_generator(random_seed)
        try:
            self.optimizer.update_configurations(random_seed=random_seed)
        except Exception as err:
            print(
                f"Expected the optimizer to support random seed update\n"
                f"Ignoring {err=} of {type(err)=}"
            )
            pass

    def register_optimality_criterion(
            self,
            criterion: Criterion | str | None = None,
    ) -> None:
        """
        Validate and set the **robust** OED optimality criterion as described by
        `criterion`, update the configurations object with the criterion, and return the new
        criterion.

        .. note::
            This method only creates a valid criterion (instance of :py:class`Criterion` if
            a string is passed (name of the criterion)), and then calls super class (parent)
            to set the criterion.
            For additional details see e.g.,
            :py:class:`pyoed.oed.InversionOED.register_optimality_criterion`
        """
        # Check the criterion and if legacy (string) is passed, replace it with utility
        # function objection
        if isinstance(criterion, str):
            criterion = uf.find_criteria(criterion)
            criterion = criterion(configs={"oed_problem": self})

        # Pass up to put the criterion in the right place
        return super().register_optimality_criterion(criterion=criterion)

    def uncertain_parameter_manager(self, uncertain_parameter_val=None, uncertain_hyperparameter_val=None):
        """
        Creat an return a context manager that enables updating the
        uncertain parameter to the passed value, execute needed code and then
        reset the uncertain parameter.

        Assuming `obj` is this object, and `val` is the uncertain parameter value
        one wants to run the method `obj.run_code()`, the following code can be used

        .. code-block::

            with obj.uncertain_parameter_manager(val) as mngr:
                mngr.run_code()

        :returns: a reference to `self` that enables calling any function under
            `self` and then automatically, the uncertain parameter is reset.
        """
        if uncertain_parameter_val is uncertain_hyperparameter_val is None:
            raise TypeError(
                f"Either the either uncertain_parameter_val or "
                f"uncertain_hyperparameter_val must be passed; both are None!"
            )

        elif not (uncertain_parameter_val  is None or uncertain_hyperparameter_val is None):
            raise TypeError(
                f"Only uncertain_parameter_val or "
                f"uncertain_hyperparameter_val is allows; Can't pass both!"
            )
        elif uncertain_parameter_val is None:
            uncertain_parameter_val = self.uncertain_parameter_sampler.evaluate(
                    uncertain_hyperparameter_val
                )
        else:
            # given the uncertain parameter val
            uncertain_parameter_val = utility.asarray(uncertain_parameter_val)

        # NOTE: This can be moved outside this method if needed...
        class UncertainParameterManager(ContextDecorator):
            def __init__(self, oed_problem, uncertain_parameter_val):
                # Keep track of the OED problem and the uncertain parameter
                self.oed_problem = oed_problem
                self.uncertain_parameter_val = uncertain_parameter_val

                # Currently supported parameters that we will need to revert
                self.prior_cov = None
                self.obs_err_cov = None

                # Call super's __init__
                super().__init__()

            def __enter__(self):
                ip = self.oed_problem.inverse_problem
                if self.oed_problem.uncertain_parameter_name == "prior-variance":
                    # Get current parameter(s)
                    self.prior_cov = ip.prior.unweighted_covariance_matrix()
                    # Set passed parameter
                    ip.prior.update_covariance_matrix(
                       uncertain_parameter_val
                    )

                elif self.oed_problem.uncertain_parameter_name == "observation-variance":
                    ip = self.oed_problem.inverse_problem
                    # Get current parameter(s)
                    self.obs_err_cov = ip.observation_error_model.unweighted_covariance_matrix()
                    # Set passed parameter
                    ip.observation_error_model.update_covariance_matrix(
                        uncertain_parameter_val
                    )
                return self.oed_problem

            def __exit__(self, *exc):
                # Reset Original parameter(s)
                ip = self.oed_problem.inverse_problem
                if self.obs_err_cov is not None:
                    ip.observation_error_model.update_covariance_matrix(self.obs_err_cov)
                if self.prior_cov is not None:
                    ip.prior.update_covariance_matrix(self.prior_cov)

        return UncertainParameterManager(
            oed_problem=self,
            uncertain_parameter_val=uncertain_parameter_val,
        )

    def set_uncertain_parameter(self, uncertain_parameter_val, uncertain_hyperparameter_val=None, ):
        """
        Update the inverse problem based on the passed value of the uncertain parameter.

        .. warning::
            Currently the uncertain parameter (the current uncertain parameter value) is unrecoverable.
            This means, that the passed `uncertain_parameter_val` takes place, and the current uncertain
            parameter value in the inverse problem is lost.
        """
        warnings.warn(
            f"Calling this method permanently sets the uncertain parameter. "
            f"The current uncertain parameter value is UNRECOVERABLE."
            f"If reverting to the original uncertain parameter is required, we "
            f"recommen employing the context manager given by the method `uncertain_parameter_manager`"
        )

        if uncertain_parameter_val is uncertain_hyperparameter_val is None:
            raise TypeError(
                f"Either the either uncertain_parameter_val or "
                f"uncertain_hyperparameter_val must be passed; both are None!"
            )

        elif not (uncertain_parameter_val  is None or uncertain_hyperparameter_val is None):
            raise TypeError(
                f"Only uncertain_parameter_val or "
                f"uncertain_hyperparameter_val is allows; Can't pass both!"
            )
        elif uncertain_parameter_val is None:
            uncertain_parameter_val = self.uncertain_parameter_sampler.evaluate(
                    uncertain_hyperparameter_val
                )
        else:
            # given the uncertain parameter val
            uncertain_parameter_val = utility.asarray(uncertain_parameter_val)


        # Update/Check the uncertain parameter
        if self.uncertain_parameter_name == "prior-variance":
            self.inverse_problem.prior.update_covariance_matrix(uncertain_parameter_val)
        elif self.uncertain_parameter_name == "observation-variance":
            self.inverse_problem.observation_error_model.update_covariance_matrix(uncertain_parameter_val)
        else:
            raise ValueError(
                f"The passed parameter '{self.uncertain_parameter_name}' is not"
                " recognized or *NOT-YET* supported!"
            )

    def objective_function_value(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=True,
    ):
        """
        Evaluate the value of the OED objective function (regularized utility
        function) given the passed observational design and the value(s) of the
        uncertain parameter; this is similar to
        :py:meth:`BinaryOED.objective_function`, where the uncertainty parameter is
        updated first, then the objective is calculated, and then the uncertain
        parameter is reset to the original if needed

        :param design: an observational design vector conformable with the observation
            operator and the observation error covariance matrix operator.
            This method accepts a design that is either ``binary`` or ``relaxed`` (over the
            interval [0, 1]), and uses the design to properly relax the OED optimization
            objective (with the right limits at the boundary).
        :param uncertain_parameter_val: value of the uncertain parameter; this must match
            the registered uncertain parameter and sampler.
        :param bool reset_parameter_val: if `True`, the current values of the uncertain parameter
            are restored after evaluating the objective function value

        :returns: value of the *registered* OED optimality criterion

        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter` is registered.
        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter_sampler` is
            registered with a valid type. Only instances of :py:class:`ParameterSampler`
            are accepted.
        :raises: :py:class:`TypeError` is raised if no optimality criterion has been
            registered yet
        :raises: :py:class:`TypeError` is raised if the either the uncertain parameter
            or the associated sampler is missing.
        """
        if self.oed_criterion is None:
            raise TypeError(
                f"No optimality criterion has been registered yet! Use"
                f" `register_optimality_criterion()`"
            )

        # Retrieve the underlying inverse problem and update the uncertain parameter
        inverse_problem = self.inverse_problem

        # Retrieve and check the uncertain parameter and the sampler
        uncertain_parameter_name = self.uncertain_parameter_name
        if uncertain_parameter_name is None:
            raise TypeError(
                "No uncertain parameter is registered! use"
                " `register_uncertain_parameter()`"
            )
        uncertain_parameter_sampler = self.uncertain_parameter_sampler
        if not isinstance(uncertain_parameter_sampler, ParameterSampler):
            raise TypeError(
                "The registered sampler is not valid! Expected an instance of"
                f" `ParameterSampler` but found {type(uncertain_parameter_sampler)}'."
                " Use `register_uncertain_parameter()` to register a valid sampler."
            )

        # Evaluate the criterion/utility function objective value
        obj_val = self.oed_criterion.evaluate(
            design,
            uncertain_parameter_val,
            reset_parameter_val=reset_parameter_val,
        )

        # Check if artificial scaling is registered
        if self.objective_function_scaling_factor is not None:
            obj_val *= self.objective_function_scaling_factor
        if self.objective_function_offset is not None:
            obj_val += self.objective_function_offset

        # Update objective with penalty/regularization term (if registered)
        if self.penalty_weight not in [0, None]:
            obj_val += self.penalty_function(design) * self.penalty_weight

        # Return the objective value
        return obj_val

    def objective_function_grad_parameter(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=True,
    ):
        """
        Evaluate the gradient of the :py:meth:`objective_function` (regularized utility/criterion)
        with respect to the uncertainty hyperparameter. The `design` is set and is
        regarded as a constant of course.

        :param design: an observational design vector conformable with the observation operator
            and the observation error covariance matrix operator
        :param uncertain_parameter_val: value of the uncertain parameter; this must match the
            registered uncertain parameter and sampler.
        :param bool reset_parameter_val: if `True`, the current values of the uncertain parameter
            are restored after evaluating the objective function value
        :param bool verbose:

        :returns: gradient of the *registered* OED optimality criterion
            (penalized based on the registered penalty function and parameter),
            with respect to the uncertainty parameter

        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter_name` is registered.
        :raises: :py:class:`TypeError`  is raised if no `uncertain_parameter_sampler` is
            registered with a valid type. Only instances of :py:class:`ParameterSampler`
            are accepted.
        :raises: :py:class`TypeError` is raised if penalty function/term is not properly set.
        :raises: :py:class`TypeError` is raised if no valid optimality `criterion` object is
            set in the configurations dictionary.
        :raises: :py:class`TypeError` is raised if a `criterion` is registerd but it does not
            provide a :py:meth:grad_uncertainty` function.
        """
        # Retrieve the underlying inverse problem and update the uncertain parameter
        ip = self.inverse_problem

        # Verify the OED criterion (utility function) is properly set to A-opt
        criterion = self.oed_criterion
        if criterion is None:
            raise TypeError(
                f"You must register/set an OED optimality criterion first. Use/Call"
                f" register_optimality_criterion()"
            )
        elif not hasattr(criterion, "grad_uncertainty"):
            raise TypeError(
                f"The provided criterion {type(criterion)=} does not provide access to"
                f" derivative with respect to design!\n{criterion.__class__} needs to"
                " implement a method `grad_uncertainty` to be utilized here!"
            )
        else:
            # all good!
            pass

        # Retrieve and check the uncertain parameter and the sampler
        uncertain_parameter_name = self.uncertain_parameter_name
        uncertain_parameter_sampler = self.uncertain_parameter_sampler
        if uncertain_parameter_name is None:
            raise TypeError(
                "No uncertain parameter is registered! use"
                " `register_uncertain_parameter()`"
            )
        if not isinstance(uncertain_parameter_sampler, ParameterSampler):
            raise TypeError(
                "The registered sampler is not valid! Expected an instance of"
                f" `ParameterSampler` but found {type(uncertain_parameter_sampler)}'."
                " Use `register_uncertain_parameter()` to register a valid sampler."
            )

        # Evaluate the gradient of the utility function/optimality criterion
        grad = criterion.grad_uncertainty(
            design,
            uncertain_parameter_val,
            reset_parameter_val=reset_parameter_val,
        )

        # NOTE: the regularization is generally indpendent from the uncertain parameter

        # Return the gradient
        return grad

    def objective_function_grad_design(
        self,
        design,
        uncertain_parameter_val,
        reset_parameter_val=True,
    ):
        """
        Derivative of the objective function (that is the regularized utility/criterion,
        e.g., trace of FIM for A-opt) with respect to the relaxed design. This is used
        when the OED optimization problem is solved with relaxation of the design allowing
        the design to take any value in the interval [0, 1] and thus following a gradient
        descent/ascent direction for optimization over the design.
        The gradient is thus :math:`\\nabla_{d} \\mathcal{J(d, \\lambda)}` where
        :math:`d` is the design, :math:`\\lambda` is the uncertain parameter, and
        :math:`\\mathcal{J}` is the OED objective (A-opt etc.) evaluated by
        :py:meth:`objective_function`.

        :param design: an observational design vector conformable with the observation operator
            and the observation error covariance matrix operator.
            This is intended to be a relaxed design (over the interval [0, 1]),
        :param uncertain_parameter_val: value of the uncertain parameter; this must match the
            registered uncertain parameter and sampler.
        :param bool reset_parameter_val: if `True`, the current values of the uncertain parameter
            are restored after evaluating the objective function value.
        :param str oed_evaluation_method: how to evaluate the criterion; 'exact' or 'randomized'.
            This is currently used only if the registered optimality criterion is A- or
            D-optimality. More will be added gradually.
        :param dict oed_evaluation_randomization_settings: a dictionary holding settings of
            the randomization method (to approximate the optimality criterion or the
            utility function) used only if `oed_evaluation_method` is set to 'randomized'.
        :param bool verbose: screen verbosity True/False

        :returns: gradient of the *registered* OED optimality criterion w.r.t the relaxed design

        :raises: :py:class:`TypeError` is raised if no `uncertain_parameter_name` is registered.
        :raises: :py:class:`TypeError`  is raised if no `uncertain_parameter_sampler` is
            registered with a valid type. Only instances of :py:class:`ParameterSampler`
            are accepted.
        :raises: :py:class`TypeError` is raised if penalty function/term is not properly set.
        :raises: :py:class`TypeError` is raised if no valid optimality `criterion` object is
            set in the configurations dictionary.
        :raises: :py:class`TypeError` is raised if a `criterion` is registerd but it does not
            provide a :py:meth:grad_design` function.
        """
        # Validate Penalty term components (if any)
        penalty_weight = self.penalty_weight
        if penalty_weight not in [0, None] and (
            self.penalty_function is None or self.penalty_function_gradient is None
        ):
            raise TypeError(
                f"For nonzero penalty, you MUST set proper function for the penalty"
                f" term along with associate gradient\nUse register_penalty_term()"
                f" method before calling this function"
            )

        # This implements Equation (3.6c) in the robust OED paper;
        # Retrieve the underlying inverse problem and update the uncertain parameter
        ip = self.inverse_problem

        # Retrieve and check the uncertain parameter and the sampler
        uncertain_parameter_name = self.uncertain_parameter_name
        uncertain_parameter_sampler = self.uncertain_parameter_sampler
        if uncertain_parameter_name is None:
            raise TypeError(
                "No uncertain parameter is registered! use"
                " `register_uncertain_parameter()`"
            )
        if not isinstance(uncertain_parameter_sampler, ParameterSampler):
            raise TypeError(
                "The registered sampler is not valid! Expected an instance of"
                f" `ParameterSampler` but found {type(uncertain_parameter_sampler)}'."
                " Use `register_uncertain_parameter()` to register a valid sampler."
            )

        # Verify the OED criterion (utility function) is properly set to A-opt
        criterion = self.oed_criterion
        if criterion is None:
            raise TypeError(
                f"You must register/set an OED optimality criterion first. Use/Call"
                f" register_optimality_criterion()"
            )
        elif not hasattr(criterion, "grad_design"):
            # Finite differences
            grad = utility.finite_differences_gradient(
                fun=lambda x: criterion.evaluate(
                    x,
                    uncertain_parameter_val=uncertain_parameter_val,
                    reset_parameter_val=reset_parameter_val,
                ),
                state=design,
                bounds=[(0, 1)]*design.size,
            )
        else:

            # Evaluate the gradient of the optimality criterion (the utility function)
            grad = criterion.grad_design(
                design,
                uncertain_parameter_val,
                reset_parameter_val=reset_parameter_val,
            )

        # Apply the artificial scaling (if registered)
        if self.objective_function_scaling_factor is not None:
            grad *= self.objective_function_scaling_factor

        # Adde derivative of the penalty term (if registered)
        if self.penalty_weight not in [0, None]:
            grad += self.penalty_function_gradient(design) * self.penalty_weight

        # Return the gradient
        return grad

    def solve(self, init_guess=None, ):
        """
        Solve the robust OED optimization problem by using the registered
        optimization procedure.

        :param init_guess: initial guess (e.g., initial design, initial policy parameters)

        :returns: an instance of :py:class:`RobustSensorPlacementBayesianInversionOEDResults`
             holding results obtianed by solving the robust OED optimization problem.

        :raises TypeError: if no valid optimizer is registered
        """
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )

        if self.verbose:
            print(
                "Stared Solving the Robust OED problem "
            )

        # Solve the OED optimization problem, create a results object, and return it
        optimization_results = self.optimizer.solve(
            init_guess,
        )

        # Create and retuen a data-structure holding the Robust OED results
        return RobustSensorPlacementBayesianInversionOEDResults(
            oed_problem=self,
            optimization_results=optimization_results,
        )

    @staticmethod
    def show_supported_optimizers():
        """
        Print out optimization routines available to choose from.
        In addition to those shown here, the user can register fully instantiated
        optimizer (instance of :py:class:`Optimizer`) by calling
        :py:meth:`register_optimizer`.

        :returns: a list of supported optimization routines names
        """
        sep = "*" * 80
        msg = f"{sep}\n  Supoorted Optimization Routines: \n{sep}\n"
        for opt in self.supported_optimizers:
            msg += f" - {opt} \n"
        msg += f"{sep}\n"
        print(msg)

    @staticmethod
    def show_supported_uncertain_parameters():
        """
        Print out uncertain parameters available to choose from.
        """
        sep = "*" * 80
        msg = f"{sep}\n  Supoorted Uncertain Parameters: \n{sep}\n"
        for par in self.supported_uncertain_parameters:
            msg += f" - {par} \n"
        msg += f"{sep}\n"
        print(msg)

    @property
    def objective_function_offset(self):
        """Numeric offset added to the value of the objective (shifting)"""
        return self._OBJECTIVE_OFFSET

    @objective_function_offset.setter
    def objective_function_offset(self, value):
        """Numeric offset added to the value of the objective (shifting)"""
        if value is not None:
            assert utility.isnumber(
                value
            ), "The scaling factor must be a number (or None)"
        self._OBJECTIVE_OFFSET = float(value)

    @property
    def objective_function_scaling_factor(self):
        """A scaling factor of (to be multiplied by) the objective function self.objective_function"""
        return self._OBJECTIVE_SCALING_FACTOR

    @objective_function_scaling_factor.setter
    def objective_function_scaling_factor(self, value):
        """A scaling factor of (to be multiplied by) the objective function self.objective_function"""
        if value is not None:
            assert utility.isnumber(
                value
            ), "The scaling factor must be a number (or None)"
        self._OBJECTIVE_SCALING_FACTOR = float(value)

    @property
    def uncertain_parameter_name(self):
        """
        Name of the unceratin parameter
        """
        return self.configurations.uncertain_parameter

    @property
    def uncertain_parameter_sampler(self):
        """
        Handle to the sampler of the uncertain parameter
        problem
        """
        return self.configurations.uncertain_parameter_sampler
    @property
    def uncertain_parameter_size(self):
        """
        Handle to the sampler of the uncertain parameter
        problem
        """
        return self.uncertain_parameter_sampler.hyperparameter_size
    @property
    def uncertain_parameter_bounds(self):
        """
        Handle to the sampler of the uncertain parameter
        problem
        """
        return self.uncertain_parameter_sampler.hyperparameter_bounds

    @property
    def supported_optimizers(self, ):
        return self._SUPPORTED_OPTIMIZERS

    @property
    def supported_uncertain_parameters(self, ):
        return self._SUPPORTED_UNCERTAIN_PARAMETERS

    @property
    def supported_uncertain_parameter_samplers(self, ):
        return self._SUPPORTED_UNCERTAIN_PARAMETER_SAMPLERS



@dataclass(kw_only=True, slots=True)
class RobustSensorPlacementBayesianInversionOEDResults(RobustInversionOEDResults):
    """
    Base class for objects holding results of
    :py:class:`RobustSensorPlacementBayesianInversionOED`
    """
    ...

