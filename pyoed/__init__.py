# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import importlib as _importlib

# PyOED's Version
__version__ = "2.0.dev0"

# PyOED's subpackages
submodules = [
    'utility',
    'configs',
    'stats',
    'models',
    'assimilation',
    'optimization',
    'ml',
    'oed',
    'tests',
    'examples',
]

__all__ = submodules + [
    'tests',
]

def __dir__():
    return __all__


def __getattr__(name):
    if name in submodules:
        return _importlib.import_module(f'pyoed.{name}')
    else:
        try:
            return globals()[name]
        except KeyError:
            raise AttributeError(
                f"Module 'pyoed' has no attribute '{name}'"
            )

# Alias to PyOED's settings struct
from . import configs
SETTINGS = configs.SETTINGS

