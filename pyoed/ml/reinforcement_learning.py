# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Reinfocement Learning (Policy Gradient) tailored for binary OED optimization.

- This work was developed initially under DOERL package: https://gitlab.com/ahmedattia/doerl

.. Note::

    This was part of Stochastic Learning Approach to Binary Optimization for Optimal
    Design of Experiments; see: https://arxiv.org/abs/2101.05958

"""

import numpy as np
from pyoed import utility
from pyoed.utility.mixins import RandomNumberGenerationMixin


# Base Classes
# ===========
class Policy(object):
    def __init__(self, *args, **kwargs):
        """
        A class defining a Reinfocement Learning policy
        """
        raise NotImplementedError

    def update_policy_parameters(self, *args, **kwargs):
        """ """
        raise NotImplementedError

    def sample_action(self, *args, **kwargs):
        """
        Use the policy probability distribution to sample a single action
        """
        raise NotImplementedError

    def sample_trajectory(self, *args, **kwargs):
        """
        Use the policy probability distribution to sample a trajectory, starting  from the passed initial state

        :param init_state:
        :param length: length of the trajectory; nuumber of state-action pairs to be sampled

        :returns: trajectory: list contining state-action pairs [(state, action), ...(state, action)]. \n
                  The action associated with last state should do nothing to the corresponding state
                  number of entries in the trajectory is equal to the passed length+1, i.e., 'length' state
                  are generated  after the initial state  which is used in the first pair in
                  the trajectory.

        """
        raise NotImplementedError

    def conditional_probability(self, *args, **kwargs):
        """
        Calculate the probability of s1 conditioned by :math:`s_0`; i.e. :math:`p(s_1|s_0)`

        """
        raise NotImplementedError

    def conditional_log_probability_gradient(self, *args, **kwargs):
        """
        Calculate the gradient of log-probability of s1 conditioned by :math:`s_0`; i.e. :math:`p(s_1|s_0)`, w.r.t. parameters
        """
        raise NotImplementedError

    @property
    def hyperparameters(self):
        return self._hyperparameters


class State(object):
    def __init__(self, *args, **kwargs):
        """
        A class defining a Reinfocement Learning State for the environment
        """
        raise NotImplementedError

    @property
    def size(self):
        """
        Size of the internal state
        """
        return self._state.size

    @property
    def state(self):
        """
        Retrieve the internal state
        """
        return self._state

    @state.setter
    def state(self, state):
        """
        Update the internal state, after assertion
        """
        if isinstance(State):
            assert (
                state.size == self.size
            ), "passed state must be of equal size to the internal state!"
            self._state = state.copy()  # compy to avoid external manipulation
        elif utility.isiterable(state):
            assert (
                len(state) == self.size
            ), "passed state must be of equal size to the internal state!"
            self._state[...] = np.array(state).flatten()
        else:
            print(
                "Can't used passed value to udpate internal state. Wrong Type detected!"
            )
            raise TypeError
        raise NotImplementedError


class Action(object):
    def __init__(self):
        """
        A class defining a Reinfocement Learning Action
        """
        raise NotImplementedError

    @property
    def size(self):
        """
        Size of the internal action
        """
        return self._action.size

    @property
    def action(self):
        """
        Retrieve the internal action
        """
        return self._action

    @action.setter
    def action(self, action):
        """
        Update the internal actiion, after assertion
        """
        raise NotImplementedError


class Agent(object):
    """
    A class defining a Reinfocement Learning Agent
    """

    def __init__(self, *args, **kwargs):
        raise NotImplementedError

    def trajectory_return(self, *args, **kwargs):
        """
        Given a trajectory; sequence/list of state-action pairs, calculate the total reward. \n
        Given the current state, and action, return the value of the reward function. Inspect policy if needed
        """
        raise NotImplementedError

    def reward(self, *args, **kwargs):
        """
        Given the current state, and action, return the value of the reward function. Inspect policy if needed
        """
        raise NotImplementedError

    def initialize_state(self, *args, **kwargs):
        """
        Initialize the state of the environment
        """
        raise NotImplementedError

    def update_state(self, *args, **kwargs):
        """
        Update the environment state to the passed state
        """
        raise NotImplementedError

    def sample_trajectory(self, *args, **kwargs):
        """
        Sample a trajectory as long as the passed length
        """
        raise NotImplementedError

    def policy_gradient(self, *args, **kwargs):
        """
        Calculate policy gradient
        """
        raise NotImplementedError

    def train(self, *args, **kwargs):
        """
        Train the agent to optimize the policy
        """
        raise NotImplementedError

    @property
    def policy(self):
        """
        Handle to the underlying policy
        """
        return self._policy


# RL Classes
# ===========
class FlipAction(Action):
    """
    A class defining a Reinfocement Learning Action
    An action is a binary vector that describes which state kept as is, and which is flipped

    :param int size: size of the binary vector/state

    """

    def __init__(self, size):
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._action = np.zeros(int(size), dtype=np.bool)

    def __copy__(self):
        """
        Copy this Action instance
        """
        action_copy = BinaryState(size=self._actiion.size)
        action_copy._action = self._action.copy()
        return action_copy

    def copy(self):
        return self.__copy__()

    @property
    def size(self):
        """
        Size of the internal action
        """
        return self._action.size

    @property
    def action(self):
        """
        Retrieve the internal action
        """
        return self._action

    @action.setter
    def action(self, action):
        """
        Update the internal action, after assertion
        """
        raise NotImplementedError


class BinaryState(State):
    """
    A class defining a Reinfocement Learning State for the environment

    :param int size: size of the binary vector/state

    """

    def __init__(self, size):
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._state = np.zeros(int(size), dtype=np.bool)

    def __copy__(self):
        """
        Copy this State
        """
        state_copy = BinaryState(size=self._state.size)
        state_copy._state = self._state.copy()
        return state_copy

    def copy(self):
        return self.__copy__()

    def update(self, action, in_place=True):
        """
        Given an action object, update the entries of the internal state,
        update the state and either return self, or a copy with updated state

        :param FlipAction action: action to apply to the current state

        :returns: state resulting by applying the passed action on the current state

        """
        assert isinstance(action, FlipAction), "action must be an instace of FlipAction"

        if in_place:
            out_state = self
        else:
            out_state = self.copy()
        out_state.state[action.action] = ~out_state.state[action.action]
        return out_state


class BernoulliPolicy(Policy, RandomNumberGenerationMixin):
    """
    A class defining a Reinfocement Learning policy, based on Bernoulli distribution

    .. math::
       p(x) = \\theta^{x}\\times (1-\\theta)^{1-x}, \\quad x \\in \\{0, 1\\}

    Parameters of the constructor are below.

    :param int size: dimension of the Bernoulli random variable
    :param float theta: success probablity: :math:`p(x=1)`
    :param None|int random_seed: random seed to set the underlying random number generator
    """

    def __init__(
        self,
        size,
        theta=0.5,
        random_seed=None,
    ):
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._state_size = size
        self._hyperparameters = dict()
        self.update_policy_parameters(theta=theta)
        self.update_random_number_generator(random_seed=random_seed)

    def update_policy_parameters(self, theta):
        """
        Update theta; that is :math:`p(x=1)`

        :param float theta: probability of sucess :math:`p(x==1)`

        """
        if utility.isnumber(theta):
            assert 0 <= theta <= 1, (
                "Parameter value %f is not allowed! theta must fall in the interval [0, 1]"
                % theta
            )
            theta = np.ones(self._state_size, dtype=float) * theta

        elif utility.isiterable(theta):
            if len(theta) != self._state_size:
                print(
                    "parameter must be scalar or iterable of length equal to problem size"
                )
                print(
                    "Len(theta): %d , State size: %d " % (len(theta), self._state_size)
                )
                raise ValueError
            theta = np.array(theta).flatten()
            if np.any(theta < 0) or np.any(theta > 1):
                print(
                    "Parameter value %f is not allowd! theta must fall in the interval [0, 1]"
                )
                raise ValueError
        else:
            print("theta must be scalr or iterable of size :%d" % self._state_size)
            raise TypeError
        self._hyperparameters.update({"theta": theta})

    def sample_action(self):
        """
        Sample an appropriate action
        """
        size = self._state_size
        theta = self._hyperparameters["theta"]
        action = FlipAction(size)
        sampled_action = np.array(
            [
                self.random_number_generator.binomial(1, theta[i])
                for i in range(theta.size)
            ]
        ).astype(np.bool)
        # print("Sampled Action : ", sampled_action)
        # print("Under Policy: ", theta)
        action.action[...] = sampled_action
        return action

    def sample_trajectory(self, init_state, length):
        """
        Use the policy probability distribution to sample a trajectory, starting  from the passed initial state

        :param State init_state: initial state of the trajectory to sample
        :param int length: length of the trajectory; nuumber of state-action pairs to be sampled

        :returns: trajectory; list contining state-action pairs [(state,
            action), ...(state, action)].

        .. note::

            - The action associated with last state should do nothing to the
              corresponding state

            - Number of entries in the trajectory is equal to the passed
              length+1, i.e., 'length'

            - States are generated after the initial state  which is used in
              the first pair in the trajectory.

        :raises: TypeError if init_state is not an instance of `pyoed.ml.reinforcement_leanrning.State` class
        """
        if not isinstance(init_state, State):
            print(
                "init_state must be an instance of pyoed.ml.reinforcement_leanrning.State \nReceived type {:}".format(
                    type(init_state)
                )
            )
            raise TypeError

        state = init_state.copy()
        action = self.sample_action()
        trajectory = [(state, action)]
        for i in range(1, length + 1):
            s = trajectory[i - 1][0]
            a = trajectory[i - 1][1]
            state = s.update(action=a, in_place=False)
            action = self.sample_action()
            if i == length:
                # set all entries of action to zeros
                action.action[...] = 0  # Better approach should be considered
            trajectory.append((state, action))
        return trajectory

    def conditional_probability(self, s0, s1, log_probability=True):
        """
        Calculate the probability of s1 conditioned by :math:`s_0`; i.e. :math:`p(s_1|s_0)`.

        Here, we assume iid entries of states

        :param State s0: Multivariate Bernoulli state
        :param State s1: Multivariate Bernoulli state
        :param bool log_probability: either return probability or the log-probability

        :returns: p; value of the conditional probability
        """
        assert isinstance(s0, BinaryState) and isinstance(
            s0, BinaryState
        ), "s0 and s1 must be both instances of BinaryState"
        assert s0.size == s1.size, "s0 and s1 must be of equal sizes"
        # get parameter value (theta) for all state entries
        theta = self._hyperparameters["theta"]
        assert (
            theta.size == s0.size
        ), "State size doesn't match the size of the parameter space!"

        # calculate log-probability
        diff = s1.state.astype(np.int) - s0.state.astype(np.int)
        diff_sqr = np.power(diff, 2)
        log_prob = diff_sqr * np.log(theta)
        log_prob -= (1.0 - diff_sqr) * np.log(1 - theta)
        log_prob = np.sum(log_prob)

        # update if PMF value is required and return
        p = log_prob if log_probability else np.exp(log_prob)
        return p

    def conditional_log_probability_gradient(self, s0, s1):
        """
        Calculate the gradient of log-probability of s1 conditioned by :math:`s_0`; i.e. :math:`\\log p(s_1|s_0)`, w.r.t.
        parameters :math:`\\theta`.
        Here, we assume iid entries of states

        :param State s0: Multivariate Bernoulli state
        :param State s1: Multivariate Bernoulli state

        :returns: log_prob_grad a vector containing derivatives of the log-probability of the multivariate Bernoulli w.r.t
            the parameters :math:`\\theta`

        """
        assert isinstance(s0, BinaryState) and isinstance(
            s0, BinaryState
        ), "s0 and s1 must be instances of BinaryState"
        assert s0.size == s1.size, "s0 and s1 must be of equal sizes"
        # get parameter value (theta) for all state entries
        theta = self._hyperparameters["theta"]
        assert (
            theta.size == s0.size
        ), "State size doesn't match the size of the parameter space!"

        # calculate gradient of log-probability
        diff = s1.state.astype(np.int) - s0.state.astype(np.int)
        diff_sqr = np.power(diff, 2)

        log_prob_grad = diff_sqr / theta
        log_prob_grad += (diff_sqr - 1) / (1 - theta)
        return log_prob_grad
