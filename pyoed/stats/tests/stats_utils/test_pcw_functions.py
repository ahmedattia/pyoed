"""
Test PCW Functions in pyoed.stats.stats_utils
"""

from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    create_random_number_generator,
)
from ...stats_utils.pcw_functions import (
    get_available_pcw_functions,
    get_pcw_function,
    CovarianceProductPCWConfigs,
    PCWFunctionConfigs,
    PCWFunction,
)
from ....configs import PyOEDConfigsValidationError

import pytest

pytestmark = pytest.mark.stats


def test_all_built_in_pcw_functions():
    """
    Test that all built-in pointwise covariance weighting functions can be
    retrieved.
    """
    for pcw_function_name in get_available_pcw_functions():
        # Check if the function passes internal validation on initialization
        try:
            get_pcw_function(pcw_function_name)
        except PyOEDConfigsValidationError as e:
            pytest.fail(
                f"Failed to initialize pointwise covariance weighting function "
                f"{pcw_function_name} due to error: {e}",
            )


def test_custom_pcw_function():
    kernel = CovarianceProductPCWConfigs.kernel
    kernel_grad = CovarianceProductPCWConfigs.kernel_grad
    bounds = CovarianceProductPCWConfigs.bounds
    name = CovarianceProductPCWConfigs.name
    name_regex = CovarianceProductPCWConfigs.name_regex

    # Full configuration
    PCWFunction(
        PCWFunctionConfigs(
            kernel=kernel,
            kernel_grad=kernel_grad,
            bounds=bounds,
            name=name,
            name_regex=name_regex,
        )
    )

    # Partial configuration
    PCWFunction(
        PCWFunctionConfigs(
            kernel=kernel,
            kernel_grad=kernel_grad,
            bounds=bounds,
            # name=name,
            # name_regex=name_regex,
        )
    )

    # Name but no regex
    PCWFunction(
        PCWFunctionConfigs(
            kernel=kernel,
            kernel_grad=kernel_grad,
            bounds=bounds,
            name=name,
            # name_regex=name_regex,
        )
    )

    # regex but no name
    PCWFunction(
        PCWFunctionConfigs(
            kernel=kernel,
            kernel_grad=kernel_grad,
            bounds=bounds,
            # name=name,
            name_regex=name_regex,
        )
    )

    # Assert that essential attributes are present
    with pytest.raises(PyOEDConfigsValidationError):
        PCWFunction(
            PCWFunctionConfigs(
                kernel=kernel,
                kernel_grad=kernel_grad,
                # bounds=bounds,
            )
        )

    with pytest.raises(PyOEDConfigsValidationError):
        PCWFunction(
            PCWFunctionConfigs(
                kernel=kernel,
                # kernel_grad=kernel_grad,
                bounds=bounds,
            )
        )

    with pytest.raises(PyOEDConfigsValidationError):
        PCWFunction(
            PCWFunctionConfigs(
                # kernel=kernel,
                kernel_grad=kernel_grad,
                bounds=bounds,
            )
        )
