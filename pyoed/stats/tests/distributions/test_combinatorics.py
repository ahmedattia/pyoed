# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Test conditional bernoulli model in `pyoed.stats.distributions.conditional_bernoulli`
"""

import numpy as np
from itertools import (
    product,
    combinations,
)

from pyoed.stats.distributions.combinatorics import (
    RFunction,
)
from pyoed import utility
from pyoed.tests import (
    SETTINGS as _SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)

class SETTINGS:
    """Local settings"""
    RANDOM_SEED = _SETTINGS.RANDOM_SEED
    SIZE = _SETTINGS.OBSERVATION_SIZE
    SMALL_SAMPLE_SIZE = 32
    MODERATE_SAMPLE_SIZE = 100
    LARGE_SAMPLE_SIZE = 10000
    GRADIENTS_RELATIVE_ERROR_TOL = 1e-2

import pytest
pytestmark = [pytest.mark.stats, pytest.mark.distributions]



@pytest.mark.parametrize("method", ['recursion', 'tabulation'], )
def test_stats_distributions_poisson_binomial_RFunction(method ):
    """Test the evaluation of the R-Function"""
    r_function = RFunction({"method":method})

    # Test with N=100
    N = SETTINGS.SIZE * 2

    # Create random weights
    rng = create_random_number_generator()
    p = rng.uniform(size=N)

    w = 1.0 / (1.0-p)

    # Test with n = 0
    v = r_function.evaluate(0, w, log=False, dtype=float, )
    assert v == 1, f"Invalid R(0, w); expected 1, received {v=}"

    # Test with n = 1
    v = r_function.evaluate(1, w, log=False, dtype=float, )
    v2 = sum(w)
    assert_allclose(
        v, v2,
        err_msg=f"Invalid R(0, w); expected R(1, w)={v2}, but received {v=}"
    )

    # Test with n = 2, 3
    for n in [2, 3]:
        v = r_function.evaluate(n, w, log=False, dtype=float, )
        all_combs = list(combinations(w, n))
        v2 = sum([np.prod(c) for c in all_combs])
        assert_allclose(
            v, v2,
            err_msg=f"Invalid R({n}, w); expected R({n}, w)={v2}, but received {v=}"
        )

        # Test R function gradient
        fun = lambda _w: r_function.evaluate(n=n, w=_w, log=False, dtype=float, )
        grad = r_function.gradient(n=n, w=w, log=False, dtype=float, )
        _, rel_err = utility.validate_function_gradient(
            fun=fun,
            state=w,
            gradient=grad,
            verbose=True,
        )
        assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL




