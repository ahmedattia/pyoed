# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Test bernoulli model in `pyoed.stats.distributions.bernoulli`
"""

import numpy as np
# from itertools import product

from pyoed.stats.distributions.bernoulli import (
    Bernoulli,
    BernoulliConfigs,
)
from pyoed.configs import PyOEDConfigsValidationError

from pyoed import utility
from pyoed.tests import (
    SETTINGS as _SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)

class SETTINGS:
    """Local settings"""
    RANDOM_SEED = _SETTINGS.RANDOM_SEED
    SIZE = _SETTINGS.OBSERVATION_SIZE
    SMALL_SAMPLE_SIZE = 32
    MODERATE_SAMPLE_SIZE = 100
    LARGE_SAMPLE_SIZE = 10000
    GRADIENTS_RELATIVE_ERROR_TOL = 1e-4

import pytest
pytestmark = [pytest.mark.stats, pytest.mark.distributions]


# Fixtures:
# =========
def create_Bernoulli_distribution(
        size=SETTINGS.SIZE,
        success_probability=None,
        random_seed=SETTINGS.RANDOM_SEED,
):
    """
    Create a multivariate Bernoulli distribution (instance of :py:class:`Bernoulli`)

    :returns: both the distribution and the configurations dictionary used to create the distribution
    """

    # Create success probabilities (if not passed)
    if success_probability is None:
        rng = create_random_number_generator() # Create random number generator
        success_probability = np.round(rng.random(int(size)), 3)

    configs = dict(
        parameter=success_probability,
        verbose=False,
        random_seed=random_seed,
        name="Testing MultivariateBernoulli",
    )

    dist = Bernoulli(configs=configs)

    return dist, configs


@pytest.mark.parametrize("size", [0, 1, SETTINGS.SIZE])
def test_stats_distributions_bernoulli_Bernoulli(size):
    """
    Test the Bernoulli distribution :py:class:`pyoed.stats.distributions.bernoulli.Bernoulli`
    """
    # Create random number generator
    rng = create_random_number_generator()

    ## Test configurations incompatability upon instantiation
    if size == 0:
        with pytest.raises(PyOEDConfigsValidationError):
            create_Bernoulli_distribution(size=size, )
        return

    else:

        # Invalid parameter values (outside [0, 1])
        success_probability = 10 * (rng.random(size) - 0.5)
        with pytest.raises(PyOEDConfigsValidationError):
            create_Bernoulli_distribution(success_probability=success_probability)


    #
    ## Create a valid distribution and start testing its functionality
    dist, orig_configs = create_Bernoulli_distribution(size=size)

    # Check distribution type
    assert isinstance(dist, Bernoulli), f"Invalid distribution {type(dist)=}"
    assert isinstance(orig_configs, (dict, BernoulliConfigs)), f"Invalid configurations {type(orig_configs)=}"


    # ===========================
    ## Test properties/attributes
    # ===========================

    # `success_probability`
    # ---------------------
    parameter = dist.success_probability
    assert_allequal(orig_configs['parameter'], parameter, err_msg=f"Invalid {parameter=} of {type(parameter)=}")

    # `size`
    # ------
    size = dist.size
    assert int(size) == size == orig_configs['parameter'].size, f"Invlid {size=} of {type(size)=}"

    # `verbose`
    # ---------
    verbose = dist.verbose
    assert bool(verbose) == verbose == orig_configs['verbose'], f"Invlid {verbose=} of {type(verbose)=}"


    # =============
    ## Test methods
    # =============

    # `update_configurations`
    # -----------------------
    # Get copy of the configurations
    dist_configs = dist.configurations
    assert isinstance(dist_configs, (dict, BernoulliConfigs)), f"Invalid configurations {type(dist_configs)=}"

    # Test original configurations first
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"

    new_configs = dict(
        parameter=rng.random(orig_configs['parameter'].size+1),
        name="TEST Name Update",
    )
    # * Updating configurations (1 at a time, then all at once)
    dist.update_configurations( parameter=new_configs['parameter'])
    assert dist.size == new_configs['parameter'].size, f"Invalid size after updating configs"
    assert_allequal(dist.success_probability, new_configs['parameter'], err_msg=f"Invalid parameter after updating configs")

    for rs in [None, 12345]:
        dist.update_configurations(random_seed=rs, )
        assert dist.configurations.random_seed == rs, f"Invalid random seed after updating configs"

    for verbose in [True, False]:
        dist.update_configurations(verbose=verbose, )
        assert dist.verbose == verbose, f"Invalid verbosity after updating configs"

    dist.update_configurations(name=new_configs['name'], )
    assert dist.configurations.name == new_configs['name'], f"Invalid name after updating configs"

    # Reset configurations & test
    dist.update_configurations(**orig_configs)
    dist_configs = dist.configurations
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"


    # `sample`
    # -----------------------
    for antithetic in [False, True]:
        for dtype in [int, bool, float]:
            sample = dist.sample(sample_size=SETTINGS.SMALL_SAMPLE_SIZE, dtype=dtype, antithetic=antithetic)
            assert isinstance(sample, np.ndarray), f"Invalid {type(sample)=}"
            assert sample.dtype == dtype, f"Invalid {sample.dtype=}"
            assert sample.shape == (SETTINGS.SMALL_SAMPLE_SIZE, dist.size), f"Invalid {sample.shape=}"

    # `pmf`
    # -----------------------
    # Calculate PMF for all possible combinations and make sure probabilities add up to 1
    tot_prob = 0.0
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        p = dist.pmf(d)
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
        tot_prob += p
    assert np.allclose(tot_prob, 1.0), f"invalid distribution (total probability = {tot_prob} != 1)"

    # `log_pmf`
    # -----------------------
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        assert_allclose(np.exp(dist.log_pmf(d)), dist.pmf(d), err_msg="Invalid log-pmf")

    # `expect`
    # -----------------------
    func = lambda x: np.sum(x)
    expect = 0.0
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        expect += func(d) * dist.pmf(d)

    dist_expect = dist.expect(func=func, )
    assert 0 <= dist_expect <= dist.size, f"Expectation is invalid {dist_expect=}; expected{expect}"
    assert_allclose(expect, dist_expect, err_msg=f"Expectation is invalid {dist_expect=}; expected{expect}")


    # `grad_pmf`
    # `grad_log_pmf`
    # -----------------------
    bounds = [ (0, 1) ] * dist.size
    for p in [rng.random(dist.size) for _ in range(5)]:

        # Pick two entries and make them binary
        deg_p = p.copy()
        deg_p[0] = 1
        deg_p[-1] = 0

        # Success probability

        # Design sample
        for d in dist.sample(sample_size=SETTINGS.SMALL_SAMPLE_SIZE):

            # function to evaluate pmf
            def pmf_fun(p):
                _p = dist.success_probability
                dist.success_probability = p
                f = dist.pmf(d)
                dist.success_probability = _p
                return f

            # function to evaluate log-pmf
            def log_pmf_fun(p):
                _p = dist.success_probability
                dist.success_probability = p
                f = dist.log_pmf(d)
                dist.success_probability = _p
                return f


            # Calculate gradient of the PMF (exactly)
            _p = dist.success_probability
            dist.success_probability = deg_p
            grad_pmf = dist.grad_pmf(d)
            dist.success_probability = _p

            # `grad_pmf`
            _, rel_err = utility.validate_function_gradient(
                fun=pmf_fun,
                state=deg_p,
                gradient=grad_pmf,
                bounds=bounds,
                verbose=True,
            )
            assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL

            # Calculate gradient of the log PMF (exactly)
            _p = dist.success_probability
            dist.success_probability = p
            grad_pmf = dist.grad_pmf(d)
            grad_log_pmf = dist.grad_log_pmf(d)
            dist.success_probability = _p

            # `grad_log_pmf`
            _, rel_err = utility.validate_function_gradient(
                fun=log_pmf_fun,
                state=p,
                gradient=grad_log_pmf,
                bounds=bounds,
                verbose=True,
            )
            assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL

    # `index_to_binary_state`
    # `index_from_binary_state`
    # -----------------------
    for k in range(1, 2**dist.size+1):
        for dtype in [bool, int, float]:
            d1 = utility.index_to_binary_state(k, size=dist.size, dtype=dtype)
            d2 = dist.index_to_binary_state(k, dtype=dtype)
            assert d1.dtype == d2.dtype == dtype, f"Ivalid retuned {d1.dtype=}, {d2.dtype=}"
            assert_allequal(d1, d2, err_msg=f"Invalid indexing in the distribution")
            assert k == dist.index_from_binary_state(d2), f"Invalid indexing in the distribution"


