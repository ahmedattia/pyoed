# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Test conditional bernoulli model in `pyoed.stats.distributions.conditional_bernoulli`
"""
import warnings
warnings.warn(
    f"The conditional Bernoulli test MUST be updated to test batch-mode evaluation of "
    f"pmf-related quantities (e.g., values & gradients. ...)"
)

import numpy as np
from itertools import product

from pyoed.stats.distributions.poisson_binomial import (
    PoissonBinomial,
)
from pyoed.stats.distributions.conditional_bernoulli import (
    ConditionalBernoulli,
    GeneralizedConditionalBernoulli,
    ConditionalBernoulliConfigs,
    GeneralizedConditionalBernoulliConfigs,
)
from pyoed import utility
from pyoed.tests import (
    SETTINGS as _SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)
from pyoed.configs import PyOEDConfigsValidationError


class SETTINGS:
    """Local settings"""
    RANDOM_SEED = _SETTINGS.RANDOM_SEED
    SIZE = _SETTINGS.OBSERVATION_SIZE
    BUDGETS = None
    SMALL_SAMPLE_SIZE = 32
    MODERATE_SAMPLE_SIZE = 100
    LARGE_SAMPLE_SIZE = 10000
    GRADIENTS_RELATIVE_ERROR_TOL = 1e-5
    GRADIENTS_ABSOLUTE_ERROR_TOL = 1e-6

import pytest
pytestmark = [pytest.mark.stats, pytest.mark.distributions]


# Fixtures:
# =========
def create_ConditionalBernoulli_distribution(
        size=SETTINGS.SIZE,
        success_probability=None,
        random_seed=SETTINGS.RANDOM_SEED,
):
    """
    Create a multivariate ConditionalBernoulli distribution (instance of
    :py:class:`ConditionalBernoulli`)

    :returns: both the distribution and the configurations dictionary
      used to create the distribution
    """
    # Create success probabilities (if not passed)
    if success_probability is None:
        rng = create_random_number_generator() # Create random number generator
        success_probability = np.round(rng.random(int(size)), 3)
        # Add degeneracy
        if size > 2:
            deg_locs = rng.choice(range(size), size=2, replace=False)
            success_probability[deg_locs[0]] = 0
            success_probability[deg_locs[1]] = 1

    configs = dict(
        parameter=success_probability,
        verbose=False,
        random_seed=random_seed,
        name="Testing Multivariate ConditionalBernoulli",
    )

    dist = ConditionalBernoulli(configs=configs)

    return dist, configs

def create_GeneralizedConditionalBernoulli_distribution(
    size=SETTINGS.SIZE,
    budgets=SETTINGS.BUDGETS,
    success_probability=None,
    random_seed=SETTINGS.RANDOM_SEED,
):
    """
    Create a multivariate Generalized ConditionalBernoulli distribution
    (instance of :py:class:`GeneralizedConditionalBernoulli`)

    :returns: both the distribution and the configurations dictionary
      used to create the distribution
    """

    # Create success probabilities (if not passed)
    if success_probability is None:
        rng = create_random_number_generator() # Create random number generator
        success_probability = np.round(rng.random(int(size)), 3)
        # Add degeneracy
        if size > 2:
            deg_locs = rng.choice(range(size), size=2, replace=False)
            success_probability[deg_locs[0]] = 0
            success_probability[deg_locs[1]] = 1

    configs = dict(
        verbose=False,
        parameter=success_probability,
        budgets=budgets,
        random_seed=random_seed,
        name="Testing Multivariate GeneralizedConditionalBernoulli",
    )

    dist = GeneralizedConditionalBernoulli(configs=configs)
    return dist, configs


@pytest.mark.parametrize(
    "size, n",
    product(
        [0, 1, SETTINGS.SIZE],
        list(set([0, 1, SETTINGS.SIZE//2, SETTINGS.SIZE-1, SETTINGS.SIZE]))
    )
)
def test_stats_distributions_bernoulli_ConditionalBernoulli(size, n):
    """
    Test the ConditionalBernoulli distribution
    :py:class:`pyoed.stats.distributions.conditional_bernoulli.ConditionalBernoulli`
    """

    # Create random number generator
    rng = create_random_number_generator()

    ## Test configurations incompatability upon instantiation
    if size == 0:
        with pytest.raises(PyOEDConfigsValidationError):
            create_ConditionalBernoulli_distribution(size=size)
        return

    else:

        # Invalid parameter values (outside [0, 1])
        success_probability = 10 * (rng.random(size) - 0.5)
        with pytest.raises(PyOEDConfigsValidationError):
            create_ConditionalBernoulli_distribution(size=size, success_probability=success_probability)

    #
    ## Create a valid distribution and start testing its functionality
    dist, orig_configs = create_ConditionalBernoulli_distribution(size=size)

    # Check distribution type
    assert isinstance(dist, ConditionalBernoulli), f"Invalid distribution {type(dist)=}"
    assert isinstance(orig_configs, (dict, ConditionalBernoulliConfigs)), f"Invalid configurations {type(orig_configs)=}"


    # ===========================
    ## Test properties/attributes
    # ===========================

    # `success_probability`
    # ---------------------
    parameter = dist.success_probability
    assert_allequal(orig_configs['parameter'], parameter, err_msg=f"Invalid {parameter=} of {type(parameter)=}")

    # `size`
    # ------
    size = dist.size
    assert int(size) == size == orig_configs['parameter'].size, f"Invlid {size=} of {type(size)=}"

    # `verbose`
    # ---------
    verbose = dist.verbose
    assert bool(verbose) == verbose == orig_configs['verbose'], f"Invlid {verbose=} of {type(verbose)=}"


    # =============
    ## Test methods
    # =============

    # `update_configurations`
    # -----------------------
    # Get copy of the configurations
    dist_configs = dist.configurations
    assert isinstance(dist_configs, ConditionalBernoulliConfigs), f"Invalid configurations {type(dist_configs)=}"

    # Test original configurations first
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"

    new_configs = dict(
        parameter=rng.random(orig_configs['parameter'].size+1),
        name="TEST Name Update",
    )
    # * Updating configurations (1 at a time, then all at once)
    dist.update_configurations(parameter=new_configs['parameter'])
    assert dist.size == new_configs['parameter'].size, f"Invalid size after updating configs"
    assert_allequal(dist.success_probability, new_configs['parameter'], err_msg=f"Invalid parameter after updating configs")

    for rs in [None, 12345]:
        dist.update_configurations(random_seed=rs, )
        assert dist.configurations.random_seed == rs, f"Invalid random seed after updating configs"

    for verbose in [True, False]:
        dist.update_configurations(verbose=verbose, )
        assert dist.verbose == verbose, f"Invalid verbosity after updating configs"

    dist.update_configurations(name=new_configs['name'], )
    assert dist.configurations.name == new_configs['name'], f"Invalid name after updating configs"

    # Reset configurations & test
    dist.update_configurations(**orig_configs)
    dist_configs = dist.configurations
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"


    # `sample`
    # -----------------------
    min_active = np.where(dist.success_probability==1)[0].size
    for antithetic in [False, True]:
        for dtype in [int, bool, float]:
            if min_active <= n <= np.count_nonzero(dist.success_probability):
                sample = dist.sample(
                    n=n,
                    sample_size=SETTINGS.SMALL_SAMPLE_SIZE,
                    dtype=dtype,
                    antithetic=antithetic,
                )
                assert isinstance(sample, np.ndarray), f"Invalid {type(sample)=}"
                assert sample.dtype == dtype, f"Invalid {sample.dtype=}"
                assert sample.shape == (SETTINGS.SMALL_SAMPLE_SIZE, dist.size), f"Invalid {sample.shape=}"

            else:
                with pytest.raises(ValueError):
                    sample = dist.sample(
                        n=n,
                        sample_size=SETTINGS.SMALL_SAMPLE_SIZE,
                        dtype=dtype,
                        antithetic=antithetic,
                    )

    # `sum_pmf`
    # -----------------------
    # Calculate PMF for all possible possible values of the sum
    tot_prob = 0.0
    for m in range(dist.size+1):
        p = dist.sum_pmf(n=m, )
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
        tot_prob += p
    assert_allclose(tot_prob, 1.0, err_msg=f"invalid Poisson-Binomial probability (total probability = {tot_prob} != 1)")


    # `sum_log_pmf`
    # -----------------------
    for m in range(dist.size+1):
        assert_allclose(np.exp(dist.sum_log_pmf(n=m, )), dist.sum_pmf(n=m, ),
                        err_msg="Invalid log-pmf of the Poisson-Binomial model")

    # `pmf`
    # -----------------------
    # Calculate PMF for all possible combinations and make sure probabilities add up to 1
    all_states = []
    all_probs = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        all_states.append(d)
        p = dist.pmf(d, n=n, )
        all_probs.append(p)
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
    all_probs = np.asarray(all_probs)
    all_states = np.vstack(all_states)
    tot_prob = np.sum(all_probs)

    min_active = np.where(dist.success_probability==1)[0].size
    if min_active <= n  <= np.count_nonzero(dist.success_probability):
        assert_allclose(tot_prob, 1.0, err_msg=f"invalid distribution (total probability = {tot_prob} != 1)")
    else:
        assert_allclose(tot_prob, 0.0, err_msg=f"invalid distribution (total probability for impossible {n=} = {tot_prob} != 0)")

    # PMF in batch mode
    batch_pmf = dist.pmf(all_states, n=n, batch_as_column=False)
    assert_allclose(batch_pmf, all_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=False)")
    batch_pmf = dist.pmf(all_states.T, n=n, batch_as_column=True)
    assert_allclose(batch_pmf, all_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=True)")


    # `log_pmf`
    # -----------------------
    all_states = []
    all_log_probs = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        all_states.append(d)
        lp = dist.log_pmf(d, n=n, )
        all_log_probs.append(lp)
        assert_allclose(np.exp(lp), dist.pmf(d, n=n, ), err_msg="Invalid log-pmf")
    all_log_probs = np.asarray(all_log_probs)
    all_states = np.vstack(all_states)

    # Log-pmf in batch mode
    batch_log_pmf = dist.log_pmf(all_states, n=n, batch_as_column=False)
    assert_allclose(batch_log_pmf, all_log_probs, err_msg=f"invalid batch log-PMF evaluation (batch_as_column=False)")
    batch_log_pmf = dist.log_pmf(all_states.T, n=n, batch_as_column=True)
    assert_allclose(batch_log_pmf, all_log_probs, err_msg=f"invalid batch log-PMF evaluation (batch_as_column=True)")


    # `expect`
    # -----------------------
    func = lambda x: np.sum(x)
    expect = 0.0
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        expect += func(d) * dist.pmf(d, n=n, )

    dist_expect = dist.expect(func=func, n=n, )
    assert 0 <= dist_expect <= dist.size, f"Expectation is invalid {dist_expect=}; expected{expect}"
    assert_allclose(expect, dist_expect, err_msg=f"Expectation is invalid {dist_expect=}; expected{expect}")


    # `grad_sum_pmf`
    # `grad_sum_log_pmf`
    # -----------------------
    for p in [rng.random(dist.size) for _ in range(5)]:

        # Success probability
        p[p<0.01] = 0.01
        p[p>=0.99] = 0.99

        # Update success probability
        _p = dist.success_probability
        dist.success_probability = p

        # Calculate gradient & reset success probability
        grad_sum_pmf = dist.grad_sum_pmf(n=n, )
        dist.success_probability = _p

        # function to evaluate pmf
        def sum_pmf_fun(p):
            _p = dist.success_probability
            dist.success_probability = p
            f = dist.sum_pmf(n=n, )
            dist.success_probability = _p
            return f

        # `grad_sum_pmf`
        pmf_p = sum_pmf_fun(p)
        fd_grad, rel_err = utility.validate_function_gradient(
            fun=sum_pmf_fun,
            state=p,
            gradient=grad_sum_pmf,
            verbose=True,
        )
        assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL
        assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL or max(abs(fd_grad-grad_sum_pmf)) < SETTINGS.GRADIENTS_ABSOLUTE_ERROR_TOL

        # `grad_sum_log_pmf`
        # This is already tested in grad_sum_pmf.


    # `grad_pmf`
    # `grad_log_pmf`
    # -----------------------
    for p in [rng.random(dist.size) for _ in range(5)]:

        # Success probability
        p[p<0.01] = 0.01
        p[p>=0.99] = 0.99

        # Update success probability
        _p = dist.success_probability
        dist.success_probability = p

        # Design sample
        min_active = np.where(dist.success_probability==1)[0].size
        if min_active <= n  <= np.count_nonzero(dist.success_probability):
            sample = dist.sample(sample_size=SETTINGS.SMALL_SAMPLE_SIZE, n=n, )
        else:
            with pytest.raises(ValueError):
                dist.sample(sample_size=SETTINGS.SMALL_SAMPLE_SIZE, n=n, )
            continue

        for d in sample:

            # Calculate gradients (exactly)
            grad_pmf = dist.grad_pmf(d, n=n, )
            grad_log_pmf = dist.grad_log_pmf(d, n=n, )

            # function to evaluate pmf
            def pmf_fun(p):
                _p = dist.success_probability
                dist.success_probability = p
                f = dist.pmf(d, n=n, )
                dist.success_probability = _p
                return f

            # `grad_pmf`
            pmf_p = pmf_fun(p)
            fd_grad, rel_err = utility.validate_function_gradient(
                fun=pmf_fun,
                state=p,
                gradient=grad_pmf,
                verbose=True
            )
            assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL or max(abs(fd_grad-grad_pmf)) < SETTINGS.GRADIENTS_ABSOLUTE_ERROR_TOL

            # `grad_log_pmf`
            # This is already tested in grad_pmf.

        # Reset success probability
        dist.success_probability = _p

    # grad_pmf and grad_log_pmf in batch mode...
    all_states = []
    all_pmf_grads = []
    all_log_pmf_grads = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        all_states.append(d)
        all_pmf_grads.append(dist.grad_pmf(d, n=n))
        all_log_pmf_grads.append(dist.grad_log_pmf(d, n=n))
    # States and gradiesnts all as rows
    all_states = np.vstack(all_states)
    all_pmf_grads = np.vstack(all_pmf_grads)
    all_log_pmf_grads = np.vstack(all_log_pmf_grads)

    # grad_pmf in batch mode
    batch_grad_pmf = dist.grad_pmf(all_states, n=n, batch_as_column=False)
    assert_allclose(batch_grad_pmf, all_pmf_grads, err_msg=f"invalid batch grad-PMF evaluation (batch_as_column=False)")
    batch_grad_pmf = dist.grad_pmf(all_states.T, n=n, batch_as_column=True)
    assert_allclose(batch_grad_pmf, all_pmf_grads.T, err_msg=f"invalid batch grad-PMF evaluation (batch_as_column=False)")
    #
    # grad_log_pmf in batch mode
    batch_grad_log_pmf = dist.grad_log_pmf(all_states, n=n, batch_as_column=False)
    assert_allclose(batch_grad_log_pmf, all_log_pmf_grads, err_msg=f"invalid batch log-grad-PMF evaluation (batch_as_column=False)")
    batch_grad_log_pmf = dist.grad_log_pmf(all_states.T, n=n, batch_as_column=True)
    assert_allclose(batch_grad_log_pmf, all_log_pmf_grads.T, err_msg=f"invalid batch log-grad-PMF evaluation (batch_as_column=True)")


    # `index_to_binary_state`
    # `index_from_binary_state`
    # -----------------------
    for k in range(1, 2**dist.size+1):
        for dtype in [bool, int, float]:
            d1 = utility.index_to_binary_state(k, size=dist.size, dtype=dtype)
            d2 = dist.index_to_binary_state(k, dtype=dtype)
            assert d1.dtype == d2.dtype == dtype, f"Ivalid retuned {d1.dtype=}, {d2.dtype=}"
            assert_allequal(d1, d2, err_msg=f"Invalid indexing in the distribution")
            assert k == dist.index_from_binary_state(d2), f"Invalid indexing in the distribution"


# Viable Budgets for the Generalized Conditional Bernoulli Model
_BUDGETS_NONE  = None
_BUDGETS_0     = [0]
_BUDGETS_1     = [1]
_BUDGETS_1_2   = [1, 2]
_BUDGETS_N     = [SETTINGS.SIZE]
_BUDGETS_ALL   = list(range(SETTINGS.SIZE))
_BUDGETS_MORE  = list(range(SETTINGS.SIZE)) + [SETTINGS.SIZE+i for i in range(1, 3)]
@pytest.mark.parametrize(
    "size, budgets",
    product(
        [0, 1, SETTINGS.SIZE],
        [_BUDGETS_NONE, _BUDGETS_0, _BUDGETS_1, _BUDGETS_1_2, _BUDGETS_N, _BUDGETS_ALL, _BUDGETS_MORE],
    )
)
def test_stats_distributions_bernoulli_GeneralizedConditionalBernoulli(size, budgets):
    """
    Test the GeneralizedConditionalBernoulli distribution
    :py:class:`pyoed.stats.distributions.conditional_bernoulli.GeneralizedConditionalBernoulli`
    """
    # Set budgets as arry to enable comparison
    if budgets is not None:
        budgets = np.asarray(budgets)

    # Create random number generator
    rng = create_random_number_generator()

    ## Test configurations incompatability upon instantiation
    if size == 0:
        with pytest.raises(PyOEDConfigsValidationError):
            create_GeneralizedConditionalBernoulli_distribution(size=size)
        return

    elif budgets is None:  # Just do it once

        # Invalid parameter values (outside [0, 1])
        success_probability = 10 * (rng.random(size) - 0.5)
        with pytest.raises(PyOEDConfigsValidationError):
            create_GeneralizedConditionalBernoulli_distribution(
                size=size,
                success_probability=success_probability,
            )
        return

    #
    ## Create a valid distribution and start testing its functionality
    dist, orig_configs = create_GeneralizedConditionalBernoulli_distribution(size=size)

    # Check distribution type
    assert isinstance(dist, GeneralizedConditionalBernoulli), f"Invalid distribution {type(dist)=}"
    assert isinstance(dist.conditional_bernoulli_model, ConditionalBernoulli), f"Invalid distribution {type(dist._CB_MODEL)=}"
    assert isinstance(dist.poisson_binomial_model, PoissonBinomial), f"Invalid distribution {type(dist._CB_MODEL)=}"
    assert isinstance(orig_configs, dict), f"Invalid configurations {type(orig_configs)=}"
    assert isinstance(dist.configurations, GeneralizedConditionalBernoulliConfigs), f"Invalid configurations {type(dist.configurations)=}"

    assert_allequal(orig_configs['parameter'], dist.parameter, err_msg=f"Invalid {dist.parameter=} of {type(dist.parameter)=}")
    assert int(dist.size) == dist.size == orig_configs['parameter'].size, f"Invlid {dist.size=} of {type(dist.size)=}"


    # When budget is None, it is replaced with all possiblities
    if budgets is None:

        assert_allequal(dist.budgets, np.arange(0, dist.size+1))
        assert len(dist.budget_sizes_probabilities) == dist.size, f"Expected list of size {dist.size}"
        assert_allclose(
            sum(dist.budget_sizes_probabilities),
            1,
            msg=f"expected total probability of 1; found {sum(dist.budget_sizes_probabilities)=}",
        )


    ##
    ## All clear at this point, there is a valid budget to be registered.

    # ===========================
    ## Test properties/attributes
    # ===========================

    # `success_probability`
    # ---------------------
    parameter = dist.success_probability
    assert_allequal(orig_configs['parameter'], parameter, err_msg=f"Invalid {parameter=} of {type(parameter)=}")

    # `size`
    # ------
    size = dist.size
    assert int(size) == size == orig_configs['parameter'].size, f"Invlid {size=} of {type(size)=}"

    # `verbose`
    # ---------
    verbose = dist.verbose
    assert bool(verbose) == verbose == orig_configs['verbose'], f"Invlid {verbose=} of {type(verbose)=}"

    # =============
    ## Test methods
    # =============
    cb_dist = dist.conditional_bernoulli_model

    # `update_configurations`
    # -----------------------
    # Get copy of the configurations
    dist_configs = dist.configurations
    cb_dist_configs = cb_dist.configurations
    assert isinstance(dist_configs, GeneralizedConditionalBernoulliConfigs), f"Invalid configurations {type(dist_configs)=}"
    assert isinstance(cb_dist_configs, ConditionalBernoulliConfigs), f"Invalid configurations {type(cb_dist_configs)=}"

    # Test original configurations first
    assert_allequal(orig_configs['parameter'].size, dist.size, err_msg=f"Invalid size")
    assert_allequal(orig_configs['parameter'].size, cb_dist.size, err_msg=f"Invalid size")

    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert_allequal(orig_configs['parameter'], cb_dist_configs.parameter, err_msg=f"Invalid parameter")

    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['random_seed'] == cb_dist_configs.random_seed, f"Invalid random seed"

    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['verbose'] == bool(cb_dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"

    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"
    assert orig_configs['name'] == cb_dist_configs.name, f"Invalid distribution name"

    new_configs = dict(
        parameter=rng.random(orig_configs['parameter'].size+1),
        name="TEST Name Update",
    )
    # * Updating configurations (1 at a time, then all at once)
    dist.update_configurations(parameter=new_configs['parameter'])
    assert dist.size == new_configs['parameter'].size, f"Invalid size after updating configs"
    assert cb_dist.size == new_configs['parameter'].size, f"Invalid size after updating configs"
    assert_allequal(dist.success_probability, new_configs['parameter'], err_msg=f"Invalid parameter after updating configs")
    assert_allequal(cb_dist.success_probability, new_configs['parameter'], err_msg=f"Invalid parameter after updating configs")

    for rs in [None, 12345]:
        dist.update_configurations(random_seed=rs, )
        assert dist.configurations.random_seed == rs, f"Invalid random seed after updating configs"
        assert cb_dist.configurations.random_seed == rs, f"Invalid random seed after updating configs"

    for verbose in [True, False]:
        dist.update_configurations(verbose=verbose, )
        assert dist.verbose == verbose, f"Invalid verbosity after updating configs"
        assert cb_dist.verbose == verbose, f"Invalid verbosity after updating configs"

    dist.update_configurations(name=new_configs['name'], )
    cb_dist_name = cb_dist.configurations.name
    assert dist.configurations.name == new_configs['name'], f"Invalid name after updating configs"
    assert cb_dist.configurations.name == cb_dist_name, f"CB Model Name was accidently modified"

    # Reset configurations & test
    dist.update_configurations(**orig_configs)
    dist_configs = dist.configurations
    cb_dist_configs = dist.conditional_bernoulli_model.configurations

    assert_allequal(orig_configs['parameter'].size, dist.size, err_msg=f"Invalid size")
    assert_allequal(orig_configs['parameter'].size, cb_dist.size, err_msg=f"Invalid size")

    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert_allequal(orig_configs['parameter'], cb_dist_configs.parameter, err_msg=f"Invalid parameter")

    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['random_seed'] == cb_dist_configs.random_seed, f"Invalid random seed"

    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['verbose'] == bool(cb_dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"

    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"
    assert orig_configs['name'] == cb_dist_configs.name, f"Invalid distribution name"

    # Register the budget if valid; otherwise everything fails
    if any(budgets<0) or any(budgets>dist.size):
        with pytest.raises(TypeError):
            dist.register_budgets(budgets)
        return
    else:
        dist.register_budgets(budgets)

    # `check_registered_budgets`
    b, pb = dist.check_registered_budgets()
    assert_allequal(b, budgets, err_msg=f"Registered budget is invalid")
    assert all([_>=0 for _ in pb]) and all([_<=1 for _ in pb]), f"invalid probabilities of sums (outside [0, 1]) {pb=} "
    assert_allclose(pb, [dist.sum_pmf(n) for n in b], err_msg=f"Probability of registered budgets is invalid")

    # `sample`
    # -----------------------
    min_active = np.where(dist.success_probability==1)[0].size
    for antithetic in [False, True]:
        for dtype in [int, bool, float]:
            if sum(dist.check_registered_budgets()[1])>0:
                sample = dist.sample(
                    sample_size=SETTINGS.SMALL_SAMPLE_SIZE,
                    dtype=dtype,
                    antithetic=antithetic,
                )
                assert isinstance(sample, np.ndarray), f"Invalid {type(sample)=}"
                assert sample.dtype == dtype, f"Invalid {sample.dtype=}"
                assert sample.shape == (SETTINGS.SMALL_SAMPLE_SIZE, dist.size), f"Invalid {sample.shape=}"
                for d in sample:
                    assert np.count_nonzero(d) in budgets, f"Sampled design is infeasible!"

            else:
                with pytest.raises(ValueError):
                    sample = dist.sample(
                        sample_size=SETTINGS.SMALL_SAMPLE_SIZE,
                        dtype=dtype,
                        antithetic=antithetic,
                    )

    # `sum_pmf`
    # -----------------------
    # Calculate PMF for all possible possible values of the sum
    tot_prob = 0.0
    for m in range(dist.size+1):
        p = dist.sum_pmf(n=m, )
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
        tot_prob += p
    assert_allclose(tot_prob, 1.0, err_msg=f"invalid Poisson-Binomial probability (total probability = {tot_prob} != 1)")

    # `sum_log_pmf`
    # -----------------------
    for m in range(dist.size+1):
        assert_allclose(np.exp(dist.sum_log_pmf(n=m, )), dist.sum_pmf(n=m, ),
                        err_msg="Invalid log-pmf of the Poisson-Binomial model")


    # `pmf`
    # -----------------------
    # Calculate PMF for all possible combinations and make sure probabilities add up to 1
    all_states = []
    all_probs = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        all_states.append(d)
        p = dist.pmf(d, )
        all_probs.append(p)
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
    all_probs = np.asarray(all_probs)
    all_states = np.vstack(all_states)
    tot_prob = np.sum(all_probs)

    # PMF in batch mode
    batch_pmf = dist.pmf(all_states, batch_as_column=False)
    assert_allclose(batch_pmf, all_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=False)")
    batch_pmf = dist.pmf(all_states.T, batch_as_column=True)
    assert_allclose(batch_pmf, all_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=True)")


    # `log_pmf`
    # -----------------------
    all_states = []
    all_log_probs = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        if np.count_nonzero(d) in dist.budgets:
            all_states.append(d)
            p = dist.log_pmf(d, )
            all_log_probs.append(p)
            assert_allclose(np.exp(p), dist.pmf(d, ), err_msg="Invalid log-pmf")
        else:
            assert np.isinf(dist.log_pmf(d, )), "Invalid log-pmf for state with invalid budget"
    all_log_probs = np.asarray(all_log_probs)
    all_states = np.vstack(all_states)

    # log-PMF in batch mode
    batch_log_pmf = dist.log_pmf(all_states, batch_as_column=False)
    assert_allclose(batch_log_pmf, all_log_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=False)")
    batch_log_pmf = dist.log_pmf(all_states.T, batch_as_column=True)
    assert_allclose(batch_log_pmf, all_log_probs, err_msg=f"invalid batch PMF evaluation (batch_as_column=True)")


    # `expect`
    # -----------------------
    func = lambda x: np.sum(x)
    expect = 0.0
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        expect += func(d) * dist.pmf(d, )

    dist_expect = dist.expect(func=func, )
    assert 0 <= dist_expect <= dist.size, f"Expectation is invalid {dist_expect=}; expected{expect}"
    assert_allclose(expect, dist_expect, err_msg=f"Expectation is invalid {dist_expect=}; expected{expect}")


    # `grad_sum_pmf`
    # `grad_sum_log_pmf`
    # -----------------------
    for p in [rng.random(dist.size) for _ in range(5)]:

        # We want PoissonBinomail gradient to match with underlying CB model
        for n in range(0, dist.size+1):
            gn1 = dist.grad_sum_pmf(n=n, )
            gn2 = dist._CB_MODEL.grad_sum_pmf(n=n, )
            assert_allclose(gn1, gn2, err_msg=f"Invalid Gradient of PoissonBinomial {gn1=}; gn2=")

    # `grad_pmf`
    # `grad_log_pmf`
    # -----------------------
    for p in [rng.random(dist.size) for _ in range(5)]:

        # Success probability
        p[p<0.01] = 0.01
        p[p>=0.99] = 0.99


        # Design sample
        min_active = np.where(dist.success_probability==1)[0].size
        if any(min_active <= budgets) and \
                any(budgets <= np.count_nonzero(dist.success_probability)):
            sample = dist.sample(sample_size=5, )
        else:
            with pytest.raises(ValueError):
                dist.sample(sample_size=5, )
            continue

        for d in sample:

            # Check the smapler comply to registered budgets!
            assert sum(d) in budgets, "Some Samples do not attain the registered budgets!"

            ## Calculate gradient (Keep success probs intact)
            _p = dist.success_probability
            dist.success_probability = p
            grad_pmf = dist.grad_pmf(d, )

            # Reset success probability
            dist.success_probability = _p

            # function to evaluate pmf
            def pmf_fun(p):
                _p = dist.success_probability
                dist.success_probability = p
                f = dist.pmf(d, )
                dist.success_probability = _p
                return f

            # `grad_pmf`
            _, rel_err = utility.validate_function_gradient(
                fun=pmf_fun,
                state=p,
                gradient=grad_pmf,
                verbose=True,
            )
            assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL

            # `grad_log_pmf`
            # This is already tested in grad_pmf.


    # grad_pmf and grad_log_pmf in batch mode...
    all_states = []
    all_pmf_grads = []
    all_log_pmf_grads = []
    for k in range(1, 2**dist.size+1):
        d = utility.index_to_binary_state(k, size=dist.size, dtype=bool)
        if np.count_nonzero(d) in dist.budgets:
            all_states.append(d)
            all_pmf_grads.append(dist.grad_pmf(d, ))
            all_log_pmf_grads.append(dist.grad_log_pmf(d, ))
        else:
            assert not np.any(dist.grad_pmf(d, )), "Invalid grad-pmf for state with invalid budget"
    # States and gradiesnts all as rows
    all_states = np.vstack(all_states)
    all_pmf_grads = np.vstack(all_pmf_grads)
    all_log_pmf_grads = np.vstack(all_log_pmf_grads)

    # grad_pmf in batch mode
    batch_grad_pmf = dist.grad_pmf(all_states, batch_as_column=False)
    assert_allclose(batch_grad_pmf, all_pmf_grads, err_msg=f"invalid batch grad-PMF evaluation (batch_as_column=False)")
    batch_grad_pmf = dist.grad_pmf(all_states.T, batch_as_column=True)
    assert_allclose(batch_grad_pmf, all_pmf_grads.T, err_msg=f"invalid batch grad-PMF evaluation (batch_as_column=False)")
    #
    # grad_log_pmf in batch mode
    batch_grad_log_pmf = dist.grad_log_pmf(all_states, batch_as_column=False)
    assert_allclose(batch_grad_log_pmf, all_log_pmf_grads, err_msg=f"invalid batch log-grad-PMF evaluation (batch_as_column=False)")
    batch_grad_log_pmf = dist.grad_log_pmf(all_states.T, batch_as_column=True)
    assert_allclose(batch_grad_log_pmf, all_log_pmf_grads.T, err_msg=f"invalid batch log-grad-PMF evaluation (batch_as_column=True)")


    # `index_to_binary_state`
    # `index_from_binary_state`
    # -----------------------
    for k in range(1, 2**dist.size+1):
        for dtype in [bool, int, float]:
            d1 = utility.index_to_binary_state(k, size=dist.size, dtype=dtype)
            d2 = dist.index_to_binary_state(k, dtype=dtype)
            assert d1.dtype == d2.dtype == dtype, f"Ivalid retuned {d1.dtype=}, {d2.dtype=}"
            assert_allequal(d1, d2, err_msg=f"Invalid indexing in the distribution")
            assert k == dist.index_from_binary_state(d2), f"Invalid indexing in the distribution"


