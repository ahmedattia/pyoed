# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Test conditional bernoulli model in `pyoed.stats.distributions.conditional_bernoulli`
"""

import numpy as np
from itertools import (
    product,
)

from pyoed.stats.distributions.poisson_binomial import (
    PoissonBinomial,
    PoissonBinomialConfigs,
)
from pyoed import utility
from pyoed.tests import (
    SETTINGS as _SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)
from pyoed.configs import PyOEDConfigsValidationError


class SETTINGS:
    """Local settings"""
    RANDOM_SEED = _SETTINGS.RANDOM_SEED
    SIZE = _SETTINGS.OBSERVATION_SIZE
    SMALL_SAMPLE_SIZE = 32
    MODERATE_SAMPLE_SIZE = 100
    LARGE_SAMPLE_SIZE = 10000
    GRADIENTS_RELATIVE_ERROR_TOL = 1e-2

import pytest
pytestmark = [pytest.mark.stats, pytest.mark.distributions]


# Fixtures:
# =========
def create_PoissonBinomial_distribution(
    method,
    size=SETTINGS.SIZE,
    success_probability=None,
    random_seed=SETTINGS.RANDOM_SEED,
):
    """
    Create a Poisson Binomial distribution with Series (recursive) evaluation
    of probabilities and derivatives (instance of :py:class:`PoissonBinomial`)

    :returns: both the distribution and the configurations dictionary
      used to create the distribution
    """
    # Create success probabilities (if not passed)
    if success_probability is None:
        rng = create_random_number_generator() # Create random number generator
        success_probability = np.round(rng.random(int(size)), 3)
        # Add degeneracy
        if size > 2:
            deg_locs = rng.choice(range(size), size=2, replace=False)
            success_probability[deg_locs[0]] = 0
            success_probability[deg_locs[1]] = 1

    configs = dict(
        R_function_evaluation_method=method,
        parameter=success_probability,
        verbose=False,
        random_seed=random_seed,
        name="Testing PoissonBinomial",
    )

    dist = PoissonBinomial(configs=configs)

    return dist, configs


@pytest.mark.parametrize(
    "size, method", product([0, 1, SETTINGS.SIZE, SETTINGS.SIZE*2], ['tabulation', 'recursion']),
)
def test_stats_distributions_poisson_binomial_PoissonBinomial(size, method, ):
    """
    Test the PoissonBinomial distribution
    :py:class:`pyoed.stats.distributions.poisson_binomial.PoissonBinomial`
    """

    # Create random number generator
    rng = create_random_number_generator()

    ## Test configurations incompatability upon instantiation
    if size == 0:
        with pytest.raises(PyOEDConfigsValidationError):
            create_PoissonBinomial_distribution(size=size, method=method, )
        return

    else:

        # Invalid parameter values (outside [0, 1])
        success_probability = 10 * (rng.random(size) - 0.5)
        with pytest.raises(PyOEDConfigsValidationError):
            create_PoissonBinomial_distribution(success_probability=success_probability, method=method, )

    #
    ## Create a valid distribution and start testing its functionality
    dist, orig_configs = create_PoissonBinomial_distribution(size=size, method=method, )

    # Check distribution type
    assert isinstance(dist, PoissonBinomial), f"Invalid distribution {type(dist)=}"
    assert isinstance(orig_configs, (dict, PoissonBinomialConfigs)), f"Invalid configurations {type(orig_configs)=}"


    # ===========================
    ## Test properties/attributes
    # ===========================

    # `success_probability`
    # ---------------------
    parameter = dist.success_probability
    assert_allequal(orig_configs['parameter'], parameter, err_msg=f"Invalid {parameter=} of {type(parameter)=}")

    # `size`
    # ------
    size = dist.size
    assert int(size) == size == orig_configs['parameter'].size, f"Invlid {size=} of {type(size)=}"

    # `verbose`
    # ---------
    verbose = dist.verbose
    assert bool(verbose) == verbose == orig_configs['verbose'], f"Invlid {verbose=} of {type(verbose)=}"


    # =============
    ## Test methods
    # =============

    # `update_configurations`
    # -----------------------
    # Get copy of the configurations
    dist_configs = dist.configurations
    assert isinstance(dist_configs, (dict, PoissonBinomialConfigs)), f"Invalid configurations {type(orig_configs)=}"

    # Test original configurations first
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"
    assert orig_configs['R_function_evaluation_method'] == dist_configs.R_function_evaluation_method, f"Invalid evaluation method"

    new_configs = dict(
        parameter=rng.random(orig_configs['parameter'].size+1),
        name="TEST Name Update",
    )
    # * Updating configurations (1 at a time, then all at once)
    dist.update_configurations(parameter=new_configs['parameter'])
    assert dist.size == new_configs['parameter'].size, f"Invalid size after updating configs"
    assert_allequal(dist.success_probability, new_configs['parameter'], err_msg=f"Invalid parameter after updating configs")

    for rs in [None, 12345]:
        dist.update_configurations(random_seed=rs, )
        assert dist.configurations.random_seed == rs, f"Invalid random seed after updating configs"

    for verbose in [True, False]:
        dist.update_configurations(verbose=verbose, )
        assert dist.verbose == verbose, f"Invalid verbosity after updating configs"

    dist.update_configurations(name=new_configs['name'], )
    assert dist.configurations.name == new_configs['name'], f"Invalid name after updating configs"

    # Reset configurations & test
    dist.update_configurations(**orig_configs)
    dist_configs = dist.configurations
    assert_allequal(orig_configs['parameter'], dist_configs.parameter, err_msg=f"Invalid parameter")
    assert orig_configs['random_seed'] == dist_configs.random_seed, f"Invalid random seed"
    assert orig_configs['verbose'] == bool(dist_configs.verbose) == dist_configs.verbose, f"Invalid verbosity"
    assert orig_configs['name'] == dist_configs.name, f"Invalid distribution name"
    assert orig_configs['R_function_evaluation_method'] == dist_configs.R_function_evaluation_method, f"Invalid evaluation method"


    # `sample`
    # -----------------------
    min_active = np.where(dist.success_probability==1)[0].size
    sample = dist.sample(
        sample_size=SETTINGS.SMALL_SAMPLE_SIZE,
    )
    assert isinstance(sample, np.ndarray), f"Invalid {type(sample)=}"
    assert np.ndim(sample) == 1 and sample.size== SETTINGS.SMALL_SAMPLE_SIZE, f"Invalid {sample.shape=}"

    # `pmf`
    # -----------------------
    # Calculate PMF for all possible possible values of the sum
    tot_prob = 0.0
    for m in range(dist.size+1):
        p = dist.pmf(n=m, )
        assert 0 <=p <= 1, f"Invalid probability (PMF value) {p=}"
        tot_prob += p
    assert_allclose(tot_prob, 1.0, err_msg=f"invalid Poisson-Binomial probability (total probability = {tot_prob} != 1)")


    # `log_pmf`
    # -----------------------
    for m in range(dist.size+1):
        assert_allclose(np.exp(dist.log_pmf(n=m, )), dist.pmf(n=m, ),
                        err_msg="Invalid log-pmf of the Poisson-Binomial model")

    # `expect`
    # -----------------------
    func = lambda n: n**2+1.0
    expect = 0.0
    for m in range(dist.size+1):
        expect += func(m) * dist.pmf(n=m, )

    dist_expect = dist.expect(func=func, )
    assert_allclose(expect, dist_expect, err_msg=f"Expectation is invalid {dist_expect=}; expected{expect}")


    # `grad_pmf`
    # `grad_log_pmf`
    # -----------------------
    for p in [np.round(rng.random(dist.size), 3) for _ in range(5)]:

        # Success probability
        p[p<0.05] = 0.05
        p[p>=0.95] = 0.95

        # Update success probability
        _p = dist.success_probability
        dist.success_probability = p

        # Evaluate the gradient for multiple choices of n
        # for m in rng.choice(range(dist.size+1), size=min(dist.size+1, 10), replace=False):
        for m in [max(0, dist.size//2+i) for i in [-2, 0, 2]]:

            # Calculate gradient & reset success probability
            grad_pmf = dist.grad_pmf(n=m)
            # dist.success_probability = _p

            # function to evaluate pmf
            def pmf_fun(p):
                _p = dist.success_probability
                dist.success_probability = p
                f = dist.pmf(n=m, )
                dist.success_probability = _p
                return f

            # Validate `grad_pmf` with finite differences
            pmf_p = pmf_fun(p)
            # grad_eps = 1e-6
            _, rel_err = utility.validate_function_gradient(
                fun=pmf_fun,
                state=p,
                gradient=grad_pmf,
                verbose=True,
            )
            assert max(abs(rel_err)) < SETTINGS.GRADIENTS_RELATIVE_ERROR_TOL

