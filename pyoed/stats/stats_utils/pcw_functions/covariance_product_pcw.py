from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


bounds = (0.0, 1.0)


def kernel(design, i, j):
    if i == j:
        if design[i] == 0 or design[j] == 0:
            weight = 0
        else:
            weight = 1.0 / (design[i] * design[j])
    else:
        weight = design[i] * design[j]

    return weight


def kernel_grad(design, i, j, k):
    if design[i] == 0 or design[j] == 0:
        weight = 0
    else:
        if k == i == j:
            weight = -2.0 / np.power(design[i], 3)
        elif k == i != j:
            weight = design[j]
        elif k == j != i:
            weight = design[i]
        else:
            weight = 0
    return weight


CovarianceProductPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
