from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


bounds = (0.0, 1.0)


def kernel(design, i, j):
    if design[i] < 0 or design[j] < 0:
        raise ValueError("Can't use square root kernel with negative design value!")
    if i == j:
        if design[i] == 0 or design[j] == 0:
            weight = 0
        else:
            weight = 1.0 / (np.sqrt(design[i]) * np.sqrt(design[j]))
    else:
        weight = np.sqrt(design[i]) * np.sqrt(design[j])
    return weight


def kernel_grad(design, i, j, k):
    if design[i] == 0 or design[j] == 0:
        weight = 0
    else:
        if k == i == j:
            weight = -1.0 / np.power(design[i], 2)
        elif k == i != j:
            weight = 0.5 * np.sqrt(design[j] / design[i])
        elif k == j != i:
            weight = 0.5 * np.sqrt(design[i] / design[j])
        else:
            weight = 0
    return weight


CovarianceSqrtPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
