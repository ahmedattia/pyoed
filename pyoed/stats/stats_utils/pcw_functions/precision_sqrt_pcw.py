from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


def kernel(design, i, j):
    weight = np.sqrt(design[i] * design[j])
    return weight


bounds = (0.0, 1.0)


def kernel_grad(design, i, j, k):
    if k == i == j:
        weight = 1.0
    elif k == i:
        if design[i] == 0:
            weight = 0
        else:
            weight = 0.5 * np.sqrt(design[j] / design[i])
    elif k == j:
        if design[i] == 0:
            weight = 0
        else:
            weight = 0.5 * np.sqrt(design[i] / design[j])
    else:
        weight = 0
    return weight


PrecisionSqrtPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
