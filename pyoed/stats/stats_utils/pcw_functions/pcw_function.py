from dataclasses import dataclass
import re
from typing import Callable

import numpy as np

from ....configs import (
    PyOEDConfigs,
    PyOEDObject,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
)


@dataclass(kw_only=True, slots=True)
class PCWFunctionConfigs(PyOEDConfigs):
    """
    Configuration class for PCWFunction.

    :param kernel: Callable function that computes the kernel value.
    :param kernel_grad: Callable function that computes the gradient of the kernel
        value.
    :param bounds: Tuple of the lower and upper bounds of the kernel.
    """

    kernel: Callable[[np.ndarray, int, int], float] | None = None
    kernel_grad: Callable[[np.ndarray, int, int, int], float] | None = None
    bounds: tuple[float, float] | None = None
    name: str | None = None
    name_regex: re.Pattern | None = None


@set_configurations(PCWFunctionConfigs)
class PCWFunction(PyOEDObject):
    """
    Class for pointwise covariance weighting functions used in OED, a.k.a. functions
    that evaluates the weighting kernel of observation error covariances in the
    pointwise/Shur approach.

    .. note::

        When the design is relaxed, we allow the design (or more generally, the weights)
        to take values in the interval [0, 1]. For a zero design, we wish to remove the
        corresponding row/column from the covariance matrix if we follow a covariance-*
        weighting.  Similarly, we remove the corresponding row/column from the precision
        matrix if we follow a precision-* weighting approach.

        How do we interpret the behaviour?

        A zero (discard the sensor) should be considered when the sensor is useless,
        e.g., the associated uncertainty is too high, while a one (activate the sensor)
        is considered when the  sensor is sharp  (has very low uncertainty). Thus,
        increasing  covariances should lead to a zero weight, while decreasing variances
        should point design=1.  This means when we relax the design, we should switch
        the end points of the domain, that is [1] <-- (0  <-- , --> 1) --> [0]. This can
        be counter-intuitive, however, one can think of it in terms of the observations
        rather than uncertainties. Discarding an observation, means projecting  the
        parameter-to-observable map (F) onto a lower dimensional space.  This means that
        a zero weight corresponds to removing a row/column from the precision matrix
        while design = 1 means keep that row/column. Higher weight in this case should
        attain 1, and  lower weight should attain zero because weights here correspond
        to precisions rather than uncretainties (also correspond to keep/remove
        observation entries).  This actually, matches our intuition above about
        weighting covariances.  + In all cases, when we relax the design/weights, we
        construct a relaxed surface that joins the corner points (or approximation
        thereof), and use that surface to approximate the solution of the original
        binary optimization problem.
    """

    def __init__(self, configs: dict | PCWFunctionConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self, configs: dict | PCWFunctionConfigs, raise_for_invalid: bool = True
    ) -> bool:
        aggregated_configs = self.aggregate_configurations(configs)

        if aggregated_configs.kernel is None:
            raise PyOEDConfigsValidationError("Kernel must be provided!")

        for key in ["kernel", "kernel_grad"]:
            if not validate_key(
                aggregated_configs,
                configs,
                key=key,
                test=lambda x: callable(x),
                message=f"{key.capitalize()} must be a callable function.",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        if not validate_key(
            aggregated_configs,
            configs,
            key="kernel",
            test=lambda k: _validate_pcw_grad(k, aggregated_configs.kernel_grad),
            message="kernel and kernel_grad do not pass a FD gradient check.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if aggregated_configs.kernel_grad is None:
            raise PyOEDConfigsValidationError("Kernel gradient must be provided!")

        if not validate_key(
            aggregated_configs,
            configs,
            key="kernel_grad",
            test=lambda kg: _validate_pcw_grad(aggregated_configs.kernel, kg),
            message="kernel and kernel_grad do not pass a FD gradient check.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if aggregated_configs.bounds is None:
            raise PyOEDConfigsValidationError("Bounds must be provided!")

        if not validate_key(
            aggregated_configs,
            configs,
            key="bounds",
            test=lambda x: isinstance(x, tuple) and len(x) == 2,
            message="Bounds must be a tuple of two floats.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def kernel(self, design: np.ndarray, i: int, j: int) -> float:
        return self.configurations.kernel(design, i, j)

    def kernel_grad(self, design: np.ndarray, i: int, j: int, k: int) -> float:
        return self.configurations.kernel_grad(design, i, j, k)

    def bounds(self) -> tuple[float, float]:
        return self.configurations.bounds


def _validate_pcw_grad(
    kernel,
    kernel_grad,
    size=5,
    fd_eps=1e-6,
    err_tol=1e-3,
    random_seed=None,
) -> bool:
    """
    Given a pointwise weighting kernel `kernel` check that it is of a valid type,
    here `PCWFunction`, and validate it evaluation function and gradient
    using finite differences

    :param kernel: the pointwise weighting kernel
    :param kernel_grad: the gradient of the pointwise weighting kernel
    :param size: the size to assume fo the covariance matrix (must be >=2).
    :param fd_eps: the finite difference `epsilon` value. (we use one sided FD here)
    :param err_tol: the error tolerance for the validation
    :param [int | None] random_seed: seed to use for the random number geneerator which
        is used to generate testing vectors for the kernel

    :returns: a boolean flag `is_valid` indicating whether the kernel is valid or not
    """
    rng = np.random.default_rng(random_seed)

    # Validate the kernel and kernel_grad using finite differences
    _i = 0
    _j = size - 1
    _k = int((_i + _j) // 2)
    comb = [(_i, _j, _i), (_i, _j, _j), (_i, _j, _k)]

    d = rng.random(size)
    dp = d.copy()
    for i, j, k in comb:
        f0 = kernel(d, i, j)
        dp[k] += fd_eps
        f1 = kernel(dp, i, j)
        dp[k] -= fd_eps
        fd = (f1 - f0) / fd_eps
        g = kernel_grad(d, i, j, k)
        err = (g - fd) / fd
        if err > err_tol and not (g==fd==0):
            print("Kernel FD Check: -> ", g, fd, err)
            return False
    return True
