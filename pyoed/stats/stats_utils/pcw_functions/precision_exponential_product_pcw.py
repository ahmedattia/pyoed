from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


def kernel(design, i, j):
    if i == j:
        weight = np.exp(-design[i] - design[j])
    else:
        weight = 1.0 / np.exp(-design[i] - design[j])
    return weight


bounds = (-5.0, 0.0)


def kernel_grad(design, i, j, k):
    if k == i == j:
        weight = -2.0 * np.exp(-2.0 * design[i])
    elif k == i != j or k == j != i:
        weight = np.exp(design[i] + design[j])
    else:
        weight = 0
    return weight


PrecisionExponentialProductPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)

