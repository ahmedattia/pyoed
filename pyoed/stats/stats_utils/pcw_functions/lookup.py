from enum import StrEnum
from re import (
    compile,
    IGNORECASE,
    Pattern,
)

from .pcw_function import PCWFunction, PCWFunctionConfigs

from .covariance_product_pcw import CovarianceProductPCWConfigs
from .covariance_sqrt_pcw import CovarianceSqrtPCWConfigs
from .covariance_sigmoid_product_pcw import CovarianceSigmoidProductPCWConfigs
from .covariance_sigmoid_sqrt_product_pcw import CovarianceSigmoidSqrtProductPCWConfigs
from .covariance_exponential_product_pcw import CovarianceExponentialProductPCWConfigs
from .covariance_exponential_sqrt_product_pcw import (
    CovarianceExponentialSqrtProductPCWConfigs,
)
from .precision_product_pcw import PrecisionProductPCWConfigs
from .precision_sqrt_pcw import PrecisionSqrtPCWConfigs
from .precision_sigmoid_product_pcw import PrecisionSigmoidProductPCWConfigs
from .precision_sigmoid_sqrt_product_pcw import PrecisionSigmoidSqrtProductPCWConfigs
from .precision_exponential_product_pcw import PrecisionExponentialProductPCWConfigs
from .precision_exponential_sqrt_product_pcw import (
    PrecisionExponentialSqrtProductPCWConfigs,
)


class PREBUILT_PCW_FUNCTIONS(StrEnum):
    COVARIANCE_PRODUCT = "covariance-product"
    COVARIANCE_SQRT = "covariance-sqrt"
    COVARIANCE_SIGMOID_PRODUCT = "covariance-sigmoid-product"
    COVARIANCE_SIGMOID_SQRT_PRODUCT = "covariance-sigmoid-sqrt-product"
    COVARIANCE_EXPONENTIAL_PRODUCT = "covariance-exponential-product"
    COVARIANCE_EXPONENTIAL_SQRT_PRODUCT = "covariance-exponential-sqrt-product"
    PRECISION_PRODUCT = "precision-product"
    PRECISION_SQRT = "precision-sqrt"
    PRECISION_SIGMOID_PRODUCT = "precision-sigmoid-product"
    PRECISION_SIGMOID_SQRT_PRODUCT = "precision-sigmoid-sqrt-product"
    PRECISION_EXPONENTIAL_PRODUCT = "precision-exponential-product"
    PRECISION_EXPONENTIAL_SQRT_PRODUCT = "precision-exponential-sqrt-product"


_ACCEPTABLE_PCW_NAME_REGEX: dict[Pattern, PCWFunctionConfigs] = {
    compile(
        r"\Acov(ariance)*(-| |_)*prod(uct)*\Z", IGNORECASE
    ): CovarianceProductPCWConfigs,
    compile(
        r"\Acov(ariance)*(-| _)*(sqrt|(square(-| |_)*root))\Z",
        IGNORECASE,
    ): CovarianceSqrtPCWConfigs,
    compile(
        r"\Acov(ariance)*(-| |_)*sigmoid(-| |_)*prod(uct)*\Z", IGNORECASE
    ): CovarianceSigmoidProductPCWConfigs,
    compile(
        r"\Acov(ariance)*(-| |_)*sigmoid(-| |_)*(sqrt|(square(-| |_)*root))(-| |_)*prod(uct)*\Z",
        IGNORECASE,
    ): CovarianceSigmoidSqrtProductPCWConfigs,
    compile(
        r"\Acov(ariance)*(-| |_)*exp(onent|onential)*(-| |_)*prod(uct)*\Z", IGNORECASE
    ): CovarianceExponentialProductPCWConfigs,
    compile(
        r"\Acov(ariance)*(-| _)*exp(onent|onential)*(-| |_)*(sqrt|(square(-|"
        r" |_)*root))(-| |_)*prod(uct)*\Z",
        IGNORECASE,
    ): CovarianceExponentialSqrtProductPCWConfigs,
    compile(
        r"\Aprec(ision)*(-| |_)*prod(uct)*\Z", IGNORECASE
    ): PrecisionProductPCWConfigs,
    compile(
        r"\Aprec(ision)*(-| |_)*(sqrt|(square(-| |_)*root))\Z",
        IGNORECASE,
    ): PrecisionSqrtPCWConfigs,
    compile(
        r"\Aprec(ision)*(-| |_)*sigmoid(-| |_)*prod(uct)*\Z", IGNORECASE
    ): PrecisionSigmoidProductPCWConfigs,
    compile(
        r"\Aprec(ision)*(-| |_)*sigmoid(-| |_)*(sqrt|(square(-| |_)*root))(-| |_)*prod(uct)*\Z",
        IGNORECASE,
    ): PrecisionSigmoidSqrtProductPCWConfigs,
    compile(
        r"\Aprec(ision)*(-| |_)*exp(onent|onential)*(-| |_)*prod(uct)*\Z", IGNORECASE
    ): PrecisionExponentialProductPCWConfigs,
    compile(
        (
            r"\Aprec(ision)*(-| _)*exp(onent|onential)*(-| |_)*(sqrt|(square(-|"
            r" |_)*root))(-| |_)*prod(uct)*\Z"
        ),
        IGNORECASE,
    ): PrecisionExponentialSqrtProductPCWConfigs,
}


def get_available_pcw_functions() -> list[str]:
    """
    Get the available pointwise covariance weighting kernels.

    :return: a set of available pointwise covariance weighting kernels.
    """
    return [p.value for p in PREBUILT_PCW_FUNCTIONS]


def get_pcw_function(kernel_name):
    """
    Helper function to get a pointwise covariance weighting function and its
    bounds from its name.

    :param str kernel_name: name of the weighting kernel. Call
        :py:func:`get_available_pcw_functions` to get the list of available kernels.

    :return pcw_function: Corresponding pointwise covariance weighting function.
    """
    for pattern, pcw_config in _ACCEPTABLE_PCW_NAME_REGEX.items():
        if pattern.match(kernel_name):
            return PCWFunction(pcw_config)

    raise ValueError(
        f"Unrecognized kernel name: '{kernel_name}'\n"
        "Supported are the following schemes:\n"
        "\n".join(get_available_pcw_functions())
    )
