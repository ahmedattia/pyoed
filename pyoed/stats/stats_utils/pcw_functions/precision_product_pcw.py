from re import compile, IGNORECASE
from .pcw_function import PCWFunctionConfigs


bounds = (0.0, 1.0)


def kernel(design, i, j):
    weight = design[i] * design[j]
    return weight


def kernel_grad(design, i, j, k):
    if k == i == j:
        weight = 2 * design[i]
    elif k == i:
        weight = design[j]
    elif k == j:
        weight = design[i]
    else:
        weight = 0
    return weight


PrecisionProductPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
