from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


def kernel(design, i, j):
    sigmoid = lambda x: 1.0 / (1 + np.exp(-x))
    weight = sigmoid(design[i]) * sigmoid(design[j])
    return weight


bounds = (-10.0, 10.0)


def kernel_grad(design, i, j, k):
    sigmoid = lambda x: 1.0 / (1 + np.exp(-x))
    if k == i == j:
        si = sigmoid(design[i])
        weight = 2 * np.power(si, 2) * (1.0 - si)
    elif k == i:
        si = sigmoid(design[i])
        sj = sigmoid(design[j])
        weight = si * (1.0 - si) * sj
    elif k == j:
        si = sigmoid(design[i])
        sj = sigmoid(design[j])
        weight = sj * (1.0 - sj) * si
    else:
        weight = 0
    return weight


PrecisionSigmoidProductPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
