# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Package loader for the pcw_functions module.
PCW (Pointwise Covariance Weighting) is a specialized model that
applies pointwise weighting to covariance matrices in support for
relaxed design.
"""

import numpy as np

from .pcw_function import PCWFunction, PCWFunctionConfigs
from .lookup import (
    PREBUILT_PCW_FUNCTIONS,
    get_available_pcw_functions,
    get_pcw_function,
)

from .covariance_product_pcw import CovarianceProductPCWConfigs
from .covariance_sqrt_pcw import CovarianceSqrtPCWConfigs
from .covariance_sigmoid_product_pcw import CovarianceSigmoidProductPCWConfigs
from .covariance_sigmoid_sqrt_product_pcw import CovarianceSigmoidSqrtProductPCWConfigs
from .covariance_exponential_product_pcw import CovarianceExponentialProductPCWConfigs
from .covariance_exponential_sqrt_product_pcw import (
    CovarianceExponentialSqrtProductPCWConfigs,
)
from .precision_product_pcw import PrecisionProductPCWConfigs
from .precision_sqrt_pcw import PrecisionSqrtPCWConfigs
from .precision_sigmoid_product_pcw import PrecisionSigmoidProductPCWConfigs
from .precision_sigmoid_sqrt_product_pcw import PrecisionSigmoidSqrtProductPCWConfigs
from .precision_exponential_product_pcw import PrecisionExponentialProductPCWConfigs
from .precision_exponential_sqrt_product_pcw import (
    PrecisionExponentialSqrtProductPCWConfigs,
)

