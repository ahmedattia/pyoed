from re import compile, IGNORECASE
import numpy as np
from .pcw_function import PCWFunctionConfigs


def kernel(design, i, j):
    sigmoid = lambda x: 1.0 / (1 + np.exp(-x))
    if i == j:
        weight = 1.0 / np.sqrt(sigmoid(design[i]) * sigmoid(design[j]))
    else:
        weight = np.sqrt(sigmoid(design[i]) * sigmoid(design[j]))
    return weight


bounds = (-10, 10)


def kernel_grad(design, i, j, k):
    sigmoid = lambda x: 1.0 / (1 + np.exp(-x))
    if k == i == j:
        s = sigmoid(design[i])
        weight = 1.0 - 1.0 / s
    elif k == i != j:
        si = sigmoid(design[i])
        sj = sigmoid(design[j])
        weight = 0.5 * np.sqrt(si * sj) * (1.0 - si)
    elif k == j != i:
        si = sigmoid(design[i])
        sj = sigmoid(design[j])
        weight = 0.5 * np.sqrt(si * sj) * (1.0 - sj)
    else:
        weight = 0
    return weight


CovarianceSigmoidSqrtProductPCWConfigs = PCWFunctionConfigs(
    kernel=kernel,
    kernel_grad=kernel_grad,
    bounds=bounds,
)
