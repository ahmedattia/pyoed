# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from dataclasses import dataclass
from enum import StrEnum
import time
from typing import Callable
import re

import numpy as np
from scipy import sparse as sp

from pyoed.configs import (
    set_configurations,
    validate_key,
    PyOEDData,
    PyOEDConfigsValidationError,
    SETTINGS,
)
from pyoed.utility import (
    isnumber,
    factorize_spsd_matrix,
    plot_sampling_results,
)
from pyoed.utility.mixins import RandomNumberGenerationMixin
from pyoed.stats.core.sampling import (
    Proposal,
    Sampler,
    SamplerConfigs,
)
from .proposals import (
    GaussianProposal,
    GaussianProposalConfigs,
)


@dataclass(kw_only=True, slots=True)
class ChainDiagnostics(PyOEDData):
    """
    A dataclass to hold the diagnostics of the Markov chain

    :param acceptance_rate: the rate of acceptance of the proposed samples
    :param rejection_rate: the rate of rejection of the proposed samples
    """

    acceptance_rate: float
    rejection_rate: float

    def __str__(self):
        sep = "*" * 40
        return (
            f"\n{sep}\nChain Diagnostics\n{sep}\n"
            f"Acceptance Rate: {self.acceptance_rate:.2f}\n"
            f"Rejection Rate: {self.rejection_rate:.2f}\n"
        )


@dataclass(kw_only=True, slots=True)
class SamplingResults(PyOEDData):
    """
    A dataclass to hold the results of the MCMC sampling

    :param chain_state_repository: a list of all generated states in the Markov chain
    :param collected_ensemble: a list of all collected samples from the Markov chain
    :param proposals_repository: a list of all proposed states in the Markov chain
    :param acceptance_flags: a list of flags indicating whether the proposed states were
        accepted or not
    :param acceptance_probabilities: a list of probabilities of accepting the proposed
        states
    :param uniform_random_numbers: a list of uniform random numbers used in the
        acceptance step
    :param map_estimate: the MAP estimate of the distribution
    :param map_estimate_log_density: the log-density of the MAP estimate
    :param chain_diagnostics: the diagnostics of the Markov chain
    :param chain_time: the time taken to generate the Markov chain
    """

    chain_state_repository: list[np.ndarray]
    collected_ensemble: list[np.ndarray]
    proposals_repository: list[np.ndarray]
    acceptance_flags: list[int]
    acceptance_probabilities: list[float]
    uniform_random_numbers: list[float]
    map_estimate: np.ndarray
    map_estimate_log_density: float
    chain_diagnostics: ChainDiagnostics | None
    chain_time: float

    def __str__(self):
        sep = "*" * 40
        return (
            f"\n{sep}\nSampling Results\n{sep}\n"
            f"MAP Estimate: {self.map_estimate}\n"
            f"MAP Estimate Log-Density: {self.map_estimate_log_density}\n"
            f"Chain Time: {self.chain_time:.2f} seconds\n"
            f"{self.chain_diagnostics}\n"
            "Inspect the object for more details\n"
        )


@dataclass(kw_only=True, slots=True)
class MCMCSamplerConfigs(SamplerConfigs):
    """
    Configurations for the MCMC sampler :py:class:`MCMCSampler`.

    :param size: dimension of the target distribution to sample
    :param log_density: log of the (unscaled) density function to be sampled
    :param burn_in: number of sample points to discard before collecting samples
    :param mix_in: number of generated samples between accepted ones (to descrease
        autocorrelation)
    :param proposal: a proposal object to be used for generating new samples
    :param constraint_test: a function that returns a boolean value `True` if sample
        point satisfy any constrints, and `False` otherwise; ignored if `None`, is
        passed.
    """

    size: int | None = None
    log_density: Callable[[np.ndarray], float] | None = None
    burn_in: int = 500
    mix_in: int = 10
    proposal: Proposal | None = None
    constraint_test: Callable[[np.ndarray], bool] | None = None


@set_configurations(MCMCSamplerConfigs)
class MCMCSampler(Sampler):
    """
    Basic class for MCMC sampling with a chosen (Default is a Gaussian) proposal
    """

    def __init__(self, configs: dict | MCMCSamplerConfigs | None = None):
        """
        Implementation of the MCMC sampling algorithm with a predefined proposal

        :param configs: a configurations object. See :py:class:`MCMCSamplerConfigs`.
        """
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Run initialization logic.
        self.proposal = self.configurations.proposal
        self.update_random_number_generators(
            random_seed=self.configurations.random_seed, update_proposal=True
        )

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        A method to check the passed configuratios and make sure they are conformable
        with each other, and with current configurations once combined. This guarantees
        that any key-value pair passed in configs can be properly used.

        :param dict configs: a dictionary holding key/value configurations
        :param bool raise_for_invalid: if `True` raise :py:class:`TypeError` for
          invalid configrations type/key

        :returns: True/False flag indicating whether passed coinfigurations dictionary
            is valid or not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        is_pos_int = lambda x: isnumber(x) and (int(x) == x) and (x > 0)

        for param in ["size", "burn_in", "mix_in"]:
            if not validate_key(
                aggregated_configs,
                configs,
                param,
                test=is_pos_int,
                message=f"{param} must be a positive integer",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        if not validate_key(
            aggregated_configs,
            configs,
            "log_density",
            test=callable,
            message="log_density must be a callable function",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraint_test",
            test=lambda x: callable(x) or (x is None),
            message="constraint_test must be a callable function or None",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "proposal",
            test=lambda x: isinstance(x, Proposal) or (x is None),
            message="proposal must be an instance of class `Proposal` or None",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # TODO: What is that????
        if not validate_key(
            aggregated_configs,
            configs,
            "proposal",
            test=lambda x: x.size == aggregated_configs.size,
            message="proposal size must match the size of the target distribution",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        self.validate_configurations(kwargs, raise_for_invalid=True)

        # Configurations with side effects
        if v := kwargs.pop("random_seed", None):
            self.update_random_number_generators(random_seed=v, update_proposal=True)
        if v := kwargs.pop("proposal", None):
            self.proposal = v
        if v := kwargs.pop("constraint_test", None):
            self.constraint_test = v

        # Update the rest of the configurations
        if v := kwargs.pop("log_density", None):
            self.configurations.log_density = v
        if v := kwargs.pop("burn_in", None):
            self.burn_in = v
        if v := kwargs.pop("mix_in", None):
            self.mix_in = v

        # Unsettable configurations
        if "size" in kwargs:
            raise NotImplementedError("Size cannot be updated after initialization!")

        super().update_configurations(**kwargs)

    def generate_white_noise(self, size, truncate=False):
        """
        Generate a standard normal random vector of size `size` with values truncated at
        -/+3 if `truncate` is set to `True`

        :returns: a numpy array of size `size` sampled from a standard multivariate
            normal distribution of dimension `size` with mean 0 and covariance matrix
            equals an identity matrix.

        :remarks:
            - this function returns a numpy array of size `size` even if `size` is set to 1
        """
        # Sample
        randn_vec = self.random_number_generator.standard_normal(size)
        if truncate:
            randn_vec[randn_vec > 3] = 3
            randn_vec[randn_vec < -3] = -3
        return randn_vec

    def log_density(self, state) -> float:
        """
        Evaluate the value of the logarithm of the target unscaled posterior density function
        """
        val = self.configurations.log_density(state)
        if isinstance(val, np.ndarray) and (val.size == 1):
            val = val.item()
        return val

    def sample(
        self,
        sample_size=1,
        initial_state=None,
        full_diagnostics=False,
    ):
        """
        Generate and return a sample of size `sample_size`.
        This method returns a list with each entry representing a sample point
        from the underlying distribution

        :param int sample_size:
        :param initial_state:
        :param bool full_diagnostics: if `True` all generated states will be tracked
          and kept for full disgnostics, otherwise, only collected samples are kept in memory
        :param bool verbose:
        """
        mcmc_results = self.start_MCMC_sampling(
            sample_size=sample_size,
            initial_state=initial_state,
            full_diagnostics=full_diagnostics,
        )
        return mcmc_results.collected_ensemble

    def map_estimate(
        self,
        sample_size=100,
        initial_state=None,
        full_diagnostics=False,
    ):
        """
        Search for a MAP (maximum aposteriori) estimate by sampling (space exploration)
        This method returns a single-point estimate of the MAP of the distribution.

        :param sample_size:
        :param initial_state:
        :param full_diagnostics:
        """
        results = self.start_MCMC_sampling(
            sample_size=sample_size,
            initial_state=initial_state,
            full_diagnostics=full_diagnostics,
        )
        return results["map_estimate"]

    def start_MCMC_sampling(
        self,
        sample_size,
        initial_state=None,
        full_diagnostics=False,
    ) -> SamplingResults:
        """
        Start the HMC sampling procedure with initial state as passed.
        Use the underlying configurations for configuring the Hamiltonian trajectory, burn-in and mixin settings.

        :param int sample_size: number of smaple points to generate/collect from the predefined target distribution
        :param initial_state: initial point of the chain (any point that falls in the target distribution or near by it
            will result in faster convergence). You can try prior mean if this is used in a Bayesian approach
        :param bool randomize_step_size: if `True` a tiny random number is added to the passed step
            size to help improve space exploration
        :param bool full_diagnostics: if `True` all generated states will be tracked and kept for full disgnostics, otherwise,
            only collected samples are kept in memory
        """

        # Extract configurations from the configurations dictionary
        state_space_dimension = self.size
        burn_in_steps = self.burn_in
        mixing_steps = self.mix_in
        constraint_test = self.constraint_test

        liner, sliner = "=" * 53, "-" * 40
        if self.verbose:
            print("\n%s\nStarted Sampling\n%s\n" % (liner, liner))

        # Chain initial state
        if initial_state is None:
            initial_state = self.generate_white_noise(state_space_dimension)
        else:
            initial_state = np.array(initial_state).flatten()
            if initial_state.size != state_space_dimension:
                raise TypeError(
                    f"Passed initial stae has invalid shape/size"
                    f"Passed initial state has size {initial_state.size}"
                    f"Expected size: {state_space_dimension}"
                )

        # Setup and construct the chain using HMC proposal:
        chain_length = burn_in_steps + sample_size * mixing_steps

        # Initialize the chain
        current_state = initial_state.copy()  # initial state = ensemble mean

        # All generated sample points will be kept for testing and efficiency analysis
        chain_state_repository = [initial_state]
        proposals_repository = []
        acceptance_flags = []
        acceptance_probabilities = []
        uniform_random_numbers = []
        collected_ensemble = []

        map_estimate = None
        map_estimate_log_density = -np.infty

        # Build the Markov chain
        start_time = time.time()  # start timing
        for chain_ind in range(chain_length):
            ## Proposal step :propose state

            # Advance the current state
            proposed_state = self.proposal(
                initial_state=current_state,
            )

            ## MH step (Accept/Reject) proposed state
            # Calculate acceptance proabability
            current_log_prob = self.log_density(current_state)
            constraint_violated = False
            if constraint_test is not None:
                if not constraint_test(proposed_state):
                    constraint_violated = True

            if constraint_violated:
                acceptance_probability = 0

            else:
                proposal_log_prob = self.log_density(proposed_state)
                energy_loss = current_log_prob - proposal_log_prob
                _loss_thresh = 1000
                # this should avoid overflow errors
                if abs(energy_loss) >= _loss_thresh:
                    if energy_loss < 0:
                        sign = -1
                    else:
                        sign = 1
                    energy_loss = sign * _loss_thresh
                acceptance_probability = np.exp(-energy_loss)
                acceptance_probability = min(acceptance_probability, 1.0)

                # Update Mode (Map Point Estimate)
                if proposal_log_prob >= current_log_prob:
                    map_estimate = proposed_state.copy()
                    map_estimate_log_density = proposal_log_prob

            # a uniform random number between 0 and 1
            uniform_probability = self.random_number_generator.random()

            # MH-rule
            if acceptance_probability > uniform_probability:
                current_state = proposed_state
                accept_proposal = True
            else:
                accept_proposal = False

            if self.verbose:
                print(
                    f"\rHMC Iteration [{chain_ind+1:4d}/{chain_length:4d}]; Accept Prob: {acceptance_probability:3.2f}; --> Accepted? {accept_proposal}",
                    end="  ",
                )

            #
            if chain_ind >= burn_in_steps and chain_ind % mixing_steps == 0:
                collected_ensemble.append(current_state.copy())

            # Update Results Repositories:
            if full_diagnostics:
                proposals_repository.append(proposed_state)
                acceptance_probabilities.append(acceptance_probability)
                uniform_random_numbers.append(uniform_probability)
                #
                if accept_proposal:
                    acceptance_flags.append(1)
                else:
                    acceptance_flags.append(0)
                chain_state_repository.append(np.squeeze(current_state))

        # Stop timing
        chain_time = time.time() - start_time

        # ------------------------------------------------------------------------------------------------

        # Now output diagnostics and show some plots :)
        if full_diagnostics:
            output_dir = self.configurations.output_dir
            chain_diagnostics = self.mcmc_chain_diagnostic_statistics(
                proposals_repository=proposals_repository,
                chain_state_repository=chain_state_repository,
                collected_ensemble=collected_ensemble,
                acceptance_probabilities=acceptance_probabilities,
                uniform_probabilities=uniform_random_numbers,
                acceptance_flags=acceptance_flags,
                map_estimate=map_estimate,
                output_dir=output_dir,
                plot_title="MCMC",
                filename_prefix="MCMC_Sampling",
            )
        else:
            chain_diagnostics = None

        #
        # ======================================================================================================== #
        #                Output sampling diagnostics and plot the results for 1 and 2 dimensions                   #
        # ======================================================================================================== #
        #
        if self.verbose:
            print("MCMC sampler:")
            print(f"Time Elapsed for MCMC sampling: {chain_time} seconds")
            if chain_diagnostics is not None:
                print(f"Acceptance Rate: {chain_diagnostics.acceptance_rate:.2f}")

        return SamplingResults(
            chain_state_repository=chain_state_repository,
            collected_ensemble=collected_ensemble,
            proposals_repository=proposals_repository,
            acceptance_flags=acceptance_flags,
            acceptance_probabilities=acceptance_probabilities,
            uniform_random_numbers=uniform_random_numbers,
            map_estimate=map_estimate,
            map_estimate_log_density=map_estimate_log_density,
            chain_diagnostics=chain_diagnostics,
            chain_time=chain_time,
        )

    def mcmc_chain_diagnostic_statistics(
        self,
        proposals_repository,
        chain_state_repository,
        uniform_probabilities,
        acceptance_probabilities,
        collected_ensemble,
        map_estimate=None,
        acceptance_flags=None,
        output_dir=None,
        plot_title="MCMC",
        filename_prefix="MCMC_Sampling",
    ) -> ChainDiagnostics:
        """
        Return diagnostic statistics of the chain such as the rejection rate, acceptance ratio, etc.
        """
        if acceptance_flags is None:
            acceptance_flags = np.asarray(
                acceptance_probabilities >= uniform_probabilities, dtype=np.int
            )
        else:
            acceptance_flags = np.asarray(acceptance_flags)
        acceptance_rate = (
            float(acceptance_flags.sum()) / np.size(acceptance_flags) * 100.0
        )
        rejection_rate = 100.0 - acceptance_rate

        # Plots & autocorrelation, etc.
        plot_sampling_results(
            collected_ensemble,
            log_density=self.log_density,
            map_estimate=map_estimate,
            output_dir=output_dir,
            title=plot_title,
            filename_prefix=filename_prefix,
        )
        # TODO: Add More; e.g., effective sample size, etc.
        return ChainDiagnostics(
            acceptance_rate=acceptance_rate, rejection_rate=rejection_rate
        )

    def update_random_number_generators(
        self,
        random_seed,
        update_proposal=True,
    ):
        """
        Reset/Update the underlying random_number generator by resetting it's seed.
        The random number generator is provided by the `RandomNumberGenerationMixin`
        If `update_proposal` is `True` do the same for underlying `proposal`
        This actually replaces the current random number generator(s) with a new one(s)
        created from the given `random_seed`.
        """
        self.update_random_number_generator(random_seed=random_seed)
        if update_proposal:
            self.proposal.update_configurations(random_seed=random_seed)

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        return self.configurations.size

    @property
    def mix_in(self):
        """Return the number of generated samples between accepted ones"""
        return self.configurations.mix_in

    @mix_in.setter
    def mix_in(self, value):
        """Update the mix-in setting"""
        if not (isnumber(value) and (value > 0) and (int(value) == value)):
            raise TypeError(
                f"Passed mix-in value must be a positive integer; received {value}"
            )
        self.configurations.mix_in = int(value)

    @property
    def burn_in(self):
        """Return the number of sample points to discard before collecting samples"""
        return self.configurations.burn_in

    @burn_in.setter
    def burn_in(self, value):
        """Update the burn-in setting"""
        if not (isnumber(value) and (value > 0) and (int(value) == value)):
            raise TypeError(
                f"Passed burn-in value must be a positive integer; received {value}"
            )
        self.configurations.burn_in = int(value)

    @property
    def constraint_test(self) -> Callable[[np.ndarray], bool]:
        """
        Return the function that returns a boolean value `True` if sample point satisfy
        any constraint, False otherwise.
        """
        if self.configurations.constraint_test is None:
            return lambda _: True
        return self.configurations.constraint_test

    @constraint_test.setter
    def constraint_test(self, value):
        """Update the constraint test function"""
        self.configurations.constraint_test = value
        self.proposal.constraint_test = value

    @property
    def proposal(self):
        """Get a handle of the proposal"""
        return self.configurations.proposal

    @proposal.setter
    def proposal(self, value):
        """Update the proposal"""
        if value is None:
            value = GaussianProposal({"size": self.size})
        else:
            self.validate_configurations({'proposal': value}, raise_for_invalid=True)
        if self.configurations.constraint_test is not None:
            value.constraint_test = self.configurations.constraint_test
        self.configurations.proposal = value


class VALID_INTEGRATORS(StrEnum):
    """
    Valid symplectic integrators for the HMC sampler
    """

    LEAPFROG = r"\Aleapfrog\Z"
    VERLET = r"\Averlet\Z"
    TWO_STAGE = r"\A2(-|_| )*stage(s)*\Z"
    THREE_STAGE = r"\A3(-|_| )*stage(s)*\Z"


@dataclass(kw_only=True, slots=True)
class MassMatrix(PyOEDData):
    """
    Data container for a covariance matrix of a Gaussian proposal.
    """

    matrix: np.ndarray | sp.spmatrix
    inverse: np.ndarray | sp.spmatrix
    sqrt: np.ndarray | sp.spmatrix

    def __str__(self):
        sep = "*" * 20
        return (
            f"{sep}\n  Mass Matrix Container \n{sep}\n"
            f"Matrix: {self.matrix}\n"
            f"Inverse: {self.inverse}\n"
            f"Square Root: {self.sqrt}\n"
        )


@dataclass(kw_only=True, slots=True)
class HMCSamplerConfigs(SamplerConfigs):
    """
    Configurations for the HMC sampler :py:class:`HMCSampler`.

    :param size: dimension of the target distribution to sample
    :param log_density: log of the (unscaled) density function to be sampled
    :param log_density_grad: the gradient of the `log_density` function passed.
        If None, it will be approximated using finite differences.
    :param burn_in: number of sample points to discard before collecting samples
    :param mix_in: number of generated samples between accepted ones (to descrease
        autocorrelation)
    :param symplectic_integrator: name of the symplectic integrator to use; acceptable
        are `'leapfrog'`, `'2-stage'`, `'3-stage'`, where both 'leapfrog' and 'verlet'
        are equivalent.
    :param symplectic_integrator_stepsize: the step size of the symplectic integrator
    :param symplectic_integrator_num_steps: number of steps of size
        `symplectic_integrator_stesize`
    :param mass_matrix: mass matrix to be used to adjust sampling the auxilliary
        Gaussian momentum
    :param constraint_test: a function that returns a boolean value `True` if sample
        point satisfy any constrints, and `False` otherwise; ignored if `None`, is
        passed.
    """

    size: int | None = None
    log_density: Callable[[np.ndarray], float] | None = None
    log_density_grad: Callable[[np.ndarray], np.ndarray] | None = None
    burn_in: int = 500
    mix_in: int = 10
    symplectic_integrator: str = "verlet"
    symplectic_integrator_stepsize: float = 1e-2
    symplectic_integrator_num_steps: int = 20
    mass_matrix: float | None | np.ndarray | sp.spmatrix = 1
    sparse: bool = True
    constraint_test: Callable[[np.ndarray], bool] | None = None

@set_configurations(HMCSamplerConfigs)
class HMCSampler(Sampler, RandomNumberGenerationMixin):
    def __init__(self, configs: dict | HMCSamplerConfigs | None = None):
        """
        Implementation of the HMC sampling algorithm
        (with multiple choices of the symplectic integrator).

        :param configs: A configurations object. See :py:class:`HMCSamplerConfigs`.
        """
        super().__init__(
            self.configurations_class.data_to_dataclass(
                configs,
            )
        )

        # Associate `_MASS_MATRIX` attribute to self
        self._MASS_MATRIX = self.update_mass_matrix(self.configurations.mass_matrix)

    def standardize_configurations(
        self,
        configs: dict | HMCSamplerConfigs,
    ) -> HMCSamplerConfigs:
        """

        """
        # First Aggregate and convert to the proper (expected) type
        configs = self.configurations_class.data_to_dataclass(configs)

        # Stnadardize the mass matrix
        configs.mass_matrix = self._standardize_mass_matrix(
            mass_matrix=configs.mass_matrix,
            size=configs.size,
            sparse=configs.sparse,
        )

        return configs

    def validate_configurations(
        self, configs: dict | SamplerConfigs, raise_for_invalid: bool = True
    ) -> bool:
        """
        A method to check the passed configuratios and make sure they are conformable
        with each other, and with current configurations once combined. This guarantees
        that any key-value pair passed in configs can be properly used

        :param configs: a dictionary holding key/value configurations
        :param bool raise_for_invalid: if `True` raise :py:class:`TypeError` for
          invalid configrations type/key

        :returns: True/False flag indicating whether passed coinfigurations dictionary
          is valid or not

        :raises: see the parameter ``raise_for_invalid``
        """
        aggregated_configs = self.aggregate_configurations(configs)

        def is_valid_mass_matrix(mat):
            if not (
                    mat is None or
                    isnumber(mat) or
                    isinstance(mat, np.ndarray) or
                    sp.issparse(mat)
            ):
                return False
            else:
                return True


        for key in ["size", "burn_in", "mix_in", "symplectic_integrator_num_steps"]:
            if not validate_key(
                aggregated_configs,
                configs,
                key,
                test=lambda x: isnumber(x) and (int(x) == x) and (x > 0),
                message=f"{key} must be a positive integer",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        for key in ["log_density", "log_density_grad"]:
            if not validate_key(
                aggregated_configs,
                configs,
                key,
                test=callable,
                message=f"{key} must be a callable function",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        if not validate_key(
            aggregated_configs,
            configs,
            "symplectic_integrator",
            test=lambda x: any(re.match(v, x) for v in VALID_INTEGRATORS),
            message=(
                "invalid symplectic_integrator, must be one of "
                ", ".join(v.name.lower() for v in VALID_INTEGRATORS)
            ),
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "symplectic_integrator_stepsize",
            test=lambda x: isnumber(x) and (x > 0),
            message="symplectic_integrator_stepsize must be a positive number",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "sparse",
            test=lambda x: isinstance(x, bool),
            message="sparse must be a boolean",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraint_test",
            test=lambda x: callable(x) or (x is None),
            message="constraint_test must be a callable function or None",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `mass_matrix`
        if not validate_key(
            aggregated_configs,
            configs,
            "mass_matrix",
            test=is_valid_mass_matrix,
            message="Invalid mass matrix",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(
            configs, raise_for_invalid=raise_for_invalid
        )

    def update_configurations(self, **kwargs):
        """
        Update the configurations of the sampler object with the passed key-value pairs.
        """
        # Unsettable configurations
        if "size" in kwargs:
            raise TypeError(
                "Cannot update `size` configurations after initialization!"
            )

        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Additional update
        if "mass_matrix" in kwargs:
            self.update_mass_matrix(kwargs["mass_matrix"])

    def update_mass_matrix(self, mass_matrix):
        """
        Update the mass matrix (standardize, update configurations, and update locally
        maintained MassMatrix instance.)
        """
        # Standardize the mass matrix
        self.configurations.mass_matrix = self._standardize_mass_matrix(
            mass_matrix=mass_matrix,
            size=self.configurations.size,
            sparse=self.configurations.sparse,
        )

        # Associate `_MASS_MATRIX` attribute to self
        self._MASS_MATRIX = self._build_mass_matrix(
            mass_matrix=self.configurations.mass_matrix,
            size=self.configurations.size,
            sparse=self.configurations.sparse,
        )
        return self._MASS_MATRIX

    def _standardize_mass_matrix(
        self,
        mass_matrix,
        size,
        sparse,
    ) -> list[np.ndarray | sp.spmatrix]:
        """
        Standardize the mass matrix and convert the passed one into
        numpy/scipy array based on `sparse`.

          - If `None`, an identity matrix is created.
          - If a scalar is passed, it must be positive, and is converted to multiple of
            the identity.
          - If 1d array, it is set as the diagonal of a square matrix.
          - If not the above, it must be 2d array with shape (`size` x `size`)

        :raises TypeError: is raised if the passed mass matrix is invalid type.
        """
        if mass_matrix is None:
            if sparse:
                mass_matrix = sp.eye(size).tocsc()
            else:
                mass_matrix = np.eye(size)

        elif isnumber(mass_matrix):
            assert mass_matrix > 0, "The mass matrix diagonal value (scalar) cannot be negative!"

            if sparse:
                mass_matrix = sp.csc_array(
                    sp.diags(
                        np.array(mass_matrix).repeat(size),
                        offsets=0,
                        format="csc",
                    ),
                )
            else:
                mass_matrix = np.diag(
                    np.array(mass_matrix).repeat(size),
                    k=0,
                )

        elif isinstance(mass_matrix, np.ndarray):
            if mass_matrix.ndim == 1:
                if mass_matrix.size in [1, size]:
                    if mass_matrix.size == 1:
                        mass_matrix = mass_matrix.repeat(size)

                    # Convert to diagonal...
                    if any(mass_matrix <= 0):
                        raise TypeError(
                            f"Invalid mass values; expected positive diagonals; "
                            f"received negagive value(s)"
                        )

                    if sparse:
                        mass_matrix = sp.csc_array(
                            sp.diags(
                                np.array(mass_matrix).repeat(size),
                                offsets=0,
                                format="csc",
                            ),
                        )
                    else:
                        mass_matrix = np.diag(
                            np.array(mass_matrix).repeat(size),
                            k=0,
                        )

                else:
                    raise TypeError(
                        f"Invalid size of the diagonal of the mass matrix; "
                        f"expected 1d (diagonal of mass matrix) of size {size=}; "
                        f"received 1d array of {mass_matrix.size=}"
                    )

            elif mass_matrix.ndim == 2:
                if mass_matrix.shape == (size, size):
                    # All good
                    if sparse:
                        mass_matrix = sp.csc_array(mass_matrix)

                else:
                    raise TypeError(
                        f"Invalid mass matrix shape {mass_matrix.shape=} "
                    )

            else:
                raise TypeError(
                    f"Invalid mass matrix number of dimensions {mass_matrix.ndim=}"
                )

        elif sp.issparse(mass_matrix):
            # If not sparse, recurse with numpy array version
            if mass_matrix.ndim == 1:
                if mass_matrix.size in [1, size]:
                    mass_matrix = mass_matrix.toarray().flatten()
                    if mass_matrix.size == 1:
                        mass_matrix = mass_matrix.repeat(size)

                    # Convert to diagonal...
                    if any(mass_matrix <= 0):
                        raise TypeError(
                            f"Invalid mass values; expected positive diagonals; "
                            f"received negagive value(s)"
                        )

                    if sparse:
                        mass_matrix = sp.csc_array(
                            sp.diags(
                                np.array(mass_matrix).repeat(size),
                                offsets=0,
                                format="csc",
                            ),
                        )
                    else:
                        mass_matrix = np.diag(
                            np.array(mass_matrix).repeat(size),
                            k=0,
                        )

                else:
                    raise TypeError(
                        f"Invalid size of the diagonal of the mass matrix; "
                        f"expected 1d (diagonal of mass matrix) of size {size=}; "
                        f"received 1d array of {mass_matrix.size=}"
                    )

            elif mass_matrix.ndim == 2:
                if mass_matrix.shape == (size, size):
                    # All good
                    if not sparse:
                        mass_matrix = mass_matrix.toarray()

                else:
                    raise TypeError(
                        f"Invalid mass matrix shape {mass_matrix.shape=} "
                    )

            else:
                raise TypeError(
                    f"Invalid mass matrix number of dimensions {mass_matrix.ndim=}"
                )

        else:
            raise TypeError(
                f"`mass_matrix` has unsupported type: {type(mass_matrix)=}"
            )

        return mass_matrix

    def _build_mass_matrix(self, mass_matrix, size, sparse) -> MassMatrix:
        """
        Convert mass_matrix into a :py:class:`MassMatrix` object.
        """
        matrix = self._standardize_mass_matrix(
            mass_matrix,
            size=size,
            sparse=sparse,
        )
        inv = sp.linalg.inv(matrix) if self.configurations.sparse else np.linalg.inv(matrix)
        sqrt = factorize_spsd_matrix(matrix)
        return MassMatrix(matrix=matrix, inverse=inv, sqrt=sqrt)


    # TODO: I think both of these methods should return the full results as well, no?
    def sample(
        self,
        sample_size=1,
        initial_state=None,
        full_diagnostics=False,
    ):
        """
        Generate and return a sample of size `sample_size`.
        This method returns a list with each entry representing a sample point from
        the underlying distribution

        :param int sample_size:
        :param initial_state:
        :param bool full_diagnostics: if `True` all generated states will be tracked
          and kept for full disgnostics, otherwise, only collected samples are kept in memory
        """
        return self.start_MCMC_sampling(
            sample_size=sample_size,
            initial_state=initial_state,
            full_diagnostics=full_diagnostics,
        ).collected_ensemble

    def map_estimate(
        self,
        sample_size=100,
        initial_state=None,
        full_diagnostics=False,
    ):
        """
        Search for a MAP (maximum aposteriori) estimate by sampling (space exploration)
        This method returns a single-point estimate of the MAP of the distribution

        :param int sample_size:
        :param initial_state:
        :param bool verbose:
        """
        return self.start_MCMC_sampling(
            sample_size=sample_size,
            initial_state=initial_state,
            full_diagnostics=full_diagnostics,
        ).map_estimate

    def start_MCMC_sampling(
        self,
        sample_size,
        initial_state=None,
        randomize_step_size=False,
        full_diagnostics=False,
    ) -> SamplingResults:
        """
        Start the HMC sampling procedure with initial state as passed.
        Use the underlying configurations for configuring the Hamiltonian trajectory, burn-in and mixin settings.

        :param int sample_size: number of smaple points to generate/collect from the predefined target distribution
        :param initial_state: initial point of the chain (any point that falls in the target distribution or near by it
            will result in faster convergence). You can try prior mean if this is used in a Bayesian approach
        :param bool randomize_step_size: if `True` a tiny random number is added to the passed step
            size to help improve space exploration
        :param bool full_diagnostics: if `True` all generated states will be tracked and kept for full disgnostics, otherwise,
            only collected samples are kept in memory
        """

        # Extract configurations from the configurations dictionary
        state_space_dimension = self.size
        burn_in_steps = self.burn_in
        mix_in_steps = self.mix_in
        symplectic_integrator = self.symplectic_integrator
        hamiltonian_num_steps = self.symplectic_integrator_num_steps
        hamiltonian_step_size = self.symplectic_integrator_stepsize

        liner, sliner = "=" * 53, "-" * 40
        if self.verbose:
            print("\n%s\nStarted Sampling\n%s\n" % (liner, liner))

        # Chain initial state
        if initial_state is None:
            initial_state = self.generate_white_noise(state_space_dimension)
        else:
            initial_state = np.array(initial_state).flatten()
            if initial_state.size != state_space_dimension:
                raise TypeError(
                    f"Passed initial stae has invalid shape/size"
                    f"Passed initial state has size {initial_state.size}"
                    f"Expected size: {state_space_dimension}"
                )

        # Setup and construct the chain using HMC proposal:
        chain_length = burn_in_steps + sample_size * mix_in_steps

        # Initialize the chain
        current_state = initial_state.copy()  # initial state = ensemble mean

        # All generated sample points will be kept for testing and efficiency analysis
        chain_state_repository = [initial_state]
        proposals_repository = []
        acceptance_flags = []
        acceptance_probabilities = []
        uniform_random_numbers = []
        collected_ensemble = []
        map_estimate = None
        map_estimate_log_density = -np.infty

        # Build the Markov chain
        start_time = time.time()  # start timing
        for chain_ind in range(chain_length):
            ## Proposal step :propose (momentum, state) pair
            # Generate a momentum proposal
            current_momentum = self.generate_white_noise(size=state_space_dimension)
            current_momentum = self.mass_matrix_sqrt_matvec(current_momentum)

            # Advance the current state and momentum to propose a new pair:
            proposed_momentum, proposed_state = self.apply_symplectic_integration(
                momentum=current_momentum,
                state=current_state,
                num_steps=hamiltonian_num_steps,
                step_size=hamiltonian_step_size,
                randomize_step_size=randomize_step_size,
                symplectic_integrator=symplectic_integrator,
            )

            # print("proposed_momentum, proposed_state", proposed_momentum, proposed_state)

            ## MH step (Accept/Reject) proposed (momentum, state)
            # Calculate acceptance proabability
            # Total energy (Hamiltonian) of the extended pair (proposed_momentum,
            # Here, we evaluate the kernel of the posterior at both the current and the proposed state proposed_state)
            current_energy = self.total_Hamiltonian(
                momentum=current_momentum, state=current_state
            )
            constraint_violated = False
            if not self.constraint_test(proposed_state):
                constraint_violated = True

            if constraint_violated:
                acceptance_probability = 0

            else:
                proposal_kinetic_energy = self.kinetic_energy(proposed_momentum)
                proposal_potential_energy = self.potential_energy(proposed_state)
                proposal_energy = proposal_kinetic_energy + proposal_potential_energy

                energy_loss = proposal_energy - current_energy
                _loss_thresh = 1000
                if (
                    abs(energy_loss) >= _loss_thresh
                ):  # this should avoid overflow errors
                    if energy_loss < 0:
                        sign = -1
                    else:
                        sign = 1
                    energy_loss = sign * _loss_thresh
                acceptance_probability = np.exp(-energy_loss)
                acceptance_probability = min(acceptance_probability, 1.0)

                # Update Mode (Map Point Estimate)
                if -proposal_potential_energy > map_estimate_log_density:
                    map_estimate = proposed_state.copy()
                    map_estimate_log_density = -proposal_potential_energy

            # a uniform random number between 0 and 1
            uniform_probability = self.random_number_generator.random()

            # MH-rule
            if acceptance_probability > uniform_probability:
                current_state = proposed_state
                accept_proposal = True
            else:
                accept_proposal = False

            if self.verbose:
                print(
                    f"\rHMC Iteration [{chain_ind+1:4d}/{chain_length:4d}]; Accept Prob: {acceptance_probability:3.2f}; --> Accepted? {accept_proposal}",
                    end="  ",
                )

            #
            if chain_ind >= burn_in_steps and chain_ind % mix_in_steps == 0:
                collected_ensemble.append(current_state.copy())

            # Update Results Repositories:
            if full_diagnostics:
                proposals_repository.append(proposed_state)
                acceptance_probabilities.append(acceptance_probability)
                uniform_random_numbers.append(uniform_probability)
                #
                if accept_proposal:
                    acceptance_flags.append(1)
                else:
                    acceptance_flags.append(0)
                chain_state_repository.append(np.squeeze(current_state))

        # Stop timing
        chain_time = time.time() - start_time

        # ------------------------------------------------------------------------------------------------

        # Now output diagnostics and show some plots :)
        output_dir = self.configurations.output_dir
        if full_diagnostics:
            chain_diagnostics = self.mcmc_chain_diagnostic_statistics(
                proposals_repository=proposals_repository,
                chain_state_repository=chain_state_repository,
                collected_ensemble=collected_ensemble,
                acceptance_probabilities=acceptance_probabilities,
                uniform_probabilities=uniform_random_numbers,
                acceptance_flags=acceptance_flags,
                map_estimate=map_estimate,
                output_dir=output_dir,
                plot_title="HMC Sampling",
                filename_prefix="HMC_Sampling",
            )
        else:
            chain_diagnostics = None

        #
        # ======================================================================================================== #
        #                Output sampling diagnostics and plot the results for 1 and 2 dimensions                   #
        # ======================================================================================================== #
        #
        if self.verbose:
            print("MCMC sampler:")
            print(f"Time Elapsed for MCMC sampling: {chain_time} seconds")
            if chain_diagnostics is not None:
                print(f"Acceptance Rate: {chain_diagnostics['acceptance_rate']:.2f}")

        return SamplingResults(
            chain_state_repository=chain_state_repository,
            collected_ensemble=collected_ensemble,
            proposals_repository=proposals_repository,
            acceptance_flags=acceptance_flags,
            acceptance_probabilities=acceptance_probabilities,
            uniform_random_numbers=uniform_random_numbers,
            chain_diagnostics=chain_diagnostics,
            map_estimate=map_estimate,
            map_estimate_log_density=map_estimate_log_density,
            chain_time=chain_time,
        )

    def generate_white_noise(self, size, truncate=False):
        """
        Generate a standard normal random vector of size `size` with values truncated
            at -/+3 if `truncate` is set to `True`

        :returns: a numpy array of size `size` sampled from a standard multivariate normal
            distribution of dimension `size` with mean 0 and covariance matrix equals
            an identity matrix.

        :remarks:
            - this function returns a numpy array of size `size` even if `size` is set to 1
        """
        # Sample
        randn_vec = self.random_number_generator.standard_normal(size)

        # Truncate if requested
        if truncate:
            randn_vec[randn_vec > 3] = 3
            randn_vec[randn_vec < -3] = -3
        return randn_vec

    def mass_matrix_matvec(self, momentum):
        """
        Multiply the mass matrix (in the configurations) by the passed momentum
        """
        momentum = np.asarray(momentum).flatten()
        if momentum.size != self.size:
            raise ValueError(
                f"The passed momentum has invalid size;"
                f"received {momentum}, expected {self.size}"
            )

        return self.mass_matrix.matrix.dot(momentum)

    def mass_matrix_inv_matvec(self, momentum):
        """
        Multiply the inverse of the mass matrix (in the configurations) by the passed momentum
        """
        momentum = np.asarray(momentum).flatten()
        if momentum.size != self.size:
            return ValueError(
                f"The passed momentum has invalid size;"
                f"received {momentum}, expected {self.size}"
            )

        return self.mass_matrix.inverse.dot(momentum)

    def mass_matrix_sqrt_matvec(self, momentum):
        """
        Multiply the Square root (Lower Cholesky factor) of the mass matrix (in the configurations) by the passed momentum
        """
        momentum = np.asarray(momentum).flatten()
        if momentum.size != self.size:
            raise ValueError(
                f"The passed momentum has invalid size;"
                f"received {momentum}, expected {self.size}"
            )

        return self.mass_matrix.sqrt.dot(momentum)

    def log_density(self, state):
        """
        Evaluate the value of the logarithm of the target unscaled posterior density function
        """
        val = self.configurations.log_density(state)
        if isinstance(val, np.ndarray) and (val.size == 1):
            val = val.item()
        return val

    def log_density_grad(self, state):
        """
        Evaluate the gradient of the logarithm of the target unscaled posterior density function
        """
        return self.configurations.log_density_grad(state)

    def _create_func_grad(
        self, func, size, approach="fd", fd_eps=1e-5, fd_central=False
    ):
        """
        Given a callable/function `func`, create a function that evaluates the gradient of this function
        :param int size: the domain size which determines the size of the returned gradient
        :param str approach: the approach to use for creating the function.

        :remarks: this method is planned to enable automatic differentiation (AD) if available on
            the current platform, otherwise finite differences 'fd' is used
        """
        if re.match(
            r"\A(f(-|_| )*d|finite(-|_| )*difference(s)*)\Z", approach, re.IGNORECASE
        ):

            def func_grad(x, fd_eps=fd_eps, fd_central=fd_central):
                """Function to generate gradient using finite differences"""
                x = np.asarray(x).flatten()
                grad = np.zeros_like(x)
                e = np.zeros_like(x)
                for i in range(e.size):
                    e[:] = 0.0
                    e[i] = fd_eps

                    if fd_central:
                        grad[i] = (func(x + e) - func(x - e)) / (2.0 * fd_eps)
                    else:
                        grad[i] = (func(x + e) - func(x)) / fd_eps
                return grad

            return func_grad

        elif re.match(
            r"\A(a(-|_| )*d|automatic(-|_| )*differentiation)\Z",
            approach,
            re.IGNORECASE,
        ):
            raise NotImplementedError(
                "TODO: A/D is not yet supported for creating function gradient"
            )

        else:
            print(f"Unrecognized gradient generation approach {approach}")
            raise ValueError

    def potential_energy(self, state):
        """
        Evaluate the value of the potential energy at the given `state`. The potential
        energy is the negative value of the logarithm of the unscaled posterior density
        function.
        """
        if np.any(np.isnan(state)):
            if self.verbose:
                print("NaN values in the passed state")
                print(f"Received State:\n {repr(state)}")
            # raise ValueError
            return np.nan
        return -self.log_density(state)

    def potential_energy_grad(self, state):
        """
        Evaluate the gradient of the potential energy at the given `state`. The
        potential energy is the negative value of the logarithm of the unscaled
        posterior density function
        """
        if np.any(np.isnan(state)):
            if self.verbose:
                print("NaN values in the passed state")
                print(f"Received State:\n {repr(state)}")
            # raise ValueError
            return np.nan
        return -self.log_density_grad(state)

    def kinetic_energy(self, momentum):
        """
        Evaluate the Kinetic energy of the posterior; this is independent from the state
        and is evaluated as the weighted l2 norm of the momentum (scaled by the inverse
        of hte mass matrix); This is half of the squared Mahalanobis distance of the
        Gaussian momentum.
        """
        momentum = np.asarray(momentum).flatten()
        if momentum.size != self.size:
            ValueError(
                f"The passed momentum has invalid size;"
                f"received {momentum}, expected {self.self.size}"
            )
        return 0.5 * np.dot(momentum, self.mass_matrix_inv_matvec(momentum))

    def total_Hamiltonian(self, momentum, state):
        """
        Evaluate the value of the total energy function: Hamiltonian = kinetic energy +
        potential energy
        """
        return self.kinetic_energy(momentum) + self.potential_energy(state)

    def build_Hamiltonian_trajectory(
        self,
        momentum,
        state,
        step_size,
        num_steps,
        randomize_step_size=False,
    ):
        """
        Given the current momentum and state pair of the Hamiltonian system,
        generate a trajectory of (momentum, state).

        :param momentum:
        :param state:
        :param bool randomize_step_size: if `True` a tiny random number is added
          to the passed step size to help improve space exploration
        """
        # local copies
        momentum = np.asarray(momentum).flatten()
        state = np.asarray(state).flatten()

        trajectory = [(momentum, state)]
        # Loop over number of steps, for each step update current momentum and state then append to trajectory
        for _ in range(num_steps):
            trajectory.append(
                self.apply_symplectic_integration(
                    momentum=trajectory[-1][0],
                    state=trajectory[-1][1],
                    step_size=step_size,
                    num_steps=num_steps,
                    randomize_step_size=randomize_step_size,
                )
            )
        return trajectory

    def apply_symplectic_integration(
        self,
        momentum,
        state,
        step_size,
        num_steps,
        randomize_step_size=False,
        symplectic_integrator="3-stage",
    ):
        """
        Apply one full step of size `step_size` of the symplectic integrator to
        the Hamiltonian system

        :parm momentum:
        :param state:
        :param int num_steps:
        :param float step_size:
        :param str symplectic_integrator: name of the symplectic integrator to use;
          acceptable are: 'verlet', 'leapfrog', '2-stage', '3-stage',
          where both 'leapfrog' and 'verlet' are equivalent
        :param bool randomize_step_size: if `True` a tiny random number is added to
          the passed step size to help improve space exploration

        :returns: a tuple (p, s) where p and s are the integrated (forward in time)
          momentum and state respectively
        """
        if np.any(np.isnan(momentum)):
            raise ValueError(
                "Cannot apply symplectic integorator;"
                " NaN values found in the passed momentum"
            )
        if np.any(np.isnan(state)):
            raise ValueError(
                "Cannot apply symplectic integorator;"
                " NaN values found in the passed state"
            )
        current_momentum = np.asarray(momentum).flatten()
        current_state = np.asarray(state).flatten()

        state_space_dimension = self.size
        if not (current_momentum.size == current_state.size == state_space_dimension):
            raise ValueError(
                f"The momentum and state must be of the same size as the underlying space dimnsion; "
                f"State size: {current_state.size}, "
                f"Momentum size: {current_momentum.size}, "
                f"Underlying space dimension: {state_space_dimension}"
            )

        # validate step size (and randomize if asked)
        if step_size <= 0:
            raise ValueError(
                f"Step size of the symplectic integrator must be positive!"
            )

        if randomize_step_size:
            # Random step size perturbation (update random state)
            u = (
                self.random_number_generator.random() - 0.5
            ) * 0.4  # perturb step-size:
            h = (1 + u) * step_size
        else:
            h = step_size

        for _ in range(num_steps):
            #
            if any(
                re.match(v, symplectic_integrator, re.IGNORECASE)
                for v in [VALID_INTEGRATORS.LEAPFROG, VALID_INTEGRATORS.VERLET]
            ):
                # Update state
                proposed_state = current_state + (
                    0.5 * h
                ) * self.mass_matrix_inv_matvec(current_momentum)
                # print("1: proposed state", proposed_state)

                # Update momentum
                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = current_momentum - h * grad
                # print("<: proposed momentum", proposed_momentum)

                # Update state again
                proposed_state += (0.5 * h) * self.mass_matrix_inv_matvec(
                    proposed_momentum
                )
            elif re.match(
                VALID_INTEGRATORS.TWO_STAGE, symplectic_integrator, re.IGNORECASE
            ):
                a1 = 0.21132
                a2 = 1.0 - 2.0 * a1
                b1 = 0.5

                proposed_state = current_state + (a1 * h) * self.mass_matrix_inv_matvec(
                    current_momentum
                )

                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = current_momentum - (b1 * h) * grad

                proposed_state = proposed_state + (
                    a2 * h
                ) * self.mass_matrix_inv_matvec(proposed_momentum)

                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = proposed_momentum - (b1 * h) * grad

                proposed_state += (a1 * h) * self.mass_matrix_inv_matvec(
                    proposed_momentum
                )

            elif re.match(
                VALID_INTEGRATORS.THREE_STAGE, symplectic_integrator, re.IGNORECASE
            ):
                a1 = 0.11888010966548
                a2 = 0.5 - a1
                b1 = 0.29619504261126
                b2 = 1.0 - 2.0 * b1

                proposed_state = current_state + (a1 * h) * self.mass_matrix_inv_matvec(
                    current_momentum
                )

                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = current_momentum - (b1 * h) * grad

                proposed_state = proposed_state + (
                    a2 * h
                ) * self.mass_matrix_inv_matvec(proposed_momentum)

                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = proposed_momentum - (b2 * h) * grad

                proposed_state = proposed_state + (
                    a2 * h
                ) * self.mass_matrix_inv_matvec(proposed_momentum)

                grad = self.potential_energy_grad(proposed_state)
                proposed_momentum = proposed_momentum - (b1 * h) * grad

                proposed_state += (a1 * h) * self.mass_matrix_inv_matvec(
                    proposed_momentum
                )

            else:
                raise ValueError(
                    "Unsupported symplectic integrator %s" % symplectic_integrator
                )

            # Update current state and momentum
            current_momentum = proposed_momentum
            current_state = proposed_state

        return (proposed_momentum, proposed_state)

    def mcmc_chain_diagnostic_statistics(
        self,
        proposals_repository,
        chain_state_repository,
        uniform_probabilities,
        acceptance_probabilities,
        collected_ensemble,
        map_estimate=None,
        acceptance_flags=None,
        output_dir=None,
        plot_title="MCMC",
        filename_prefix="MCMC_Sampling",
    ):
        """
        Return diagnostic statistics of the chain such as the rejection rate, acceptance ratio, etc.
        """
        if acceptance_flags is None:
            acceptance_flags = np.asarray(
                acceptance_probabilities >= uniform_probabilities, dtype=np.int
            )
        else:
            acceptance_flags = np.asarray(acceptance_flags)
        acceptance_rate = (
            float(acceptance_flags.sum()) / np.size(acceptance_flags) * 100.0
        )
        rejection_rate = 100.0 - acceptance_rate

        # Plots & autocorrelation, etc.
        plot_sampling_results(
            collected_ensemble,
            log_density=self.log_density,
            map_estimate=map_estimate,
            output_dir=output_dir,
            title=plot_title,
            filename_prefix=filename_prefix,
        )
        # TODO: Add More; e.g., effective sample size, etc.

        # Return all diagonistics in a dictionary
        return ChainDiagnostics(
            acceptance_rate=acceptance_rate,
            rejection_rate=rejection_rate,
        )

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        return self.configurations.size

    @property
    def mix_in(self):
        """Return the number of generated samples between accepted ones"""
        return self.configurations.mix_in

    @mix_in.setter
    def mix_in(self, value):
        """Update the mix-in setting"""
        self.validate_configurations({"mix_in": value})
        self.configurations.mix_in = int(value)

    @property
    def burn_in(self):
        """Return the number of sample points to discard before collecting samples"""
        return self.configurations.burn_in

    @burn_in.setter
    def burn_in(self, value):
        """Update the burn-in setting"""
        self.validate_configurations({"burn_in": value})
        self.configurations.burn_in = int(value)

    @property
    def symplectic_integrator(self):
        """Return the name of the symplectic integrator used"""
        return self.configurations.symplectic_integrator

    @symplectic_integrator.setter
    def symplectic_integrator(self, value):
        """Update the symplectic integrator setting"""
        value = str(value).lower()
        self.validate_configurations({"symplectic_integrator": value})
        self.configurations.symplectic_integrator = value

    @property
    def symplectic_integrator_stepsize(self):
        """Return the step size of the symplectic integrator"""
        return self.configurations.symplectic_integrator_stepsize

    @symplectic_integrator_stepsize.setter
    def symplectic_integrator_stepsize(self, value):
        """Update the step size of the symplectic integrator"""
        self.validate_configurations({"symplectic_integrator_stepsize": value})
        self.configurations.symplectic_integrator_stepsize = float(value)

    @property
    def symplectic_integrator_num_steps(self):
        """Return the number of steps of the symplectic integrator"""
        return self.configurations.symplectic_integrator_num_steps

    @symplectic_integrator_num_steps.setter
    def symplectic_integrator_num_steps(self, value):
        """Update the number of steps of the symplectic integrator"""
        self.validate_configurations({"symplectic_integrator_num_steps": value})
        self.configurations.symplectic_integrator_num_steps = int(value)

    @property
    def mass_matrix(self) -> MassMatrix:
        """Return the constructed mass matrix object"""
        return self._MASS_MATRIX

    @mass_matrix.setter
    def mass_matrix(self, value):
        """Update the mass matrix setting"""
        self.update_configurations(mass_matrix=value)

    @property
    def constraint_test(self) -> Callable[[np.ndarray], bool]:
        """
        Return the function that returns a boolean value `True` if sample point satisfy
        any constraint, False otherwise.
        """
        if self.configurations.constraint_test is None:
            return lambda _: True
        return self.configurations.constraint_test

    @constraint_test.setter
    def constraint_test(self, value):
        """Update the constraint test function"""
        self.validate_configurations({"constraint_test": value})
        self.configurations.constraint_test = value


## Simple interfaces (to generate instances from classes developed here).
def create_mcmc_sampler(
    size,
    log_density,
    burn_in=100,
    mix_in=10,
    constraint_test=None,
    proposal_variance=0.1,
    output_dir=None,
    random_seed=None,
):
    """
    Given the size of the target space, and a function to evalute log density,
        create and return an :py:class:`MCMCSampler` instance/object to generate samples
        using standard MCMC sampling approach.
        Configurations/settings can be updated after inistantiation

    :param int size: dimension of the target distribution to sample
    :param log_density: a callable (function) to evaluate the logarithm of the target density (unscaled);
        this function takes one vector (1d array/iterable) of length equal to `size`, and returns a scalar
    :param int burn_in: number of steps to drop before collecting samples in the chain (for convergence)
    :param int mix_in: number of steps to drop between collected samples (reduce autocorrelation)
    :param proposal_variance: variance of the Gaussian proposal
    :param constraint_test: a function that returns a boolean value `True` if sample point satisfy
        any constrints, and `False` otherwise; ignored if `None`, is passed.
    :param random_seed: random seed used when the object is initiated to keep track of random samples
        This is useful for reproductivity. If `None`, random seed follows `numpy.random.seed` rules

    :return: instance of :py:class:`MCMCSampler` (with some or all configurations passed)
    """
    proposal = GaussianProposal(
        GaussianProposalConfigs(
            size=size,
            variance=proposal_variance,
            output_dir=output_dir,
            random_seed=random_seed,
        )
    )
    return MCMCSampler(
        MCMCSamplerConfigs(
            size=size,
            log_density=log_density,
            proposal=proposal,
            burn_in=burn_in,
            mix_in=mix_in,
            constraint_test=constraint_test,
            output_dir=output_dir,
            random_seed=random_seed,
        )
    )


def create_hmc_sampler(
    size,
    log_density,
    log_density_grad=None,
    burn_in=100,
    mix_in=5,
    symplectic_integrator="verlet",
    symplectic_integrator_stepsize=1e-2,
    symplectic_integrator_num_steps=10,
    mass_matrix=0.1,
    constraint_test=None,
    random_seed=None,
    output_dir=None,
):
    """
    Given the size of the target space, and a function to evalute log density,
        create and return an :py:class:`HMCSampler` instance/object to generate samples using HMC sampling approach.
        Configurations/settings can be updated after inistantiation

    :param int size: dimension of the target distribution to sample
    :param log_density: a callable (function) to evaluate the logarithm of the target density (unscaled);
        this function takes one vector (1d array/iterable) of length equal to `size`, and returns a scalar
    :param log_density_grad: a callable (function) to evaluate the gradient of `log_density`;
        this function takes one vector (1d array/iterable) of length equal to `size`, and returns
        a vector of the same length. If `None` is passed, the gradient is automatically evaluated using
        finite differences (FD).
    :param int burn_in: number of steps to drop before collecting samples in the chain (for convergence)
    :param int mix_in: number of steps to drop between collected samples (reduce autocorrelation)

    :param float symplectic_integrator_stepsize: (positive scalar) the step size of the symplectic integrator
    :param int symplectic_integrator_num_steps: (postive integer) number of steps of size
            `symplectic_integrator_stesize` taken before returnig the next proposed point over the
            Hamiltonian trajectory
    :param mass_matrix: (nonzero scalar or SPD array of size `size x size`)  mass matrix
        to be used to adjust sampling the auxilliary Gaussian momentum
    :param constraint_test: a function that returns a boolean value `True` if sample point satisfy
        any constrints, and `False` otherwise; ignored if `None`, is passed.
    :param random_seed: random seed used when the object is initiated to keep track of random samples
        This is useful for reproductivity. If `None`, random seed follows `numpy.random.seed` rules

    :return: instance of :py:class:`HMCSampler` (with some or all configurations passed)
    """
    return HMCSampler(
        HMCSamplerConfigs(
            size=size,
            log_density=log_density,
            log_density_grad=log_density_grad,
            burn_in=burn_in,
            mix_in=mix_in,
            symplectic_integrator=symplectic_integrator,
            symplectic_integrator_stepsize=symplectic_integrator_stepsize,
            symplectic_integrator_num_steps=symplectic_integrator_num_steps,
            mass_matrix=mass_matrix,
            constraint_test=constraint_test,
            random_seed=random_seed,
            output_dir=output_dir,
        )
    )


## Eyeball test for MCMC sampling
def banana_potential_energy_value(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    Potential energy of the posterir. This is dependent on the target state, not the momentum.
    It is the negative of the posterior-log, and MUST be implemented for each distribution
    """
    x, y = state[:]
    #
    pdf_val = 1 / (2 * (1 - rho**2))
    t1 = x**2 / a**2 + a**2 * (y - b * x**2 / a**2 - b * a**2) ** 2
    t2 = -2 * rho * x * (y - b * x**2 / a**2 - b * a**2)
    pdf_val = (t1 + t2) / (2 * (1 - rho**2))
    #
    return pdf_val


def banana_potential_energy_gradient(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    Gradient of the Potential energy of the posterir.
    """
    x, y = state.flatten()
    #
    pdf_grad = np.empty(2)
    pdf_grad[:] = 1 / (2 * (1 - rho**2))
    #
    t1_x = 2 * x / a**2 + 2 * a**2 * (y - b * x**2 / a**2 - b * a**2) * (
        -2 * b * x / a**2
    )
    t1_y = 2 * a**2 * (y - b * x**2 / a**2 - b * a**2)
    t1 = np.array([t1_x, t1_y])
    #
    t2_x = -2 * rho * y + 6 * b * rho * x**2 / a**2 + 2 * rho * b * a**2

    t2_y = -2 * rho * x
    t2 = np.array([t2_x, t2_y])
    #
    pdf_grad *= t1 + t2
    #
    return pdf_grad


def banana_log_density(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    The logarithm of the banana distribution PDF
    """
    return -banana_potential_energy_value(
        state,
        a=a,
        b=b,
        rho=rho,
    )


def banana_log_density_gradient(
    state,
    a=2.15,
    b=0.75,
    rho=0.9,
):
    """
    The gradient of the logarithm of the banana distribution PDF
    """
    return -banana_potential_energy_gradient(
        state,
        a=a,
        b=b,
        rho=rho,
    )


def sample_banana_distribution(
    sample_size,
    verbose=False,
    method="hmc",
    constraint_test=None,
):
    """
    Simplified example to sample a Banana-shaped probability distributions
    """
    initial_state = [0, 0]

    if method == "hmc":
        sampler = create_hmc_sampler(
            size=2,
            log_density=banana_log_density,
            log_density_grad=banana_log_density_gradient,
            constraint_test=constraint_test,
        )
    elif method == "mcmc":
        sampler = create_mcmc_sampler(
            size=2,
            log_density=banana_log_density,
            constraint_test=constraint_test,
        )
    else:
        raise ValueError(f"Unrecognized sampling method '{method}'")

    # Sample and return full results
    results = sampler.start_MCMC_sampling(
        sample_size=sample_size, initial_state=initial_state, full_diagnostics=True
    )

    return results
