# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from dataclasses import dataclass, replace
from typing import Callable
import warnings

import numpy as np
import scipy as sp
from scipy import sparse

from ...utility import (
    asarray,
    isnumber,
    isiterable,
    factorize_spsd_matrix,
    matrix_logdet,
    plot_sampling_results,
)
from ...utility.mixins import RandomNumberGenerationMixin
from ...configs import (
    set_configurations,
    PyOEDData,
    PyOEDConfigsValidationError,
    validate_key,
)
from pyoed.stats.core.sampling import (
    Proposal,
    ProposalConfigs,
)


## Module-level variables
_CONSTRAINT_MAX_TRIALS = 100  # Maximum number of trials (per point) to satisfy constraints (e.g., bounds) for proposl/sampler


@dataclass(kw_only=True, slots=True)
class GaussianProposalConfigs(ProposalConfigs):
    """
    Configurations for the GaussianProposal class.

    :param size: dimension of the target distribution to sample.
    :param mean: mean of the proposal. If `None` (default), the mean is set to the
        current state passed to the `sample` or `__call__` method.
    :param variance: variance of the proposal. If `None`, the variance is set to 1.0.
    :param constraint_test: a function that returns a boolean value `True` if sample
        point satisfy any desired constraints, and `False` otherwise; ignored if `None`,
        is passed.
    :param constraint_max_trials: the maximum number of trials to satisfy constraints
        (e.g., bounds) for each sample point. Default is 100.
    """

    size: int | None = None
    mean: float | np.ndarray | None = None
    variance: float | np.ndarray | sparse.spmatrix = 1.0
    constraint_test: Callable[[np.ndarray], bool] | None = None
    constraint_max_trials: int = 100
    sparse = True
    name: str = "Gaussian"


@dataclass(kw_only=True, slots=True)
class GaussianCovarianceMatrix(PyOEDData):
    """
    Data container for a covariance matrix of a Gaussian proposal.
    """

    matrix: np.ndarray | sparse.spmatrix
    inverse: np.ndarray | sparse.spmatrix
    sqrt: np.ndarray | sparse.spmatrix

    def __str__(self):
        sep = "*" * 20
        return (
            f"{sep}\n  Gaussian Proposal Covariance Matrix Container \n{sep}\n"
            f"Matrix: {self.matrix}\n"
            f"Inverse: {self.inverse}\n"
            f"Square Root: {self.sqrt}\n"
        )


@set_configurations(configurations_class=GaussianProposalConfigs)
class GaussianProposal(Proposal, RandomNumberGenerationMixin):
    """
    A class implementing a Gaussian proposal for MCMC sampling.

    :param configs: Configurations object, see :py:class:`GaussianProposalConfigs`
    """

    def __init__(self, configs: dict | GaussianProposalConfigs | None = None):
        super().__init__(self._standardize_configs(configs))

        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

        # Update the covariance matrix
        self._COVARIANCE = self._build_covariance(variance=self.configurations.variance)

    def _standardize_configs(
        self, configs: dict | GaussianProposalConfigs
    ) -> GaussianProposalConfigs:
        """
        Convert the passed configs to a standardized format.
        This allows great flexibility for the user where, for example,
        a scalar variance is passed for `N` dimensoinal Gaussian.
        In this case, the covariance matrix is casted into a sparse/dense
        diagonal array/matrix with that scalar on its main diagonal.
        The mean is assured to be a vector of length `N` as well.

        .. note::
            The size is allowed to be `None` as long as it can be inferred
            from `mean` or `covariance`. However, it is best practice always
            to pass a proper value of the `size` argument

        :param configs: full or partial configurations required to create/update
          the :py:class:`GaussianProposal` class/instance.

        :returns: a version of the passed `configs` that is guaranteed to by
            an instance of :py:class:`GaussianProposalConfigs` with proper
            values those are conformable with each other.

        :raises PyOEDConfigsValidationError: if any of the configs are of
            invalid shape/type

        .. note::

            The logic of stanardization is as follows:

            1. The `size` is validated against sizes of `mean` and `variance`.
        """
        # Create a proper `GaussianProposalConfigs` object
        configs = self.configurations_class.data_to_dataclass(configs)

        # Extract entries
        size = configs.size
        mean = configs.mean
        variance = configs.variance
        sparse_ = configs.sparse
        constraint_test = configs.constraint_test

        # Validate `constraint_test`
        if not (constraint_test is None or callable(constraint_test)):
            raise PyOEDConfigsValidationError(
                "Constraint test must be a callable or none; "
                f" received {type(constraint_test)}"
            )

        # Validate check type of the mean
        if isnumber(mean) or isinstance(mean, np.ndarray) or isiterable(mean):
            mean = asarray(mean, ).flatten()  # local copy
        elif mean is None:
            pass
        else:
            raise PyOEDConfigsValidationError(
                f"Expected 'mean' to be `None`, scalar, or 1d iterable/array of scalars\n"
                f"Received unexpected/unsupported {type(mean)=} "
            )

        # Validate/Check type of the variance
        if isnumber(variance):
            variance = asarray(variance).flatten()

        elif isinstance(variance, np.ndarray):
            variance = asarray(variance)  # local copy

        elif sparse.issparse(variance):
            variance = sparse.csc_matrix(variance, copy=True)

        else:
            raise PyOEDConfigsValidationError(
                f"Expected 'variance' to be scalar, 1d/2d iterable/array of scalars\n"
                f"Received unexpected/unsupported {type(variance)=} "
            )

        ## functions to check consistency of mean/variance against size
        def mean_consistent(mean_, size_):
            if not ((mean_.size == size_) or (mean_.size == 1)):
                raise PyOEDConfigsValidationError(
                    f"Inconsistent mean size."
                    f" Expected either scalar or vector of {size_=}"
                    f" Found mean with {mean_.size=}"
                )
        def variance_consistent(variance__, size_):
            if not (
                isnumber(variance) or
                variance__.size == 1 or
                np.prod(variance__.shape) == size_ or
                variance__.shape == (size_, size_)
            ):
                raise PyOEDConfigsValidationError(
                    "Inconsistent variance size."
                    f" Expected either scalar or vector of {size_=}"
                    f" or matrix of shape ({size_}, {size_})."
                    f" Found variance with {variance__.shape=}"
                )

        ## Deduce size and validate against mean/variance
        # A few different scenarios can happen here:
        if mean is None and size is None:  # deduce size from variance
            size = variance.shape[0]
        elif mean is None and size is not None:  # ensure variance is OK
            variance_consistent(variance, size)
        elif mean is not None and size is None:  # deduce size and ensure variance is OK
            size = max(mean.size, variance.shape[0])
            mean_consistent(mean, size)
            variance_consistent(variance, size)
        elif mean is not None and size is not None:  # ensure mean & variance are OK
            mean_consistent(mean, size)
            variance_consistent(variance, size)

        # Convert mean and variance to final types/shapes
        ## mean
        if mean is not None and (1 == mean.size <size):
            mean = np.ones(size) * mean[0]

        ## variance (2d square array/matrix)
        if np.prod(variance.shape) == 1:
            variance = asarray(variance).flatten()[0]

            if sparse_:
                variance = sparse.diags(np.full(size, variance), offsets=0, format="csc",)
            else:
                variance = np.diag(np.full(size, variance), k=0)

        else:
            # Nd. Need to check with a vector (diagonal) is passed or full array
            if variance.shape in [(size, 1), (1, size), (size, )]:
                if sparse.issparse(variance):
                    variance = asarray(variance).flatten()

                if sparse_:
                    variance = sparse.diags(variance, offsets=0, format="csc", )
                else:
                    variance = np.diag(variance, k=0)

            elif variance.shape == (size, size):
                pass

            else:
                raise PyOEDConfigsValidationError(
                    "Inconsistent variance shape.\n"
                    f"Expected shape to be 2d array of shape ({size}, {size}). "
                    f"Found {variance} of {type(variance)=} with {variance.shape=}"
                )

        # Sparsity update
        if sparse_ and not sparse.issparse(variance):
            variance = sparse.csc_matrix(variance)
        elif not sparse_ and sparse.issparse(variance):
            variance = variance.toarray()

        ## Final sanity check in our code
        # Mean (None or 1d array of size `size`)
        if mean is not None:
            if mean.size != size:
                raise PyOEDConfigsValidationError(
                    "Inconsistent mean size."
                    f" Expected mean to be 1d array of size {size=}"
                    f" Found {mean} of {type(mean)=} with {mean.shape=}"
                )

        # Variance 2d array of shape (`size`, `size`)
        if variance.shape != (size, size):
            raise PyOEDConfigsValidationError(
                "Inconsistent variance shape.\n"
                f"Expected shape to be 2d array of shape ({size}, {size}). "
                f"Found {variance} of {type(variance)=} with {variance.shape=}"
            )

        if sparse_ != sparse.issparse(variance):
            raise PyOEDConfigsValidationError(
                f"Inconsistent sparsity!\n"
                f"{sparse_=}; {sparse.issparse(variance)=}. "
                f"Found {variance} of {type(variance)=} with {variance.shape=}"
            )

        return replace(
            configs,
            size=size,
            mean=mean,
            variance=variance,
            constraint_test=constraint_test,
        )

    def _build_covariance(self, variance=None) -> GaussianCovarianceMatrix:
        """
        Update the variance/covariance matrix used for proposing new sample points

        :param variance: the variance/covariance matrix to be used/set.
            If None, the one in the configurations dictionary is used. Thus, one can
            update covariances by calling udpate_configurations(variance=new_value).

        :returns: a GaussianCovarianceMatrix object holding the covariance matrix, its
            inverse, and its square root (Cholesky factor).
        """
        # Create the inverse of the covariance matrix once.
        if variance is None:
            variance = self.configurations.variance
        matrix = variance.copy()
        inv = sparse.linalg.inv(matrix) if self.sparse else np.linalg.inv(matrix)
        sqrt = factorize_spsd_matrix(matrix)
        return GaussianCovarianceMatrix(matrix=matrix, inverse=inv, sqrt=sqrt)

    def validate_configurations(self, configs, raise_for_invalid=True):
        """
        A method to check the passed configurations and make sure they
        are conformable with each other, and with current configurations once combined.
        This guarantees that any key-value pair passed in configs can be properly used.

        :param configs: an object holding configurations
        :param bool raise_for_invalid: If True, raise an exception if any of the passed
            configurations is invalid.

        :returns: True/False flag indicating whether passed configurations dictionary is
            valid or not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        # Bypassing the usual construct because _standardize_configs does what we need.
        try:
            self._standardize_configs(aggregated_configs)
        except (TypeError, ValueError) as e:
            if raise_for_invalid:
                raise PyOEDConfigsValidationError(
                    f"Failed to validate configurations, see trace for details\n"
                    f"err={e}"
                ) from e
            return False

        return super().validate_configurations(
            aggregated_configs, raise_for_invalid=raise_for_invalid
        )

    def generate_white_noise(self, size, truncate=False) -> np.ndarray:
        """
        Generate a standard normal random vector of size `size` with values truncated at
            -/+3 if `truncate` is set to `True`

        :param int size: dimension of the probability space to sample
        :param bool truncate: if `True`, truncate the samples at -/+3, that is any
            sample point/entry above 3 is set to 3, and any value below -3 is set to -3.

        :returns: a numpy array of size `size` sampled from a standard multivariate
            normal distribution of dimension `size` with mean 0 and covariance matrix
            equals an identity matrix.

        :remarks:
            - this function returns a numpy array of size `size` even if `size` is set
              to 1
        """
        # Sample
        white_noise = self.random_number_generator.standard_normal(size)
        # Truncate if requested
        if truncate:
            white_noise[white_noise > 3] = 3
            white_noise[white_noise < -3] = -3
        return white_noise

    def covariance_matrix_matvec(self, state) -> np.ndarray:
        """
        Multiply the mass matrix (in the configurations) by the passed state/vector
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise TypeError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )

        return self.covariance.matrix.dot(state)

    def covariance_matrix_inv_matvec(self, state) -> np.ndarray:
        """
        Multiply the inverse of the mass matrix (in the configurations) by the passed state/vector
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise TypeError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )
        return self.covariance.inverse.dot(state)

    def covariance_matrix_sqrt_matvec(self, state) -> np.ndarray:
        """
        Multiply the Square root (Lower Cholesky factor) of the mass matrix (in the configurations) by the passed state/vector
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise TypeError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )
        return self.covariance.sqrt.dot(state)

    def sample(
        self,
        sample_size=1,
        initial_state=None,
    ) -> np.ndarray:
        """
        Generate and return a sample of size `sample_size` from
        a Gaussian distribution centered around the passed `initial_state`.
        If the `initial_state` is not passed, the preconfigured `mean`
        is used. Thus, the proposal mean must be set in configurations,
        otherwise a TypeError is raised

        :returns: 1d (if internal dimension/size is 1) or 2d numpy
            array (if internal dimension/size is more than 1)
            holding samples.
            Each entry `sample[i]` (row element) retrieves a sample
            point from the underlying probability distribution.
        """
        size = self.size
        if initial_state is None and self.mean is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass "
                "an `initial_state` to propose around"
            )
        elif initial_state is not None:
            mean = asarray(initial_state).flatten()
        else:
            mean = asarray(self.mean).flatten()

        if mean.size != size:
            raise ValueError(
                "The proposal mean has wrong size/shape!"
                f" Expected state/mean of size {size}; received size {mean.size}"
            )

        # Retrieve an constraint test (if assigned)
        constraint_test = self.constraint_test
        if constraint_test is None:
            constraint_test = lambda x: True

        # Initialize samples container
        proposed_samples = np.empty((sample_size, size))
        proposed_samples[...] = np.nan

        for i in range(sample_size):
            success = False
            for _ in range(self.constraint_max_trials):
                sample = self.generate_white_noise(size=size)
                sample = self.covariance_matrix_sqrt_matvec(sample) + mean
                if constraint_test(sample):
                    proposed_samples[i, :] = sample
                    success = True
                    break
            if not success:
                raise ValueError(
                    "Max # of trials for constraint satisfaction has been exceeded!"
                    " Bailing out! Consider increasing the max # of trials, or"
                    " inspecting the mathematics underlying the constraint."
                )

        return proposed_samples

    def pdf(
        self,
        state,
        normalize=False,
        log=False,
        mean=None,
    ):
        """
        Evaluate the value of the density function (normalized or upto a *fixed* scaling
        constant) at the passed state/vector.

        If the `mean` is not passed, the preconfigured `mean` is used, thus, the
        proposal mean must be set in configurations, otherwise a TypeError is raised

        :param state: What to evaluate the density function at.
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed
            state/vector.
        """
        if mean is None and self.mean is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an alternative mean value (initial state) to propose around"
            )
        mean = asarray(mean).flatten() if mean is not None else self.mean
        pdf = self.log_density(
            state=state,
            normalize=normalize,
            mean=mean,
        )
        # scale (if needed) and return
        if not log:
            pdf = np.exp(pdf)
        return pdf

    def pdf_gradient(
        self,
        state,
        normalize=False,
        log=False,
        mean=None,
    ):
        """
        Evaluate the gradient of the density function at the passed state/vector.

        If the `mean` is not passed, the preconfigured `mean` is used, thus, the
        proposal mean must be set in configurations, otherwise an error raised

        :param state:
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state/vector.
        """
        if mean is None and self.mean is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an alternative mean value (initial state) to propose around"
            )
        mean = asarray(mean).flatten() if mean is not None else self.mean
        grad = self.log_density_gradient(
            state=state,
            mean=mean,
        )
        if not log:
            grad *= self.pdf(
                state=state,
                log=False,
                normalize=False,
                mean=mean,
            )
            if normalize:
                scl = -self.size / 2.0 * np.log(2 * np.pi)
                scl -= 0.5 * matrix_logdet(
                    self.covariance.matrix,
                    method="exact",
                )
                grad *= scl

        return grad

    def log_density(
        self,
        state,
        normalize=False,
        mean=None,
    ):
        """
        Evaluate the logarithm of the density function at the passed state, where the
        distrubution mean is adjusted to `mean`. If `mean` is `None` the configured mean
        is used. This parameter is passed to satisify the needs for the case where the
        proposal mean changes frequently, e.g., MCMC.

        :param state:
        :param mean:
        :param bool normalize: scale the PDF by the normalization factor
        :param bool normalize:

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        state = asarray(state).flatten()
        size = self.size
        if mean is None and self.mean is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an alternative mean value (initial state) to propose around"
            )
        mean = asarray(mean).flatten() if mean is not None else self.mean
        if not (mean.size == state.size == size):
            raise ValueError(
                "The proposal mean and/or state has wrong size/shape!\n"
                f"Expected mean of size {size}; received size {mean.size}\n"
                f"Expected state of size {size}; received size {state.size}"
            )

        # Evaluate the exponent
        innov = state - mean
        scaled_innov = self.covariance_matrix_inv_matvec(innov)
        pdf = -0.5 * np.dot(innov, scaled_innov)

        # Normalization constant (log-scale)
        if normalize:
            scl = -size / 2.0 * np.log(2 * np.pi)
            scl -= 0.5 * matrix_logdet(
                self.covariance.matrix,
                method="exact",
            )
            pdf += scl

        return pdf

    def log_density_gradient(
        self,
        state,
        normalize=False,
        mean=None,
    ):
        """
        Evaluate the gradient of the logarithm of the density function at the passed
        state, where the distrubution mean is adjusted to `mean`. If `mean` is `None`
        the configured mean is used. This parameter is passed to satisify the needs for
        the case where the proposal mean changes frequently, e.g., MCMC.

        :param state: the state to evaluate the gradient at
        :param normalize: scale the PDF by the normalization factor
        :param mean: the mean of the proposal distribution. If None, the configured mean
            is used.

        :returns: the gradient of the logarithm of the density function evaluated at the passed state.
        """
        state = asarray(state).flatten()
        size = self.size
        if mean is None and self.mean is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an alternative mean value (initial state) to propose around"
            )
        mean = asarray(mean).flatten() if mean is not None else self.mean
        if not (mean.size == state.size == size):
            raise ValueError(
                "The proposal mean and/or state has wrong size/shape!\n"
                f"Expected mean of size {size}; received size {mean.size}\n"
                f"Expected state of size {size}; received size {state.size}"
            )
        if normalize:
            raise NotImplementedError(
                "Normalization is not yet implemented for the gradient of the"
                " log-density"
            )

        # Evaluate the exponent
        innov = state - mean
        grad = -self.covariance_matrix_inv_matvec(innov)
        return grad

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in the configurations, and
        update as nessesary/possible/valid.
        """
        # Aggregate the passed settings to the current configurations
        self.validate_configurations(
            kwargs,
            raise_for_invalid=True,
        )

        if v := kwargs.pop("random_seed", None):
            self.update_random_number_generator(random_seed=v)

        RESTANDARDIZE = False
        REBUILD_COVARIANCE = False

        if v := kwargs.pop("mean", None):
            RESTANDARDIZE = True
            self.configurations.mean = v
        if v := kwargs.pop("variance", None):
            RESTANDARDIZE = REBUILD_COVARIANCE = True
            self.configurations.variance = v
        if v := kwargs.pop("sparse", None):
            RESTANDARDIZE = REBUILD_COVARIANCE = True
            self.configurations.sparse = v

        if RESTANDARDIZE:
            self.configurations = self._standardize_configs(self.configurations)
            if REBUILD_COVARIANCE:
                self._COVARIANCE = self._build_covariance(self.configurations.variance)

        if v := kwargs.pop("constraint_test", None):
            self.configurations.constraint_test = v
        if v := kwargs.pop("constraint_max_trials", None):
            self.configurations.constraint_max_trials = v

        if v := kwargs.pop("size", None):
            raise ValueError("Cannot change the size of the Gaussian proposal!")

        if kwargs:
            super().update_configurations(**kwargs)

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        if self.configurations is not None:
            return self.configurations.size
        else:
            raise AttributeError(
                f"The Proposal has not been yet fully configured. \n"
                f"size` property is only accesible after full "
                f"configuration/initialization."
            )

    @property
    def mean(self):
        """Return the mean of the Gaussian proposal"""
        return self.configurations.mean

    @property
    def covariance(self) -> GaussianCovarianceMatrix:
        """Return the covariance matrix of the Gaussian proposal"""
        return self._COVARIANCE

    @property
    def sparse(self):
        """Return the sparsity configuration"""
        return self.configurations.sparse

    @property
    def constraint_test(self):
        """Return the constraint test function"""
        return self.configurations.constraint_test

    @property
    def constraint_max_trials(self):
        """Return the maximum number of trials to satisfy constraints"""
        return self.configurations.constraint_max_trials


@dataclass(kw_only=True, slots=True)
class GaussianMixtureModelProposalConfigs(ProposalConfigs):
    """
    Configurations for the GaussianMixtureModelProposal class.

    :param size: dimension of the target distribution to sample.
    :param num_components: the number of components in the mixture. This is mandatory.
    :param weights: a list of weights of each of the GMM component. These must sum up to
        1. This is mandatory.
    :param means: a list of means of each of the components of the GMM model. If `None`,
        all components will be centered around the current state passed to the `sample`
        or `__call__` method.
    :param variances: a list of variances of each of the components of the GMM model.
        If `None`, all components will have a variance of 1.0.


    """

    size: int | None = None
    num_components: int | None = None
    weights: list[float] | None = None
    means: list[float | np.ndarray] | None = None
    variances: list[float | np.ndarray | sparse.spmatrix] | None = None
    constraint_test: Callable[[np.ndarray], bool] | None = None
    constraint_max_trials: int = 100
    sparse: bool = True
    random_seed: int = 123
    name: str = "GMM Proposal"


@set_configurations(GaussianMixtureModelProposalConfigs)
class GaussianMixtureModelProposal(Proposal, RandomNumberGenerationMixin):
    """
    A class implementing a Gaussian Mixture Model (GMM) proposal for MCMC sampling.

    :param configs: A configurations object, see
        :py:class:`GaussianMixtureModelProposalConfigs`
    """

    def __init__(
        self, configs: dict | GaussianMixtureModelProposalConfigs | None = None
    ):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        # Post validation, guaranteed that size & num_components are set and valid.
        # Now, standardize the rest of the configurations and build covariances.
        c = self.configurations  # shorthand
        c.weights = self._standardize_weights(c.weights)
        c.means = self._standardize_means(c.means)
        c.variances = self._standardize_variances(c.variances)
        self._COVARIANCES = self._build_covariance_matrices(c.variances)
        self.update_random_number_generator(random_seed=c.random_seed)

    def _standardize_weights(self, weights) -> list[float]:
        """
        Standardize the weights of the GMM components.
        """
        num_components = self.num_components
        weights = asarray(weights).flatten()
        if len(weights) != num_components:
            raise ValueError(
                f"The passed parameter is incompatible with `num_components`!"
                f"Expected {num_components=}; recieved parameter with {len(weights)=}!"
            )
        if abs(np.sum(weights) - 1) > 1e-8:
            raise ValueError(
                f"The passed weights must add up to 1. "
                f"{np.sum(weights)=} which is not acceptable!"
            )
        return weights

    def _standardize_means(self, means) -> list[np.ndarray] | None:
        """
        Standardize and check the means of the GMM components.
        """
        size, num_components = self.size, self.num_components
        if means is None:
            return None
        validated_means = [asarray(mean).flatten() for mean in means]
        if any(mean.size != size for mean in validated_means):
            raise ValueError(f"Mismatch in the size of the passed means!")
        return validated_means

    def _standardize_variances(self, variances) -> list[np.ndarray | sparse.spmatrix]:
        """
        Standardize and check the variances of the GMM components.
        """
        size, num_components = self.size, self.num_components
        if len(variances) != num_components:
            raise ValueError(
                f"The passed parameter is incompatible with {num_components=}!"
                f" Recieved parameter with {len(variances)=}!"
            )

        def _standardize_variance(variance):
            if isnumber(variance):
                variance = np.array(variance).flatten()
            elif isinstance(variance, np.ndarray):
                variance = np.array(variance)
            elif sparse.issparse(variance):
                variance = sparse.csc_matrix(variance, copy=True)
            else:
                raise TypeError(f"`variance` is of unsupported type: {type(variance)}")

            variance_size = variance.shape[0]
            if not (variance_size == 1) or (variance_size == size):
                raise ValueError(
                    "Inconsistent variance size."
                    f" Expected either scalar or vector of {size=}"
                    f" or matrix of shape ({size}, {size})."
                    f" Found variance with {variance.shape=}"
                )

            if variance.size == 1:
                if sparse.issparse(variance):
                    variance = variance.toarray()
                variance = np.diag(np.full(size, variance[0]), k=0)
            elif variance.size == size:
                if sparse.issparse(variance):
                    variance = variance.toarray()
                if self.sparse:
                    variance = sparse.diags(
                        variance, offsets=0, format="csc",
                    )
                else:
                    variance = np.diag(variance, k=0)
            else:  # variance.size == size**2 # Good
                pass
            return variance

        validated_variances = [
            _standardize_variance(variance) for variance in variances
        ]
        if any(variance.shape != (size, size) for variance in validated_variances):
            raise ValueError(f"Mismatch in the size of the passed variances!")
        if any(variance.size == 1 for variance in validated_variances):
            self.sparse = False
        return validated_variances

    def _build_covariance_matrices(self, variances) -> list[GaussianCovarianceMatrix]:
        """
        Build the covariance matrices of the GMM components.

        :param variances: the variance/covariance matrices of GMM components to be
            used/set.
        """

        def _build_covariance(self, variance) -> GaussianCovarianceMatrix:
            matrix = variance.copy()
            inv = sparse.linalg.inv(matrix) if self.sparse else np.linalg.inv(matrix)
            sqrt = factorize_spsd_matrix(matrix)
            return GaussianCovarianceMatrix(matrix=matrix, inverse=inv, sqrt=sqrt)

        return [_build_covariance(variance) for variance in variances]

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in the configurations, and
        update as nessesary/possible/valid.
        """
        # Aggregate the passed settings to the current configurations
        self.validate_configurations(kwargs, raise_for_invalid=True)

        c = self.configurations  # shorthand

        # Updates with side-effects
        REBUILD_COVARIANCES = False
        if v := kwargs.pop("variances", None):
            c.variances = self._standardize_variances(v)
            REBUILD_COVARIANCES = True
        if v := kwargs.pop("sparse", None):
            c.sparse = v
            REBUILD_COVARIANCES = True
        if REBUILD_COVARIANCES:
            self._COVARIANCES = self._build_covariance_matrices(c.variances)
        if v := kwargs.pop("random_seed", None):
            self.update_random_number_generator(random_seed=v)

        # Simple updates
        if v := kwargs.pop("weights", None):
            c.weights = self._standardize_weights(v)
        if v := kwargs.pop("means", None):
            c.means = self._standardize_means(v)
        if v := kwargs.pop("constraint_test", None):
            c.constraint_test = v
        if v := kwargs.pop("constraint_max_trials", None):
            c.constraint_max_trials = v

        # Unsettable configurations
        if "size" in kwargs:
            raise ValueError("Cannot change the size of the GMM proposal!")
        if "num_components" in kwargs:
            raise ValueError(
                "Cannot change the number of components of the GMM proposal!"
            )

        super().update_configurations(**kwargs)

    def validate_configurations(
        self,
        configs: dict | GaussianMixtureModelProposalConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        A method to check the passed configurations and make sure they are conformable
        with each other, and with current configurations once combined. This guarantees
        that any key-value pair passed in configs can be properly used.

        :param configs: an object holding configurations
        :param bool raise_for_invalid: If True, raise an exception if any of the passed
            configurations is invalid.

        :returns: True/False flag indicating whether passed configurations dictionary is
            valid or not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        if not validate_key(
            aggregated_configs,
            configs,
            "num_components",
            test=lambda x: isinstance(x, int) and x > 0,
            message="`num_components` must be a positive integer!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "size",
            test=lambda x: isinstance(x, int) and x > 0,
            message=(
                "`size` must be a positive integer! GMM does not dynamically"
                " sense the size of the state space from the passed state."
            ),
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraints_test",
            test=lambda x: callable(x) or x is None,
            message="`constraints_test` must be a callable function or None!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraint_max_trials",
            test=lambda x: isinstance(x, int) and x > 0,
            message="`constraint_max_trials` must be a positive integer!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "sparse",
            test=lambda x: isinstance(x, bool),
            message="`sparse` must be a boolean!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Bypassing the usual construct because _standardize_X does what we need.
        if "weights" in configs:
            try:
                self._standardize_weights(configs["weights"])
            except (TypeError, ValueError) as e:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        "Failed to validate weights, see trace for details"
                    ) from e
                return False

        if "means" in configs:
            try:
                self._standardize_means(configs["means"])
            except (TypeError, ValueError) as e:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        "Failed to validate means, see trace for details"
                    ) from e
                return False

        if "variances" in configs:
            try:
                self._standardize_variances(configs["variances"])
            except (TypeError, ValueError) as e:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        "Failed to validate variances, see trace for details"
                    ) from e
                return False

        return super().validate_configurations(configs, raise_for_invalid)

    def generate_white_noise(self, size, truncate=False):
        """
        Generate a standard normal random vector of size `size` with values truncated
            at -/+3 if `truncate` is set to `True`

        :param int size: dimension of the probability space to sample
        :param bool truncate: if `True`, truncate the samples at -/+3, that is any sample
            point/entry above 3 is set to 3, and any value below -3 is set to -3.

        :returns: a numpy array of size `size` sampled from a standard multivariate normal
            distribution of dimension `size` with mean 0 and covariance matrix equals
            an identity matrix.

        :remarks:
            - this function returns a numpy array of size `size` even if `size` is set to 1
        """
        # Sample
        white_noise = self.random_number_generator.standard_normal(size)

        # Truncate if requested
        if truncate:
            white_noise[white_noise > 3] = 3
            white_noise[white_noise < -3] = -3
        return white_noise

    def covariance_matrix_matvec(self, state, component):
        """
        Multiply the covariance matrix of the choosen `component` by the passed state/vector

        :param state: the state (random variable) to multiple covariance by
        :param int component: the index of the component to multiply by.
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise ValueError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )
        if not (0 <= component < self.num_components):
            raise ValueError(
                "Invalid component index {component=}. Expected index in [0, {num_components=}]"
            )
        return self.covariances[component].matrix.dot(state)

    def covariance_matrix_inv_matvec(self, state, component):
        """
        Multiply the inverse of the covariance matrix of the choosen `component` by the
        passed state/vector

        :param state: the state (random variable) to multiple covariance by
        :param int component: the index of the component to multiply by.
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise TypeError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )
        if not (0 <= component < self.num_components):
            raise ValueError(
                f"Invalid component index {component=}. Expected index in [0, {self.num_components=}]"
            )
        return self.covariances[component].inverse.dot(state)

    def covariance_matrix_sqrt_matvec(self, state, component):
        """
        Multiply the square root (lower Cholesky factor) of the covariance matrix of the
        choosen `component` by the passed state/vector

        :param state: the state (random variable) to multiple covariance by
        :param int component: the index of the component to multiply by.
        """
        state = asarray(state).flatten()
        if state.size != self.size:
            raise TypeError(
                f"The passed state/vector has invalid size;"
                f"received {state}, expected {self.size}"
            )
        if not (0 <= component < self.num_components):
            raise ValueError(
                f"Invalid component index {component=}. Expected index in [0, {self.num_components=}]"
            )
        return self.covariances[component].sqrt.dot(state)

    def sample_component(
        self,
        component,
        sample_size=1,
        initial_state=None,
    ) -> np.ndarray:
        """
        Generate and return a sample of size `sample_size` from a Gaussian distribution
        centered around the passed `initial_state`.
        If the `initial_state` is not passed, the preconfigured `mean` of the
        corresponding component is used, thus, the proposal mean must be set in
        configurations, otherwise an error is raised

        :param int component: the index of the component to multiply by.
        :param int sample_size: the number of samples to generate
        :param initial_state: the state to propose around. If `None`, the preconfigured
            mean of the component is used.

        :returns: 1d (if internal dimension/size is 1) or 2d numpy array (if internal
            dimension/size is more than 1) holding samples. Each entry `sample[i]` (row
            element) retrieves a sample point from the underlying probability
            distribution.
        """
        num_components = self.num_components
        if not (0 <= component < num_components):
            raise ValueError(
                f"Invalid component index {component=}. Expected index in [0, {num_components=}]"
            )
        size = self.size
        if initial_state is None and self.means is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an `initial_state` to propose around"
            )
        elif initial_state is not None:
            mean = asarray(initial_state).flatten()
        else:
            mean = self.means[component]

        if mean.size != size:
            raise ValueError(
                f"The proposal mean has wrong size/shape!"
                f"Expected state/mean of size {size}; received size {mean.size}"
            )

        constraint_test = self.constraint_test
        if constraint_test is None:
            constraint_test = lambda x: True

        # Initialize samples container
        proposed_samples = np.empty((sample_size, size))
        proposed_samples[...] = np.nan

        for i in range(sample_size):
            success = False
            for _ in range(self.constraint_max_trials):
                sample = self.generate_white_noise(size=size)
                sample = self.covariance_matrix_sqrt_matvec(sample, component) + mean
                if constraint_test(sample):
                    proposed_samples[i, :] = sample
                    success = True
                    break
            if not success:
                raise ValueError(
                    "Max # of trials for constraint satisfaction has been exceeded!"
                    " Bailing out! Consider increasing the max # of trials, or"
                    " inspecting the mathematics underlying the constraint."
                )

        return proposed_samples

    def sample(
        self,
        sample_size: int = 1,
        initial_state=None,
    ) -> np.ndarray:
        """
        Generate GMM samples

        :param sample_size: the number of samples to generate from the GMM model
        :param initial_state: the state to propose around. If `None`, the preconfigured
            mean of the component is used.

        :returns: a numpy array of shape `(sample_size, size)` sampled from the GMM
            model
        """
        size, weights, num_components = self.size, self.weights, self.num_components

        # Sample weights according to probabilities
        components_indices = self.random_number_generator.choice(
            range(num_components), size=sample_size, p=weights
        )

        sample_sizes = np.zeros(num_components, dtype=int)
        for i in range(num_components):
            sample_sizes[i] = np.count_nonzero(components_indices == i)

        # Generate samples from each component
        samples = np.empty((sample_size, size))
        collected = 0
        for i, n in enumerate(sample_sizes):
            if n > 0:
                samples[collected : collected + n, :] = self.sample_component(
                    component=i,
                    sample_size=n,
                    initial_state=initial_state,
                )
                collected += n

        return samples

    def pdf(
        self,
        state,
        normalize=False,
        log=False,
        mean=None,
    ):
        """
        Evaluate the value of the density function (normalized or upto a *fixed* scaling
        constant) at the passed state/vector.

        If a value of the `mean` is passed, all components's means are shifted to this state.
        This gives flexibility in sampling using methods such as MCMC

        :param state:
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state/vector.
        """
        pdf = 0.0
        for i, w in enumerate(self.weights):
            pdf += w * self.pdf_component(
                state=state,
                normalize=normalize,
                component=i,
                mean=mean,
            )
        # scale (if needed) and return
        if log:
            pdf = np.log(pdf)
        return pdf

    def log_density(
        self,
        state,
        normalize=False,
        mean=None,
    ):
        """
        Evaluate the logarithm of the density function at the passed state.

        If a value of the `mean` is passed, all components's means are shifted to this state.
        This gives flexibility in sampling using methods such as MCMC

        :param state:
        :param int component: the index of the component to multiply by.
            The components are enumerated 0, 1, ...
        :param bool normalize: scale the PDF by the normalization factor

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        return self.pdf(
            state=state,
            normalize=normalize,
            mean=mean,
            log=True,
        )

    def pdf_component(
        self,
        state,
        component,
        normalize=False,
        mean=None,
    ):
        """
        Evaluate the value of the density function of one component (a Gaussian) of the GMM model.

        If a value of the `mean` is passed, the coponent's means is shifted to this, otherwise,
        the one in the configurations dictionary is used.
        This gives flexibility in sampling using methods such as MCMC

        :param state:
        :param int component: the index of the component to multiply by.
            The components are enumerated 0, 1, ...
        :param bool normalize: scale the PDF by the normalization factor

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        log_pdf = self.log_density_component(
            state=state,
            component=component,
            normalize=normalize,
            mean=mean,
        )
        pdf = np.exp(log_pdf)
        return pdf

    def log_density_component(
        self,
        state,
        component,
        normalize=False,
        mean=None,
    ):
        """
        Evaluate the logarithm of the value of the density function of one component (a Gaussian) of the GMM model.

        If a value of the `mean` is passed, the coponent's means is shifted to this, otherwise,
        the one in the configurations dictionary is used.
        This gives flexibility in sampling using methods such as MCMC

        :param state:
        :param int component: the index of the component to multiply by.
            The components are enumerated 0, 1, ...
        :param bool normalize: scale the PDF by the normalization factor

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        state = asarray(state).flatten()
        if mean is None and self.means is None:
            raise ValueError(
                "You must either set mean for the proposal, or pass an alternative mean value (initial state) to propose around"
            )
        mean = asarray(mean).flatten() if mean is not None else self.means[component]
        size = self.size
        if not (mean.size == state.size == size):
            raise TypeError(
                f"The proposal mean and/or state has wrong size/shape!\n"
                f"Expected mean of size {size}; received size {mean.size}\n"
                f"Expected state of size {size}; received size {state.size}"
            )
        # Evaluate the exponent
        innov = state - mean
        scaled_innov = self.covariance_matrix_inv_matvec(innov, component=component)
        pdf = -0.5 * np.dot(innov, scaled_innov)

        # Normalization constant (log-scale)
        if normalize:
            scl = -size / 2.0 * np.log(2 * np.pi)
            scl -= 0.5 * matrix_logdet(
                self._COVARIANCE_MATRICIES[component],
                method="exact",
            )
            pdf += scl

        return pdf

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        return self.configurations.size

    @property
    def num_components(self):
        """Return the number of components in the GMM model"""
        return self.configurations.num_components

    @property
    def weights(self):
        """Return the weights of the GMM model"""
        return self.configurations.weights

    @weights.setter
    def weights(self, value):
        """Set the weights of the GMM model"""
        self.validate_configurations({"weights": value})
        self.configurations.weights = self._standardize_weights(value)

    @property
    def means(self):
        """Return the means of the GMM model"""
        return self.configurations.means

    @means.setter
    def means(self, value):
        """Set the means of the GMM model"""
        self.validate_configurations({"means": value})
        self.configurations.means = self._standardize_means(value)

    @property
    def sparse(self):
        """Return the sparsity configuration"""
        return self.configurations.sparse

    @sparse.setter
    def sparse(self, value):
        """Set the sparsity configuration"""
        self.validate_configurations({"sparse": value})
        self.configurations.sparse = value
        self.configurations.variances = self._standardize_variances(
            self.configurations.variances
        )
        self._COVARIANCES = self._build_covariance_matrices(
            self.configurations.variances
        )

    @property
    def constraint_test(self):
        """Return the constraint test function"""
        return self.configurations.constraint_test

    @constraint_test.setter
    def constraint_test(self, value):
        """Set the constraint test function"""
        self.validate_configurations({"constraint_test": value})
        self.configurations.constraint_test = value

    @property
    def constraint_max_trials(self):
        """Return the maximum number of trials to satisfy constraints"""
        return self.configurations.constraint_max_trials

    @property
    def covariances(self) -> list[GaussianCovarianceMatrix]:
        """Return the covariance matrices of the GMM model"""
        return self._COVARIANCES


@dataclass(kw_only=True, slots=True)
class ExponentialProposalConfigs(ProposalConfigs):
    """
    Configurations for the Exponential proposal class.

    :param size: dimension of the target distribution to sample.
    :param scale: scale parameter of the proposal. This may be a scalar or a vector of
        size `size`.
    :param constraint_test: a function that returns a boolean value `True` if sample
        point
    :param constraint_max_trials: the maximum number of trials to satisfy constraints
    :param random_seed: random seed used when the object is initiated to keep track of
        random samples. This is useful for reproductivity. If `None`, random seed
        follows `numpy.random.seed` rules.
    """

    size: int | None = None
    scale: float | np.ndarray | None = None
    constraint_test: Callable[[np.ndarray], bool] | None = None
    constraint_max_trials: int = 100
    random_seed: int = 123
    name: str = "Exponential"


@set_configurations(ExponentialProposalConfigs)
class ExponentialProposal(Proposal, RandomNumberGenerationMixin):
    """
    A class implementing an Exponential proposal for MCMC sampling.
    The distribution assumes independent components of the random vector.
    """

    def __init__(self, configs: dict | ExponentialProposalConfigs | None = None):
        """
        Initialize the Exponential proposal object.

        :param configs: A configurations object, see :py:class:`ExponentialConfigs`
        """

        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)
        # Passed validation, now finish class and confi setup.
        self.scale = self.configurations.scale
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

    def validate_configurations(
        self,
        configs: dict | ExponentialProposalConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        A method to check the passed configurations and make sure they are conformable
        with each other, and with current configurations once combined. This guarantees
        that any key-value pair passed in configs can be properly used.

        :param configs: an object holding configurations
        :param bool raise_for_invalid: If True, raise an exception if any of the passed
            configurations is invalid.

        :returns: True/False flag indicating whether passed configurations dictionary is
            valid or not
        """
        aggregated_configs = self.aggregate_configurations(configs)

        if not validate_key(
            aggregated_configs,
            configs,
            "size",
            test=lambda x: isinstance(x, int) and x > 0,
            message=("`size` must be a positive integer!"),
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraints_test",
            test=lambda x: callable(x) or x is None,
            message="`constraints_test` must be a callable function or None!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            aggregated_configs,
            configs,
            "constraint_max_trials",
            test=lambda x: isinstance(x, int) and x > 0,
            message="`constraint_max_trials` must be a positive integer!",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if "scale" in configs:
            try:
                self._standardize_scale(configs["scale"])
            except (TypeError, ValueError) as e:
                if raise_for_invalid:
                    raise PyOEDConfigsValidationError(
                        "Failed to validate scale, see trace for details"
                    ) from e
                return False

        return super().validate_configurations(configs, raise_for_invalid)

    def sample(
        self,
        sample_size=1,
        initial_state=None,
    ):
        """
        Generate and return a sample of size `sample_size` from an iid Exponential
        distribution with predefine scale parameter.
        The `initial_state` is not used here; it is added to unify the :py:meth:`sample`
        interface across proposals.

        :returns: 1d (if internal dimension/size is 1) or 2d numpy array (if internal
            dimension/size is more than 1) holding samples. Each entry `sample[i]` (row
            element) retrieves a sample point from the underlying probability
            distribution.
        """
        size, scale = self.size, self.scale

        constraint_test = self.constraint_test
        if constraint_test is None:
            constraint_test = lambda x: True

        proposed_samples = np.empty((sample_size, size))
        proposed_samples[...] = np.nan
        for i in range(sample_size):
            success = False
            for _ in range(self.constraint_max_trials):
                sample = self.random_number_generator.exponential(
                    scale=scale, size=size
                )
                if constraint_test(sample):
                    proposed_samples[i, :] = sample
                    success = True
                    break
            if not success:
                raise ValueError(
                    "Max # of trials for constraint satisfaction has been exceeded!"
                    " Bailing out! Consider increasing the max # of trials, or"
                    " inspecting the mathematics underlying the constraint."
                )
        return proposed_samples

    def pdf(
        self,
        state,
        normalize=False,
        log=False,
    ):
        """
        Evaluate the value of the density function (normalized or upto a *fixed* scaling
        constant) at the passed state.

        :param state:
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state.
        """
        pdf = self.log_density(
            state=state,
            normalize=normalize,
        )
        # scale (if needed) and return
        if not log:
            pdf = np.exp(pdf)
        return pdf

    def pdf_gradient(
        self,
        state,
        normalize=False,
        log=False,
    ):
        """
        Evaluate the gradient of the density function at the passed state/vector.

        :param state:
        :param bool normalize: scale the PDF by the normalization factor
        :param bool log: if True, evaluate the gradient of the logarithm of the PDF

        :returns: the value of the density function evaluated at the passed state/vector.
        """
        grad = self.log_density_gradient(state=state)
        if not log:
            grad *= self.pdf(
                state=state,
                log=False,
                normalize=False,
            )
            if normalize:  # Retrieve the scale parameter (reciprocal of rate parameter)
                grad *= np.prod(self.scale)

        return grad

    def log_density(
        self,
        state,
        normalize=False,
    ):
        """
        Evaluate the logarithm of the density function at the passed state.

        :param state:
        :param bool normalize:

        :returns: the logarithm of the density function evaluated at the passed state.
        """
        state = asarray(state).flatten()
        size, scale = self.size, self.scale
        if not (scale.size == state.size == size):
            raise ValueError(
                f"The proposal rate/scaling parameter and/or state has wrong size/shape!\n"
                f"Expected scale/rate of size {size}; received size {scale.size}\n"
                f"Expected state of size {size}; received size {state.size}"
            )
        # Evaluate the unscaled PDF (log scale)
        pdf = -np.sum(state / scale)
        if normalize:
            pdf -= np.sum(np.log(scale))
        return pdf

    def log_density_gradient(
        self,
        state,
    ):
        """
        Evaluate the gradient of the logarithm of the density function at the passed state.

        :param state:
        :param bool normalize:

        :returns: the gradient of the logarithm of the density function evaluated at the passed state.
        """
        state = asarray(state).flatten()
        size, scale = self.size, self.scale
        if not (scale.size == state.size == size):
            raise ValueError(
                f"The proposal rate/scaling parameter and/or state has wrong size/shape!"
                f"Expected scale/rate of size {size}; received size {scale.size}"
                f"Expected state of size {size}; received size {state.size}"
            )
        # Evaluate the unscaled PDF (log scale)
        # TODO: Is this correct? -Abhi
        return -1.0 / scale

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in the configurations, and
        update as nessesary/possible/valid
        """
        # Aggregate the passed settings to the current configurations
        self.validate_configurations(kwargs, raise_for_invalid=True)

        if v := kwargs.pop("random_seed", None):
            self.update_random_number_generator(random_seed=v)

        c = self.configurations  # shorthand

        if v := kwargs.pop("scale", None):
            c.scale = self._standardize_scale(v)
        if v := kwargs.pop("constraint_test", None):
            c.constraint_test = v
        if v := kwargs.pop("constraint_max_trials", None):
            c.constraint_max_trials = v

        if "size" in kwargs:
            raise ValueError("Cannot change the size of the proposal!")

        if kwargs:
            super().update_configurations(**kwargs)

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        return self.configurations.size

    @property
    def scale(self):
        """Return the scale parameter of the exponential proposal"""
        return self.configurations.scale

    def _standardize_scale(
        self,
        scale=None,
    ):
        """
        Verify and standardize the scale parameter(s) of the exponential distribution.

        :param scale: the scale parameter to be used/set.
        """
        size = self.size

        ## Check the proposal scale parameter
        proposal_scale = scale
        if proposal_scale is None:
            raise TypeError("The proposal scale parameter must be set; received None!")
        proposal_scale = asarray(proposal_scale).flatten()
        if proposal_scale.size == 1 < size:
            proposal_scale = proposal_scale[0] * np.ones(size)
        elif proposal_scale.size != size:
            raise ValueError(
                f"The proposal mean has wrong size; "
                f"expected {size}; recevived {proposal_scale.size}"
            )
        return proposal_scale

    @scale.setter
    def scale(self, value):
        self.configurations.scale = self._standardize_scale(value)

    @property
    def constraint_test(self):
        """Return the constraint test function"""
        return self.configurations.constraint_test

    @constraint_test.setter
    def constraint_test(self, value):
        """Set the constraint test function"""
        self.configurations.constraint_test = value

    @property
    def constraint_max_trials(self):
        """Return the maximum number of trials to satisfy constraints"""
        return self.configurations.constraint_max_trials


## Visualization of a proposal and associated (builtin) sampling/proposing mechanisms
def visualize_proposal(
    proposal,
    sample_size=1000,
    #
    xlim=None,
    ylim=None,
    title=None,
    grid_size=200,
    labels=None,
    linewidth=1.0,
    markersize=2,
    fontsize=18,
    fontweight="bold",
    keep_plots=False,
    output_dir=None,
    filename_prefix=None,
    verbose=False,
):
    """

    :param Proposal proposal:
    :param int sample_size: size of the sample to generate for scatter plotting
    :param xlim: x-limit of plots (only used for 2D case)
    :param ylim: y-limit of plots (only used for 2D case)
    :param title: title to be added to created plot(s)
    :param grid_size: number of discretization grid points to create 2D mesh for plots (only used for 2D case)
    :param labels: iterable of labels to be used for the variables on plots (X_i auto used if not passed)
    :param linewidth: width of lines (e.g., for contour plots)
    :param markersize: markers in scatter plots
    :param fontsize: general font size on all plots
    :param fontweight: font weighti
    :param keep_plots: if `False` all plots will be closed by calling :py:meth:`matplotlib.pyplot.close('all')`
    :param output_dir: location to save files
    :param filename_prefix: if `None`, a prefix with proposal name will be chosen,
      e.g., `"Exponential_Proposal_"`
    :param bool verbose: screen verbosity

    :remarks: Set `keep_plots` to `True`, for example, if you are doing interactive plotting or
        using a notebook to show plots.
    """
    if not isinstance(proposal, Proposal):
        raise TypeError(f"Expected instance of `Proposal`; received {type(proposal)}")

    if output_dir is None:
        output_dir = proposal.configurations.output_dir

    # Sample the proposal
    if verbose:
        print(f"***Sampling the '{proposal.configurations.name}' proposal")

    sample = proposal.sample(sample_size=sample_size)
    _, size = sample.shape

    #  Log density function of the proposal
    log_density = lambda x: proposal.pdf(x, normalize=True, log=True)

    # Filename Prefix update
    if filename_prefix is None:
        filename_prefix = f"{proposal.configurations.name}_Proposal"

    # Create plots
    plot_sampling_results(
        sample=sample,
        log_density=log_density,
        xlim=xlim,
        ylim=ylim,
        title=title,
        grid_size=grid_size,
        labels=labels,
        linewidth=linewidth,
        markersize=markersize,
        fontsize=fontsize,
        fontweight=fontweight,
        keep_plots=keep_plots,
        output_dir=output_dir,
        filename_prefix=filename_prefix,
        verbose=verbose,
    )


## Simple interfaces (to generate instances from classes developed here).
def create_Gaussian_proposal(
    size,
    mean=None,
    variance=1,
    random_seed=None,
    output_dir=None,
    constraint_test=None,
) -> GaussianProposal:
    """
    Given the size of the target space, create and return an
    :py:class:`GaussianProposal` instance/object to generate samples using Gaussian
    proposal centered around current state (or predefined mean) Configurations/settings
    can be updated after inistantiation

    This function shows how to create :py:class:`GaussianProposal` instances (with some
    or all configurations passed)
    """
    return GaussianProposal(
        GaussianProposalConfigs(
            size=size,
            mean=mean,
            variance=variance,
            random_seed=random_seed,
            constraint_test=constraint_test,
            output_dir=output_dir,
        )
    )


## Simple interfaces (to generate instances from classes developed here).
def create_GaussianMixtureModel_proposal(
    size=2,
    num_components=3,
    weights=[0.5, 0.25, 0.25],
    means=[(0, 0), (0, 0.25), (0.25, 0)],
    variances=[0.04, 0.04, 0.04],
    random_seed=None,
    constraint_test=lambda x: 0 <= x[0] <= 1 and 0 <= x[1] <= 1,
) -> GaussianMixtureModelProposal:
    """
    Given the size of the target space, create and return an :py:class:`GaussianMixtureModelProposal` instance/object
    to generate samples using GMM proposal centered around current state (or predefined mean)
    Configurations/settings can be updated after inistantiation

    This function shows how to create :py:class:`GaussianProposal` instances
    (with some or all configurations passed)
    """
    configs = GaussianMixtureModelProposalConfigs(
        size=size,
        num_components=num_components,
        weights=weights,
        means=means,
        variances=variances,
        random_seed=random_seed,
        constraint_test=constraint_test,
    )
    return GaussianMixtureModelProposal(configs)


def create_Exponential_proposal(
    size,
    scale=1,
    random_seed=None,
    constraint_test=None,
    output_dir=None,
) -> ExponentialProposal:
    """
    Given the size of the target space, and the scale parameter, create and return an
    :py:class:`ExponentialProposal` instance/object to generate samples using and iid
    Exponential proposal Configurations/settings can be updated after inistantiation

    This function shows how to create :py:class:`ExponentialProposal` instances
        (with some or all configurations passed)
    """
    configs = ExponentialProposalConfigs(
        size=size,
        scale=scale,
        random_seed=random_seed,
        constraint_test=constraint_test,
        output_dir=output_dir,
    )
    return ExponentialProposal(configs)
