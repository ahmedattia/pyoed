# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import math
from decimal import Decimal
import numpy as np
import re
import warnings
from dataclasses import dataclass, field, replace
from typing import Iterable

# import itertools
from pyoed import utility
from pyoed.utility import (
    isnumber,
    isiterable,
)
from pyoed.configs import (
    PyOEDConfigs,
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
    remove_unknown_keys,
)
from .bernoulli import Bernoulli, BernoulliConfigs
from .combinatorics import RFunction, RFunctionConfigs


@dataclass(kw_only=True, slots=True)
class PoissonBinomialConfigs(BernoulliConfigs):
    """
    Configurations class for the :py:class:`PoissonBinomial` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`PoissonBinomial`
    provides the following fields:

    :param name: name of the distribution
    :param R_function_evaluation_method: the name of the evaluation metho of the R-function.
        See :py:class:`RFunctionConfigs` for supported evaluation methods.
    """

    name: str = "Poisson Binomial Distribution"
    R_function_evaluation_method: str = "tabulation"

@set_configurations(configurations_class=PoissonBinomialConfigs)
class PoissonBinomial(Bernoulli):
    """
    An implementation of the Poisson-binomial distribution which models the sum
    of independent (non-identical) Bernoulli trials.
    This version uses Discrete Fourier Transform to calculate probabilities
    and derivatives following ``Method 1``
    in :ref:`[1] <poisson_binomial_series_1>` which evaluates
    :math:`R(n, S)` as a series.
    For details see :ref:`[2] <probabilistic_optimization_paper_1>`.

    :param dict | PoissonBinomialConfigs | None configs: (optional) configurations for
        the model

    **References:**

    .. _poisson_binomial_series_1:

    1. Sean X. Chen, and Jun S. Liu. "Statistical applications of the Poisson-binomial and
       conditional Bernoulli distributions." Statistica Sinica (1997): 875-892.

    .. _probabilistic_optimization_paper_1:

    2. Ahmed Attia. "Probabilistic Approach to Black-Box Binary Optimization with
        Budget Constraints: Application to Sensor Placement."
        arXiv preprint arXiv:2406.05830 (2024).
    """

    def __init__(self, configs: dict | PoissonBinomialConfigs | None = None) -> None:
        super().__init__(
            configs=self.configurations_class.data_to_dataclass(configs),
        )

        self.configurations.update(
            {"parameter": utility.asarray(self.configurations.parameter).flatten()}
        )

        # Associate an R-Function evaluator to self
        self._R_FUNCTION = RFunction(
            configs={
                "method": self.configurations.R_function_evaluation_method,
            }
        )

    def validate_configurations(
        self,
        configs: dict | PoissonBinomialConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validation stage for the the passed configs.

        :param configs: configurations to validate. If a PoissonBinomialConfigs
            object is passed, validation is performed on the entire set of
            configurations. However, if a dictionary is passed, validation is performed
            only on the configurations corresponding to the keys in the dictionary.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`PoissonBinomialConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `R_function_evaluation_method`; copied from RFunction validation
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="R_function_evaluation_method",
            test=lambda v: (
                isinstance(v, str)
                and re.match(
                    r"\A(recur(rence|sion)|tab(le|ulation))\Z", v, re.IGNORECASE
                )
            ),
            message=lambda v: f"Invalid `R_function_evaluation_method`. Expected 'recursion' or 'tabulation'.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid, )

    def update_configurations(self, **kwargs):
        ## Validate and udpate configurations object
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Check if "R_function_evaluation_method" is passed
        method = kwargs.get("R_function_evaluation_method", None)
        if method is not None:
            self.R_function.update_configurations(
                method=method,
            )

    def calculate_w(
        self,
        p: Iterable[float],
        dtype: type = Decimal,
        log: bool = False,
        undefined_as_nan: bool = False,
    ) -> Iterable[Decimal | float]:
        """
        Calculate Bernoulli weights `w` from success probabilities `p`.
        The weights are defined as:

        .. math::
            w_i = \\frac{p_i}{1-p_i}

        .. note::
            The weights cannot be evaluated for any value of `p` equal to `1`.

        .. note::
            This is a wrapper around :py:meth:`RFunction.calculate_w`

        :param p: a sequence of success probabilities.
        :param dtype: Data type (must be a callable to transform
            into the input to the desired data type)
        :param log: return the logarithm of `w` if `True`
        :param undefined_as_nan: if `True` set the value of `w` to `nan`
            for any value of the probability outside the domain `[0, 1)]`

        :returns: a sequence (of the same length as `p`) with weights of type
            :py:class`decimal.Decimal`

        :raises ValueError: if any of the probabilities are no in the interval
            `[0, 1)` and `undefined_as_nan` is `False`
        """
        return self.R_function.calculate_w(
            p=p,
            dtype=dtype,
            log=log,
            undefined_as_nan=undefined_as_nan,
        )

    def pmf(self, n):
        """
        Calculate the probability (probability mass function) of the sum
        of the multivariate Bernoulli Distribution.
        This funciton models the probability mass function (PMF) of a
        Poisson-Binomial distribution/model.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: value of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """
        return math.exp(self.log_pmf(n=n))

    def log_pmf(self, n):
        """
        Calculate the **log probability** (log of the probability mass function)
        of the sum of the multivariate Bernoulli Distribution.
        This funciton models the logarithm of the probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: logarithm of the value of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        :raises: :py:class:`ValueError` if the probability mass function value is zero at `n`.
        """
        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        ones_locs = np.where(success_probability == 1)[0]
        # zeros_locs = np.where(success_probability==0)[0]

        # Eliminate one probabilities (from total and probabilities)
        valid_inds = np.setdiff1d(range(self.size), ones_locs)
        # valid_inds = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_inds]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        # TODO: Handle degeneracy
        if n < 0 or n > self.size:
            log_pmf = -math.inf
        else:
            # Evaluate weights
            w = self.calculate_w(
                p=theta,
            )

            # Evaluate log(R(n, S))
            log_R_n_S = self.R_function.evaluate(
                n=n,
                w=w,
                log=True,
                dtype=Decimal,
            )

            # log P(Z) = log(R(n, S)) - sum(log(1+w))
            scl = sum([(Decimal("1.0") + v).ln() for v in w])
            log_pmf = float(log_R_n_S - scl)

            if self.debug:
                R_n_S = self.R_function.evaluate(
                    n=n,
                    w=w,
                    log=False,
                    dtype=Decimal,
                )
                print(f"{R_n_S=}; {log_R_n_S=}; {log_pmf=}, {math.exp(log_pmf)=}")

        return log_pmf

    def grad_pmf(self, n):
        """
        Calculate the derivative/gradient of the probability (probability
        mass function) of the sum of the multivariate Bernoulli Distribution.
        This funciton models the gradient of the probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        .. note::
          This function calculates gradient of :py:meth:`sum_pmf` with respect
          to the distribution parameter, i.e., the probability of successes.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: gradient of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """

        def prod(args):
            """Product via sum"""
            return sum([v.ln() for v in args]).exp()

        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        ones_locs = np.where(success_probability == 1)[0]
        # zeros_locs = np.where(success_probability==0)[0]

        # Eliminate one probabilities (from total and probabilities)
        valid_inds = np.setdiff1d(range(self.size), ones_locs)
        # valid_inds = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_inds]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        # Initialize teh gradient
        grad = np.zeros_like(success_probability)

        # Evaluate gradient for probabilities in [0, 1)
        w = np.asarray(
            self.calculate_w(
                p=theta,
                dtype=float,
            )
        )
        R_n_S = self.R_function.evaluate(
            n=n,
            w=w,
            log=False,
        )
        grad[valid_inds] = np.prod(1 - theta) * (
            (1 + w) ** 2
            * self.R_function.gradient(
                n=n,
                w=w,
                log=False,
                dtype=float,
            )
            - float(R_n_S) * (1 + w)
        )

        # Evaluate probability for ones indexes
        for ind in ones_locs:
            _w = [w[i] for i in range(len(w)) if i != ind]
            R1 = self.R_function.evaluate(
                n=n,
                w=_w,
                log=False,
                dtype=Decimal,
            )
            R2 = self.R_function.evaluate(
                n=n+1,
                w=_w,
                log=False,
                dtype=Decimal,
            )
            gval = float(R1 - R2) * np.prod(1 - success_probability[valid_inds])
            if self.debug:
                print(
                    f"DEBUG: {success_probability=}; {ones_locs=}; {ind=}; {R1=}; {R2=}; {gval=}"
                )
            grad[ind] = gval

        if self.debug:
            # Gradient from log-gradient
            # grad = self.pmf(n=n) * self.grad_log_pmf(n=n, zero_bounds=True, )

            # Another way
            shifted_p = [Decimal("1") - Decimal(_) for _ in p]
            shifted_w = [Decimal("1") + Decimal(_) for _ in w]
            scl = prod(shifted_p)  # product(1-p) = 1/product(1+w)
            R_n_S = self.R_function.evaluate(
                n=n,
                w=w,
                log=False,
            )
            grad_R = self.R_function.gradient(
                n=n,
                w=w,
                log=False,
                dtype=Decimal,
            )
            grad_check = [
                scl * (gr * sw**2 - R_n_S * sw) for gr, sw in zip(grad_R, shifted_w)
            ]
            grad_check = np.asarray([g for g in grad_check], dtype=float)

            # Third way
            w = np.asarray([float(v) for v in w])
            grad_check_2 = np.prod(1 - p) * (
                (1 + w) ** 2
                * self.R_function.gradient(
                    n=n,
                    w=w,
                    log=False,
                    dtype=float,
                )
                - float(R_n_S) * (1 + w)
            )

            print(f"{grad=}")
            print(f"{grad_check=}")
            print(f"{grad_check_2=}")

        return grad

    def grad_log_pmf(
        self,
        n,
        zero_bounds=True,
    ):
        """
        Calculate the derivative/gradient of the log-probability (logarithm of
        the probability mass function) of the sum of the multivariate
        Bernoulli Distribution.
        This funciton models the gradient of the log-probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        .. note::
          This function calculates gradient of :py:meth:`sum_log_pmf` with respect
          to the distribution parameter, i.e., the probability of successes.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.
        :param bool zero_bounds: if `True` single-out (set to zero) any entries with
          zero or 1 probability

        :returns: gradient of the log-PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        :raises: :py:class:`ValueError` if the probability mass function value is zero at `n`
          and `zero_bounds` is `False`.
        """
        if not utility.isnumber(n) or n < 0 or int(n) != n:
            raise TypeError(
                f"Invalid Value/Type of the sum `n`. Expected non-negative integer. "
                f"Received '{n=}' of '{type(n)=}'"
            )

        # Gradient from log-gradient
        pmf = self.pmf(n=n)
        grad = self.grad_pmf(
            n=n,
        )
        if pmf == 0:
            warnings.warn(f"Gradient Log Probability is not defined for probability 0.")
            if zero_bounds:
                grad[:] = 0.0
            else:
                grad[:] = np.nan
        else:
            grad = grad / pmf

        if self.debug:
            # Retrieve copy of the success probability
            success_probability = self.success_probability.copy()
            ones_locs = np.where(success_probability == 1)[0]
            # zeros_locs = np.where(success_probability==0)[0]

            # Eliminate one probabilities (from total and probabilities)
            valid_inds = np.setdiff1d(range(self.size), ones_locs)
            # valid_inds = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
            theta = success_probability[valid_inds]

            # Update sum (given entries with 1 probabitlity)
            Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
            n -= ones_locs.size

            # Initialize the gradient (zero for degenerate entries)
            grad = np.zeros(self.size)

            if n < 0 or n > Ns:
                # sum is negative, or sum exceeds total successes are impossible cases (prob=0)
                grad[:] = np.nan

            elif n == Ns == 0:
                # Nothing is left after removing degenerate entries.
                pass

            else:
                # Evaluate weights
                w = self.calculate_w(
                    p=theta,
                )

                # Gradient of log-R with respect to w
                grad_log_R = self.R_function.gradient(
                    n=n,
                    w=w,
                    log=True,
                )
                valid_grad = utility.asarray(
                    [
                        (Decimal("1") + v) ** 2 * g - (1 + v)
                        for v, g in zip(w, grad_log_R)
                    ],  # Gradient log R wrt probabilities
                    dtype=float,
                )
                # Set valid locations
                grad[valid_inds] = valid_grad

        return grad

    def sample(
        self,
        sample_size=1,
    ):
        """
        Sample a Poisson binomial random variable according to the PMF calculated from
        all possible values. This requires calculating PMF for all values of the sum (n).

        :param int sample_size: size of the sample to generate

        :returns: a sample of `n` values (the sum of bernoulli trials) calculated based
            on success probabilities of the trials.

        :raises: :py:class:`ValueError` if the sample size is not a positive integer.
        """
        # Check arguments
        if (
            not utility.isnumber(sample_size)
            or int(sample_size) != sample_size
            or sample_size < 1
        ):
            raise TypeError(
                f"Invalid {sample_size=} of {type(sample_size)=}! "
                f"Expected positive integer"
            )

        # Generate the sample
        p = utility.asarray([self.pmf(n) for n in range(self.size + 1)], dtype=float)
        sample = self.random_number_generator.choice(
            range(self.size + 1),
            size=sample_size,
            replace=True,
            p=p,
        )
        return sample

    def expect(
        self,
        func,
    ):
        """
        Calculate the expected value of a function (func) which accepts scalars `n`
        (the bernoulli sum ) as parameter/argument.
        """
        f_vals = [func(n) for n in range(self.size + 1)]
        p = utility.asarray([self.pmf(n) for n in range(self.size + 1)], dtype=float)
        expect = sum([x * y for x, y in zip(f_vals, p)])
        return expect

    @property
    def R_function(self):
        """A handler to the underlying R-Function instance"""
        return self._R_FUNCTION


## TODO: Add creator functions
