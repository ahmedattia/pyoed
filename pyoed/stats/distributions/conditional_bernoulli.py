# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import re
from decimal import Decimal
import math
import numpy as np
import warnings

from multiprocess import (
    Pool,
    cpu_count,
    get_context,
    get_start_method,
    set_start_method,
)
# TODO: Experimental; turn this on if multiprocess.Pool is safe...
_PARALLELIZE_R_FUNCTION_EVALUATION = False

import itertools
from dataclasses import dataclass
from typing import Iterable

from pyoed import utility
from pyoed.configs import (
    validate_key,
    remove_unknown_keys,
    set_configurations,
)

from pyoed.stats.core import Distribution
from .bernoulli import (
    Bernoulli,
    BernoulliConfigs,
)
from .poisson_binomial import (
    PoissonBinomial,
    PoissonBinomialConfigs,
)


@dataclass(kw_only=True, slots=True)
class ConditionalBernoulliConfigs(BernoulliConfigs):
    """
    Configurations class for the :py:class:`ConditionalBernoulliConfigs` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`ConditionalBernoulliConfigs`
    provides the following fields:

    :param name: name of the distribution
    :param R_function_evaluation_method: the name of the evaluation metho of the R-function.
        See :py:class:`RFunctionConfigs` for supported evaluation methods.
    """
    name: str = "ConditionalBernoulli: Conditional Bernoulli probability model/distribution"
    R_function_evaluation_method: str = "tabulation"


@set_configurations(configurations_class=ConditionalBernoulliConfigs)
class ConditionalBernoulli(Bernoulli):
    """
    An implementation of the conditional Bernoulli model.
    This models a multivariate bernoulli model condioned by the sum of the number
    of active entries.

    For details see :ref:`[1] <poisson_binomial_series>` and
    :ref:`[2] <probabilistic_optimization_paper>`.

    :param dict | ConditionalBernoulliConfigs | None configs: (optional) configurations
        for the model


    **References:**

    .. _poisson_binomial_series:

    1. Sean X. Chen, and Jun S. Liu. "Statistical applications of the Poisson-binomial and
       conditional Bernoulli distributions." Statistica Sinica (1997): 875-892.

    .. _probabilistic_optimization_paper:

    2. Ahmed Attia. "Probabilistic Approach to Black-Box Binary Optimization with
        Budget Constraints: Application to Sensor Placement."
        arXiv preprint arXiv:2406.05830 (2024).
    """
    def __init__(self, configs: dict | ConditionalBernoulliConfigs | None = None) -> None:
        super().__init__(
            configs = self.configurations_class.data_to_dataclass(configs),
        )

        # Make sure the parameter is an array
        self.configurations.update({'parameter': utility.asarray(self.configurations.parameter).flatten()})

        # Create and maintain a PoissonBinomial model with the same parameter
        self._PB_MODEL = PoissonBinomial(
            configs=remove_unknown_keys(
                data=self.configurations,
                target_dataclass=PoissonBinomialConfigs,
            )
        )

    def validate_configurations(
        self,
        configs: dict | ConditionalBernoulliConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validation stage for the the passed configs.

        :param configs: configurations to validate. If a ConditionalBernoulliConfigs
            object is passed, validation is performed on the entire set of
            configurations. However, if a dictionary is passed, validation is performed
            only on the configurations corresponding to the keys in the dictionary.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ConditionalBernoulliConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `R_function_evaluation_method`; copied from RFunction validation
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="R_function_evaluation_method",
            test=lambda v: (
                isinstance(v, str) and
                re.match(r"\A(recur(rence|sion)|tab(le|ulation))\Z", v, re.IGNORECASE)
            ),
            message=lambda v: f"Invalid `R_function_evaluation_method`. Expected 'recursion' or 'tabulation'.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid, )

    def update_configurations(self, **kwargs):
        ## Validate and udpate configurations object (by calling super)
        super().update_configurations(**kwargs)

        # Extract the name before updating the CB model
        _ = kwargs.pop('name', None)  # Remove name if passed

        # Update the underlying CB Model (CB & PB share all configurations except for name)
        self.poisson_binomial_model.update_configurations(
            **remove_unknown_keys(
                data=kwargs,
                target_dataclass=PoissonBinomialConfigs,
            )
        )


    def coverage_probability(self, i, n, ):
        """
        Given success probability, calculate the inclusion probability (coverage probability)
        for index `i`, where the index starts at `0` and ranges to `size-1` where `size` is the
        dimension of the probability space, that is the size of `p`.

        .. note:
            Inclusion probability is the probability that `1` appears in a selected
            sample in the index `i`
        """
        if i not in range(0, self.size):
            raise ValueError(
                f"Invalid index {i=}. Expected integer index in: 0, 1, ..., {self.size}"
            )
        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )
        if n < 0:
            raise ValueError(
                f"Invalid Value/Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Check if n is impossible first
        if not self.poisson_binomial_model.pmf(n) > 0:
            cp = 0.0
        else:

            success_probability = self.success_probability
            if success_probability[i] in [0, 1]:
                # NOTE: Inclusion probability is the probability that `1` appears
                #   in a selected sample in the index `i
                cp = success_probability[i]

            else:
                # Eliminate zero/one probabilities
                zeros_locs = np.where(success_probability==0)[0]
                ones_locs = np.where(success_probability==1)[0]

                valid_locs = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
                theta = success_probability[valid_locs]

                Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
                n -= ones_locs.size
                if n<0 or n>Ns:
                    cp = 0.0
                else:
                    # Slicing valid indexes modifies the index i
                    mapped_i = np.where(valid_locs==i)[0]
                    if mapped_i.size !=1:
                        raise ValueError(
                            f"Unexpected Error happened."
                            f"The mapping of the index {i=} after truncating 0/1 "
                            f"probabilities unexpectedly failed!\n"
                            f"{zeros_locs=}\n"
                            f"{ones_locs=}\n"
                            f"{valid_locs=}\n"
                            f"mapped_i <- where(valid_locs==i) --> {mapped_i=}"
                        )
                    else:
                        mapped_i = mapped_i[0]

                    w = self.calculate_w(p=theta, dtype=Decimal, )
                    w_i = w[mapped_i]
                    excl_w_i = w[: mapped_i] + w[mapped_i+1: ]

                    # R-function values (at a log-scale)
                    R_i_S = self.R_function.evaluate(n=n-1, w=excl_w_i, log=True, dtype=Decimal, )
                    R_n_S = self.R_function.evaluate(n=n, w=w, log=True, dtype=Decimal, )

                    # Coverage probability (logscale then convert by exponentiation)
                    cp = w_i.ln() + R_i_S - R_n_S
                    cp = float(cp.exp())
        # Done.
        return cp
    # Alias
    inclusion_probability = coverage_probability

    def calculate_w(self, p, dtype=float, undefined_as_nan=False, ):
        """
        Calculate weights from success probabilities `p`.
        """
        return self.poisson_binomial_model.calculate_w(p=p, dtype=dtype, undefined_as_nan=undefined_as_nan, )

    def sum_pmf(self, n):
        """
        Calculate the probability (probability mass function) of the sum
        of the multivariate Bernoulli Distribution.
        This funciton models the probability mass function (PMF) of a
        Poisson-Binomial distribution/model.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: value of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """
        return self.poisson_binomial_model.pmf(n=n)

    def sum_log_pmf(self, n):
        """
        Calculate the **log probability** (log of the probability mass function)
        of the sum of the multivariate Bernoulli Distribution.
        This funciton models the logarithm of the probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: logarithm of the value of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """
        return self.poisson_binomial_model.log_pmf(n=n)

    def grad_sum_pmf(self, n):
        """
        Calculate the derivative/gradient of the probability (probability
        mass function) of the sum of the multivariate Bernoulli Distribution.
        This funciton models the gradient of the probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        .. note::
          This function calculates gradient of :py:meth:`sum_pmf` with respect
          to the distribution parameter, i.e., the probability of successes.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: gradient of the PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """
        return self.poisson_binomial_model.grad_pmf(n=n)

    def grad_sum_log_pmf(self, n, ):
        """
        Calculate the derivative/gradient of the log-probability (logarithm of
        the probability mass function) of the sum of the multivariate
        Bernoulli Distribution.
        This funciton models the gradient of the log-probability mass function (PMF)
        of a Poisson-Binomial distribution/model.

        .. note::
          This function calculates gradient of :py:meth:`sum_log_pmf` with respect
          to the distribution parameter, i.e., the probability of successes.

        :param n: non-negative integer defining the sum (number of nonzero
          entries) of a multivariate Bernoulli random variable.

        :returns: gradient of the log-PMF of the Poisson-Binomial model/distribution.

        :raises: :py:class:`TypeError` if `n` is not non-negative integer
        """
        return self.poisson_binomial_model.grad_log_pmf(n=n)

    def _verify_pmf(self, x, n):
        """
        Evaluate PMF using R-Function only
        .. note::
            This is for verification, and either this implementation or `pmf` will be used
        """
        # Reshaping and assertion
        x = utility.asarray(x, dtype=int).flatten()
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )
        if n < 0 or n > self.size:
            raise ValueError(
                f"Invalid sum/budget `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        zeros_locs = np.where(success_probability==0)[0]
        ones_locs  = np.where(success_probability==1)[0]

        # Impossible states (non-zero for zero proability or 1 for zero probability)
        if any(x[zeros_locs]!=0) or any(x[ones_locs]!=1):
            # Probability is zero; return
            return 0.0

        # Eliminate zero/one probabilities and Evaluate PMF
        valid_locs = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_locs]
        design = x[valid_locs]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        if n < 0 or n > Ns:
            pmf = 0.0
        elif n == Ns == 0:
            pmf = 1.0
        else:

            # Calculate weights
            w = self.calculate_w(p=theta, dtype=Decimal, )

            # log R(n, S)
            log_R_n_S = self.R_function.evaluate(n=n, w=w, log=True, dtype=Decimal, )

            # PMF
            pmf = float(
                (
                    sum(
                        [
                            Decimal(int(design[i])) * w[i].ln()
                            for i in range(Ns)
                        ]
                    ) - log_R_n_S
                ).exp()
            )

        return pmf

    def pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate the value of the probability mass function (PMF) of
        a Conditional Bernoulli distribution, evaluated at given binary
        state/realization `x`, and with parameters defined by the underlying probability of
        success.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.

        :returns: value of the PMF of the CB model (probabiltiy of :py:math`x`
            conditioned by the sum)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Calculate probability
        p = np.exp(self.log_pmf(x=x, n=n, batch_as_column=batch_as_column, ))

        # Check round-off errors
        # eps = 1e-10  # Consider making make this a class-level variable if needed
        # if -eps <= p < 0.0: p = 0.0
        # if 1 < p < 1+eps: p = 1.0
        return p

    def log_pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate the log of the probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state
        or a batch of states (random variable realization) `x`, and
        with registered parameters theta.

        .. note::
            This method is just a wrapper that chooses either
            :py:`meth:`_log_pmf` or :py:meth:`_batch_log_pmf` based on whether
            `x` is 1d or 2d numpy array, respectively.

        :param x: scalar, or 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the log-pmf is evaluated for each column
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: log-pmf (or batch of log-pmf values) of the probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        if x.ndim == 1:
            return self._log_pmf(x=x, n=n)
        elif x.ndim == 2:
            return self._batch_log_pmf(x=x, n=n, batch_as_column=batch_as_column, )
        else:
            raise TypeError(
                f"x must be 1d or 2d numpy array; received array-like of shape {x.shape}"
            )

    def _batch_log_pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate the value of log-PMF (probability mass function) of
        a Conditional Bernoulli distribution

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.

        :returns: logarithm of the value of the PMF of the CB model (probabiltiy of :py:math`x`
            conditioned by the sum)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer or it exceeds size of the CB model
        """
        ## Validate arguments
        # Validate number of successes (n)
        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        if n < 0:
            raise ValueError(
                f"Invalid sum/budget `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Validate the random variable (or batches of random variables)
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        # Check x size/shape and infer batch mode
        if x.ndim == 2:
            if batch_as_column:
                size, batch_size = x.shape
            else:
                batch_size, size = x.shape
        else:
            raise TypeError(
                f"Received {x.shape=}; expected "
                f"2d array with number of `rows` or `columns` (based on "
                f" `batch_as_column`) eaual to {self.size}"
            )

        if size != self.size:
            raise TypeError(
                f"{batch_as_column=}; x has shape {x.shape}; expected 1d/2d array (or bach) with "
                f"random variable (row/column) size of {self.size=}"
            )


        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        zeros_locs = np.where(success_probability==0)[0]
        ones_locs = np.where(success_probability==1)[0]

        # Eliminate zero/one probabilities
        valid_locs = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_locs]
        w = np.asarray(self.calculate_w(p=theta, dtype=float, ))
        log_w = np.log(w)

        # Number of available successes (after taking out definite 1,)
        n -= ones_locs.size
        log_R_n_S = self.R_function.evaluate(n=n, w=w, log=True, dtype=float, )

        ## At this point, all ones/zeros went away, and some non-degenerate entries exist
        # Initiate log probability array and loop over it
        log_prob = np.empty(batch_size)
        for b_ind in range(batch_size):
            # Update sum (n) by reducing any degenerate entries with value 1 (corresponding to ones_locs)
            if batch_as_column:
                design = x[valid_locs, b_ind]
                invalid_degenerate = any(x[zeros_locs, b_ind] != 0) or any(x[ones_locs, b_ind] != 1)
            else:
                design = x[b_ind, valid_locs]
                invalid_degenerate = any(x[b_ind, zeros_locs] != 0) or any(x[b_ind, ones_locs] != 1)

            if invalid_degenerate:
                val = - np.inf
            else:
                Ns = design.size  # Ns = self.size - ones_locs.size - zeros_locs.size
                # Check possibilities
                if n < 0 or n > Ns or np.count_nonzero(design) != n:
                    # Impossible design
                    # first two conditions: sum is negative, or sum exceeds total successes are impossible cases (prob=0)
                    # last conditions: (Number of Nonzeros != remaining budget)
                    val = - np.inf

                else:
                    # Calculate log-prob recursively
                    # Calculate log-probability
                    val = np.sum(design * log_w ) - log_R_n_S

            # Update
            log_prob[b_ind] = val
        return log_prob

    def _log_pmf(self, x, n):
        """
        Calculate the value of log-PMF (probability mass function) of
        a Conditional Bernoulli distribution at one (or batch of) random variable
        realization(x) `x`.

        .. note::
            While this method can be called by the user, it is ment for internal use.
            The user should call :py:meth:`log_pmf` instead, and based on the shape of `x`,
            logarithm of the pmf will be calculated.

        :param x: scalar, or 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the log-pmf is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer or it exceeds size of the CB model
        """
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        if n < 0:
            raise ValueError(
                f"Invalid Value/Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        zeros_locs = np.where(success_probability==0)[0]
        ones_locs = np.where(success_probability==1)[0]

        # Check if any of the ones/zeros locations do not match the passed variable;
        # If there is a mismatch -> probability = 0.
        if any(x[ones_locs]!=1) or any(x[zeros_locs]!=0):
            # log_prob = -np.inf
            return -np.inf

        # Check if n exceeds the # of nonzero probabilities
        if n > self.size-zeros_locs.size:
            # log_prob = -np.inf
            return -np.inf

        # Eliminate zero/one probabilities
        valid_locs = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_locs]
        design = x[valid_locs]

        # Update sum (n) by reducing any degenerate entries with value 1 (corresponding to ones_locs)
        Ns = design.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        # At this point, all ones/zeros went away, and some non-degenerate entries exist

        # Check possibilities
        if n < 0 or n > Ns:
            # sum is negative, or sum exceeds total successes are impossible cases (prob=0)
            log_prob = -np.inf

        elif np.count_nonzero(design) != n:
            # This state is Impossible (Number of Nonzeros != remaining budget)
            log_prob = -np.inf

        else:
            # Calculate log-prob recursively
            w = self.calculate_w(p=theta, dtype=float, )
            log_R_n_S = self.R_function.evaluate(n=n, w=w, log=True, dtype=float, )

            # Calculate log-probability
            log_prob = np.sum(design * np.log(w)) - log_R_n_S

        return log_prob

    def grad_pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate the gradient of the probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state
        or a batch of states (random variable realization) `x`, and with parameters theta.

        .. note::
            This method is just a wrapper that chooses either
            :py:`meth:`_grad_pmf` or :py:meth:`_batch_grad_pmf` based on whether
            `x` is 1d or 2d numpy array, respectively.

        :param x: scalar, or 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: gradient (or batch of gradients) of the probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        if x.ndim == 1:
            return self._grad_pmf(x=x, n=n)
        elif x.ndim == 2:
            return self._batch_grad_pmf(x=x, n=n, batch_as_column=batch_as_column, )
        else:
            raise TypeError(
                f"x must be 1d or 2d numpy array; received array-like of shape {x.shape}"
            )

    def _grad_pmf(self, x, n, ):
        """
        Calculate the gradient of the probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state x,
        and with parameters theta.

        .. note::
            This version accumulates the R-values (combinatorics) needed upfornt
            in a lazy fashion to enabble batch evaluation of the gradient.

        .. note::
            While this method can be called by the user, it is ment for internal use.
            The user should call :py:meth:`grad_pmf` instead, and based on the shape of `x`,
            gradient will be calculated.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.

        :returns: gradient of the probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer

        """
        ## Validate arguments

        # Validate number of successes (n)
        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Validate the random variable (x); type, shape, etc.
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        elif x.dtype != int:
            x = x.astype(int)
        if x.ndim != 1 or x.size !=self.size:
            raise TypeError(
                f"Received {x.shape=}; expected 1d array of size {self.size=}"
            )

        # if np.count_nonzero(x) != n or n < 0 or n > self.size:
        if np.count_nonzero(x) != n:
            # Infeasible state/variable. Probability is zero, and gradient is zero
            if self.verbose:
                warnings.warn(
                    f"The passed random variable realization is outside "
                    f"the feasible/supported domain.\n"
                    f"The probability is zero; and the gradient is thus set to zero."
                )
            return np.zeros(self.size)

        # Initialized the gradient
        grad = np.zeros(self.size, dtype=float)

        ## Now, fill the entries of the gradient(s) for degenerate and non-degenerate prob
        # Retrieve copy of the success probability
        zeros_locs = np.where(self.success_probability==0)[0]       # theta == 0
        ones_locs  = np.where(self.success_probability==1)[0]       # theta == 1
        valid_locs = np.setdiff1d(
            range(self.size),
            np.union1d(ones_locs, zeros_locs),
        )  # 0 < theta < 1

        # Indexes of the binary probabilities mismatching with passed state
        zeros_flags_mismatch = x[zeros_locs] != 0
        ones_flags_mismatch = x[ones_locs] != 1

        # Valid Success probabilities (theta) and corresponding weights
        theta = self.success_probability[valid_locs]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        # Weights for non-degenerate probability
        w = np.asarray(self.calculate_w(p=theta, dtype=float, ))  # Weights for valid indexes
        log_w = np.log(w)  # log of Weights for valid indexes
        log_1pw = np.log(1+w)

        ## Evaluate all R values needed at once (lazy fashion) if needed.
        # R-values needed for any combination of probability and design value
        # log(R(n-|I|, V)), I= ones_locs, V=weights[valid_locs]
        log_R_n_mI = self.R_function.evaluate(
            n=n,
            w=w,
            log=True,
            dtype=float,
        )

        # R-values for non-degenerate indexes (0 < p < 1)
        if Ns > 0:
            if _PARALLELIZE_R_FUNCTION_EVALUATION:
                ## Parallel version (with current contex)
                with Pool(processes=cpu_count()-1) as pool:
                    log_R_n_mI_m1_excl = pool.map(
                        lambda i: self.R_function.evaluate(
                            n=n-1,
                            w=np.concatenate([w[: i], w[i+1: ]]),
                            log=True,
                            dtype=float,
                        ),
                        range(Ns),
                    )

                with Pool(processes=cpu_count()-1) as pool:
                    log_R_n_mI_excl = pool.map(
                        lambda i: self.R_function.evaluate(
                            n=n,
                            w=np.concatenate([w[: i], w[i+1: ]]),
                            log=True,
                            dtype=float,
                        ),
                        range(Ns),
                    )

            else:
                ## Serial version
                # log_R_n_mI_m1_excl <=> log(R(n-|I|-1, V\{i))
                log_R_n_mI_m1_excl = [
                    self.R_function.evaluate(
                        n=n-1,
                        w=np.concatenate([w[: i], w[i+1: ]]),
                        log=True,
                        dtype=float,
                    )
                    for i in range(Ns)
                ]

                # log_R_n_mI_excl <=> log(R(n-|I|, V\{i))
                log_R_n_mI_excl = [
                    self.R_function.evaluate(
                        n=n,
                        w=np.concatenate([w[: i], w[i+1: ]]),
                        log=True,
                        dtype=float,
                    )
                    for i in range(Ns)
                ]

        else:
            log_R_n_mI_m1_excl = log_R_n_mI_excl = None

        # R-values for degenerate indexes (p=0)
        if zeros_locs.size > 0:
            # log(R(n-|I|-1, V)), I= ones_locs, V=weights[valid_locs]
            log_R_n_mI_m1 = self.R_function.evaluate(
                n=n-1,
                w=w,
                log=True,
                dtype=float,
            )
        else:
            log_R_n_mI_m1 = None

        # R-values for degenerate indexes (p=1)
        if ones_locs.size > 0:
            # log(R(n-|I|+1, V)), I= ones_locs, V=weights[valid_locs]
            log_R_n_mI_p1 = self.R_function.evaluate(
                n=n+1,
                w=w,
                log=True,
                dtype=float,
            )
        else:
            log_R_n_mI_p1 = None

        ###
        ## The following part can be safely used for any realization of x;
        ##   thus it can safely work for batch mode.
        ###

        # Extract portion of x corresponding to non-degenerate entries (0<p<1)
        design = x[valid_locs]

        ## Now, the heavy work; update gradient for non-degenerate probabilities
        if Ns > 0:
            if any(zeros_flags_mismatch) or any(ones_flags_mismatch):
                # If binary probabilities mismatch, probability is zero
                pass
                # grad[valid_locs] = 0.0
            else:
                # Loop over valid indexes
                for i in range(Ns):

                    # Exclude the ith entry
                    # w_i = w[i]
                    # w_excl_i = np.concatenate([w[: i], w[i+1: ]])
                    log_w_excl_i = np.concatenate([log_w[: i], log_w[i+1: ]])
                    x_excl_i = np.concatenate([design[: i], design[i+1: ]])

                    # Product of w^x (log-scale)
                    w_p_x = np.sum(x_excl_i * log_w_excl_i)

                    if self.debug:
                        print(
                            f"DEBUG: {i=}; {w_p_x=}; {w[i]=}; {log_w_excl_i=} \n"
                            f"DEBUG: {w_p_x=}; {log_R_n_mI=};"
                        )

                    # Gradient based on the value of x[i]
                    if design[i] == 0:
                        gval = - np.exp(
                            w_p_x - 2 * log_R_n_mI + 2 * log_1pw[i] + log_R_n_mI_m1_excl[i]
                        )
                        if self.debug:print(f" {log_R_n_mI_m1_excl[i]=}")

                    else:
                        gval = np.exp(
                            w_p_x - 2 * log_R_n_mI + 2 * log_1pw[i] + log_R_n_mI_excl[i]
                        )
                        if self.debug: print(f" {log_R_n_mI_excl[i]=}")

                    if self.debug:
                        print(
                            f"DEBUG: Updating index {valid_locs[i]=} of the gradient "
                            f"with {gval=}"
                        )
                    if np.isnan(gval) or np.isinf(gval):
                        continue
                    else:
                        # Update gradient at this index
                        grad[valid_locs[i]] = gval


        ## Compute gradient for degenerate probabilities
        if zeros_locs.size > 0 or ones_locs.size > 0:

            # log(Product of w^x) = sum (log(w^x)) = sum(x log(w))
            log_w_pow_x = np.sum(design * log_w)

            ## Loop over degenerate probability with probability (theta=0)
            if zeros_locs.size > 0:
                if any(ones_flags_mismatch):
                    pass
                    # grad[zeros_locs] = 0.0

                else:

                    # Evaluate the gradient for zero-valued xi
                    zero_xi_grad = - np.exp(
                        log_R_n_mI_m1 - 2 * log_R_n_mI + log_w_pow_x
                    )
                    one_xi_grad = np.exp(
                        - log_R_n_mI + log_w_pow_x
                    )

                    # Fill gradient entries
                    gval = [
                        (
                            zero_xi_grad if xi==0 else one_xi_grad
                        ) for xi in x[zeros_locs]
                    ]
                    if np.any(np.isnan(gval)) or np.any(np.isinf(gval)):
                        pass
                    else:
                        grad[zeros_locs] = gval

            ## Loop over degenerate probability with probability (theta=1)
            if ones_locs.size > 0:
                if any(zeros_flags_mismatch):
                    grad[ones_locs] = 0.0

                else:

                    # Evaluate the gradient for zero-valued xi
                    zero_xi_grad = - np.exp(
                        - log_R_n_mI + log_w_pow_x
                    )

                    one_xi_grad = np.exp(
                        log_R_n_mI_p1 - 2 * log_R_n_mI + log_w_pow_x
                    )

                    gval = [
                        (
                            zero_xi_grad if xi==0 else one_xi_grad
                        ) for xi in x[ones_locs]
                    ]
                    # Fill gradient entries
                    if np.any(np.isnan(gval)) or np.any(np.isinf(gval)):
                        pass
                    else:
                        grad[ones_locs] = gval

        return grad

    def _batch_grad_pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate (in batch mode) the gradients of the probability mass function
        (PMF) of a conditional Bernoulli distribution, evaluated at
        given batch of binary states x, and with parameters theta.

        .. note::
            While this method can be called by the user, it is ment for internal use.
            The user should call :py:meth:`grad_pmf` instead, and based on the shape of `x`,
            gradient will be calculated.

        :param x: 2D numpy array of binary values (0/10) or bytes
            Each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: gradient (batch of gradients of same shape as `x`) of the probabiltiy
            of the CB model.

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        ## Validate arguments

        # Validate number of successes (n)
        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Validate the random variable (x); type, shape, etc.
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        elif x.dtype != int:
            x = x.astype(int)

        # Assertion on shape of the batch
        if x.ndim == 2:
            if batch_as_column:
                size, batch_size = x.shape
            else:
                batch_size, size = x.shape
        else:
            raise TypeError(
                f"Received {x.shape=}; expected "
                f"2d array with number of `rows` or `columns` (based on "
                f" `batch_as_column`) eaual to {self.size}"
            )

        if size != self.size:
            raise TypeError(
                f"x has shape {x.shape}; expected 1d/2d array (or bach) with "
                f"random variable ""(row/column) size of {self.size}"
            )

        # Initialized the gradient
        grad = np.zeros_like(x, dtype=float)

        ## Now, fill the entries of the gradient(s) for degenerate and non-degenerate prob
        # Retrieve copy of the success probability
        zeros_locs = np.where(self.success_probability==0)[0]       # theta == 0
        ones_locs  = np.where(self.success_probability==1)[0]       # theta == 1
        valid_locs = np.setdiff1d(
            range(self.size),
            np.union1d(ones_locs, zeros_locs),
        )  # 0 < theta < 1

        # Valid Success probabilities (theta) and corresponding weights
        theta = self.success_probability[valid_locs]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        total_n = n
        n -= ones_locs.size

        # Weights for non-degenerate probability
        w = np.asarray(self.calculate_w(p=theta, dtype=float, ))  # Weights for valid indexes
        log_w = np.log(w)  # log of Weights for valid indexes
        log_1pw = np.log(1+w)

        ## Evaluate all R values needed at once (lazy fashion) if needed.
        # R-values needed for any combination of probability and design value
        # log(R(n-|I|, V)), I= ones_locs, V=weights[valid_locs]
        log_R_n_mI = self.R_function.evaluate(
            n=n,
            w=w,
            log=True,
            dtype=float,
        )

        # R-values for non-degenerate indexes (0 < p < 1)
        if Ns > 0:
            ## The parallelizm only if multiprocess doesn't miss other processes (e.g., PETSc)
            if _PARALLELIZE_R_FUNCTION_EVALUATION:
                ## Parallel version (with current contex)
                with Pool(processes=cpu_count()-1) as pool:
                    log_R_n_mI_m1_excl = pool.map(
                        lambda i: self.R_function.evaluate(
                            n=n-1,
                            w=np.concatenate([w[: i], w[i+1: ]]),
                            log=True,
                            dtype=float,
                        ),
                        range(Ns),
                    )

                with Pool(processes=cpu_count()-1) as pool:
                    log_R_n_mI_excl = pool.map(
                        lambda i: self.R_function.evaluate(
                            n=n,
                            w=np.concatenate([w[: i], w[i+1: ]]),
                            log=True,
                            dtype=float,
                        ),
                        range(Ns),
                    )

            else:
                ## Serial version
                # log_R_n_mI_m1_excl <=> log(R(n-|I|-1, V\{i))
                log_R_n_mI_m1_excl = [
                    self.R_function.evaluate(
                        n=n-1,
                        w=np.concatenate([w[: i], w[i+1: ]]),
                        log=True,
                        dtype=float,
                    )
                    for i in range(Ns)
                ]

                # log_R_n_mI_excl <=> log(R(n-|I|, V\{i))
                log_R_n_mI_excl = [
                    self.R_function.evaluate(
                        n=n,
                        w=np.concatenate([w[: i], w[i+1: ]]),
                        log=True,
                        dtype=float,
                    )
                    for i in range(Ns)
                ]

        else:
            log_R_n_mI_m1_excl = log_R_n_mI_excl = None

        # R-values for degenerate indexes (p=0)
        if zeros_locs.size > 0:
            # log(R(n-|I|-1, V)), I= ones_locs, V=weights[valid_locs]
            log_R_n_mI_m1 = self.R_function.evaluate(
                n=n-1,
                w=w,
                log=True,
                dtype=float,
            )
        else:
            log_R_n_mI_m1 = None

        # R-values for degenerate indexes (p=1)
        if ones_locs.size > 0:
            # log(R(n-|I|+1, V)), I= ones_locs, V=weights[valid_locs]
            log_R_n_mI_p1 = self.R_function.evaluate(
                n=n+1,
                w=w,
                log=True,
                dtype=float,
            )
        else:
            log_R_n_mI_p1 = None

        ## Loop over batches
        for b_ind in range(batch_size):

            # Indexes of the binary probabilities mismatching with passed state
            trgt_x = x[:, b_ind] if batch_as_column else x[b_ind, :]
            zeros_flags_mismatch = trgt_x[zeros_locs] != 0
            ones_flags_mismatch = trgt_x[ones_locs] != 1

            if np.count_nonzero(trgt_x) != total_n:
                # Infeasible state/variable. Probability is zero, and gradient is zero
                if self.verbose:
                    warnings.warn(
                        f"Passed random variable realization(s) is outside "
                        f"the feasible/supported domain.\n"
                        f"The probability is zero; and the gradient is thus set to zero."
                    )
                continue

            # Extract portion of x corresponding to non-degenerate entries (0<p<1)
            design = trgt_x[valid_locs]

            ## Now, the heavy work; update gradient for non-degenerate probabilities
            if Ns > 0:
                if any(zeros_flags_mismatch) or any(ones_flags_mismatch):
                    # If binary probabilities mismatch, probability is zero
                    if False:
                        # No need now since grad is initialized to 0 already
                        if batch_as_column:
                            grad[valid_locs, b_ind] = 0.0
                        else:
                            grad[b_ind, valid_locs] = 0.0

                else:
                    # Loop over valid indexes
                    for i in range(Ns):

                        # Exclude the ith entry
                        # w_i = w[i]
                        # w_excl_i = np.concatenate([w[: i], w[i+1: ]])
                        log_w_excl_i = np.concatenate([log_w[: i], log_w[i+1: ]])
                        x_excl_i = np.concatenate([design[: i], design[i+1: ]])

                        # Product of w^x (log-scale)
                        w_p_x = np.sum(x_excl_i * log_w_excl_i)

                        if self.debug:
                            print(
                                f"DEBUG: {i=}; {w_p_x=}; {w[i]=}; {log_w_excl_i=} \n"
                                f"DEBUG: {w_p_x=}; {log_R_n_mI=};"
                            )

                        # Gradient based on the value of x[i]
                        if design[i] == 0:
                            gval = - np.exp(
                                w_p_x - 2 * log_R_n_mI + 2 * log_1pw[i] + log_R_n_mI_m1_excl[i]
                            )
                            if self.debug:print(f" {log_R_n_mI_m1_excl[i]=}")

                        else:
                            gval = np.exp(
                                w_p_x - 2 * log_R_n_mI + 2 * log_1pw[i] + log_R_n_mI_excl[i]
                            )
                            if self.debug: print(f" {log_R_n_mI_excl[i]=}")

                        if self.debug:
                            print(
                                f"DEBUG: Updating index {valid_locs[i]=} of the gradient "
                                f"with {gval=}"
                            )
                        if np.isnan(gval) or np.isinf(gval):
                            gval = 0.0
                        # Update gradient at this index
                        if batch_as_column:
                            grad[valid_locs[i], b_ind] = gval
                        else:
                            grad[b_ind, valid_locs[i]] = gval


            ## Compute gradient for degenerate probabilities
            if zeros_locs.size > 0 or ones_locs.size > 0:

                # log(Product of w^x) = sum (log(w^x)) = sum(x log(w))
                log_w_pow_x = np.sum(design * log_w)

                ## Loop over degenerate probability with probability (theta=0)
                if zeros_locs.size > 0:
                    if any(ones_flags_mismatch):
                        if False:
                            # No need now since grad is initialized to 0 already
                            if batch_as_column:
                                grad[zeros_locs, b_ind] = 0.0
                            else:
                                grad[b_ind, zeros_locs] = 0.0

                    else:

                        # Evaluate the gradient for zero-valued xi
                        zero_xi_grad = - np.exp(
                            log_R_n_mI_m1 - 2 * log_R_n_mI + log_w_pow_x
                        )
                        one_xi_grad = np.exp(
                            - log_R_n_mI + log_w_pow_x
                        )
                        gval = [
                            (
                                zero_xi_grad if xi==0 else one_xi_grad
                            ) for xi in trgt_x[zeros_locs]
                        ]

                        if any(np.isnan(gval)) or any(np.isinf(gval)):
                            gval = 0.0
                        # Fill gradient entries
                        if batch_as_column:
                            grad[zeros_locs, b_ind] = gval
                        else:
                            grad[b_ind, zeros_locs] = gval


                ## Loop over degenerate probability with probability (theta=1)
                if ones_locs.size > 0:

                    if any(zeros_flags_mismatch):
                        if False:
                            # No need now since grad is initialized to 0 already
                            if batch_as_column:
                                grad[ones_locs, b_ind] = 0.0
                            else:
                                grad[b_ind, ones_locs] = 0.0

                    else:

                        # Evaluate the gradient for zero-valued xi
                        zero_xi_grad = - np.exp(
                            - log_R_n_mI + log_w_pow_x
                        )

                        one_xi_grad = np.exp(
                            log_R_n_mI_p1 - 2 * log_R_n_mI + log_w_pow_x
                        )

                        gval = [
                            (
                                zero_xi_grad if xi==0 else one_xi_grad
                            ) for xi in trgt_x[ones_locs]
                        ]
                        if any(np.isnan(gval)) or any(np.isinf(gval)):
                            gval = 0.0
                        # Fill gradient entries
                        if batch_as_column:
                            grad[ones_locs, b_ind] = gval
                        else:
                            grad[b_ind, ones_locs] = gval


        return grad

    def grad_log_pmf(self, x, n, batch_as_column=True, ):
        """
        Calculate the gradient of the log-probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state
        or a batch of states (random variable realization) `x`, and with parameters theta.

        .. note::

            Given the assumption that the Bernoulli RVs modeled are uncorrelated, the
            gradient of log-probabilities is same as partial derivatives of
            corresponding derivatives of log-prob of each entry; thus, whether `joint` is
            `True` or `False` the result is the same.

        :param x: scalar, or 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: gradient (or batch of gradients) of the log-probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)

        if x.ndim == 1:
            pmf = self.pmf(x=x, n=n, )
            if pmf == 0:
                grad_log_pmf = np.zeros(self.size)
            else:
                grad_log_pmf = self.grad_pmf(x=x, n=n) / pmf

        elif x.ndim == 2:
            pmf = self.pmf(x=x, n=n, batch_as_column=batch_as_column)
            grad_log_pmf = self.grad_pmf(
                x=x,
                n=n,
                batch_as_column=batch_as_column
            )  # will be scaled...

            # Get batch size and random variable size
            if batch_as_column:
                size, batch_size = grad_log_pmf.shape
            else:
                batch_size, size = grad_log_pmf.shape

            for b_ind in range(batch_size):
                if pmf[b_ind] == 0:
                    if batch_as_column:
                        grad_log_pmf[:, b_ind] = 0.0
                    else:
                        grad_log_pmf[b_ind, :] = 0.0

                else:
                    if batch_as_column:
                        grad_log_pmf[:, b_ind] /= pmf[b_ind]
                    else:
                        grad_log_pmf[b_ind, :] /= pmf[b_ind]

        else:
            raise TypeError(
                f"Received {x.shape=}; expected "
                f"2d array with number of `rows` or `columns` (based on "
                f" `batch_as_column`) eaual to {self.size}"
            )

        return grad_log_pmf

    def _old_grad_pmf(self, x, n):
        """
        Calculate the gradient of the probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state x,
        and with parameters theta.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.

        :returns: gradient of the probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        p = self.pmf(x=x, n=n)
        if p in [0, 1]:
            grad = np.zeros(self.size)
        else:
            grad = p * self.grad_log_pmf(x=x, n=n)
        return grad

    def _old_grad_log_pmf(self, x, n, zero_bounds=True, ):
        """
        Calculate the gradient of log-PMF (probability mass function), with respect to
        distribution parameters (success probability)

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.
        :param bool zero_bounds: if `True` single-out (set to zero) any entries with
          zero or 1 probability

        :returns: the gradient of the log probability

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer

        .. note::

            Given the assumption that the Bernoulli RVs modeled are uncorrelated, the
            gradient of log-probabilities is same as partial derivatives of
            corresponding derivatives of log-prob of each entry; thus, whether `joint` is
            `True` or `False` the result is the same.
        """
        # Reshaping and assertion
        x = utility.asarray(x, dtype=bool).flatten().astype(int)
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        if n < 0:
            raise ValueError(
                f"Invalid Value/Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        # Retrieve copy of the success probability
        success_probability = self.success_probability.copy()
        zeros_locs = np.where(success_probability==0)[0]
        ones_locs = np.where(success_probability==1)[0]

        # Check if any of the ones/zeros locations do not match the passed variable;
        # If there is a mismatch -> probability = 0.
        if any(x[ones_locs]!=1) or any(x[zeros_locs]!=0):
            # Initialize the gradient; set to nan as it is undefined since probability is zero
            grad = np.empty(self.size)
            if zero_bounds:
                grad[:] = 0.0
            else:
                grad[:] = np.nan
            return grad

        # Eliminate zero/one probabilities
        valid_locs = np.setdiff1d(range(self.size), np.union1d(ones_locs, zeros_locs))
        theta = success_probability[valid_locs]
        design = x[valid_locs]

        # Update sum (given entries with 1 probabitlity)
        Ns = theta.size  # Ns = self.size - ones_locs.size - zeros_locs.size
        n -= ones_locs.size

        # Initialize the gradient (zero for degenerate entries)
        grad = np.zeros(self.size)

        if n < 0 or n > Ns:
            # sum is negative, or sum exceeds total successes are impossible cases (prob=0)
            grad[:] = np.nan

        elif n == Ns == 0:
            # Nothing is left after removing degenerate entries.
            pass

        elif np.count_nonzero(design) != n:
            # This state is Impossible (Number of Nonzeros != remaining budget);
            # probability is zer, and gradient of log-probability is undefined
            grad[:] = np.nan

        else:

            # Calculate log-prob recursively
            w = np.asarray(self.calculate_w(p=theta, dtype=float, ))

            # Grad log-R
            grad_log_R = self.R_function.gradient(n=n, w=w, log=True, dtype=float, )

            # Gradient (at valid indexes (all but 1 probs))
            scl = 1.0 + w
            grad[valid_locs]= scl * (design/theta - scl*grad_log_R)

        # Adjust bounds if necessary
        if zero_bounds:
            grad[np.isnan(grad)] = 0.0

        return grad

    def _build_q_matrix(self, z):
        """Construct a matrix of probabilities q(i, j) defining P(sum(x_{m=j}^{N})=i)"""
        theta = self.success_probability.copy()

        q = np.zeros((z+1, self.size))
        for j in range(self.size):
            q[0, j] = np.prod(1-self.success_probability[j: ])
        q[1, -1] = theta[-1]
        for i in range(1, z+1):
            for j in range(self.size-1)[::-1]:
                if i==1 and j == self.size-1:
                    continue
                elif i > self.size-j:
                    continue
                else:
                    q[i, j] = theta[j] * q[i-1, j+1] + (1-theta[j]) * q[i, j+1]
        return q

    def sample(self,
               n,
               sample_size=1,
               antithetic=False,
               dtype=bool,
    ):
        """
        Sample a Condional Bernoulli random variable (1d or multivariate) according
        to probability of success `p`, that is ::math:`p:=(P(x=1))`, of the underlying
        Bernoulli random variable.
        If antithetic is True, sample_size must be even

        :param n: non-negative integer defining the sum to condition on.
        :param int sample_size: size of the sample to generate
        :param bool antithetic:
        :param type dtype: data type of the returned array
        :param random_seed: ``None|int`` dictates the random seed to be used to initialize
            the underlying random number generator

        :returns: bernoulli_sample: array of shape ``sample_size x n`` where ``n`` is
            the size of `p`

        :raises: :py:class:`ValueError` if `n` is out of range of possible values or invalid type
            or the sample size is not a positive integer.

        .. note::
            If p is scalar or iterable of length 1, this will be 1d array of
            size=sample_size. Otherwise, if p is multivariate, this will be 2d
            array with each row representing one sample.
        """
        if not utility.isnumber(sample_size) or int(sample_size) != sample_size or sample_size<1:
            raise TypeError(
                f"Invalid {sample_size=} of {type(sample_size)=}! "
                f"Expected positive integer"
            )
        if not utility.isnumber(n) or int(n) != n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received '{n}' of type '{type(n)}'"
            )

        theta = self.success_probability

        # If n is not possible; sampling cannot be carried out
        min_active = np.where(theta==1)[0].size
        max_active = np.count_nonzero(theta)
        if n>self.size or n < min_active or n > max_active:
            raise ValueError(
                f"Invalid Value/Type of the sum `n`. Expected integer within [0, {self.size}]; "
                f"Received {n=} of {type(n)=}; "
                f"Minumum number of active entries is {min_active=}; "
                f"Maximum number of active entries is {max_active=}; "
            )
        if n == 0 and np.all(theta==1):
            raise ValueError(
                f"It is impossible to achieve sum zero if all success probabilities are equal to 1"
            )
        if n == self.size and np.any(theta==0):
            raise ValueError(
                f"It is impossible to achieve sum {n} if any success probability is 0"
            )

        # Initialize the samples array
        cb_sample = np.empty((sample_size, self.size), dtype=dtype)
        if n == 0:
            cb_sample[...] = 0

        elif n == self.size:
            cb_sample[...] = 1

        else:
            q = self._build_q_matrix(n)

            # Loop over all entries and assign binary values based on comulative probability
            for j in range(1, self.size):

                # Check the number of acquired active entries
                cumsum = np.sum(cb_sample[:, :j-1], axis=1).astype(int)

                active_prob = np.asarray([(q[n-1-l, j] if n-1-l>=0 else 0.0) * theta[j-1] for l in cumsum])
                active_prob /= np.asarray([q[n-l, j-1] for l in cumsum])

                cb_sample[:, j-1]= self.random_number_generator.binomial(
                    n=1,
                    p=active_prob,
                )
            # Final column just remainin from summing each row entries from above
            cb_sample[:, -1] = n - np.sum(cb_sample[:, :-1], axis=1).astype(int)

        return cb_sample

    def expect(self, func, n, objective_value_tracker=None, ):
        """
        Calculate the expected value of a function (func) which accepts w as parameter
        """
        assert callable(func), "func must be a callable"
        theta = self.success_probability
        if self.size > 30:
            warnings.warn(
                f"****\n"
                f"You'll be waiting for long time for enumerating too many possibilities!\n"
                f"The dimension is {self.size}; resulting in {2**self.size} possible instances\n"
                f"****"
            )

        # Initialize the expectation value and check the tracker
        e_val = 0.0
        if objective_value_tracker is None: objective_value_tracker = {}

        # Loop over all expectation terms
        for x in itertools.product([0, 1], repeat=theta.size):
            # Skip nonfeasible realizations
            if np.count_nonzero(x) != n:
                continue

            # Retrieve the index corresponding to this binary variable
            k = utility.index_from_binary_state(x)

            # Evaluate the value of the objective (try retrieving from objective_value_tracker)
            try:
                v = objective_value_tracker[k]
            except KeyError:
                v = func(x)
                # NOTE: we can discard updating the tracker; the only benefit is in-place update
                objective_value_tracker.update({k: v})

            # Evaluate the joint probability mass function (PMF) value
            p = self.pmf(x, n=n, )

            # Update expectation
            e_val += v * p

        return e_val

    @property
    def poisson_binomial_model(self, ):
        """A handler to the underlying Poisson Binomial model instance"""
        return self._PB_MODEL

    @property
    def R_function(self, ):
        """A handler to the underlying R-function instance"""
        return self.poisson_binomial_model.R_function


@dataclass(kw_only=True, slots=True)
class GeneralizedConditionalBernoulliConfigs(ConditionalBernoulliConfigs):
    """
    Configurations class for the :py:class:`GeneralizedConditionalBernoulliConfigs` abstract base class.
    This class inherits functionality from :py:class:`ConditionalBernoulliConfigs` in addition
    to the following attributes/keys.

    :param budgets: `None` or an iterable (of ints) with allowed/feasible budgets.
        Any budget must be between 0, and the size of the binary variable (inclusive).
        If `None`, no budget-constraint is asserted; this is equivalent to setting budget
        to include all budgets between 0, and the size of the binary variable (inclusive).
    """

    budgets: None | Iterable[int] = None
    name: str = "GeneralizedConditionalBernoulli: Conditional Bernoulli model with multiple budgets"


@set_configurations(configurations_class=GeneralizedConditionalBernoulliConfigs)
class GeneralizedConditionalBernoulli(ConditionalBernoulli):
    """
    A Generalization of the :py:class:`ConditionalBernoulli` model
    where the sum is allowed to be a set of values rather than just one value.
    """
    def __init__(self, configs: dict | GeneralizedConditionalBernoulliConfigs | None = None) -> None:
        super().__init__(
            configs = self.configurations_class.data_to_dataclass(configs),
        )

        # Make sure the parameter is an array
        self.configurations.update({'parameter': utility.asarray(self.configurations.parameter).flatten()})

        # Create and maintain a ConditionalBernoulli model with the same parameter
        self._CB_MODEL = ConditionalBernoulli(
            configs=remove_unknown_keys(
                data=self.configurations,
                target_dataclass=ConditionalBernoulliConfigs,
            )
        )

        # Standardize and register budgets (local attributes are re-added here for clarity)
        self._BUDGETS = self._BUDGETS_PROBABILITIES = None  # Initialize (updated in `register_budgets`)
        budgets, budgets_probabilities = self.register_budgets(self.configurations.budgets)
        self._BUDGETS = self.configurations.budgets = budgets
        self._BUDGETS_PROBABILITIES = budgets_probabilities


    def update_configurations(self, **kwargs):
        ## Validate and udpate configurations object
        super().update_configurations(**kwargs)

        # Extract the name before updating the CB model
        _ = kwargs.pop('name', None)  # Remove name if passed

        # Update the underlying PB and CB Models (CB & PB share all configurations except for name)
        self.poisson_binomial_model.update_configurations(
            **remove_unknown_keys(
                data=kwargs,
                target_dataclass=PoissonBinomialConfigs,
            )
        )
        self.conditional_bernoulli_model.update_configurations(
            **remove_unknown_keys(
                data=kwargs,
                target_dataclass=ConditionalBernoulliConfigs,
            )
        )

        # If `parameter` is passed, update budget sizes probabilities for existing budgets
        if 'parameter' in kwargs:
            self.register_budgets(self.configurations.budgets)

        # If `budgets` is passed, update budget sizes & probabilities for existing parameter
        if 'budgets' in kwargs:
            self.register_budgets(kwargs['budgets'])


    def register_budgets(self, budgets):
        """
        Set the budget (the sum of the Bernoulli random variable to condition on)
        The budget could be a number (integer) or set of numbers.
        The probability of each budget/size is recalculated.
        In the former case, the distribution is identical to the parent class.
        In the latter, the probability is calculated by conditioning on the union
        of all budgets.

        :param int|iterable(int) budgets: either an integer or an iterable
          e.g., list of integers, defining acceptable budgets (sum of the Bernoulli
          random variable).

        :raises: :py:class:`TypeError` if the type of `budgets` is not acceptable.
        """
        def __raise_unaccepted_budgets(msg=""):
            err_msg = f"""UNACCEPTABLE BUDGET\n******
            \rUnsupported/Unacceptable {budgets=} of {type(budgets)=}!
            \rExpected an integer or an iterable contatining a set of integers.
            """
            if msg:
                err_msg += f"\n{msg}\n******"
            raise TypeError(err_msg)

        # The maximum number of possible budgets (active entries)
        # Variable size
        max_budget = size = self.size

        # Check/validate passed budget(s)
        if budgets is None:
            warnings.warn(
                f"Employing this object ({self} without budget constraints) is expensive! "
                f"This is mainly because it still assumes constrained distribution with "
                f"all possible budgets.\n"
                f"For NO budgets, use `ConditionalBernoulli` instead."
            )
            budgets = np.arange(0, self.size+1)

        elif utility.isnumber(budgets) and int(budgets)==budgets:
            # Number
            if budgets < 0 or budgets > max_budget:
                msg =f"Found budgets out of acceptable range [0, {max_budget}]"
                msg += f"\n{budgets=}\n"
                __raise_unaccepted_budgets(msg=msg)
            else:
                budgets = utility.asarray(budgets, dtype=int)

        elif utility.isiterable(budgets):
            # Iterable
            try:
                _budgets = utility.asarray(budgets, dtype=int)
            except Exception as err:
                __raise_unaccepted_budgets(msg=f"Unexpected {err=} of type{type(err)=}")

            if np.ndim(_budgets.squeeze()) > 1:
                __raise_unaccepted_budgets(msg="one dimensional iterable is only acceptable here")
            else:
                _budgets = _budgets.flatten()
                if any(_budgets!=budgets):
                    __raise_unaccepted_budgets(msg="Integer budgets are only acceptable!")
                else:
                    # Sort and remove any duplicates
                    budgets = np.unique(_budgets)

            if any(budgets<0) or any(budgets>max_budget):
                msg =f"Found budgets out of acceptable range [0, {max_budget}]"
                msg += f"\n{budgets=}\n"
                __raise_unaccepted_budgets(msg=msg)

        else:
            __raise_unaccepted_budgets()

        # Calculate budget probabilities (PoissonBinomial model)
        budget_probabilities = np.array([self.sum_pmf(n) for n in budgets])

        # Set the budget sizes and calculate their probabilities once
        self._BUDGETS = self.configurations.budgets = budgets
        self._BUDGETS_PROBABILITIES = budget_probabilities

        if self.debug:
            print("DEGUG: Updated Budgets: ", self.success_probability, self._BUDGETS, self._BUDGETS_PROBABILITIES)

        return budgets, budget_probabilities

    def check_registered_budgets(self):
        """
        Check/validate registerd budgets and their probabilities.

        :returns: the registerd budgets/sizes and the corresponding probabilities.

        :raises: :py:class:`TypeError` if no valid budget is registered
        """
        budgets = self.budgets
        budgets_probs = self.budgets_probabilities
        if not isinstance(budgets, np.ndarray):
            raise TypeError(
                f"Expected registerd budgets to be a numpy array; found {type(budgets)=}!\n"
                f"It seems like it was manually updated by the user!\n"
                f"Use the method `register_budgets` to update budgets instead!"
            )
        if len(budgets)==0:
            raise TypeError(
                f"No valid budgtes have been registerd yet!\n"
                f"Use the method `register_budgets` to register budgets first!"
            )
        if len(budgets_probs) != len(budgets):
            # If there is incosistency, reclaculate probabilities (by registering) and recurse
            self.register_budgets(budgets)
            return self.check_registered_budgets()

        # Clear; return the budgets
        return budgets, budgets_probs

    def coverage_probability(self, i, ):
        """
        Calculate the inclusion probability (coverage probability)
        for index `i`, where the index starts at `0` and ranges to `size-1` where `size` is the
        dimension of the probability space, that is the size of `p`.
        This is conditioned by the registered budget of course.

        .. note::
            Inclusion probability is the probability that `1` appears in a selected sample in the index `i`
        """
        # TODO: To wdevelop the math as conditional probability. This should be straightforward!"
        # TODO: Given inclusion probability, gradients should related to it too!
        # raise NotImplementedError(
        #     "TODO: To wdevelop the math as conditional probability. This should be straightforward!"
        # )
        # The ATTMPT BELOW SEEM WRONG!!! Doesn't add up to n or anthing expected!
        # TODO: NEED TO THINK CLARER AND DERIVE THE MATH!
        budgets, budgets_probs = self.check_registered_budgets()

        tot_prob = sum(budgets_probs)
        cp = 0.0
        if tot_prob > 0:
            for budget, budget_prob in zip(budgets, budgets_probs):
                cp += self.conditional_bernoulli_model.coverage_probability(i=i, n=budget) * budget_prob
            cp /= tot_prob
        return cp

    def pmf(self, x, batch_as_column=True, ):
        """
        Calculate the value of the probability mass function (PMF) of
        a Conditional Bernoulli distribution, evaluated at given binary
        state/realization `x`, and with parameters defined by the underlying probability of
        success.
        The variable `x` is conditioned by the registered budget.

        :param x: 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: value of the PMF of the CB model (probabiltiy of :py:math`x`
            conditioned by the sum)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Registered budgets, associated probabilities, and total probability
        budgets, budgets_probs = self.check_registered_budgets()
        tot_prob = sum(budgets_probs)

        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        if x.ndim == 1:
            size = x.size
        elif x.ndim == 2:
            if batch_as_column:
                size, batch_size = x.shape
            else:
                batch_size, size = x.shape
        else:
            raise TypeError(
                f"Received {x.shape=}; expected "
                f"2d array with number of `rows` or `columns` (based on "
                f" `batch_as_column`) eaual to {self.size}"
            )
        if size != self.size:
            raise TypeError(
                f"x has shape {x.shape}; expected 1d/2d array (or bach) with "
                f"random variable ""(row/column) size of {self.size}"
            )

        # If some budgets are probable, evaluate generalized pmf
        if tot_prob > 0:
            p = 0.0
            for budget, budget_prob in zip(budgets, budgets_probs):
                p += self.conditional_bernoulli_model.pmf(
                    x=x,
                    n=budget,
                    batch_as_column=batch_as_column,
                ) * budget_prob
            p /= tot_prob

        elif x.ndim == 1:
            p = 0.0
        else:
            p = np.zeros(batch_size)

        return p

    def log_pmf(self, x, batch_as_column=True, ):
        """
        log-PMF conditioned by the registerd budgets.
        This returns the logarithm of :py:meth:`pmf`.
        """
        #  NOTE: zero-probability should not happen in our optimization work, but need to decide what to do
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)
        #
        pmf = self.pmf(x=x, batch_as_column=batch_as_column, )
        #
        if isinstance(pmf, np.ndarray):
            log_pmf = np.zeros_like(pmf, dtype=float, )
            log_pmf[pmf==0] = -np.inf
            log_pmf[pmf!=0] = np.log(pmf[pmf!=0])

        else:
            if pmf == 0:
                log_pmf = -np.inf
            else:
                log_pmf = np.log(pmf)
        return log_pmf

    def _batch_grad_log_pmf(self, x, batch_as_column=True, zero_bounds=True, ):
        """
        Calculate (in batch mode) the gradients of the probability mass function
        (PMF) of a generalized conditional Bernoulli distribution, evaluated at
        given batch of binary states x, and with parameters theta.

        .. note::
            While this method can be called by the user, it is ment for internal use.
            The user should call :py:meth:`grad_pmf` instead, and based on the shape of `x`,
            gradient will be calculated.

        :param x: 2D numpy array of binary values (0/10) or bytes
            Each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: gradient (batch of gradients of same shape as `x`) of the probabiltiy
            of the CB model.

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        ## Validate arguments
        # Validate the random variable (or batches of random variables)
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        # Check x size/shape and infer batch mode
        if x.ndim == 2:
            if batch_as_column:
                size, batch_size = x.shape
            else:
                batch_size, size = x.shape
        else:
            raise TypeError(
                f"Received {x.shape=}; expected "
                f"2d array with number of `rows` or `columns` (based on "
                f" `batch_as_column`) eaual to {self.size}"
            )

        if size != self.size:
            raise TypeError(
                f"x has shape {x.shape}; expected 1d/2d array (or bach) with "
                f"random variable ""(row/column) size of {self.size}"
            )

        # Get registered budgets and associated probabilities
        budgets, budgets_probs = self.check_registered_budgets()

        # sum(P(n)) : denominator or second term (scalar)
        sum_P_n = np.sum(budgets_probs)

        if sum_P_n == 0:
            if zero_bounds:
                grad_log_pmf = np.zeros_like(x, dtype=float)
            else:
                grad_log_pmf = np.empty_like(x, dtype=float)
                grad_log_pmf[...] = np.nan

        else:
            # Loop over all budgets, and calculate each term
            sum_P_x_n_grad_P_n = 0.0  # Term to accumulate P(x/n) grad P(n)
            sum_P_n_grad_P_x_n = 0.0  # Term to accumulate P(n) grad P(x/n)
            sum_grad_P_n = 0.0  # Term to accumulate grad P(n)
            sum_P_x_n_P_n = 0.0  # Term to accumulate P(x/n) P(n)

            # Loop over each (n, P(n)) pair to update the terms above
            for n, pn in zip(budgets, budgets_probs):

                # Calculat the components of the update
                pmf = self.conditional_bernoulli_model.pmf(
                    x=x,
                    n=n,
                    batch_as_column=batch_as_column,
                )  # P(x/n)  1d array
                grad_pmf = self.conditional_bernoulli_model.grad_pmf(
                    x=x,
                    n=n,
                    batch_as_column=batch_as_column,
                )   # grad P(x/n) 2darray
                grad_sum_pmf = self.conditional_bernoulli_model.grad_sum_pmf(n)  # grad P(n) 1d array

                # Replicate grad_sum_pmf
                grad_sum_pmf = grad_sum_pmf.reshape(
                    (grad_sum_pmf.size, 1)
                ).repeat(
                    pmf.size,
                    axis=1,
                )
                if not batch_as_column:
                    grad_sum_pmf = grad_sum_pmf.T

                ## Update sum_P_x_n_grad_P_n (2d array)
                if batch_as_column:
                    # Muliply columns of grad_sum_pmf by each entry in pmf
                    sum_P_x_n_grad_P_n += grad_sum_pmf * pmf
                else:
                    # Muliply rows of grad_sum_pmf by each entry in pmf
                    sum_P_x_n_grad_P_n += grad_sum_pmf * pmf.reshape((pmf.size, 1))


                ## Update sum_P_n_grad_P_x_n  (2d array)
                sum_P_n_grad_P_x_n += pn * grad_pmf

                ## Update sum_P_n_P_x_n (stays as 1d array)
                sum_P_x_n_P_n += pn * pmf

                ## Update sum_grad_P_n
                sum_grad_P_n += grad_sum_pmf

            # Convert sum_P_x_n_P_n into matrix by replication
            sum_P_x_n_P_n = sum_P_x_n_P_n.reshape((sum_P_x_n_P_n.size, 1)).repeat(self.size, axis=1)
            if batch_as_column:
                sum_P_x_n_P_n = sum_P_x_n_P_n.T

            # Add terms to calculate gradient
            grad_log_pmf = (
                (sum_P_x_n_grad_P_n + sum_P_n_grad_P_x_n) / sum_P_x_n_P_n
            ) - (
                sum_grad_P_n  / sum_P_n
            )

        fix_val = 0.0 if zero_bounds else np.nan
        for b_ind in range(batch_size):
            if batch_as_column:
                if np.any(np.isinf(grad_log_pmf[:, b_ind])) or np.any(np.isnan(grad_log_pmf[:, b_ind])):
                    grad_log_pmf[:, b_ind] = fix_val
            else:
                if np.any(np.isinf(grad_log_pmf[b_ind, :])) or np.any(np.isnan(grad_log_pmf[b_ind, :])):
                    grad_log_pmf[b_ind, :] = fix_val

        return grad_log_pmf

    def _grad_log_pmf(self, x, zero_bounds=True, ):
        """
        Calculate the gradient of log-PMF (probability mass function), with respect to
        distribution parameters (success probability)

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.
        :param bool zero_bounds: if `True` single-out (set to zero) any entries with
          zero or 1 probability

        :returns: the gradient of the log probability

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer

        .. note::
           Given the assumption that the Bernoulli RVs modeled are uncorrelated, the
           gradient of log-probabilities is same as partial derivatives of
           corresponding derivatives of log-prob of each entry; thus, whether `joint` is
           `True` or `False` the result is the same.
        """
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        # Check x size/shape and infer batch mode
        if x.ndim != 1 or x.size != self.size:
            raise TypeError(
                f"x has shape {x.shape}; expected 1d/2d array (or bach) with "
                f"random variable ""(row/column) size of {self.size}"
            )

        budgets, budgets_probs = self.check_registered_budgets()

        # Check if any of the budges has non-zero probability; if not, probability is zero
        budgets_probs_sum = sum(budgets_probs)

        if budgets_probs_sum == 0:
            if zero_bounds:
                grad_log_pmf = np.zeros(self.size)
            else:
                grad_log_pmf = np.empty(self.size)
                grad_log_pmf[:] = np.nan

        else:
            # Initialize the two parts of the gradient formula
            term_1 = np.zeros(self.size)
            term_2 = np.zeros(self.size)

            # Normalization factors for the two terms
            norm_1 = 0.0
            norm_2 = budgets_probs_sum

            # Iterate over each n (along with its probability) to accumulate
            # the two terms of the gradient
            for n, pn in zip(budgets, budgets_probs):
                pmf = self.conditional_bernoulli_model.pmf(x=x, n=n)
                grad_pmf = self.conditional_bernoulli_model.grad_pmf(x=x, n=n)
                grad_sum_pmf = self.conditional_bernoulli_model.grad_sum_pmf(n)

                norm_1 += pn * pmf
                term_1 += pmf * grad_sum_pmf + pn * grad_pmf
                term_2 += grad_sum_pmf

            if norm_1 > 0 and norm_2 > 0:
                # Scale and add two terms of the gradient
                grad_log_pmf = term_1 / norm_1 - term_2 / norm_2

            else:
                # Gradient is undefined as either or both norms (probs) are zero
                # This means that
                grad_log_pmf = np.empty(self.size)
                grad_log_pmf[:] = np.nan

        # Adjust bounds if necessary
        if zero_bounds:
            grad_log_pmf[np.isnan(grad_log_pmf)] = 0.0

        return grad_log_pmf

    def grad_log_pmf(self, x, batch_as_column=True, zero_bounds=True, ):
        """
        Calculate the gradient of the log-probability mass function (PMF) of
        a generalized conditional Bernoulli distribution, evaluated at given binary state
        or a batch of states (random variable realization) `x`, and with parameters theta.

        .. note::
            This method is just a wrapper that chooses either
            :py:`meth:`_grad_log_pmf` or :py:meth:`_batch_grad_log_pmf` based on whether
            `x` is 1d or 2d numpy array, respectively.

        :param x: scalar, or 1D or 2D numpy array of binary values (0/10) or bytes.
            If `x` is 2D array, each **COLUMN** is regarded as one instance of the random
            variable, and the gradient is evaluated for each column.
            If you want rows to be regarded as random variable, switch `batch_as_column`
            to `False `
        :param n: non-negative integer defining the sum to condition on.
        :param batch_as_column: Only used if `x` is 2d array.
            if `Ture`, and `x` is two dimensional, each column is
            regarded as instance of the random variable (default), otherwise, each row
            is taken as a random variable.

        :returns: gradient (or batch of gradients) of the log-probabiltiy of the GCB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        # Reshaping and assertion
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)

        if x.ndim == 1:
            return self._grad_log_pmf(x=x, zero_bounds=zero_bounds, )
        elif x.ndim == 2:
            return self._batch_grad_log_pmf(x=x, batch_as_column=batch_as_column, zero_bounds=zero_bounds, )
        else:
            raise TypeError(
                f"x must be 1d or 2d numpy array; received array-like of shape {x.shape}"
            )

    def grad_pmf(self, x, batch_as_column=True, ):
        """
        Calculate the gradient of the probability mass function (PMF) of
        a conditional Bernoulli distribution, evaluated at given binary state x,
        and with parameters theta.
        The variable `x` is conditioned by the registered budget.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param n: non-negative integer defining the sum to condition on.

        :returns: gradient of the probabiltiy of the CB model

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
            and/or `n` is not non-negative integer
        """
        if not isinstance(x, np.ndarray):
            x = utility.asarray(x, dtype=bool).astype(int)
        if x.dtype != int:
            x = x.astype(int)
        budgets, budgets_probs = self.check_registered_budgets()

        if sum(budgets_probs) == 0:
            grad = np.zeros_like(x)

        else:
            grad_log_pmf = self.grad_log_pmf(x=x, batch_as_column=batch_as_column, zero_bounds=True, )

            if grad_log_pmf.ndim == 1:
                grad = self.pmf(x=x) * grad_log_pmf

            elif grad_log_pmf.ndim == 2:
                pmf = self.pmf(x=x, batch_as_column=batch_as_column)  # 1d array

                if batch_as_column:
                    grad = grad_log_pmf * pmf
                else:
                    grad = grad_log_pmf * pmf.reshape((pmf.size, 1))

            else:
                raise TypeError(
                    f"This should never be reached; "
                    f"Please report as a bug!"
                )
        return grad


    def sample(self,
               sample_size=1,
               antithetic=False,
               dtype=bool,
    ):
        """
        Sample a Condional Bernoulli random variable (1d or multivariate) according
        to probability of success `p`, that is ::math:`p:=(P(x=1))`, of the underlying
        Bernoulli random variable.
        If antithetic is True, sample_size must be even.
        The random variable is conditioned by the registered budgets.

        .. note::
          This is similar to :py:meth:`ConditionalBernoulli.sample` except that we replace
          `n` with the registered budgets.
          To sample, we first sample sizes based on proabilities of each budget, and then
          sample the CB model conditioned by each sample size.

        :param n: non-negative integer defining the sum to condition on.
        :param int sample_size: size of the sample to generate
        :param bool antithetic:
        :param type dtype: data type of the returned array
        :param random_seed: ``None|int`` dictates the random seed to be used to initialize
            the underlying random number generator

        :returns: bernoulli_sample: array of shape ``sample_size x n`` where ``n`` is
            the size of `p`

        :raises: :py:class:`ValueError` if the `sample_size` is not a positive integer or if
          no proper budget registered with nonzero probabilities.

        .. note::
            If p is scalar or iterable of length 1, this will be 1d array of
            size=sample_size. Otherwise, if p is multivariate, this will be 2d
            array with each row representing one sample.
        """
        # Check budgets
        budgets, budgets_probs = self.check_registered_budgets()

        if sum(budgets_probs) == 0:
            raise ValueError(
                f"Can't sample conditioned by the registered budget; "
                f"All budgets are associated with zero probabilities\n"
                f"{budgets=}\n{budgets_probs=}\n"
                f"{self.success_probability=}"
            )

        # Check arguments
        if not (
            utility.isnumber(sample_size) or
            int(sample_size) != sample_size or
            sample_size<1
        ):
            raise TypeError(
                f"Invalid {sample_size=} of {type(sample_size)=}! "
                f"Expected positive integer"
            )

        sample_sizes = self.random_number_generator.choice(
            budgets,
            size=sample_size,
            replace=True,
            p=budgets_probs/sum(budgets_probs),
        )
        sample_sizes = [np.where(sample_sizes==n)[0].size for n in budgets]

        # Initialize sample
        cb_sample = np.empty((sample_size, self.size), dtype=dtype, )
        added = 0
        for n, n_samples in zip(budgets, sample_sizes):
            if n_samples == 0:
                # Proceed if no samples are to be collected here.
                continue

            sample = self.conditional_bernoulli_model.sample(
                n=n,
                sample_size=n_samples,
                antithetic=antithetic,
                dtype=dtype,
            )
            # Update and increment
            cb_sample[added: added+n_samples, :] = sample
            added += n_samples

        return cb_sample

    def expect(self, func, objective_value_tracker=None, ):
        """
        Calculate the expected value of a function (func) which accepts w as parameter
        """
        # Check budgets
        budgets, budgets_probs = self.check_registered_budgets()

        # Check arguments
        assert callable(func), "func must be a callable"
        theta = self.success_probability
        if self.size > 30:
            warnings.warn(
                "****\n"
                "You'll be waiting for long time for enumerating too many possibilities!\n"
                "The dimension is {self.size}; resulting in {2**{self.size} possible instances}\n"
                "****"
            )

        # Initialize the expectation value and check the tracker
        e_val = 0.0
        if objective_value_tracker is None: objective_value_tracker = {}

        # Loop over all expectation terms
        for x in itertools.product([0, 1], repeat=self.size):
            # Skip nonfeasible realizations
            if np.count_nonzero(x) not in budgets:
                continue

            # Retrieve the index corresponding to this binary variable
            k = utility.index_from_binary_state(x)

            # Evaluate the value of the objective (try retrieving from objective_value_tracker)
            try:
                v = objective_value_tracker[k]
            except KeyError:
                v = func(x)
                # NOTE: we can discard updating the tracker; the only benefit is in-place update
                objective_value_tracker.update({k: v})

            # Evaluate the joint probability mass function (PMF) value
            p = self.pmf(x, )

            # Update expectation
            e_val += v * p

        return e_val


    @property
    def conditional_bernoulli_model(self):
        """
        Return a reference to the underlying Conditional Bernoulli Model
        """
        return self._CB_MODEL

    @property
    def budgets(self):
        """Copy of the budget sizes list"""
        return self._BUDGETS.copy()
    @budgets.setter
    def budgets(self, val):
        """Update the budget sizes and recalculate associated probabilities"""
        self.register_budgets(val)

    @property
    def budgets_probabilities(self, ):
        """Copy of the budget sizes probabilities"""
        return self._BUDGETS_PROBABILITIES.copy()


## TODO: Add creator functions

