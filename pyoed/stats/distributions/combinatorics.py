# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module provides access to useful combinatorial functions and tools.
"""
import re
import math
from decimal import Decimal
import numpy as np
from dataclasses import dataclass, field
from typing import Iterable

from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    validate_key,
    set_configurations,
)
from pyoed import utility



@dataclass(kw_only=True, slots=True)
class RFunctionConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`RFunction` abstract base class.  This class
    inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`RFunction`
    provides the following fields:

    :param name: name of the class. Default is `'R-Function'`.
    :param method: the method to use for calculating the R-function and its derivative.
        Only two values are accepted:

        - `'recursion'`: The first method with closed form-recurrence relation
          is used.
        - `'tabulation'`: (default) The second method where values of the R-function and
          derivatives are tabulated row-by-row.

    """
    name: str = "R-Function"
    method: str = "tabulation"

@set_configurations(configurations_class=RFunctionConfigs)
class RFunction(PyOEDObject):
    """
    Implementations of the R-function along with its derivatives.
    The code here provides **two methods** to calculating the value of
    R-function :math:`R(n, S)` for a given set of weights :math:`(w_1, w_2, w_{N})`,
    where :math:`S:=\\{1, 2, \\ldots, N\\}`.

    :param dict | RFunctionConfigs | None configs: (optional) configurations for
        the R-function. Configurations are ported from :py:class:`RFunctionConfigs`.
    """
    def __init__(self, configs: dict | RFunctionConfigs | None = None) -> None:
        super().__init__(
            configs=self.configurations_class.data_to_dataclass(configs),
        )

    def validate_configurations(
        self,
        configs: dict | RFunctionConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation model **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        .. note::
            The purposed of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`RFunctionConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`ToyLinearTimeIndependentConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `name`: string indicating name of the object
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="name",
            test=lambda v: isinstance(v, str),
            message=lambda v: f"Invalid `name`. Expected string. Recived `{v}` of type {type(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `method`: string indicating recurrence or tabulation (with re variations for some flexibility)
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="method",
            test=lambda v: (
                isinstance(v, str) and
                re.match(r"\A(recur(rence|sion)|tab(le|ulation))\Z", v, re.IGNORECASE)
            ),
            message=lambda v: f"Invalid `method`. Expected 'recursion' or 'tabulation'. Recived `{v}` of type {type(v)}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid, )


    def calculate_w(
        self,
        p: Iterable[float],
        dtype: type = Decimal,
        log: bool = False,
        undefined_as_nan: bool = False,
    ) -> Iterable[Decimal | float]:
        """
        Calculate Bernoulli weights `w` from success probabilities `p`.
        The weights are defined as:

        .. math::
            w_i = \\frac{p_i}{1-p_i}

        .. note::
            The weights cannot be evaluated for any value of `p` equal to `1`.

        :param p: a sequence of success probabilities.
        :param dtype: Data type (must be a callable to transform
            into the input to the desired data type)
        :param log: return the logarithm of `w` if `True`
        :param undefined_as_nan: if `True` set the value of `w` to `nan`
            for any value of the probability outside the domain `[0, 1)]`

        :returns: a sequence (of the same length as `p`) with weights of type
            :py:class`decimal.Decimal`

        :raises ValueError: if any of the probabilities are no in the interval
            `[0, 1)` and `undefined_as_nan` is `False`
        """
        theta = [Decimal(v) for v in p]
        if not undefined_as_nan and any([t<0 for t in theta]) or any([t>=1 for t in theta]):
            raise ValueError(
                f"The R function is undefined for invalid or degenerate probabilities!\n"
                f"Some probabilities passed in `p` are found ouside the interval [0 1)!\n"
                f"Received {p=}"
            )

        # Calculate weights
        if log:
            w = [t.ln() - (Decimal(1.0)-t).ln() if 0<=t<1 else math.nan  for t in theta]
        else:
            w = [t / (Decimal(1.0)-t) if 0<=t<1 else math.nan for t in theta]

        # Check/Correct return data type
        if dtype is not Decimal:
            w = [dtype(v) for v in w]

        return w

    def evaluate(self, n, w, log=False, dtype=Decimal, ):
        """
        Evaluate the value of the R-function :math:`R(n, S)` where
        :math:`S=\\{1, 2, \\ldots, N\\}`, with :math:`N` being the length/size of
        the weights vector :math:`w`.

        .. note::
            This method is a wrapper that calls either
            :py:meth:`evaluate_by_recursion` or
            :py:meth:`evaluate_by_tabulation` based on the registered evaluation method.

        :param int n: an integer which defines the first argument of the R-function.
        :param iterable w: the vector of weights derived from Bernoulli trials parameters..
        :param bool log: if `True` return the logarithm (natural logarithm)  `log(R(n, S))`,
            otherwise return the value of `R(n, S)`

        :returns: the value(s) of `R(n, S)` either as a scalar (if `n` is not `None`)
            or a sequence if `n` is None.
        :rtype: :py:class:`decimal.Decimal` or a list of `decimal.Decimal` values
        """
        method = self.method
        if re.match(r"\Arecur(rence|sion)\Z", method, re.IGNORECASE):
            result = self.evaluate_by_recursion(n=n, w=w, log=log, dtype=dtype, )
        elif re.match(r"\Atab(le|ulation)\Z", method, re.IGNORECASE):
            result = self.evaluate_by_tabulation(n=n, w=w, log=log, dtype=dtype, )
        else:
            raise ValueError(
                f"This shouldn't happen! "
                f"The {method=} is not recognized; it might have been changed "
                f"manually by the user by directly modifying the configurations dictionary!"
            )
        return result

    def evaluate_by_recursion(self, n, w, log=False, dtype=Decimal, enforce_non_negative_R=False, ):
        """
        Evaluate the value of the R-function :math:`R(n, S)` where
        :math:`S=\\{1, 2, \\ldots, N\\}`, with :math:`N` being the length/size of
        the weights vector :math:`w`.
        Here, the **recursion** method is used.

        Calculate the R(n, S) function value, where :math:`S:=\\{1, 2, \\ldots,N \\}`
        where :math:`N` is the length/size of `w`, and `w` is the weights vector calculated
        from the probability of success of a multivariate Bernoulli ditribution
        :math:`\\theta` as :math:`w=\\frac{\\theta}{1-\\theta}`.
        The R-funciton is given by:

        .. math::
            R(z, S) := \\sum_{B\\in A\\,; |B|=k} \\prod_{i\\in B} w_i \\,;\\,
            w_i := \\frac{\\theta_i }{1-\\theta_i}

        This is calculated using the recurrence relation

        .. math::
            R(z, S) = \\frac{1}{z} \\sum_{i=1}^{z} (-1)^{i+1} T(i, S) R(z-i, S)\\,;\\,
            T(i, S) := \\sum_{j\\in S} w_j^{i}

        .. warning::
            This method is numerically unstable, especially for larg values of Ns!

        .. note::
            The R function returns very high numbers for large dimensions
            (nature of combinatorics), and thus one shouldn't use numpy arrays to store
            such values.
            We have to use native Python numbers (and store things in lists).

        :param int n: an integer which defines the first argument of the R-function.
        :param iterable w: the vector of weights derived from Bernoulli trials parameters..
        :param bool log: if `True` return the logarithm (natural logarithm)  `log(R(n, S))`,
            otherwise return the value of `R(n, S)`

        :returns: the value(s) of `R(n, S)` either as a scalar (if `n` is not `None`)
            or a sequence if `n` is None.
        :rtype: :py:class:`decimal.Decimal` or a list of `decimal.Decimal` values

        :raises: :py:class:`TypeError` `n` is not integer or `w` is of unrecognized type
        """
        if not utility.isnumber(n) or int(n)!=n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer"
                f"Received '{n}' of type '{type(n)}'"
            )
        else:
            n = int(n)

        # Convert probabilities into Decimals
        if utility.isnumber(w):
            w = [Decimal(w)]
        elif utility.isiterable(w):
            w = [Decimal(t) for t in w]
        else:
            raise TypeError(
                f"Invalid Type of the weights `n`. Expected iterable "
                f"Received '{w=}' of '{type(w)=}'"
            )

        # Based on n, evalute R-function (on a log scale)
        Ns = len(w)

        if n < 0 or n > Ns:
            R_n_S = Decimal('-inf') if log else Decimal('0.0')

        elif n == 0:
            R_n_S = Decimal('1.0').ln() if log else Decimal(1.0)

        else:

            ## Recursive append of R (log-scale) list

            # Initialize R[0] = 1, R[1] = sum(w)
            R = [
                Decimal(1.0),
            ]

            ## Recursive append of R list

            ## Allocate lists (require very large numbers); can't use numpy arrays
            series_length = n if n is not None else Ns
            # S:
            #---
            S = [Decimal((-1)**(i+1)) for i in range(1, series_length+1)]

            # T:
            #---
            T = [ sum([v**Decimal(i) for v in w]) for i in range(1, series_length+1) ]

            # R is calculated recursively
            #----------------------------
            for i in range(1, series_length+1):
                # New entry
                R_i = sum( [s*t*r for s,t,r in zip(S[: i], T[: i], R[::-1]) ] ) / Decimal(i)

                if enforce_non_negative_R and R_i < 0:
                    msg = f"\n***\nNumerical Instabiliy is hits\n***\n"
                    msg += f"Encountered Non-Positive value of R(n, N)!\n"
                    msg += f"Weights: {w=}"
                    msg += f"\nR({n}, {Ns}) -> {R_i} which is wrong!\n{'-'*25}\n"
                    for j in range(i, max(0, i-5), -1):
                        try:
                            msg += f"{j=}: S(j)={S[j]}; T(j)={T[j]}; R(i-j)={R[-1-j]}; \n"
                        except(IndexError):
                            break
                    raise ValueError(msg)

                # Update R list
                R.append(R_i)

            # Extract the last R-function value
            R_n_S = R[-1]
            if log:
                if R_n_S == 0:
                    R_n_S = Decimal('-inf')
                elif R_n_S > 0:
                    R_n_S = R_n_S.ln()
                else:
                    raise ValueError(
                        f"The R-function value R({n=}, S)={R_n_S} is negative. \n"
                        f"The weights: {w=}. \n"
                        f"Cannot evaluate the logarithm!"
                    )

        # Check/Correct return data type
        if dtype is not Decimal:
            R_n_S = dtype(R_n_S)

        return R_n_S

    def evaluate_by_tabulation(self, n, w, log=False, dtype=Decimal, ):
        """
        Evaluate the value of the R-function :math:`R(n, S)` (or its logarithm) where
        :math:`S=\\{1, 2, \\ldots, N\\}`, with :math:`N` being the length/size of
        the weights vector :math:`w`.
        Here, the **recursion** method is used.

        Calculate the R(n, S) function value, where :math:`S:=\\{1, 2, \\ldots,N \\}`
        where :math:`N` is the length/size of `w`, and `w` is the weights vector calculated
        from the probability of success of a multivariate Bernoulli ditribution
        :math:`\\theta` as :math:`w=\\frac{\\theta}{1-\\theta}`.
        The R-funciton is given by:

        .. math::
            R(z, S) := \\sum_{B\\in A\\,; |B|=k} \\prod_{i\\in B} w_i \\,;\\,
            w_i := \\frac{\\theta_i }{1-\\theta_i}

        This is calculated using a tabulated relationship with c(i, j) entry of the table
        calculated by the following recurrence relation

        .. math::
            c(i, j) = \\frac{1}{z} \\sum_{i=1}^{z} (-1)^{i+1} T(i, S) R(z-i, S)\\,;\\,
            T(i, S) := \\sum_{j\\in S} w_j^{i}  \\, i<=j \\,, i=0, 1, \\ldots, N

        and :math:`R(z, S)` is the value in the cell `c(z, )` and `N` is the cardinality of
        S:=\\{1, 2,\\ldots, N\\}`.

        .. note::
            The R function returns very high numbers for large dimensions
            (nature of combinatorics), and thus one can't use numpy arrays to store
            such values.
            We have to use native Python numbers (and store things in lists).

        .. warning::
            This method is numerically unstable, especially for larg values of Ns!

        .. note::
            The R function returns very high numbers for large dimensions
            (nature of combinatorics), and thus one shouldn't use numpy arrays to store
            such values.
            We have to use native Python numbers (and store things in lists).

        :param int|None n: if an integer passed it must be in the interval `[0, N]` where `N`
            is the size/dimension  of the probaility distribution.
            If `None`, the values of R(n, S) for all possible values of `n` are returned.
        :param iterable w: the vector of weights derived from Bernoulli trials parameters..
        :param bool log: if `True` return the logarithm (natural logarithm)  `log(R(n, S))`,
            otherwise return the value of `R(n, S)`

        :returns: the value(s) of `R(n, S)` either as a scalar (if `n` is not `None`)
            or a sequence if `n` is None.
        :rtype: :py:class:`decimal.Decimal` or a list of `decimal.Decimal` values

        :raises: :py:class:`TypeError` `n` is not integer or `w` is of unrecognized type
        :raises: :py:class:`ValueError` if any of the weights in `w` fall outside
            the interval [0, 1].
        """
        if not utility.isnumber(n) or int(n)!=n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer; "
                f"Received '{n}' of type '{type(n)}'"
            )
        else:
            n = int(n)

        # Convert probabilities into Decimals
        if utility.isnumber(w):
            w = [Decimal(w)]
        elif utility.isiterable(w):
            w = [Decimal(t) for t in w]
        else:
            raise TypeError(
                f"Invalid Type of the weights `n`. Expected iterable  "
                f"Received '{w=}' of '{type(w)=}'"
            )

        # Based on n, evalute R-function (on a log scale)
        Ns = len(w)

        if n < 0 or n > Ns:
            R_n_S = Decimal('-inf') if log else Decimal('0.0')

        elif n == 0:
            R_n_S = Decimal('1.0').ln() if log else Decimal(1.0)

        else:

            # R is calculated recursively
            #----------------------------
            # Initialize the first row
            row_length = Ns - n + 1
            moving_row = [Decimal('1') ] * row_length

            # Calculate new row (i) from previsous row (i-1)
            for i in range(n):
                moving_row[0] *= w[i]
                for j in range(1, row_length):
                    moving_row[j] = moving_row[j] * w[i+j] + moving_row[j-1]

            # Extract the last R-function value
            R_n_S = moving_row[-1]
            if self.debug:
                print(f"{n=}; N=\n{len(w)=}; (log)\n{R_n_S=} \n\n")

            # Calculate logarithm if needed
            if log:
                if R_n_S == 0:
                    R_n_S = Decimal('-inf')
                elif R_n_S > 0:
                    R_n_S = R_n_S.ln()
                else:
                    raise ValueError(
                        f"The R-function value R({n=}, S)={R_n_S} is negative. \n"
                        f"The weights: {w=}. \n"
                        f"Cannot evaluate the logarithm!"
                    )

        # Check/Correct return data type
        if dtype is not Decimal:
            R_n_S = dtype(R_n_S)

        return R_n_S

    def gradient(self, n, w, dtype=Decimal, log=False, ):
        """
        Evaluate the gradient of the R-function :math:`R(n, S)`,
        or its logarithm, with respect to the weights `w`.

        .. note::
            This method is a wrapper that calls either
            :py:meth:`gradient_by_recursion` or
            :py:meth:`gradient_by_tabulation` based on the registered evaluation method.

        """
        method = self.method
        if re.match(r"\Arecur(rence|sion)\Z", method, re.IGNORECASE):
            result = self.gradient_by_recursion(n=n, w=w, dtype=dtype, log=log, )
        elif re.match(r"\Atab(le|ulation)\Z", method, re.IGNORECASE):
            result = self.gradient_by_tabulation(n=n, w=w, dtype=dtype, log=log, )
        else:
            raise ValueError(
                f"This shouldn't happen! "
                f"The {method=} is not recognized; it might have been changed "
                f"manually by the user by directly modifying the configurations dictionary!"
            )
        return result

    def gradient_by_recursion(self, n, w, dtype=Decimal, log=False, enforce_non_negative_R=False, ):
        """
        Evaluate the gradient of the R-function :math:`R(n, S)`
        with respect to the weights `w`.
        This method returns the derivative of the result generated by
        :py:meth:`evaluate_by_recursion`, and accepts the same arguments.

        .. note::
            If `log` is `True` this function returns the gradient of the logarithm
            of the R-function. This is simply evaluated by applying the rule of derivative
            of the logarithm.

        """
        if not utility.isnumber(n) or int(n)!=n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer; "
                f"Received '{n}' of type '{type(n)}'"
            )
        else:
            n = int(n)

        # Convert probabilities into Decimals
        if utility.isnumber(w):
            w = [Decimal(w)]
        elif utility.isiterable(w):
            w = [Decimal(t) for t in w]
        else:
            raise TypeError(
                f"Invalid Type of the weights `n`. Expected iterable; "
                f"Received '{w=}' of '{type(w)=}'"
            )

        # Based on n, evalute R-function (on a log scale)
        Ns = len(w)

        if n <=0  or n > Ns:
            grad = [Decimal('0.0') for _ in range(Ns)]
            R_n_S = Decimal('1')

        elif n == 1:
            grad = [Decimal(1.0)] * Ns
            R_n_S = sum(w)

        else:
            ## Recursive calculation of gradient of log(R)
            ## Allocate lists (require very large numbers); can't use numpy arrays

            ## Full list of Signs
            #--------------------
            S = [Decimal((-1)**(i+1)) for i in range(1, n+1)]

            ## Full list for T & its gradient
            #--------------------------------
            T = [ sum([v**Decimal(i) for v in w]) for i in range(1, n+1) ]
            nabla_T = [
                [
                    Decimal(i) * v**(Decimal(i-1)) if v!=0 else Decimal('0') for v in w
                ]
                for i in range(1, n+1)
            ]

            ## Expanding list for gradient R
            # Initialize R[0] = 1, R[1] = sum(w)
            R = [Decimal(1.0)]

            ## Initialzie gradient R for k = 1
            #---------------------------------
            nabla_R = [[Decimal(0.0)] * Ns]

            def prod(args):
                """Product via sum"""
                return sum([v.ln() for v in args]).exp()
            def vec_sum(args, size):
                """Vectorize sum for multiple iterables of same length"""
                if self.debug:
                    print(f"Vec summing: {args=} of {type(args)=}")
                    print(f"{list(args)=}")
                return [sum([a[i] for a in args]) for i in range(size)]
            def vec_prod(args, size):
                """Vectorize product for multiple iterables of same length"""
                return [prod([a[i] for a in args]) for i in range(size)]

            if self.debug:
                print(
                    f"\n****\nDEBG: Evaluating Gradient R by recursion\n****\n"
                    f"{S=}\n"
                    f"{T=}\n"
                    f"{R=}\n"
                    f"{nabla_R=}\n"
                    f"{nabla_T=}"
                )

            ## Evaluate R and its gradient recursivelly (k=2, ...)
            #----------------------------------------------------
            for i in range(1, n+1):
                # New entry
                R_i = sum( [s*t*r for s,t,r in zip(S[: i], T[: i], R[::-1]) ] ) / Decimal(i)

                if enforce_non_negative_R and R_i < 0:
                    msg = f"Encountered Non-Positive value of R(n, N)!"
                    msg += f"\nR({i}, {Ns}) -> {R_i} which is wrong!\n{'-'*25}\n"
                    for j in range(i, max(0, i-5), -1):
                        try:
                            msg += f"{j=} -> S(j={j})={S[j]}; T(j={j})={T[j]}; R(j={j-1})={R[len(R)-1]}; \n"
                        except(IndexError):
                            break
                    raise ValueError(msg)

                # Validating the gradient
                ## Gradient ((-1)^(i+1)/k)
                SCALE = [s/Decimal(i) for s in S[: i]]
                nabla_R_i = vec_sum(
                        [
                            vec_sum(
                                [
                                    [scl * t * gr for gr in grad_r],
                                    [scl * r * gt for gt in grad_t]
                                ],
                                size=Ns
                            )
                            for scl, t, grad_t, r, grad_r in zip(
                                SCALE,
                                T[: i],
                                nabla_T[: i],
                                R[::-1],
                                nabla_R[::-1]
                            )
                        ]
                    ,
                    size=Ns,
                )

                if self.debug:
                    print(
                        f"********\nIteration {i=}/{n=}\n"
                        f"{R_i=}\n"
                        f"{SCALE=}\n"
                        f"T[: {i}]={T[: i]=}\n"
                        f"nabla_T[: {i}]={nabla_T[: i]=}\n\n"
                        f"R={R=}\n"
                        f"nabla_R={nabla_R=}\n"
                        f"R[::-1]={R[::-1]=}\n"
                        f"nabla_R[::-1]={nabla_R[::-1]=}\n*******\n"
                        f"Resulting Gradient: {nabla_R_i=}\n"
                    )

                # Append to R
                R.append(R_i)
                nabla_R.append(nabla_R_i)

            grad = nabla_R[-1]
            R_n_S = R[-1]

        ## Convert to derivative of the logarithm.
        if log:
            ## grad log R(n, S) = 1/R(n, S) grad R(n, S)
            grad = [gr/R_n_S for gr in grad]

        # Check/Correct return data type
        if dtype is not Decimal:
            grad = [dtype(v) for v in grad]

        return grad

    def gradient_by_tabulation(self, n, w, dtype=Decimal, log=False, ):
        """
        Evaluate the gradient of the R-function :math:`R(n, S)`
        (or its logarithm) with respect to the weights `w`.
        This method returns the derivative of the result generated by
        :py:meth:`evaluate_by_tabulation`, and accepts the same arguments.
        """
        if not utility.isnumber(n) or int(n)!=n:
            raise TypeError(
                f"Invalid Type of the sum `n`. Expected integer; "
                f"Received '{n}' of type '{type(n)}'"
            )
        else:
            n = int(n)

        # Convert probabilities into Decimals
        if utility.isnumber(w):
            w = [Decimal(w)]
        elif utility.isiterable(w):
            w = [Decimal(t) for t in w]
        else:
            raise TypeError(
                f"Invalid Type of the weights `n`. Expected iterable  "
                f"Received '{w=}' of '{type(w)=}'"
            )

        # Based on n, evalute R-function (on a log scale)
        Ns = len(w)

        if n < 0 or n > Ns:
            grad = [Decimal('0.0') for _ in range(Ns)]
            R_n_S = Decimal('1')

        elif n == 1:
            grad = [Decimal(1.0)] * Ns
            R_n_S = sum(w)

        else:
            def vec_sum(args, size):
                """Vectorize sum for multiple iterables of same length"""
                if self.debug:
                    print(f"Vec summing: {args=} of {type(args)=}")
                    print(f"{list(args)=}")
                return [sum([a[i] for a in args]) for i in range(size)]
            # R is calculated recursively
            #----------------------------
            # Initialize the first row
            row_length = Ns - n + 1

            # Each row of the table (with non-null values) (for both R and its gradient)
            moving_row      = [Decimal('1') ] * row_length
            moving_row_grad = [[Decimal('0') ] * Ns for _ in range(row_length)]

            # Calculate new row (i) from previsous row (i-1) (for both R and gradient)
            for i in range(n):
                # Backup of R_vals to update gradient
                R_vals = [_ for _ in moving_row]

                # Update R values row
                moving_row[0] *= w[i]
                for j in range(1, row_length):
                    moving_row[j] = moving_row[j] * w[i+j] + moving_row[j-1]

                # Update R gradient row
                moving_row_grad[0] = [w[i]*v for v in moving_row_grad[0]]
                moving_row_grad[0][i] += R_vals[0]
                for j in range(1, row_length):
                    moving_row_grad[j] = vec_sum(
                        [
                            [v*w[i+j] for v in moving_row_grad[j]],
                            moving_row_grad[j-1]
                        ],
                        size=Ns
                    )
                    moving_row_grad[j][i+j] += R_vals[j]

            grad = moving_row_grad[-1]
            R_n_S = moving_row[-1]

        ## Convert to derivative of the logarithm.
        if log:
            ## grad log R(n, S) = 1/R(n, S) grad R(n, S)
            grad = [gr/R_n_S for gr in grad]

        # Check/Correct return data type
        if dtype is not Decimal:
            grad = [dtype(v) for v in grad]

        return grad

    @property
    def verbose(self):
        """Screen verbosity of the model"""
        return self.configurations.verbose
    @verbose.setter
    def verbose(self, val):
        """Set screen verbosity"""
        self.update_configurations(verbose=val)

    @property
    def method(self, ):
        """Return the name of the evaluation method used"""
        return self.configurations.method
    @method.setter
    def method(self, val):
        """Update the name of the evaluation method used"""
        self.update_configurations(method=val)

