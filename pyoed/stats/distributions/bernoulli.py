# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import numpy as np
import warnings
from dataclasses import dataclass, field, replace
from typing import Iterable
import itertools

from pyoed import utility
from pyoed.configs import (
    PyOEDConfigs,
    PyOEDConfigsValidationError,
    validate_key,
    set_configurations,
    remove_unknown_keys,
)
from pyoed.stats.core import (
    Distribution,
    DistributionConfigs,
)


@dataclass(kw_only=True, slots=True)
class BernoulliConfigs(DistributionConfigs):
    """
    Configurations class for the :py:class:`Bernoulli` abstract base class.  This class
    inherits functionality from :py:class:`DistributionConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`DistributionConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`Bernoulli`
    provides the following fields:

    :param name: name of the distribution
    :param parameter: probability of success of the bernoulli trials.
        This determins the dimension of the probability distribution
    """

    parameter: float | Iterable[float] = 0.5
    name: str = "Mulitvariate Bernoulli Distribution (iid)"


@set_configurations(configurations_class=BernoulliConfigs)
class Bernoulli(Distribution):
    """
    An implementation of the multivariate Bernoulli Distribution with independent
    components (no covariances).

    :param dict | BernoulliConfigs | None configs: (optional) configurations for
        the model
    """
    def __init__(self, configs: dict | BernoulliConfigs | None = None) -> None:
        super().__init__(
            configs=self.configurations_class.data_to_dataclass(configs),
        )

    def validate_configurations(
        self,
        configs: dict | BernoulliConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validation stage for the the passed configs.

        :param configs: configurations to validate. If a BernoulliConfigs
            object is passed, validation is performed on the entire set of
            configurations. However, if a dictionary is passed, validation is performed
            only on the configurations corresponding to the keys in the dictionary.
        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`BernoulliConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage (of local parameters)
        # `parameter`: float or  Iterable of floats
        def valid_parameter(v):
            eps = 1e-12  # for tolerance in probability
            if utility.isnumber(v):
                return True
            if utility.isiterable(v):
                try:
                    _v = np.asarray(
                        v,
                        dtype=float,
                    )
                    if _v.ndim == 0:
                        _v = _v.flatten()
                except Exception as err:
                    pass
                else:
                    if _v.size > 0 and all([-eps <= p <= 1 + eps for p in v]):
                        return True
            # Neither number or iterable of floats within [0, 1]
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="parameter",
            test=valid_parameter,
            message=f"Invalid succes probability `parameter` to be valid number/iterable in [0, 1]",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid, )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid
        """
        ## Validate and udpate configurations object
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Check if "parameter" is passed; make sure it is 1d array
        parameter = kwargs.get("parameter", None)
        if parameter is not None:
            self.configurations.update(
                {
                    "parameter": utility.asarray(parameter).flatten(),
                }
            )

    def update_parameter(self, p):
        """Update the success probability and any dependent values/configurations"""
        self.update_configurations(parameter=p)

    def sample(
        self,
        sample_size=1,
        antithetic=False,
        dtype=bool,
    ):
        """
        Sample a Bernoulli random variable (1d or multivariate) according to probability
        of success `p`, that is ::math:`p:=(P(x=1))`.
        If antithetic is True, sample_size must be even

        :param int sample_size: size of the sample to generate
        :param bool antithetic:
        :param type dtype: data type of the returned array

        :returns: bernoulli_sample: array of shape ``sample_size x n`` where ``n`` is
          the size of `p`

        :raises: :py:class`TypeError` if the `sample_size` is invalid

        .. note::
            If p is scalar or iterable of length 1, this will be 1d array of
            size=sample_size. Otherwise, if p is multivariate, this will be 2d
            array with each row representing one sample.
        """
        if not (sample_size > 0 and int(sample_size) == sample_size):
            raise TypeError(
                f"sampel_size must be +ve integer"
                f"Received {sample_size=} of {type(sample_size)=}"
            )

        # Correct sample size if antithetic is not proper
        return_sample_size = sample_size
        if antithetic:
            if sample_size % 2 != 0:
                sample_size += 1

        # Retrieve success probability
        success_probability = self.success_probability

        # Initialize container
        bernoulli_sample = np.empty((sample_size, self.size), dtype=dtype)

        # Sample size (based on the value of antithetic parameter)
        uniform_size = sample_size // 2 if antithetic else sample_size

        for i in range(self.size):
            # Sample uniform random variables and use to sample Bernoulli trials
            uniform = self.random_number_generator.random(uniform_size)

            # First sample:
            num_ones = np.count_nonzero(uniform <= success_probability[i])

            # Second sample; (if antithetic only sample_size//2  sampled in the first sample)
            if antithetic:
                num_ones += np.count_nonzero((1 - uniform) <= success_probability[i])

            num_zeros = sample_size - num_ones
            sample = np.zeros(sample_size, dtype=dtype)
            sample[:num_ones] = 1
            self.random_number_generator.shuffle(sample)
            bernoulli_sample[:, i] = sample

        # Update in case extra sample is added to enable antithetic usage
        bernoulli_sample = bernoulli_sample[:return_sample_size, :]

        return bernoulli_sample

    def expect(
        self,
        func,
        objective_value_tracker=None,
    ):
        """
        Calculate the expected value of a function (func) which accepts w as parameter
        """
        assert callable(func), "func must be a callable"
        theta = self.success_probability
        if self.size > 15:
            warnings.warn(
                "****\n"
                "You'll be waiting for long time for enumerating too many possibilities!\n"
                "The dimension is {self.size}; resulting in {2**{self.size} possible instances}\n"
                "****"
            )

        # Initialize the expectation value and check the tracker
        e_val = 0.0
        if objective_value_tracker is None:
            objective_value_tracker = {}

        # Loop over all expectation terms
        for k in range(1, 2**self.size + 1):

            # Retrieve the binary variable/realization uniquely associated with the passed index
            x = self.index_to_binary_state(
                k,
            )

            # Evaluate the value of the objective (try retrieving from objective_value_tracker)
            try:
                v = objective_value_tracker[k]
            except KeyError:
                v = func(x)
                # NOTE: we can discard updating the tracker; the only benefit is in-place update
                objective_value_tracker.update({k: v})

            # Evaluate the joint probability mass function (PMF) value
            p = self.pmf(
                x,
                joint=True,
            )

            # Update expectation
            e_val += v * p

        return e_val

    def pmf(
        self,
        x,
        joint=True,
    ):
        """
        Calculate the value of the probability mass function (PMF) of
        a uncorrelated multivariate Bernoulli distribution, evaluated at given binary
        state/realization `x`, and with parameters defined by the underlying probability of
        success.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param bool joint: if `True` joint PMF value is returned, otherwise marginal PMF for
            all entries is returned.

        :returns: joint PMF (if `joint` is `True`; default) or the marginal PMF values
            (if `joint` is `False`)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
        """
        # Reshaping and assertion
        x = utility.asarray(x, dtype=int).flatten()
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        # Calculate probabilities (marginal and joint)
        theta = self.success_probability
        v0 = np.power(theta, x)
        v1 = np.power(1 - theta, 1 - x)
        marginal_prob = v0 * v1
        marginal_prob[np.isinf(marginal_prob)] = np.nan
        if not joint:
            prob = marginal_prob
        else:
            prob = np.prod(marginal_prob)
        return prob

    def log_pmf(
        self,
        x,
        joint=True,
    ):
        """
        Calculate the value of log-PMF (probability mass function) of
        a uncorrelated multivariate Bernoulli distribution

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param bool joint: if `True` joint PMF value is returned, otherwise marginal PMF for
            all entries is returned.

        :returns: logarithm of the joint PMF (if `joint` is `True`; default) or the
            logarithmic values of the marginal PMF (if `joint` is `False`)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
        """
        # Reshaping and assertion
        x = utility.asarray(x, dtype=int).flatten()
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        # Calculate probabilities (marginal and joint)
        marginal_prob = self.pmf(
            x,
            joint=False,
        )
        marginal_log_prob = np.log(marginal_prob)
        marginal_log_prob[np.isinf(marginal_log_prob)] = np.nan

        if not joint:
            log_prob = marginal_log_prob
        else:
            log_prob = np.sum(marginal_log_prob)
        return log_prob

    def grad_pmf(
        self,
        x,
        joint=True,
    ):
        """
        Calculate the gradient of the probability mass function (PMF) of
        a uncorrelated multivariate Bernoulli distribution, evaluated at
        given binary state x, and with parameters theta.
        The derivative is taken with respect to the distribution parameters
        (success probabilities) not the realization of the random variable x.

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param bool joint: if `True` joint PMF value is returned,
            otherwise marginal PMF for all entries is returned.

        :returns: gradient of the joint PMF (if `joint` is `True`; default) or
            the gradient of the marginal PMF values (if `joint` is `False`)

        :raises: :py:class:`TypeError` if the passed `x` has wrong shape/size
        """
        # Reshaping and assertion
        x = utility.asarray(x, dtype=bool).flatten().astype(int)
        if x.size != self.size:
            raise TypeError(f"x has size {x.size}; expected {self.size}")

        # Calculate gradient of the PMF
        # Get success probabilities & check for degeneracy
        theta = self.success_probability

        # Initialize the gradient
        grad = np.zeros(self.size)

        if joint:
            zeros_locs = np.where(theta == 0)[0]
            ones_locs = np.where(theta == 1)[0]

            # Indexes with non-degenerate probabilities
            valid_inds = np.setdiff1d(
                range(self.size),
                np.union1d(ones_locs, zeros_locs),
            )

            # Claculate marginal probabilities
            marginal_prob = self.pmf(
                x,
                joint=False,
            )

            ## Calcualte gradient for non-degenerate entries
            _theta = theta[valid_inds]
            _x = x[valid_inds]
            grad[valid_inds] = self.pmf(
                x=x,
                joint=True,
            ) * (_x / _theta - (1 - _x) / (1 - _theta))
            if self.debug:
                print(
                    f"Testing:\n{_theta=}\n"
                    f"{_x=}\n"
                    f"{marginal_prob=}\n"
                    f"{( _x / _theta - (1-_x) / (1-_theta))=}\n"
                    f"{self.pmf(x=x, joint=True)=}\n"
                    f"{grad[valid_inds]=}\n"
                )

            ## Calculate gradient of degenerate entries
            for i in zeros_locs:
                if x[i] == theta[i]:
                    # theta == 0 & x = 0
                    grad[i] = -np.prod(marginal_prob[:i]) * np.prod(
                        marginal_prob[i + 1 :]
                    )
                else:
                    # theta == 0 & x = 1
                    grad[i] = np.prod(marginal_prob[:i]) * np.prod(
                        marginal_prob[i + 1 :]
                    )

            for i in ones_locs:
                if x[i] == theta[i]:
                    # theta == 1 & x = 1
                    grad[i] = np.prod(marginal_prob[:i]) * np.prod(
                        marginal_prob[i + 1 :]
                    )
                else:
                    # theta == 1 & x = 0
                    grad[i] = -np.prod(marginal_prob[:i]) * np.prod(
                        marginal_prob[i + 1 :]
                    )

        else:
            zeros_locs = x == 0
            grad[zeros_locs] = -1
            grad[~zeros_locs] = +1

        return grad

    def grad_log_pmf(
        self,
        x,
        joint=True,
        zero_bounds=True,
    ):
        """
        Calculate the gradient of log-PMF (probability mass function),
            with respect to distribution parameters (success probability)

        :param x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        :param bool joint: if `True` joint PMF is considered,
            otherwise marginal PMF is used.

        :returns: the gradient of the log probability

        .. note::
            Given the assumption that the Bernoulli RVs modeled are uncorrelated,
            the gradient of log-probabilities is same as partial derivatives of
            corresponding derivatives of log-prob of each entry; thus, whether
            `joint` is `True` or `False` the result is the same.
        """
        pmf = self.pmf(
            x=x,
            joint=joint,
        )
        if joint:
            if pmf == 0:
                if zero_bounds:
                    grad = np.zeros(self.size)
                else:
                    grad = np.empty(self.size)
                    grad[:] = np.nan
            else:
                grad = (
                    self.grad_pmf(
                        x=x,
                    )
                    / pmf
                )
        else:
            # Initiallize gradient & caculate for (non-degenerate) entries;
            #  set degenerate to nan
            grad = np.zeros(self.size)
            zero_prob_flags = pmf == 0
            grad[~zero_prob_flags] = self.grad_pmf(
                x=x,
                joint=True,
            )[~zero_prob_flags]

            # The log-probability is undefined for zero probability
            # Gradient (of the log-probability) is undefined in these directions; set to nan
            grad[zero_prob_flags] = 0.0 if zero_bounds else np.nan

        return grad

    def index_to_binary_state(
        self,
        k,
        dtype=bool,
    ):
        """
        Return the binary state=:math:`(v_1, v_2, \\dots)` of dimension as the size of
        this distribution, with index k.

        ..note::
            This is actually a wrapper around the utility function
            `pyoed.utility.math.index_to_binary_state` which is added
            here only for convenience.
        """
        return utility.index_to_binary_state(
            k=k,
            size=self.size,
            dtype=dtype,
        )

    def index_from_binary_state(self, state):
        """
        Reverse of "index_to_binary_state"
        Return the index k corresponding to the passed state (of dimension=size).

        ..note::
            This is actually a wrapper around the utility function
            `pyoed.utility.math.index_from_binary_state` which is added
            here only for convenience.
        """
        return utility.index_from_binary_state(
            state=state,
        )

    @property
    def success_probability(self):
        """Return the underlying probability of success"""
        return self.configurations.parameter

    # Alias
    parameter = success_probability

    @success_probability.setter
    def success_probability(self, val):
        """Update the success probability (parameter) of the Bernoulli distribution"""
        self.update_configurations(
            parameter=utility.asarray(val, dtype=float).flatten()
        )

    @property
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        return self.parameter.size
