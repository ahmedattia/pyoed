# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Abstract classes for probability distributions e.g., Multivariate Bernoulli.
These are also the base for sampling proposals..
"""

## Import
# Essentials for Class design
from abc import (
    abstractmethod as _abstractmethod,
    abstractproperty as _abstractproperty,
)
from dataclasses import dataclass

from pyoed import utility
from pyoed.utility.mixins import RandomNumberGenerationMixin
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    validate_key,
    set_configurations,
)


@dataclass(kw_only=True, slots=True, )
class DistributionConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`Distribution` abstract base class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and only adds
    new class-level variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`DistributionConfigs`
    provides the following fields:

    :param name: name of the distribution
    :param random_seed: random seed used for pseudo random number generation
    """

    name : str | None = None
    random_seed: int | None = None

@set_configurations(configurations_class=DistributionConfigs)
class Distribution(PyOEDObject, RandomNumberGenerationMixin):
    """
    Base class for probability distributions (and sampling proposals).

    .. note::

        Each class derived from `Distribution` should have its own `__init__` method
        in which the constructor just calls `super().__init__(configs=configs)`
        and then add any additiona initialization as needed.
        The validation :py:meth:`self.validate_configurations` is carried out at the
        initialization time by the base class :py:class:`Distribution`.

    :param dict | DistributionConfigs | None configs: (optional) configurations for
        the model
    """
    def __init__(self, configs: dict | DistributionConfigs | None = None) -> None:
        super().__init__(
            configs=self.configurations_class.data_to_dataclass(configs),
        )

        # Maintain a proper random number generator (here and in the proposal)
        self.update_random_number_generator(random_seed=self.configurations.random_seed)

    def validate_configurations(
        self,
        configs: dict | DistributionConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each distribution **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the model class.

        .. note::
            The purposed of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a DistributionConfigs object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `name`: None or string
        if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="name",
                test=lambda v: isinstance(v, (str, type(None))),
                message=f"Distribution name is of invalid type. Expected None or string. ",
                raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `random_seed`: None or int
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="random_seed",
            test=lambda v: v is None or (isinstance(v, int) and v>=0),
            message=f"random_seed must be None or a non-negative integer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(configs=configs, raise_for_invalid=raise_for_invalid, )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Check if "random_seed" is passed; update random number generator
        if "random_seed" in kwargs:
            self.update_random_number_generator(random_seed=kwargs["random_seed"])


    @_abstractmethod
    def sample(self, sample_size=1):
        """
        Generate sample(s) from the created distribution.
        This method returns a list with each entry representing a sample point
        from the underlying distribution
        """
        ...

    @_abstractproperty
    def size(self):
        """Return the dimentionsize of the underlying probability space"""
        ...

