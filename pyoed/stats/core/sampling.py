# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Base classes for sampling algorithms; e.g., MCMC samplers.
"""

from abc import abstractmethod as _abstractmethod
from dataclasses import dataclass

from pyoed import utility
from pyoed.utility.mixins import (
    RandomNumberGenerationMixin,
)
from pyoed.configs import (
    PyOEDConfigs,
    PyOEDObject,
    set_configurations,
    aggregate_configurations,
    validate_key,
    SETTINGS,
)
from .distributions import (
    Distribution,
    DistributionConfigs,
)


@dataclass(kw_only=True, slots=True)
class ProposalConfigs(DistributionConfigs):
    """
    Configurations for the Proposal class.
    These do not add any additional keys to the base configurations of
    :py:class:`DistributionConfigs`.
    """


@set_configurations(ProposalConfigs)
class Proposal(Distribution):
    """
    Base class for Proposal (algorithms to propose samples which can be accepted or
    rejected by the MH step in MCMC). An example is a Gaussian proposal. Derived from
    :py:class:`Distribution`, hence, one should be able to use it as a distribution.

    :param configs: an object that holds the proposal configurations.
    """

    def __init__(self, configs: dict | ProposalConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self, configs: dict | ProposalConfigs, raise_for_invalid: bool = True
    ) -> bool:
        return super().validate_configurations(configs, raise_for_invalid)

    def __call__(self, **kwargs):
        """
        The proposal instance can be used as a function to generate one sample point.
        The only difference between this function and `sample` is that the returned
        sample point is extracted out of the sample(list)
        """
        return self.sample(sample_size=1, **kwargs)[0]


@dataclass(kw_only=True, slots=True)
class SamplerConfigs(PyOEDConfigs):
    """
    Base configurations for the Sampler class.

    :param random_seed: random seed used when the object is initiated to keep track
        of random samples. This is useful for reproductivity. If `None`, random seed follows `numpy.random.seed` rules.
    """

    random_seed: None | int = SETTINGS.RANDOM_SEED


@set_configurations(SamplerConfigs)
class Sampler(PyOEDObject, RandomNumberGenerationMixin):
    """
    Base class for Samplers (algorithms to generate samples from a predefined
    distribution). An example is an inverse CDF sampler, MCMC sampler, etc.

    :param configs: an object that holds the proposal configurations.
    """

    def __init__(self, configs: dict | SamplerConfigs | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Update random number generator (from the mixin class)
        self.update_random_number_generator(
            random_seed=self.configurations.random_seed,
        )

    def validate_configurations(
        self, configs: dict | SamplerConfigs, raise_for_invalid: bool = True
    ) -> bool:
        """
        Each Sampler may implement it's own function that validates its own
        configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation on local configurations, return
        `super().validate_configurations(configs, raise_for_invalid)` under this method.
        If one does not want to do any validation at all, simply omit this method.

        .. note::
            The purposed of this method is to make sure that the settings in the
            configurations object are of the right type/values and are conformable with
            each other.  This function is called upon instantiation of the object, and
            each time a configuration value is updated. Thus, this function ought to be
            inexpensive and should not do heavy computations.

        :param configs: configurations to validate.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the model configurations :py:class:`PyOEDConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = aggregate_configurations(
            obj=self, configs=configs, configs_class=self.configurations_class
        )

        ## Validate specific entries/configs

        is_valid_seed = lambda x: x is None or (
            utility.isnumber(x) and int(x) == x and x >= 0
        )
        # Validate `random_seed`
        if not validate_key(
            aggregated_configs,
            configs,
            "random_seed",
            test=is_valid_seed,
            message=f" `rand_seed` must be non-negative integer or None",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises: :py:class:`TypeError` is raised if any of the passed keys in ``kwargs``
            is invalid/unrecognized

        :remarks:
            Generally, we don't want actual implementations in abstract classes,
            however, this one is provided as good guidance. Derived classes can rewrite it.
        """
        if self.validate_configurations(kwargs, raise_for_invalid=True):
            self.configurations.update(kwargs)

        if "random_seed" in kwargs:
            self.update_random_number_generator(random_seed=random_seed)

    @_abstractmethod
    def sample(self, sample_size) -> list:
        """
        This function generates samples from the created sampler.
        This method returns a list with each entry representing a sample point from the
        underlying distribution
        """
        ...

    @property
    @_abstractmethod
    def size(self) -> int:
        """Return the dimension size of the underlying probability space"""
        ...
