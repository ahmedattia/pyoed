# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module implements several variants of the four-dimensional variational (4D-Var) data assimilation scheme.
4D-Var refers to simulation/prediction in three-dimensional spatial coordinates + one temporal dimension.
The name 4D-Var is popular in the atmospheric weather prediction (NWP) literature.
This algorithm is equivalent to standard regularized least-squares inversion.
Multiple observations (over a time window) are used to update the model parameter/initial condition.
When the time window collapses to one observation instance, the 4D-Var reduces to 3D-Var scheme
"""

import warnings
import re
import inspect
from dataclasses import dataclass
from typing import Type
import numpy as np
import scipy.sparse.linalg as splinalg

from pyoed import utility
from pyoed.configs import (
    set_configurations,
    validate_key,
)
from pyoed.models import(
    ErrorModel,
    ObservationOperator,
)
from pyoed.models.simulation_models import (
    TimeDependentModel,
)
from pyoed.assimilation.core import (
    VariationalSmoother,
    VariationalSmootherConfigs,
    VariationalSmootherResults,
    GaussianPosterior,
)
from pyoed.optimization import (
    create_optimizer,
    Optimizer,
    OptimizerConfigs,
    ScipyOptimizer,
    ScipyOptimizerConfigs,
)


@dataclass(kw_only=True, slots=True)
class VanillaFourDVarConfigs(VariationalSmootherConfigs):
    """
    Configurations class for the :py:class:`VanillaDVar` abstract base class.
    """
    name: str | None = "4D-Var: Four Dimensional Variational Data Assimilation"
    optimizer: None | Optimizer = ScipyOptimizer
    optimizer_configs: None | OptimizerConfigs = ScipyOptimizerConfigs


@set_configurations(VanillaFourDVarConfigs)
class VanillaFourDVar(VariationalSmoother):
    """
    A class implementing the vanilla 4D-Var DA scheme to invert for model initial state.
    This version is very elementary and is developed as guidelines to create more
    advanced versions, e.g., parameter-retrieval, or extended (parameter-state)
    retrieval for inifinite dimensional formulations; see remrks below for assumptions
    about the dynamical model.

    This scheme assumes Gaussian observational noise.  The model unknown state is
    regularized (to avoid overfitting to noise) by assuming a prior (usually a
    Gaussian), or by specifying a regularization matrix and following an :math:`\\ell_2`
    regularization approach.
    """
    # Hard-coded optimizers we support out of the box
    _SUPPORTED_OPTIMIZERS = [
        "ScipyOptimizer",
    ]

    def __init__(self, configs: VanillaFourDVarConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        ## Add Internally preserved (local) variables

        # The ptimization routine (overwrite default one)
        self.register_optimizer(
            optimizer=self.configurations.optimizer,
            optimizer_configs=self.configurations.optimizer_configs,
        )

    def register_optimizer(
        self,
        optimizer: str | Optimizer | Type[Optimizer] = "ScipyOptimizer",
        optimizer_configs: None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None,
    ) -> Type[Optimizer]:
        """
        Register (and return) an optimization routine, and make sure the
        objective function in the optimization routine is set to the objctive function of
        this objective :py:meth:`objective_function_value`.
        The objective function of the optimizer (and its derivatives) are set to the
        objective function and derivatives implemented here **unless the objective function is
        passed explicitly in the configurations**.
        This method sets a reference to the optimizer and update the underlying configurations
        accordingly.

        .. note::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            and valid configurations `optimizer_configs` is passed, the optimizer is
            updated with the passed configurations by calling
            `optimizer.update_configurations(optimizer_configs)`.

        .. warning::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            the responsibility is on the developer/user to validate the contents of the
            optimizer.

        :param optimizer: the optimization routine (optimizer) to register.
            This can be one of the following:

            - An optimizer instance (object that inherits :py:class`Optimizer`).
              In this case, the optimizer is registered **as is** and is updated
              with the passed configurations if available.
            - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
              the optimizer.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.
            - The name of the optimizer (`str`). This has to match the name of one
              of the available optimization routine.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.

        :param optimizer_configs: the configurations of the optimization routine.
            This can be one of the following:

            - `None`, in this case configurations are discarded, and whatever default
              configurations of the selected/passed optimizer are employed.
            - A `dict` holding full/partial configurations of the selected optimizer.
              These are either used to instantiate or update the optimizer configurations
              based on the type of the passed `optimizer`.
            - A class providing implementations of the configurations (this must be
              a subclass of :py:class:`OptimizerConfigs`.
            - An instance of a subclass of :py:class:`OptimizerConfigs` which is to
              set/udpate optimizer configurations.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer and/or configurations
            are/is not supported
        """
        if isinstance(optimizer, str) and optimizer in self.supported_optimizers:
            # Convert class name to class
            optimizer = eval(optimizer)

        # Create a dictionary with configurations to be forced
        new_configs = {
            "fun": self.objective_function_value,
            "jac": self.objective_function_gradient,
            "verbose": self.verbose,
        }

        try:
            optimizer = create_optimizer(
                optimizer=optimizer,
                optimizer_configs=optimizer_configs,
                new_configs=new_configs,
            )
        except Exception as err:
            raise TypeError(
                "Failed to create/update the optimizer! "
                f"See the error below for additional details\n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

        # Update optimizer and configurations properly
        return super().register_optimizer(
            optimizer=optimizer,
        )


    def validate_configurations(
        self,
        configs: dict | VanillaFourDVarConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`VanillaFourDVarConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # Local tests
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        def is_valid_observations(observations):
            if observations is None:
                valid = True
            elif isinstance(observations, dict):
                try:
                    obs_times = np.asarray([k for k in self.observations.keys()])
                    obs_times.sort()
                    # TODO: Maybe more assertions...
                    valid = True
                except:
                    valid = False
            else:
                valid = False
            return valid

        def is_valid_window(window):
            if window is None:
                valid = True
            elif utility.isiterable(window):
                try:
                    t0, t1 = window
                    prec = len(("%f" % (self._TIME_EPS - int(self._TIME_EPS))).split(".")[1])
                    t0 = np.round(t0, prec)
                    t1 = np.round(t1, prec)
                    valid = True
                except:
                    valid = False

                if not(0 <= t0 < t1):
                    valid = False
            else:
                valid = False
            return valid

        def is_valid_optimizer(optimizer):
            if inspect.isclass(optimizer):
                return issubclass(optimizer, Optimizer)
            elif isinstance(optimizer, Optimizer):
                return True
            elif isinstance(optimizer, str):
                return optimizer in self.supported_optimizers
            else:
                return False

        # `optimizer`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="optimizer",
            test=is_valid_optimizer,
            message=f"Invalid optimizer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `observations`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observations",
            test=is_valid_observations,
            message=f"Invalid observations",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `window`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="window",
            test=is_valid_window,
            message=f"Invalid assimilation `window`",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Optimizer and/or optimizer configurations
        if "optimizer" in kwargs:
            self.register_optimizer(
                optimizer=kwargs["optimizer"],
                optimizer_configs=kwargs.get("optimizer", None),
            )
        elif "optimizer_configs" in kwargs:
            optimizer_configs = kwargs["optimizer_configs"]
            if isinstance(optimizer_configs, dict):
                pass
            elif isinstance(optimizer_configs, OptimizerConfigs):
                optimizer_configs = optimizer_configs.asdict()
            else:
                raise TypeError(
                    f"Updating optimizer confiurations requires passing a dictionary or instance "
                    f"of proper Configs class that the optimizer accepts (here "
                    f"{self.optimizer.configurations_class}).\n"
                    f"Passed {optimizer_configs=} of {type(optimizer_configs)}"
                )
            self.optimizer.update_configurations(**optimizer_configs)

        if "observations" in kwargs:
            self.register_observations(kwargs["observations"])

    def register_model(
        self,
        model: None | TimeDependentModel = None,
    ) -> TimeDependentModel | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.


        :raises TypeError: if the type of passed model is not supported
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...
        # Here, the model need to be time-dependent
        if not isinstance(model, (type(None), TimeDependentModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `TimeDependentModel`"
            )

        if model is not None:
            if self.prior is not None:
                if not model.is_state_vector(self.prior.mean):
                    raise TypeError(
                        f"The passed model doesn't seem to be compatible with "
                        f"the registered prior .\n"
                        f"The models has {model.state_size=}\n"
                        f"The prior dimension is {self.prior.size=}"
                    )

        # Return the model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
        initiate_posterior: bool = True,
    ) -> Type[ErrorModel] | None:
        """
        Update prior/regularization information. Since, changing the prior implies
        a different posterior, the argument `initiate_posterior` is added with
        defaul value `True` to make sure the posterio is overwritten/reset

        :raises: TypeError is raised if:

            - the passed `prior` type is not supported
            - a valid simulation model has not been registered yet
            - the prior is not compatible with the registered simulation model

        .. note::
            The prior here is asserted on the model state or model parmeter based
            on the `invert_for` flag selected in the configurations dictionary
        """
        # Associate with self and do super validation/testing
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # start with class derived from pyoed.models.error_models.*
        if prior is not None:

            if not isinstance(prior, ErrorModel) or not hasattr(prior, 'mean') or not hasattr(prior, 'covariance_matrix'):
                raise TypeError(
                    f"Currently, only Gaussian error models instances are allowed\n"
                    f"Received: {prior=} of {type(prior)=}"
                )

            if self.model is not None:
                if not self.model.is_state_vector(prior.mean):
                    raise TypeError(
                        f"The passed prior doesn't seem to be compatible with "
                        f"the registered simulation model and inversion objective.\n"
                        f"A prior in the sapce of the model state is expected"
                    )


        # Initiate the (Gaussian) posterior
        if initiate_posterior:
            if prior is None:
                self._POSTERIOR = None
            else:
                # Covariances must be updated after solving the inverse problem
                self._POSTERIOR = GaussianPosterior(configs=dict(mean=prior.mean))

        return prior

    def register_observation_operator(self, observation_operator: None | ObservationOperator):
        """
        The observation operator is a function/map that takes as input a model state,
        and returns an observation instance.

        :param observation_operator: the obsevation operator

        :raises TypeError: if `observation_operator` is not derived
            from :class:`ObservationOperator`
        :raises TypeError: if the passed operator doesn't provide a function
            to apply the tangent linear of the observation operator 'Jacobian_T_matvec'
        :raises TypeError: if the model state does not match the observation operator
            domain size

        .. note::
            We are assuming the observation operator is time-independent,
            however, it provides a method `apply` and/or `__call__` which converts
            a model state into an equivalent observation
        """
        # Associate with self and do super validation/testing
        observation_operator = super().register_observation_operator(
            observation_operator=observation_operator,
        )

        ## Extra testing
        ...

        # If valid operator found, make assertions on shape, attributes, etc.
        if observation_operator is not None:

            # Verify  the observation operator has 'Jacobian_T_matvec' method implemented
            if not hasattr(observation_operator, "Jacobian_T_matvec"):
                raise TypeError(
                    "3D-Var requires a method 'Jacobian_T_matvec' to be associated with the "
                    "observation operator"
                )

            if not callable(getattr(observation_operator, "Jacobian_T_matvec", None)):
                raise TypeError(
                    "3D-Var requires a valid method 'Jacobian_T_matvec' to be associated "
                    "with the observation operator"
                )

        return observation_operator

    def _check_init_guess(self, init_guess, raise_for_invalid=True, ):
        """
        Validate the passed `init_guess` by checking whther it is a
        valid parameter (if `self.invert_for` is equal to `'parameter'`)
        or a valid state (if `self.invert_for` is equal to `'state'`)

        :returns: whether the `init_guess` is valid or not

        :raises TypeError: if the `init_guess` is not valid and
            `raise_for_invalid` is `True`
        """
        # Validate passed init_guess
        if self.model.is_state_vector(init_guess):
            return True
        else:
            if raise_for_invalid:
                raise TypeError(
                    f"The passed init_guess is not of valid type/shape/size\n"
                    f"passed {init_guess=}"
                    f"Expected vector of size: {self.model.state_size}"
                )
            else:
                return False

    def solve(
        self,
        init_guess=None,
        skip_map_estimate=False,
        update_posterior=False,
    ):
        """
        Solve the inverse problem, i.e., find the model state given the
        registered observation and prior information

        :param init_guess: initial guess of the model state
        :param bool skip_map_estimate: use the prior mean as a map estimte
            (ignore observations) and upate posterior.
            This useful for OED when the model  is linear
        :param bool update_posterior: if True, the posterior mean and covariance operator
            are updated (given/around the solution of the inverse problem)

        :returns: the analysis state (MAP estimate of the posterior)

        :raises:
            - TypeError if any of the 4D-Var elements (model, observation operator,
              prior, solver, or data) is missing
            - ValueError if the underlying optimization routine is not supported yet
            - `AttributeError` is raised if the passed model doesn not provide a method
              'Jacobian_T_matvec' to apply the tangent linear model (TLM/Jacobian)
              transpose of the right-hand-side of the discretized PDE to a model
              state.
        """
        # Check to assure all elements are registered
        if not self.check_registered_elements():
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components."
                f"{self.show_registered_elements(display=False, )}"
            )

        # Use prior mean if no initial guess is given
        if init_guess is None: init_guess = self.prior.mean

        # Validate passed init_guess
        self._check_init_guess(init_guess)

        # Initialize the map point estimate
        if skip_map_estimate:
            if self.verbose:
                print("Skipping the MAP estimate as requested as requested...")
            map_point = init_guess
        else:
            if self.verbose:
                print("Solving the 4D-Var inverse problem for the MAP estimate")

            # Placeholder for the map estimate (numpy array or not)
            map_point = self.model.state_vector(init_val=0.0)

            optimization_results = self.optimizer.solve(
                init_guess,
            )
            map_point = optimization_results.x

        if self.debug:
            print("DEBUG: 1- map: ", map_point)

        # Update the posterior if needed
        if update_posterior:
            if self.verbose:
                print("Updating posterior mean/mode using MAP estimate")

            if self.posterior is None:
                self._POSTERIOR = GaussianPosterior(configs=dict(mean=map_point.copy()))
            else:
                self._POSTERIOR.mean = map_point.copy()

            if self.debug:
                print("DEBUG: 2- map: ", map_point)

            if self.verbose:
                print("Evaluating posterior covariance matrix/operator")

            self.posterior.update_covariance_matrix(self.posterior_covariance(map_point))

            if self.verbose:
                print("Done")

        if self.debug:
            print("DEBUG: 3- map: ", map_point)
        return map_point

    def show_registered_elements(self, display=True, ) -> None:
        """
        Compose and (optionally) print out a message containing the elements
        of the inverse problem and show what's registered and what's not

        :param display: if `True` print out the composed message about
            registered/unregistered elements

        :returns: the composed message
        """
        all_elements = [
            'model', 'prior', 'observation_operator',
            'observation_error_model', 'observations',
            'optimizer',
        ]

        sep = "*"*40
        registered_message = f"\n\n{sep}\n\tRegistered Elements\n{sep}\n"
        unregistered_message = f"{sep}\n\tUnregistered Elements\n{sep}\n"

        for element in all_elements:
            if getattr(self, element) is None:
                unregistered_message += f"- '{element}'\n"

            else:
                registered_message += f"- '{element}': {getattr(self, element)}\n"

        registered_message += f"{sep}\n"
        unregistered_message += f"{sep}\n"
        msg = registered_message + unregistered_message

        if display: print(msg)
        return msg

    def check_registered_elements(self, ) -> bool:
        """Check if all inversion elements are registered or not"""
        return self.optimizer is not None and super().check_registered_elements()

    def objective_function_value(
        self,
        state,
        data_misfit_only=False,
        return_traject=False,
    ):
        """
        Evaluate the value of the 4D-Var objective function
        (data misfit + prior/regularization) given the passed `state`
        as an estimate of the true model state/parameter

        :param state: state giving an estimate of the model initial state/parameter
        :param bool return_traject: if `True` the checkpoints and checkpointed states
            (forward traject *only at* the observation time points) are returned along with
            the value of the objective function

        :returns: value of the 4D-Var objective function. The checkpoints
            and checkpointed states are returned only if `return_traject` is `True`

        :raises:
            - TypeError is raised if `state` is not a valid model state
            - ValueError is raised if observations are not yet registered
            - AssertionError is raised if less than two valid observations are registered or
              observation time(s) fall outside the assimilation window
            - KeyError or AttributeError (as raised by the model) when the time
              integration step size
              of the simulation model is inaccessible;
              this is used only if `return_traject` is `True` and `dt` is `None`
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.window is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
                False if data_misfit_only else (self.prior is None)
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # get t0; tf is discarded, and is replaced with last observation time
        t0 = self.window[0]
        obs_tspan = self.observation_times

        if len(obs_tspan) < 1:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )

        if (t0 - obs_tspan[0]) > self._TIME_EPS:
            raise AssertionError(
                "An observation is registered outside the assimilation window!"
                "This should never happen"
            )

        # Validate passed init_guess
        self._check_init_guess(state)
        x0 = state.copy()  # state at the initial time of the assimilation window

        # Data-Misfit term
        # Initialize the data misfit term; (check if an observation exist at the beginning of the window)
        _t0 = self.find_observation_time(t0)
        if _t0 is None:
            misfit = 0.0

        else:
            ## There is an observation at the initial time
            # innovation term (data-misfit): H(x) - y
            y = self.observations[_t0][self.observation_operator.extended_design]
            innov = self.observation_operator(x0) - y
            scld_innov = self.observation_error_model.covariance_inv_matvec(innov)
            misfit = np.dot(innov, scld_innov)
            obs_tspan = obs_tspan[1:]

        # Initialize return tspan and trajectory if required
        checkpoints = [t0] if return_traject else None
        traject = [x0.copy()] if return_traject else None

        # Integrate forward, and checkpoint as needed
        flt_time = t0  # floating timepoint
        flt_state = x0.copy()  # floating state at each observation/checkpoint

        # Loop over all observation time instances/points
        for tobs in obs_tspan:
            # Retrieve observation and observation time
            _tobs = self.find_observation_time(tobs)  # make sure the right key is used
            y = self.observations[_tobs][self.observation_operator.extended_design]

            # Local time window upto the next observation time
            _, local_traject = self.model.integrate_state(
                state=flt_state,
                tspan=[flt_time, _tobs],
                checkpoints=[flt_time, _tobs],
            )

            ## Calculate the data misfit term
            # innovation term (data-misfit): H(x) - y
            innov = self.observation_operator(local_traject[-1]) - y
            scld_innov = self.observation_error_model.covariance_inv_matvec(innov)
            misfit += np.dot(innov, scld_innov)
            # Update returned trajectory if needed
            if return_traject:
                checkpoints.append(_tobs)
                traject.append(local_traject[-1])

            # Update time and state for next window
            flt_time = _tobs
            flt_state = local_traject[-1]

        # Evaluate the prior term
        if data_misfit_only:
            prior_term = 0
        else:
            xdiff = state - self.prior.mean
            prior_term = self.prior.covariance_inv_matvec(xdiff)
            prior_term = np.dot(xdiff, prior_term)

        # Objective value (prior + data misfit)
        obj = 0.5 * (prior_term + misfit)

        if return_traject:
            return obj, checkpoints, traject
        else:
            return obj

    def objective_function_gradient(
        self,
        state,
        data_misfit_only=False,
        checkpoints=None,
        checkpointed_states=None,
    ):
        """
        Evaluate the gradient of the 4D-Var objective function
        given the passed `state` as an estimate of the true model state/parameter

        :param state: state giving an estimate of the model initial state/parameter
        :param checkpoints: used if checkpointed states are passed, otherwise ignored
        :param checkpointed_states: trajectory of states evaluated at the passed
            checkpoints, starting at the passed `state`.
            This is useful to reduce redundancy in the optimization procedure

        :returns: the gradient of the 4D-Var cost function

        :raises: ValueError if `checkpoints` and/or `checkpointed_states`
            are passed but invalid or incompatible
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.window is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
                False if data_misfit_only else (self.prior is None)
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # get t0; tf is discarded, and is replaced with last observation time
        t0 = self.window[0]
        obs_tspan = self.observation_times

        if len(obs_tspan) < 1:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )

        # `_checkpoints` are created from observation timespan to use if needed
        if (t0 - obs_tspan[0]) > self._TIME_EPS:
            raise AssertionError(
                "An observation is registered outside the assimilation window! "
                "This should never happen"
            )

        elif (obs_tspan[0] - t0 ) > self._TIME_EPS:
            _checkpoints = np.insert(obs_tspan, 0, t0)
        else:
            _checkpoints = obs_tspan

        if checkpoints is checkpointed_states is None:
            # Neither checkpoints or checkpointed_states are given;
            #   integrate forward, checkpoint, and integrate backward
            checkpoints = _checkpoints
            checkpointed_states = None
            recheck_states = True
        elif checkpoints is None or checkpointed_states is None:
            # Only one of them is passed;
            raise ValueError("Either checkpoints or checkpointed_states are invalid")

        else:
            # 1- make sure checkpoints holds t0, and all observation timepoints
            # 2- checkpointed_states of the same length as checkpoints
            try:
                if np.allclose(_checkpoints, checkpoints) and len(checkpoints) == len(
                    checkpointed_states
                ):
                    # all seems good;
                    recheck_states = False
                else:
                    raise ValueError
            except:
                checkpoints = _checkpoints
                checkpointed_states = None
                recheck_states = True

        if self.debug:
            print(f"DEBUG: {t0=}, {checkpoints=}, {_checkpoints=}")

        # Propagate forward, and checkpoint states if checkpoints/checkpointed_states are invalid
        if recheck_states:
            # Start from t0, propagate forward to each timepoint, then propagate backward
            # We can either store states at all timesteps (memory consuming) or checkpoint at
            #   observation times (redundancy); I'll go with the latter for now (TODO: discuss)
            tspan = (checkpoints[0], checkpoints[-1])
            _, checkpointed_states = self.model.integrate_state(
                state.copy(), tspan=tspan, checkpoints=checkpoints,
            )

        # Adjoint step: Loop over checkpoints, and evaluate the observation-term update by applying Jacobian
        adjoint = 0.0
        for subwindow_ind in [i for i in range(len(checkpoints) - 1)][::-1]:
            subwindow = (checkpoints[subwindow_ind], checkpoints[subwindow_ind + 1])

            # checkpointed states (begninning and end of the window)
            x0 = checkpointed_states[subwindow_ind]
            xf = checkpointed_states[subwindow_ind + 1]

            # Evaluate observation innovation at t1 (if an observation exist)
            tobs = self.find_observation_time(subwindow[1])
            if tobs is not None:
                if self.debug:
                    print(f"DEBUG: Updating adjoint with observation at time: {tobs}")
                y = self.observations[tobs][self.observation_operator.extended_design]

                # Data-misfit
                innov = self.observation_operator(xf) - y  # H(x) - y
                innov = self.observation_error_model.covariance_inv_matvec(
                    innov
                )  # R^{-1} (H(x)-y)
                adjoint += self.observation_operator.Jacobian_T_matvec(innov, eval_at=xf)
                # loop backward over the subwindow, and apply model jacobian transpose at all time steps in the subwindow
            else:
                raise ValueError(
                    "This should never happen; we make sure the checkpoints match"
                    " observation times!"
                )

            local_checkpoints, local_traject = self.model.integrate_state(
                x0.copy(), tspan=subwindow, checkpoints=None,
            )
            for i in range(len(local_traject) - 1, 0, -1):
                adjoint = self.model.Jacobian_T_matvec(
                    adjoint,
                    eval_at=local_traject[i],  # TODO: check eval_at
                    eval_at_t=local_checkpoints[i],
                    dt=local_checkpoints[i] - local_checkpoints[i - 1],
                )

        misfit_term = adjoint
        # Finally check if there is an observation at the initial time
        tobs = self.find_observation_time(t0)
        if tobs is not None:
            x0 = state  # the passed (initial) state
            y = self.observations[tobs][self.observation_operator.extended_design]
            innov = self.observation_operator(x0) - y  # H(x) - y
            innov = self.observation_error_model.covariance_inv_matvec(innov)
            misfit_term += self.observation_operator.Jacobian_T_matvec(innov, eval_at=x0)

        # Regularize the gradient with the prior term
        if data_misfit_only:
            prior_term = 0
        else:
            prior_term = self.prior.covariance_inv_matvec(state - self.prior.mean)

        # Add prior and data-misfit terms
        gradient = prior_term + misfit_term

        return gradient


    def apply_forward_operator(
        self,
        state,
        save_states=False,
        checkpoints=None,
        scale_by_noise=True,
    ):
        """
        Apply :math:`F`, the forward operator to the passed state.  The forward operator
        :math:`F` here, refers to the simulation model followed by the observation
        operator.  The result is a data point (an observation), or a dictionary of
        observations indexed by the time (for time-dependent models)

        For time-dependent simulations with multiple observation points (e.g., in 4D-Var
        settings), the observations are evaluated at the passed checkpoints (if they
        correspond a registered observation time) If there is an observation at the
        initial time, the forward operator at the initial time corresponds to applying
        the observation operator without applying model simulation/dynamics.

        :param state: data structure holding the model state at the initial time of the
            registered assimilation window
        :param checkpoints: times at which to store the computed observations, must be
            sorted and lie within the registered window.  If None (default), use the
            registered observation times
        :param save_states: if `True` states will be checkpointed and saved as well as
            observations
        :param bool scale_by_noise: if True, the observations are scaled by the
            observation error precision

        :returns:
            - `checked_observations` a dictionary holding times as keys, and
              observations as values.  The observations result from forward integration
              of the simulation model, and applying observation operator (with/without)
              scaling by observation error precisions
            - `checked_states`: a dictionary holding model states evaluated at the
              checkpoints; set to None if `save_states` is `False`, otherwise
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.window is None,
                self.observation_operator is None,
                False if scale_by_noise else (self.observation_error_model is None),
                self.observation_times is None,
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Start with t0, and propagate the model forward (over checkpoints)
        t0 = self.window[0]
        obs_tspan = self.observation_times

        if len(obs_tspan) < 1:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )

        # validate initial observation time and create checkpoints with t0 + all observation times
        if (t0 - obs_tspan[0]) > self._TIME_EPS:
            raise AssertionError(
                "An observation is registered outside the assimilation window!"
                "This should never happen"
            )

        # Validate the passed checkpoints
        if checkpoints is None:
            checkpoints = obs_tspan
        else:
            checkpoints = [t for t in checkpoints]
            checkpoints.sort()
            if (t0 - checkpoints[0]) > self._TIME_EPS:
                raise AssertionError(
                    "An observation is requested before the initial time of the"
                    " assimilation window!"
                )

        # Validate the passed model state
        self._check_init_guess(state)

        # Initialize state, state time, and checked observations
        flt_state = state.copy()
        flt_time = t0
        checked_observations = dict()
        checked_states = {t0: flt_state.copy()} if save_states else None

        # Loop over all checkpoints, and evaluate the observation(s)
        for t in checkpoints:
            # check if an observation is requested at the initial time
            if abs(t - t0) <= self._TIME_EPS:
                y = self.observation_operator(flt_state)
                if scale_by_noise:
                    y = self.observation_error_model.covariance_inv_matvec(y)
                checked_observations.update({t: y.copy()})
            elif t > flt_time:
                # propagate state from flt_time to t, and overwrite
                _, local_traject = self.model.integrate_state(
                    state=flt_state,
                    tspan=[flt_time, t],
                    checkpoints=[flt_time, t],
                )
                flt_state = local_traject[-1]
                flt_time = t
                y = self.observation_operator(flt_state)
                if scale_by_noise:
                    y = self.observation_error_model.covariance_inv_matvec(y)
                checked_observations.update({t: y.copy()})
                if save_states:
                    checked_states.update({t: flt_state.copy()})
            else:
                raise ValueError(
                    "Impossible: checkpoint/time is before the begining of the window!"
                )

        return checked_observations, checked_states

    def apply_forward_operator_adjoint(
        self,
        obs,
        obs_time,
        eval_at=None,
        checkpointed_states=None,
        checkpointed_states_times=None,
        save_all=False,
        scale_by_noise=True,
    ):
        """
        Apply F^*, the adjoint of the forward operator, to the passed observation at the
        given observation time `obs_time`.  Here, F is a composition of the simulation
        model and the observation operator (e.g., simulation followed by restriction)
        applied at all observation times.  Thus, the adjoint is applied recursively
        backward in time.  If the passed `obs_time` coincides with the initial time of
        the assimilation window, the adjoint corresponds to applying the adjoint of the
        observation operator only, and the model dynamics in this case correspond to
        applying an identiy operator.

        :param obs: the vector in observation space to apply the adjoint to
        :param obs_time: time at which obs it given, if no observation time is given, it
            is set to the last registered observation time
        :param bool save_all: (default `False`) if True, adjoint evaluated at all time
            points is returned as a dictionary indexed by observation times, otherwise,
            only resulting adjoint at the initial time of the assimilation window.
        :param bool scale_by_noise: if True, the observations are scaled by the
            observation error precision
        :param eval_at: state to linearize the forward operator around, used if
            checkpointed states are not given to checkpoint forward trajectory
        :param checkpointed_states: trajectory of states evaluated at all checkpoints
            (t0 + observation times),
        :param checkpointed_states_times: times at which checkpointed_states are given
            This is useful to reduce redundancy in the optimization procedure.  Both
            `checkpointed_states` and `checkpointed_states_times` are ignored for linear
            problems as they are not needed for adjoint evaluation

        :returns: `adjoint`, `checked_adj` where:

            - `adjoint`: the adjoint evaluated at the initial time of the assimilation
              window
            - `checked_adj` a dictionary (indexed by times) of checkpointed states
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.window is None,
                self.observation_operator is None,
                False if scale_by_noise else (self.observation_error_model is None),
                self.observation_times is None,
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Start with t0, and propagate the model forward (over checkpoints)
        t0 = self.window[0]
        obs_tspan = self.observation_times

        if len(obs_tspan) < 1:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )

        # validate initial observation time and create checkpoints with t0 + all observation times

        # local copy of the observation
        if not self.observation_operator.is_observation_vector(obs):
            raise TypeError(
                f"Invalid observation\n"
                f"Received {obs=} of {type(obs)=};\n"
                f"{self.observation_operator.shape=}"
            )
        y = obs.copy()
        if self.debug:
            print(
                f"\nDEBUG: inside `apply_forward_operator_adjoint`; \n"
                f"Apply adjoint from time: {obs_time}\n"
                f"DEBUG: Input Y: {str(y)}"
            )

        # Extract and validate times
        if len(obs_tspan) == 1 and abs(t0 - obs_tspan[0]) < self._TIME_EPS:
            raise AssertionError("One observation at initial time; use 3D-Var!")

        elif len(obs_tspan) == 0:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )
        elif (t0 - obs_tspan[0]) > self._TIME_EPS:
            raise AssertionError(
                "An observation is registered outside the assimilation window! "
                "This should never happen"
            )

        elif (t0 - obs_tspan[0]) < -self._TIME_EPS:
            all_checkpoints = [t0] + obs_tspan
        else:
            all_checkpoints = obs_tspan
        all_checkpoints = np.asarray(all_checkpoints)
        # Validate checkpoints
        if obs_time is None:
            obs_time = obs_tspan[-1]
        all_checkpoints = all_checkpoints[all_checkpoints <= obs_time]

        def update_checkpoints():
            """Local function to recheck states"""
            if self.verbose:
                print(
                    "Invalid checkpointed states and/or checkpointed times\n"
                    "Some necessary times/states are not found\n"
                    "Distroy checkpointed states, and reconstruct properly"
                )
            if eval_at is not None:
                # If end and beginning time match, the eval_at state is copied
                if all_checkpoints[0] == all_checkpoints[-1]:
                    checkpointed_states_times = [all_checkpoints[0]]
                    checkpointed_states = [eval_at.copy()]
                else:
                    (
                        checkpointed_states_times,
                        checkpointed_states,
                    ) = self.model.integrate_state(
                        eval_at,
                        tspan=(all_checkpoints[0], all_checkpoints[-1]),
                        checkpoints=all_checkpoints,
                    )
            else:
                raise TypeError(
                    "Cannot recheck states without eval_at! found eval_at type: None"
                )

            return checkpointed_states_times, checkpointed_states

        # Checkpointed states are needed if the model is nonlinear; use this to chek if states are required
        try:
            self.observation_operator.Jacobian_T_matvec(obs, eval_at=None)
            self.model.Jacobian_T_matvec(
                self.model.state_vector(),
                eval_at=None,
                eval_at_t=all_checkpoints[0],
            )
            # Model and observation operator are both linear
            nonlinear_problem = recheck = False

            # Fill up/correct checkpointed states & checkpointed states times
            if checkpointed_states_times is None:
                checkpointed_states_times = all_checkpoints
            elif len(checkpointed_states_times) != len(all_checkpoints):
                checkpointed_states_times = all_checkpoints
            else:
                pass

            if checkpointed_states is None:
                checkpointed_states = [None] * len(all_checkpoints)
            elif len(checkpointed_states) != len(all_checkpoints):
                checkpointed_states = [None] * len(all_checkpoints)
            else:
                pass

        except:
            # Model and/or observation operator are/is nonlinear
            nonlinear_problem = True

            # Recheck if needed
            if checkpointed_states_times is None or checkpointed_states is None:
                recheck = True
            elif len(checkpointed_states_times) != len(checkpointed_states):
                recheck = True
            else:
                # TODO: Consider adding more assertions/checks
                recheck = False

        if self.debug:
            print(f"DEBUG: Rechecking states? {recheck}")

        if recheck:
            checkpointed_states_times, checkpointed_states = update_checkpoints()

        if self.verbose:
            print("Adjoint Step: backward integration of the adjoint...")

        # Adjoint step: Loop over checkpoints, and evaluate the observation-term update by applying Jacobian
        # Initialize the adjoint (from observation at teh end of the window (or checkpoints))
        if scale_by_noise:
            y = self.observation_error_model.covariance_inv_matvec(y)

        if self.debug:
            print("DEBUG: Scaled (Rinv) Y: ", y)
        adjoint = self.observation_operator.Jacobian_T_matvec(y, eval_at=checkpointed_states[0])

        checked_adj = (
            dict({checkpointed_states_times[-1]: adjoint.copy()}) if save_all else None
        )

        if self.debug:
            print("DEBUG: Initial adjoint: Ht Rinv Y: ", adjoint)

        if len(all_checkpoints) > 1:
            # Loop backward to propagate adjoint over subwindows
            for subwindow_ind in [i for i in range(len(all_checkpoints) - 1)][::-1]:
                # Extract window times and checkpointed states (observation times)
                t0, tf = (
                    all_checkpoints[subwindow_ind],
                    all_checkpoints[subwindow_ind + 1],
                )
                subwindow = (t0, tf)
                if self.debug:
                    print(
                        f"DEBUG: Adjoinrtg propagation over subwindow: [t0, tf]=[{t0},"
                        f" {tf}]"
                    )

                # Double check that a checkpointed observation time exists (even if None for linear problems)
                _tind = np.where(abs(checkpointed_states_times - t0) <= self._TIME_EPS)[
                    0
                ]
                if _tind.size != 1:
                    raise AssertionError(
                        "This is not supposed to happen! More than one state is"
                        f" checked at time {t0}"
                    )

                # Get a checked state (if any; for nonlinear problems)
                x0 = checkpointed_states[_tind[0]]
                if x0 is not None:
                    # Since a state is checkpointed, we build a subtrajectory and apply the adjoint recursively on it
                    # forward
                    local_checkpoints, local_traject = self.model.integrate_state(
                        x0,
                        tspan=(t0, tf),
                        checkpoints=None,  # simulate at all simulation time-steps
                    )
                    # backward
                    for i in range(len(local_traject) - 1, 0, -1):
                        adjoint = self.model.Jacobian_T_matvec(
                            adjoint,
                            eval_at=local_traject[i - 1],
                            eval_at_t=local_checkpoints[i - 1],
                            dt=local_checkpoints[i] - local_checkpoints[i - 1],
                        )
                        if self.debug:
                            print(
                                f"i={i}; adjoint over"
                                f" {(local_checkpoints[i - 1], local_checkpoints[i])}"
                            )
                            print("Adjoint is: ", adjoint)

                else:
                    local_checkpoints, _ = self.model.integrate_state(
                        self.model.state_vector(),
                        tspan=(t0, tf),
                        checkpoints=None,
                    )

                    # Not checkpointed states
                    for i in range(len(local_checkpoints) - 1, 0, -1):
                        adjoint = self.model.Jacobian_T_matvec(
                            adjoint,
                            eval_at=None,
                            eval_at_t=local_checkpoints[i - 1],
                        )
                    if self.debug:
                        print(
                            "Adjoint evaluation for linear model; number of"
                            f" applications {len(local_checkpoints)-1}"
                        )
                        print(f"Adjoint over {(subwindow[0], subwindow[1])}")
                        print("Adjoint is: ", adjoint)

                # Update checked adjoints (if requested)
                if save_all:
                    checked_adj.update({t0: adjoint.copy()})
                if self.debug:
                    print("DEBUG: adjoint, checked_adj: ", adjoint, checked_adj)

        return adjoint, checked_adj

    def full_Hessian(self, eval_at=None, data_misfit_only=False, ):
        """Construct the Hessian by evaluating the tangent linear model"""
        Hessian = np.zeros((self.prior.size, self.prior.size))
        Rinv = self.observation_error_model.precision_matrix()

        for t in self.observation_times:
            print(f"Evaluating at t: {t}")
            Fadj = utility.asarray(
                lambda x: self.apply_forward_operator_adjoint(
                    x,
                    obs_time=t,
                    eval_at=eval_at,
                    scale_by_noise=False,
                )[0],
                shape=(self.prior.size, self.observation_operator.shape[0])
            )
            Hessian += Fadj @ Rinv @ Fadj.T

        if not data_misfit_only:
            Hessian += self.prior.precision_matrix()
        return Hessian

    def full_Hessian_inv(self, eval_at=None, data_misfit_only=False, ):
        """
        Construct the inverse of the Hessian by evaluating the tangent linear model
        """
        return np.linalg.inv(
            self.full_Hessian(
                eval_at=eval_at,
                data_misfit_only=data_misfit_only,
            )
        )

    def posterior_covariance(self, eval_at=None, data_misfit_only=False, ):
        """
        Construct the inverse of the Hessian by evaluating the tangent linear model
        """
        cov = utility.asarray(
            lambda x: self.Hessian_inv_matvec(
                x,
                eval_at=eval_at,
                data_misfit_only=data_misfit_only,
            ),
            shape=(self.prior.size, self.prior.size),
        )
        return cov

    def Hessian_matvec(
        self,
        state,
        eval_at=None,
        data_misfit_only=False,
    ):
        """
        Return the product of the Hessian of 4D-Var objective with a vector `state`.

        :param state: initial state/guess of the model state/parameter
        :param eval_at: state at which the Hessian is evaluated (passed to the model adjoint)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded, and only
            the data-misfit term is evaluated

        :returns: product of the inverse of the posterior covariance (of the linearized problem)
            with a state vector

        :notes: This method fixes and replace old code now moved to `_incorrect_Hessian_matvec`
        """
        # return self.full_Hessian(eval_at=eval_at).dot(state)
        if self.verbose:
            print(
                "\n***Inside Hessian MatVec ***\n "
                "Passed {state=} of {type(state)=}"
            )

        ## Validate Tspan, and observation times
        t0, _ = self.window
        obs_tspan = self.observation_times
        if len(obs_tspan) == 1 and abs(t0 - obs_tspan[0]) < self._TIME_EPS:
            raise AssertionError("One observation at initial time; use 3D-Var!")

        elif len(obs_tspan) < 1:
            raise AssertionError(
                "At least one observations must be registered for 4D-Var"
            )

        elif (t0 - obs_tspan[0]) > self._TIME_EPS:
            raise AssertionError(
                "An observation is registered outside the assimilation window!"
                "This should never happen"
            )

        elif (t0 - obs_tspan[0]) < -self._TIME_EPS:
            checkpoints = [t0] + obs_tspan

        else:
            checkpoints = obs_tspan

        # Check the passed state
        self._check_init_guess(state)

        # Square root of the observation error precision (covariance inverse)
        # Note; when OED is utilized, the design changes observation_error_model and
        #   active_size is used instead of size; thus to get the observation size,
        #   we should rely on the observation operator
        # observation_size = self.observation_error_model.active_size
        observation_size = self.observation_operator.shape[0]
        Rinvsqrt = utility.asarray(
            self.observation_error_model.covariance_sqrt_inv_matvec,
            shape=(observation_size, observation_size),
        )

        # Square root of the data-misfit term of the Hessian (state size x observation size)
        data_Hessmat_sqrt = np.empty((self.model.state_size, observation_size))

        # TODO: The following can be factored to allow recursion  and fast matrix-matrix computation
        # Each column of Rinvsqrt is propagated backward (through the model adjoint) to the initial time
        out_state = np.zeros_like(state)
        for tobs in checkpoints:
            data_Hessmat_sqrt[...] = 0.0
            for j in range(observation_size):
                adjoint, _ = self.apply_forward_operator_adjoint(
                    obs=Rinvsqrt[:, j],
                    eval_at=eval_at,
                    obs_time=tobs,
                    save_all=False,
                    scale_by_noise=False,
                )
                data_Hessmat_sqrt[:, j] = adjoint
            out_state += data_Hessmat_sqrt.dot(data_Hessmat_sqrt.T.dot(state))

        # Add prior term if requested, and return
        if not data_misfit_only:
            out_state += self.prior.covariance_inv_matvec(state)

        return out_state

    def Hessian_inv_matvec(
        self,
        state,
        eval_at=None,
        data_misfit_only=False,
        precond=None,
        solver="scipy-minres",
        solver_options={"maxiter": None, "callback": None},
    ):
        """
        Return the product of the Hessian inverse of 4D-Var objective with a vector 'state'

        :param state: initial state/guess of the model state/parameter
        :param eval_at: state at which the Hessian is evaluated (passed to the model adjoint)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded, and only
            the data-misfit term is evaluated

        :returns: product of the posterior covariance (of the linearized problem)
            with a state vector

        :remarks: this function is very important and resembles the most epensive part of
            linear/linearized Bayesian algorithms. Preconditioning, and/or reduced-order
            approxmiations are very important for performance here.
        """
        # Slice the passed parameter/state
        state = utility.asarray(state).flatten()

        # Check eval_at for linearization if not passed
        if eval_at is None:
            if self.posterior is not None:
                eval_at = self.posterior.mean
            else:
                eval_at = self.prior.mean

        # Define a linear operator A for Hessian_mat_vec application
        matvec = lambda x: self.Hessian_matvec(
            x,
            eval_at=eval_at,
            data_misfit_only=data_misfit_only,
        )
        A = splinalg.LinearOperator(
            (state.size, state.size),
            matvec=matvec,
        )

        # Retrieve the preconditioner if given
        if precond is not None:
            M = splinalg.LinearOperator(
                (state.size, state.size), matvec=lambda p: precond(eval_at, p)
            )
        else:
            M = None

        # Define an Elementary callback
        callback = solver_options["callback"] if "callback" in solver_options else None

        def report(xk):
            frame = inspect.currentframe().f_back
            try:
                print(f"Solver-Iteration; Residual-Norm: {frame.f_locals['r_norm']}")
            except KeyError:
                pass

        callback = report if callback is None and self.verbose else None
        solver_options.update({"callback": callback})

        if self.debug:
            print("DEBUG: 3DVAR: Preconditioner:...", M)

        # Start solving the linear system H x = b, where x is the passed state and H is the Hessian
        if re.match(r"\A(sp|scipy)( |-|_)*minres\Z", solver, re.IGNORECASE):
            out = splinalg.minres(A=A, b=state, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*cg\Z", solver, re.IGNORECASE):
            out = splinalg.cg(A=A, b=state, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*bicgstab\Z", solver, re.IGNORECASE):
            out = splinalg.bicgstab(A=A, b=state, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*gmres\Z", solver, re.IGNORECASE):
            out = splinalg.gmres(A=A, b=state, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*lgmres\Z", solver, re.IGNORECASE):
            out = splinalg.lgmres(A=A, b=state, M=M, **solver_options)

        else:
            raise ValueError("Unsupported linear-system solver : '{0}'!".format(solver))

        if out[1] == 0:
            # Success; all good and converged
            out = out[0]

        elif out[1] > 0:
            warnings.warn(
                f"Solver {solver} did not achieve toleranc {solver_options['tol']}"
            )
            out = out[0]

        else:
            msg = ""
            if np.any(np.isnan(state)) or np.any(np.isinf(state)):
                msg += f"Found Inf/Nan values in the right-hand side!\n"
            msg += (
                f"Failed to apply {solver} to solve the linear system involving the"
                " Hessian matrix"
            )
            msg += f":INPUT; rhs: {repr(state[:])}\nOUTPUT of {solver}: {repr(out)}\n"
            raise ValueError(msg)

        return out


    @property
    def supported_optimizers(self, ):
        return self._SUPPORTED_OPTIMIZERS

