# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This utility mnodule provides implementations to aid in reducing the computaitonal
cost of solving data assimilation and inverse problems.
This includes, reduced order modeling and randmization for matrix-vector approximations, etc.
"""
from abc import ABC, abstractmethod

import numpy as np
from scipy.sparse.linalg import LinearOperator, isolve, spsolve_triangular, svds

from pyoed.assimilation.filtering import VariationalFilter
from pyoed.assimilation.smoothing import VariationalSmoother
from pyoed.utility.mixins import RandomNumberGenerationMixin


class ReducedHessian(ABC):
    """
    Base class implementing Reduced-Order approximation of the Hessian of the negative-log-posterior function in DA.i
    """

    @abstractmethod
    def array(self):
        """Return an array representation of the reduced-order Hessian"""
        ...

    @abstractmethod
    def array_inverse(self):
        """Return an array representation of the the inverse of the reduced-order Hessian"""
        ...

    @abstractmethod
    def matvec(self):
        """Return the matrix-vector product of the reduced-order Hessian by a vector"""
        ...

    @abstractmethod
    def initialize(self):
        """
        Re-Create/Re-Initialize the reduced-order Hessian
        """
        ...


class SVDReducedHessian(ReducedHessian, RandomNumberGenerationMixin):
    """
    Reduced-Order Approximation of the Hessian (of the posterior negative-log) using standard Singular Value Decomposition.
    This can be used to speed up inverse applications, but it still is expensive to carry out.
    Yet, it is valid for Hessian with singular values those do not decay quickly.

    :param inverse_problem: the inverse problem to use (a variational filter or smoother)
        e.g., a 4D-Var smoother instance
    :param int rank_p: requested rank of the reduced order Hessian approximation (Number of singular values to keep.)
        If `None`, all singular values are kept (Full Hessian).

    :param bool data_misfit_only:
    :param eval_at: point to approximate/linearize the Hessian at (optional; discarded for linear problems)
    :param bool verbose: flag to control screen output verbosity

    :returns: an instance of :py:class:`NystromRandomizedHessian`
    """

    def __init__(
        self,
        inverse_problem,
        rank_p=50,
        data_misfit_only=False,
        eval_at=None,
        random_seed=None,
        verbose=False,
    ):
        assert isinstance(
            inverse_problem, (VariationalFilter, VariationalSmoother)
        ), "the passed inverse problem object must be derived from VariationalFilter or VariationalSmoother!"
        if rank_p is not None:
            assert (
                rank_p == int(rank_p) and rank_p >= 1
            ), "rank_p must be a positive integer!"
        self._INVERSE_PROBLEM = inverse_problem
        self._LINEARIZE_AT = eval_at
        self._DATA_MISFIT_ONLY = bool(data_misfit_only)
        self._VERBOSE = bool(verbose)

        _s = self._INVERSE_PROBLEM.model.state_vector()[:]
        try:
            _p = self._INVERSE_PROBLEM.model.parameter_vector()[:]
        except:
            raise
            _p = None

        try:
            self._INVERSE_PROBLEM.Hessian_matvec(
                _s, eval_at=eval_at, data_misfit_only=True
            )
            self._STATE_SIZE = _s.size
        except:
            try:
                if _p is None:
                    print("Tried creating parameter vector but failed...")
                    raise TypeError
                self._INVERSE_PROBLEM.Hessian_matvec(
                    _p, eval_at=eval_at, data_misfit_only=True
                )
                self._STATE_SIZE = _p.size
            except:
                print(
                    "Failed to figure out the size of the Hessian! Neither state nor parameter sizes work!"
                )
                raise
        self._RANK_P = rank_p if rank_p is not None else self._STATE_SIZE
        self.initialize(
            Hessian_matvec=self._INVERSE_PROBLEM.Hessian_matvec,
            eval_at=eval_at,
            data_misfit_only=data_misfit_only,
            verbose=verbose,
        )

        # Update/Reset the underlying random number generator
        self.update_random_number_generator(random_seed=random_seed)

    def initialize(
        self, Hessian_matvec=None, eval_at=None, data_misfit_only=None, verbose=False
    ):
        """
        Use the associated settings to generate/update the POD reduced-order approximation of the Hessian
        All the arguments here are optional, and if not explicitly provided, are replaced with internal values set upon initilization (or if modified explicitly) check properties

        :param Hessian_matvec:
        :param eval_at:
        :param data_misfit_only:
        """
        sep = "*" * 50 + "\n"
        if self.verbose:
            print("\n{0}Evaluating the SVD Reduced-order Hessian\n{0} ".format(sep))

        # Get pointers to the underlying inverse_problem
        inverse_problem = self._INVERSE_PROBLEM
        rank_p = self.rank_p
        state_size = self.state_size

        # Hessian matvec evaluator
        if eval_at is None:
            eval_at = self.linearize_at
        else:
            self._LINARIZE_AT = eval_at

        if data_misfit_only is None:
            data_misfit_only = self.data_misfit_only
        else:
            self._DATA_MISFIT_ONLY = data_misfit_only
        if Hessian_matvec is None:
            Hessian_matvec = inverse_problem.Hessian_matvec
        full_H_matvec = lambda x: Hessian_matvec(
            x, eval_at=eval_at, data_misfit_only=data_misfit_only
        )

        # Carry out SVD Decomposition
        if self.verbose:
            print("Singular value decomposition of the Hessian")
        if self.rank_p == state_size:
            if verbose:
                print("SVD with all Singular Values Kept; i.e.,  exact SVD...")
            # Full SVD
            # Construct H, then apply full SVD
            e = np.empty(state_size)
            H = np.empty((state_size, state_size))
            for i in range(state_size):
                if verbose:
                    print(f"\rColumn {i}/{state_size}", end="")
                e[:] = 0.0
                e[i] = 1.0
                H[:, i] = full_H_matvec(e)
            if verbose:
                print("")

            # Apply full SVD
            U, s, Vh = np.linalg.svd(H, hermitian=True)

        else:
            if verbose:
                print(f"Keeping Largest {rank_p}/{state_size} Singular Values Only...")
            # Reduced-Order SVD
            A = LinearOperator(
                (state_size, state_size), matvec=full_H_matvec, rmatvec=full_H_matvec
            )
            # TODO: make the solver an optional argument to give more flexibility
            U, s, Vh = svds(
                A, k=rank_p, solver="arpack"
            )  # default solver is 'arpack'; both 'arpack' and 'lobpcg' are supported

        # Associate Singular Values/Vectors to self
        self._U = U
        self._S = s
        self._Vh = Vh
        # self._Vh = U.T  # Make sure it is symmetric
        # print("DEBUG in SVDReduce: U~V", np.allclose(U, Vh.T))
        # TODO: add tests for verification if needed!
        if self.verbose:
            print("DONE. Reduced-Hessian is ready...\n{0}\n".format(sep))

    def array(self, eval_at=None, data_misfit_only=False, verbose=False):
        """Construct an array repersentatiton of the reduced Hessian"""
        rH = self._Vh.copy()
        for i, s in enumerate(self._S):
            rH[i, :] *= s
        rH = np.dot(self._U, rH)
        return rH

    def array_inverse(self, eval_at=None, data_misfit_only=False, verbose=False):
        """Construct an array repersentatiton of the reduced Hessian"""
        rHinv = self._U.copy().T
        for i, s in enumerate(self._S):
            rHinv[i, :] /= s
        rHinv = np.dot(self._Vh.T, rHinv)
        return rHinv

    def matvec(self, state, eval_at=None, data_misfit_only=None, verbose=False):
        """
        Return the product of the Reduced-Hessian-Approxmiation of 4D-Var objective
            (inverse of posterior covariance) with a vector 'state'.

        :param state: initial state/guess of the model state/parameter
        :param eval_at: state at which the Hessian is evaluated
            (passed to the model adjoint)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded,
            and only the data-misfit term is evaluated
        :param bool verbose: flag to control screen output verbosity

        :returns: estimate of the product of the inverse of the posterior covariance
            (of the linearized problem) with a state vector
        """
        # Hessian matvec evaluator
        if eval_at is None:
            eval_at = self.linearize_at
        if data_misfit_only is None:
            data_misfit_only = self.data_misfit_only

        reset = True
        if (
            eval_at is self.linearize_at is None
            and data_misfit_only == self.data_misfit_only
        ):
            reset = False
        elif (
            eval_at is not None
            and self.linearize_at is not None
            and data_misfit_only == self.data_misfit_only
        ):
            if np.allclose(eval_at[:], self.linearize_at):
                reset = False
        if reset:
            self.initialize(data_misfit_only=data_misfit_only, eval_at=eval_at)

        # local copy of the passed state
        x = state[:].copy()

        out = np.dot(self._U, self._S * self._Vh.dot(x))
        return out

    # Aliases
    apply = Hessian_matvec = matvec

    def inv_matvec(self, state, eval_at=None, data_misfit_only=None, verbose=False):
        """
        Return the product of the Inverse of the Reduced-Hessian-Approxmiation of 4D-Var objective
            (inverse of posterior covariance) with a vector 'state'.

        """
        # Hessian matvec evaluator
        if eval_at is None:
            eval_at = self.linearize_at
        if data_misfit_only is None:
            data_misfit_only = self.data_misfit_only

        reset = True
        if (
            eval_at is self.linearize_at is None
            and data_misfit_only == self.data_misfit_only
        ):
            reset = False
        elif (
            eval_at is not None
            and self.linearize_at is not None
            and data_misfit_only == self.data_misfit_only
        ):
            if np.allclose(eval_at[:], self.linearize_at):
                reset = False
        if reset:
            self.initialize(data_misfit_only=data_misfit_only, eval_at=eval_at)

        # local copy of the passed state
        x = state[:].copy()

        out = np.dot(self._Vh.T, self._U.T.dot(x) / self._S)
        return out

    # Add alias
    Hessian_inv_matvec = inv_matvec

    def test_accuracy(self, sample_size=10, verbose=False, test_inverse_Hessian=True):
        """
        Test the accuracy of the constructed reduced-order Hessian approximation
        1- Generate random vectors and evaluate Hessian matvec and Hessian inv matvec (both full and reduced order approxiamtion)
        2- show infinity norm of the difference magnitude;

        By default, this function tests the accuracy of the Hessian-inverse matrix vector product. set `test_inverse_Hessian` to test Hessian instead of the inverse
        """

        # Get pointers to the underlying inverse_problem
        inverse_problem = self._INVERSE_PROBLEM
        state_size = self.state_size

        # Hessian matvec evaluator
        eval_at = self.linearize_at
        data_misfit_only = self.data_misfit_only
        if test_inverse_Hessian:
            full_H_matvec = lambda x: inverse_problem.Hessian_inv_matvec(
                x, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=False
            )
            reduced_H_matvec = lambda x: self.Hessian_inv_matvec(
                x, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=False
            )
        else:
            full_H_matvec = lambda x: inverse_problem.Hessian_matvec(
                x, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=False
            )
            reduced_H_matvec = lambda x: self.Hessian_matvec(
                x, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=False
            )

        linf_errs = np.empty(
            sample_size
        )  # infinity norm of ( H x - rH x ), with H full and rH reduced Hessians
        l2_errs = np.empty(sample_size)  # L2 norm of ( Hx - rHx )
        wl2_errs = np.empty(sample_size)  # | x H x - x rH x |
        if verbose:
            print(
                f"Testing Accuracy of the reduced-order Hessian using {sample_size} random states"
            )

        for i in range(sample_size):
            if verbose:
                print(f"\rTesting {i+1}/{sample_size}", end="")
            state = self.random_number_generator.standard_normal(state_size)
            yf = full_H_matvec(state)
            yr = reduced_H_matvec(state)

            linf_errs[i] = np.linalg.norm(yf - yr, ord=np.infty)
            l2_errs[i] = np.linalg.norm(yf - yr, ord=2)
            wl2_errs[i] = np.abs(state.dot(yf) - state.dot(yr))
        if verbose:
            print()

        print(
            "Reduced-Order validation (Maximum Errors for randomly-generated vectors):"
        )
        print(f"| H x - rH x |_inf : {linf_errs.max()}")
        print(f"| H x - rH x |_2   : {l2_errs.max()}")
        print(f"| x H x - x rH x | : {wl2_errs.max()}")

    @property
    def inverse_problem(self):
        """
        Pointer to the underlying inverse_problem (which gives access to all other elements; model, error models, observation operator, etc.)
        """
        return self._INVERSE_PROBLEM

    @property
    def verbose(self):
        """
        Evaluate the data-misfit term of the Hessian only
        """
        return self._VERBOSE

    @verbose.setter
    def verbose(self, value):
        """ """
        self._VERBOSE = bool(value)

    @property
    def data_misfit_only(self):
        """
        Evaluate the data-misfit term of the Hessian only
        """
        return self._DATA_MISFIT_ONLY

    @data_misfit_only.setter
    def data_misfit_only(self, value):
        """ """
        self._DATA_MISFIT_ONLY = bool(value)

    @property
    def state_size(self):
        """ """
        return self._STATE_SIZE

    @property
    def rank_p(self):
        """ """
        return self._RANK_P

    @rank_p.setter
    def rank_p(self, value):
        """ """
        raise NotImplementedError("TODO")

    @property
    def linearize_at(self):
        """ """
        return self._LINEARIZE_AT

    @linearize_at.setter
    def linearize_at(self, value):
        """ """
        raise NotImplementedError("TODO")


class NystromRandomizedHessian(ReducedHessian, RandomNumberGenerationMixin):
    """
    An implementation that enables creating randomized approximation for a forward
    model once, and thus enable reutilizing it efficiently

    :param inverse_problem: the inverse problem to use (a variational filter or smoother)
        e.g., a 4D-Var smoother instance
    :param int rank_p: requested rank of the reduced order Hessian approximation
    :param int over_sampling:
    :param bool data_misfit_only:
    :param bool verbose: flag to control screen output verbosity

    :returns: an instance of :py:class:`NystromRandomizedHessian`
    """

    def __init__(
        self,
        inverse_problem,
        rank_p=20,
        over_sampling=15,
        data_misfit_only=True,
        random_seed=None,
        verbose=False,
    ):
        assert isinstance(
            inverse_problem, (VariationalFilter, VariationalSmoother)
        ), "the passed inverse problem object must be derived from VariationalFilter or VariationalSmoother!"
        assert (
            rank_p == int(rank_p) and rank_p >= 1
        ), "rank_p must be a positive integer!"
        assert (
            over_sampling == int(over_sampling) and over_sampling >= 1
        ), "over_sampling must be a positive integer!"
        self._INVERSE_PROBLEM = inverse_problem
        raise NotImplementedError(
            "TODO: Update this; we need to enable parameter or state size based on the inversion size!"
        )
        self._STATE_SIZE = self._INVERSE_PROBLEM.model.state_vector().size
        self._DATA_MISFIT_ONLY = bool(data_misfit_only)
        self._RANK_P = rank_p
        self._OVER_SAMPLING = over_sampling
        self._VERBOSE = bool(verbose)
        self.initialize()

        # Update/Reset the underlying random number generator
        self.update_random_number_generator(random_seed=random_seed)

    def initialize(self):
        """
        Use the associated settings to generate/update the randomized approximation of the Hessian
        """
        sep = "*" * 50 + "\n"
        if self.verbose:
            print("\n{0}Evaluating the Nystrom Reduced-order Hessian\n{0} ".format(sep))

        # TODO: Discuss; how to utilize rank_p and over_sampling properly?
        rank_p = self.rank_p
        over_sampling = self.over_sampling

        # Total number of random vectors
        total_random_vectors = rank_p + over_sampling

        # Get pointers to the underlying inverse_problem the simulation model, and prior
        inverse_problem = self._INVERSE_PROBLEM
        model = inverse_problem.model
        prior = inverse_problem.prior

        if self.verbose:
            print("Sampling Normally-distributed Random Matrix 'Omega'")
            print("Total number of random vectors: {0}".format(total_random_vectors))

        # Sample a standard normal distribution with each random sample set as a column
        Omega = self.random_number_generator.standard_normal(
            (self._STATE_SIZE, total_random_vectors)
        )

        if self.verbose:
            print("Evaluating the randomized prior-preconditioned GN-Hessian")

        # TODO: discuss: I don't think utilization of data_misfit is correct here
        data_misfit_only = self.data_misfit_only
        # Computing the Hessian-Vector product of the misfit below
        # Loop over columns of Omega, extract columns, apply Hessian (TODO: utilize GN-Hessian)
        # to the vector
        Y = np.empty_like(
            Omega
        )  # Y = H Omega; H: prior-preconditioned data misfit Hessian
        for i in range(total_random_vectors):
            x = Omega[:, i]
            Bx = prior.covariance_sqrt_matvec(x, lower=True)
            # print(f"DEBUG: x.size: {x.size}\nBx.size: {Bx.size}")
            var1 = inverse_problem.Hessian_matvec(
                state=Bx, data_misfit_only=True
            )  # TODO: this works for linear models! otherwise, we need eval_at as well!
            Y[:, i] = prior.covariance_sqrt_matvec(var1, lower=True)

        if self.verbose:
            print("QR decomposition of the prior-preconditioned GN-Hessian")

        # QR factorization ow Y (the preconditioned randomized Hessian)
        Q, Ra = np.linalg.qr(Y)

        # TODO: discuss; is the following correct?
        T = np.dot(Omega.T, Y)
        T = np.dot(Ra, T)
        T = np.dot(T, Ra.T)

        if self.verbose:
            print("SVD (for pseudo-inversion) of the prior-preconditioned GN-Hessian")

        # SVD
        U, S, V = np.linalg.svd(T, full_matrices=False)

        if self.verbose:
            print("Updating attributes...")

        # Extract Singular vectors
        Left_Singular = np.dot(Q, U)
        Right_Singular = np.dot(V, Q.T)

        # Associate elements for applying the randomized Hessian
        self._U = U
        self._S = S
        self._V = V
        self._LEFT_SINGULAR = Left_Singular
        self._RIGHT_SINGULAR = Right_Singular
        self._DR = S / (1.0 + S)

        # TODO: add tests for verification if needed!
        if self.verbose:
            print("DONE. Reduced-Hessian is ready...\n{0}\n".format(sep))

    def array(self, eval_at=None, data_misfit_only=False, verbose=False):
        """Construct an array repersentatiton of the reduced Hessian"""
        rH = np.empty((self._STATE_SIZE, self._STATE_SIZE))
        e = np.empty(self._STATE_SIZE)
        for i in range(self._STATE_SIZE):
            e[:] = 0.0
            e[i] = 1.0
            rH[:, i] = self.matvec(
                e, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=verbose
            )
        return rH

    def array_inverse(self, eval_at=None, data_misfit_only=False, verbose=False):
        """Construct an array repersentatiton of the reduced Hessian"""
        rH = np.empty((self._STATE_SIZE, self._STATE_SIZE))
        e = np.empty(self._STATE_SIZE)
        matvec = lambda x: self.matvec(
            x, eval_at=eval_at, data_misfit_only=data_misfit_only, verbose=verbose
        )
        A = LinearOperator(rH.shape, matvec=matvec)

        # TODO: Add other (more efficient) solvers and preconditioners as needed!

        for i in range(self._STATE_SIZE):
            e[:] = 0.0
            e[i] = 1.0
            _out = isolve.gmres(A=A, b=e)
            if not _out[1]:
                # Solution is successful
                _out = _out[0]
            else:
                print(
                    "Failed to apply GMRES to solve the linear system involving the covariance matrix"
                )
                raise ValueError
            rH[:, i] = _out
        return rH

    def matvec(self, state, eval_at=None, data_misfit_only=False, verbose=False):
        """
        Return the product of the Randomized-Hessian-Approxmiation of 4D-Var objective
            (inverse of posterior covariance) with a vector 'state'.
            Here, we use a two-pass algorithm for GN-Hessian approximation

        :param state: initial state/guess of the model state/parameter
        :param eval_at: state at which the Hessian is evaluated
            (passed to the model adjoint)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded,
            and only the data-misfit term is evaluated
        :param bool verbose: flag to control screen output verbosity

        :returns: estimate of the product of the inverse of the posterior covariance
            (of the linearized problem) with a state vector
        """
        # Get pointers to the underlying inverse_problem the simulation model, and prior
        inverse_problem = self._INVERSE_PROBLEM
        model = inverse_problem.model
        prior = inverse_problem.prior

        # Validate the passed state
        assert model.is_state_vector(
            state
        ), "passed state is invalid (check shape/type)"
        assert eval_at is None or model.is_state_vector(eval_at), "eval_at is invalid"

        # Apply the reduced-order Hessian to the passed_state
        # TODO: discuss; we need the Hessian matvec, not it's inverse for Newton-CG in scipy
        ## The preconditioner applied to a vector looks as follows:
        ## B0^{1/2}(I - Left_singular * Dr * Right_Singular^T)*B0^{1/2}
        ## Dr = diag(S(i)/(S(i)+1)), i = 1,...,r

        # local copy of the passed state
        x = state.copy()

        # Data-misfit term
        Bx = prior.covariance_sqrt_matvec(x, lower=True)
        out = self._RIGHT_SINGULAR.dot(Bx)
        out *= self._DR  # pointwise with diagonal; equivalent ot dot with diagonal mat
        out = self._LEFT_SINGULAR.dot(out)

        # add prior term if requested
        if not data_misfit_only:
            # Add prior term
            out += prior.covariance_sqrt_matvec(Bx, lower=True)

        return out

    # Add an alias
    Hessian_matvec = matvec

    def inv_matvec(self, state, eval_at=None, data_misfit_only=None, verbose=False):
        """
        Return the product of the Inverse of the Reduced-Hessian-Approxmiation of 4D-Var objective
            (inverse of posterior covariance) with a vector 'state'.
        """
        raise NotImplementedError("TODO...")

    # Add an alias
    Hessian_inv_matvec = inv_matvec

    @property
    def inverse_problem(self):
        """
        Pointer to the underlying inverse_problem (which gives access to all other elements; model, error models, observation operator, etc.)
        """
        return self._INVERSE_PROBLEM

    @property
    def verbose(self):
        """
        Evaluate the data-misfit term of the Hessian only
        """
        return self._VERBOSE

    @verbose.setter
    def verbose(self, value):
        """ """
        self._VERBOSE = bool(value)

    @property
    def data_misfit_only(self):
        """
        Evaluate the data-misfit term of the Hessian only
        """
        return self._DATA_MISFIT_ONLY

    @data_misfit_only.setter
    def data_misfit_only(self, value):
        """ """
        self._DATA_MISFIT_ONLY = bool(value)

    @property
    def rank_p(self):
        """ """
        return self._RANK_P

    @rank_p.setter
    def rank_p(self, value):
        """ """
        raise NotImplementedError("TODO")

    @property
    def over_sampling(self):
        """ """
        return self._OVER_SAMPLING

    @over_sampling.setter
    def over_sampling(self, value):
        """ """
        raise NotImplementedError("TODO")
