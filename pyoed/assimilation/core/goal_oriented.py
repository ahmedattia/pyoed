# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module provides blueprint for Gola-Oriented Inverse problems and data assimilation.
The goal operator is an additional operator that performs on the inference parameter/state.
This includes prediction to future time, etc.
"""

from dataclasses import dataclass

from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
)
from .assimilation import InverseProblem


@dataclass(kw_only=True, slots=True)
class GoalOrientedOperatorConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`GoalOrientedOperator` abstract base class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and only adds new
    class-level variables which can be updated as needed.

    :param inverse_problem: The inverse problem instance to be used with the goal
        operator (e.g., prediction operator)
    """
    name: str = "Goal-Oriented Operator"
    inverse_problem: None | InverseProblem = None


@set_configurations(GoalOrientedOperatorConfigs)
class GoalOrientedOperator(PyOEDObject):
    """
    Base class implementing a Goal-Oriented Operator that performs over an inverse problem.

    .. note::
        Every Goal-Oriented Operator *SHOULD* inherit this class.

    :param dict | GoalOrientedOperatorConfigs | None configs: (optional) configurations for
        the goal-oriented operator object

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """

    def __init__(self, configs: dict | GoalOrientedOperatorConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Make sure the inverse problem is properly registered
        self.configurations.inverse_problem = self.register_inverse_problem(
            self.configurations.inverse_problem
        )

    def validate_configurations(
        self,
        configs: dict | GoalOrientedOperatorConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`InverseProblemonfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`GoalOrientedOperatorConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `inverse_problem`: None or instance of InverseProblem
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="inverse_problem",
            test=lambda v: isinstance(v, (type(None), InverseProblem)),
            message=f" Inverse Problem is of invalid type. Expected None or instance of InverseProblem.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Update the inverse problem
        if "inverse_problem" in kwargs:
            self.register_inverse_problem(kwargs['inverse_problem'])

    def register_inverse_problem(
        self,
        inverse_problem: None | InverseProblem = None,
    ) -> InverseProblem | None:
        """
        Register (and return) the inverse problem to be registered.

        :raises TypeError: if the type of passed inverse problem is not supported
        """
        if not isinstance(inverse_problem, (type(None), InverseProblem)):
            raise TypeError(
                f"The passed {inverse_problem=} of {type(inverse_problem)=} "
                f"is not supported!\n"
                f"Expected instance of a class derived from `InverseProblem`"
            )

        # Set the inverse problem (and return it)
        self.configurations.inverse_problem = inverse_problem
        return inverse_problem

    def generate_vector(self, *args, **kwargs ):
        """Create a vector conformable with the goal operator (goal-vector)"""
        raise NotImplementedError(
            f"Each Goal-oriented operator must implement its own `apply_adjoint()` method"
            f"You Must Implement an `generate_vector()` method for the goal "
            f"oriented operator: of {type(self)=}"
        )

    def apply(self, *args, **kwargs, ):
        """Apply goal-oriented operator"""
        raise NotImplementedError(
            f"Each Goal-oriented operator must implement its own `apply_adjoint()` method"
            f"You Must Implement an `apply()` method for the goal "
            f"oriented operator: of {type(self)=}"
        )

    def apply_adjoint(self, *args, **kwargs, ):
        """Apply the adjoint (Jacbian-transposed, etc.) of the goal-oriented operator"""
        raise NotImplementedError(
            f"Each Goal-oriented operator must implement its own `apply_adjoint()` method"
            f"You Must Implement an `apply_adjoint()` method for the goal "
            f"oriented operator: of {type(self)=}"
        )

    def __call__(self, *args, **kwargs):
        """This is an alias to :py:meth:`self.apply()`"""
        return self.apply(*args, **kwargs, )

    @property
    def inverse_problem(self):
        """
        A reference to the underlying inverse problem.
        """
        return self.configurations.inverse_problem

    @property
    def size(self):
        """
        Dimension/Size of the goal space (size of a goal vector)

        .. note::
            Better implementation should be provided to return dimension without creating a vector.
        """
        return len(self.generate_vector())


@dataclass(kw_only=True, slots=True)
class GoalOrientedInverseProblemConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`GoalOrientedInverseProblem` base class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and only adds new
    class-level variables which can be updated as needed.

    :param inverse_problem: The inverse problem instance to be used with the goal
        operator (e.g., prediction operator)
    :param goal_operator: a goal operator (function of the inverse problem); this
        needs to be `None` or an instance (derived from `GoalOrientedOperator`).
    """
    name: str = "Goal-Oriented Inverse Problem"
    inverse_problem: None | InverseProblem = None
    goal_operator : None | GoalOrientedOperator = None


@set_configurations(GoalOrientedInverseProblemConfigs)
class GoalOrientedInverseProblem(PyOEDObject):
    """
    Base class implementing a Goal-Oriented Inverse problems.

    .. note::
        Every Goal-Oriented inverse problem *SHOULD* inherit this class.

    :param dict | GoalOrientedInverseProblemConfigs | None configs: (optional)
        configurations for the goal-oriented inverse problem

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """

    def __init__(self, configs: dict | GoalOrientedInverseProblemConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Make sure the inverse problem is properly registered
        self.configurations.inverse_problem = self.register_inverse_problem(
            self.configurations.inverse_problem
        )

        # Make sure the goal operator is registered
        self.configurations.goal_operator = self.register_goal_operator(
            self.configurations.goal_operator
        )

    def validate_configurations(
        self,
        configs: dict | GoalOrientedInverseProblemConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`InverseProblemonfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`GoalOrientedOperatorConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `inverse_problem`: None or instance of InverseProblem
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="inverse_problem",
            test=lambda v: isinstance(v, InverseProblem),
            message=f" Inverse Problem is of invalid type. Expected None or instance of InverseProblem.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `goal_operator`: None or instance of InverseProblem
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="goal_operator",
            test=lambda v: isinstance(v, (type(None), GoalOrientedOperator)),
            message=f" Goal operator is of invalid type. Expected None or instance of GoalOrientedOperator.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Update the inverse problem
        if "inverse_problem" in kwargs:
            self.register_inverse_problem(kwargs['inverse_problem'])

        # Update the goal operator
        if "goal_operator" in kwargs:
            self.register_goal_operator(kwargs['goal_operator'])

    def register_inverse_problem(
        self,
        inverse_problem: None | InverseProblem = None,
    ) -> InverseProblem | None:
        """
        Register (and return) the inverse problem to be registered.

        :raises TypeError: if the type of passed inverse problem is not supported
        """
        if not isinstance(inverse_problem, InverseProblem):
            raise TypeError(
                f"The passed {inverse_problem=} of {type(inverse_problem)=} "
                f"is not supported!\n"
                f"Expected instance of a class derived from `InverseProblem`"
            )

        # Set the inverse problem (and return it)
        self.configurations.inverse_problem = inverse_problem
        return inverse_problem

    def register_goal_operator(
        self,
        goal_operator: None | GoalOrientedOperator = None,
    ) -> GoalOrientedOperator | None:
        """
        Register (and return) the goal-oriented operator  to be registered.

        :raises TypeError: if the type of passed goal oriented operator i
            s not supported
        """
        if not isinstance(goal_operator, (type(None), GoalOrientedOperator)):
            raise TypeError(
                f"The passed {goal_operator=} of {type(goal_operator)=} "
                f"is not supported!\n"
                f"Expected instance of a class derived from `GoalOrientedOperator`"
            )

        # Set the goal-oriented operator (and return it)
        self.configurations.goal_operator = goal_operator
        return goal_operator

    @property
    def inverse_problem(self):
        """
        A reference to the underlying inverse problem.
        """
        return self.configurations.inverse_problem

    @property
    def goal_operator(self):
        """
        A reference to the underlying goal-operator.
        """
        return self.configurations.goal_operator

