# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

from . import (
    assimilation,
    filtering,
    smoothing,
    goal_oriented,
)

# import base classes
from .assimilation import (
    InverseProblem,
    InverseProblemConfigs,
    InverseProblemResults,
    Filter,
    FilterConfigs,
    FilterResults,
    Smoother,
    SmootherConfigs,
    SmootherResults,
    GaussianPosterior,
    GaussianPosteriorConfigs,
    ComplexGaussianPosterior,
    ComplexGaussianPosteriorConfigs,
)

from .filtering import (
    VariationalFilter,
    VariationalFilterConfigs,
    VariationalFilterResults,
    BayesianFilter,
    BayesianFilterConfigs,
    BayesianFilterResults,
    HybridFilter,
    HybridFilterConfigs,
    HybridFilterResults,
)

from .smoothing import (
    VariationalSmoother,
    VariationalSmootherConfigs,
    VariationalSmootherResults,
    BayesianSmoother,
    BayesianSmootherConfigs,
    BayesianSmootherResults,
    HybridSmoother,
    HybridSmootherConfigs,
    HybridSmootherResults,
)

from .smoothing import (
    VariationalSmoother,
    VariationalSmootherConfigs,
    VariationalSmootherResults,
    BayesianSmoother,
    BayesianSmootherConfigs,
    BayesianSmootherResults,
    HybridSmoother,
    HybridSmootherConfigs,
    HybridSmootherResults,
)

from .goal_oriented import (
    GoalOrientedOperatorConfigs,
    GoalOrientedOperator,
    GoalOrientedInverseProblemConfigs,
    GoalOrientedInverseProblem,
)

__all__ = [s for s in dir() if not s.startswith("_")]  # Remove dunders.

