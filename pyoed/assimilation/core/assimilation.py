# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Abstract classes for data assimilation algoirthms (inverse problems);
including both filtering (time-independent) and smoothing (time-dependent).
In filtering, one observation is used to update model state/parameter or
the underlying probability distribution, while in filters,
multiple observations (e.g., at different times) are used.
The output is an estimate of the model state/parameter
Uncertainty measure, e.g., posterior covariance, provided if a Bayesian approach is followed.
"""

import os
import inspect
import pickle
import warnings
from dataclasses import dataclass
from typing import (
    Any,
    Iterable,
)
import numpy as np

from pyoed import utility
from pyoed.configs import (
    PyOEDObject,
    PyOEDConfigs,
    PyOEDData,
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
)
from pyoed.models import(
    SimulationModel,
    TimeDependentModel,
    ErrorModel,
    ObservationOperator,
)
from pyoed.models.error_models.Gaussian import (
    GaussianErrorModel,
    GaussianErrorModelConfigs,
    ComplexGaussianErrorModel,
    ComplexGaussianErrorModelConfigs,
)
from pyoed.optimization import (
    Optimizer,
    OptimizerResults,
    OptimizerConfigs
)


@dataclass(kw_only=True, slots=True)
class InverseProblemConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`InverseProblem` abstract base class.
    This class inherits functionality from :py:class:`PyOEDConfigs` and only adds new class-level
    variables which can be updated as needed.

    See :py:class:`PyOEDConfigs` for more details on the functionality of this class
    along with a few additional fields. Otherwise :py:class:`InverseProblemConfigsConfigs`
    provides the following fields:

    :param name: name of the DA (inverse problem solver) approach/method.
    :param model: the simulation model.
    :param prior: Background/Prior model (e.g., :py:class:`GaussianErrorModel`)
    :param observation_operator: operator to map model state to observation space
    :param observation_error_model: Observation error model (e.g., :py:class:`GaussianErrorModel`)
    :param observations: Observational data (the data type is very much dependent of the DA method)

    :param optimizer: the optimization routine (optimizer) to be *registered*
        and later used for solving the OED problem.
        This can be one of the following:

        - `None`: In this case, no optimizer is registered, and the :py:meth:`solve`
          won't be functional until an optimization routine is registered.
        - An optimizer instance (object that inherits :py:class`Optimizer`).
          In this case, the optimizer is registered **as is** and is updated
          with the passed configurations if available.
        - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
          the optimizer.

    :param optimizer_configs: the configurations of the optimization routine.
        This can be one of the following:

        - `None`, in this case configurations are discarded, and whatever default configurations
          of the selected/passed optimizer are employed.
        - A `dict` holding full/partial configurations of the selected optimizer.
          These are either used to instantiate or update the optimizer configurations based on
          the type of the passed `optimizer`.
        - A class providing implementations of the configurations (this must be a subclass of
          :py:class:`OptimizerConfigs`.
        - An instance of a subclass of :py:class:`OptimizerConfigs` which is to set/udpate
          optimizer configurations.

    .. note::
        Not all DA (inverse problem) objects are optimization-based. For example, particle-based
        (EnKF, PF, etc.) employ a sample to estimate the flow of the distribution through the model
        dynamics (prior -> posterior). Thus, the optimizer (and configs) in this case (the default)
        are set to `None`.
        For optimization-based methods such as `3DVar`, `4DVar`, etc., an optimizer must be registered
        for the inverse problem to be solved.

    """
    name: str | None = None
    model: None | SimulationModel = None
    prior: None | ErrorModel = None
    observation_error_model: None | ErrorModel = None
    observation_operator: None | ObservationOperator = None
    observations: None | Any = None

    optimizer : None | Optimizer | Optimizer = None
    optimizer_configs : None | dict | OptimizerConfigs | OptimizerConfigs = None



@set_configurations(InverseProblemConfigs)
class InverseProblem(PyOEDObject):
    """
    Base class for implementations of Inversion/Inference/DA (Data Assimilation) methods/approaches.

    .. note::
        The `optimizer` configuration attribute can be assigned an optimization
        routine to be used for solving
        the inverse problem (for optimization-based DA methods).
        For optimization based DA methods, the method :py:meth:`solve` relies on this object
        for solving the OED optimization problem.
        Since not all DA methods are optimization-base, this is `None` by default and need
        to be created by derived classes.
        One can, however, discard this attribute and write full functionality in
        the :py:meth:`solve` method. However, employing this attribute is expected for consistency.

    :param dict | InverseProblemConfigs | None configs: (optional) configurations for
        the optimization object

    :raises PyOEDConfigsValidationError: if passed invalid configs
    """
    def __init__(self, configs: dict | InverseProblemConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        ## Register elements properly
        # The ptimization routine
        self.configurations.optimizer = self.register_optimizer(
            optimizer=self.configurations.optimizer,
            optimizer_configs=self.configurations.optimizer_configs,
        )

        # Register/Initialize the model
        self.configurations.model = self.register_model(
            model=self.configurations.model,
        )

        # Register/Initialize the prior
        self.configurations.prior = self.register_prior(
            prior=self.configurations.prior,
        )

        # Register/Initialize the observation error model
        self.configurations.observation_error_model = self.register_observation_error_model(
            observation_error_model=self.configurations.observation_error_model,
            )

        # Register/Initialize the observation operator
        self.configurations.observation_operator = self.register_observation_operator(
            observation_operator=self.configurations.observation_operator,
        )

        # Register/Initialize the observations
        self.configurations.observations = self.register_observations(
            observations=self.configurations.observations,
        )

        # Initialize the posterior (Initially None; updated if needed after solving the inverse problem)
        self._POSTERIOR = None


    def validate_configurations(
        self,
        configs: dict | InverseProblemConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`InverseProblemonfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`InverseProblemConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # `name`: None or string
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="name",
            test=lambda v: isinstance(v, (str, type(None))),
            message=f"name is of invalid type. Expected None or string. ",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `model`: None or instance of SimulationModel
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="model",
            test=lambda v: isinstance(v, (SimulationModel, type(None))),
            message=f"Simulation model is of invalid type. Expected None or instance of SimulationModel.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `prior`: None or instance of ErrorModel
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="prior",
            test=lambda v: isinstance(v, (ErrorModel, type(None))),
            message=f"Prior is of invalid type. Expected None or instance of ErrorModel.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `observation_error_model`: None or instance of ErrorModel
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observation_error_model",
            test=lambda v: isinstance(v, (ErrorModel, type(None))),
            message=f"Observation error model is of invalid type. Expected None or instance of ErrorModel.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `observation_operator`: None or instance of ObservationOperator
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="observation_operator",
            test=lambda v: isinstance(v, (ObservationOperator, type(None))),
            message=f"Observation operator is of invalid type. Expected None or instance of ObservationOperator.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Update the optimizer
        if "optimizer" in kwargs:
            optimizer = kwargs["optimizer"]
            optimizer_configs = kwargs.get('optimizer_configs', None)
            self.register_optimizer(optimizer=optimizer, optimizer_configs=optimizer_configs)
        else:
            if 'optimizer_configs' in kwargs:
                self.register_optimizer(
                    optimizer=self.optimizer,
                    optimizer_configs=kwargs['optimizer_configs'],
                )

        if "model" in kwargs:
            self.register_model(kwargs['model'])

        if "prior" in kwargs:
            self.register_prior(self, model=kwargs['prior'])

        if "observation_error_model" in kwargs:
            self.register_observation_error_model(kwargs['observation_error_model'])

        if "observation_operator" in kwargs:
            self.register_observation_operator(kwargs['observation_operator'])

        if "observations" in kwargs:
            self.register_observations(kwargs['observations'])


    def register_model(
        self,
        model: None | SimulationModel = None,
    ) -> SimulationModel | None:
        """
        Register (and return) the simulation model to be registered.

        :raises TypeError: if the type of passed model is not supported
        """
        if not isinstance(model, (type(None), SimulationModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `SimulationModel`"
            )

        # Set the model
        self.configurations.model = model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the prior to be registered.

        :raises TypeError: if the type of passed prior is not supported
        """
        if not isinstance(prior, (type(None), ErrorModel)):
            raise TypeError(
                f"The passed {prior=} of {type(prior)=} is not supported!\n"
                f"Expected instance of a class derived from `ErrorModel`"
            )

        # Set the model
        self.configurations.prior = prior
        return prior

    def register_observation_operator(
        self,
        observation_operator: None | ObservationOperator = None,
    ) -> ObservationOperator | None:
        """
        Register (and return) the observation operator to be registered.

        :raises TypeError: if the type of passed observation operator is not supported
        """
        if not isinstance(observation_operator, (type(None), ObservationOperator)):
            raise TypeError(
                f"The passed {observation_operator=} of {type(observation_operator)=} is not supported!\n"
                f"Expected instance of a class derived from `ErrorModel`"
            )

        # Set the model
        self.configurations.observation_operator = observation_operator
        return observation_operator

    def register_observation_error_model(
        self,
        observation_error_model: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the observation error model to be registered.

        :raises TypeError: if the type of passed observation error model is not supported
        """
        if not isinstance(observation_error_model, (type(None), ErrorModel)):
            raise TypeError(
                f"The passed {observation_error_model=} of {type(observation_error_model)=} is not supported!\n"
                f"Expected instance of a class derived from `ErrorModel`"
            )

        # Set the model
        self.configurations.observation_error_model = observation_error_model
        return observation_error_model

    def register_observations(self, observations, ):
        """
        Register (and return) the observational data.

        :raises TypeError: if the type of passed observational data is not supported
        """
        self.configurations.observations = observations
        return observations

    def register_optimizer(
        self,
        optimizer: Optimizer | None,
        *args,
        **kwargs,
    ) -> Optimizer | None:
        """
        Register (and return) the passed optimizer.

        .. note::
            This method does not create a new optimizer instance.
            It just takes the created optimizer, makes sure it is an instance derived
            from the :py:class:`pyoed.optimization.Optimizer` and associates it with
            this assimilation (DA) object.

        .. note::
            A derived class is expected to create this optimizer, and pass it up
            by calling `super().register_optimizer(optimizer)` so that it can be
            registered properly.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer is not supported
        """
        if not isinstance(optimizer, (Optimizer, type(None))):
            raise TypeError(
                f"Unsupported optimizer {optimizer=} of {type(optimizer)=}\n"
                f"Expected an instance derived from the PyOED's optimizer base class "
                f"`pyeod.optimization.Optimizer`"
            )

        # Set the optimizer (and its configurations)
        # Update optimizer and configurations
        self.configurations.optimizer = optimizer
        if optimizer is None:
            if self.configurations.optimizer_configs is None:
                warnings.warn(
                    f"Optimizer `None` kills optimizer_configs"
                )
            self.configurations.optimizer_configs = None
        else:
            self.configurations.optimizer_configs = optimizer.configurations


        return self.configurations.optimizer

    def solve_inverse_problem(self, *args, **kwargs):
        """
        Start solving the inverse problem (DA) for the registered configuration with passed arguments.

        .. warning::
            This method is added for backward compatibility, and it will be deprecated soon.
            User need to call :py:meth:`solve` instead.
        """
        warnings.warn(
            f"Calling `solve_inverse_problem` will be deprecated in the future. "
            f"This method will be replace with `solve`",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.solve(*args, **kwargs, )

    def apply_forward_operator(self, *args, **kwargs, ):
        """
        Apply F, the forward operator to the passed state.
        The forward operator here, refers to the simulation model followed by the observation operator.
        The result is a data point (an observation), or a dictionary of observations indexed by the time
        (for time-dependent models).

        For time-dependent simulations with multiple observation points
        (e.g., in 4D-Var settings), the observations are evaluated
        at the simulation time instances (corresponding to registered
        observations) over the registered time window.
        """
        raise NotImplementedError(
            f"This method is not implemented for objects of type `{self.__class__}`"
        )

    def apply_forward_operator_adjoint(self, *args, **kwargs, ):
        """
        Apply F^*, the adjoint of the forward operator to the passed observation.
        """
        raise NotImplementedError(
            f"This method is not implemented for objects of type `{self.__class__}`"
        )

    def show_registered_elements(self, display=True, ) -> None:
        """
        Compose and (optionally) print out a message containing the elements
        of the inverse problem and show what's registered and what's not

        :param display: if `True` print out the composed message about
            registered/unregistered elements

        :returns: the composed message
        """
        all_elements = [
            'model', 'prior', 'observation_operator',
            'observation_error_model', 'observations',
        ]

        sep = "*"*40
        registered_message = f"\n\n{sep}\n\tRegistered Elements\n{sep}\n"
        unregistered_message = f"{sep}\n\tUnregistered Elements\n{sep}\n"

        for element in all_elements:
            if getattr(self, element) is None:
                unregistered_message += f"- '{element}'\n"

            else:
                registered_message += f"- '{element}': {getattr(self, element)}\n"

        registered_message += f"{sep}\n"
        unregistered_message += f"{sep}\n"
        msg = registered_message + unregistered_message

        if display: print(msg)
        return msg

    def check_registered_elements(self, *args, **kwargs, ) -> bool:
        """
        Check if all elements of the inverse problem (simulation model, observation operator,
        prior, observation error model, and observational data)
        are registered or not.

        .. note::
            This method SHOULD be modified by derived classed to check other elements.
            For example, a smoother requires observation times, assimilation window, etc.
        """
        if (
            self.model is None or
            self.prior is None or
            self.observations is None or
            self.observation_operator is None or
            self.observation_error_model is None
        ):
            return False
        else:
            return True


    def solve(self, init_guess=None, ):
        """
        Start solving the inverse problem.

        .. note::
            This method needs to be replicated (rewritten) for any DA (inverse problem) object so
            that it can replace the returned results object with teh appropriate one.

        :param init_guess: The initial guess of the design to be used as
            starting point of the optimization routine

        :returns: an instance of (derived from) InverseProblemResults holding results
            obtained by solving the inverse problem
        """
        raise NotImplementedError(
            f"This method is not implemented for objects of type `{self.__class__}`"
        )

    def plot_results(
        self,
        results,
        overwrite=False,
        bruteforce_results=None,
        num_active=None,
        uncertain_parameter_sample=None,
        exhaustive_parameter_search_results=None,
        show_legend=True,
        output_dir=None,
        keep_plots=False,
        fontsize=20,
        line_width=2,
        usetex=True,
        show_axis_grids=True,
        axis_grids_alpha=(0.25, 0.4),
        plots_format='pdf',
    ):
        """
        Generic plotting function for inverse problems.
        Given the results returned by :py:meth:`solve`, visualize the results.
        Additional plotters can be added to this method or derived methods...

        :raises TypeError: if no valid optimizer is registered
        """
        if isinstance(results, InverseProblemResults):
            _results = results.asdict()
        elif isinstance(results, dict):
            _results = results
            pass
        else:
            raise TypeError(
                f"Expected results to be dictionary or `InverseProblemResults`; "
                f"received {results=} of {type(results)=}!"
            )

        try:
            optimization_results = _results['optimization_results']
        except(KeyError) as err:
            raise TypeError(
                f"The passed results of {type(results)=} is not supported!"
                f"There is not `optimization_results` entry/key found! Trace the error below\n"
                f"Unexpected {err=} of {type(err)=}"
            )
        if self.optimizer is None:
            raise TypeError(
                f"No valid optimizer is registered. "
                f"Use the method register_optimizer() to register "
                f"an optimizer before calling this `solve` method!"
            )
        plot_results = self.optimizer.plot_results(
            optimization_results,
            overwrite=overwrite,
            bruteforce_results=bruteforce_results,
            num_active=num_active,
            uncertain_parameter_sample=uncertain_parameter_sample,
            exhaustive_parameter_search_results=exhaustive_parameter_search_results,
            show_legend=show_legend,
            output_dir=output_dir,
            keep_plots=keep_plots,
            fontsize=fontsize,
            line_width=line_width,
            usetex=usetex,
            show_axis_grids=show_axis_grids,
            axis_grids_alpha=axis_grids_alpha,
            plots_format=plots_format,
        )

        return plot_results


    @property
    def optimizer(self):
        """Thre registered optimizer."""
        return self.configurations.optimizer
    @optimizer.setter
    def optimizer(self, val):
        return self.update_configurations(optimizer=val)

    @property
    def model(self):
        """Thre registered simulation model."""
        return self.configurations.model
    @model.setter
    def model(self, val):
        return self.update_configurations(model=val)

    @property
    def prior(self):
        """Thre registered prior."""
        return self.configurations.prior
    @prior.setter
    def prior(self, val):
        return self.update_configurations(prior=val)

    @property
    def posterior(self):
        """The posterior."""
        return self._POSTERIOR

    @property
    def observation_error_model(self):
        """Thre registered observation error model."""
        return self.configurations.observation_error_model
    @observation_error_model.setter
    def observation_error_model(self, val):
        return self.update_configurations(observation_error_model=val)

    @property
    def observation_operator(self):
        """Thre registered observation operator."""
        return self.configurations.observation_operator
    @observation_operator.setter
    def observation_operator(self, val):
        return self.update_configurations(observation_operator=val)

    @property
    def observations(self):
        """Thre registered observational data."""
        return self.configurations.observations
    @observations.setter
    def observations(self, val):
        return self.update_configurations(observations=val)



@dataclass(kw_only=True, slots=True)
class InverseProblemResults(PyOEDData):
    """
    Base class to hold DA (inverse problems) data/results

    :param inverse_problem: instance of a class derived from :py:class:`InverseProblem`.
    """
    inverse_problem: InverseProblem | None = None
    optimization_results: OptimizerResults | None = None

    def __str__(self) -> str:
        sep = "*" * 50
        name = type(self).__name__
        msg = f"{sep}\n  PyOED Empty InverseProblem Data Container\n{sep}\n"
        return msg

    def write_results(self, saveto):
        """
        Save the underlying InverseProblem results to pickled dictionary.

        :param saveto: name/path of the file to save data to.
        :raises TypeError: if `saveto` is not a valid file path
        :raises IOError: if writing failed
        """
        # Check output file path
        saveto = os.path.abspath(saveto)
        if not utility.path_is_accessible(path):
            raise IOError(
                f"The file path {saveto=} is not accessible!"
            )
        else:
            # Create directory if needed
            d, _ = os.path.split(saveto)
            if not os.path.isdir(d):
                os.makedirs(d)

        # Try writing results
        try:
            pickle.dump(self.asdict(), open(saveto, "wb"))
            if self.verbose:
                print(f"Results dumped/saved to: {saveto}")
        except Exception as err:
            raise IOError(
                f"Failed to write InverseProblemResults, Returning...\n"
                f"Unexpected{err=} of {type(err)=}"
            )

    @classmethod
    def load_results(cls, readfrom, ):
        """
        Inspect pickled file, and load inverse problem results;

        :raises IOError: if loading failed
        """
        try:
            data = pickle.load(open(readfrom, 'rb'))
        except Exception as err:
            raise IOError(
                f"Failed to load InverseProblemResults from {readfrom} \n"
                f"Unexpected{err=} of {type(err)=}"
            )
        return cls(configs=data)




@dataclass(kw_only=True, slots=True)
class FilterConfigs(InverseProblemConfigs):
    """
    Configurations class for the :py:class:`Filter` abstract base class.
    """
    ...


@set_configurations(FilterConfigs)
class Filter(InverseProblem):
    """
    Base class for all filtering DA implementations.
    Currently, this mirrors :py:class:`InverseProblem`.
    """
    def __init__(self, configs: dict | FilterConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def register_model(
        self,
        model: None | SimulationModel = None,
    ) -> SimulationModel | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.


        :raises TypeError: if the type of passed model is not supported
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...

        # Return the model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the prior to be registered.
        This calls `InverseProblem.register_prior` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed prior is not supported
        """
        # Associate with self and do super validation/testin
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # Return Prior
        return prior

    def register_observation_operator(
        self,
        observation_operator: None | ObservationOperator = None,
    ) -> ObservationOperator | None:
        """
        Register (and return) the observation operator to be registered.
        This calls `InverseProblem.register_observation_operator` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observation operator is not supported
        """
        # Associate with self and do super validation/testing
        observation_operator = super().register_observation_operator(
            observation_operator=observation_operator,
        )

        ## Extra testing
        ...

        # If valid operator found, make assertions on shape, attributes, etc.
        if observation_operator is not None:

            # Compare observation operator against registered relevant elements
            if self.observations is not None:
                if observation_operator.shape[0] != self.observations.size:
                    raise TypeError(
                        f"Expected observation operator of shape ({self.observations.size}, *)\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The observation size {self.observations.size=}\n"
                    )

            if self.model is not None:
                if observation_operator.shape[1] != self.model.state_size:
                    raise TypeError(
                        f"Expected observation operator of shape (*, {self.model.state_size})\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The model state size {self.model.state_size=}"
                    )

            if self.observation_error_model is not None:
                if observation_operator.shape[0] != self.observation_error_model.size:
                    raise TypeError(
                        f"Expected observation operator of shape "
                        f"({self.observation_error_model.size}, *)\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The observation error model size "
                        f"{self.observation_error_model.size=}"
                    )

        return observation_operator

    def register_observation_error_model(
        self,
        observation_error_model: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the observation error model to be registered.
        This calls `InverseProblem.register_observation_error_model` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observation error model is not supported
        """
        # Associate with self and do super validation/testing
        observation_error_model = super().register_observation_error_model(
            observation_error_model=observation_error_model,
        )

        ## Extra testing
        ...

        # If valid error model found, make assertions on shape, attributes, etc.
        if observation_error_model is not None:

            # Compare observation error model against registered relevant elements
            if self.observations is not None:
                if observation_error_model.size != self.observations.size:
                    raise TypeError(
                        f"Expected observation error model of shape size "
                        f"{self.observations.size=}\n"
                        f"The observation error model size {observation_error_model.size=}"
                        f"The observation size {self.observations.size=}"
                    )

            if self.observation_operator is not None:
                if observation_error_model.size !=self.observation_operator.shape[0]:
                    raise TypeError(
                        f"Expected observation error model of shape "
                        f"({observation_operator.shape[0]}, "
                        f"{observation_operator.shape[0]})\n"
                        f"The observation error model size {observation_error_model.size=}"
                        f"The observation operator shape {self.observation_operator.shape=}\n"
                    )

        return observation_error_model

    def register_observations(self, observations: None | Iterable, ) -> None | np.ndarray:
        """
        Register (and return) the observational data.
        This calls `InverseProblem.register_observations` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observational data is not supported
        """
        # If valid observations found, make assertions on shape, attributes, etc.
        if observations is not None:

            if self.observation_operator is self.observation_error_model is None:
                try:
                    _observation = utility.asarray(observation)
                except(Exception) as err:
                    raise TypeError(
                        f"Invalid {observation=} of {type(observation)=}\n"
                        f"The observation need to be castable to one-dimensional numpy array\n"
                        f"See the error below resulted when tried casting to array:\n"
                        f"Unexpected {err=} of {type(err)=}"
                    )
                else:
                    # Make sure the array is castable to numpy array
                    if not np.issubdtype(_observation.dtype, np.number):
                        raise TypeError(
                            f"The observation need to be castable to one-dimensional *numeric* "
                            f"numpy array. When casted to numpy array, the resulting data type"
                            f"is {_observations.dtype=}"
                        )
                    else:
                        observations = _observations

            # Compare observation error model against registered relevant elements
            if self.observation_operator is not None:
               if not self.observation_operator.is_observation_vector(observations):
                raise TypeError(
                    "Observation passed is not a valid observation!"
                    f"Received {type(observations)}"
                )

            if self.observation_error_model is not None:
                if observations.size != self.observation_error_model.size:
                    raise TypeError(
                        f"Expected observations "
                        f"of size: {observations.size=}\n"
                        f"The observation error model size {observation_error_model.size=}"
                        f"The passed observation has size {observations.size=}"
                    )

        observations = super().register_observations(observations=observations, )

        return observations


@dataclass(kw_only=True, slots=True)
class FilterResults(InverseProblemResults):
    """
    Base class for objects holding results of :py:class:`Filter`
    """
    ...


@dataclass(kw_only=True, slots=True)
class SmootherConfigs(InverseProblemConfigs):
    """
    Configurations class for the :py:class:`Smoother` abstract base class.

    :param window: the assimilation window `(t0, tf)`
    """
    window: None | Iterable = None
    ...


@set_configurations(SmootherConfigs)
class Smoother(InverseProblem):
    """
    Base class for all smoothing DA implementations.
    Currently, this mirrors :py:class:`InverseProblem`.
    """
    _TIME_EPS = 1e-10  # for comparison of temporal checkpoints

    def __init__(self, configs: dict | SmootherConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        self.configurations.window = self.register_window(
            window=self.configurations.window,
        )

    def register_model(
        self,
        model: None | TimeDependentModel = None,
    ) -> TimeDependentModel | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.


        :raises TypeError: if the type of passed model is not supported
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...

        if not isinstance(model, (type(None), TimeDependentModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `TimeDependentModel`"
            )

        # Return the model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the prior to be registered.
        This calls `InverseProblem.register_prior` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed prior is not supported
        """
        # Associate with self and do super validation/testin
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # Return Prior
        return prior

    def register_window(self, window: None | Iterable) -> None | Iterable:
        """
        Update the assimilation/inversion time window (cycle)

        :param window: an iterable with two entries :math:`(t_0, t_1)`
            indicating the beginning and the end of the inversion time window.

        :raises TypeError: if `window` is invalid iterable with two entries
        """
        if window is None:
            pass
        elif utility.isiterable(window):
            try:
                t0, t1 = window
                prec = len(("%f" % (self._TIME_EPS - int(self._TIME_EPS))).split(".")[1])
                t0 = np.round(t0, prec)
                t1 = np.round(t1, prec)
            except:
                raise TypeError(
                    f"Failed to cast the passed assimilation window!\n"
                    f"window must be an iterable (tuple, list, ...) with two entries"
                )

            # TODO: what happens if the user has already registered observations!
            if not(0 <= t0 < t1):
                raise TypeError(f"Invalid assimilation time window ({t0}, {t1})")

            if self.observations is not None:
                for t in self.observation_times:
                    if not (t0 <= t <= t1):
                        raise TypeError(
                            f"An observation registered at {t=} which falls "
                            f"outside the passed  window ({t0=}, {t1=})"
                        )

        else:
            raise TypeError(
                f"Invalid assimilation time window "
                f"{window=} of {type(window)=}"
            )

        # Associate and return
        self.configurations.window = window
        return window

    def register_observation_operator(
        self,
        observation_operator: None | ObservationOperator = None,
    ) -> ObservationOperator | None:
        """
        Register (and return) the observation operator to be registered.
        This calls `InverseProblem.register_observation_operator` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observation operator is not supported
        """
        # Associate with self and do super validation/testing
        observation_operator = super().register_observation_operator(
            observation_operator=observation_operator,
        )

        ## Extra testing
        ...

        # If valid operator found, make assertions on shape, attributes, etc.
        if observation_operator is not None:

            # Compare observation operator against registered relevant elements
            obs_times = self.observation_times
            if obs_times is not None and len(obs_times) > 0:
                if observation_operator.shape[0] != self.observations[obs_times[0]].size:
                    raise TypeError(
                        f"Expected observation operator of shape ({self.observations[obs_times[0]].size}, *)\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The observation size {self.observations[obs_times[0]].size=}\n"
                    )

            if self.model is not None:
                if observation_operator.shape[1] != self.model.state_size:
                    raise TypeError(
                        f"Expected observation operator of shape (*, {self.model.state_size})\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The model state size {self.model.state_size=}"
                    )

            if self.observation_error_model is not None:
                if observation_operator.shape[0] != self.observation_error_model.size:
                    raise TypeError(
                        f"Expected observation operator of shape "
                        f"({self.observation_error_model.size}, *)\n"
                        f"The observation operator shape {observation_operator.shape=}"
                        f"The observation error model size "
                        f"{self.observation_error_model.size=}"
                    )

        return observation_operator

    def register_observation_error_model(
        self,
        observation_error_model: None | ErrorModel = None,
    ) -> ErrorModel | None:
        """
        Register (and return) the observation error model to be registered.
        This calls `InverseProblem.register_observation_error_model` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observation error model is not supported
        """
        # Associate with self and do super validation/testing
        observation_error_model = super().register_observation_error_model(
            observation_error_model=observation_error_model,
        )

        ## Extra testing
        ...

        # If valid error model found, make assertions on shape, attributes, etc.
        if observation_error_model is not None:

            # Compare observation error model against registered relevant elements
            obs_times = self.observation_times
            if obs_times is not None and len(obs_times) > 0:
                if observation_error_model.size != self.observations[obs_times[0]].size:
                    raise TypeError(
                        f"Expected observation error model of shape size "
                        f"{self.observations[obs_times[0]].size=}\n"
                        f"The observation error model size {observation_error_model.size=}"
                        f"The observation size {self.observations[obs_times[0]].size=}"
                    )

            if self.observation_operator is not None:
                if observation_error_model.size !=self.observation_operator.shape[0]:
                    raise TypeError(
                        f"Expected observation error model of shape "
                        f"({observation_operator.shape[0]}, "
                        f"{observation_operator.shape[0]})\n"
                        f"The observation error model size {observation_error_model.size=}"
                        f"The observation operator shape {self.observation_operator.shape=}\n"
                    )

        return observation_error_model

    def register_observations(self, observations: None | dict, ) -> None | dict:
        """
        Register (and return) the observational data.
        This calls `InverseProblem.register_observations` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed observational data is not supported
        """
        ## Extra testing
        ...

        # If valid observations found, make assertions on shape, attributes, etc.
        if observations is None:
            observations = self.configurations.observations = None
        elif isinstance(observations, dict):
            _observations = observations.copy()
            # Associate with self and do super validation/testing
            self.configurations.observations = {}

            for t, observation in _observations.items():
                self.register_observation(t=t, observation=observation, )
        else:
            raise TypeError(
                f"Invalid observations type\n"
                f"Received {observations=} of {type(observations)=}"
            )

        return self.configurations.observations

    def register_observation(self, t, observation, overwrite=False, ):
        """
        Given an `observation` instance/vector and the associated time
        `t`, update observation/data information

        :param float t: time at which the passed `observation` is registered
        :param observation: an observation vector; this should be specified
            by the forward or the observation operator
        :param bool overwrite: overwrite an existing observation if
            already registered at the passed time

        :raises ValueError: is raised if:
            + `overwrite=False` and another observation exists at time `t`
            + No valid observation operator has been registered yet
            + The assimilation window is not yet registered

        :raises TypeError: is raised if the observation is not validated
            by the associated observation operator
        """
        # Check if observation is previously adde to the data dictionary
        prec = len(("%f" % (self._TIME_EPS - int(self._TIME_EPS))).split(".")[1])
        t = np.round(t, prec)

        checked_t = self.find_observation_time(t)
        if checked_t is not None and not overwrite:
            raise ValueError(
                f"An observation is already registered at time t={t}\n"
                "Set overwrite flag to True if you want to replace/overwrite"
                " observation at this time"
            )

        # Validate passed time against registered assimilation window
        if self.window is not None:
            t0, tf = self.window
            if (t0 - t) > self._TIME_EPS or (t - tf) > self._TIME_EPS:
                raise ValueError(
                    "Observation time must fall within the registered assimilation"
                    f" window\nPassed time: {t}\nRegistered Assimilation Timewindow:"
                    f" {self.window}"
                )

        ## Check the observation type
        if self.observation_operator is self.observation_error_model is None:
            try:
                _observation = utility.asarray(observation)
            except(Exception) as err:
                raise TypeError(
                    f"Invalid {observation=} of {type(observation)=}\n"
                    f"The observation need to be castable to one-dimensional numpy array\n"
                    f"See the error below resulted when tried casting to array:\n"
                    f"Unexpected {err=} of {type(err)=}"
                )
            else:
                # Make sure the array is castable to numpy array
                if not np.issubdtype(_observation.dtype, np.number):
                    raise TypeError(
                        f"The observation need to be castable to one-dimensional *numeric* "
                        f"numpy array. When casted to numpy array, the resulting data type"
                        f"is {_observations.dtype=}"
                    )
                else:
                    observations = _observations

        elif self.observation_operator is not None:
           # Compare observations to observation operator
           if not self.observation_operator.is_observation_vector(observation):
            raise TypeError(
                "Observation passed is not a valid observation!"
                f"Received {type(observation)}"
            )

        elif self.observation_error_model is not None:
           # Compare observations to observation error model
            if observation.size != self.observation_error_model.size:
                raise TypeError(
                    f"Expected observations "
                    f"of size: {observation.size=}\n"
                    f"The observation error model size {observation_error_model.size=}"
                    f"The passed observation has size {observation.size=}"
                )

        if self.observations is None:
            self._OBSERVATIONS = self.configurations.observations = {}
        observations = self.observations
        observations.update({t: observation})

        return observation

    def find_observation_time(self, t, time_keys=None, ):
        """
        Local function to find the key in `self.observations` reresenting
        time equal to or within `_TIME_EPS` of the passed time

        :param t: the time to lookup
        :param time_keys: the timespan to look into. If `None`, the registered
            observation times will be used.
        :returns: the matched time (if found) or `None`
        """
        if time_keys is not None:
            time_keys = np.asarray(time_keys).flatten()
        else:
            time_keys = self.observation_times

        if time_keys is None:
            return None

        if len(time_keys) == 0:
            found_t = None
        else:
            locs = np.where(np.abs(time_keys - t) <= self._TIME_EPS)[0]
            if locs.size == 0:
                found_t = None
            elif locs.size == 1:
                found_t = time_keys[locs[0]]
            else:
                raise ValueError(
                    "Found multiple time instances equal, or very close, to the passed"
                    " time\nThis should never happen!"
                )
        return found_t

    def check_registered_elements(self, *args, **kwargs, ) -> bool:
        """
        Check if all elements of the inverse problem (simulation model, observation operator,
        prior, observation error model, and observational data)
        are registered or not.

        .. note::
            This method SHOULD be modified by derived classed to check other elements.
            For example, a smoother requires observation times, assimilation window, etc.
        """
        return self.window is not None and super().check_registered_elements(*args, **kwargs)

    def show_registered_elements(self, display=True, ) -> None:
        """
        Compose and (optionally) print out a message containing the elements
        of the inverse problem and show what's registered and what's not

        :param display: if `True` print out the composed message about
            registered/unregistered elements

        :returns: the composed message
        """
        all_elements = [
            'model', 'prior', 'observation_operator',
            'observation_error_model', 'observations',
            'window'
        ]

        sep = "*"*40
        registered_message = f"\n\n{sep}\n\tRegistered Elements\n{sep}\n"
        unregistered_message = f"{sep}\n\tUnregistered Elements\n{sep}\n"

        for element in all_elements:
            if getattr(self, element) is None:
                unregistered_message += f"- '{element}'\n"

            else:
                registered_message += f"- '{element}': {getattr(self, element)}\n"

        registered_message += f"{sep}\n"
        unregistered_message += f"{sep}\n"
        msg = registered_message + unregistered_message

        if display: print(msg)
        return msg


    @property
    def observation_times(self):
        """Return a numpy array holding registered obserevation times"""
        if self.observations is None:
            obs_times = None
        else:
            obs_times = np.asarray([k for k in self.observations.keys()])
            obs_times.sort()
        return obs_times

    @property
    def window(self):
        """"""
        return self.configurations.window


@dataclass(kw_only=True, slots=True)
class SmootherResults(InverseProblemResults):
    """
    Base class for objects holding results of :py:class:`Smoother`
    """



@dataclass(kw_only=True, slots=True)
class GaussianPosteriorConfigs(GaussianErrorModelConfigs):
    """
    Just renaming (possibly add more features later) around
    Gaussian distribution/model configurations.
    """
    ...

@set_configurations(GaussianPosteriorConfigs)
class GaussianPosterior(GaussianErrorModel):
    """
    A class approximating the posterior distribution of the 3D-Var problem around
    the MAP estimate.
    Everything is the same as in `GaussianErrorModel`, except for the attribute
    `__STDEV` which is involved in the methods `covariance_matvec`, and `generate_noise`.
    These two functions are replaced with matrix-free versions.
    Here, we do not construct the covariance (posterior covariance), instead, we use
    the fact that the posterior covariance is (or can be approximated by):

    .. math::
        \\mathbf{A}:= \\left( \\mathbf{B}^{-1} +
        \\mathbf{M}^T \\mathbf{H}^T \\mathbf{R}_k^{-1}
        \\mathbf{H} \\mathbf{M} \\right)^{-1} \\,,

    where :math:`\\mathbf{B}` is the prior covariance matrix, :math:`\\mathbf{H}`
    is the linear observation operator (or the linearized, e.g., Jacobian,
    around the MAP estimate),

    :math:`\\mathbf{R}` is the observation error covariance matrix,
    and :math:`\\mathbf{M}` is the tangent linear
    of the simulation model (TLM) evaluated at the MAP estimate
    """
    def __init__(self, configs: dict | GaussianPosteriorConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)


@dataclass(kw_only=True, slots=True)
class ComplexGaussianPosteriorConfigs(ComplexGaussianErrorModelConfigs):
    """
    Just renaming (possibly add more features later) around
    Complex-valued Gaussian distribution/model configurations.
    """
    ...

@set_configurations(ComplexGaussianPosteriorConfigs)
class ComplexGaussianPosterior(ComplexGaussianErrorModel):
    """
    A class approximating the posterior distribution of the 3D-Var problem around
    the MAP estimate.
    """
    def __init__(self, configs: dict | ComplexGaussianPosteriorConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)


