# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
Abstract classes for filtering algoirthms (Bayesian and Variational approaches).
A HybridFilter Class is also added, but without clear functionality yet.
Unlike smoothing algorithms, in the case of filters a single observation is given
(e.g., time-independent problems).
The output of a filtering algorithm is a point estimate of the truth.
Uncertainty measure, e.g., posterior covariance, provided if a Bayesian approach is followed.
"""

from abc import abstractmethod
from dataclasses import dataclass
from pyoed.configs import set_configurations
from pyoed import utility
from .assimilation import (
    Filter,
    FilterConfigs,
    FilterResults,
)


@dataclass(kw_only=True, slots=True)
class VariationalFilterConfigs(FilterConfigs):
    """
    Configurations class for the :py:class:`VariationalFilter` abstract base class.
    """
    ...


@set_configurations(VariationalFilterConfigs)
class VariationalFilter(Filter):
    """
    Base class for variational filters.
    In this case, a single point estimate is obtained by solving
    a weighted least-squares optimization problem to minimize
    the mismatch between model prediction and
    observation (in the appropriate projected space, e.g., observation space)
    The mismatch is usually regularized using a penalty term
    (usually asserted by the prior)
    """
    def __init__(self, configs: dict | VariationalFilterConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def objective(
        self,
        init_guess,
        data_misfit_only=False,
    ):
        """
        Evaluate the objective function, and the associated gradient

        :param init_guess: model parameter/state to evaluate the objective function and
            the associated gradient at
        :param bool data_misfit_only: discard the prior/regularization term if `True`.
            This is added for flexibility
        :returns: objective function value, and the gradient;
            (the objective value is a scalar, and the gradient is a one-dimensional
            Numpy array)
        """
        obj_val = self.objective_function_value(
            init_guess=init_guess,
            data_misfit_only=data_misfit_only,
        )
        obj_grad = self.objective_function_gradient(
            init_guess=init_guess,
            data_misfit_only=data_misfit_only,
        )
        return obj_val, obj_grad

    @abstractmethod
    def objective_function_value(self, init_guess, data_misfit_only=False, ):
        """
        A method to evaluate the variational objective function.
        """
        ...

    def objective_function_gradient(self, init_guess, data_misfit_only=False, ):
        """
        A method to evaluate the variational objective function.
        This implementation (by default) provides a gradient approximation using
        finite difference approximation.
        For efficient evaluation of the gradient, the derived class need to provide
        and implementation of the analytical gradient.
        """
        return utility.finite_differences_gradient(
            fun=lambda ig, dm: self.objective_function_value(init_guess=ig, data_misfit_only=dm),
            state=init_guess,
            verbose=self.verbose,
        )


@dataclass(kw_only=True, slots=True)
class VariationalFilterResults(FilterResults):
    """
    Base class for objects holding results of :py:class:`Filter`
    """
    ...



@dataclass(kw_only=True, slots=True)
class BayesianFilterConfigs(FilterConfigs):
    """
    Configurations class for the :py:class:`BayesianFilter` abstract base class.
    """
    ...


@set_configurations(BayesianFilterConfigs)
class BayesianFilter(Filter):
    """
    Base class for Bayesian filtering algorithms.
    In this case, the probability distribution (or an estimate thereof)
    of the model state/parameter is considered.
    The goal is to apply Bayes' theorem, and retrieve the exact/approximate posterior
    or samples from teh posterior
    """
    def __init__(self, configs: dict | BayesianFilterConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)


@dataclass(kw_only=True, slots=True)
class BayesianFilterResults(FilterResults):
    """
    Base class for objects holding results of :py:class:`Filter`
    """
    ...



@dataclass(kw_only=True, slots=True)
class HybridFilterConfigs(FilterConfigs):
    """
    Configurations class for the :py:class:`HybridFilter` abstract base class.
    """
    ...


@set_configurations(HybridFilterConfigs)
class HybridFilter(Filter):
    """
    Base class for Bayesian-variational filters.
    """
    def __init__(self, configs: dict | HybridFilterConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)


@dataclass(kw_only=True, slots=True)
class HybridFilterResults(FilterResults):
    """
    Base class for objects holding results of :py:class:`Filter`
    """
    ...


