# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
Utility functions for the data assimilation module.
These are utility functions for various assimilation algorithms, but are not useful otherwise.
Any utility function that is general will likely be moved to the ``utility`` module.
"""

# TODO: This module requires major update with various functions to be added (from 3dvar, 4dvar, etc.)

from pyoed import utility
from pyoed.assimilation.filtering import VariationalFilter
from pyoed.assimilation.smoothing import VariationalSmoother


def apply_TLM_operator(
    ip,
    state,
    obs_time=None,
    eval_at=None,
    save_all=False,
    scale_by_noise=False,
):
    """
    Apply the tangent linear model (TLM) of the parameter-to-observble map (aka the
    forwrad sensitivities) to the passed state/parameter and return the resulting observation.
    For a linear problem, the TLM is the same as the forwrad operator (model simulation +
    observation operator).
    For a nonlinear problem, the sensitivity array is constructed and is then multiplied
    (transposed) by the passed state/paraemter.

    For an instance of a `VariationalFilter`, all time arguments (`obs_times`, `save_all`)
    are ignored/dropped.

    This function just unifies the interface for both filters and smoothers.
    Specifically, it identifies the underlying inverse problem and chooses the
    acceptable arguments from passed arguments.
    Despite being useful, this method is only for internal purposes (at leasto for now) and
    is not intended not be called directly by the user.

    ..note::
        Values for all arguments should be passed. Default values are added for convenience
        assuming type/assertion checks are carried out by the adjoint operators in the inverse
        problem implementation

    ..note::
        Constructing sensitivity array is not efficient, and should be replaced with matrix-free
        operation.

    :param ip: An inverse prlblem. Expecte to be an instance of one of two classes
        :py:class:`VariationalFilter` or :py:class:`VariationalSmoother`.
        A :py:class:`TypeError` exception is raised otherwise.
    :param state: the model state/parameter to which to the TLM applied (multiplied).

    :returns: the result (observation) of applying the simulation model followed by the
        applying the observation operator
    """
    # Check if the problem is linear or not
    problem_is_linear = utility.inverse_problem_linearity_test(ip)

    if problem_is_linear:
        if isinstance(ip, VariationalSmoother):
            res, _ = ip.apply_forward_operator(
                    state,
                    checkpoints=[obs_time],
                    save_states=save_all,
                    scale_by_noise=scale_by_noise,
                )

            # Extract observation at the passed checkpoint/obs-time
            res = res[obs_time]

        elif isinstance(ip, VariationalFilter):
            res = ip.apply_forward_operator(
                        state,
                        scale_by_noise=scale_by_noise,
                    )

        else:
            raise ValueError(
                "This is not possible!; "
                "Expected only either VariationalFilter/VariationalSmoother instance\n"
                "Please report this as a bug!"
            )

    else:
        # For nonlinear problem, we get the adjoint operator as a matrix
        #    (best we can do for a general-purpose function)
        # Sensitivites (by applying adjoint transposed)
        F_adj = utility.asarray(
            lambda obs: apply_adjoint_operator(
                ip=ip,
                obs=obs,
                obs_time=obs_time,
                eval_at=eval_at,
                save_all=save_all,
                scale_by_noise=scale_by_noise,
            ),
            shape=(ip.prior.size, ip.observation_operator.shape[0])
        )

        res = F_adj.T.dot(state)

    return res

def apply_adjoint_operator(
    ip,
    obs,
    obs_time=None,
    eval_at=None,
    save_all=False,
    scale_by_noise=False,
):
    """
    This function just unifies the interface for both filters and smoothers.
    Specifically, it identifies the underlying inverse problem and chooses the
    acceptable arguments from passed arguments.
    Despite being useful, this method is only for internal purposes (at leasto for now) and
    is not intended not be called directly by the user.

    ..note::
        Values for all arguments should be passed. Default values are added for convenience
        assuming type/assertion checks are carried out by the adjoint operators in the inverse
        problem implementation

    :param ip: An inverse prlblem. Expecte to be an instance of one of two classes
        :py:class:`VariationalFilter` or :py:class:`VariationalSmoother`.
        A :py:class:`TypeError` exception is raised otherwise.
    :param obs: the observation to the TLM transposed is applied to (multiplied by).

    :returns: the result (observation) of applying the simulation model followed by the
        applying the observation operator
    """
    # Retrieve the inverse problem and test its type, then return results accordingly
    if isinstance(ip, VariationalSmoother):
        res = ip.apply_forward_operator_adjoint(
                obs=obs,
                obs_time=obs_time,
                eval_at=eval_at,
                save_all=save_all,
                scale_by_noise=scale_by_noise,
            )[0]

    elif isinstance(ip, VariationalFilter):
        # Discard time, and save all parameters (unacceptable for filtering)
        res = ip.apply_forward_operator_adjoint(
                obs=obs,
                eval_at=eval_at,
                scale_by_noise=scale_by_noise,
            )[0]

    else:
        raise ValueError(
            "This is not possible!; "
            "Expected only either VariationalFilter/VariationalSmoother instance\n"
            "Please report this as a bug!"
        )

    return res

