# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
A module that provides implementation of time-dependent prediction (goal-oriented) operators
for Dolfin (Fenics) time-dependent simulation models.
"""
import numpy as np
import warnings
from dataclasses import dataclass
from typing import Iterable
try:
    import dolfin as dl
    import ufl
except ImportError:
    dl = ufl = None  # TODO: Either this or just raise ImportError (as suggested below!)
    print(
        """\n
          \r***\n  Failed to import Fenics (dolfin);
          \rto use simulation models in this module, you must install Fenics/Dolfin first!
          \rSee information/instruction in 'optional-requirements.txt'
          \r***\n
          """
    )
    warnings.warn(
        f"The AD model with Fenics backend module is loaded, "
        f"however it won't be possible to actually use it witout Fenics/Dolfin installed!"
    )

from pyoed import utility
from pyoed.configs import (
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
)
from pyoed.models.simulation_models import TimeDependentModel
from pyoed.models.observation_operators.fenics_observation_operators import DolfinPointWise
from pyoed.assimilation.core.goal_oriented import (
    GoalOrientedOperatorConfigs,
    GoalOrientedOperator,
)


@dataclass(kw_only=True, slots=True)
class DolfinLinearPredictionOperatorConfigs(GoalOrientedOperatorConfigs):
    """
    Configurations class for the :py:class:`DolfinLinearPredictionOperator` abstract base class.
    This class inherits functionality from :py:class:`GoalOrientedOperatorConfigs` and
    only adds new class-level variables which can be updated as needed.

    :param prediction_time: prediction time (non-negative float)
    :param prediction_coordinates: if given, need to be an array with (x, y, ...)
        coordinates at which to make prediction (restriction/observation operator)
    """
    name: str = "Time-dependent Goal-Oriented Prediction Operator (Fenics/Dolfin)"
    prediction_time: float = 0
    prediction_coordinates: None | Iterable = None


@set_configurations(DolfinLinearPredictionOperatorConfigs)
class DolfinLinearPredictionOperator(GoalOrientedOperator):
    """
    An implementation of a linear goal-oriented time-dependent prediction operator
    for inverse problems with Fenics-based simulation models.
    """
    def __init__(self, configs: dict | DolfinLinearPredictionOperatorConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Create a prediction operator
        self._PREDICTION_OPERATOR = self.update_prediction_operator(
            self.configurations.prediction_coordinates,
        )

    def validate_configurations(
        self,
        configs: dict | GoalOrientedOperatorConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`InverseProblemonfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`GoalOrientedOperatorConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        # Local validation test
        is_nonnegative_float = lambda v: utility.isnumber(v) and v>=0

        ## Validation Stage
        # `prediction_time`: non-negative float
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="prediction_time",
            test=is_nonnegative_float,
            message=f"Prediction time need to be a non-negative float.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # `prediction_coordinates`: None or iterable
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="prediction_coordinates",
            test=lambda v: v is None or utility.isiterable(v),
            message=f"Prediction coordinates need to be None or iterable.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if invalid configurations passed
        """
        # Validate and udpate
        super().update_configurations(**kwargs)

        ## Special updates based on specific arguments

        # Update the inverse problem
        if "prediction_coordinates" in kwargs:
            self.update_prediction_operator(kwargs['prediction_coordinates'])

    def _check_prediction_operator(self):
        """Check if a prediction operator is created..."""
        if self.prediction_operator is None:
            raise TypeError(
                f"No valid prediction operator is recognized; "
                f"A prediction operator is created by using the "
                f"`prediction_coordinates` in the configurations. "
                f"You need to create a prediction operator by calling: "
                f"the method `update_configurations(prediction_coordinates)`"
            )

    def generate_vector(self, init_val=0, return_np=False, ):
        """
        Generate vector compatible with the prediction space
        """
        self._check_prediction_operator()

        p = dl.Vector()
        self.prediction_operator.init_vector(p, 0)

        # Initialize the vector to the given value
        p.set_local(p.get_local()+init_val)
        if return_np:
            p = p.get_local()
        return p

    def update_prediction_operator(self, prediction_coordinates):
        """
        Update the prediction operator based on the prediction coordinates

        :param prediction_coordinates: the prediction coordinates to use to create
            the prediction operator.
            If `None`, the prediction operator is set to `None`
        """
        if prediction_coordinates is None:
            prediction_operator = None
        else:
            # Reference to the simulation
            model = self.inverse_problem.model

            prediction_operator = DolfinPointWise(
                configs=dict(
                    model=model,
                    targets=prediction_coordinates,
                    Vh=model.state_dof,
                )
            )._OBS_OPER

        self._PREDICTION_OPERATOR = prediction_operator
        return prediction_operator

    def apply(self, x):
        """Apply the prediction operator"""
        self._check_prediction_operator()

        # Prediction time
        pred_t = self.configurations.prediction_time

        # Reference to the simulation
        model = self.inverse_problem.model

        # Simulate the model forward to the prediction time
        u = model.integrate_state(
            x,
            tspan=(0, pred_t),
            return_np=False,
        )[-1][-1]

        # Generate prediction vector
        out = self.generate_vector(return_np=False, )
        self.prediction_operator.mult(u, out)
        return out.get_local()


    def apply_adjoint(self, x):
        """Apply the adjoint of the prediction operator"""
        self._check_prediction_operator()

        # Prediction time
        pred_t = self.configurations.prediction_time

        # Reference to the simulation
        model = self.inverse_problem.model

        # Check input
        p = self.generate_vector(return_np=False, )
        p.set_local(x[:])

        # Apply transpose of the restriction operator
        state = model.state_vector(return_np=False, )
        self.prediction_operator.transpmult(p, state)

        # Propage state backward (adjoint pass)
        out = model.Jacobian_T_matvec(
            state,
            eval_at_t=pred_t,
            eval_at=None,
            dt=None,
            return_np=True,
        )
        return out

    @property
    def prediction_operator(self):
        return self._PREDICTION_OPERATOR

    @property
    def size(self):
        pred = self.prediction_operator
        if pred is not None:
            return pred.size(0)
        else:
            return None

