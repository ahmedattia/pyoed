# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
The most simple goal-oriented linear inverse problem.
Linear Inverse problem + Linear goal operator.
"""
from dataclasses import dataclass

from ..core.goal_oriented import (
    GoalOrientedInverseProblemConfigs,
    GoalOrientedInverseProblem,
)
from pyoed import utility
from pyoed.configs import (
    PyOEDConfigsValidationError,
    set_configurations,
    validate_key,
)
from pyoed.utility import inverse_problem_linearity_test


@dataclass(kw_only=True, slots=True)
class GoalOrientedLinearInverseProblemConfigs(GoalOrientedInverseProblemConfigs):
    """
    Configurations class for the :py:class:`GoalOrientedLinearInverseProblem` class.
    This class inherits functionality from :py:class:`GoalOrientedInverseProblemConfigs`
    and only adds new class-level variables which can be updated as needed.
    """
    name: str = "Goal Oriented Linear Inverse Problem"


@set_configurations(GoalOrientedLinearInverseProblemConfigs)
class GoalOrientedLinearInverseProblem(GoalOrientedInverseProblem):

    def __init__(self, configs: dict | GoalOrientedLinearInverseProblemConfigs | None = None) -> None:
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # More init functionality
        ...

    def validate_configurations(
        self,
        configs: dict | GoalOrientedLinearInverseProblemConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Each simulation optimizer **SHOULD** implement it's own function that validates its
        own configurations.  If the validation is self contained (validates all
        configuations), then that's it.  However, one can just validate the
        configurations of of the immediate class and call super to validate
        configurations associated with the parent class.

        If one does not wish to do any validation (we strongly advise against that),
        simply add the signature of this function to the optimizer class.

        .. note::
            The purpose of this method is to make sure that the settings in the
            configurations object `self._CONFIGURATIONS` are of the right type/values
            and are conformable with each other.  This function is called upon
            instantiation of the object, and each time a configuration value is updated.
            Thus, this function need to be inexpensive and should not do heavy
            computations.

        :param configs: configurations to validate. If a :py:class:`InverseProblemonfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.

        :raises PyOEDConfigsValidationError: if the configurations are invalid and
            `raise_for_invalid` is set to True.
        :raises AttributeError: if any (or a group) of the configurations does not exist
            in the optimizer configurations :py:class:`GoalOrientedOperatorConfigs`.
        """
        # Fuse configs into current/default configurations
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        def is_valid_inverse_problem(inverse_problem):
            if inverse_problem is None:
                return True
            elif not inverse_problem_linearity_test(inverse_problem):
                return False
            elif not hasattr(inverse_problem, 'Hessian_inv_matvec'):
                return False
            else:
                return True

        bad_ip_msg = f"The passed inverse problem need to be None or an instance of "
        bad_ip_msg += f"InverseProblem, by linear, and provide an attribute "
        bad_ip_msg += f"`Hessian_inv_matvec`\n"
        bad_ip_msg += f"The passed inverse_problem failed either or all of those tests!"

        # `inverse_problem`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="inverse_problem",
            test=is_valid_inverse_problem,
            message=bad_ip_msg,
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # All good!
        return super().validate_configurations(configs, raise_for_invalid)

    def posterior_covariance_matvec(self, p, ):
        """Multiply the posterior covariance matrix by a vector"""
        # Apply the prediction operator adjoint to the passed
        x = self.goal_operator.apply_adjoint(p)
        x = self.inverse_problem.Hessian_inv_matvec(x, )
        p = self.goal_operator.apply(x)
        return p

