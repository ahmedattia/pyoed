# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements several variants of the Kalman Filter (KF)
data assimilation (inversion/inferencce) scheme.
The filter consists of two steps; namely forecast (prediction) and analysis (correction).
In the former, the model integrates (propagates) the model state forward in time,
and in the latter, an observation is used to update/correct the model state at that
observation time given the associated noise models.
"""

import numpy as np
from dataclasses import dataclass

from pyoed.configs import (
    set_configurations,
    class_doc_inheritance,
)
from pyoed.models import (
    SimulationModel,
    ErrorModel,
    ObservationOperator,
)
from pyoed.models.simulation_models import (
    TimeDependentModel,
)
from pyoed import utility
from pyoed.models.error_models.Gaussian import (
    isGaussian,
    GaussianErrorModel,
)
from pyoed.assimilation.core import (
    BayesianFilter,
    BayesianFilterConfigs,
    GaussianPosterior,
)


@dataclass(kw_only=True, slots=True)
class KalmanFilterConfigs(BayesianFilterConfigs):
    """
    Configurations class for the :py:class:`KalmanFilter` class.

    :param name: Name of the filter
    """

    name: str | None = "KF: Vanilla Kalman Filtering"


@set_configurations(KalmanFilterConfigs)
class KalmanFilter(BayesianFilter):
    """
    A class implementing the vanilla Kalman Filtering (KF) scheme for state
    inference/inversion, assuming the model equation is on the form
    :math:` \\mathbf{x}_{i+1}=\\mathcal(M) \\mathbf{x}_{i}`,
    and :math:`\\mathbf{x}_{i}` is the model state at ith time instance/step.

    Standard KF inversion/filtering retrieves model state from noisy observations.

    This version is very elementary and is developed as guidelines to create more
    advanced versions:

      - Here, the filter works by applying 'analysis/correction' step to registered prior
        based on the registered observation.
      - The analysis step creates a Gaussian posterior which can be used to make future
        forecasts/prediction (which generates a prior at a future time step).

    This scheme assumes linear models and Gaussian observational noise.
    The inference/inversion (unknown) state is regularized by a Gaussian Prior,
    which is equivalent to 3DVar schemes given the linear-Gaussian assumption holds.

    You can either pass the KF elements upon initialization, or later using the
    proper `register` methods. However, you can't use `solve()`
    before registering all proper elements (simulation model, prior, observations/data,
    and observation operator).

    .. note::

        - The KF scheme assumes the dynamical model and the observational operator
            are both specified.
        - A necessary requirement here is that both the dynamical model and the
            observation operator are both linear, otherwise results are not expected to be good.
        - The prior and the observation error model are both Gaussian
    """

    def __init__(self, configs: KalmanFilterConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

    def validate_configurations(
        self,
        configs: dict | KalmanFilterConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`KalmanFilterConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(
            configs=configs,
        )

        ## Validation Stage
        ...

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def register_model(
        self,
        model: None | TimeDependentModel = None,
    ) -> TimeDependentModel | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.

        :raises TypeError: if the type of passed model is not supported
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...

        # Here, the model need to be time-dependent
        if not isinstance(model, (type(None), TimeDependentModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `TimeDependentModel`"
            )

        if model is not None:
            if self.prior is not None:
                if not model.is_state_vector(self.prior.mean):
                    raise TypeError(
                        f"The passed model doesn't seem to be compatible with "
                        f"the registered prior .\n"
                        f"The models has {model.state_size=}\n"
                        f"The models has {model.parameter_size=}\n"
                        f"The prior dimension is {self.prior.size=}"
                    )

        # Return the model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
        initiate_posterior: bool = True,
    ) -> ErrorModel | None:
        """
        Update prior/regularization information. Since, changing the prior implies
        a different posterior, the argument `initiate_posterior` is added with
        defaul value `True` to make sure the posterio is overwritten/reset

        :raises: TypeError is raised if:

            - the passed `prior` type is not supported
            - a valid simulation model has not been registered yet
            - the prior is not compatible with the registered simulation model

        .. note::
            The prior here is asserted on the model state.
        """
        # Associate with self and do super validation/testing
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # start with class derived from pyoed.models.error_models.*
        if prior is not None:

            if not isGaussian(prior):
                raise TypeError(
                    f"Currently, only Gaussian error models instances are allowed\n"
                    f"Received: {prior=} of {type(prior)=}"
                )

            if self.model is not None:
                if not self.model.is_state_vector(prior.mean):
                    raise TypeError(
                        f"The passed prior doesn't seem to be compatible with "
                        f"the registered simulation model!\n"
                        f"Expected prior size to be: {self.model.state_size=};\n "
                        f"Passed prior has {prior.size=}\n"
                    )

        # Initiate the (Gaussian) posterior
        if initiate_posterior:
            if prior is None:
                self._POSTERIOR = None
            else:
                # Covariances must be updated after solving the inverse problem
                self._POSTERIOR = GaussianPosterior(
                    configs=dict(
                        mean=prior.mean,
                        variance=prior.covariance_matrix(),
                    )
                )

        return prior

    def forecast(
        self,
        tspan,
        posterior=None,
    ):
        """
        Apply the forecast/prediction step of the filter (simulate state over a timespan).
        This is carried out by propagating the posteiror/analysis distribution
        including mean and covariance matrix
        over the passed prediction timespan (t0, tf).

        :param tuple tspan: (t0, tf) defining the forecast timespan.
            The analysis/posterior is supposed to be defined at t0, and the
            forecast (prediction) is evaluated at tf.
        :param GaussianErrorModel posterior: the posterior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the internally saved posterior (if available) is used.

        :returns: the Gaussian forecast (GaussianErrorModel)
        """
        # Verify that all inversion elements are properly registered
        if self.model is None:
            raise TypeError(
                f"Can't carry out forecast without a registerd simulation model"
                f"Make sure you call `register_model(model) first"
            )

        if posterior is None:
            posterior = self._POSTERIOR
        if posterior is None:
            raise ValueError(
                "The posterior has not been evaluated; "
                f"either pass an explicit posterior, or call the analysis step first!"
            )
        elif not isGaussian(posterior):
            raise TypeError(
                f"Expected poterior to be instance of `GaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        t0, tf = tspan
        assert tf > t0 >= 0, "(t0, tf) must be tuple with tf > t0 >= 0"

        state_size = self.model.state_size

        xa = posterior.mean
        Pa = posterior.covariance_matrix()

        # Generate forecast state (forward simulation)
        _, trajectory = self.model.integrate_state(
            xa,
            tspan=tspan,
        )
        xf = trajectory[-1]

        # Generate a model matrix/array representation
        Pa_sqrt = utility.factorize_spsd_matrix(
            Pa,
            shape=Pa.shape,
            lower=True,
        )

        # Apply model from both sides to the analysis error covariance matrix
        Pf_sqrt = np.empty((state_size, state_size))
        for j in range(state_size):
            Pf_sqrt[:, j] = self.model.integrate_state(
                Pa_sqrt[:, j],
                tspan=tspan,
            )[
                1
            ][-1]
        Pf = np.dot(Pf_sqrt, Pf_sqrt.T)

        forecast = GaussianErrorModel(
            configs={
                "mean": xf,
                "variance": Pf,
                # "sparse": False,
            }
        )

        return forecast

    def analysis(
        self,
        prior=None,
        update_posterior=False,
    ):
        """
        Analysis step of the (Vanilla Kalman) filter. This function uses the prior and
            the registered observtion to evaluate/update the posterior (Gaussian in this case).

        :param GaussianErrorModel prior: the prior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the registerd prior (if available) is used.

        :returns: the Gaussian posterior model

        .. note:
           The returned posterior is a copy from an internal posterior instance used
            for further forecasts.
        """
        if prior is None:
            prior = self.prior
        if any(
            [
                prior is None,
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
                f"\n***Also***: \n"
                f"{prior=}\n"
                f"{self.prior=}"
            )

        if not isGaussian(prior):
            raise TypeError(
                f"Expected poterior to be instance of `GaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        # state and observation vector dimensions
        state_size = self.model.state_size
        observation_size = self.observation_operator.observation_size

        # get the forecast state, and the forecast error covariance matrix.
        xf = prior.mean
        Pf = utility.asarray(prior.covariance_matrix())

        # get the measurements vector
        observation = self.observations[self.observation_operator.extended_design]

        # Get a reference to registered operators
        obs_oper = self.observation_operator
        obs_err_model = self.observation_error_model

        # Innovation or measurement residual: y - H(x)
        innov = observation - obs_oper(xf)

        # Construct an array representation of the observation operator, its transpose,
        #   and observation error covariances
        H = utility.asarray(obs_oper, shape=obs_oper.shape)
        R = utility.asarray(obs_err_model.covariance_matrix())
        #

        # Kalman Gain
        S = H @ Pf @ H.T + R
        K = Pf @ H.T @ np.linalg.inv(S)

        # Update
        xa = xf + K @ innov
        Pa = (np.eye(state_size) - K @ H) @ Pf

        if self.debug:
            print("Analysis Step: prior.mean: ", xf)
            print("Analysis Step: prior.covariance_matrix: ", Pf)
            print("Analysis Step: observation: ", observation)
            print("Analysis Step: H: ", H)
            print("Analysis Step: R: ", R)
            print("Analysis Step: S: ", S)
            print("Analysis Step: K: ", K)
            print("Analysis Step: xa: ", xa)
            print("Analysis Step: S: ", Pa)

        # Update the posterior if needed
        if update_posterior:
            if self._POSTERIOR is None:
                self._POSTERIOR = GaussianErrorModel(
                    configs={
                        "mean": xa,
                        "variance": Pa,
                        # "sparse": False,
                    }
                )
            else:
                self._POSTERIOR.update_mean(xa)
                self._POSTERIOR.update_covariance_matrix(Pa)
            posterior = self._POSTERIOR

        else:

            # Create another instance to return
            posterior = GaussianPosterior(
                configs={
                    "mean": xa,
                    "variance": Pa,
                    # "sparse": False,
                }
            )
        # Return the calculated posterior
        return posterior

    def solve(
        self,
        tspan,
        forecast_first=True,
        prior=None,
        posterior=None,
        update_posterior=True,
    ):
        """
        Solve the filtering problem over one assimilation cycle.
        Here, there are two options based on the value of `forecast_first`:

        - If `forecast_first` is `True` the posterior is first propagated
          over the timespan, and then the analysis is carried out

        - If `forecast_first` is `False`, the analysis is carried out first,
          and then the forecast over the timespan is made.

        In both cases, this returns the distribution (posterior/prior) at the
        end of the assimilation cycle `tspan`.

        .. note::
            If `forecast_first` is `True`, the posterior at the begining
            of `tspan` is either passed to `posterior` or extracted from `self`.
            Similarily, if `forecast_first` is `False`, the prior at the
            begining of `tspan` is either passed to `prior`
            or extracted from `self`.
        """
        if forecast_first:
            return self._solve_forcast_first(
                tspan=tspan, posterior=posterior, update_posterior=update_posterior
            )
        else:
            return self._solve_analysis_first(
                tspan=tspan, prior=prior, update_posterior=update_posterior
            )

    def _solve_forcast_first(self, tspan, posterior=None, update_posterior=False):
        """Solve the filtering problem by applying forecast then analysis/correction"""
        prior = self.forecast(tspan=tspan, posterior=posterior)

        posterior = self.analysis(prior=prior, update_posterior=update_posterior)

        return posterior

    def _solve_analysis_first(self, tspan, prior=None, update_posterior=False):
        """Solve the filtering problem by applying analysis/correction then forecast"""
        posterior = self.analysis(prior=prior, update_posterior=update_posterior)
        prior = self.forecast(tspan=tspan, posterior=posterior)

        return prior

