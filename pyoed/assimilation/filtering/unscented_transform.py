# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements several variants of the Unscented Kalman Filter (UKF)
data assimilation (inversion/inferencce) scheme.
"""

import numpy as np
from dataclasses import dataclass

from pyoed.configs import (
    validate_key,
    set_configurations,
    PyOEDObject,
    PyOEDConfigs,
    PyOEDConfigsValidationError,
)
from pyoed.models import (
    SimulationModel,
    ErrorModel,
    ObservationOperator,
)
from pyoed.models.simulation_models import (
    TimeDependentModel,
)
from pyoed import utility
from pyoed.models.error_models.Gaussian import (
    isGaussian,
    GaussianErrorModel,
)
from pyoed.assimilation.core import (
    BayesianFilter,
    BayesianFilterConfigs,
    GaussianPosterior,
)


@dataclass(kw_only=True, slots=True)
class UnscentedTransformConfigs(PyOEDConfigs):
    """
    Configurations class for the :py:class:`UnscentedTransform` class.

    :param mean: The mean of the distribution to generate sigma points of
    :param covariance: The covariance matrix of the distribution to generate sigma points of
    :param scheme: the name of the scheme used for generating the data points
    :param alpha: first parameter to be used for `scaled` sigma point generation scheme
    :param beta: second parameter to be used for `scaled` sigma point generation scheme
    :param kappa: third parameter to be used for `scaled` sigma point generation scheme
    :param w0: The weight parameter of the `mean` sigma point generation scheme
    """
    mean: np.ndarray | None = None
    covariance: np.ndarray | None = None
    scheme: str = "min"
    alpha: float = np.sqrt(3)
    beta: float = 2.0
    kappa: float = 1.0
    w0: float = 1.0 / 3.0
    name: str | None = "UT: Unscented Transform"


@set_configurations(UnscentedTransformConfigs)
class UnscentedTransform(PyOEDObject):
    """
    The unscented transform based on mean and covariance matrix of a distribution.
    """
    _SUPPORTED_SCHEMES = ['min', 'base', 'mean', 'scaled']

    def __init__(self, configs: UnscentedTransformConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # post validation initialization
        self.configurations.scheme = self.configurations.scheme.strip().lower()

        # Lazy evaluation of covariance Cholesky factor and sigma points
        self._COVARIANCE_STDEV = None
        self._SIGMA_POINTS = None

    def validate_configurations(
        self,
        configs: dict | UnscentedTransformConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`UnscentedTransformConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(
            configs=configs,
        )

        ## Local tests
        is_valid_scheme = lambda s: s.strip().lower() in self._SUPPORTED_SCHEMES
        def is_valid_mean(x):
            covariance = aggregated_configs.covariance

            if not isinstance(x, np.ndarray):
                return False
            else:
                if x.ndim==1 and covariance.shape == (x.size, x.size):
                    return True
                else:
                    return False

        def is_valid_covariance(C):
            mean = aggregated_configs.mean

            if not isinstance(C, np.ndarray):
                return False
            else:
                if C.shape == (mean.size, mean.size):
                    return True
                else:
                    return False

        ## Validation Stage
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="mean",
            test=is_valid_mean,
            message=f"The mean is invalid; expected 1d numpy array",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="covariance",
            test=is_valid_covariance,
            message=f"The covariance is invalid; expected 2d squared numpy array",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="scheme",
            test=is_valid_scheme,
            message=f"The scheme is invalid; expected one of {self._SUPPORTED_SCHEMES}",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        for key in ["alpha", "beta", "kappa"]:
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key=key,
                test=utility.isnumber,
                message=f"The configuration parameter {key} is invalid; expected number",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="w0",
            test=lambda v: utility.isnumber(v) and (0 <= v < 1),
            message=f"The configuration parameter w0 is invalid; expected number in [0, 1)",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises PyOEDConfigsValidationError: if any of the passed keys in
            `kwargs` is invalid/unrecognized
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        #
        if "scheme" in kwargs:
            self.configurations.scheme = kwargs['scheme'].strip().lower()

        if "w0" in kwargs and self.scheme == "mean":
            self._SIGMA_POINTS = None

        if self.scheme == "scaled" and any([v in kwargs for v in ["alpha", "beta", "kappa"]]):
            self._SIGMA_POINTS = None

        for key in ["mean", "covariance", "scheme"]:
            if key in kwargs:
                # Reset sigma points
                self._SIGMA_POINTS = None
                break
        if "covariance" in kwargs:
            # Destroy the cholesky factor
            self._COVARIANCE_STDEV = None

    def _create_sigma_points(self):
        """
        Construct the sigma points along with the weights needed to reconstruct
        the mean and the covariance of the underlying distribution.
        """
        # TODO: Proceed here ...
        match self.scheme:
            case 'min':
                # Each column is a repetition of the mean
                S = self.mean[:, None].repeat(self.num_sigma_points, axis=1)
                S[:, 1: ] += np.sqrt(self.size) * self.covariance_stdev

            case 'base':
                S = self.mean[:, None].repeat(self.num_sigma_points, axis=1)
                L = np.sqrt(self.size) * self.covariance_stdev
                S[:, :self.size] += L
                S[:, self.size: ] -= L

            case 'scaled':
                S = self.mean[:, None].repeat(self.num_sigma_points, axis=1)
                L = self.alpha * np.sqrt(self.kappa) * self.covariance_stdev
                S[:, 1: self.size+1] += L
                S[:, self.size+1: ] -= L

            case 'mean':
                kappa = self.size / (1.0 - self.w0)
                S = self.mean[:, None].repeat(self.num_sigma_points, axis=1)
                L = np.sqrt(kappa) * self.covariance_stdev
                S[:, 1: self.size+1] += L
                S[:, self.size+1: ] -= L

            case default:
                raise PyOEDConfigsValidationError(
                    f"The registered {self.scheme=} should have never been accepted!"
                )
        return S

    def _calculate_mean_weights(self):
        """Evaluate the weights associated with each sigma point to evaluate the mean"""
        match self.scheme:
            case 'min':
                w = np.hstack((1.0, np.zeros((self.size))))

            case 'base':
                w = np.ones((self.num_sigma_points)) / self.num_sigma_points

            case 'scaled':
                a2k = self.alpha**2 * self.kappa
                w = np.ones((self.num_sigma_points)) / (2.0 * a2k)
                w[0] = (a2k - self.size) / a2k

            case 'mean':
                kappa = self.size / (1.0 - self.w0)
                w = np.r_[self.w0, np.ones(2 * self.size) / (2 * kappa)]

            case default:
                raise PyOEDConfigsValidationError(
                    f"The registered {self.scheme=} should have never been accepted!"
                )
        return w

    def _calculate_covariance_weights(self):
        """Evaluate the weights associated with each sigma point to evaluate the covariance"""
        match self.scheme:
            case 'min':
                w = np.hstack((0.0, np.ones((self.size)) / self.size))

            case 'base':
                w = np.ones((self.num_sigma_points)) / self.num_sigma_points

            case 'scaled':
                a2k = self.alpha**2 * self.kappa
                w = np.ones((self.num_sigma_points)) / (2.0 * a2k)
                w[0] = (a2k - self.size) / a2k + 1 - self.alpha**2 + self.beta

            case 'mean':
                kappa = self.size / (1.0 - self.w0)
                w = np.r_[self.w0, np.ones(2 * self.size) / (2 * kappa)]

            case default:
                raise PyOEDConfigsValidationError(
                    f"The registered {self.scheme=} should have never been accepted!"
                )
        return w


    def reconstruct(self, sigma_points, mean_weights, covariance_weights):
        """
        Reconstruct the mean and the covariance from sigma points, means weights, and means covariances.
        """
        # Evaluate mean (weighted average of columns)
        mean = sigma_points @ mean_weights

        # Calcuate sigma points deviation (column subtraction) and use for covariance evaluation
        S = sigma_points
        Sm = S - mean[:, None]
        covariance = Sm @ (covariance_weights * Sm).T

        return mean, covariance


    @property
    def sigma_points(self):
        """
        Return (create if not available) the sigma points.
        Two-dimensional Array with each COLUMN set as one sigma point.
        """
        if self._SIGMA_POINTS is None:
            self._SIGMA_POINTS = self._create_sigma_points()
        return self._SIGMA_POINTS

    @property
    def mean_weights(self):
        """Return the weights associated with each sigma point to evaluate the mean"""
        return self._calculate_mean_weights()
    @property
    def covariance_weights(self):
        """Return the weights associated with each sigma point to evaluate the covariance"""
        return self._calculate_covariance_weights()

    @property
    def num_sigma_points(self):
        """Number of sigma points (based on the current sigma-point generation scheme)"""
        match self.scheme:
            case 'min':
                N = self.size + 1

            case 'base':
                N = 2 * self.size

            case 'mean':
                N = 2 * self.size + 1

            case 'scaled':
                N = 2 * self.size + 1

            case default:
                raise PyOEDConfigsValidationError(
                    f"The registered {self.scheme=} should have never been accepted!"
                )

        return N

    @property
    def size(self):
        """Dimension of the spase (size)"""
        return self.mean.size

    @property
    def w0(self):
        """weight parameter of the `mean` generation scheme"""
        return self.configurations.w0
    @w0.setter
    def w0(self, val):
        self.update_configurations(w0=val)
    @property
    def alpha(self):
        """alpha parameter of the `scaled` generation scheme"""
        return self.configurations.alpha
    @alpha.setter
    def alpha(self, val):
        self.update_configurations(alpha=val)

    @property
    def beta(self):
        """beta parameter of the `scaled` generation scheme"""
        return self.configurations.beta
    @beta.setter
    def beta(self, val):
        self.update_configurations(beta=val)

    @property
    def kappa(self):
        """kappa parameter of the `scaled` generation scheme"""
        return self.configurations.kappa
    @kappa.setter
    def kappa(self, val):
        self.update_configurations(kappa=val)

    @property
    def mean(self):
        """Return the registered mean"""
        return self.configurations.mean
    @mean.setter
    def mean(self, val):
        """Update the distribution mean"""
        self.update_configurations(mean=val)

    @property
    def covariance(self):
        """Return the registered covariance matrix"""
        return self.configurations.covariance
    @covariance.setter
    def covariance(self, val):
        """Update the distribution covariance matrix"""
        self.update_configurations(covariance=val)

    @property
    def scheme(self):
        """Return the registered sigma-point generation scheme"""
        return self.configurations.scheme
    @scheme.setter
    def scheme(self, val):
        """Update the sigma-point generation scheme"""
        self.update_configurations(scheme=val)

    @property
    def covariance_stdev(self):
        """
        Return (evaluation if not available) the lower Cholesky factor
        of the registered covariance matrix
        """
        # Lazy evaluation
        if self._COVARIANCE_STDEV is None:
            self._COVARIANCE_STDEV = np.linalg.cholesky(self.covariance, )
        return self._COVARIANCE_STDEV


