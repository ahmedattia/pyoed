# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
This module implements several variants of the three-dimensional variational (3D-Var)
data assimilation scheme.
3D-Var refers to simulation/prediction in three-dimensional spatial coordinates.
The name 3D-Var is popular in the atmospheric weather prediction (NWP) literature.
This algorithm is equivalent to standard regularized least-squares inversion.
Single observation (for both time independent or time dependent models) is used to
update the model parameter
"""

import warnings
import re
import inspect
from dataclasses import dataclass
from typing import Type
import numpy as np
from scipy import optimize as sp_optimize
import scipy.sparse.linalg as splinalg

from pyoed import utility
from pyoed.configs import (
    set_configurations,
    validate_key,
)
from pyoed.models import(
    ErrorModel,
    ObservationOperator,
)
from pyoed.models.simulation_models import (
    SimulationModel,
)
from pyoed.models.error_models.Gaussian import isGaussian
from pyoed.assimilation.core import (
    VariationalFilter,
    VariationalFilterConfigs,
    VariationalFilterResults,
    GaussianPosterior,
)
from pyoed.optimization import (
    create_optimizer,
    Optimizer,
    OptimizerConfigs,
    ScipyOptimizer,
    ScipyOptimizerConfigs,
)


@dataclass(kw_only=True, slots=True)
class VanillaThreeDVarConfigs(VariationalFilterConfigs):
    """
    Configurations class for the :py:class:`VanillaThreeDVar` abstract base class.
    """
    name: str | None= "3D-Var: Three Dimensional Variational Data Assimilation"
    invert_for: str = "parameter"
    optimizer: None | Optimizer = ScipyOptimizer
    optimizer_configs: None | OptimizerConfigs = ScipyOptimizerConfigs

    # NOTE: The optimization_preconditioner configuration is remove.


@set_configurations(VanillaThreeDVarConfigs)
class VanillaThreeDVar(VariationalFilter):
    """
    A class implementing the vanilla 3D-Var DA scheme to invert for model state or
    parameter, assuming the model equation is on the form
    :math:`\\mathbf{x}=\\mathcal{M} \\mathbf{p}`, and :math:`\\mathbf{p},\\mathbf{x}`
    are the model parameter and state respectively.  Standard 3D-Var inversion retrieves
    model state from observations for time-independent models. This class allows
    inversion for model state or model parameter respectively This can be decided by
    upon inistantiation using the configuration key `invert_for`.

    Similar to :py:class:`pyoed.assimilation.smoothing.fourDVar.VanillaFourDVar`, this
    version is very elementary and is developed as guidelines to create more advanced
    versions, e.g., parameter-retrieval, or extended (parameter-state) retrieval for
    inifinite dimensional formulations; see remrks below for assumptions about the
    dynamical model.

    This scheme assumes Gaussian observational noise.  The model unknown parameter is
    regularized (to avoid overfitting to noise) by assuming a prior (usually a
    Gaussian), or by specifying a regularization matrix and following an :math:`\\ell_2`
    regularization approach.

    You can either pass the 3D-Var elements upon initialization, or later using the
    proper `register` methods. However, you can't use `solve()` before
    registering all proper elements (simulation model, prior, observations/data,
    observation operator, and linear and optimization solvers)

    :param dict configs: a dictionary containing configurations of the 3D-Var scheme.
        You can see default configurations by calling:

        ```VanillaThreeDVar.get_default_configurations()```

        Default Configurations include:

            - ``model``: simulation model. See the remarks for assumptions made about
              the dynamical/simulation model instance.
            - ``invert_for``: the objective of solving the inverse problem; this could
              be 'state' or 'parameter'. In the former case, the dynamical model is not
              needed, while in the latter, a model must be provided with a method
              :py:meth:`Jacobian_T_matvec` provided by the model instance.
            - ``prior``: Background/Prior model (e.g., GaussianErrorModel)
            - ``observation_operator``: operator to map model state to observation space
            - ``observation_error_model``: Observation error model (e.g., GaussianErrorModel)
            - ``optimization_routine``: string describing the package and the algorithm
              to use for optimizatio

                .. note::
                    Call the staticmethod `VanillaThreeDVar.show_supported_optimizers()`
                    for a list of optimizers implemented

            - ``output``: a dictionary controlling screen/file output configurations
              (control model verbosity):

                - ``output_dir``: Path to folder (will be created if doesn't exist) to
                  which reports/output, etc., are written.

    :remarks:
        - The 3D-Var scheme assumes the dynamical model and the observational operator
            are both specified. In other formulations, these two operators are augmented in
            a single operator :math:`\\mathcal{F}` which is called the forward operator
            (aka parameter-to-observable map).
            The dunamical model maps the moel parameter to the state by forward solution
        - In this version of 3D-Var, we assume the model state is on the *Discretized*
          form :math:`y = M(x)`, where :math:`M` is the discretized forward model that
          projects the model parameter :math:`x` onto the state space, that is :math:`M`
          is a parameter-to-state map.  A requirement here is that the model provides an
          implementation of a method `Jacobian_T_matvec` which multiplies the tangent
          linear model (TLM) i.e., the derivative of :math:`M` with respect to the
          parameter , by a given parameter vector.
    """
    # Hard-coded optimizers we support out of the box
    _SUPPORTED_OPTIMIZERS = [
        "ScipyOptimizer",
    ]

    def __init__(self, configs: VanillaThreeDVarConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        ## Add Internally preserved (local) variables

        # The ptimization routine (overwrite default one)
        self.register_optimizer(
            optimizer=self.configurations.optimizer,
            optimizer_configs=self.configurations.optimizer_configs,
        )

        self.configurations.invert_for = self.configurations.invert_for.lower().strip()

    def register_optimizer(
        self,
        optimizer: str | Optimizer | Type[Optimizer] = ScipyOptimizer,
        optimizer_configs: None | dict | OptimizerConfigs | Type[OptimizerConfigs] = None,
    ) -> Optimizer:
        """
        Register (and return) an optimization routine, and make sure the
        objective function in the optimization routine is set to the objctive function of
        this objective :py:meth:`objective_function_value`.
        The objective function of the optimizer (and its derivatives) are set to the
        objective function and derivatives implemented here **unless the objective function is
        passed explicitly in the configurations**.
        This method sets a reference to the optimizer and update the underlying configurations
        accordingly.

        .. note::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            and valid configurations `optimizer_configs` is passed, the optimizer is
            updated with the passed configurations by calling
            `optimizer.update_configurations(optimizer_configs)`.

        .. warning::
            If the optimizer passed is an instance of (derived from) :py:class:`Optimizer`,
            the responsibility is on the developer/user to validate the contents of the
            optimizer.

        :param optimizer: the optimization routine (optimizer) to register.
            This can be one of the following:

            - An optimizer instance (object that inherits :py:class`Optimizer`).
              In this case, the optimizer is registered **as is** and is updated
              with the passed configurations if available.
            - The class (subclass of :py:class:`Optimizer`) to be used to instantiate
              the optimizer.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.
            - The name of the optimizer (`str`). This has to match the name of one
              of the available optimization routine.
              To see available *built-in* optimization routines, see/call
              :py:meth:`show_supported_optimizers`.

        :param optimizer_configs: the configurations of the optimization routine.
            This can be one of the following:

            - `None`, in this case configurations are discarded, and whatever default
              configurations of the selected/passed optimizer are employed.
            - A `dict` holding full/partial configurations of the selected optimizer.
              These are either used to instantiate or update the optimizer configurations
              based on the type of the passed `optimizer`.
            - A class providing implementations of the configurations (this must be
              a subclass of :py:class:`OptimizerConfigs`.
            - An instance of a subclass of :py:class:`OptimizerConfigs` which is to
              set/udpate optimizer configurations.

        :returns: the registered optimizer.

        :raises TypeError: if the type of passed optimizer and/or configurations
            are/is not supported
        """
        if isinstance(optimizer, str) and optimizer in self.supported_optimizers:
            # Convert class name to class
            optimizer = eval(optimizer)

        # Create a dictionary with configurations to be forced
        new_configs = {
            "fun": self.objective_function_value,
            "jac": self.objective_function_gradient,
            "verbose": self.verbose,
        }

        try:
            optimizer = create_optimizer(
                optimizer=optimizer,
                optimizer_configs=optimizer_configs,
                new_configs=new_configs,
            )
        except Exception as err:
            raise TypeError(
                "Failed to create/update the optimizer! "
                f"See the error below for additional details\n"
                f"Unexpected {err=} of {type(err)=}"
            )
            raise

        # Update optimizer and configurations properly
        return super().register_optimizer(
            optimizer=optimizer,
        )

    def validate_configurations(
        self,
        configs: dict | VanillaThreeDVarConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`VanillaThreeDVarConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        # Local tests
        is_int = lambda x: utility.isnumber(x) and (x == int(x))
        is_positive_int = lambda x: is_int(x) and (x > 0)

        def is_valid_inference(invert_for):
            if re.match(r"\Aparameter\Z", invert_for, re.IGNORECASE):
                return True
            elif re.match(r"\Astate\Z", invert_for, re.IGNORECASE):
                return True
            else:
                return False

        def is_valid_optimizer(optimizer):
            if inspect.isclass(optimizer):
                return issubclass(optimizer, Optimizer)
            elif isinstance(optimizer, Optimizer):
                return True
            elif isinstance(optimizer, str):
                return optimizer in self.supported_optimizers
            else:
                return False

        # `optimizer`:
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="optimizer",
            test=is_valid_optimizer,
            message=f"Invalid optimizer",
            raise_for_invalid=raise_for_invalid,
        ):
            return False


        # `invert_for`
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="invert_for",
            test=is_valid_inference,
            message=f"Invalid value of `invert_for`",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def update_configurations(self, **kwargs):
        """
        Take any set of keyword arguments, and lookup each in
        the configurations, and update as nessesary/possible/valid

        :raises TypeError: if any of the passed keys in `kwargs` is invalid/unrecognized

        :remarks:
            - Generally, we don't want actual implementations in abstract classes,
              however, this one is provided as good guidance.
              Derived classes can rewrite it and/or provide additional updates.
        """
        # Check that the configurations are valid
        super().update_configurations(**kwargs)

        # Optimizer and/or optimizer configurations
        if "optimizer" in kwargs:
            self.register_optimizer(
                optimizer=kwargs["optimizer"],
                optimizer_configs=kwargs.get("optimizer", None),
            )
        elif "optimizer_configs" in kwargs:
            optimizer_configs = kwargs["optimizer_configs"]
            if isinstance(optimizer_configs, dict):
                pass
            elif isinstance(optimizer_configs, OptimizerConfigs):
                optimizer_configs = optimizer_configs.asdict()
            else:
                raise TypeError(
                    f"Updating optimizer confiurations requires passing a dictionary or instance "
                    f"of proper Configs class that the optimizer accepts (here "
                    f"{self.optimizer.configurations_class}).\n"
                    f"Passed {optimizer_configs=} of {type(optimizer_configs)}"
                )
            self.optimizer.update_configurations(**optimizer_configs)

        if "invert_for" in kwargs:
            self.configurations.invert_for = kwargs['invert_for'].lower().strip()

    def register_model(
        self,
        model: None | SimulationModel = None,
    ) -> Type[SimulationModel] | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.


        :raises TypeError: if the type of passed model is not supported
        :raises TypeError: if the model does not provide `Jacobian_T_matvec` method
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...
        # Here, the model need to be time-dependent
        if not isinstance(model, (type(None), SimulationModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `TimeDependentModel`"
            )

        if model is not None:
            if self.prior is not None:
                checker = (
                    model.is_parameter_vector
                    if self.invert_for_parameter
                    else model.is_state_vector
                )
                if not checker(self.prior.mean):
                    raise TypeError(
                        f"The passed model doesn't seem to be compatible with "
                        f"the registered prior .\n"
                        f"A model with state/parameter size in the sapce of '{self.invert_for=}' is expected\n"
                        f"The models has {model.state_size=}\n"
                        f"The models has {model.parameter_size=}\n"
                        f"The prior dimension is {self.prior.size=}"
                    )

            # Verify  the model has 'Jacobian_T_matvec' method implemented
            if not hasattr(model, "Jacobian_T_matvec"):
                raise TypeError(
                    "3D-Var requires a method 'Jacobian_T_matvec' to be associated with the "
                    "model"
                )

            if not callable(getattr(model, "Jacobian_T_matvec", None)):
                raise TypeError(
                    "3D-Var requires a valid method 'Jacobian_T_matvec' to be associated "
                    "with the model"
                )

        # Return the model
        return model

    def register_prior(
        self,
        prior: None | ErrorModel = None,
        initiate_posterior: bool = True,
    ) -> Type[ErrorModel] | None:
        """
        Update prior/regularization information. Since, changing the prior implies
        a different posterior, the argument `initiate_posterior` is added with
        defaul value `True` to make sure the posterio is overwritten/reset

        :raises: TypeError is raised if:

            - the passed `prior` type is not supported
            - a valid simulation model has not been registered yet
            - the prior is not compatible with the registered simulation model

        .. note::
            The prior here is asserted on the model state or model parmeter based
            on the `invert_for` flag selected in the configurations dictionary
        """
        # Associate with self and do super validation/testing
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # start with class derived from pyoed.models.error_models.*
        if prior is not None:

            # NOTE: The prior does not need to be Gaussian.
            # NOTE: It just need to provide mean &covariance matrix
            # FIXIT: ...
            # if not isGaussian(prior):
            #     raise TypeError(
            #         f"Currently, only Gaussian error models instances are allowed\n"
            #         f"Received: {prior=} of {type(prior)=}"
            #     )

            if self.model is not None:
                checker = (
                    self.model.is_parameter_vector
                    if self.invert_for_parameter
                    else self.model.is_state_vector
                )
                if not checker(prior.mean):
                    raise TypeError(
                        f"The passed prior doesn't seem to be compatible with "
                        f"the registered simulation model and inversion objective.\n"
                        f"A prior in the sapce of '{self.invert_for}' is expected"
                    )


        # Initiate the (Gaussian) posterior
        if initiate_posterior:
            if prior is None:
                self._POSTERIOR = None
            else:
                # Covariances must be updated after solving the inverse problem
                self._POSTERIOR = GaussianPosterior(configs=dict(mean=prior.mean))

        return prior

    def register_observation_operator(self, observation_operator: None | ObservationOperator):
        """
        The observation operator is a function/map that takes as input a model state,
        and returns an observation instance.

        :param observation_operator: the obsevation operator

        :raises TypeError: if `observation_operator` is not derived
            from :class:`ObservationOperator`
        :raises TypeError: if the passed operator doesn't provide a function
            to apply the tangent linear of the observation operator 'Jacobian_T_matvec'
        :raises TypeError: if the model state does not match the observation operator
            domain size

        .. note::
            We are assuming the observation operator is time-independent,
            however, it provides a method `apply` and/or `__call__` which converts
            a model state into an equivalent observation
        """
        # Associate with self and do super validation/testing
        observation_operator = super().register_observation_operator(
            observation_operator=observation_operator,
        )

        ## Extra testing
        ...

        # If valid operator found, make assertions on shape, attributes, etc.
        if observation_operator is not None:

            # Verify  the observation operator has 'Jacobian_T_matvec' method implemented
            if not hasattr(observation_operator, "Jacobian_T_matvec"):
                raise TypeError(
                    "3D-Var requires a method 'Jacobian_T_matvec' to be associated with the "
                    "observation operator"
                )

            if not callable(getattr(observation_operator, "Jacobian_T_matvec", None)):
                raise TypeError(
                    "3D-Var requires a valid method 'Jacobian_T_matvec' to be associated "
                    "with the observation operator"
                )

        return observation_operator

    def objective_function_value(self, init_guess, data_misfit_only=False, ):
        """
        Evaluate the value of the 3D-Var objective function
        (data misfit + prior/regularization) given the passed `init_guess`
        as an estimate of the true model parameter/state

        :param init_guess: an estimate of the true model parameter/state
        :param bool data_misfit_only: discard the prior/regularization term if `True`.
            This is added for flexibility

        :returns: value of the 3D-Var objective function.
        :raises TypeError: if `init_guess` is not a valid model parameter/state bsed
            on the inverstion objective selected upon instantiation
        :raises TypeError: if any of the necessary components (prior,
            observation, error models, and observation operator) is/are not registered
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
                False if data_misfit_only else (self.prior is None)
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Validate passed init_guess
        self._check_init_guess(init_guess)

        # Solve the model equations  (model parmaeter --> model state)
        if self.invert_for_parameter:
            # init_guess is made for the model parameter
            # Solve the forward problem (parameter->state)
            state = self.model.solve_forward(init_guess.copy(), )
        else:
            # init_guess is made for the model state
            state = init_guess.copy()

        ## 1- Data-Misfit term
        # Extract registered observations, and project onto the design space
        # TODO: Consider this behaviour and see if we can elevate from the inverse probem!
        y = self.observations[self.observation_operator.extended_design]

        # Calculate the data misfit term y - H(x)
        innov = self.observation_operator(state) - y
        scld_innov = self.observation_error_model.covariance_inv_matvec(innov)
        misfit = np.dot(innov, scld_innov)
        if self.debug:
            print("DEBUG: data-misfit: ", misfit * 0.5)

        ## 2- Add the prior term (if requested)
        if data_misfit_only:
            obj = 0.5 * misfit
        else:
            xdiff = init_guess - self.prior.mean
            prior_term = self.prior.covariance_inv_matvec(xdiff)
            prior_term = np.dot(xdiff, prior_term)
            if self.debug:
                print("DEBUG: prior term: ", prior_term)

            # Add prior and data-misfit terms
            obj = 0.5 * (prior_term + misfit)

        return obj

    def objective_function_gradient(self, init_guess, data_misfit_only=False, ):
        """
        Evaluate the gradient of the 3D-Var objective function
        given the passed `init_guess` as an estimate of the true model parameter/state

        :param init_guess: an estimate of the model initial parameter/state
        :param bool data_misfit_only: discard the prior/regularization term if `True`.
            This is added for flexibility

        :returns: the gradient of the 3D-Var cost function
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
                False if data_misfit_only else (self.prior is None)
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Validate passed init_guess
        self._check_init_guess(init_guess)

        # Solve the model equations  (model parmaeter --> model state)
        if self.invert_for_parameter:
            # init_guess is made for the model parameter
            # Solve the forward problem (parameter->state)
            state = self.model.solve_forward(init_guess.copy(), )
        else:
            # init_guess is made for the model state
            state = init_guess.copy()

        ## 1- Data-Misfit term
        # Extract registered observations, and project onto the design space
        # TODO: Consider this behaviour and see if we can elevate from the inverse probem!
        y = self.observations[self.observation_operator.extended_design]

        #
        innov = self.observation_operator(state) - y
        innov = self.observation_error_model.covariance_inv_matvec(innov)
        grad = self.observation_operator.Jacobian_T_matvec(innov, eval_at=state, )

        if self.invert_for_parameter:
            # Evaluate the forward operator adjoint for parameter sensitivities
            grad = self.model.Jacobian_T_matvec(grad, eval_at=init_guess, )

        # 2- Add the prior term (if requested)
        if not data_misfit_only:
            xdiff = init_guess - self.prior.mean
            xdiff = self.prior.covariance_inv_matvec(xdiff)
            if self.debug:
                print(
                    f"DEBUG: xdiff size {xdiff.size}; "
                    f"grad size {grad.size}"
                )
            grad += xdiff

        return grad

    def apply_forward_operator(self, init_guess, scale_by_noise=True, ):
        """
        Apply :math:`F`, the forward operator to the passed parameter/state `x`.
        The forward operator :math:`F` here, refers to the simulation model followed by
        the observation operator. The result is a data point (an observation)

        :param init_guess: data structure holding the model parameter (for parameter
            inversion) or state (for state inversion).
            In the former, a forward model simulation is applied followed by the
            observation operator. In the latter, only observation operator is applied
        :param bool scale_by_noise: if True, the observations are scaled by
            the observation error precision (inverse of the covariance matix)

        :returns: the observations result from forward solving the forward model equations,
            and applying observation operator (with/without) scaling by observation
            error precisions
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                False if not scale_by_noise else (self.observation_error_model is None),
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
            )

        # Validate passed init_guess
        self._check_init_guess(init_guess)

        # Solve the model equations  (model parmaeter --> model state)
        if self.invert_for_parameter:
            # init_guess is made for the model parameter
            # Solve the forward problem (parameter->state)
            state = self.model.solve_forward(init_guess.copy(), )
        else:
            # init_guess is made for the model state
            state = init_guess.copy()

        # Apply observation operator (state --> Observation)
        obs = self.observation_operator(state)

        # Scale by noise (optional)
        if scale_by_noise:
            obs = self.observation_error_model.covariance_inv_matvec(obs)
        return obs

    def apply_forward_operator_adjoint(
        self, obs, eval_at=None, scale_by_noise=True,
    ):
        """
        Apply F^*, the adjoint of the forward operator, to the passed observation.
        Here, F is a composition of the simulation model and the observation
        operator (e.g., simulation followed by restriction).

        :param obs: the vector in observation space to apply the adjoint to
        :param bool scale_by_noise: if True, the observations are scaled by
            the observation error precision
        :param eval_at: parameter/state to linearize the forward operator around,

        :returns: the adjoint
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                False if not scale_by_noise else (self.observation_error_model is None),
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Validate passed observation
        if not self.observation_operator.is_observation_vector(obs):
            raise TypeError(
                f"The passed observation is not of valid type/shape/size "
                f"Recieved: shape: {obs.shape} "
                f"Expected: size: {self.observation_operator.shape[0]}"
            )

        y = obs.copy()
        if scale_by_noise:
            y = self.observation_error_model.covariance_inv_matvec(y)

        try:
            # for linear observation operator, no need to pass state for linearization
            adjoint = self.observation_operator.Jacobian_T_matvec(y)

        except(Exception) as err:
            if eval_at is None:
                print(
                    f"The observation operator raised the error below when attempted "
                    f"applying jacobian-vector-transpose without `eval_at` "
                    f"{err=} of {type(err)=}"
                )
                raise

            if self.invert_for_parameter:
                state = self.model.solve_forward(eval_at)
            else:
                state = eval_at
            adjoint = self.observation_operator.Jacobian_T_matvec(y, eval_at=state)

        if self.debug:
            print(
                f"DEBUG: Prior to Model adjoint solve: "
                f"DEBUG:   passed (rhs) obs size {obs.size}"
                f"DEBUG: Prior to Model adjoint solve:"
                f"DEBUG:   adjoint size {adjoint.size}"
            )

        # Solve model adjoint
        if self.invert_for_parameter:
            # Evaluate the forward operator adjoint for parameter sensitivities
            adjoint = self.model.Jacobian_T_matvec(adjoint, eval_at=eval_at, )

        if self.debug:
            print(
                f"DEBUG: AFTER Model adjoint solve: adjoint size {adjoint.size}"
            )
        return adjoint

    def full_Hessian(self, eval_at=None, data_misfit_only=False, ):
        """
        Construct the Hessian by evaluating the tangent linear model
        This method works by wrapping the forward and the adjoint propagators
        with the covariance matrices.
        """
        Fadj = utility.asarray(
            lambda x: self.apply_forward_operator_adjoint(
                x, eval_at=eval_at, scale_by_noise=False
            ),
            shape=(self.prior.size, self.observation_operator.shape[0]),
        )
        Rinv = self.observation_error_model.precision_matrix()
        Hessian = Fadj @ Rinv @ Fadj.T

        if not data_misfit_only:
            Hessian += self.prior.precision_matrix()
        return Hessian

    def Hessian_matvec(self, vec, eval_at=None, data_misfit_only=False, ):
        """
        Return the product of the Hessian of 3D-Var objective with a vector 'vec'

        :param vec: a vector (state/parameter) to multiply the 3D-Var objective Hessian by
        :param eval_at: parameter/state at which the Hessian is evaluated
            (passed to the model adjoint and/or observtion operator to linearize around)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded,
            and only the data-misfit term is evaluated

        :returns: product of the inverse of the posterior covariance (of the linearized
            problem) by a vector
        """
        ## 1- Data Misfit term (F)
        # Square root of the observation error precision (covariance inverse)
        observation_size = self.observation_operator.shape[0]
        Rinvsqrt = utility.asarray(
            self.observation_error_model.covariance_sqrt_inv_matvec,
            shape=(observation_size, observation_size),
        )

        # Square root of the data-misfit term of the Hessian (state size x observation size)
        inference_size = (
            self.model.state_size
            if not self.invert_for_parameter
            else self.model.parameter_size
        )

        # Each column of Rinvsqrt is propagated backward (through the model adjoint) to the parameter space
        data_Hessmat_sqrt = np.zeros((inference_size, observation_size))
        for j in range(observation_size):
            adjoint = self.apply_forward_operator_adjoint(
                obs=Rinvsqrt[:, j],
                eval_at=eval_at,
                scale_by_noise=False,
            )
            data_Hessmat_sqrt[:, j] += adjoint
        out_vec = data_Hessmat_sqrt.dot(data_Hessmat_sqrt.T.dot(vec))

        ## 2- Add prior term if requested, and return
        if not data_misfit_only:
            out_vec += self.prior.covariance_inv_matvec(vec)

        return out_vec

    def posterior_covariance(self, map_point=None, return_array=True, ):
        """
        Create a linear operator or a numpy array representing the posterior covariance
        (Gaussian/Laplace/approximation around the MAP point estimate).
        If the `map_point` is not passed, the registered posterior map point is used
        (if the inverse problem has been already solved), otherwise, the prior mean is used.
        We advise to explicitly pass a valid `map_point`. Note that if the problem is linear,
        the map_point has no effect.

        :param map_point: solution of the 4D-Var inverse problem;
        :param bool return_array: if `True` construct the posterior covariance matrix
            (as a numpy dense array), otherwise, return a scipy LinearOperator with the posterior
            covariance vector product as the underlying matvec function.

        :returns: posterior covariance matrix/operator
        """
        # get a map_point if not passed
        if map_point is None:
            if self.posterior is not None:
                map_point = self.posterior.mean
            else:
                map_point = self.prior.mean

        # Construct posterior covariance (Laplace) matrix (with linearization at map_point)
        post_cov_oper = lambda x: self.Hessian_inv_matvec(
            x,
            eval_at=map_point,
            data_misfit_only=False,
        )

        if return_array:
            size = (
                self.model.state_size
                if not self.invert_for_parameter
                else self.model.parameter_size
            )
            post_cov = utility.asarray(post_cov_oper, shape=(size, size))

        else:
            post_cov = post_cov_oper

        return post_cov

    def Hessian_inv_matvec(
        self,
        vec,
        eval_at=None,
        data_misfit_only=False,
        precond=None,
        solver="scipy-minres",
        solver_options={"maxiter": None, "callback": None},
    ):
        """
        Return the product of the Hessian inverse of 4D-Var objective with a vector 'state/parameter'

        :param vec: a vector (state/parameter) to multiply the 3D-Var objective Hessian
            inverse by
        :param eval_at: parameter/state at which the Hessian is evaluated
            (passed to the model adjoint and/or observtion operator to linearize around)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded, and only
            the data-misfit term is evaluated
        :param precond: if passed, it is aexpected to be a function on the form
            `precond(x, p)` that accepts a state `x` and a parameter `p`

        :returns: product of the posterior covariance (of the linearized problem)
            with a state vector

        :remarks: this function is very important and resembles the most epensive part of
            linear/linearized Bayesian algorithms. Preconditioning, and/or reduced-order
            approxmiations can be very important for performance here.

        .. note::
            This method works by solving a linear system with `Hessian_matvec`.
            One could just compute the Hessian (in full) and invert it if the
            dimension of the problem (state/parameter) is really small.
        """
        # Slice the passed parameter/state
        vec = utility.asarray(vec).flatten()

        # Check eval_at for linearization if not passed
        if eval_at is None:
            if self.posterior is not None:
                eval_at = self.posterior.mean
            else:
                eval_at = self.prior.mean

        # Define a linear operator A for Hessian_mat_vec application
        matvec = lambda x: self.Hessian_matvec(
            x,
            eval_at=eval_at,
            data_misfit_only=data_misfit_only,
        )
        A = splinalg.LinearOperator(
            (vec.size, vec.size),
            matvec=matvec,
        )

        # Retrieve the preconditioner if given
        if precond is not None:
            M = splinalg.LinearOperator(
                (vec.size, vec.size), matvec=lambda p: precond(eval_at, p)
            )
        else:
            M = None

        # Define an Elementary callback
        callback = solver_options["callback"] if "callback" in solver_options else None

        def report(xk):
            frame = inspect.currentframe().f_back
            try:
                print(f"Solver-Iteration; Residual-Norm: {frame.f_locals['r_norm']}")
            except KeyError:
                pass

        callback = report if callback is None and self.verbose else None
        solver_options.update({"callback": callback})

        if self.debug:
            print("DEBUG: 3DVAR: Preconditioner:...", M)

        # Start solving the linear system H x = b, where x is the passed state and H is the Hessian
        if re.match(r"\A(sp|scipy)( |-|_)*minres\Z", solver, re.IGNORECASE):
            out = splinalg.minres(A=A, b=vec, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*cg\Z", solver, re.IGNORECASE):
            out = splinalg.cg(A=A, b=vec, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*bicgstab\Z", solver, re.IGNORECASE):
            out = splinalg.bicgstab(A=A, b=vec, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*gmres\Z", solver, re.IGNORECASE):
            out = splinalg.gmres(A=A, b=vec, M=M, **solver_options)

        elif re.match(r"\A(sp|scipy)( |-|_)*lgmres\Z", solver, re.IGNORECASE):
            out = splinalg.lgmres(A=A, b=vec, M=M, **solver_options)

        else:
            raise ValueError("Unsupported linear-system solver : '{0}'!".format(solver))

        if out[1] == 0:
            # Success; all good and converged
            out = out[0]

        elif out[1] > 0:
            warnings.warn(
                f"Solver {solver} did not achieve toleranc {solver_options['tol']}"
            )
            out = out[0]

        else:
            msg = ""
            if np.any(np.isnan(state)) or np.any(np.isinf(state)):
                msg += f"Found Inf/Nan values in the right-hand side!\n"
            msg += (
                f"Failed to apply {solver} to solve the linear system involving the"
                " Hessian matrix"
            )
            msg += f":INPUT; rhs: {repr(state[:])}\nOUTPUT of {solver}: {repr(out)}\n"
            raise ValueError(msg)

        return out

    def solve(
        self,
        init_guess=None,
        skip_map_estimate=False,
        update_posterior=False,
    ):
        """
        Solve the inverse problem, i.e., find the model state/parameter given the
        registered observation and prior information

        :param init_guess: initial guess of the model state/parameter
        :param bool skip_map_estimate: use the prior mean as a map estimte
            (ignore observations) and upate posterior.
            This useful for OED when the model  is linear
        :param bool update_posterior: if True, the posterior mean and covariance operator
            are updated (given/around the solution of the inverse problem)

        :returns: the analysis state/parameter (MAP estimate of the posterior)

        :raises:
            - TypeError if any of the 3D-Var elements (model, observation operator,
              prior, solver, or data) is missing
            - ValueError if the underlying optimization routine is not supported yet
            - ValueError if the 'invert_for' is not recognized; only 'state'
              or 'parmeter' are accepted.
            - `AttributeError` is raised if the passed model doesn not provide a method
              'Jacobian_T_matvec' to apply the tangent linear model (TLM/Jacobian)
              transpose of the right-hand-side of the discretized PDE to a model
              parameter.
        """
        # Check to assure all elements are registered
        if not self.check_registered_elements():
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components."
                f"{self.show_registered_elements(display=False, )}"
            )


        # Use prior mean if no initial guess is given
        if init_guess is None: init_guess = self.prior.mean

        # Validate passed init_guess
        self._check_init_guess(init_guess)


        # Initialize the map point estimate
        if skip_map_estimate:
            if self.verbose:
                print("Skipping the MAP estimate as requested as requested...")
            map_point = init_guess
        else:
            if self.verbose:
                print("Solving the 3D-Var inverse problem for the MAP estimate")

            # Placeholder for the map estimate (numpy array or not)
            if self.invert_for_parameter:
                map_point = self.model.parameter_vector(init_val=0.0)
            else:
                map_point = self.model.state_vector(init_val=0.0)

            optimization_results = self.optimizer.solve(
                init_guess,
            )
            map_point = optimization_results.x

        if self.debug:
            print("DEBUG: 1- map: ", map_point)

        # Update the posterior if needed
        if update_posterior:
            if self.verbose:
                print("Updating posterior mean/mode using MAP estimate")

            if self.posterior is None:
                self._POSTERIOR = GaussianPosterior(configs=dict(mean=map_point.copy()))
            else:
                self._POSTERIOR.mean = map_point.copy()

            if self.debug:
                print("DEBUG: 2- map: ", map_point)

            if self.verbose:
                print("Evaluating posterior covariance matrix/operator")

            self.posterior.update_covariance_matrix(self.posterior_covariance(map_point))

            if self.verbose:
                print("Done")

        if self.debug:
            print("DEBUG: 3- map: ", map_point)
        return map_point


    def _check_init_guess(self, init_guess, raise_for_invalid=True, ):
        """
        Validate the passed `init_guess` by checking whther it is a
        valid parameter (if `self.invert_for` is equal to `'parameter'`)
        or a valid state (if `self.invert_for` is equal to `'state'`)

        :returns: whether the `init_guess` is valid or not

        :raises TypeError: if the `init_guess` is not valid and
            `raise_for_invalid` is `True`
        """
        # Validate passed init_guess
        checker = (
            self.model.is_parameter_vector
            if self.invert_for_parameter
            else self.model.is_state_vector
        )
        if checker(init_guess):
            return True
        else:
            if raise_for_invalid:
                raise TypeError(
                    f"The passed init_guess is not of valid type/shape/size\n"
                    f"passed {init_guess=}"
                    f"The initial guess failed the {checker=}"
                )
            else:
                return False


    def show_registered_elements(self, display=True, ) -> None:
        """
        Compose and (optionally) print out a message containing the elements
        of the inverse problem and show what's registered and what's not

        :param display: if `True` print out the composed message about
            registered/unregistered elements

        :returns: the composed message
        """
        all_elements = [
            'model', 'prior', 'observation_operator',
            'observation_error_model', 'observations',
            'optimizer',
        ]

        sep = "*"*40
        registered_message = f"\n\n{sep}\n\tRegistered Elements\n{sep}\n"
        unregistered_message = f"{sep}\n\tUnregistered Elements\n{sep}\n"

        for element in all_elements:
            if getattr(self, element) is None:
                unregistered_message += f"- '{element}'\n"

            else:
                registered_message += f"- '{element}': {getattr(self, element)}\n"

        registered_message += f"{sep}\n"
        unregistered_message += f"{sep}\n"
        msg = registered_message + unregistered_message

        if display: print(msg)
        return msg


    def check_registered_elements(self, ) -> bool:
        """Check if all inversion elements are registered or not"""
        if self.optimizer is None:
            return False
        else:
            return super().check_registered_elements()

    @property
    def invert_for(self):
        """The objective of solving the inverse problem (this can be decided ONLY upon initistantiation)"""
        return self.configurations.invert_for

    @property
    def invert_for_parameter(self):
        """Flag that is True if the inversion is carried out for model parameter"""
        return self.invert_for == "parameter"

    @property
    def supported_optimizers(self, ):
        return self._SUPPORTED_OPTIMIZERS



class DolfinThreeDVar(VanillaThreeDVar):
    """
    Class for solving 3D-Var inverse problems with simulation/forward model equations on
    the form :math:`F(u, m) = 0`, where :math:`u` is the model state, and :math:`m` is
    the model parameter, respectively.

    This formulation is adopted by many simulation packages and backends, e.g., PETSc,
    however, this implementation was originally developed for models implemented in
    Fenics/Dolfin, hence the name.  We may consider renaming with the package being
    expanded.

    Similar to :py:class:`VanillaThreeDVar`, this class allows inversion for model state
    or model parameter respectively.  This can be decided by upon inistantiation using
    the configuration key `invert_for`.  This class inherits most functionality from
    :py:class:`VanillaThreeDVar`.  The main difference is the evaluation of the gradient
    of the 3D-Var cost functional, and the application of the forward operator adjoint.

    This scheme assumes Gaussian observational noise.  The model unknown parameter is
    regularized (to avoid overfitting to noise) by assuming a prior (usually a
    Gaussian), or by specifying a regularization matrix and following an
    :math:`\\ell_2-` regularization approach.

    You can either pass the 3D-Var elements upon initialization, or later using the
    proper `register` methods. However, you can't use `solve()` before
    registering all proper elements (simulation model, prior, observations/data,
    observation operator, and linear and optimization solvers)

    :param dict configs: a dictionary containing configurations of the 3D-Var scheme.
        You can see default configurations by calling:

        ```DolfinThreeDVar.get_default_configurations()```

        Default Configurations include:

            - ``model``: simulation model. See the remarks for assumptions made about
              the dynamical/simulation model instance.
            - ``invert_for``: the objective of solving the inverse problem; this could
              be 'state' or 'parameter'.  In the former case, the dynamical model is
              utilized only to validate states/parameter, however, in the latter the
              dynamical/simulation model is used to access a method to calculate
              forward/adjoint sensitivities.
            - ``prior``: Background/Prior model (e.g., GaussianErrorModel)
            - ``observation_operator``: operator to map model state to observation space
            - ``observation_error_model``: Observation error model (e.g., GaussianErrorModel)
            - ``optimization_routine``: string describing the package and the algorithm
              to use for optimization

                .. note::
                    Call the staticmethod `VanillaThreeDVar.show_supported_optimizers()`
                    for a list of optimizers implemented

            - ``output``: a dictionary controlling screen/file output configurations
              (control model verbosity)

                * ``output_dir``: Path to folder (will be created if doesn't exist)
                  to which reports/output, etc., are written.

    :remarks:
        - The 3D-Var scheme assumes the dynamical model and the observational operator
            are both specified. In other formulations, these two operators are augmented in
            a single operator :math:`\\mathcal{F}` which is called the forward operator
            (aka parameter-to-observable map).
            The dunamical model maps the moel parameter to the state by forward solution
    """

    def register_model(
        self,
        model: None | SimulationModel = None,
    ) -> Type[SimulationModel] | None:
        """
        Register (and return) the simulation model to be registered.
        This calls `InverseProblem.register_model` and adds extra
        assertions/functionality specific for filters.


        :raises TypeError: if the type of passed model is not supported
        :raises TypeError: if the model does not provide `solve_adjoint` method
        """
        # Associate with self and do super validation/testing
        model = super().register_model(model=model)

        ## Extra testing
        ...
        # Here, the model need to be time-dependent
        if not isinstance(model, (type(None), SimulationModel)):
            raise TypeError(
                f"The passed {model=} of {type(model)=} is not supported!\n"
                f"Expected instance of a class derived from `TimeDependentModel`"
            )

        if model is not None:
            if self.prior is not None:
                checker = (
                    model.is_parameter_vector
                    if self.invert_for_parameter
                    else model.is_state_vector
                )
                if not checker(self.prior.mean):
                    raise TypeError(
                        f"The passed model doesn't seem to be compatible with "
                        f"the registered prior .\n"
                        f"A model with state/parameter size in the sapce of '{self.invert_for=}' is expected\n"
                        f"The models has {model.state_size=}\n"
                        f"The models has {model.parameter_size=}\n"
                        f"The prior dimension is {self.prior.size=}"
                    )

            # Verify  the model has 'solve_adjoint' method implemented
            if not hasattr(model, "solve_adjoint"):
                raise TypeError(
                    "3D-Var requires a method 'solve_adjoint' to be associated with the "
                    "model"
                )

            if not callable(getattr(model, "solve_adjoint", None)):
                raise TypeError(
                    "3D-Var requires a valid method 'solve_adjoint' to be associated "
                    "with the model"
                )

        # Return the model
        return model

    def objective_function_gradient(self, init_guess, data_misfit_only=False, ):
        """
        Evaluate the gradient of the 3D-Var objective function
        given the passed `init_guess` as an estimate of the true model parameter/state

        :param init_guess: an estimate of the model initial parameter/state
        :param bool data_misfit_only: discard the prior/regularization term if `True`.
            This is added for flexibility

        :returns: the gradient of the 3D-Var cost function
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
                False if data_misfit_only else (self.prior is None)
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Validate passed init_guess
        self._check_init_guess(init_guess)

        # Solve the model equations  (model parmaeter --> model state)
        if self.invert_for_parameter:
            # init_guess is made for the model parameter
            # Solve the forward problem (parameter->state)
            state = self.model.solve_forward(init_guess.copy(), )
        else:
            # init_guess is made for the model state
            state = init_guess.copy()

        ## 1- Data-Misfit term
        # Extract registered observations, and project onto the design space
        # TODO: Consider this behaviour and see if we can elevate from the inverse probem!
        y = self.observations[self.observation_operator.extended_design]

        #
        innov = self.observation_operator(state) - y
        innov = self.observation_error_model.covariance_inv_matvec(innov)
        grad = self.observation_operator.Jacobian_T_matvec(innov, eval_at=state, )

        if self.invert_for_parameter:
            # Evaluate the forward operator adjoint for parameter sensitivities
            grad = self.model.solve_adjoint(grad, eval_at=init_guess, )

        # 2- Add the prior term (if requested)
        if not data_misfit_only:
            xdiff = init_guess - self.prior.mean
            xdiff = self.prior.covariance_inv_matvec(xdiff)
            if self.debug:
                print(
                    f"DEBUG: xdiff size {xdiff.size}; "
                    f"grad size {grad.size}"
                )
            grad += xdiff

        return grad

    def apply_forward_operator_adjoint(
        self, obs, eval_at=None, scale_by_noise=True,
    ):
        """
        Apply F^*, the adjoint of the forward operator, to the passed observation.
        Here, F is a composition of the simulation model and the observation
        operator (e.g., simulation followed by restriction).

        :param obs: the vector in observation space to apply the adjoint to
        :param bool scale_by_noise: if True, the observations are scaled by
            the observation error precision
        :param eval_at: parameter/state to linearize the forward operator around,

        :returns: the adjoint
        """
        # Verify that all inversion elements are properly registered
        if any(
            [
                self.model is None,
                self.observation_operator is None,
                False if not scale_by_noise else (self.observation_error_model is None),
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
            )

        # Validate passed observation
        if not self.observation_operator.is_observation_vector(obs):
            raise TypeError(
                f"The passed observation is not of valid type/shape/size "
                f"Recieved: shape: {obs.shape} "
                f"Expected: size: {self.observation_operator.shape[0]}"
            )

        y = obs.copy()
        if scale_by_noise:
            y = self.observation_error_model.covariance_inv_matvec(y)

        try:
            # for linear observation operator, no need to pass state for linearization
            adjoint = self.observation_operator.Jacobian_T_matvec(y)

        except(Exception) as err:
            if eval_at is None:
                print(
                    f"The observation operator raised the error below when attempted "
                    f"applying jacobian-vector-transpose without `eval_at` "
                    f"{err=} of {type(err)=}"
                )
                raise

            if self.invert_for_parameter:
                state = self.model.solve_forward(eval_at)
            else:
                state = eval_at
            adjoint = self.observation_operator.Jacobian_T_matvec(y, eval_at=state)

        if self.debug:
            print(
                f"DEBUG: Prior to Model adjoint solve: "
                f"DEBUG:   passed (rhs) obs size {obs.size}"
                f"DEBUG: Prior to Model adjoint solve:"
                f"DEBUG:   adjoint size {adjoint.size}"
            )

        # Solve model adjoint
        if self.invert_for_parameter:
            # Evaluate the forward operator adjoint for parameter sensitivities
            adjoint = self.model.Jacobian_T_matvec(adjoint, eval_at=eval_at, )

        if self.debug:
            print(
                f"DEBUG: AFTER Model adjoint solve: adjoint size {adjoint.size}"
            )
        return adjoint


    def Hessian_matvec(self, vec, eval_at=None, data_misfit_only=False, ):
        """
        Return the product of the Hessian of 3D-Var objective with a vector 'vec'

        :param vec: a vector (state/parameter) to multiply the 3D-Var objective Hessian by
        :param eval_at: parameter/state at which the Hessian is evaluated
            (passed to the model adjoint and/or observtion operator to linearize around)
        :param bool data_misfit_only: if True the prior term of the Hessian is discarded,
            and only the data-misfit term is evaluated

        :returns: product of the inverse of the posterior covariance (of the linearized
            problem) by a vector

        :notes: the implementation here is incorrect if the model is nonlinear; we need
            TLM and proper reformulation as done in :py:class:`VanillaThreeDVar`
        """
        # Verify that all inversion elements are properly registered
        self.check_registered_elements()

        # Validate passed init_guess
        checker = (
            self.model.is_parameter_vector
            if self.invert_for_parameter
            else self.model.is_state_vector
        )
        if not checker(vec):
            raise TypeError("The passed vec is not of valid type/shape/size")

        #
        if not self.invert_for_parameter:
            raise NotImplementedError("TODO")

        else:
            if eval_at is not None and self.debug:
                print("DEBUG: eval_at/parameter", eval_at[:])

            # First Term in Hessian Matvec (incremental forward solve)
            state = self.model.solve_forward(eval_at)
            if np.any(np.isnan(state)):
                raise ValueError("Failed to solve the forward problem!")

            u_inc = self.model.solve_incremental_forward(
                H_dir=vec,
                parameter=eval_at,
                state=state,
                return_np=False,
            )
            Bu_p = self.observation_operator(u_inc)
            scld_Bu_p = self.observation_error_model.covariance_inv_matvec(Bu_p)
            res = self.observation_operator.Jacobian_T_matvec(scld_Bu_p, eval_at=state)

            # Solve adjoint equation

            # 1- Data-Misfit term: H^T(y - H(x)) for state; M^T H^T (y-H(x)) for parameter
            y = self.observations[self.observation_operator.extended_design]  # data/observation

            #
            Hx = self.observation_operator(state)
            innov = Hx - y
            innov = self.observation_error_model.covariance_inv_matvec(innov)
            adj_rhs = self.observation_operator.Jacobian_T_matvec(innov, eval_at=state)

            adj = self.model.solve_adjoint(
                adj_rhs, eval_at=eval_at, solve_for_param=False
            )

            if self.debug:
                print("DEBUG: adj_rhs is valid", self.model.is_state_vector(adj_rhs))
                print("DEBUG: adj is valid", self.model.is_state_vector(adj))

            # Incremental adjoint solve
            adj_inc = self.model.solve_incremental_adjoint(
                H_dir=vec,
                parameter=eval_at,
                adj=adj,
                adj_res=res,
                state=state,
                u_inc=u_inc,
                return_np=False,
            )

            # Combine three terms
            out_vec = self.model.evaluate_second_order_sensitivities(
                H_dir=vec,
                adj=adj,
                inc_state=u_inc,
                inc_adj=adj_inc,
                parameter=eval_at,
                state=state,
                return_np=True,
            )

        # 3- Add prior term if requested, and return
        _out_vec = out_vec[:]
        if not data_misfit_only:
            _out_vec += self.prior.covariance_inv_matvec(vec.copy())[:]

        if isinstance(out_vec, np.ndarray):
            out_vec[:] = _out_vec
        else:
            out_vec.set_local(_out_vec)

        return out_vec


