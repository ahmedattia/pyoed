# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

"""
This module implements several variants of the Kalman Filter (KF)
data assimilation (inversion/inferencce) scheme for complex-valued random variables.
"""

import numpy as np
import warnings

from dataclasses import dataclass
from pyoed.configs import(
    validate_key,
    set_configurations,
)
from pyoed import utility
from pyoed.models.error_models.Gaussian import (
    GaussianErrorModel,
    isGaussian,
    ComplexGaussianErrorModel,
    isComplexGaussian,
)
from pyoed.assimilation.core import ComplexGaussianPosterior
from .kalman import (
    KalmanFilterConfigs,
    KalmanFilter,
)


# TODO: Implemente Two versions of the complex Kalman filter:
# 2- Augmented Complex Kalman Filter (which employs the complex augmented Gaussian formulation)

@dataclass(kw_only=True, slots=True)
class ConventionalComplexKalmanFilterConfigs(KalmanFilterConfigs):
    """
    Configurations class for the :py:class:`ConventionalComplexKalmanFilter` class.
    The configurations here are identical to the parent's :py:class:`KalmanFilterConfigs`
    except that the prior has to be a complex Gaussian distribution.
    """
    name: str | None = "CCKF: Conventional Complex Valued Kalman Filter"


@set_configurations(ConventionalComplexKalmanFilterConfigs)
class ConventionalComplexKalmanFilter(KalmanFilter):
    """
    A class implementing the *simplest* version of Kalman Filtering (KF) scheme
    for complex-valued state inference/inversion, assuming the model equation is on the form
    :math:` \\mathbf{x}_{i+1}=\\mathcal(M) \\mathbf{x}_{i}`,
    and :math:`\\,mathbf{x}_{i}` is the model state at ith time instance/step.

    Standard (conventional) complex-valued KF inversion/filtering retrieves model state from noisy
    observations.
    Unlike :py:class:`KalmanFilter`, this class supports complex-valued states and models,
    that is both :math:`\\mathbf{x}` and :math:`\\mathcal{M}` are potentially complex-valued.

    The observations :math:`\\mathbf{y}`, however, in this implementation are real-valued.

    .. note::
        Everything here is similar to :py:class:`KalmanFilter` except that transposes are replaced
        with transposed conjugets (Hermitian).
        This implicitly assumes proper signal (the random variable is proper), and thus,
        the pseudo-covariance matrix vanishes.

    .. note::
        - The scheme assumes the dynamical model and the observational operator are both specified.
        - A necessary requirement here is that both the dynamical model and the
            observation operator are both linear, otherwise results are not expected to be good.
        - The prior must by complex-valued error-model (e.g., ComplexGaussianErrorModel).
            Thus, if a standard real-valued Gaussian prior (e.g., :py:class:`GaussianErrorModel`)
            is passed, its moments will be used to create an instance of :py:class:`ComplexGaussianErrorModel`
            with same mean, and covariance.
        - Pseudo-covariances are discarded here, and are not evaluated in the posterior
          (they are set to 0+0j in the returned complex Gaussian posterior).
        - It is up to the user to make sure these elements are propoperly specified (at least initially!)
    """

    def __init__(self, configs: ConventionalComplexKalmanFilterConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Now, initiate the posterior
        self.register_prior(self.configurations.prior, initiate_posterior = True)

    def validate_configurations(
        self,
        configs: dict | ConventionalComplexKalmanFilterConfigs,
        raise_for_invalid: bool = True,
    ) -> bool:
        """
        Validate passed configurations.

        :param configs: configurations to validate.
            If a :py:class:`ConventionalComplexKalmanFilterConfigs` object
            is passed, validation is performed on the entire set of configurations.
            However, if a dictionary is passed, validation is performed only on the
            configurations corresponding to the keys in the dictionary.
        """
        aggregated_configs = self.aggregate_configurations(configs=configs, )

        ## Validation Stage
        ...


        # `prior`: None, Gaussian, or Complex Gaussian
        if not validate_key(
            current_configs=aggregated_configs,
            new_configs=configs,
            key="prior",
            test=lambda v: v is None or isGaussian(v) or isComplexGaussian(v),
            message=f"Prior is of invalid type. Expected None, Gaussian, or ComplexGaussian error model.",
            raise_for_invalid=raise_for_invalid,
        ):
            return False

        # Return results of validating super
        return super().validate_configurations(
            configs=configs,
            raise_for_invalid=raise_for_invalid,
        )

    def register_prior(
        self,
        prior: None | GaussianErrorModel | ComplexGaussianErrorModel = None,
        initiate_posterior: bool = True,
    ) -> ComplexGaussianErrorModel | None:
        """
        Update prior/regularization information. Since, changing the prior implies
        a different posterior, the argument `initiate_posterior` is added with
        defaul value `True` to make sure the posterio is overwritten/reset

        :raises: TypeError is raised if:

            - the passed `prior` type is not supported
            - a valid simulation model has not been registered yet
            - the prior is not compatible with the registered simulation model

        .. note::
            The prior here is asserted on the model state.
        """
        if prior is None or isComplexGaussian(prior):
            # Perfect; this is expected
            pass

        elif isGaussian(prior):
            warnings.warn(
                f"Passign Gaussian (non-Complex) prior is non-reliable especially if "
                f"there is no sufficient spin-off time period!\n"
                f"Replacing the Gaussian prior with Complex Gaussian Prior with the "
                f"same mean and variance/covariance. No relation (pseudo covariance) is considered."
            )
            # Convert from standard Gaussian to ComplexGaussian
            pCov = prior.covariance_matrix()
            prior = ComplexGaussianErrorModel(
                configs={
                    "mean": prior.mean,
                    "variance": pCov,
                    "relation": 0.0,
                }
            )
        else:
            raise TypeError(
                f"Only None, GaussianErrorModel, or ComplexGaussianErrorModel instances are allowed\n"
                f"Received: {prior=} of {type(prior)=}"
            )

        # Associate with self and do super validation/testing
        prior = super().register_prior(prior=prior)

        ## Extra testing
        ...

        # Initiate the (Gaussian) posterior
        if initiate_posterior:
            if prior is None:
                self._POSTERIOR = None

            else:
                # Covariances must be updated after solving the inverse problem
                self._POSTERIOR = ComplexGaussianErrorModel(
                    configs={
                        "size": prior.size,
                        "mean": prior.mean,
                        "variance": prior.covariance_matrix(),
                        "relation": prior.relation_matrix(),
                        "design": prior.design,
                        "sparse": prior.sparse,
                        "map_to_real": prior.map_to_real,
                        "random_seed": prior.random_seed,
                    }
                )

        return prior

    def forecast(
        self,
        tspan,
        posterior=None,
    ):
        """
        Apply the forecast/prediction step of the filter (simulate state over a timespan).
        This is carried out by propagating the posteiror/analysis distribution
        including mean and covariance matrix
        over the passed prediction timespan (t0, tf).

        :param tuple tspan: (t0, tf) defining the forecast timespan.
            The analysis/posterior is supposed to be defined at t0, and the
            forecast (prediction) is evaluated at tf.
        :param GaussianErrorModel posterior: the posterior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the internally saved posterior (if available) is used.

        :returns: the Gaussian forecast (GaussianErrorModel)
        """
        # Verify that all inversion elements are properly registered
        if self.model is None:
            raise TypeError(
                f"Can't carry out forecast without a registerd simulation model"
                f"Make sure you call `register_model(model) first"
            )

        if posterior is None:
            posterior = self._POSTERIOR
        if posterior is None:
            raise ValueError(
                "The posterior has not been evaluated; "
                f"either pass an explicit posterior, or call the analysis step first!"
            )
        elif not isComplexGaussian(posterior):
            raise TypeError(
                f"Expected poterior to be instance of `GaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        t0, tf = tspan
        assert tf > t0 >= 0, "(t0, tf) must be tuple with tf > t0 >= 0"

        state_size = self.model.state_size

        xa = posterior.mean
        Pa = posterior.covariance_matrix()

        # Generate forecast state (forward simulation)
        _, trajectory = self.model.integrate_state(
            xa,
            tspan=tspan,
        )
        xf = trajectory[-1]

        # Generate a model matrix/array representation
        Pa_sqrt = utility.factorize_spsd_matrix(
            Pa,
            shape=Pa.shape,
            lower=True,
        )

        # Apply model from both sides to the analysis error covariance matrix
        Pf_sqrt = np.empty((state_size, state_size), dtype=complex)
        for j in range(state_size):
            Pf_sqrt[:, j] = self.model.integrate_state(
                Pa_sqrt[:, j],
                tspan=tspan,
            )[
                1
            ][-1]
        Pf = Pf_sqrt @ Pf_sqrt.conjugate().T

        forecast = GaussianErrorModel(
            configs={
                "mean": xf,
                "variance": Pf,
                "sparse": posterior.sparse,
            }
        )

        return forecast

    def analysis(
        self,
        prior=None,
        update_posterior=False,
    ):
        """
        Analysis step of the (Vanilla Kalman) filter. This function uses the prior and
            the registered observtion to evaluate/update the posterior (Gaussian in this case).

        :param GaussianErrorModel prior: the prior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the registerd prior (if available) is used.

        :returns: the Gaussian posterior model

        .. note:
           The returned posterior is a copy from an internal posterior instance used
            for further forecasts.
        """
        # Get the registered prior if not passed explicitly.
        if prior is None:
            prior = self.prior

        # Check all registered elements
        if any(
            [
                prior is None,
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
                f"\n***Also***: \n"
                f"{prior=}\n"
                f"{prior=}"
            )

        # Check the prior
        if not isComplexGaussian(prior):
            raise TypeError(
                f"Expected poterior to be instance of `ComplexGaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        # state and observation vector dimensions
        state_size = self.model.state_size
        observation_size = self.observation_operator.observation_size

        # get the forecast state, and the forecast error covariance matrix.
        xf = prior.mean
        Pf = utility.asarray(prior.covariance_matrix(), dtype=complex)

        # get the measurements vector
        observation = self.observations[self.observation_operator.extended_design]

        # Get a reference to registered operators
        obs_oper = self.observation_operator
        obs_err_model = self.observation_error_model

        # Innovation or measurement residual: y - H(x)
        innov = observation - obs_oper(xf)

        # Construct an array representation of the observation operator, its transpose,
        #   and observation error covariances
        H = utility.asarray(obs_oper, shape=obs_oper.shape, dtype=complex)
        R = utility.asarray(obs_err_model.covariance_matrix(), dtype=complex)
        #

        # Kalman Gain
        S = H @ ( Pf @ H.conjugate().T ) + R
        K = Pf @ ( H.conjugate().T @ np.linalg.inv(S) )

        # Update
        xa = xf + K @ innov
        Pa = (np.eye(state_size) - K @ H) @ Pf

        if self.debug:
            print("Analysis Step: prior.mean: ", xf, )
            print("Analysis Step: prior.covariance_matrix: ", Pf, )
            print("Analysis Step: observation: ", observation, )
            print("Analysis Step: H: ", H, )
            print("Analysis Step: R: ", R, )
            print("Analysis Step: S: ", S, )
            print("Analysis Step: K: ", K, )
            print("Analysis Step: xa: ", xa, )
            print("Analysis Step: S: ", Pa, )

        # Update the posterior if needed
        if update_posterior:
            if self._POSTERIOR is None:
                self._POSTERIOR = ComplexGaussianErrorModel(
                    configs={
                        "mean": xa,
                        "variance": Pa,
                        "sparse": prior.sparse,
                    }
                )
            else:
                self._POSTERIOR.update_mean(xa)
                self._POSTERIOR.update_covariance_matrix(Pa)
            posterior = self._POSTERIOR

        else:

            # Create another instance to return
            posterior = ComplexGaussianPosterior(
                    configs={
                        "mean": xa,
                        "variance": Pa,
                        "sparse": prior.sparse,
                    }
                )
        # Return the calculated posterior
        return posterior


@dataclass(kw_only=True, slots=True)
class AugmentedComplexKalmanFilterConfigs(ConventionalComplexKalmanFilterConfigs):
    """
    Configurations class for the :py:class:`AugmentedComplexKalmanFilter` class.
    The configurations here are identical to the parent's :py:class:`ConventionalComplexKalmanFilterConfigs`
    except for the name.
    """
    name: str | None = "ACKF: Augmented Complex Valued Kalman Filter"


@set_configurations(AugmentedComplexKalmanFilterConfigs)
class AugmentedComplexKalmanFilter(ConventionalComplexKalmanFilter):
    """
    A class implementing the augmented version of Kalman Filtering (KF) scheme
    for complex-valued state inference/inversion, assuming the model equation is on the form
    :math:` \\mathbf{x}_{i+1}=\\mathcal(M) \\mathbf{x}_{i}`,
    and :math:`\\,mathbf{x}_{i}` is the model state at ith time instance/step.

    .. note::
        This implementation relies on the augmented statistics, and incorporates both
        covariances and pseudo-covariances.

    .. note::
        - The scheme assumes the dynamical model and the observational operator are both specified.
        - A necessary requirement here is that both the dynamical model and the
            observation operator are both linear, otherwise results are not expected to be good.
        - The prior must by complex-valued error-model (e.g., ComplexGaussianErrorModel).
            Thus, if a standard real-valued Gaussian prior (e.g., :py:class:`GaussianErrorModel`)
            is passed, its moments will be used to create an instance of :py:class:`ComplexGaussianErrorModel`
            with same mean, and covariance.
        - Pseudo-covariances are incoporated here, and are thus part of the posterior.
        - It is up to the user to make sure these elements are propoperly specified (at least initially!)
    """

    def __init__(self, configs: AugmentedComplexKalmanFilterConfigs | dict | None = None):
        configs = self.configurations_class.data_to_dataclass(configs)
        super().__init__(configs)

        # Now, initiate the posterior
        self.register_prior(self.configurations.prior, initiate_posterior = True)


    def forecast(
        self,
        tspan,
        posterior=None,
    ):
        """
        Apply the forecast/prediction step of the filter (simulate state over a timespan).
        This is carried out by propagating the posteiror/analysis distribution
        including mean and covariance matrix
        over the passed prediction timespan (t0, tf).

        :param tuple tspan: (t0, tf) defining the forecast timespan.
            The analysis/posterior is supposed to be defined at t0, and the
            forecast (prediction) is evaluated at tf.
        :param GaussianErrorModel posterior: the posterior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the internally saved posterior (if available) is used.

        :returns: the Gaussian forecast (GaussianErrorModel)
        """
        # Verify that all inversion elements are properly registered
        if self.model is None:
            raise TypeError(
                f"Can't carry out forecast without a registerd simulation model"
                f"Make sure you call `register_model(model) first"
            )

        if posterior is None:
            posterior = self._POSTERIOR
        if posterior is None:
            raise ValueError(
                "The posterior has not been evaluated; "
                f"either pass an explicit posterior, or call the analysis step first!"
            )
        elif not isComplexGaussian(posterior):
            raise TypeError(
                f"Expected poterior to be instance of `GaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        t0, tf = tspan
        assert tf > t0 >= 0, "(t0, tf) must be tuple with tf > t0 >= 0"

        state_size = self.model.state_size

        xa = posterior.mean
        Pa = posterior.covariance_matrix()

        # Generate forecast state (forward simulation)
        _, trajectory = self.model.integrate_state(
            xa,
            tspan=tspan,
        )
        xf = trajectory[-1]

        # Generate a model matrix/array representation
        Pa_sqrt = utility.factorize_spsd_matrix(
            Pa,
            shape=Pa.shape,
            lower=True,
        )

        # Apply model from both sides to the analysis error covariance matrix
        Pf_sqrt = np.empty((state_size, state_size), dtype=complex)
        for j in range(state_size):
            Pf_sqrt[:, j] = self.model.integrate_state(
                Pa_sqrt[:, j],
                tspan=tspan,
            )[
                1
            ][-1]
        Pf = Pf_sqrt @ Pf_sqrt.conjugate().T

        forecast = GaussianErrorModel(
            configs={
                "mean": xf,
                "variance": Pf,
                "sparse": posterior.sparse,
            }
        )

        return forecast

    def analysis(
        self,
        prior=None,
        update_posterior=False,
    ):
        """
        Analysis step of the (Vanilla Kalman) filter. This function uses the prior and
            the registered observtion to evaluate/update the posterior (Gaussian in this case).

        :param GaussianErrorModel prior: the prior (yielding mean & covariance)
            used as starting point for prediction.
            If `None`, the registerd prior (if available) is used.

        :returns: the Gaussian posterior model

        .. note:
           The returned posterior is a copy from an internal posterior instance used
            for further forecasts.
        """
        # Get the registered prior if not passed explicitly.
        if prior is None:
            prior = self.prior

        # Check all registered elements
        if any(
            [
                prior is None,
                self.model is None,
                self.observation_operator is None,
                self.observation_error_model is None,
                self.observations is None,
            ]
        ):
            raise TypeError(
                f"Some of the necessary components of the inverse problem "
                f"is/are missing; make sure you use the right registeration "
                f"for all components"
                f"{self.show_registered_elements(display=False, )}"
                f"\n***Also***: \n"
                f"{prior=}\n"
                f"{prior=}"
            )

        # Check the prior
        if not isComplexGaussian(prior):
            raise TypeError(
                f"Expected poterior to be instance of `ComplexGaussianErrorModel`\n"
                f"Received {posterior=} of {type(posterior)=}"
            )

        # state and observation vector dimensions
        state_size = self.model.state_size
        observation_size = self.observation_operator.observation_size

        # get the forecast state, and the forecast error covariance matrix.
        xf = prior.mean
        Pf = utility.asarray(prior.covariance_matrix(), dtype=complex)

        # get the measurements vector
        observation = self.observations[self.observation_operator.extended_design]

        # Get a reference to registered operators
        obs_oper = self.observation_operator
        obs_err_model = self.observation_error_model

        # Innovation or measurement residual: y - H(x)
        innov = observation - obs_oper(xf)

        # Construct an array representation of the observation operator, its transpose,
        #   and observation error covariances
        H = utility.asarray(obs_oper, shape=obs_oper.shape, dtype=complex)
        R = utility.asarray(obs_err_model.covariance_matrix(), dtype=complex)
        #

        # Kalman Gain
        S = H @ ( Pf @ H.conjugate().T ) + R
        K = Pf @ ( H.conjugate().T @ np.linalg.inv(S) )

        # Update
        xa = xf + K @ innov
        Pa = (np.eye(state_size) - K @ H) @ Pf

        if self.debug:
            print("Analysis Step: prior.mean: ", xf, )
            print("Analysis Step: prior.covariance_matrix: ", Pf, )
            print("Analysis Step: observation: ", observation, )
            print("Analysis Step: H: ", H, )
            print("Analysis Step: R: ", R, )
            print("Analysis Step: S: ", S, )
            print("Analysis Step: K: ", K, )
            print("Analysis Step: xa: ", xa, )
            print("Analysis Step: S: ", Pa, )

        # Update the posterior if needed
        if update_posterior:
            if self._POSTERIOR is None:
                self._POSTERIOR = ComplexGaussianErrorModel(
                    configs={
                        "mean": xa,
                        "variance": Pa,
                        "sparse": prior.sparse,
                    }
                )
            else:
                self._POSTERIOR.update_mean(xa)
                self._POSTERIOR.update_covariance_matrix(Pa)
            posterior = self._POSTERIOR

        else:

            # Create another instance to return
            posterior = ComplexGaussianPosterior(
                    configs={
                        "mean": xa,
                        "variance": Pa,
                        "sparse": prior.sparse,
                    }
                )
        # Return the calculated posterior
        return posterior



