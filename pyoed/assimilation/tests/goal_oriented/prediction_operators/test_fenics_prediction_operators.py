# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import numpy as np

# simulation model(s)
from pyoed.models.simulation_models import (
    fenics_models,
)

# observation operators(s)
from pyoed.models.observation_operators import (
    fenics_observation_operators,
)

# error models
from pyoed.models.error_models import (
    Gaussian,
    Laplacian,
)

# data assimilation (inversion)
from pyoed.assimilation.smoothing.fourDVar import VanillaFourDVar as FourDVar
from pyoed.assimilation.goal_oriented.prediction_operators import (
    fenics_prediction_operators,
)


from pyoed.tests import (
    SETTINGS,
    assert_allclose,
    assert_allequal,
    create_random_number_generator,
)

class SETTINGS:
    """Local settings"""
    RANDOM_SEED = SETTINGS.RANDOM_SEED
    SIZE = SETTINGS.OBSERVATION_SIZE
    PREDICTION_SIZE = 3

import pytest
pytestmark = pytest.mark.assimilation


def create_goal_operator(inverse_problem):
    """

    """
    pred_coord = inverse_problem.model.get_model_grid()[: SETTINGS.PREDICTION_SIZE]
    goper = fenics_prediction_operators.DolfinLinearPredictionOperator(
        configs={
            'inverse_problem': inverse_problem,
            'prediction_time': 1,
            'prediction_coordinates': pred_coord,
        }
    )
    return goper


def create_fenics_linear_inverse_problem():
    """  """
    # Create a random number generator
    rng = create_random_number_generator()

    # 1- Create the simulation model (Advection-Diffusion 2D)
    # Instantiate the 2D Advection Diffusion object
    dt = 0.2
    model = fenics_models.create_AdvectionDiffusion2D_model(
        dt=dt,
    )

    # 2- Extract true initial condition, create prior model, and create forecast state
    u0_true = model.create_initial_condition()
    model_grid = model.get_model_grid()

    # Create prior model and update its mean
    prior_mean = model.parameter_vector(init_val=0.0)
    Vh = model.parameter_dof
    configs = dict(
        Vh=Vh,
        mean=prior_mean,
        gamma=1,
        delta=16,
        random_seed=SETTINGS.RANDOM_SEED,
    )
    prior = Laplacian.DolfinBiLaplacianErrorModel(configs)

    # prior.mean = u0_true + prior.generate_noise()
    init_guess = prior_mean.copy()

    # 3- Observation operator, and observation error model
    observation_operator = (
        fenics_observation_operators.create_pointwise_observation_operator(
            model=model,
            Vh=model.state_dof,
            num_obs_points=10,
            exclude_boxes=[((0.25, 0.150), (0.50, 0.400)), ((0.60, 0.625), (0.75, 0.850))],
        )
    )

    # Time
    checkpoints=np.arange(0, 0.61, dt)
    pred_time = 0.8

    _, traject = model.integrate_state(
        u0_true, tspan=(checkpoints[0], checkpoints[-1]), checkpoints=checkpoints[1:]
    )
    d = 0.0
    for x in traject:
        y = observation_operator(x)
        d += y
    d /= len(traject)
    obs_err_std = 1e-12 + 0.05* np.linalg.norm(d, np.inf)
    observation_error_variance = obs_err_std**2
    obs_err_model = Gaussian.GaussianErrorModel(
        configs=dict(
            size=observation_operator.shape[0],
            mean=0.0,
            variance=observation_error_variance,
            random_seed=SETTINGS.RANDOM_SEED,
        )
    )
    window = (checkpoints[0], checkpoints[-1])
    problem = FourDVar(
        configs=dict(
            window=window,
            model=model,
            prior=prior,
            observation_operator=observation_operator,
            observation_error_model=obs_err_model,
        )
    )

    # 4- Create and register observations
    print("Creating and registering synthetic observations...")
    tspan = (checkpoints[0], checkpoints[-1])
    obs_times, true_obs = model.integrate_state(
        u0_true, tspan=tspan, checkpoints=checkpoints[1:]
    )
    act_obs = []
    for t, y in zip(obs_times, true_obs):
        y = observation_operator.apply(y, return_np=True)
        yobs = obs_err_model.add_noise(y)
        act_obs.append(yobs)
        problem.register_observation(t=t, observation=yobs)

    return problem

def test_goal_oriented_LinearPredictionOperator():
    """
    Test :py:class:`pyoed.assimilation.goal_oriented.prediction_operators.fenics_prediction_operators.DolfinLinearPredictionOperator`
    """
    ip = create_fenics_linear_inverse_problem()
    gooper = create_goal_operator(ip)

    # Test dimension/size of the prediction operator
    assert gooper.generate_vector(return_np=True, ).size == SETTINGS.PREDICTION_SIZE, "Prediction dimension incorrect"
    assert gooper.size == SETTINGS.PREDICTION_SIZE, "Prediction dimension incorrect"

    # A stae & a goal vector for testing (size, etc.)
    test_state = ip.model.state_vector()
    test_p = gooper.generate_vector()

    # Test apply (and __cal__)
    p = gooper.apply(test_state)
    assert len(p) == len(test_p)
    p = gooper(test_state)
    assert len(p) == len(test_p)

    # Test apply_adjoint
    state = gooper.apply_adjoint(test_p)
    assert len(state) == len(test_state)


