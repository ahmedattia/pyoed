PyOED
=====

PyOED is a comprehensive Python toolkit for **Model-constrained Optimal
Experimental Design (OED)**.  The package targets scientists and researchers
interested in understanding the details of OED formulations and approaches. It
is also meant to enable researchers experiment with standard and innovative OED 
technologies within external test problems (e.g., simulations). 

OED, inverse problems (e.g., Bayesian inversion), and data assimilation are
closely related research fields, and their formulations overlap significantly.
Thus, PyOED will be expanded with a plethora of Bayesian inversion, DA, and OED
methods as well as new scientific simulation models, observation error models,
and observation operators. These pieces are added such that they can be permuted
together to enable testing OED methods in various settings.


Documentation 
--------------

For detailed documentation, tutorials, examples, and detailed installation instructions see the [PyOED documentation webpage](https://web.cels.anl.gov/~aattia/pyoed).


Installation 
------------
First, create (and navigate to) a folder where the PyOED source code will be downloaded.
Then, install conda, and follow the installation instructions as follows.

1. **Install conda (miniconda is recommended):** [See conda's official website](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)

2. **Install PyOED:**

    **Linux and macOS (x86 only)**
    ```
    git clone https://gitlab.com/ahmedattia/pyoed
    conda create -n pyoed --file requirements.txt --file optional-requirements.txt -c conda-forge
    conda activate pyoed
    pip install --upgrade pip
    pip install -e .
    ```

    **macOS with Apply Silicon**
    ```
    git clone https://gitlab.com/ahmedattia/pyoed
    conda create -n pyoed 
    conda activate pyoed
    conda config --env --set subdir osx-64
    conda install --file requirements.txt --file optional-requirements.txt -c conda-forge
    pip install --upgrade pip
    pip install -e .
    ```

