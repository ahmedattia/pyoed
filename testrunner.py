
# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved


"""
The simplest approach to run all tests under PyOED.
This is an in-progress module that will evolve to enable running individual tests
"""
try:
    import pytest
except(ImportError):
    print("PyOED tests require installing pytest package!")
    print("Istall pytest before running this script:")
    print("    Conda: $ conda install pytest")
    print("    Pip: $ pip install pytest")
    raise


import sys, os
import subprocess
import argparse
import pyoed
pyoed.SETTINGS.OUTPUT_DIR = os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    "__PYTES_RESULTS__"
)
pyoed.SETTINGS.RANDOM_SEED = 1011

# Optional packages
_NO_TESTS_FLGS = []

# 1- Fenics models & observations
try:
    import dolfin
except:
    dolfin = None
    _NO_TESTS_FLGS.append("fenics")

# 2- CDI/Ptychography simulations
try:
    import skimage
except:
    dolfin = None
    _NO_TESTS_FLGS.append("cdi")


def main(verbose=False, test_optional=False, parallel="", time=False, ):
    cmd = "pytest"
    if not test_optional and len(_NO_TESTS_FLGS)>0:
        cmd += ' -m "{0}"'.format(" and ".join([f"not {f}" for f in _NO_TESTS_FLGS]))

    if parallel != "":
        cmd += f" -n {parallel}"

    if time:
        cmd += " --durations=0"

    if verbose:
        cmd += " -vv"
        print(cmd)

    status = subprocess.Popen(cmd, shell=True).wait()

    if verbose:
        print(f"Ran pytest in the shell with status '{status}'")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run PyOED tests')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose mode')
    parser.add_argument('-t', '--time', action='store_true', help='Time tests')
    parser.add_argument(
        '-n',
        '--parallel',
        type=str,
        default="",
        help="Parallelize tests; e.g., <-n 5> runs on 5 cores; <-n auto> use avaliable cores."
    )
    args = parser.parse_args()
    main(verbose=args.verbose, parallel=args.parallel, time=args.time, )


