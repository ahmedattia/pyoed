Optimization Test Functions
===========================
This subpackage provides access to several engineered **objective functions** for testing optimization algorithms 


Entries on this page:

.. contents::
   :local:
   :depth: 2


Base Class for Test Functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: pyoed.optimization.core.test_functions
    :members:
    :undoc-members:
    :show-inheritance:


Test Functions with Binary Input
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autosummary::

   pyoed.optimization.test_functions.binary_test_functions

.. automodule:: pyoed.optimization.test_functions.binary_test_functions
    :members:
    :undoc-members:
    :show-inheritance:

