Advection-Diffusion Models
==========================

.. autosummary::
   pyoed.models.simulation_models.advection_diffusion.AdvectionDiffusion1D
   pyoed.models.simulation_models.advection_diffusion.AdvectionDiffusion2D
   pyoed.models.simulation_models.advection_diffusion.AdvectionDiffusion3D
   
.. autosummary::
   pyoed.models.simulation_models.advection_diffusion.create_AD_1D_model
   pyoed.models.simulation_models.advection_diffusion.create_AD_2D_model
   pyoed.models.simulation_models.advection_diffusion.create_AD_3D_model


.. automodule:: pyoed.models.simulation_models.advection_diffusion
    :members:
    :undoc-members:
    :show-inheritance:


