Data Assimilation (Inversion)
=============================
This subpackage provides implementations to solve data assimilation (DA) problems (inverse problems) for both 
time-independent and time-dependent simulation models and inverse problems.

.. toctree::
   :maxdepth: 1 
    
    DA Core (Base Classes) <pyoed.assimilation.core>
    
.. toctree::
   :maxdepth: 2

    Filtering DA Methods <pyoed.assimilation.filtering>
    Smooting DA Methods <pyoed.assimilation.smoothing>

.. toctree::
   :maxdepth: 1

    Goal-Oriented DA <pyoed.assimilation.goal_oriented>
    
    DA Utilities  <pyoed.assimilation.assimilation_utils>
    Extras (ROM, etc.) <pyoed.assimilation.extras>

