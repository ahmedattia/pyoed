Identity Observation Operators
==============================

.. autosummary::
   ~pyoed.models.observation_operators.identity.Identity
   ~pyoed.models.observation_operators.identity.create_Identity_observation_operator


.. automodule:: pyoed.models.observation_operators.identity
   :members:
   :undoc-members:
   :show-inheritance:

