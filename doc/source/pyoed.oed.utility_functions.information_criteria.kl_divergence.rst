KL-Divergence Optimality Criteria
=================================

.. contents::
   :local:
   :depth: 2


.. automodule:: pyoed.oed.utility_functions.information_criteria.kl_divergence
   :members:
   :undoc-members:
   :show-inheritance:


