Information-theoritic OED Criteria
==================================
Information-theoretic OED criteria are based on summaries of the information content 
or expected information gain.
The most commonly used are the expected information gain (:ref:`EIG`), and the 
:ref:`KL-divergence` between prior and the posterior in Bayesian OED. 


.. contents::
   :local:
   :depth: 2


.. _EIG:

EIG Criterion
^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   EIG Criteria <pyoed.oed.utility_functions.information_criteria.eig>

.. _KL-divergence:

KL-divergence Criteria
^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   EK-Divergence Criteria  <pyoed.oed.utility_functions.information_criteria.kl_divergence>

