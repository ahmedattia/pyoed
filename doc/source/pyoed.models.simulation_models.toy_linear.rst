Toy (Linear) Models
===================

.. autosummary:: 
   ~pyoed.models.simulation_models.toy_linear.ToyLinearTimeIndependent
   ~pyoed.models.simulation_models.toy_linear.ToyLinearTimeDependent

.. autosummary:: 
   ~pyoed.models.simulation_models.toy_linear.create_ToyLinearTimeIndependent_model
   ~pyoed.models.simulation_models.toy_linear.create_ToyLinearTimeDependent_model


.. automodule:: pyoed.models.simulation_models.toy_linear
    :members:
    :undoc-members:
    :show-inheritance:


