Alphabetic Criteria
===================
OED Alphabetic criteria are based on summaries of the Fisher information matrix.
In the context of Bayesian OED, these criteria summarize the posterior uncertainty.
The most commonly used are :ref:`a-optimality` and :ref:`d-optimality` which are defined
as the trace, and the determinant, of the posterior covariance, respectively. Each of
the commonly known alphabetic criteria is (or will be) implemented with different
variations for maximum flexibility.


.. contents::
   :local:
   :depth: 2


.. _a-optimality:

A Optimality Criteria
^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   A-optimality <pyoed.oed.utility_functions.alphabetic_criteria.a_opt>


.. _d-optimality:

D Optimality Criteria
^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   D-optimality <pyoed.oed.utility_functions.alphabetic_criteria.d_opt>


