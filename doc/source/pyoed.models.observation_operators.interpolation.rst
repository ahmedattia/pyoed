Interpolation Observation Operators
===================================

.. autosummary:: 
   ~pyoed.models.observation_operators.interpolation.ArrayOperator
   ~pyoed.models.observation_operators.interpolation.SelectionOperator
   ~pyoed.models.observation_operators.interpolation.CartesianInterpolator


.. automodule:: pyoed.models.observation_operators.interpolation
   :members:
   :undoc-members:
   :show-inheritance:

