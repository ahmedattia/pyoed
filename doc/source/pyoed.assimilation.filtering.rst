DA Filtering (Inversion) Methods
================================
This subpackage provides implementations to solve both 
**time-independent** as well as **time-dependent** DA problems where one observation is assimilated/matched 
against model output.
For time-dependent problems, the filtering is carried out **sequentially**. 


DA Filtering Algorithms
^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

    Three dimensional Variational (3DVar) DA <pyoed.assimilation.filtering.threeDVar>
    Kalman Filtering <pyoed.assimilation.filtering.kalman>



DA Filtering Base Classes 
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.assimilation.core.filtering
   :members:
   :undoc-members:
   :show-inheritance:


