Contact
========

.. _package repo: https://gitlab.com/ahmedattia/pyoed 
.. _email us: aattia@mcs.anl.gov
.. _Ahmed Attia: https://web.cels.anl.gov/~aattia/ 
.. _Abhijit Chowdhary: https://abhijit-c.github.io/ 

- For any problems with the package please create an issue on the `package repo`_
- If you have other questions or requests please `email us`_


Collaborators
-------------

This is a list of people who have contributed to the development of the code:

    +----------------------+---------------------------------+--------------------+----------------------------+
    | Who                  | Affiliation                     | When               | Work                       |
    +======================+=================================+====================+============================+
    | `Ahmed Attia`_       | Argonne National Lab            | Lifetime           | Lead                       |
    +----------------------+---------------------------------+--------------------+----------------------------+
    | `Abhijit Chowdhary`_ | North Carolina State University | May 2023 - Present | Nonlinear DA/OED           |
    +----------------------+---------------------------------+--------------------+----------------------------+
    | Shady Ahmed          | Oklahoma State University       | May - August 2021  | Initial Models             |
    +----------------------+---------------------------------+--------------------+----------------------------+


.. seealso::
   Read the `PyOED Paper <https://arxiv.org/abs/2301.08336>`_


