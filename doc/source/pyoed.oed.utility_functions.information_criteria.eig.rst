Expected Information Gain (EIG) Optimality Criteria
===================================================

.. contents::
   :local:
   :depth: 2


.. automodule:: pyoed.oed.utility_functions.information_criteria.eig
   :members:
   :undoc-members:
   :show-inheritance:


