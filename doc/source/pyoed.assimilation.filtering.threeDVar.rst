Three Dimensional Variation (3DVar) DA
======================================

.. contents::
   :local:
   :depth: 2
    

.. automodule:: pyoed.assimilation.filtering.threeDVar
    :members:
    :undoc-members:
    :show-inheritance:

