2D Subsurface (Poisson) Model (FEniCS)
======================================

.. autosummary::
   ~pyoed.models.simulation_models.fenics_models.subsurf.Subsurf2D
   ~pyoed.models.simulation_models.fenics_models.subsurf.create_Subsurf2D_model
   

.. automodule:: pyoed.models.simulation_models.fenics_models.subsurf
    :members:
    :undoc-members:
    :show-inheritance:


