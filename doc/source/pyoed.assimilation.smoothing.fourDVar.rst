Four Dimensional Variation (4DVar) DA
======================================


.. automodule:: pyoed.assimilation.smoothing.fourDVar
    :members:
    :undoc-members:
    :show-inheritance:

