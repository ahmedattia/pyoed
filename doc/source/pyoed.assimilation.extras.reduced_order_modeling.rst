Reduced Order Modeling for Inverse Problems
===========================================

.. autosummary::
   pyoed.assimilation.extras.reduced_order_modeling.ReducedHessian
   pyoed.assimilation.extras.reduced_order_modeling.SVDReducedHessian
   pyoed.assimilation.extras.reduced_order_modeling.NystromRandomizedHessian


.. automodule:: pyoed.assimilation.extras.reduced_order_modeling
    :members:
    :undoc-members:
    :show-inheritance:

