Utility Modules
===============

**PyOED** is shipped with plenty of utility functions to perform general-purpose tasks.

.. autosummary:: pyoed.utility.mixins
.. autosummary:: pyoed.utility.math
.. autosummary:: pyoed.utility.stats
.. autosummary:: pyoed.utility.plot
.. autosummary:: pyoed.utility.misc

.. toctree::
   :maxdepth: 2

   pyoed.utility.mixins
   pyoed.utility.math
   pyoed.utility.stats
   pyoed.utility.plot
   pyoed.utility.misc
