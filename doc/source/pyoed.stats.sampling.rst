Sampling Methods
================

Entries on this page:

.. contents::
   :local:
   :depth: 2

.. note::
   All sampling classes (samplers) inherit the 
   :py:mod:`Sampler base class (Sampler) <pyoed.stats.core.sampling.Sampler>`
   and each sampler is associated with a configurations class derived from
   the :py:mod:`Sampler configurations base class (SamplerConfigs) <pyoed.stats.core.sampling.SamplerConfigs>`


Rejection Sampling
^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.sampling.rejection
   :members:
   :undoc-members:
   :show-inheritance:


Markov Chain Monte Carlo Sampling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.sampling.mcmc
   :members:
   :undoc-members:
   :show-inheritance:


Proposals
^^^^^^^^^

.. automodule:: pyoed.stats.sampling.proposals
   :members:
   :undoc-members:
   :show-inheritance:


