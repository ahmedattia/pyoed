General Purpose OED Routines
============================
This module provides methods to apply OED by employing Laplace approximation to 
the posterior successively.


OED via Successive Laplace Approximations
-----------------------------------------
.. automodule:: pyoed.oed.common.successive_laplace_oed
   :members:
   :undoc-members:
   :show-inheritance:

