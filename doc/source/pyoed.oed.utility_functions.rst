OED Utility Functions (Optimality Criteria)
===========================================

An OED utility function is a function that quantifies the quality of an experimental
design.
In control, utility functions are associated with maximization problem.
In Bayesian OED, one the other hand the term optimality criterion is used instead of a
utility function and is associated with a minimization problem.
Thus, we use the two terms `"utility function"` and `"optimality criterion"`
interchangeably and assume the optimization problem is determined accordingly. 

This module provides a continuously expanding collection of utility functions used in
the OED sub-package. Should you decide that you want to pass your own custom utility
function to the OED solver, you can do so by inheriting the abstract base class
:class:`UtilityFunction` class (in the :ref:`utility functions part of the OED core
<criteria-core>`). Further functionality may ought to be added as required.

We categorize the utility functions in this module into two categories, namely, 
:ref:`Alphabetic Criteria <alphabetic-criteria>` and :ref:`Information Criteria 
<information-criteria>`.

.. note::
   The user can define a utility function by inheriting the base optimality criterion 
   (base utility function) :class:`UtilityFunction` from the OED core 
   subpackage `pyoed.oed.core`.

Contents of this page:

.. contents::
   :local:
   :depth: 2
    

.. _alphabetic-criteria:

OED Utility Functions based on the Alphabetic Criteria
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   Alphabetic Criteria <pyoed.oed.utility_functions.alphabetic_criteria>


.. _information-criteria:

OED Utility Functions based on Information Content/Gain 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   Information-theoretic Criteria <pyoed.oed.utility_functions.information_criteria>


Lookup function (for easy access)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    A look up module `pyoed.oed.utility_functions.lookup` is available 
    for users to employ for finding a utility function that matches a commonly 
    used  pattern.
    The most common use (which is expected is to call the 
    :func:`lookup.find_criteria(pattern)`) 
    where `'pattern'` is a string that describes the criterion to be created.
    For example, to create a Bayesian A-Opt OED utility function:

    .. code-block:: python

        pyoed.oed.utility_functions.lookup('Bayesian A-opt')

    To create a Bayesian A-Opt OED utility function with design relaxation, 
    one can use:

    .. code-block:: python

        pyoed.oed.utility_functions.lookup('Relaxed Bayesian A-opt')


.. _criteria-core:

Utility Functions Base Classes (Core) 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.core.utility_functions
   :members:
   :undoc-members:
   :show-inheritance:

