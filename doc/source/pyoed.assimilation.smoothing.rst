Data Assimilation (Inversion)
=============================
This subpackage provides implementations to solve **time-dependent** data assimilation problems **only**.
In this case, the simulation model, the observation operator, and the observational data are generally 
time dependent. Of course, one can set the observation operator to be the same at all observation time 
point which is common when the observational configuration does not change over time.
The DA algorithm assimilates multiple observations at once to correct the model trajectory over a given assimilation timespan (assimilation window).


DA Smoothing Algorithms
^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

    Four dimensional Variational (4DVar) DA <pyoed.assimilation.smoothing.fourDVar>
    Kalman Smoothing <pyoed.assimilation.smoothing.kalman>


DA Smoothing Base Classes 
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.assimilation.core.smoothing
   :members:
   :undoc-members:
   :show-inheritance:

