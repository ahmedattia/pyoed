User Guide
==========

PyOED consists of several modular subpackages which can be employed with great flexibility.
An important aspect of PyOED's current version is the great flexibility inherent in 
the developed class across the whole system by associating a 
:doc:`configurations data class <pyoed.configs>` to each object upon instantiation.
Thus, each PyOED class/object is associated with a configurations object (dataclass instance) 
that can be accessed, validated, and updated in a way that guarantees robustness and flexibility.
PyOED provides 
:ref:`a plethora of implementations <pyoed_packages>` of simulation models, 
observation operators, error models, novel optimization algorithms and much more.
Additionally, the :doc:`PyOED team <contact>` welcomes 
:ref:`contributions <extend-pyoed>`, 
and is happy to provide :ref:`new features <collaborate-pyoed>` upon reasonable requests. 

.. rst-class:: center

    **PyOED Structure at a Glance**

    .. image:: ./Images/PyOED_At_A_Glance.png 
      :width: 80%
      :align: center
      :alt: PyOED high-level diagram!


.. _pyoed_packages:

PyOED Subpackages & Modules 
----------------------------

.. toctree::
   :maxdepth: 2

    Configs <pyoed.configs>
    Models <pyoed.models>
    Data Assimilation (Inverse Problems) <pyoed.assimilation>
    Optimal Experimental Design <pyoed.oed>
    Optimization <pyoed.optimization>
    Statistics <pyoed.stats>
    Machine Learning <pyoed.ml>
    Utility <pyoed.utility>
 

.. _extend-pyoed:

Extending (Contributing to) PyOED
---------------------------------

.. toctree::
   :maxdepth: 2

    Extend (Contribute to) PyOED <pyoed.contribute>


.. _collaborate-pyoed:

Collaboration & Feature Requests
--------------------------------

.. toctree::
   :maxdepth: 2

    Collaborations and Feature Requests <pyoed.feature_request>

