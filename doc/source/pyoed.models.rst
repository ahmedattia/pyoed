PyOED Models
============

This package gives access to simulation, observation, and error models.


.. toctree::
   :maxdepth: 1

   pyoed.models.core


.. toctree::
   :maxdepth: 2

   pyoed.models.simulation_models
   pyoed.models.observation_operators
   pyoed.models.error_models

