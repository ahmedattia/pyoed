Lorenz Models
=============

.. autosummary:: 
   ~pyoed.models.simulation_models.lorenz.Lorenz63
   ~pyoed.models.simulation_models.lorenz.Lorenz96

..
   pyoed.models.simulation_models.lorenz.Lorenz96Coupled

.. autosummary:: 
   ~pyoed.models.simulation_models.lorenz.create_Lorenz63_model
   ~pyoed.models.simulation_models.lorenz.create_Lorenz96_model

..
   pyoed.models.simulation_models.lorenz.create_Lorenz96Coupled_model


.. automodule:: pyoed.models.simulation_models.lorenz
    :members:
    :undoc-members:
    :show-inheritance:


