Examples & Tutorials 
====================

PyOED provides a number of :ref:`examples (Python scripts) <examples>` 
and :ref:`tutorials (Jupyter Notebook) <tutorials>` of various complexity. 

.. contents::
   :local:
   :depth: 2
    

We will continuously add new examples for users to adopt and follow to learn PyOED features. 


.. _tutorials:

Tutorials (Jupyter Notebooks)
-----------------------------

.. toctree::
   :maxdepth: 1

   tutorials/toy_linear
   tutorials/OED_AD_FE

   tutorials/binary_optimization

-----------------------------


.. _examples:

Advanced Examples (Python Scripts)
----------------------------------

.. toctree::
    :maxdepth: 2

     Starters (First Steps) <pyoed.examples.starters>

     Models  <pyoed.examples.models>
     Statistics  <pyoed.examples.stats>
     
     Optimization <pyoed.examples.optimization>
     
     Data Assimilation <pyoed.examples.assimilation>
     Optimal Experimental Design (OED) <pyoed.examples.oed>
     
