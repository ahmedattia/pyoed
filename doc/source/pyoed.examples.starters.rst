Starters
========
This set of examples is meant to be most basic and enable researchers create building
blocks of the package in simplified settings.

.. contents::
   :local:
   :depth: 2


Creators
--------
.. automodule:: pyoed.examples.starters.creators
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        create_toy_linear_time_independent_inverse_problem
        create_toy_linear_time_dependent_inverse_problem
        create_toy_nonlinear_time_independent_inverse_problem
        create_toy_nonlinear_time_dependent_inverse_problem

        create_toy_linear_time_independent_relaxed_OED_problem
        create_toy_nonlinear_time_independent_relaxed_OED_problem
        create_toy_linear_time_dependent_relaxed_OED_problem
        create_toy_nonlinear_time_dependent_relaxed_OED_problem
        create_toy_linear_time_independent_binary_OED_problem
        create_toy_nonlinear_time_independent_binary_OED_problem
        create_toy_linear_time_dependent_binary_OED_problem
        create_toy_nonlinear_time_dependent_binary_OED_problem


Toy Linear Examples
-------------------
.. automodule:: pyoed.examples.starters.toy_linear
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        create_linear_inverse_problem
        create_relaxed_oed_problem
        create_binary_oed_problem


Toy Nonlinear Examples
----------------------
.. automodule:: pyoed.examples.starters.toy_nonlinear
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        create_nonlinear_inverse_problem
        create_oed_problem
        create_posterior_log_density
        create_posterior_log_density_gradient
        sample_posterior

Examples with a Toy Quark Model  
-------------------------------
.. automodule:: pyoed.examples.starters.toy_quark
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        create_toy_quark_inverse_problem
        sample_posterior
        heatmap
        annotate_heatmap
