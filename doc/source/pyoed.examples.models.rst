Models
======

.. contents::
   :local:
   :depth: 2
    

Simulation Models
-----------------
    Some examples to create and use simulation models.

Observation Operators
---------------------
    Some examples to create and use observation operators. 



Error Models
---------------------
    Some examples to create and use error models (prior, noise, etc.). 


