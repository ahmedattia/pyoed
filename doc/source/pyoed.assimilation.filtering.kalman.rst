Kalman Filters
==============

.. contents::
   :local:
   :depth: 2
    

.. automodule:: pyoed.assimilation.filtering.kalman
    :members:
    :undoc-members:
    :show-inheritance:


.. automodule:: pyoed.assimilation.filtering.complex_kalman
    :members:
    :undoc-members:
    :show-inheritance:

