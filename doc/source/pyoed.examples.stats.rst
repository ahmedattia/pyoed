Statistics
==========
Set of elementary examples showing how the :mod:`pyoed.stats` subpackage can be employed.

.. contents::
   :local:
   :depth: 2
    

Distributions
-------------


Sampling
--------

.. automodule:: pyoed.examples.stats.sampling
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        banana_potential_energy_value
        banana_potential_energy_gradient
        banana_log_density
        banana_log_density_gradient
        mcmc_sample_banana_distribution
        rejection_sample_banana_distribution


