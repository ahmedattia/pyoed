Stats Package Core
==================

Entries on this page:

.. contents::
   :local:
   :depth: 2

Distributions
^^^^^^^^^^^^^

.. automodule:: pyoed.stats.core.distributions
   :members:
   :undoc-members:
   :show-inheritance:


Sampling
^^^^^^^^

.. automodule:: pyoed.stats.core.sampling
   :members:
   :undoc-members:
   :show-inheritance:



