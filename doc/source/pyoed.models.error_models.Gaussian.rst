Gaussian Error Models
=====================

.. contents::
   :local:
   :depth: 2
    

Standard Gaussian Error Models with Binary Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.models.error_models.Gaussian._Gaussian
   :members:
   :undoc-members:
   :show-inheritance:

Gaussian Error Models with Pre-Post-Weighted Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.models.error_models.Gaussian._weighted_Gaussian_prepost
   :members:
   :undoc-members:
   :show-inheritance:

Gaussian Error Models with Pointwise-Weighted Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.models.error_models.Gaussian._weighted_Gaussian_pointwise
   :members:
   :undoc-members:
   :show-inheritance:

Gaussian Error Models for Fenics (Dolfin) Developments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.models.error_models.Gaussian._fenics_Gaussian
   :members:
   :undoc-members:
   :show-inheritance:


Helper Functions
^^^^^^^^^^^^^^^^

.. automodule:: pyoed.models.error_models.Gaussian.Gaussian
   :members:
   :undoc-members:
   :show-inheritance:

