Statistics 
==========
This subpackage provides implementations of common and specialized statistical and probabilistic models 
and algorithms. 

.. toctree::
   :maxdepth: 2

    Stats Package Core <pyoed.stats.core>
    Probability Distributions <pyoed.stats.distributions>
    Sampling <pyoed.stats.sampling>
    Statistical utilities and helper functions <pyoed.stats.stats_utils>

