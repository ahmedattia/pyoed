Laplacian Error Models
======================

.. automodule:: pyoed.models.error_models.Laplacian
   :members:
   :undoc-members:
   :show-inheritance:


