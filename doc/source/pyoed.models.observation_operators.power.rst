Power Observation Operators
===========================

.. autosummary:: 
   ~pyoed.models.observation_operators.power.Power
   ~pyoed.models.observation_operators.power.Exponential
   ~pyoed.models.observation_operators.power.create_Power_observation_operator
   ~pyoed.models.observation_operators.power.create_Exponential_observation_operator


.. automodule:: pyoed.models.observation_operators.power
   :members:
   :undoc-members:
   :show-inheritance:

