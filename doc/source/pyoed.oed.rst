Optimal Experimental Design Routines
====================================
This package provides access to all elements needed to formulate and solve OED problems 
for various applications with various settings and configurations.
Our main assumption is that an OED problem involves an inverse problem (hereby we focus
on model-constrained OED), however, the base classes provided in this package are extensible
enought to enable relaxing this assumption.


OED Main Classes
----------------

.. toctree::
   :maxdepth: 2
    
   OED utility functions (optimality criteria) <pyoed.oed.utility_functions>
   OED for sensor placement <pyoed.oed.sensor_placement>
   Robust OED for sensor placement <pyoed.oed.robust_oed>
   Common (general-purpose OED) algorithms <pyoed.oed.common>


OED Base/Core Classes
---------------------

.. toctree::
   :maxdepth: 1
    
   OED core: base classes and functionality <pyoed.oed.core>


