Optimization
============
This subpackage provides access to **novel** optimization routines for solving OED problems


.. toctree::
   :maxdepth: 1

    Scipy Optimization Interface <pyoed.optimization.scipy_optimization>

.. toctree::
   :maxdepth: 2

    Binary Optimization <pyoed.optimization.binary_optimization>


.. toctree::
   :maxdepth: 1

    Optimization Test Functions <pyoed.optimization.test_functions>
    Optimization Utilities <pyoed.optimization.optimization_utils>


Optimization Base Classes
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.optimization.core.optimizers
   :members:
   :undoc-members:
   :show-inheritance:


