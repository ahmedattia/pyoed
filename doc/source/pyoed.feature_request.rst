Collaborations and Feature Requests 
===================================

Collaborations on OED projects using PyOED (or not!)
----------------------------------------------------

We use PyOED as the main workforce for new OED developments.
However, we are always extremely excited about establishing new collaborations on solving 
challenging OED problems.



Requesting New Features in PyOED
--------------------------------



Through the Gitlab Repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Direct Communication
^^^^^^^^^^^^^^^^^^^^


