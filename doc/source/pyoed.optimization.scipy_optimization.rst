Scipy Optimization Interface
============================


.. autosummary::
   pyoed.optimization.scipy_optimization.ScipyOptimizer
   pyoed.optimization.scipy_optimization.ScipyOptimizerConfigs
   pyoed.optimization.scipy_optimization.ScipyOptimizerResults


.. automodule:: pyoed.optimization.scipy_optimization
    :members:
    :undoc-members:
    :show-inheritance:

