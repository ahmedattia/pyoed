Probability Distributions/Models
================================

Entries on this page:

.. contents::
   :local:
   :depth: 2


.. note::
   All distribution classes inherit the
   :py:mod:`distribution base class (Distribution) <pyoed.stats.core.distributions.Distribution>`
   and each distribution class/object is associated with a configurations class derived from
   the :py:mod:`distribution configurations base class (DistributionConfigs) <pyoed.stats.core.distributions.DistributionConfigs>`


Multivariate Bernoulli Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.distributions.bernoulli
   :members:
   :undoc-members:
   :show-inheritance:

Poisson Binomial Distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.distributions.poisson_binomial
   :members:
   :undoc-members:
   :show-inheritance:


Conditional Bernoulli Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.distributions.conditional_bernoulli
   :members:
   :undoc-members:
   :show-inheritance:


Combinatorial Functions
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.stats.distributions.combinatorics
   :members:
   :undoc-members:
   :show-inheritance:


