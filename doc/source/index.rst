
PyOED
=======

**PyOED** brings together variational and Bayesian data assimilation 
algorithms for inverse problems, optimal design of experiments, 
and novel optimization, statistical, and reinforcement learning solvers, 
into an integrated extensible research environment. 

.. tip::
    .. raw:: html

      <font color="red"><strong>If you are new to PyOED, we recomended to:</strong></font> 
    
    
    - **first** experiment with the :doc:`step-by-step guidance/examples 
      <pyoed.getting_started>`, then 
    - inspect :doc:`PyOED's configurations approach <pyoed.configs>`. 
    - **After that,** the user can follow our list of 
      :doc:`examples (scripts) and Tutorials (Jupyter Notebooks) 
      <pyoed.examples_tutorials>`.
    - The :doc:`full documentation <pyoed>` can then give guidance on all details of PyOED.


Working with PyOED
------------------

.. toctree::
   :maxdepth: 2

    Install <install>

.. toctree::
   :maxdepth: 2
    
    Getting Started <pyoed.getting_started>


.. toctree::
   :maxdepth: 2
    
    Examples <pyoed.examples_tutorials>

.. toctree::
   :maxdepth: 3
    
    User Guide <pyoed>
    
.. toctree::
   :maxdepth: 2
    
    Contributors <contact>
    News & More <news>
    License <pyoed.license>

.. seealso::
   
   - Go to  |pyoed_repo_link|
   - Read the  |pyoed_paper_link|

   .. |pyoed_repo_link| raw:: html
       
      <a href="https://gitlab.com/ahmedattia/pyoed" target="_blank">PyOED Source Code Repository</a>

   .. |pyoed_paper_link| raw:: html
       
      <a href="https://doi.org/10.1145/3653071" target="_blank">PyOED Paper</a>

PyOED is developed as :doc:`open-source <pyoed.license>`, 
requests and contributions are welcome.


Citing PyOED
------------

.. tip::
   If you are utilizing PyOED in your research, please cite PyOED's paper and repository.
   
   - **Bibtex:** :download:`pyoed_citation.bib`
   - **Chicago:**
       
     1. Chowdhary, Abhijit, Shady E. Ahmed, and Ahmed Attia. "PyOED: An extensible suite for data assimilation and model-constrained optimal design of experiments." ACM Transactions on Mathematical Software 50, 2 (2024), https://doi.org/10.1145/3653071.
       
     2. A. Attia, PyOED: An extensible suite for data assimilation and model-constrained optimal design of experiments, (2024), https://gitlab.com/ahmedattia/pyoed.
   

PyOED Structure at a Glance
---------------------------


.. rst-class:: center

    .. image:: ./Images/PyOED_At_A_Glance.png 
      :width: 80%
      :align: center
      :alt: PyOED high-level diagram!


