Simulation (Forward) Models
===========================



.. toctree::
   :maxdepth: 1

    Toy (Linear) Models <pyoed.models.simulation_models.toy_linear>
    Toy (Quark) Models <pyoed.models.simulation_models.toy_quark>
    Advection Diffusion <pyoed.models.simulation_models.advection_diffusion>
    Bateman Burgers <pyoed.models.simulation_models.bateman_burgers>
    Lorenz <pyoed.models.simulation_models.lorenz>
    Imaging <pyoed.models.simulation_models.imaging>
    Black Box <pyoed.models.simulation_models.black_box>

    
.. toctree::
   :maxdepth: 2
    
    Fenics Models <pyoed.models.simulation_models.fenics_models>



