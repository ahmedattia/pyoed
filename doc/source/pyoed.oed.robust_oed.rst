Robust OED Routines
===================
We provide modules for robust OED for sensor placement assuming parametric 
uncertainty models. This package is expanding, but initially we provide implementations 
for robust OED under error model misspecification.
Thus, we provide `Uncertainty Parameter` models as well as 
`Robust OED` functionality.


.. _Uncertain Parameter:

Uncertain Parameter 
^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.robust_oed.uncertain_parameters
   :members:
   :undoc-members:
   :show-inheritance:


.. _Robust OED:

Robust OED
^^^^^^^^^^

.. automodule:: pyoed.oed.robust_oed.robust_oed
   :members:
   :undoc-members:
   :show-inheritance:


