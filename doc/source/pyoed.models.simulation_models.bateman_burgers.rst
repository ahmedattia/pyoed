Bateman-Burgers Model
=====================

.. autosummary::
   ~pyoed.models.simulation_models.bateman_burgers.Burgers1D
   ~pyoed.models.simulation_models.bateman_burgers.Burgers2D

.. autosummary::
   ~pyoed.models.simulation_models.bateman_burgers.create_Burgers_1D_model
   ~pyoed.models.simulation_models.bateman_burgers.create_Burgers_2D_model


.. automodule:: pyoed.models.simulation_models.bateman_burgers
    :members:
    :undoc-members:
    :show-inheritance:


