Data Assimilation (Inference) Examples 
======================================

.. contents::
   :local:
   :depth: 2


Three-dimensional variational (3DVar) data assimilation
-------------------------------------------------------
.. automodule:: pyoed.examples.assimilation.threeDVar
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        threeDVar_subsurf
        threeDVar_toy_linear


Four-dimensional variational (4DVar) data assimilation
-------------------------------------------------------
.. automodule:: pyoed.examples.assimilation.fourDVar
    :members:
    :undoc-members:
    :show-inheritance:
    
    .. rubric:: Functions

    .. autosummary::
        
        main
        fourDVar_AD_1d
        fourDVar_AD_2d
        fourDVar_Brg_1d
        fourDVar_Brg_2d
        fourDVar_Lorenz_63
        fourDVar_Lorenz_96
        fourDVar_Dolfin_AD_2d


Goal-orienented data assimilation
---------------------------------
.. automodule:: pyoed.examples.assimilation.goal_oriented_fourDVar
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        goal_oriented_fourDVar_Dolfin_AD_2d


