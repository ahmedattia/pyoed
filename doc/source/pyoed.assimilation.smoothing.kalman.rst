Kalman Smoothers 
================

.. automodule:: pyoed.assimilation.smoothing.kalman
    :members:
    :undoc-members:
    :show-inheritance:

