Contributing to PyOED 
=====================

One can contribute by adding new functionality to PyOED. 
Functions such as those located inside 
the :doc:`PyOED utility subpackage <pyoed.utility>` are not subject to object-oriented structure
and thus are straightforward to add to the package.
One has to only add proper unittest to new functions to assure anticipated behavior.

Adding new Classes to the package, however, should follow PyOED's rules as highlighted below.
Since each PyOED object is associated with a configurations object, 
:ref:`implementing or extending PyOED classes <extend-object-configs>` 
go hand in hand with 
:ref:`implementing or extending configurations classes <extend-objects>`.

The reader is encouraged to read the following in the same order:

- :ref:`PyOED Objects and Configurations Objects <recap-rules>`
- :ref:`Extending PyOED Implementations <extend>`


.. _recap-rules:

PyOED Objects and Configurations Objects
----------------------------------------

.. rubric:: What's associated with a PyOED Object?

By design, the base PyOED object :py:class:`~pyoed.configs.configs.PyOEDObject`, automatically 
associates any PyOED object that inherits it with the following 
(for additional details see :doc:`PyOED's configurations system <pyoed.configs>`):

1. **Special Attributes/Variables:**
   
   - ``_CONFIGURATIONS_CLASS``: a class-level variable/attribute that defines the dataclass derived 
     from :py:class:`~pyoed.configs.configs.PyOEDConfigs` which is used to create a configurations object.
     For the base :py:class:`~pyoed.configs.configs.PyOEDObject`, this is assigned 
     :py:class:`~pyoed.configs.configs.PyOEDConfigs`.
   - ``_CONFIGURATIONS``: instance of the configurations class associated with ``_CONFIGURATIONS_CLASS``, 
     with aggregated values (both user input and default values where user input overwrites default values). 
     Aggregation is carried out by calling :py:meth:`~pyoed.configs.configs.PyOEDObject.aggregate_configurations`
     upon instantiation. See :ref:`instantiation steps below for more details <object-instantiation-steps>`.
   - ``_HAS_VALIDATION``: `bool` that defines (automatically detected) whether the class has an 
     implemented :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations` method or not

2. **Methods**:

   - :py:meth:`~pyoed.configs.configs.PyOEDObject.get_configurations_class`: refers to ``_CONFIGURATIONS_CLASS`` 
   - :py:meth:`~pyoed.configs.configs.PyOEDObject.get_default_configurations`: creates an instance of the 
     configurations class (``_CONFIGURATIONS_CLASS``) with default values
   - :py:meth:`~pyoed.configs.configs.PyOEDObject.aggregate_configurations` 
   - :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations` 
   - :py:meth:`~pyoed.configs.configs.PyOEDObject.update_configurations` 

3. **Properties**:

   - :py:attr:`~pyoed.configs.configs.PyOEDObject.configurations`: refers to ``_CONFIGURATIONS``
   - :py:attr:`~pyoed.configs.configs.PyOEDObject.configurations_class`:
     equivalent to (wrapper around) :py:meth:`~pyoed.configs.configs.PyOEDObject.get_configurations_class` 
   - :py:attr:`~pyoed.configs.configs.PyOEDObject.default_configurations`: 
     equivalent to (wrapper around) :py:meth:`~pyoed.configs.configs.PyOEDObject.get_default_configurations` 
     
   - :py:attr:`~pyoed.configs.configs.PyOEDObject.verbose`: `bool` that turns on/off screen verbosity of the 
     class/object (usage of this attribute is up to the developer). 
   - :py:attr:`~pyoed.configs.configs.PyOEDObject.debug`: `bool` that turns on/off debug mode (ish) of the 
     class/object (like :py:attr:`~pyoed.configs.configs.PyOEDObject.verbose` usage of this attribute is 
     up to the developer). 


.. rubric:: How is an PyOED Object inistantiated?

.. _object-instantiation-steps:

Here are the steps *DIAGRAM*


.. _extend:

Extending PyOED Implementations 
--------------------------------

In all likelihood, the developer/user will want to extend one of the classes under the PyOED subpackages,
namely, 
:doc:`a model (simulation, error, or observation operator) <pyoed.models>`,
:doc:`a data assimilation algorithm <pyoed.assimilation>`,
:doc:`optimization algorithm <pyoed.optimization>`,
:doc:`statistical model or algorithm <pyoed.stats>`, or
:doc:`an OED related object <pyoed.oed>`.
Thus, one need to first locate the class to be inherited and extend it accordingly.

Here, we use the following example to explain how to implement new classes 
to extend existing PyOED implementations. 
   
.. admonition:: Example Description
   
   Assume we want to develop a new class (to create an object) 
   that is described as follows:
   
   1. The object's class name is ``PyOEDDummyObject`` and is expected to inherit 
      the :py:class:`~pyoed.configs.configs.PyOEDObject` with the following 
      *additional* configuration keys/attributes:
    
      1. `name`: string representation of the name of the object
      2. `verbsoe`: boolean describing screen verbosity
      3. `purpose`: string describing the objective of the object
    
   2. The object has a method named ``show_purpose`` which prints out 
      the name and the purpose of the object.


**Three main steps, the user should follow:**

1. :ref:`Locate the base class that the new class/object will inherit and the corresponding configurations class  <locate-object-configs>`
2.:ref:`Extend the base configurations and define any new configuration attributes as needed <extend-object-configs>` 
3. :ref:`Develop the class for the intended object and assign the new configurations class to it <extend-objects>` 


.. _locate-object-configs:

Locate the Base Classes to Extend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Since we want the new ``PyOEDDummyOjbect`` to inherit :py:class:`~pyoed.configs.configs.PyOEDObject`, 
we can identify the associated configurations class by calling the class-level method 
:py:meth:`get_configurations_class` associated with all PyOED objects (e.g., in an interactive environment 
such as iPython, Jupyter, etc.). 
Alternatively, one can inspect the argument passed to the decorator ``set_configurations``  
which precedes (associated with) the definition of nearly all classes in PyOED. 
The latter is equivalent to finding the value of the class variable ``_CONFIGURATIONS_CLASS``.

By inspecting the source code of :py:class:`~pyoed.configs.configs.PyOEDObject`, we see that
it is preceded by (in this specific case, the decorator is added for completeness since 
``_CONFIGURATIONS_CLASS`` is also assigned the same value as a class-level variable.)

.. code-block::
    :caption: Firt few lines of the :py:class:`~pyoed.configs.configs.PyOEDObject` class (header) source code
   
    @set_configurations(configurations_class=PyOEDConfigs)
    class PyOEDObject(metaclass=PyOEDABCMeta):
        _CONFIGURATIONS_CLASS: Type[PyOEDConfigs] = PyOEDConfigs
        ...

The code above shows that :py:class:`~pyoed.configs.configs.PyOEDObject` is associated 
with :py:class:`pyoed.configs.configs.PyOEDConfigs` as the configurations class.
Thus, we start by :ref:`extending the configurations class and define the configurations attributes needed <extend-object-configs>`.


.. _extend-object-configs:

Extending Configurations Ojbects/Classes 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The :py:class:`~pyoed.configs.configs.PyOEDConfigs` is associated with the attributes
``debug``, ``verbose``, ``output_dir`` which are strongly typed as shown in the code snippet below.

.. code-block::
    :caption: Firt few lines of the :py:class:`~pyoed.configs.configs.PyOEDConfigs` class (header) source code

    @dataclass(kw_only=True, slots=True)
    class PyOEDConfigs(PyOEDContainer):

        debug: bool = SETTINGS.DEBUG
        verbose: bool = SETTINGS.VERBOSE
        output_dir: str | Path = SETTINGS.OUTPUT_DIR

This will be the template one need to follow in order to extend any configurations class in PyOED.
Since we want our new ``PyOEDDummyObject`` to have three configuration attributes ``name``, ``verbose``, ``purpose``,
we need to extend :py:class:`~pyoed.configs.configs.PyOEDConfigs`
and add only ``name`` and ``purpose``.
This is achieved by the following code

.. code-block::
    :caption: Define the configurations class to the ``PyOEDDummyObject``

    from dataclasses import dataclass   
    from pyoed.configs import PyOEDConfigs 
 
    @dataclass(kw_only=True, slots=True)
    class PyOEDDummyObjectConfigs(PyOEDConfigs):

        name: str = "A PyOED Dummy Object" 
        purpose: str = "Teaching how to create PyOED objects"


Now, ``PyOEDDummyObjectConfigs`` provides the three desired attributes in addition to ``debug`` and ``output_dir`` 
which are defined by the parent class.
That's it!
Now, one can :ref:`develop the \`\`PyOEDDummyObject\`\` <extend-objects>`


.. _extend-objects:

Extending PyOED Objects/Classes 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::
    :caption: Define the the ``PyOEDDummyObject``

    from pyoed.configs import PyOEDObject, set_configurations 

    @set_configurations(PyOEDDummyObjectConfigs)
    class PyOEDDummyObject(PyOEDObject):

        def __init__(self, configs: dict | PyOEDDummyObjectConfigs | None = None) -> None:
            # Aggregate configurations and call super 
            configs = self.configurations_class.data_to_dataclass(configs)
            
            super().__init__(configs)


First, the decorator :py:meth:`set_configurations <pyoed.configs.configs.set_configurations>` 
adds the class variable ``_CONFIGURATIONS_CLASS`` to the ``PyOEDDummyObject`` class and assigns 
it the value ``PyOEDDummyObjectConfigs``.
The ``__init__`` method of ``PyOEDDummyObject`` has a template that is generally used by all 
PyOED objects.
First, the passed configurations ``configs`` is converted to a data class and is aggregated with 
all default values. This is guaranteed by :py:meth:`~pyoed.configs.configs.PyOEDConfigs.data_to_dataclass`
associated with all classes derived from the PyOED base configurations class 
:py:class:`~pyoed.configs.configs.PyOEDConfigs`.
Second, the aggregated configurations object ``configs`` is passed to the super class which in this case
is :py:class:`~pyoed.configs.configs.PyOEDObject``.
The ``__init__`` code of :py:class:`~pyoed.configs.configs.PyOEDObject`` is responsible for 
validating the configurations and assigning the configurations object to the right attribute 
(i.e., ``_CONFIGURATIONS``). 

The object ``PyOEDDummyObject`` now can be instantiated and used properly. 
However, the validation process will only validate the configurations keys/attributes defined by 
:py:class:`~pyoed.configs.configs.PyOEDConfigs` which is the configurations class associated 
with the parent of :py:class:`~pyoed.configs.configs.PyOEDObject`.
By inspecting the source code of :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations`, 
we see that ``verbose``, ``debug``, ``output_dir`` are only validated by calling the 
:py:func:`pyoed.configs.configs.validate_configurations` function.
This needs to be implemented in ``PyOEDDummyObject`` so that all configurations are properly validated.
This is achieved by updating the definition of ``PyOEDDummyObject`` as follows 

.. code-block::
    :caption: Define the the ``PyOEDDummyObject`` with proper validation

    from pyoed.configs import PyOEDObject, set_configurations, validate_key 

    @set_configurations(PyOEDDummyObjectConfigs)
    class PyOEDDummyObject(PyOEDObject):

        def __init__(self, configs: dict | PyOEDDummyObjectConfigs | None = None) -> None:
            # Aggregate configurations and call super 
            configs = self.configurations_class.data_to_dataclass(configs)
            
            super().__init__(configs)

        def validate_configurations(
            self,
            configs: dict | PyOEDDummyObjectConfigs,
            raise_for_invalid: bool = True,
        ) -> bool:
            # Fuse/agregate configs into current/default configurations
            aggregated_configs = self.aggregate_configurations(configs=configs, )

            ## Validation Stage (of local keys/attributes defined by ``PyOEDDummyObjectConfigs``)
            # `name`: string
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="purpose",
                test=lambda v: isinstance(v, str),
                message=f"`name` argument` is of invalid type. Expected string. ",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

            # `purpose`: string
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="purpose",
                test=lambda v: isinstance(v, str),
                message=f"`purpose` argument` is of invalid type. Expected string. ",
                raise_for_invalid=raise_for_invalid,
            ):
                return False


            ## Call and return super's validation to validate configurations in parent class
            return super().validate_configurations(configs, raise_for_invalid)

Thus, new objects implemented in PyOED should be associated with ``validate_configurations`` 
method that has similar signature to :py:meth:`pyoed.configs.configs.PyOEDObject.validate_configurations` 
where the new code sandwiches validation (by calling :py:func:`pyoed.configs.configs.validate_key`) 
of new attributes defined by the configurations class.

After that, the ``PyOEDDummyObject`` can be instantiated in multiple ways as follows

.. code-block::
    :caption: Create ``PyOEDDummyObject`` with default configurations 
    
    obj = PyOEDDummyObject()
    print(obj.configurations)

which results in the following output: 

.. code-block::

    **************************************************
      Configurations of: `PyOEDDummyObjectConfigs`
    **************************************************
      >> debug (type <class 'bool'>): False
      >> verbose (type <class 'bool'>): False
      >> output_dir (type <class 'str'>): './_PYOED_RESULTS_'
      >> name (type <class 'str'>): 'A PyOED Dummy Object'
      >> purpose (type <class 'str'>): 'Teaching how to create PyOED objects'
    **************************************************

this means that if one calls ``obj.configurations`` property/attribute, the result would be 

.. code-block::
    
    PyOEDDummyObjectConfigs(debug=False, verbose=False, output_dir='./_PYOED_RESULTS_', name='A PyOED Dummy Object', purpose='Teaching how to create PyOED objects')


Now, to create the object with modified configurations, one can pass a dictionary 
(or instance of ``PyOEDDummyObjectConfigs``) with desired full or partial 
configurations to the ``PyOEDDummyObject`` upon instantiation as follows:

.. code-block::
    :caption: Create ``PyOEDDummyObject`` with modified configurations 
    
    obj = PyOEDDummyObject(
        {
            "name": "Modified name",
            "verbose": True,
        }
    )
    print(obj.configurations)

which would result in

.. code-block::
    
    **************************************************
      Configurations of: `PyOEDDummyObjectConfigs`
    **************************************************
      >> debug (type <class 'bool'>): False
      >> verbose (type <class 'bool'>): True
      >> output_dir (type <class 'str'>): './_PYOED_RESULTS_'
      >> name (type <class 'str'>): 'Modified name'
      >> purpose (type <class 'str'>): 'Teaching how to create PyOED objects'
    **************************************************

Of course, one can call the method ``PyOEDDummyObject.update_configurations`` to update 
any of the attributes of the associated configurations object. 
This method is inherited from :py:meth:`pyoed.configs.configs.PyOEDObject.update_configurations` 
and it automatically aggregates passed keys with existing/default confiurations and runs 
validation before updating the attribute values.

-----------------------------

.. rubric:: Summary

In summary, here is the full code for creating ``PyOEDDummyObject`` 
and the associated ``PyOEDDummyObjectConfigs`` described above.

.. code-block::
    :caption: Full code of ``PyOEDDummyObject`` and   ``PyOEDDummyObjectConfigs``

    from dataclasses import dataclass   
    from pyoed.configs import (
        PyOEDObject, 
        PyOEDConfigs, 
        set_configurations, 
        validate_key,
    )

    @dataclass(kw_only=True, slots=True)
    class PyOEDDummyObjectConfigs(PyOEDConfigs):

        name: str = "A PyOED Dummy Object" 
        purpose: str = "Teaching how to create PyOED objects"


    @set_configurations(PyOEDDummyObjectConfigs)
    class PyOEDDummyObject(PyOEDObject):

        def __init__(self, configs: dict | PyOEDDummyObjectConfigs | None = None) -> None:
            # Aggregate configurations and call super 
            configs = self.configurations_class.data_to_dataclass(configs)
            
            super().__init__(configs)

        def validate_configurations(
            self,
            configs: dict | PyOEDDummyObjectConfigs,
            raise_for_invalid: bool = True,
        ) -> bool:
            # Fuse/agregate configs into current/default configurations
            aggregated_configs = self.aggregate_configurations(configs=configs, )

            ## Validation Stage (of local keys/attributes defined by ``PyOEDDummyObjectConfigs``)
            # `name`: string
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="purpose",
                test=lambda v: isinstance(v, str),
                message=f"`name` argument` is of invalid type. Expected string. ",
                raise_for_invalid=raise_for_invalid,
            ):
                return False

            # `purpose`: string
            if not validate_key(
                current_configs=aggregated_configs,
                new_configs=configs,
                key="purpose",
                test=lambda v: isinstance(v, str),
                message=f"`purpose` argument` is of invalid type. Expected string. ",
                raise_for_invalid=raise_for_invalid,
            ):
                return False


            ## Call and return super's validation to validate configurations in parent class
            return super().validate_configurations(configs, raise_for_invalid)



