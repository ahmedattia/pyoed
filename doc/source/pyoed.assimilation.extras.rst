General (Extra) DA Tools
========================
This subpackage provides implementations Misc. DA and Inversion tools such as reduced order modeling (ROM) and others. 
The tools in this package will be eventually moved to other locations once fully developed. 

.. contents::
   :local:
   :depth: 2


Reduced Order Modeling
^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

    Reduced Order Modeling and Algorithms <pyoed.assimilation.extras.reduced_order_modeling>

