Toy (Quark) Model
=================

.. autosummary:: 
   ~pyoed.models.simulation_models.toy_quark.ToyQuark

.. autosummary:: 
   ~pyoed.models.simulation_models.toy_quark.create_ToyQuark_model


.. automodule:: pyoed.models.simulation_models.toy_quark
    :members:
    :undoc-members:
    :show-inheritance:


