Error Models
============
We provide error models valid to characterize priors and observation errors as
well as model errors. All implementations provided here consider a design
variable which enables activating/deactivating entries/dimensions when
possible. Comprehensive work is done on the `Gaussian Error Model` since most
observational errors are modeled as Gaussians. We also added Laplacian error
models valid for Dolfin-based simulations to broaden the audience of the
package.


.. toctree::
   :maxdepth: 2

   pyoed.models.error_models.Gaussian

.. toctree::
   :maxdepth: 1

   pyoed.models.error_models.Laplacian


