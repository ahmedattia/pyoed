Observation Operators
=====================

.. toctree::
   :maxdepth: 1

    Identity Operators <pyoed.models.observation_operators.identity>
    Power Operators <pyoed.models.observation_operators.power>
    Interpolation Operators <pyoed.models.observation_operators.interpolation>
    FEniCS Compatable Operators <pyoed.models.observation_operators.fenics_operators>



