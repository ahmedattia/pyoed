2D Advection-Diffusion Model (FEniCS)
=====================================

.. autosummary::
   ~pyoed.models.simulation_models.fenics_models.advection_diffusion.AdvectionDiffusion2D
   ~pyoed.models.simulation_models.fenics_models.advection_diffusion.create_AdvectionDiffusion2D_model
   

.. automodule:: pyoed.models.simulation_models.fenics_models.advection_diffusion
    :members:
    :undoc-members:
    :show-inheritance:

