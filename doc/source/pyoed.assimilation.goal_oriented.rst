Goal-Oriented DA Tools
======================
This subpackage provides implementations to support Goal-Oriented DA (inverse problems) formulations and solutions. 

.. contents::
   :local:
   :depth: 2


Goal-Oriented DA Base Classes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.assimilation.core.goal_oriented
   :members:
   :undoc-members:
   :show-inheritance:


Goal-Oriented Linear Inverse Problems
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.assimilation.goal_oriented.linear_inverse_problem
   :members:
   :undoc-members:
   :show-inheritance:


Prediction Operators (Time-dependent Goal Operator)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.assimilation.goal_oriented.prediction_operators.fenics_prediction_operators
   :members:
   :undoc-members:
   :show-inheritance:

