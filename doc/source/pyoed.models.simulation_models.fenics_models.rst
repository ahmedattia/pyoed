Fenics Models
=============

This subpackage provides modular implementations of simple models using the FEniCS project.

Specifically, we provide implementations two basic models those can be followed to develop further simulation models in FEniCS if needed:

2D Advection-Diffusion Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. toctree:: 
   :maxdepth: 2
   
   pyoed.models.simulation_models.fenics_models.advection_diffusion
   

2D Subsurface (Poisson) Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. toctree:: 
   :maxdepth: 2
   
   pyoed.models.simulation_models.fenics_models.subsurf
   

