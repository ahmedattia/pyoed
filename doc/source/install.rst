========================
Download & Install PyOED
========================

.. |linux| image:: /Images/linux.png
   :height: 35px
   :class: no-scaled-link

.. |windows| image:: /Images/windows.png
   :height: 35px
   :class: no-scaled-link

.. |macos| image:: /Images/macos.png
   :height: 35px
   :class: no-scaled-link

.. |docker| image:: /Images/docker.png
   :height: 35px
   :class: no-scaled-link

.. role:: alert-text

- Currently the best approach to  
  :ref:`install it locally from source <install_pyoed_from_source>`

- While unavailable currently, we plan in the near futuer 
  to enable full installation via 
  :ref:`pip (host PyOED on PyPI) <install_pyoed_from_pypi>`, 
  and 
  :ref:`conda (publish on a conda channel) <install_pyoed_from_conda_channel>`. 


.. _install_pyoed_from_source:

---------------------------------------
Install PyOED from Source (Recommended)
---------------------------------------


.. rubric:: 1. Download PyOED's source code

Navigate to the location you want to download PyOED's source code to, then run the following command: 

    .. code-block:: bash 

         git clone https://gitlab.com/ahmedattia/pyoed


.. rubric:: 2. Install Conda (or mamba): 

a. To install conda (or miniconda) can be found at 
   `Conda's official website <https://conda.io/projects/conda/en/latest/user-guide/install/index.html>`_

b. You may alternatively consider the 
   `mamba package manager <https://mamba.readthedocs.io/en/latest/index.html>`_. 
   In this case,  
   `install mamba <https://mamba.readthedocs.io/en/latest/installation/mamba-installation.html>`_,
   and then replace ``conda`` with ``mamba`` in the following instructions.


.. rubric:: 3. Create Environment & Install Dependencies 


.. tab-set::

     .. tab-item:: |linux| :math:`\,\,` Linux 
     
        .. tab-set::

           .. tab-item:: Full Functionality

              For the full functionality of PyOED, run the command

              .. code-block:: bash

                 conda create -n pyoed --file requirements.txt --file optional-requirements.txt -c conda-forge
                
              This will create a new conda environment named ``pyoed``. You
              can then activate the environment by running

              .. code-block:: bash

                 conda activate pyoed

           .. tab-item:: Minimal Dependencies

              For the minimal set of dependencies for `PyOED`, run instead:

              .. code-block:: bash

                 conda create -n pyoed --file requirements.txt -c conda-forge

              This will create a new conda environment named ``pyoed``. You
              can then activate the environment by running

              .. code-block:: bash

                 conda activate pyoed

              .. note::
                 This will not install FEniCS, nor will it install dependencies necessary to build the documentation. 
                 If you need these, follow the instructions for full functionality instead.

     .. tab-item:: |macos| :math:`\,\,` macOS 

        .. warning::
           The Apple Silicon chips require Rosetta in order to install legacy
           FEniCS. As the new FEniCSx project develps we will migrate all optional
           functionality from to FEniCSx and ultimately remove this. If you do not
           need FEniCS support, follow all instructions for x86 macOS instead to
           avoid any potential breakage of the conda environment.
           
        .. tab-set::

           .. tab-item:: |macos| :math:`\,\,` macOS (x86 only)
                
              .. tab-set::

                 .. tab-item:: Full Functionality

                    For the full functionality of PyOED, run the command

                    .. code-block:: console

                       conda create -n pyoed --file requirements.txt --file optional-requirements.txt -c conda-forge
                        
                    This will create a new conda environment named ``pyoed``. You
                    can then activate the environment by running

                    .. code-block:: console

                       conda activate pyoed

                 .. tab-item:: Minimal Dependencies

                    For the minimal set of dependencies for `PyOED`, run instead:

                    .. code-block:: console

                       conda create -n pyoed --file requirements.txt -c conda-forge

                    This will create a new conda environment named ``pyoed``. You
                    can then activate the environment by running

                    .. code-block:: console

                       conda activate pyoed

                    .. note::
                       This will not install FEniCS, nor will it install dependencies necessary to build the documentation. 
                       If you need these, follow the instructions for full functionality instead.

           .. tab-item:: |macos| :math:`\,\,` macOS with Apple Silicon

              .. tab-set::

                 .. tab-item:: Full Functionality

                    Create a conda environement
                    
                    .. code-block:: console

                       conda create -n pyoed 
                    
                    Activate the ``pyoed`` you just installed

                    .. code-block:: bash
                       
                       conda activate pyoed

                    Set the environment to use osx-64 instead of arm-64.  
                    :alert-text:`This will run only once and 
                    MUST BE DONE IMMEDIATELY after creating and
                    activating the environment at the first time.`

                    .. code-block:: console

                       conda config --env --set subdir osx-64
                    
                    For the full functionality of PyOED, install all requirements 

                    .. code-block:: console

                       conda install --file requirements.txt --file optional-requirements.txt -c conda-forge
                        

                 .. tab-item:: Minimal Dependencies

                    Create a conda environement
                    
                    .. code-block:: bash

                       conda create -n pyoed 
                    
                    Activate the ``pyoed`` you just installed

                    .. code-block:: bash
                       
                       conda activate pyoed

                    Set the environment to use osx-64 instead of arm-64.  
                    :alert-text:`This will run only once and 
                    MUST BE DONE IMMEDIATELY after creating and
                    activating the environment at the first time.`

                    .. code-block:: bash

                       conda config --env --set subdir osx-64
                    
                    For the minimal functionality of PyOED, install non-optional requirements only 

                    .. code-block:: bash

                       conda install --file requirements.txt -c conda-forge

                    .. note::
                       This will not install FEniCS, nor will it install dependencies necessary to 
                       build the documentation. 
                       However, by following the instructions above, you can later install FEniCS 
                       without worrying about environment conflicts.

     .. tab-item:: |windows| :math:`\,\,` Windows

        .. warning::
           For the moment, installation instructions will are specific for the
           **Linux and macOS** operating systems. 

           While Windows is not explicitly supported, one could install the package
           using the same instructions for **Linux**  using the 
           `Windows Subsystem for Linux
           <https://learn.microsoft.com/en-us/windows/wsl/>`_ (WSL) utility.

           We are working to provide support for PyOED under windows, and we will
           provide instructions here accordingly.

        

.. rubric:: 4. Install PyOED 

1. Navigate to the top directory of the package where ``setup.py`` is located

2. Install PyOED using pip (because all requirements are already installed using 
   conda as above no further packages are added)::

     pip install --upgrade pip
     pip install -e .



.. _install_pyoed_from_pypi:

-----------------------------
Install from PyPi (using pip)
-----------------------------
.. raw:: html

    <font color="red"><strong>Not yet available (soon!)</strong></font> 
 
Currently, only :ref:`installation from source is supported <install_pyoed_from_source>`

.. _install_pyoed_from_conda_channel:

-----------------------------------------
Install from conda channels (using conda)
-----------------------------------------
.. raw:: html

    <font color="red"><strong>Not yet available (soon!)</strong></font> 

Currently, only :ref:`installation from source is supported <install_pyoed_from_source>`


