Core (Base Classes) of DA (Inversion) Methods
=============================================

.. contents::
   :local:
   :depth: 2


.. automodule:: pyoed.assimilation.core.assimilation
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pyoed.assimilation.core.filtering
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pyoed.assimilation.core.smoothing
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pyoed.assimilation.core.goal_oriented
   :members:
   :undoc-members:
   :show-inheritance:


