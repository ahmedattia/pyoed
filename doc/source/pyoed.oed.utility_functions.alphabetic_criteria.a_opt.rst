A Optimality (Alphabetic) Criteria 
===================================

.. contents::
   :local:
   :depth: 2


 
Bayesian A Optimality Criterion (Binary Design)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.a_opt.bayesian_a_opt
   :members:
   :undoc-members:
   :show-inheritance:

Relaxed Bayesian A Optimality Criterion (Binary or Relaxed Design)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.a_opt.relaxed_bayesian_a_opt
   :members:
   :undoc-members:
   :show-inheritance:


Robust Bayesian A Optimality 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.a_opt.robust_bayesian_a_opt
   :members:
   :undoc-members:
   :show-inheritance:

