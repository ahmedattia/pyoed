Binary Optimization
===================
This subpackage provides access to **novel** optimization routines for solving binary optimization problems


.. toctree::
   :maxdepth: 1

    Probabilistic (Stochastic) Binary Optimization <pyoed.optimization.binary_optimization.stochastic_binary_optimization>
    Constrained Probabilistic Binary Optimization <pyoed.optimization.binary_optimization.constrained_binary_optimization>
    Greedy Binary Optimization <pyoed.optimization.binary_optimization.greedy_binary_optimization>
    Robust Binary Optimization <pyoed.optimization.binary_optimization.robust_binary_optimization>
    Constrained Robust Binary Optimization <pyoed.optimization.binary_optimization.robust_constrained_binary_optimization>

