D Optimality (Alphabetic) Criteria 
===================================

.. contents::
   :local:
   :depth: 2
 

Bayesian D Optimality Criterion (Binary Design)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.d_opt.bayesian_d_opt
   :members:
   :undoc-members:
   :show-inheritance:

Relaxed Bayesian D Optimality Criterion (Binary or Relaxed Design)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.d_opt.relaxed_bayesian_d_opt
   :members:
   :undoc-members:
   :show-inheritance:


Robust Bayesian D Optimality 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: pyoed.oed.utility_functions.alphabetic_criteria.d_opt.robust_bayesian_d_opt
   :members:
   :undoc-members:
   :show-inheritance:


