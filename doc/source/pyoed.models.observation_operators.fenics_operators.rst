FEniCS Compatable Observation Operators
=======================================

.. autosummary:: 
   ~pyoed.models.observation_operators.fenics_observation_operators.DolfinPointWise
   ~pyoed.models.observation_operators.fenics_observation_operators.create_DolfinPointWise_observation_operator


.. automodule:: pyoed.models.observation_operators.fenics_observation_operators
   :members:
   :undoc-members:
   :show-inheritance:

