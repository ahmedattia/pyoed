PyOED Configurations (Configs) System
=====================================
In order to equip each PyOED object with maximum flexibility and make sure PyOED is 
highly extensible, we need to assure both accuracy and robustness.
Initially PyOED used to configure each object by aggregating keyword arguments with 
default configurations dictionaries. This proved to be problematic, and we migrated 
to employing datacalsses. Thus, PyOED is currently adopting configurations data 
classes with simple, yet powerful, configurations system.
This page describes the :ref:`fundamentals of PyOED configurations <configs-rules>` 
as well as the details of the :ref:`configurations module in PyOED <configs-module>`.

.. important::
    
   The following :ref:`configurations fundamental rules <configs-rules>` are mandated by construction 
   in PyOED and are absolutely critical for developers intending to expand PyOED modules/classes 
   or contributing to the package

.. tip::
    
   To learn about extending PyOED implementations, check :doc:`the PyOED contribution guide <pyoed.contribute>`. 


.. _configs-rules:

PyOED's Configurations (Configs) Fundamentals 
---------------------------------------------
1. PyOED configurations system is :py:mod:`pyoed.configs.configs`.
2. All PyOED objects inherit the :py:class:`PyOEDObject <pyoed.configs.configs.PyOEDObject>` base class. 
3. All PyOED objects are associated with a configurations object derived from :py:class:`PyOEDConfigs <pyoed.configs.configs.PyOEDConfigs>`
   typically named (by convention) as the object with a postfix ``Configs``. 
   For example, a PyOED class ``ObjectA`` is typically associated with a ``ObjectAConfigs``
   configurations class
4. Each PyOED object is automatically associated with the following attributes:
    
   - ``_HAS_VALIDATION``: a `bool` class variable describing whether the object has 
     a :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations` function implemented that is different 
     from the one defined by the :py:class:`PyOEDObject` base class.
   - ``_CONFIGURATIONS_CLASS``: a class variable defining the type of the configurations class. 
     This is typically, the configurations class (derived from 
     :py:class:`PyOEDConfigs <pyoed.configs.configs.PyOEDConfigs>`) that 
     the developer associates with the PyOED object.
   - ``_CONFIGURATIONS``: An instance of the associated configurations class (defined by 
     ``_CONFIGURATIONS_CLASS``) which is created upon instantiating the PyOED object. 
       
5. The user/developer should never need to access the ``_HAS_VALIDATION`` class variable/attribute.
   Moreover, the two attributes ``_CONFIGURATIONS_CLASS`` and ``_CONFIGURATIONS_CLASS`` should never 
   be accessed directly. Thus user/developer should always
   use the corresponding attributes/properties defined by the :py:class:`PyOEDObject <pyoed.configs.configs.PyOEDObject>` 
   base class which are thus associated with all PyOED objects. These properties/attributes are as follows:
    
   - ``configurations`` gives access to ``_CONFIGURATIONS``.
   - ``configurations_class``  gives access to ``_CONFIGURATIONS_CLASS``.

6. Each PyOED object need to implement methods those override (or call super's identical one) 
   the following two methods:
   
   1. :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations`: 
      This method checks the types and potentially the values in the configurations object 
      associated with that PyOED object; the configurations object is the one assigned 
      to `_CONFIGURATIONS` and is accessible by the 
      :py:attr:`pyoed.configs.configs.PyOEDObject.validate_configurations`.  
   
   2. :py:meth:`~pyoed.configs.configs.PyOEDObject.update_configurations`:
      This method takes a set of key-worded arguments, and associated values
      and aggregate them with existing (or default) configurations, validate the aggregated configurations,
      and do further specific updates.

   For additional details on how to develop new PyOED objects, see 
   :doc:`the PyOED contribution guide <pyoed.contribute>`.

7. Upon instantiating any PyOED object, the class ``__mro__`` is traversed upward to aggregate
   all configurations defined by the associated ``_CONFIGURATIONS_CLASS`` with its parents. 
   Once the full set of configurations is created, it is validated by calling the method 
   :py:meth:`~pyoed.configs.configs.PyOEDObject.validate_configurations` associated with each object.
   If no exceptions are thrown, and the configurations are validated (:py:meth:`validate_configurations`  returns `True`),
   the PyOED object is created.

.. _configs-module:

PyOED's Configurations Module (Classes & Functions)
---------------------------------------------------

.. rubric:: Configurations Classes

.. autosummary::

   ~pyoed.configs.configs.PyOEDContainer
   ~pyoed.configs.configs.PyOEDConfigs
   ~pyoed.configs.configs.PyOEDData
   ~pyoed.configs.configs.PyOEDObject
   ~pyoed.configs.configs.PyOEDRulesViolationError
   ~pyoed.configs.configs.PyOEDConfigsValidationError
   ~pyoed.configs.configs.PyOEDABCMeta
   ~pyoed.configs.configs.SETTINGS

.. rubric:: Configuration Functions 

.. autosummary::
   ~pyoed.configs.configs.set_configurations
   ~pyoed.configs.configs.remove_unknown_keys
   ~pyoed.configs.configs.extract_unknown_keys
   ~pyoed.configs.configs.validate_key
   ~pyoed.configs.configs.aggregate_configurations


Configurations Classes
^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.configs.configs
    :members: PyOEDContainer, PyOEDConfigs, PyOEDData, PyOEDObject, PyOEDABCMeta, SETTINGS 
    :undoc-members:
    :member-order: alphabetical
    :show-inheritance:
    
.. autoexception:: PyOEDConfigsValidationError
.. autoexception:: PyOEDRulesViolationError


Configurations Functions
^^^^^^^^^^^^^^^^^^^^^^^^

.. autofunction:: pyoed.configs.configs.set_configurations
.. autofunction:: pyoed.configs.configs.class_doc_inheritance
.. autofunction:: pyoed.configs.configs.remove_unknown_keys
.. autofunction:: pyoed.configs.configs.extract_unknown_keys
.. autofunction:: pyoed.configs.configs.validate_key
.. autofunction:: pyoed.configs.configs.aggregate_configurations


