Optimal Experimental Design (OED)
=================================

.. contents::
   :local:
   :depth: 2
   
OED for sensor placement with relaxed design
--------------------------------------------
.. automodule:: pyoed.examples.oed.relaxed_oed
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        create_penalty_function
        relaxed_OED_time_dependent_toy_linear
        create_dolfin_observation_operator
        relaxed_OED_Dolfin_AD_2d
        get_base_output_dir


OED for sensor placement with binary design
--------------------------------------------
.. automodule:: pyoed.examples.oed.stochastic_binary_oed
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        get_base_output_dir


OED with FeniCS-based Advection Diffusion Model 
-----------------------------------------------
.. automodule:: pyoed.examples.oed.OED_AD_FE
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        main
        get_base_output_dir


Robust OED 
----------
.. automodule:: pyoed.examples.oed.robust_oed.robust_oed
    :members:
    :undoc-members:
    :show-inheritance:

    .. rubric:: Functions

    .. autosummary::
        
        robust_oed_paper_main
        create_problem_from_settings
        plot_robust_oed_results_2d_from_file
        plot_AD_settings
        plot_robust_oed_results
        create_penalty_function
        robust_OED_time_dependent_toy_linear
        robust_OED_Dolfin_AD_2d
        _solve_inverse_problem
        _find_global_optimum
        analyze_inverse_problem_solution
        get_base_output_dir

