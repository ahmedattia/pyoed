Blackbox Models
===============

.. autosummary:: 
   ~pyoed.models.simulation_models.black_box.TimeIndependentBlackBox
   ~pyoed.models.simulation_models.black_box.create_TimeIndependentBlackBox_model


.. automodule:: pyoed.models.simulation_models.black_box
    :members:
    :undoc-members:
    :show-inheritance:


