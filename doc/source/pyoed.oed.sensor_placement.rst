Sensor Placement Routines
=========================

A unified interface for OED for sensor placement applications is provided by the 
module `pyoed.oed.sensor_placement.sensor_placement_bayesian_oed`.
Sensor placement OED problems are specialized to the case of finding 
optimal observation configurations.
The main speciality of this class is that it define the effect of the experimental 
``design`` as:
    
    1. update the design of the observation operator (binary)
    2. update the design of the observation error model (binary or relaxed).

Thus, one needs to choose the appropriate noise and observation models to suite 
the desired OED formulation.
Solving an OED problem (sensor placement or not) requires choosing an optimizer.
Thus, in the configurations an optimizer is set, and one can register desire optimizer
which itself defines whether to solve binary or relaxed OED optimization problem.


OED for Sensor Plcement 
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pyoed.oed.sensor_placement.sensor_placement_bayesian_oed
   :members:
   :undoc-members:
   :show-inheritance:

