#!/usr/bin/env python

# Copyright © 2023, UChicago Argonne, LLC
# All Rights Reserved

import setuptools

if __name__ == "__main__":
    setuptools.setup()
